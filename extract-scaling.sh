#!/bin/sh
# Extracts scaling information from the output files that are written by the
# sldg program. 
#
# Usage:
# ./extract-scaling.sh directory_to_run_in weak|strong
#
# The program assumes that each subdirectory in the specified folder contains
# the output of a single run of sldg.
echo $1
cd $1
xold=1
cnt=1
coresold=1
for dir in `ls -d [0-9]*/ | sort -n`; do
    x=`grep 'Runtime' $dir/output.dat | awk 'BEGIN{a=0}{a=a>$2?a:$2}END{print a}'`

    if [ "$cnt" -eq "2" ]; then
        xold=$x
        coresold=${dir%/}
    fi
    cnt=$((cnt+1))
    cores=${dir%/}
    if [ "$2" == "weak" ]; then
        echo -e $cores "\\t &" `echo $x | awk '{printf "%.2f",$1}'` "\\t &" `echo "$xold $x" | awk '{printf "%.2f",$1/$2}'` "\t \\\\\\\\"
    elif [ "$2" == "strong" ]; then
        echo -e $cores "\\t &" `echo $x | awk '{printf "%.2f",$1}'` "\\t &" `echo "$xold $x $coresold $cores" | awk '{printf "%.2f",$1/$2*$3/$4}'` "\t \\\\\\\\"
    else
        echo -e $cores "\\t &" `echo $x | awk '{printf "%.2f",$1}'` "\\t &" `echo "$xold $x" | awk '{printf "%.2f",$1/$2}'` "\\t &"  `echo "$xold $x $coresold $cores" | awk '{printf "%.2f",$1/$2*$3/$4}'` "\t \\\\\\\\"
    fi
done

