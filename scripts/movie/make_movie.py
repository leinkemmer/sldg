#import numpy as np
#import matplotlib.pyplot as plt
#from matplotlib.animation import FuncAnimation
#
#fig, ax = plt.subplots()
#xdata, ydata = [], []
#ln, = plt.plot([], [], 'ro', animated=True)
#
#def init():
#    ax.set_xlim(0, 2*np.pi)
#    ax.set_ylim(-1, 1)
#    return ln,
#
#def update(frame):
#    xdata.append(frame)
#    ydata.append(np.sin(frame))
#    ln.set_data(xdata, ydata)
#    return ln,
#
#anim = FuncAnimation(fig, update, frames=np.linspace(0, 2*np.pi, 128),
#                    init_func=init, blit=True)
# anim.save('evolution.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

from numpy import *
from pylab import *
import matplotlib.animation as animation
from os import listdir
import re

def load(fn):
    print('loading: ',fn)
    z=loadtxt(fn)
    nx=len(unique(z[:,0]))
    ny=len(unique(z[:,1]))
    z=z[np.lexsort((z[:,0],z[:,1]))]
    z=z[:,2]
    return z.reshape((ny,nx))

def get_minmax(fn, i):
    z = loadtxt(fn)
    return min(z[:,0]), max(z[:,0])


# frames=5
times = []
for fn in listdir('.'):
    m = re.match(r'f-t([0-9]*(\.?)[0-9]*)-0,0.data',fn)
    if m:
        times.append(m.group(1))
times = sorted(times, key=lambda x: float(x))
# print(times)
# n = frames*len(times)
# times = ['0', '0.6', '1.2', '1.8', '2.4']

x_min, x_max = get_minmax('f-t{0}-0,0.data'.format(times[0]),0)
v_min, v_max = get_minmax('f-t{0}-0,0.data'.format(times[0]),1)

FFMpegWriter = animation.writers['ffmpeg']
writer = FFMpegWriter(fps=15, extra_args=['-vcodec', 'libx264'])

fig = figure();

z = load('f-t{0}-0,0.data'.format(0))
with writer.saving(fig, "writer_test.mp4", dpi=100):
    for i,t in zip(range(len(times)),times): 
        print(t)
        z = load('f-t{0}-0,0.data'.format(t))
        # im.set_array(z)
        # mat = np.random.random((100,100))
        # imshow(mat)
        imshow(z, extent=[x_min, x_max, v_min, v_max])
        # writer.grab_frame(bbox_inches='tight', pad_inches=0)
        # writer.grab_frame(bbox_inches='tight')
        savefig('frame-{0}.png'.format(i),bbox_inches='tight', pad_inches=0)


# rcParams['savefig.bbox'] = 'tight'
# rcParams['savefig.pad_inches'] = 0 

# fig = figure();
# im = imshow(load('f-t{0}-0,0.data'.format(times[0])), animated=True, extent=[x_min, x_max, v_min, v_max])
# fig.tight_layout()

t = 0
def updatefig(i):
    global t
    # print('processing i=',i)
    if i % frames == 0:
        t = times[int(i/frames)]
        print('t=',t)
        z = load('f-t{0}-0,0.data'.format(t))
        im.set_array(z)
    return im,



# anim = animation.FuncAnimation(fig, updatefig, arange(1,n), blit=True)
# anim.save('evolution.mp4', fps=30, writer='ffmpeg_file', savefig_kwargs={bbox_inches:'tight', pad_inches:0}) #, extra_args=['-vcodec', 'libx264'])
# , bbox_inches='tight', pad_inches=0
