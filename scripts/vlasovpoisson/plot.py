from sys import argv, exit
from netCDF4 import *
from pylab import *

if len(argv)!=2:
    print('ERROR: exactly one argument, the file name, is required.')
    exit(1)

fn = argv[1]


with Dataset(fn,'r') as fs:
    for var in fs.variables:
        print(var)
    data_E = fs.variables['E'][:]
    data_f = fs.variables['f'][:]
    x1 = fs.variables['x1'][:]
    x2 = fs.variables['x2'][:]
    v1 = fs.variables['v1'][:]
    v2 = fs.variables['v2'][:]


def to_uniform_grid(dat,c1,c2):
    x_uniform = linspace(c1[0],c1[-1],len(c1))
    y_uniform = linspace(c2[0],c2[-1],len(c2))
    [xq,yq] = meshgrid(c1, c2)
    return griddata(xq.flatten(),yq.flatten(),dat.flatten(),x_uniform,y_uniform, interp='linear')

def E_field(k):
    return to_uniform_grid(data_E[k,:,:],x1,x2)

def f_slice_x1v1(ix2,iv2):
    return to_uniform_grid(data_f[iv2,:,ix2,:],x1,v1)

def f_slice_x2v2(ix1,iv1):
    return to_uniform_grid(data_f[:,iv1,:,ix1],x2,v2)


figure()

subplot(2,2,1)
title('E1')
imshow(E_field(0),extent=[x1[0],x1[-1],x2[0],x2[-1]])
colorbar()
xlabel('x1')
ylabel('x2')

subplot(2,2,2)
title('E2')
imshow(E_field(1),extent=[x1[0],x1[-1],x2[0],x2[-1]])
colorbar()
xlabel('x1')
ylabel('x2')

subplot(2,2,3)
title('f(x1,half_point,v1,half_point)')
imshow(f_slice_x1v1(int(len(x2)/2),int(len(v2)/2)),extent=[x1[0],x1[-1],v1[0],v1[-1]])
colorbar()
xlabel('x1')
ylabel('v1')

subplot(2,2,4)
title('f(half_point,x2,half_point,v2)')
imshow(f_slice_x2v2(int(len(x1)/2),int(len(v1)/2)),extent=[x2[0],x2[-1],v2[0],v2[-1]])
colorbar()
xlabel('x2')
ylabel('v2')

show()

