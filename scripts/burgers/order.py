#!/usr/bin/env python
import os
import re
from sys import argv, exit
from pylab import *
from scipy.integrate import ode
from scipy.ndimage import convolve # the numpy convolution does not support periodic bc

# Some examples
# final_time = 0.1
# problem = 'sin';  a = 0.0;   b = 2*pi
# problem = 'sech'; a = -20; b = 20
# problem = 'lorentz'; a = -25; b = 25;
# problem = 'periodic'; a=0.0; b=2*pi;


if len(argv)!=5:
    print('ERROR: exactly 4 arguments are required (problem, final_time, a, b)')
    exit(1)
problem = argv[1]
final_time = float(argv[2])
a = eval(argv[3]) # eval is sued to allow input such as pi, for example
b = eval(argv[4])

L=b-a

def sech(x):
    return 1.0/cosh(x)

def init_u0(N):
    x = linspace(a,b-L/float(N),N)
    if problem == 'sin':
        u0 = sin(x)
    elif problem == 'sech':
        u0 = sech(x)
    elif problem == 'lorentz':
        u0 = 1.0/(1.0+x**2)
    elif problem == 'osc':
        u0 = (abs(x)<1)*cos(20*(0.5+abs(x))*x)*exp(-1/(1-x**2))
    else:
        print('ERROR: no suitable initial value specified')
        exit(1)

    return u0


def error(ex):
    re_fp='[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
    output = os.popen(ex).read()
    m = re.search('Error: ({0})'.format(re_fp), output)
    return float(m.group(1))

def run(ex):
    os.popen(ex).read()
    return loadtxt('f-t{0}.data'.format(final_time))[:,1]

def error_ref(ex, ref):
    u1 = run(ex)
    return max(abs(u1-ref));

def f(t, u):
    n = len(u)
    u_hat = fft(u);
    u_hat *= -(2*pi)/(b-a)*1j*fftfreq(len(u), d=1.0/float(n)) # need the frequencies that correspond to the continuous Fourier transform
    u_d   = real(ifft(u_hat))
    return u*u_d

def cvode_fft(t, u0):
    r = ode(f).set_integrator('dopri5', method='dopri5', atol=1e-15, rtol=1e-15)
    r.set_initial_value(u0, 0.0)
    dt = 0.01
    while r.successful() and r.t < t:
        r.integrate(r.t+dt)
    return r.y

def run_fft(t, N, u0):
    x  = linspace(a,b-L/float(N),N)  # the usual linspace hack
    return cvode_fft(t, u0)

def f_lag(t, u, order):
    n = len(u)
    h = (b-a)/float(n)
   
    if order==4: 
        stencil_plus  = -array([0.0, 2.0, 3.0, -6.0, 1.0])/6.0
        stencil_minus = array([1.0, -6.0, 3.0, 2.0, 0.0])/6.0
    elif order==6:
        stencil_plus  = -array([0.0, -3.0, 30.0, 20.0, -60.0, 15.0, -2.0])/60.0
        stencil_minus = array([-2.0, 15.0, -60.0, 20.0, 30.0, -3.0, 0.0])/60.0
    elif order==8:
        stencil_plus  = -array([0.0, 4.0, -42.0, 252.0, 105.0, -420.0, 126.0, -28.0, 3.0])/420.0
        stencil_minus = array([3.0, -28.0, 126.0, -420.0, 105.0, 252.0, -42.0, 4.0, 0.0])/420.0
    else:
        print('ERROR: order={0} for lag is not implemented'.format(order))

    usq = 0.5*u*u
    u_d_plus  = convolve(usq, stencil_plus, mode='wrap')/h 
    u_d_minus = convolve(usq, stencil_minus, mode='wrap')/h 
    u_d = (u>=0)*u_d_plus + (u<0)*u_d_minus

    # return u*u_d
    return u_d

def cvode_lag(t, u0, order):
    r = ode(f_lag).set_integrator('dopri5', method='dopri5', atol=1e-15, rtol=1e-15)
    r.set_initial_value(u0, 0.0).set_f_params(order)
    dt = 0.01
    while r.successful() and r.t < t:
        r.integrate(r.t+dt)
    return r.y

def run_lag(t, N, order):
    x  = linspace(a,b-L/float(N),N)  # the usual linspace hack
    u0 = init_u0(N)
    return cvode_lag(t, u0, order)


def error_plot(ex, final_time, problem, dof, orders):
    N_ref = 4*dof[-1]
    x = linspace(a,b-L/float(N_ref),N_ref)
    u0 = init_u0(N_ref)
    fft_ref = run_fft(final_time, N_ref, u0)
    plot(x, u0)
    plot(x, fft_ref)
    savefig('solution.pdf')
    savetxt( 'test.data', array([x,fft_ref]).transpose())

    figure(figsize=(6,4.5))
    # figure(figsize=(9,4.5+2.25))

    # do the fft stuff
    l_err= []; l_dof = [];
    print('===== FFT =====')
    for N in dof:
        stride = round(N_ref/N)
        u0 = init_u0(N)
        u1  = run_fft(final_time, N, u0)
        err = max(abs(fft_ref[0:N_ref:stride]-u1))
        print(N,err)
        l_dof.append(N)
        l_err.append(err)

    loglog(l_dof, l_err, label='FFT', linewidth=2, marker='d', ms=5);


    for o in orders:
        print('===== o={0} ====='.format(o))
        print(ex.format('DOF',o,final_time,problem,0.5*L))
        if problem != 'sin':
            dof_ref = dof[-1]*4
            N_ref   = round(dof_ref/o)
            exec_str = ex.format(N_ref,o,final_time,problem,0.5*L)
            ref = run(exec_str)

        l_dof = []; l_err = []
        last_err = 1;
        for d in dof:
            exec_str = ex.format(round(d/o),o,final_time,problem,0.5*L)
            if problem == 'sin':
                err = error(exec_str)
            else:
                err = error_ref(exec_str, ref)
            DOF=round(d/o)*o
            if d == dof[0]:
                print(DOF,err)
            else:
                print(DOF,err,log(last_err/err)/log(2))
            last_err = err;
            l_dof.append(round(d/o)*o); l_err.append(err)
        # do the plotting
        loglog(l_dof, l_err, label='o={0}'.format(o), linewidth=2, marker='o', ms=5);
       
    # do the lag stuff
    N_ref = 4*dof[-1]
    for order in [4, 6]:
        l_err= []; l_dof = [];
        print('===== LAG{0} ====='.format(order))
        for N in dof:
            stride = round(N_ref/N)
            u1  = run_lag(final_time, N, order)
            err = max(abs(fft_ref[0:N_ref:stride]-u1))
            print(N,err)
            l_dof.append(N)
            l_err.append(err)

        loglog(l_dof, l_err, label='Lag{0}'.format(order), linewidth=2, marker='v', ms=5);

        N_ref = 4*dof[-1]
        ref = run_lag(final_time, N_ref, 8)
        err_ref = amax(ref-fft_ref)
        print('Error Lagrange vs FFT (consistency check): ', err_ref)
        if order==6:
            if err_ref>1e-6:
                print('ERROR: reference solutions are not very close together')
                # exit(1)


    legend(loc='lower left')
    minorticks_off()
    xlim([dof[0]/1.4,dof[-1]*1.2]); xticks(dof,dof)
    xlabel('degrees of freedom'); ylabel('error')
    if problem == 'sin':
        savefig('order-{0}-{1}.pdf'.format(problem,final_time),bbox_inches='tight')
    else:
        savefig('order-{0}-{1}-L{2}.pdf'.format(problem,final_time,L),bbox_inches='tight')


exec_order = '../../burgers --num_cells {0} --order {1} --iter_method secant -T {2} --step_size {2} --problem {3} --iter 10 -L {4} --enable_output 1'
error_plot(exec_order,final_time,problem,[8,16,32,64,128,256,512,1024,2048],[2,4,6,8])

