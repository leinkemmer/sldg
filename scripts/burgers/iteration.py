#!/usr/bin/env python
import os
import re
from pylab import *

final_time = 0.05

num_cells = 512; order = 4;

def error(ex):
    re_fp='[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
    output = os.popen(ex).read()
    m = re.search('Error: ({0})'.format(re_fp), output)
    return float(m.group(1))

def error_plot(ex, final_time, methods, iterations):
    figure(figsize=(6,4.5))

    for m in methods:
        print('===== m={0} ====='.format(m))
        l_iter = []; l_err = [];
        for i in iterations:
            exec_str = ex.format(m, i, final_time, num_cells, order)
            err = error(exec_str)
            print(i,err)
            l_iter.append(i); l_err.append(err)
        # do the plotting
        mm = m; col = 'g'
        if m == 'fixedpoint':
            mm = 'fixed-point'
            col = 'b'
        if m == 'newton':
            mm = 'Newton'
            col = 'r'
        semilogy(l_iter, l_err, col, label=mm, linewidth=2, marker='o', ms=5)

    legend(loc='best')
    minorticks_off()
    xlim([0.75,l_iter[-1]+0.25]); xticks(l_iter,l_iter)
    xlabel('number of iterations'); ylabel('error')
    savefig('iteration-sin-{0}.pdf'.format(final_time),bbox_inches='tight')


exec_order = '../../burgers --num_cells {3} --order {4} --iter_method {0} -T {2} --problem sin --iter {1} --step_size {2} --enable_output 0'
error_plot(exec_order, final_time, ['fixedpoint', 'secant', 'newton'], [1,2,3,4,5,6,7,8,9,10])
