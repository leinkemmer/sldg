#!/usr/bin/env python
from numpy import *
from pylab import *
import sys

if len(sys.argv) != 2:
    print("ERROR: please specify the file to plot as the single argument.")
    sys.exit(1)

z=loadtxt(sys.argv[1])
nx=len(unique(z[:,0]))
ny=len(unique(z[:,1]))
z=z[np.lexsort((z[:,0],z[:,1]))]
print(z)
z=z[:,2]
z=z.reshape((ny,nx))
imshow(z)
colorbar()
show()
