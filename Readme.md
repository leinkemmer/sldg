# README #

This software package provides a framework that implements the semi-Lagrangian
discontinuous Galerkin method for advection problems on distributed memory
systems (clusters). For parallelization pure MPI or a hybrid approach (using
OpenMP and MPI) can be used. Also some support for graphic processing units
(GPUs) using CUDA is available. The framework facilitate a dimension
independent implementation. A more detailed description can be found in the
Introduction section below.

The current version of the software can be obtained from
https://bitbucket.org/leinkemmer/sldg.


### Build ###

The software is build using cmake. For example, using

    mkdir build
    cd build
    ccmake ..
    make

Different programs within the package require different libraries. However, at
least an MPI implementation and the Boost library have to be installed.

### Build SLDG on clusters ###

The exact build process depends on the cluster in question. Usually one has to load
the modules for the compiler toolchain and MPI and CUDA (if desired). For example,
on our local GPU cluster

    module purge
    module load gnu7/7.3.0 autotools mpich cuda/11.2 cmake lapack
    export CC=$(which gcc)
    export CXX=$(which g++)
    export MPICC=$(which mpicc)
    export MPICXX=$(which mpic++)
    mkdir build
    cd build
    cmake -DCUDA_ENABLED=ON ..
    make

On the VSC3

    module purge
    module load gcc/7.3 openmpi cmake cuda/10.0.130
    # CC and CXX are automatically set by the build system
    export MPICC=$(which mpicc)
    export MPICXX=$(which mpic++)
    mkdir build
    cd build
    cmake -DCUDA_ENABLED=ON ..
    make

### Run ###

Once the desired program has been built (for example, vlasovpoissonpv) the
available command line options can be queried as follows (in the build folder)

    src/vlasovpoisson/vlasovpoissonpv --help

and the program can be run as follows

    src/vlasovpoisson/vlasovpoissonpv --problem ll --dof "16 16 16 16 4 4 4 4"

In this case the evolution.data file includes the time evolution of the
invariants and the electric energy. In addition, snapshots of the solution are
written to disk in the netcdf file format.


### Test ###

The software package uses cmake for testing. Thus, all available tests can be
run by executing (in the build folder)

    ctest


### Overview of the example programs ###

A range of example programs are part of this software package. Their source
code is found in in the src directory. At present these are

**vlasovpoissonpv:** 4D Vlasov-Poisson solver with MPI parallelization in the
velocity direction only. Includes GPU support.

**vlasovpoisson:** 2D Vlasov-Poisson solver with MPI parallelization in both
the space and velocity direction.

**KP/viscous-burgers/burgers:** Solvers for the KP equation, the viscous
Burgers' equation and the Burgers' equation. These programs showcase the
semi-Lagrangian discontinuous Galerkin scheme in the nonlinear setting.


### Introduction ###

The source files for the framework can be found in the include folder. The
include folder is divided into generic (mostly utility routines), container
(data structures and simple operations), and algorithms subfolders.  The user
of this framework will most likely interact with functions and classes from the
following files:

* algorithm/dg.hpp implements the advection step for the semi-Lagrangian
  discontinuous Galerkin method.
* container/domain.hpp manages the data on each MPI process. It includes
  functions to initialize the data, obtain slices of the data and, prepare
  boundary data (for use in MPI functions).
* generic/mpi.hpp manages the distribution of the physical domain to the
  different MPI processes and provides facilities to convert multi dimensional
  indices to the corresponding MPI ranks.

Let us describe the three main aspects of the implementation necessary to
extend existing codes (such as the Vlasov--Poisson implementation provided with
this software package) and to implement new schemes based on the
semi-Lagrangian discontinuous Galerkin framework.

The dimension of the problem is a template parameter denoted by d. Thus, in
order to change the dimension of a given implementation we have to recompile
the application. This, in our view, is not a serious limitation; furthermore,
it enables the compiler to apply additional optimizations for each use case.
Also, in this framework it is easy to supply specific optimizations which can
only be applied to a two dimensional problem, for example. As the index type we
use the typesafe boost::array class from the Boost library.

In developing dimension independent scientific codes it is vital to have a
construct that enables us to loop over all (or a subset) of the indices in a
multi-dimensional array. Our framework provides the function iter_next to
facilitate this behavior. Its prototype and a usage example are given in the
following listing.

	template<size_t d>
	bool iter_next(array<Index,d> &idx, array<Index,d> max, array<Index,d>* start = NULL);
	
	array<Index, d> idx; array<Index,d> max; // initialization of idx and max
	do { 	
		// computation 
	} while(iter_next(idx,max));

The argument idx specifies the current (multi-)index and max denotes the upper
bound of the iteration in each direction. In addition, the optional third
parameter can be used to specify a starting value that is different from zero
in one or more directions.

In order to perform communication over the MPI interface the appropriate slice
of the multi-dimensional data has to be extracted. To facilitate this in a
dimension independent way, the following two functions are provided.

	void pack_boundary(Index dim, leftright lr, Index bdr_len,
		vector<double>& bdr_data, multi_array<double,d>* F);

	double* unpack_boundary(array<Index,2*d-2> idx, Index dim, Index bdr_len,
		vector<double>& bdr_data)

The user has to specify the dimension in which the translation is to be
conducted (dim), the number of cells that need to be communicated (in the
dim-direction; bdr_len), and a vector to hold the boundary data (bdr_data). In
addition, the direction of the translation is given (lr); alternatively, also a
vector of advection speeds can be specified (F). In the latter case the
direction of the translation can be different at each degree of freedom (in the
directions orthogonal to dim). The vector bdr_data is then send via an
appropriate MPI call.  The receiving process uses the unpack_boundary function
in order to determine the appropriate pointer that is passed to the translate1d
function. 

The translate1d function performs the actual computation of the advection and
its prototype is given in the following listing.

	void translate1d(typename node::view& u, typename node::view& out,
		double* boundary, int boundary_size, double dist, dg_coeff<order>& c,
        translate1d_mode tm=TM_ALL);

The first two arguments specify the input and output iterator, respectively.
The pointer to the appropriate boundary data is determined by the
unpack_boundary function. In addition, we have to specify the number of cells
in the translation direction (boundary_size), and the distance (dist) of the
translation. In addition, for the last argument we can specify TM_INTERIOR. In
this case only the part of the computation that can be done without knowing the
boundary data is performed (in this situation it is permissible to pass a
nullptr pointer as the third argument). This then has to be followed up, in
order to complete the computation, by an additional call which specifies
TM_BOUNDARY as the last argument and a proper pointer to boundary data. This is
useful for interleaving communication with computation.

### License ###

The code is open source under the MIT license. If you use this code in an academic
setting, please consider providing a citation to either one of the following papers

```
@article{sldg2019,
    title={{A performance comparison of semi-Lagrangian discontinuous Galerkin and spline based Vlasov solvers in four dimensions}},
    author={Einkemmer, L.},
    journal={J. Comput. Phys.},
    volume={376},
    pages={937--951},
    year={2019},
}

@article{sldg2015,
    title={{High performance computing aspects of a dimension independent semi-Lagrangian discontinuous Galerkin code}},
    author={Einkemmer, L.},
    journal={Comput. Phys. Commun.},
    volume={202},
    pages={326-336},
    year={2016},
}
```

