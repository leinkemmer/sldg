# - Find FFTWMPI
# Find the native FFTW includes and library
#
#  FFTWMPI_INCLUDES    - where to find fftw3-mpi.h
#  FFTWMPI_LIBRARIES   - List of libraries when using FFTW.
#  FFTWMPI_FOUND       - True if FFTW found.

if (FFTWMPI_INCLUDES)
  # Already in cache, be silent
	set (FFTWMPI_FIND_QUIETLY TRUE)
endif (FFTWMPI_INCLUDES)

find_path (FFTWMPI_INCLUDES fftw3-mpi.h)

find_library (FFTWMPI_LIBRARIES NAMES fftw3_mpi)

# handle the QUIETLY and REQUIRED arguments and set FFTW_FOUND to TRUE if
# all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (FFTWMPI DEFAULT_MSG FFTWMPI_LIBRARIES FFTWMPI_INCLUDES)

mark_as_advanced (FFTWMPI_LIBRARIES FFTWMPI_INCLUDES)
