# - Find Parallel NetCDF
#
#  PNETCDF_INCLUDES    - where to find pnetcdf.h
#  PNETCDF_LIBRARIES   - List of libraries when using parallel NetCDF.
#  PNETCDF_FOUND       - True if NetCDF found.

if (PNETCDF_INCLUDES)
  # Already in cache, be silent
  set (PNETCDF_FIND_QUIETLY TRUE)
endif (PNETCDF_INCLUDES)

find_path (PNETCDF_INCLUDES pnetcdf.h)

find_library (PNETCDF_LIBRARIES NAMES pnetcdf)

# handle the QUIETLY and REQUIRED arguments and set PNETCDF to TRUE if
# all listed variables are TRUE
include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (PNETCDF DEFAULT_MSG PNETCDF_LIBRARIES PNETCDF_INCLUDES)

mark_as_advanced (PNETCDF_LIBRARIES PNETCDF_INCLUDES)
