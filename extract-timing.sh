#!/bin/sh
# Extract the timing information from the output files that are written by the
# sldg program.
#
# Usage:
# ./extract-timing.sh directory_to_run_in
#
# The program assumes that each subdirectory in the specified folder contains
# the output of a single run of sldg.
cd $1
grep 'Runtime' 1/output.dat | awk 'BEGIN{a=0}{a=$1 " " $3 " " $5 " " $7 " " $9 " " $11}END{print "# " a}'
for dir in `ls -d [0-9]*/ | sort -n`; do
	cores=${dir%/}
	grep 'Runtime' $dir/output.dat | awk -v cores=$cores 'BEGIN{a=0;b=0;c=0;d=0;e=0;f=0;g=0;h=0}{a=a>$2?a:$2; b=b>$4-$12?b:$4-$12; c=c>$6?c:$6; d=d>$8?d:$8; e=e>$10?e:$10; f=f>$12?f:$12; g=g>$14?g:$14a; h=h>$16?h:$16}END{printf("%5ld%10.3lf%10.3lf%10.3lf%10.3lf%10.3lf%10.3lf%10.3lf%10.3lf\r\n",cores,a,b,c,d,e,f,g,h);}'
done
