#!/usr/bin/env python
## This script collects the information from the evolution.data files written by
## each MPI process and plots the corresponding quantity.

from numpy import *
from pylab import *

import sys

if len(sys.argv) != 2:
    print('ERROR: exactly one argument expected that specifies the directory of the output data')
    sys.exit(1)

fn=sys.argv[1]+'/evolution-{0},{1}.data'; i=1; j=0;
print('Try to load ',fn.format(0,0))
data=loadtxt(fn.format(0,0))
x=data[:,0]
mass=data[:,1]
l1=data[:,2]
l2=data[:,3]
ke=data[:,4]
ee=data[:,5]
te=data[:,6]
while True:
    try:
        while True:
            fns=fn.format(i,j)
            print('Trying to read ',fns)
            newdata=loadtxt(fns);
            for k in range(data.shape[0]):
                mass[k]+=newdata[k][1]
                l1[k]+=newdata[k][3]
                l2[k]+=newdata[k][3]
                ke[k]+=newdata[k][4]
                ee[k]+=newdata[k][5]
                te[k]+=newdata[k][6]
            i+=1
    except FileNotFoundError:
        if i==0:
            break
        i=0; j+=1

print('Loaded {0} files with dimension {1}'.format(i,data.shape));

subplot(3,2,1); title('mass error'); semilogy(x,abs((mass-mass[0])/mass[0]));
subplot(3,2,2); title('l1 error'); semilogy(x,abs((l1-l1[0])/l1[0]));
subplot(3,2,3); title('l2 error'); semilogy(x,abs((l2-l2[0])/l2[0]));
subplot(3,2,4); title('energy error'); semilogy(x,abs((te-te[0])/te[0]));
subplot(3,2,5); title('kinetic energy'); plot(x,ke); 
subplot(3,2,6); title('electric energy'); semilogy(x,ee);

show()

