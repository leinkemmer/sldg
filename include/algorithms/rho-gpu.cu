#pragma once

#include <generic/common.hpp>
#include <cub/cub.cuh> 
#include <algorithms/dg-gpu.cu>

enum class type_of_reduction {rho_and_invariants_fluxes, 
                              meanvelocity, temperature};

//each dof in x gets on thread, which reduces over all v
template<size_t dx, size_t dv, type_of_reduction reduction_type>
__global__
void k_rho_from_f(fp* f, fp* scratch_rho, fp* scratch_u, fp* scratch_T,
                  int* e, fp* a, double* d_invariants,
                  fp* weights, fp* gauss_x, fp* delta, fp* energyflux_w,
                  fp* pparticleflux_w) {

    constexpr size_t d = dx+dv;

    // x-dimensions
    int n_bigstride=0;
    int lastdim = e[d-1]*e[d+d-1];
    int int_part = lastdim/gridDim.y;
    int remainder = lastdim-int_part*gridDim.y;
    for(int i=0;i<int(blockIdx.y);++i){
        n_bigstride += int_part + (i<remainder);
    }
    int end = n_bigstride + int_part + (int(blockIdx.y)<remainder);

    size_t stride = blockDim.x*gridDim.x; 
    size_t idx = threadIdx.x + blockDim.x*blockIdx.x;

    fp w_x=1.0;
    int offset=0;
    int idx_tmp = idx;
    for(size_t i=0;i<dx;++i){
        int idx_n = idx_tmp/e[d+i];
        int idx_o = idx_tmp-idx_n*e[d+i];
        idx_tmp   = idx_n;
        w_x      *= weights[idx_o+offset]*delta[i]*0.5;
        offset   += e[d+i];
        idx_tmp  /= e[i];
    }

    f += idx;
    fp inverse_rho_x;
    fp u;

    if(reduction_type == type_of_reduction::rho_and_invariants_fluxes)
        scratch_rho += blockIdx.y*stride;

    if(reduction_type == type_of_reduction::meanvelocity){
        scratch_u += blockIdx.y*stride;
        inverse_rho_x = 1.0/(scratch_rho[idx]);
    }

    if(reduction_type == type_of_reduction::temperature){
        scratch_T += blockIdx.y*stride;
        inverse_rho_x = 1.0/(scratch_rho[idx]);
        u = scratch_u[idx];
    }

    // remaining dimensions (if available) (maximal 3 for the velocities)
    int n_thread[3] = {1,1,1};
    int o_thread[3] = {1,1,1};
    int n_start[3] = {0,0,0};
    int o_start[3] = {0,0,0};
    for(size_t i=0;i<dv;i++) {
        n_thread[i] = e[dx+i];
        o_thread[i] = e[d+dx+i];
    }
    n_start[dv-1] = n_bigstride/e[d+d-1];
    o_start[dv-1] = n_bigstride - n_start[dv-1]*e[d+d-1]; 

    fp  zeros[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
    fp  ones[6]  = {1.0,1.0,1.0,1.0,1.0,1.0};
    fp* weights_ptr[3] = {ones, ones, ones};
    fp* gauss_x_ptr[3] = {zeros, zeros, zeros};
    fp delta_p[3]      = {2.0, 2.0, 2.0};
    fp a_p[3]          = {1.0, 0.0, 0.0}; 
    for(size_t i=0;i<dv;i++) {
        weights_ptr[i] = weights + offset;
        gauss_x_ptr[i] = gauss_x + offset;
        delta_p[i]     = delta[i+dx];
        a_p[i]         = a[i+dx];
        offset += e[d+dx+i];
    }
 
    double sum = 0.0;
    double l1  = 0.0;
    double l2  = 0.0;
    double entr= 0.0;
    double ke  = 0.0;
    double mom1 = 0.0;

    double pparticleflux_l = 0.0;
    double pparticleflux_r = 0.0;
    double energyflux = 0.0;

    double meanvelocity = 0.0;
    double temperature  = 0.0;

    fp deltav = 0.125*delta_p[0]*delta_p[1]*delta_p[2];

    for(int l=n_start[2];l<n_thread[2];l++){
        for(int p=o_start[2];p<o_thread[2];p++){
            if(dv==3){
                if(p==o_thread[2]-1)
                    o_start[2]=0;
                if(p+l*o_thread[2]>=end)
                    break;
            }
            for(int j=n_start[1];j<n_thread[1];j++)
                for(int m=o_start[1];m<o_thread[1];m++){
                    if(dv==2){
                        if(m==o_thread[1]-1)
                            o_start[1]=0;
                        if(m+j*o_thread[1]>=end)
                            break;
                    }
                    for(int i=n_start[0];i<n_thread[0];i++)
                        for(int k=o_start[0];k<o_thread[0];k++) {
                            if(dv==1){
                                if(k==o_thread[0]-1)
                                    o_start[0]=0;
                                if(k+i*o_thread[0]>=end)
                                    break;
                            }                          
                            fp w_v = deltav*weights_ptr[0][k]*
                                     weights_ptr[1][m]*weights_ptr[2][p];
                            size_t lin_idx_v = (k + o_thread[0]*(i + n_thread[0]*
                                               (m + o_thread[1]*(j + n_thread[1]*
                                               (p + o_thread[2]*(l))))));

                            fp val = f[stride*lin_idx_v];

                            fp v1 = a_p[0] + (i + gauss_x_ptr[0][k])*delta_p[0];
                            fp v2 = a_p[1] + (j + gauss_x_ptr[1][m])*delta_p[1];
                            fp v3 = a_p[2] + (l + gauss_x_ptr[2][p])*delta_p[2];
                            fp vsq = v1*v1+v2*v2+v3*v3;

                            if(reduction_type == 
                                    type_of_reduction::rho_and_invariants_fluxes){

                                fp w = w_x*w_v; 

                                sum  += w_v*val;
                                l1   += w*abs(val);
                                l2   += w*val*val;
                                //fp fc = max(val,1e-32);
                                //entr += w*fc*log(fc);
                                ke   += 0.5*w*val*vsq;
                                mom1 += w*v1*val;

                                energyflux += 0.5*w_v*v1*vsq*val;
                                int v1idx = k+o_thread[0]*i;
                                if(v1idx<e[dx]*e[d+dx]*0.5)
                                    pparticleflux_l += w_v*v1*val;
                                else
                                    pparticleflux_r += w_v*v1*val;
 
                            } 

                            if(reduction_type == type_of_reduction::meanvelocity){
                                meanvelocity += w_v*v1*val*inverse_rho_x; 
                            }

                            if(reduction_type == type_of_reduction::temperature){
                                temperature += w_v*(v1-u)*(v1-u)*val*inverse_rho_x;
                            }

                        }
                }
        }
    }

    // We have treated the entire v space, so we can write back to scratch.

    if(reduction_type == type_of_reduction::rho_and_invariants_fluxes){

        scratch_rho[idx] = sum;

        int idx_i = idx + stride*blockIdx.y;
        int stride_i = stride*gridDim.y;
        d_invariants[idx_i + 0*stride_i] = sum*w_x;
        d_invariants[idx_i + 1*stride_i] = l1;
        d_invariants[idx_i + 2*stride_i] = l2;
        d_invariants[idx_i + 3*stride_i] = entr;
        d_invariants[idx_i + 4*stride_i] = ke;
        d_invariants[idx_i + 5*stride_i] = mom1;

        //WARNING: gives only reasonable data for dx=1
        //left boundary
        if(idx==0){
            energyflux_w[blockIdx.y] = energyflux;
            pparticleflux_w[blockIdx.y] = pparticleflux_l;
        }
        //right boundary
        if(idx==e[0]*e[d]-1){
            energyflux_w[gridDim.y + blockIdx.y] = energyflux;
            pparticleflux_w[gridDim.y + blockIdx.y] = pparticleflux_r;
        }
 
    }

    if(reduction_type == type_of_reduction::meanvelocity){
        scratch_u[idx] = meanvelocity;
    }

    if(reduction_type == type_of_reduction::temperature){
        scratch_T[idx] = temperature;
    }
 
}

__global__
void k_finalize_rho(int additional_x, fp* rho_work, fp* d_rho){

    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    int stride = blockDim.x*gridDim.x;
 
    fp val=0.0; 
    for(int i=0;i<additional_x;++i){
        val += rho_work[idx + i*stride];
    }
    d_rho[idx] = val;
}


//is moment2 a good name for \int_R (sum_i^n v_i^2) f dv ??
template<size_t dx, size_t dv>
struct reduction_on_gpu {

    static constexpr size_t d = dx+dv;
    using domain_x = multi_array<fp,2*dx>;

    int tot_num_w, n_I, Nx;
    int num_threads, num_blocks_x, additional_x;
    gpu_array<char> d_temp_storage;
    size_t temp_storage_bytes;
    gpu_array<fp> weights;
    gpu_array<fp> gauss_x;
    gpu_array<fp> a; 
    gpu_array<fp> delta;
    gpu_array<double> results;
    gpu_array<int> e; 
    gpu_array<int> cub_offsets;
    unique_ptr<gpu_array<fp>> energyflux_work;
    unique_ptr<gpu_array<fp>> pparticleflux_work;
    unique_ptr<gpu_array<double>> invariants;

    unique_ptr<gpu_array<fp>> rho_work;
    unique_ptr<gpu_array<fp>> uT_work;

    reduction_on_gpu(index_dxdv_no<dx,dv> ext, 
                  array<fp,d> _a, array<fp,d> _b,
                  int min_num_blocks=750, 
                  int min_num_threads=128)
        : n_I{6}, num_blocks_x{1}, additional_x{1}, 
          d_temp_storage{0}, temp_storage_bytes{0},
          weights{sum(ext.o_part())}, gauss_x{sum(ext.o_part())},
          a{d}, delta{d}, results{n_I}, e{2*d}, cub_offsets{n_I+1} {

        num_threads = ext[0]*ext[d];
        for(int i=1;i<dx;++i) 
            num_blocks_x *= ext[i]*ext[d+i];
        if(num_threads < min_num_threads){
            if(dx > 1) {
                num_threads  *= ext[d+1];
                num_blocks_x /= ext[d+1];
            }
        }
        if(num_threads > 512){
            num_threads /= ext[d];
            num_blocks_x *= ext[d];
        }

        int tot_blocks = num_blocks_x;
        int tot_dof_v = prod(ext.v_part());
        while(tot_blocks < min_num_blocks && 
              additional_x < ext[d-1]*ext[d+d-1] &&
              additional_x*40 < tot_dof_v){
            additional_x++;
            tot_blocks = num_blocks_x*additional_x;
        }        

        cout << "Reduction: (num_blocks_x, additional_x) = (" 
             << num_blocks_x << "," << additional_x 
             << "), num_threads = " << num_threads << endl;

        Nx = prod(ext.x_part());

        invariants = make_unique<gpu_array<double>>(n_I*Nx*additional_x);
        if (additional_x > 1) {
            rho_work = make_unique<gpu_array<fp>>(Nx*additional_x); 
            uT_work  = make_unique<gpu_array<fp>>(Nx*additional_x); 
            cout << "length work: " << rho_work->size() << endl;
        }
        //2*additional_x due to left and right energyflux
        energyflux_work = make_unique<gpu_array<fp>>(2*additional_x);
        pparticleflux_work = make_unique<gpu_array<fp>>(2*additional_x);

        //Determine and allocate temporary device storage 
        //requirements for the CUB routine
        for(int i=0;i<=n_I;++i)
            cub_offsets[i] = i*Nx*additional_x;  
        cub_offsets.copy_to_gpu(); 
        cub::DeviceSegmentedReduce::Sum(d_temp_storage.d_data, temp_storage_bytes, 
                                        invariants->d_ptr(), results.d_data,
                                        n_I, cub_offsets.d_data, 
                                        cub_offsets.d_data + 1);
        d_temp_storage.resize(temp_storage_bytes);

        array<Index,2*d> _e = ext.get_array(); 
        e.set(begin(_e), end(_e));
        e.copy_to_gpu();

        a.set(begin(_a), end(_a));
        for(size_t i=0;i<d;i++)
            delta[i] = (_b[i]-a[i])/fp(e[i]);
        a.copy_to_gpu();
        delta.copy_to_gpu(); 

        int offset=0;
        for(size_t i=0;i<d;i++) {
            vector<fp> w = gauss::all_weights(e[d+i]);
            copy(begin(w), end(w), &weights[offset]);

            vector<fp> gx = gauss::all_x_scaled01(e[d+i]);
            copy(begin(gx), end(gx), &gauss_x[offset]);

            offset += e[d+i];
        }
        weights.copy_to_gpu();
        gauss_x.copy_to_gpu(); 
    }


    void set_domain(vector<fp> _a, vector<fp> _b){

        a.set(begin(_a), end(_a));
        for(size_t i=0;i<d;i++)
            delta[i] = (_b[i]-_a[i])/fp(e[i]);

        a.copy_to_gpu();
        delta.copy_to_gpu(); 

    }


    template<type_of_reduction reduction_type>
    void compute(multi_array<fp,2*(dx+dv)>& d_f, 
                 domain_x& d_rho, domain_x& d_u, domain_x& d_T, 
                 fp& mass, fp& l1, fp& l2, fp& entr, fp& ke, fp& mom, 
                 fp& ppflux_left, fp& ppflux_right,
                 fp& eflux_left, fp& eflux_right){

        timer t;
        t.start();

        dim3 num_blocks(num_blocks_x, additional_x);


        if(additional_x == 1) {
            //rho_work is directly rho, same for other moments

            k_rho_from_f<dx,dv,reduction_type><<<num_blocks,num_threads>>>(
                   d_f.data(), d_rho.data(), d_u.data(), d_T.data(),
                   e.d_data, a.d_data, invariants->d_data, weights.d_data,
                   gauss_x.d_data, delta.d_data, energyflux_work->d_data,
                   pparticleflux_work->d_data);
            cudaDeviceSynchronize(); 
            gpuErrchk( cudaPeekAtLastError() );

        } else {

            if(reduction_type == type_of_reduction::rho_and_invariants_fluxes){

                k_rho_from_f<dx,dv,reduction_type><<<num_blocks,num_threads>>>(
                       d_f.data(), rho_work->d_data, 
                       uT_work->d_data, uT_work->d_data,
                       e.d_data, a.d_data, invariants->d_data, weights.d_data,
                       gauss_x.d_data, delta.d_data, energyflux_work->d_data,
                       pparticleflux_work->d_data);
                cudaDeviceSynchronize(); 
                gpuErrchk( cudaPeekAtLastError() );

                k_finalize_rho<<<num_blocks_x,num_threads>>>(additional_x, 
                                      rho_work->d_data, d_rho.data());
            } 

            if(reduction_type == type_of_reduction::meanvelocity){

                k_rho_from_f<dx,dv,reduction_type><<<num_blocks,num_threads>>>(
                       d_f.data(), d_rho.data(), 
                       uT_work->d_data, uT_work->d_data,
                       e.d_data, a.d_data, invariants->d_data, weights.d_data,
                       gauss_x.d_data, delta.d_data, energyflux_work->d_data,
                       pparticleflux_work->d_data);
                cudaDeviceSynchronize(); 
                gpuErrchk( cudaPeekAtLastError() );

                k_finalize_rho<<<num_blocks_x,num_threads>>>(additional_x, 
                                      uT_work->d_data, d_u.data());
            }

            if(reduction_type == type_of_reduction::temperature){

                k_rho_from_f<dx,dv,reduction_type><<<num_blocks,num_threads>>>(
                       d_f.data(), d_rho.data(), 
                       d_u.data(), uT_work->d_data,
                       e.d_data, a.d_data, invariants->d_data, weights.d_data,
                       gauss_x.d_data, delta.d_data, energyflux_work->d_data,
                       pparticleflux_work->d_data);
                cudaDeviceSynchronize(); 
                gpuErrchk( cudaPeekAtLastError() );

                k_finalize_rho<<<num_blocks_x,num_threads>>>(additional_x, 
                                      uT_work->d_data, d_T.data());
            }

            cudaDeviceSynchronize(); 
            gpuErrchk( cudaPeekAtLastError() );
        }


        if(reduction_type == type_of_reduction::rho_and_invariants_fluxes){

            // Run cub-sum-reduction
            cub::DeviceSegmentedReduce::Sum(d_temp_storage.d_data, 
                                            temp_storage_bytes, 
                                            invariants->d_data, results.d_data,
                                            n_I, cub_offsets.d_data, 
                                            cub_offsets.d_data + 1);
            results.copy_to_host();
            cudaDeviceSynchronize(); 
            gpuErrchk( cudaPeekAtLastError() );

            mass = results[0];
            l1   = results[1];
            l2   = results[2];
            entr = results[3];
            ke   = results[4];
            mom  = results[5];

            energyflux_work->copy_to_host();
            pparticleflux_work->copy_to_host();

            fp ppflux_l = 0.0;
            fp ppflux_r = 0.0;
            fp eflux_l = 0.0;
            fp eflux_r = 0.0;
            for(int i=0;i<additional_x;++i){
                eflux_l  += (*energyflux_work)[i];
                eflux_r += (*energyflux_work)[i+additional_x];
                ppflux_l  += (*pparticleflux_work)[i];
                ppflux_r += (*pparticleflux_work)[i+additional_x];
            }

            cudaDeviceSynchronize();
            gpuErrchk(cudaPeekAtLastError());

            ppflux_left = ppflux_l;
            ppflux_right = ppflux_r;
            eflux_left = eflux_l;
            eflux_right = eflux_r;
        }

        t.stop();
        //cout << "total rho-gpu.compute: " << t.total() << endl;
    }


    void compute_rho_and_invariants(multi_array<fp,2*(dx+dv)>& d_f, domain_x& d_rho,
                          fp& mass, fp& l1, fp& l2, fp& entr, fp& ke, fp& mom,
                          fp& particle_flux_l, fp& particle_flux_r,
                          fp& energy_flux_l, fp& energy_flux_r) {
    
        constexpr type_of_reduction rt = 
            type_of_reduction::rho_and_invariants_fluxes;

        compute<rt>(d_f, d_rho, d_rho, d_rho, 
                    mass, l1, l2, entr, ke, mom, 
                    particle_flux_l, particle_flux_r, 
                    energy_flux_l, energy_flux_r);
    }


    void compute_meanvelocity(multi_array<fp,2*(dx+dv)>& d_f, 
                              domain_x& d_rho,
                              domain_x& d_u){
        
        constexpr type_of_reduction rt = type_of_reduction::meanvelocity;
        fp dummy;
        compute<rt>(d_f, d_rho, d_u, d_u, 
                    dummy, dummy, dummy, dummy, dummy, 
                    dummy, dummy, dummy, dummy, dummy);
    }


    void compute_temperature(multi_array<fp,2*(dx+dv)>& d_f, 
                             domain_x& d_rho,
                             domain_x& d_u,
                             domain_x& d_T){
        
        constexpr type_of_reduction rt = type_of_reduction::temperature;
        fp dummy;
        compute<rt>(d_f, d_rho, d_u, d_T, 
                    dummy, dummy, dummy, dummy, dummy,
                    dummy, dummy, dummy, dummy, dummy);
    }

};


