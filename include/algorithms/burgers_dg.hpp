#pragma once

#include <generic/common.hpp>
#include <algorithms/dg.hpp>

enum iterative_method {
    IM_FIXEDPOINT,
    IM_SECANT,
    IM_NEWTON
};

fp eval_u(fp xl, int idx, int o, dg_evaluator& dg_eval, fp* u,
        int boundary_offset) {
    return dg_eval.eval(xl,u+boundary_offset+idx*o);
}

fp burgers_characteristics_fixedpoint(fp x, fp tau, int n, int o,
        int boundary_size, fp L,
        int num_it, dg_evaluator& dg_eval, fp *__u0, fp *__u0_deriv, fp& xb) {
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    fp u1=0.0;

    xb=x;
    for(int i=0;i<num_it;i++) {
        int idx   = floor(xb/h);
        fp xl = xb/h - idx;

        u1 = eval_u(xl,idx,o,dg_eval,__u0,boundary_size*o);

        xb = x - tau*u1; // update the endpoint of the characteristic
    }
    return u1;
}

fp burgers_characteristics_secant(fp x, fp tau, int n, int o, int boundary_size,
        fp L, int num_it, dg_evaluator& dg_eval, fp *__u0, fp *__u0_deriv,
        fp& xb) {
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    fp u1=0.0;
    fp xbm1; fp um1;

    xb=x;
    for(int i=0;i<num_it;i++) {
        int idx   = floor(xb/h);
        fp xl = xb/h - idx;

        u1 = eval_u(xl,idx,o,dg_eval,__u0,boundary_size*o);

        fp tmp=xb;
        if(i==0)
            xb = x - tau*u1;
        else {
            fp f1 = x - tau*u1 - xb;
            fp f0 = x - tau*um1 - xbm1;
            if(abs(f1-f0)>1e-14)
                xb = xb - f1*(xb-xbm1)/(f1-f0);
        }
        xbm1=tmp; um1=u1;

    }
    return u1;
}

fp burgers_characteristics_newton(fp x, fp tau, int n, int o, int boundary_size,
        fp L, int num_it, dg_evaluator& dg_eval, fp *__u0, fp *__u0_deriv,
        fp& xb) {
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    fp u1=0.0, u1_deriv;

    xb=x;
    for(int i=0;i<num_it;i++) {
        int idx   = floor(xb/h);
        fp xl = xb/h - idx;

        u1       = eval_u(xl,idx,o,dg_eval,__u0,boundary_size*o);
        u1_deriv = eval_u(xl,idx,o,dg_eval,__u0_deriv,boundary_size*o);

        fp f = x - tau*u1 - xb;
        if(abs(f)>1e-14) {
            fp f_deriv = -tau*u1_deriv - 1.0;
            xb = xb - f/f_deriv;
        }

    }
    return u1;
}

void get_ucell(int i, int n, int o, domain<1>::view& u, fp* bdr_left,
        fp* bdr_right, vector<fp>& ucell) {
    if(i<0) { // left boundary
        int offset = -i-1;
        for(int j=0;j<o;j++) ucell[j] = bdr_left[offset*o+j];
    } else if(i>=n) { // right boundary
        // effective index that is zero for the first boundary cell
        int offset = i-n; 
        for(int j=0;j<o;j++) ucell[j] = bdr_right[offset*o+j];
    } else // interior
        for(int j=0;j<o;j++) ucell[j] = u[i][j];
}


fp eval(fp x, fp h, int o, dg_evaluator& dg_eval, fp* u, int boundary_offset) {
    int idx = floor(x/h);
    fp xl = x/h - idx;
    return eval_u(xl,idx,o,dg_eval,u,boundary_offset);
}

// this solves the linearized problem (using a simple approximation of the
// characteristics, mostly a test case)
void burgers_dg_linearized(domain<1>::view& u, domain<1>::view& out, fp tau,
        fp L, fp* left_boundary, fp* right_boundary, int boundary_size,
        int num_it, iterative_method im=IM_NEWTON) {
    int n=u.shape()[0]; int o=u.shape()[1];
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    dg_evaluator dg_eval(o);

    // prepare a contiguous memory for subsequent evaluations of u
    int boundary_offset = boundary_size*o;
    vector<fp> __u0(n*o+2*boundary_offset);
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            __u0[boundary_offset+i*o+j] = u[i][j];
    // include the boundary
    for(int i=0;i<boundary_size;i++)
        for(int j=0;j<o;j++) {
            __u0[j+o*i] = left_boundary[boundary_offset-1-(o*i+o-1-j)];
            __u0[boundary_offset+n*o+i*o+j] = right_boundary[j+o*i];
        }

    // set output to zero
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            out[i][j] = 0.0;

    vector<fp> quad_val(o);
    vector<fp> w_inv(o); for(int i=0;i<o;i++) w_inv[i]=1.0/gauss::w(i,o);
    vector<fp> w(o); for(int i=0;i<o;i++) w[i]=gauss::w(i,o);
    // iterate over all cells (including the boundary)
    for(int i=-boundary_size;i<n+boundary_size;i++) {
        // Predict where a function with support in cell i ends up by following
        // the characteristics forward in time
        fp x_left  = i*h;
        fp x_right = (i+1)*h;

        fp u_left = eval(x_left,h,o,dg_eval,&__u0[0],boundary_size*o);
        fp x_t1_left  = x_left  + u_left*tau;
        fp u_right = eval(x_right,h,o,dg_eval,&__u0[0],boundary_size*o);
        fp x_t1_right = x_right + u_right*tau;

        int m_left = floor(x_t1_left/h); int m_right = floor(x_t1_right/h);
        for(int m=max(0,m_left);m<=min(n-1,m_right);m++) {
            fp xi_left  = m==m_left  ? x_t1_left/h-m  : 0.0;
            fp xi_right = m==m_right ? x_t1_right/h-m : 1.0;

            // solve the characteristics backward in time
            for(int g=0;g<o;g++) {
                // the characteristic starts at the Gauss-Legendre quadrature
                // nodes
                fp x_t1 = h*(m + xi_left 
                          + (xi_right-xi_left)*gauss::x_scaled01(g,o));

                fp u0 = eval(x_t1,h,o,dg_eval,&__u0[0],boundary_size*o);
                fp xb = x_t1 - u0*tau;

                quad_val[g] = eval(xb,h,o,dg_eval,&__u0[0],boundary_size*o);
            }

            // update all degrees of freedom in the cell m
            for(int l=0;l<o;l++) {
                // Perform the required quadrature
                fp s = 0.0;
                for(int g=0;g<o;g++) {
                    fp xi = xi_left + (xi_right-xi_left)*dg_eval.xi[g];
                    s += w[g]*quad_val[g]*dg_eval.lagrange(xi,l);
                }
                out[m][l] += s*(xi_right-xi_left)*w_inv[l];
            }
        }
    }
}

// This solves the constant velocity problem (mostly a test case)
void burgers_dg_constvel(domain<1>::view& u, domain<1>::view& out, fp tau,
        fp L, fp* left_boundary, fp* right_boundary, int boundary_size, 
        int num_it, iterative_method im=IM_NEWTON) {
    int n=u.shape()[0]; int o=u.shape()[1];
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    dg_evaluator dg_eval(o);

    // prepare a contiguous memory for subsequent evaluations of u
    int boundary_offset = boundary_size*o;
    vector<fp> __u0(n*o+2*boundary_offset);
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            __u0[boundary_offset+i*o+j] = u[i][j];
    // include the boundary
    for(int i=0;i<boundary_size;i++)
        for(int j=0;j<o;j++) {
            __u0[j+o*i] = left_boundary[boundary_offset-1-(o*i+o-1-j)];
            __u0[boundary_offset+n*o+i*o+j] = right_boundary[j+o*i];
        }

    // set output to zero
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            out[i][j] = 0.0;

    vector<fp> quad_val(o);
    vector<fp> w_inv(o); for(int i=0;i<o;i++) w_inv[i]=1.0/gauss::w(i,o);
    vector<fp> w(o); for(int i=0;i<o;i++) w[i]=gauss::w(i,o);
    // iterate over all cells (including the boundary)
    for(int i=-boundary_size;i<n+boundary_size;i++) {
        // Predict where a function with support in cell i ends up by following
        // the characteristics forward in time
        fp x_left  = i*h;
        fp x_right = (i+1)*h;

        fp x_t1_left  = x_left  + 1.0*tau;
        fp x_t1_right = x_right + 1.0*tau;

        int m_left = floor(x_t1_left/h); int m_right = floor(x_t1_right/h);
        for(int m=max(0,m_left);m<=min(n-1,m_right);m++) {
            fp xi_left  = m==m_left  ? x_t1_left/h-m  : 0.0;
            fp xi_right = m==m_right ? x_t1_right/h-m : 1.0;

            // solve the characteristics backward in time
            for(int g=0;g<o;g++) {
                // the characteristic starts at the Gauss-Legendre quadrature
                // nodes
                fp x_t1 = h*(m + xi_left 
                          + (xi_right-xi_left)*gauss::x_scaled01(g,o));
                fp xb = x_t1 - 1.0*tau;


                int idx   = floor(xb/h);
                fp xl = xb/h - idx;
                quad_val[g] = eval_u(xl,idx,o,dg_eval,&__u0[0],boundary_size*o);
            }

            // update all degrees of freedom in the cell m
            for(int l=0;l<o;l++) {
                // Perform the required quadrature
                fp s = 0.0;
                for(int g=0;g<o;g++) {
                    fp xi = xi_left + (xi_right-xi_left)*dg_eval.xi[g];
                    s += w[g]*quad_val[g]*dg_eval.lagrange(xi,l);
                }
                out[m][l] += s*(xi_right-xi_left)*w_inv[l];
            }
        }
    }
}

void burgers_dg(domain<1>::view& u, domain<1>::view& out, fp tau, fp L,
        fp* left_boundary, fp* right_boundary, int boundary_size, int num_it,
        iterative_method im=IM_NEWTON) {
    int n=u.shape()[0]; int o=u.shape()[1];
    // since the problem is invariant under translation we assume here that the
    // domain is [0,L]
    fp h = L/fp(n);  
    // this temporary vector is used to store the DOFs in a single cell
    vector<fp> ucell(o); 
    dg_evaluator dg_eval(o);

    // get a pointer to the correct method
    fp (*burgers_characteristics)(fp x, fp tau, int n, int o, int boundary_size,
            fp L, int num_it, dg_evaluator& dg_eval, fp *__u0, fp *__u0_deriv,
            fp& xb);
    if(im == IM_FIXEDPOINT)
        burgers_characteristics = burgers_characteristics_fixedpoint;
    else if(im == IM_SECANT)
        burgers_characteristics = burgers_characteristics_secant;
    else
        burgers_characteristics = burgers_characteristics_newton;

    // prepare a contiguous memory for subsequent evaluations of u
    int boundary_offset = boundary_size*o;
    vector<fp> __u0(n*o+2*boundary_offset);
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            __u0[boundary_offset+i*o+j] = u[i][j];
    // include the boundary
    for(int i=0;i<boundary_size;i++)
        for(int j=0;j<o;j++) {
            __u0[j+o*i] = left_boundary[boundary_offset-1-(o*i+o-1-j)];
            __u0[boundary_offset+n*o+i*o+j] = right_boundary[j+o*i];
        }

    // prepare a contiguous memory for subsequent evaluations of u' (Newton's
    // method only)
    // TODO: this is still rather expensive in terms of computational effort
    vector<fp> __u0_deriv(n*o+2*boundary_offset);
    if(im == IM_NEWTON) {
        // construct the derivative of u for subsequent evaluations
        for(int i=0;i<n;i++) {
            for(int j=0;j<o;j++) ucell[j] = u[i][j];
            for(int j=0;j<o;j++)
                __u0_deriv[boundary_offset+o*i+j] = 
                    evaluate_derivative_1d(dg_eval.xi[j],o,ucell)/h;
        }
        // include the boundary
        for(int i=0;i<boundary_size;i++) {
            for(int j=0;j<o;j++)
                ucell[j] = left_boundary[o*i+j];
            for(int j=0;j<o;j++)
                __u0_deriv[j+o*i] = 
                    evaluate_derivative_1d(dg_eval.xi[j],o,ucell)/h;
            for(int j=0;j<o;j++)
                ucell[j] = right_boundary[o*i+j];
            for(int j=0;j<o;j++)
                __u0_deriv[boundary_offset+n*o+i*o+j] = 
                    evaluate_derivative_1d(dg_eval.xi[j],o,ucell)/h;
        }
    }

    // set output to zero
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            out[i][j] = 0.0;

    // iterate over all cells (including the boundary)
    vector<fp> quad_val(o);
    vector<fp> w_inv(o); for(int i=0;i<o;i++) w_inv[i]=1.0/gauss::w(i,o);
    vector<fp> w(o); for(int i=0;i<o;i++) w[i]=gauss::w(i,o);
    for(int i=-boundary_size;i<n+boundary_size;i++) {
        // Predict where a function with support in cell i ends up by following
        // the characteristics forward in time
        fp x_left  = i*h;
        fp x_right = (i+1)*h;

        // In the forward prediction step we use the average value of the two
        // adjacent cells. If this is not done the characteristics in the
        // backward step might not match which can yield oscillations in the
        // values contained in a single cell (note that this problem only occurs
        // for specific time step sizes and usually goes away once we refine the
        // mesh).
        fp x_t1_left, x_t1_right;
        if(i != -boundary_size && i != n+boundary_size-1) {
            get_ucell(i,n,o,u,left_boundary,right_boundary,ucell);
            fp lplus  = dg_eval.eval(0.0,&ucell[0]);
            fp rminus = dg_eval.eval(1.0,&ucell[0]);
            get_ucell(i+1,n,o,u,left_boundary,right_boundary,ucell);
            fp rplus = dg_eval.eval(0.0,&ucell[0]);
            get_ucell(i-1,n,o,u,left_boundary,right_boundary,ucell);
            fp lminus = dg_eval.eval(1.0,&ucell[0]);

            x_t1_left = x_left   + tau*0.5*(lplus+lminus);
            x_t1_right = x_right + tau*0.5*(rplus+rminus);
        } else {
            get_ucell(i,n,o,u,left_boundary,right_boundary,ucell);
            x_t1_left  = x_left  + tau*dg_eval.eval(0.0,&ucell[0]);
            x_t1_right = x_right + tau*dg_eval.eval(1.0,&ucell[0]);
        }

        // Iterate over the cells that need to be updated (except for boundary
        // cells)
        int m_left = floor(x_t1_left/h); int m_right = floor(x_t1_right/h);
        for(int m=max(0,m_left);m<=min(n-1,m_right);m++) {
            fp xi_left  = m==m_left  ? x_t1_left/h-m  : 0.0;
            fp xi_right = m==m_right ? x_t1_right/h-m : 1.0;

            // solve the characteristics backward in time
            for(int g=0;g<o;g++) {
                // the characteristic starts at the Gauss-Legendre quadrature
                // nodes
                fp x_t1 = h*(m + xi_left 
                          + (xi_right-xi_left)*gauss::x_scaled01(g,o)); 

                fp xb=0.0;
                quad_val[g] = burgers_characteristics(x_t1,tau,n,o,
                        boundary_size,L,num_it,dg_eval,&__u0[0],&__u0_deriv[0],
                        xb);
            }

            // update all degrees of freedom in the cell m
            for(int l=0;l<o;l++) {
                // Perform the required quadrature
                fp s = 0.0;
                for(int g=0;g<o;g++) {
                    fp xi = xi_left + (xi_right-xi_left)*dg_eval.xi[g];
                    s += w[g]*quad_val[g]*dg_eval.lagrange(xi,l);
                }
                out[m][l] += s*(xi_right-xi_left)*w_inv[l];
            }
        }
    }

}

