#pragma once

#ifdef __CUDACC__

#include <cufft.h>
#include <cusparse_v2.h>
#include <generic/common.hpp>
#include <algorithms/sldg2d/generic_sldg2d.hpp>
#include <programs/dk-abstract.hpp>


#ifndef USE_SINGLE_PRECISION
using cucmplx = cufftDoubleComplex;
#else
using cucmplx = cufftComplex;
#endif


inline const char* cufftGetErrorString(cufftResult_t status){

    if (status == CUFFT_SUCCESS){
        return "The cuFFT operation was successful.\n";
    } else if (status == CUFFT_INVALID_PLAN){
        return "cuFFT was passed an invalid plan handle.\n";
    } else if (status == CUFFT_ALLOC_FAILED){
        return "cuFFT failed to allocate GPU or CPU memory.\n";
    } else if (status == CUFFT_INVALID_TYPE){
        return "No longer used.\n";
    } else if (status == CUFFT_INVALID_VALUE) {
        return "User specified an invalid pointer or parameter.\n";
    } else if (status == CUFFT_INTERNAL_ERROR) {
        return "Driver or internal cuFFT library error.\n";
    } else if (status == CUFFT_EXEC_FAILED) {
        return "Failed to execute an FFT on the GPU.\n";
    } else if (status == CUFFT_SETUP_FAILED) {
        return "The cuFFT library failed to initialize.\n";
    } else if (status == CUFFT_INVALID_SIZE) {
        return "User specified an invalid transform size.\n";
    } else if (status == CUFFT_UNALIGNED_DATA) {
        return "No longer used.\n";
    } else if (status == CUFFT_INCOMPLETE_PARAMETER_LIST) {
        return "Missing parameters in call.\n";
    } else if (status == CUFFT_INVALID_DEVICE) {
        return "Execution of a plan was on different GPU than plan creation.\n";
    } else if (status == CUFFT_PARSE_ERROR) {
        return "Internal plan database error.\n";
    } else if (status == CUFFT_NO_WORKSPACE) {
        return "No workspace has been provided prior to plan execution.\n";
    } else if (status == CUFFT_NOT_IMPLEMENTED) {
        return "Function does not implement functionality for parameters given.\n";
    } else if (status == CUFFT_LICENSE_ERROR) {
        return "Used in previous versions.\n";
    } else if (status == CUFFT_NOT_SUPPORTED) {
        return "Operation is not supported for parameters given.\n";
    } else {
        return "NO VALID ERROR CODE\n";
    }
}


#define cufftErrchk(ans) { cufftAssert((ans), __FILE__, __LINE__); }
inline void cufftAssert(cufftResult_t code, const char *file, int line)
{
   if (code != CUFFT_SUCCESS)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cufftGetErrorString(code), file, line);
      exit(code);
   }
}


#define cusparseErrchk(ans) { cusparseAssert((ans), __FILE__, __LINE__); }
inline void cusparseAssert(cusparseStatus_t code, const char *file, int line)
{
   if (code != CUSPARSE_STATUS_SUCCESS)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n",
              cusparseGetErrorString(code), file, line);
      exit(code);
   }
}


struct fft_2d_in_3d_gpu{

    static constexpr int rank = 2;

    int Ntheta;
    int Nz;
    int batch;

    //distance between successive input elements 
    //in innermost dimension
    int istride;
    int ostride;

    //distance between first element of two consecutive 
    //signals in a batch of input/output data
    const int idist{1};
    const int odist{1};

    cufftHandle cufft_plan_forward;
    cufftHandle cufft_plan_backward;

    fft_2d_in_3d_gpu(int Nrp1, int _Ntheta, int _Nz)
        : Ntheta{_Ntheta}, Nz{_Nz}, batch{Nrp1},
          istride{Nrp1}, ostride{Nrp1}
    {

        int n[rank] = {Nz, Ntheta};

        //pointer that indicates storage dimensions of 
        //input/output data, reverse everywhere!!!
        int inembed[rank] = {Nrp1*Nz, Ntheta};
        int onembed[rank] = {Nrp1*Nz, Ntheta/2+1};

        #ifndef USE_SINGLE_PRECISION
        cufftErrchk (cufftPlanMany(&cufft_plan_forward, rank, n,
                          inembed,istride,idist,
                          onembed,ostride,odist,
                          CUFFT_D2Z,batch) );

        cufftErrchk (cufftPlanMany(&cufft_plan_backward, rank, n,
                          onembed,ostride,odist,
                          inembed,istride,idist,
                          CUFFT_Z2D,batch) );
        #else
        cufftErrchk (cufftPlanMany(&cufft_plan_forward, rank, n,
                          inembed,istride,idist,
                          onembed,ostride,odist,
                          CUFFT_R2C,batch) );

        cufftErrchk (cufftPlanMany(&cufft_plan_backward, rank, n,
                          onembed,ostride,odist,
                          inembed,istride,idist,
                          CUFFT_C2R,batch) );
        #endif

    }

    void forward(fp* d_in, cucmplx* d_out){

        #ifndef USE_SINGLE_PRECISION
        cufftErrchk(cufftExecD2Z(cufft_plan_forward,
                                 d_in, d_out) );
        #else
        cufftErrchk(cufftExecR2C(cufft_plan_forward,
                                 d_in, d_out) );
        #endif

    }

    void backward(cucmplx* d_in, fp* d_out){

        #ifndef USE_SINGLE_PRECISION
        cufftErrchk(cufftExecZ2D(cufft_plan_backward,
                                 d_in, d_out) );
        #else
        cufftErrchk(cufftExecC2R(cufft_plan_backward,
                                 d_in, d_out) );
        #endif

    }

    ~fft_2d_in_3d_gpu(){
        cufftErrchk( cufftDestroy(cufft_plan_forward) );
        cufftErrchk( cufftDestroy(cufft_plan_backward) );
    }

};


template<typename T>
__global__
void normalize_and_set_bc_dirichlet(fp normalize, fp* data, T Nrp1){

    int i = threadIdx.x;
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    data[idx] *= normalize;
    if(i==0 || i==Nrp1-1)
        data[idx] = 0.0;
}


//uses the gtsv2StridedBatch routine from cuSparse
//TODO: what is the difference in performance between the gtsvInterleavedBatch??
struct ode_fd_cc_gpu{

    int Nrp1, Ntheta, Nz;
    fp ar, hr;

    fft_2d_in_3d_gpu fft;
    generic_container<cucmplx> rhohat;

    cusparseHandle_t cusparse_handle;
    generic_container<cucmplx> dl;
    generic_container<cucmplx> dm;
    generic_container<cucmplx> du;
    char* d_buffer;

    fp normalize;

    ode_fd_cc_gpu(int _Nrp1, int _Ntheta, int _Nz,
                  fp _ar, fp _hr, fp (*n0r)(fp), fp (*alpha)(fp,int,int))
        : Nrp1{_Nrp1}, Ntheta{_Ntheta}, Nz{_Nz}, 
          ar{_ar}, hr{_hr}, fft{Nrp1,Ntheta,Nz}
    {

        rhohat.resize(Nrp1*(Ntheta/2+1)*Nz);

        cusparseErrchk( cusparseCreate(&cusparse_handle) );
        size_t bufferSizeInBytes;

        #ifdef USE_SINGLE_PRECISION
        cout << "single precision used in qne" << endl;
        cusparseErrchk(cusparseCgtsv2StridedBatch_bufferSizeExt(
                        cusparse_handle, Nrp1-2, dl.data(true), dm.data(true), 
                        du.data(true), rhohat.data(true)+1,
                        (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #else
        cout << "double precision used in qne" << endl;
        cusparseErrchk(cusparseZgtsv2StridedBatch_bufferSizeExt(
                       cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                       du.data(true), rhohat.data(true)+1,
                       (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #endif
        
        cout << "buffer for lin solve: " << bufferSizeInBytes*1e-9 << " GB" << endl;
        gpuErrchk( cudaMalloc(&d_buffer, bufferSizeInBytes));

        dl.resize(Nrp1*(Ntheta/2+1)*Nz);
        dm.resize(Nrp1*(Ntheta/2+1)*Nz);
        du.resize(Nrp1*(Ntheta/2+1)*Nz);

        init(n0r,alpha);

        normalize = 1.0/(Nz*Ntheta);
    }

    ~ode_fd_cc_gpu(){
        gpuErrchk(cudaFree(d_buffer));
        cusparseErrchk(cusparseDestroy(cusparse_handle));
    }

    void init(fp (*n0r)(fp), fp (*alpha)(fp,int,int)){

        fp inv_hsq = 1.0/(hr*hr);

        for(int k=0;k<Nz;++k){
            for(int j=0;j<Ntheta/2+1;++j){
                for(int i=0;i<Nrp1-2;++i){
                    
                    fp r    = ar + hr*(i+1.0);
                    fp rp12 = ar + hr*(i+1.5);
                    fp rm12 = ar + hr*(i+0.5);

                    fp mid = (n0r(rp12) + n0r(rm12))*inv_hsq + alpha(r,j,k);

                    dm.h_data[i+Nrp1*(j+(Ntheta/2+1)*k)] = {mid,0.0};


                    fp off = -1.0*n0r(rp12)*inv_hsq;
                    if(i<Nrp1-3){
                        du.h_data[i  +Nrp1*(j+(Ntheta/2+1)*k)] = {off,0};
                        dl.h_data[i+1+Nrp1*(j+(Ntheta/2+1)*k)] = {off,0};
                    }

                }
                dl.h_data[0     +Nrp1*(j+(Ntheta/2+1)*k)] = {0,0};
                du.h_data[Nrp1-3+Nrp1*(j+(Ntheta/2+1)*k)] = {0,0};
            }
        }


    }


    void solve_qns(fp* rho, fp* phi){

        gt::start("qns_copy");
        dm.copy_to_gpu();
        du.copy_to_gpu();
        dl.copy_to_gpu();
        cudaDeviceSynchronize();
        gt::stop("qns_copy");

        gt::start("qns_fft");
        fft.forward(rho,rhohat.d_data);
        cudaDeviceSynchronize();
        gt::stop("qns_fft");

        gt::start("qns_solve");
        #ifdef USE_SINGLE_PRECISION
        cusparseErrchk(cusparseCgtsv2StridedBatch(
                    cusparse_handle, Nrp1-2, dl.d_data, dm.d_data,
                    du.d_data, rhohat.d_data+1, (Ntheta/2+1)*Nz,
                    Nrp1, d_buffer));
        #else
        cusparseErrchk(cusparseZgtsv2StridedBatch(
                    cusparse_handle, Nrp1-2, dl.d_data, dm.d_data,
                    du.d_data, rhohat.d_data+1, (Ntheta/2+1)*Nz,
                    Nrp1, d_buffer));
        #endif
        cudaDeviceSynchronize();
        gt::stop("qns_solve");

        gt::start("qns_fft");
        fft.backward(rhohat.d_data,phi);
        cudaDeviceSynchronize();
        gt::stop("qns_fft");

        gt::start("qns_normalize");
        normalize_and_set_bc_dirichlet<<<Nz*Ntheta,Nrp1>>>(normalize, phi, Nrp1);
        cudaDeviceSynchronize();
        gt::stop("qns_normalize");

    }

};


template<typename T>
__global__
void normalize_and_set_bc_neumann(fp normalize, fp* data, T Nrp1){

    int i = threadIdx.x;
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    data[idx] *= normalize;
    if(i==Nrp1-1)
        data[idx] = 0.0;
}


//homogeneous Neumann at rmin instead of homogeneous Dirichlet
struct ode_fd_cc_gpu_neumann{

    int Nrp1, Ntheta, Nz;
    fp ar, hr;

    fft_2d_in_3d_gpu fft;
    generic_container<cucmplx> rhohat;

    cusparseHandle_t cusparse_handle;
    generic_container<cucmplx> dl;
    generic_container<cucmplx> dm;
    generic_container<cucmplx> du;
    char* d_buffer;

    fp normalize;

    ode_fd_cc_gpu_neumann(int _Nrp1, int _Ntheta, int _Nz,
                          fp _ar, fp _hr, fp (*n0r)(fp), fp (*alpha)(fp,int,int))
        : Nrp1{_Nrp1}, Ntheta{_Ntheta}, Nz{_Nz},
          ar{_ar}, hr{_hr}, fft{Nrp1,Ntheta,Nz}
    {

        rhohat.resize(Nrp1*(Ntheta/2+1)*Nz);

        cusparseErrchk( cusparseCreate(&cusparse_handle) );
        size_t bufferSizeInBytes;

        #ifdef USE_SINGLE_PRECISION
        cout << "single precision used in qne" << endl;
        cusparseErrchk(cusparseCgtsv2StridedBatch_bufferSizeExt(
                        cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                        du.data(true), rhohat.data(true)+1,
                        (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #else
        cout << "double precision used in qne" << endl;
        cusparseErrchk(cusparseZgtsv2StridedBatch_bufferSizeExt(
                       cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                       du.data(true), rhohat.data(true)+1,
                       (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #endif

        cout << "buffer for lin solve: " << bufferSizeInBytes*1e-9 << " GB" << endl;
        gpuErrchk( cudaMalloc(&d_buffer, bufferSizeInBytes));

        dl.resize(Nrp1*(Ntheta/2+1)*Nz);
        dm.resize(Nrp1*(Ntheta/2+1)*Nz);
        du.resize(Nrp1*(Ntheta/2+1)*Nz);

        init(n0r,alpha);

        normalize = 1.0/(Nz*Ntheta);
    }


    ~ode_fd_cc_gpu_neumann(){
        gpuErrchk(cudaFree(d_buffer));
        cusparseErrchk(cusparseDestroy(cusparse_handle));
    }


    void init(fp (*n0r)(fp), fp (*alpha)(fp,int,int)){

        fp inv_hsq = 1.0/(hr*hr);

        for(int k=0;k<Nz;++k){
            for(int j=0;j<Ntheta/2+1;++j){
                for(int i=0;i<Nrp1-1;++i){

                    fp r    = ar + hr*i;
                    fp rp12 = ar + hr*(i+0.5);
                    fp rm12 = ar + hr*(i-0.5);

                    fp mid = (n0r(rp12) + n0r(rm12))*inv_hsq + alpha(r,j,k);

                    dm.h_data[i+Nrp1*(j+(Ntheta/2+1)*k)] = {mid,0.0};

                    fp off = -1.0*n0r(rp12)*inv_hsq;
                    if(i<Nrp1-2){
                        du.h_data[i  +Nrp1*(j+(Ntheta/2+1)*k)] = {off,0};
                        dl.h_data[i+1+Nrp1*(j+(Ntheta/2+1)*k)] = {off,0};
                        if(i==0){
                            fp off0 = -1.0*n0r(rm12)*inv_hsq;
                            du.h_data[i  +Nrp1*(j+(Ntheta/2+1)*k)] = {off+off0,0};
                        }
                    }

                }
                dl.h_data[0     +Nrp1*(j+(Ntheta/2+1)*k)] = {0,0};
                du.h_data[Nrp1-2+Nrp1*(j+(Ntheta/2+1)*k)] = {0,0};
            }
        }


    }


    void solve_qns(fp* rho, fp* phi){

        gt::start("qns_copy");
        dm.copy_to_gpu();
        du.copy_to_gpu();
        dl.copy_to_gpu();
        cudaDeviceSynchronize();
        gt::stop("qns_copy");

        gt::start("qns_fft");
        fft.forward(rho,rhohat.d_data);
        cudaDeviceSynchronize();
        gt::stop("qns_fft");

        gt::start("qns_solve");
        #ifdef USE_SINGLE_PRECISION
        cusparseErrchk(cusparseCgtsv2StridedBatch(
                    cusparse_handle, Nrp1-1, dl.d_data, dm.d_data,
                    du.d_data, rhohat.d_data, (Ntheta/2+1)*Nz,
                    Nrp1, d_buffer));
        #else
        cusparseErrchk(cusparseZgtsv2StridedBatch(
                    cusparse_handle, Nrp1-1, dl.d_data, dm.d_data,
                    du.d_data, rhohat.d_data, (Ntheta/2+1)*Nz,
                    Nrp1, d_buffer));
        #endif
        cudaDeviceSynchronize();
        gt::stop("qns_solve");

        gt::start("qns_fft");
        fft.backward(rhohat.d_data,phi);
        cudaDeviceSynchronize();
        gt::stop("qns_fft");

        gt::start("qns_normalize");
        normalize_and_set_bc_neumann<<<Nz*Ntheta,Nrp1>>>(normalize, phi, Nrp1);
        cudaDeviceSynchronize();
        gt::stop("qns_normalize");

    }

};


template<size_t oz>
__device__
fp evaluate(fp x, fp* values, fp* nodes){

    fp result=0.0;

    for(size_t i=0;i<oz;++i){

        fp lagrange=1.0;
        for(size_t j=0;j<i;++j)
            lagrange *= (x-nodes[j])/(nodes[i]-nodes[j]);
        for(size_t j=i+1;j<oz;++j)
            lagrange *= (x-nodes[j])/(nodes[i]-nodes[j]);

        result += values[i]*lagrange;
    }

    return result;
}


template<size_t o2d>
__device__
fp basisf(fp x, fp y, int i, int j, fp* gauss_o2d){

    if(o2d==1)
        return 1.0;

    if(o2d==2){
        return (x-gauss_o2d[1-i])*(y-gauss_o2d[1-j])*
               (3.0*(2.0*i-1)*(2.0*j-1));

    }
}


template<size_t o2d, size_t oz>
__global__
void interpolate_z_dg_to_equi_k(fp* in, fp* out, fp* gauss_oz, 
                                int Nr, int Ntheta, int Nz){

    int stride = Nr*Ntheta*o2d*o2d;
    
    int offset = blockIdx.x;
    int i = threadIdx.x;

    if(i>=Nz || offset>=stride)
        return;

    fp cell[oz];
    
    for(size_t j=0;j<oz;++j){
        int idx = j+oz*i;
        cell[j] = in[offset + stride*idx];
    }
    
    for(size_t j=0;j<oz;++j){
    
        fp x = (1.0+2.0*j)/(2.0*fp(oz));
        fp value = evaluate<oz>(x,cell,gauss_oz);
    
        int idx = j+oz*i;
        out[offset+stride*idx] = value;
    }
}


#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull = (unsigned long long int*)address;

    unsigned long long int old = *address_as_ull, assumed;

    do{ assumed = old;
		old = atomicCAS(address_as_ull, assumed,__double_as_longlong(val +__longlong_as_double(assumed)));
    } while (assumed != old);

    return __longlong_as_double(old);
}
#endif


template<size_t o2d>
__global__
void evaluate_at_grid_corners_k(fp* rho_leg, fp* rho_cc, int Nr, 
                                int Ntheta, fp* gauss_o2d){

    int l = blockIdx.y;
    int j = blockIdx.x;
    int i = threadIdx.x;

    int offset_in = Nr*Ntheta*o2d*o2d*l;
    fp* in = rho_leg + offset_in;
    int offset_out = (Nr+1)*Ntheta*l;

    for(int jj=0;jj<o2d;++jj){
        for(int ii=0;ii<o2d;++ii){

            fp v_in = in[ii+o2d*jj + o2d*o2d*(i+j*Nr)];

            fp v_bl = v_in*basisf<o2d>(0.0,0.0,ii,jj,gauss_o2d);
            fp v_br = v_in*basisf<o2d>(1.0,0.0,ii,jj,gauss_o2d);
            fp v_tl = v_in*basisf<o2d>(0.0,1.0,ii,jj,gauss_o2d);
            fp v_tr = v_in*basisf<o2d>(1.0,1.0,ii,jj,gauss_o2d);

            fp factor = (i==0) ? 0.5 : 0.25;
            int idx_out = i + (Nr+1)*j + offset_out;
            atomicAdd(&rho_cc[idx_out],v_bl*factor);

            factor = (i+1==Nr) ? 0.5 : 0.25;
            idx_out = i+1 + (Nr+1)*j + offset_out;
            atomicAdd(&rho_cc[idx_out],v_br*factor);

            int idx_j = (j==Ntheta-1) ? 0 : j+1;
            factor = (i==0) ? 0.5 : 0.25;
            idx_out = i + (Nr+1)*idx_j + offset_out;
            atomicAdd(&rho_cc[idx_out],v_tl*factor);

            factor = (i+1==Nr) ? 0.5 : 0.25;
            idx_out = i+1 + (Nr+1)*idx_j + offset_out;
            atomicAdd(&rho_cc[idx_out],v_tr*factor);

        }
    }
}


template<size_t oz>
__global__
void compute_derivatives_k(fp* in, fp* dr_out, fp* dtheta_out, fp* dz_out, 
                           int Nr, int Ntheta, int Nz, fp* inv_2h){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int idx = i+(Nr+1)*(j+Ntheta*k);

    int offset_r = 1;
    if(i==0){
        dr_out[idx] = 0.0;
    }
    else if(i==Nr){
        dr_out[idx] = (3.0*in[idx]-4.0*in[idx-1]+in[idx-2])*
                       inv_2h[0];
    }
    else{
        dr_out[idx] = (in[idx+offset_r]-in[idx-offset_r])*inv_2h[0];
    }

    int offset_t = Nr+1;
    int idxp1_t = idx+offset_t;
    int idxm1_t = idx-offset_t;
    if(j==0)        idxm1_t = idx+(Ntheta-1)*offset_t;
    if(j==Ntheta-1) idxp1_t = idx-(Ntheta-1)*offset_t;

    dtheta_out[idx] = (in[idxp1_t]-in[idxm1_t])*inv_2h[1];
    //to ensure mass conservation, this is almost zero anyhow
    if(i==0)
        dtheta_out[idx] = 0.0;

    int offset_z = Ntheta*(Nr+1);
    int idxp1_z = idx+offset_z;
    int idxm1_z = idx-offset_z;
    if(k==0)       idxm1_z = idx+(Nz*oz-1)*offset_z;
    if(k==Nz*oz-1) idxp1_z = idx-(Nz*oz-1)*offset_z;

    dz_out[idx] = -(in[idxp1_z]-in[idxm1_z])*inv_2h[2];
}


template<size_t oz>
__global__
void interpolate_z_equi_to_dg_k(fp* in, fp* out, int Nr, int Ntheta, 
                                fp* gauss_oz){

    fp nodes[oz];
    for(size_t i=0;i<oz;++i)
        nodes[i] = (1.0+2.0*i)/(2.0*fp(oz));

    int stride = (Nr+1)*Ntheta;

    int i = threadIdx.x;
    int offset = blockIdx.x;

    fp cell[oz];

    for(size_t j=0;j<oz;++j){
        int idx = j+oz*i;
        cell[j] = in[offset + stride*idx];
    }

    for(size_t j=0;j<oz;++j){

        fp x = gauss_oz[j];
        fp value = evaluate<oz>(x,cell,nodes);

        int idx = j+oz*i;
        out[offset+stride*idx] = value;
    }
}


template<size_t o2d>
__global__
void interpolate_cc_to_dg_k(fp* in, fp* out, int Nr, int Ntheta, fp* gauss_o2d){

    //for(int k=0;k<Nz*oz;++k){
    //for(int j=0;j<Ntheta;++j){
    //    for(int i=0;i<Nr;++i){
    int k = blockIdx.y;
    int j = blockIdx.x;
    int i = threadIdx.x;

    int offset_out = k*o2d*o2d*Nr*Ntheta;
    int offset_in  = k*(Nr+1)*Ntheta;

    fp cc[4];
    for(int jj=0;jj<2;++jj){
        int idx_j = (j+jj==Ntheta) ? 0 : j+jj;
        for(int ii=0;ii<2;++ii){
            cc[ii+2*jj] = in[i+ii + idx_j*(Nr+1) +
                             offset_in];
        }
    }

    for(int jj=0;jj<o2d;++jj){
        for(int ii=0;ii<o2d;++ii){

            fp x = gauss_o2d[ii];
            fp y = gauss_o2d[jj];

            fp v = cc[0]*(1.0-x)*(1.0-y) +
                   cc[1]*(x-0.0)*(1.0-y) +
                   cc[2]*(1.0-x)*(y-0.0) +
                   cc[3]*(x-0.0)*(y-0.0);

            out[ii+o2d*jj + o2d*o2d*(i+j*Nr) + offset_out] = v;

        }
    }
}



template<size_t o2d, size_t oz>
struct quasi_neutrality_gpu : public quasi_neutrality{

    generic_container<fp> gauss_o2d;
    generic_container<fp> gauss_oz;
    generic_container<fp> inv_2h;

    unique_ptr<ode_fd_cc_gpu_neumann> ode_fd;

    quasi_neutrality_gpu(array<fp,3> _a, array<fp,3> _b,
                         int _Nr, int _Ntheta, int _Nz, bool gpu, 
                         fp (*n0r)(fp), fp (*alpha)(fp,int,int))
        : quasi_neutrality{o2d,oz,_a,_b,_Nr,_Ntheta,_Nz,gpu}
    {

        gauss_o2d.resize(o2d);
        gauss_o2d.h_data = gauss::all_x_scaled01(o2d);
        gauss_o2d.copy_to_gpu();
        gauss_oz.resize(oz);
        gauss_oz.h_data = gauss::all_x_scaled01(oz);
        gauss_oz.copy_to_gpu();

        inv_2h.resize(3);
        inv_2h.h_data[0] = 1.0/(2.0*h[0]);
        inv_2h.h_data[1] = 1.0/(2.0*h[1]);
        inv_2h.h_data[2] = 1.0/(2.0*h[2]);
        inv_2h.copy_to_gpu();

        ode_fd = make_unique<ode_fd_cc_gpu_neumann>(Nr+1, Ntheta, Nz*oz, 
                                                    a[0], h[0], n0r, alpha);

        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }


    void solve(){

        gt::start("qns_z_dg2equi");
        interpolate_z_dg_to_equi(rho.d_data, rho.d_data);
        cudaDeviceSynchronize();
        gt::stop("qns_z_dg2equi");

        gt::start("qns_dg2cc");
        evaluate_at_grid_corners(rho.d_data, rho_cc.d_data);
        cudaDeviceSynchronize();
        gt::stop("qns_dg2cc");

        ode_fd->solve_qns(rho_cc.d_data, phi_cc.d_data);

        gt::start("qns_compute_ee");
        compute_electric_energy();
        gt::stop("qns_compute_ee");

        gt::start("qns_derivative");
        compute_all_derivatives( ); 
        gt::stop("qns_derivative");

        gt::start("qns_z_equi2dt");
        interpolate_z_equi_to_dg(phi_cc.d_data, phi_cc.d_data);
        interpolate_z_equi_to_dg(dr_phi_cc.d_data, dr_phi_cc.d_data);
        interpolate_z_equi_to_dg(dtheta_phi_cc.d_data, dtheta_phi_cc.d_data);
        interpolate_z_equi_to_dg(dz_phi_cc.d_data, dz_phi_cc.d_data);
        gt::stop("qns_z_equi2dt");

        gt::start("qns_z_cc2dg");
        interpolate_cc_to_dg(phi_cc.d_data, phi.d_data);
        interpolate_cc_to_dg(dr_phi_cc.d_data, dr_phi.d_data);
        interpolate_cc_to_dg(dtheta_phi_cc.d_data, dtheta_phi.d_data);
        interpolate_cc_to_dg(dz_phi_cc.d_data, dz_phi.data());
        gt::stop("qns_z_cc2dg");

        //max_E();
    }


    void max_E(){

        dr_phi_cc.copy_to_host();
        dtheta_phi_cc.copy_to_host();
        dz_phi_cc.copy_to_host();

        fp max_vr=0.0;
        fp max_vt=0.0;
        fp max_vz=0.0;
        for(int k=0;k<Nz*oz;++k){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){
                    fp r = a[0] + i*h[0];
                    int lidx = i+(Nr+1)*(j+Ntheta*k);
                    max_vr = std::max(max_vr,abs(dr_phi_cc.h_data[lidx]/r));
                    max_vt = std::max(max_vt,abs(dtheta_phi_cc.h_data[lidx]/r));
                    max_vz = std::max(max_vz,abs(dz_phi_cc.h_data[lidx]));
                }
            }
        }

        cout << "max_vr: " << max_vr << ", max_vt: " << max_vt
             << ", max_vz: " << max_vz << endl;

    }

    void interpolate_z_dg_to_equi(fp* in, fp* out){

        int blocks = Nr*Ntheta*o2d*o2d;

        interpolate_z_dg_to_equi_k<o2d,oz><<<blocks,Nz>>>(in, out, 
                                         gauss_oz.d_data, Nr, Ntheta, Nz);
    }


    void evaluate_at_grid_corners(fp* rho_leg, fp* rho_cc){

        gpuErrchk(cudaMemset(rho_cc, fp(0.0), sizeof(fp)*(Nr+1)*Ntheta*Nz*oz));

        dim3 blocks(Ntheta, Nz*oz);
        evaluate_at_grid_corners_k<o2d><<<blocks,Nr>>>(rho_leg, rho_cc, Nr, 
                                                       Ntheta, gauss_o2d.d_data);
    }


    void compute_all_derivatives(){

        dim3 blocks(Ntheta, Nz*oz);
        compute_derivatives_k<oz><<<blocks,Nr+1>>>(phi_cc.d_data,
                                                   dr_phi_cc.d_data,
                                                   dtheta_phi_cc.d_data,
                                                   dz_phi_cc.d_data,
                                                   Nr, Ntheta, Nz, inv_2h.d_data); 
    } 

    
    void interpolate_z_equi_to_dg(fp* in, fp* out){

        int blocks = (Nr+1)*Ntheta;
        interpolate_z_equi_to_dg_k<oz><<<blocks,Nz>>>(in, out, Nr, Ntheta, 
                                                      gauss_oz.d_data);
    }


    void interpolate_cc_to_dg(fp* in, fp* out){

        dim3 blocks(Ntheta,Nz*oz);
        interpolate_cc_to_dg_k<o2d><<<blocks,Nr>>>(in, out, Nr, Ntheta, 
                                                   gauss_o2d.d_data);
    }


};


#endif
