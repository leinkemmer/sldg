#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "geometry.hpp"


template<size_t o,typename initf>
void init_legendre(initf f0,int Nx, int Ny, double ax, double ay,
                   double hx, double hy, double* out){


    constexpr size_t glp = 2*o;
    constexpr size_t Nloc = o*(o+1)/2;

    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            double coeff[Nloc]={0.0};

            for(size_t jj=0;jj<glp;++jj){
                for(size_t ii=0;ii<glp;++ii){

                    double x = gauss::x_scaled01(ii,glp);
                    double y = gauss::x_scaled01(jj,glp);

                    if(o>=1)
                        coeff[0] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp);
                    if(o>=2){
                        coeff[1] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp)*(x-0.5);
                        coeff[2] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp)*(y-0.5);
                    }
                    if(o>=3){
                        coeff[3] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp)*
                                       ((x-0.5)*(x-0.5)-1./12.);
                        coeff[4] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp)*
                                       ((x-0.5)*(y-0.5));
                        coeff[5] += f0((x+i)*hx+ax,(y+j)*hy+ay)*
                                       gauss::w(ii,glp)*gauss::w(jj,glp)*
                                       ((y-0.5)*(y-0.5)-1./12.);
                    }

                }
            }

            //multiply by proper weights:
            //hx*hy/4 by gl quadrature and (weight[ii])/(hx*hy) by legendre poly
            double weight[6] = {1.0,12.0,12.0,180.0,144.0,180.0};
            for(size_t ii=0;ii<Nloc;++ii)
                out[ii+ Nloc*(i+j*Nx)] = coeff[ii]*0.25*weight[ii];

        }
    }
}


template<size_t o>
double compute_mass_and_write_legendre(double ax, double ay, 
                                       double hx, double hy,
                                       int Nx, int Ny, double* out,
                                       std::ofstream& output, int interior_points){

    constexpr size_t Nloc = o*(o+1)/2;

    int ip = interior_points;

    double mass=0.0;
    double max = 0.0;
    for(int j=0;j<Ny;++j){
        for(int jj=0;jj<ip;++jj){
            for(int i=0;i<Nx;++i){
                for(int ii=0;ii<ip;++ii){

                    double x = ax + hx*(i + 1.0/(2.0*double(ip))+ii*1.0/double(ip));
                    double y = ay + hy*(j + 1.0/(2.0*double(ip))+jj*1.0/double(ip));

                    double xx = (x-hx*(i+0.5)-ax)/hx;
                    double yy = (y-hy*(j+0.5)-ay)/hy;

                    double v = out[0 + Nloc*(i+j*Nx)];
                    if(o>=2){
                        v += out[1 + Nloc*(i+j*Nx)]*xx +
                             out[2 + Nloc*(i+j*Nx)]*yy;
                    }
                    if(o>=3){
                        v += out[3 + Nloc*(i+j*Nx)]*(xx*xx-1./12.) +
                             out[4 + Nloc*(i+j*Nx)]*xx*yy +
                             out[5 + Nloc*(i+j*Nx)]*(yy*yy-1./12.);
                    }

                    output << std::setprecision(16)
                           << x << " " << y << " " << v << endl;

                    if(jj==0 && ii==0)
                        mass += out[0 + Nloc*(i+j*Nx)]*hx*hy;

                    if(fabs(v)>max)
                        max = fabs(v);

                }
            }
        }
    }
    //std::cout << std::setprecision(16);
    //std::cout << "max: " << max << endl;
    //std::cout << std::setprecision(8);

    return mass;

}


template<size_t o,typename referencef>
std::pair<double,double> compute_errors_legendre(
                       referencef ref,int Nx, int Ny,
                       double ax, double bx, double ay, double by,
                       double* out){

    constexpr size_t glp = 2*(o+1);
    constexpr size_t Nloc = o*(o+1)/2;

    double hx = (bx-ax)/double(Nx);
    double hy = (by-ay)/double(Ny);

    double l2 = 0.0;
    double maxerr = 0.0;
    double max=0;
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){
            for(size_t jj=0;jj<glp;++jj){
                for(size_t ii=0;ii<glp;++ii){

                    double x = ax + hx*(i + gauss::x_scaled01(ii,glp));
                    double y = ay + hy*(j + gauss::x_scaled01(jj,glp));

                    double xx = (x-hx*(i+0.5)-ax)/hx;
                    double yy = (y-hy*(j+0.5)-ay)/hy;

                    double v = out[0 + Nloc*(i+j*Nx)];
                    if(o>=2){
                        v += out[1 + Nloc*(i+j*Nx)]*xx +
                             out[2 + Nloc*(i+j*Nx)]*yy;
                    }
                    if(o>=3){
                        v += out[3 + Nloc*(i+j*Nx)]*(xx*xx-1./12.) +
                             out[4 + Nloc*(i+j*Nx)]*xx*yy +
                             out[5 + Nloc*(i+j*Nx)]*(yy*yy-1./12.);
                    }

                    double diff = ref(x,y)-v;

                    l2 += diff*diff*gauss::w(ii,glp)*gauss::w(jj,glp);
                    maxerr = std::max(maxerr,fabs(diff));

                    if(max<fabs(v)) max=fabs(v);

                }
            }
        }
    }

    //cout << "max: " << max << endl;
    //why do they divied by the domainsize??
    l2 *= hx*hy*0.25/(bx-ax)/(by-ay);
    return {sqrt(l2),maxerr};

}


template<size_t o>
struct leg_adjoint_solution{

    static constexpr size_t Nloc = o*(o+1)/2;

    double aux[Nloc][Nloc] = {{0}};

}; 


template<size_t o>
struct leg_precomp_integrals{

    static constexpr size_t Nloc = o*(o+1)/2;

    bool is_used=false;
    double data[Nloc][Nloc] = {{0}};

}; 


template<size_t o>
struct leg_all_integrals{

    static constexpr size_t Nloc = o*(o+1)/2;

    int min_i;
    int min_j;

    leg_precomp_integrals<o> pi[MAX_CELLS][MAX_CELLS];

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    void set_to_zero(){
        min_i = 65536;
        min_j = 65536;
        for(size_t r=0;r<MAX_CELLS;++r){
            for(size_t s=0;s<MAX_CELLS;++s){
                pi[r][s].is_used=false;
                for(size_t i=0;i<Nloc;++i){
                    for(size_t j=0;j<Nloc;++j){
                        pi[r][s].data[i][j] = 0.0;
                    }
                }
            }
        }
    }

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    leg_precomp_integrals<o>& operator()(int cell_i, int cell_j){
        return pi[cell_j-min_j][cell_i-min_i];
    }

}; 


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_segment_integral_f(double x1, double y1, double x2, double y2,
                        int cell_i, int cell_j, double x_star, double y_star,
                        double* in, double* aux, double anticlock,
                        double ax, double ay, double hx, double hy,
                        double* gauss_x2, double* gauss_x3, double* gauss_w3){ 

    double clx = ax + hx*(0.5 + cell_i);
    double cly = ay + hy*(0.5 + cell_j);

    x1 = (x1 - clx)/hx;
    y1 = (y1 - cly)/hy;
    x2 = (x2 - clx)/hx;
    y2 = (y2 - cly)/hy;

    double x_shift = (clx-x_star)/hx;
    double y_shift = (cly-y_star)/hy;

    if( fabs(x2-x1) < MACHINE_PREC*10 && fabs(y2-y1) < MACHINE_PREC*10 ){
        //do nothing
    } else {

        double x2m1 = x2-x1;
        double y2m1 = y2-y1;

        double green00 = (x2+x1)*0.5*y2m1;

        double green10, green01, green20, green11, green02; 
        if(o>1){
            double x_g1 = x1 + gauss_x2[0]*x2m1;
            double x_g2 = x1 + gauss_x2[1]*x2m1;
            double y_g1 = y1 + gauss_x2[0]*y2m1;
            double y_g2 = y1 + gauss_x2[1]*y2m1;

            green10 = (x_g1*x_g1 + x_g2*x_g2)*0.25*y2m1;
            green01 =-(y_g1*y_g1 + y_g2*y_g2)*0.25*x2m1;
            green20 = (x_g1*x_g1*x_g1 + x_g2*x_g2*x_g2)/6.0*y2m1;
            green11 = (x_g1*x_g1*y_g1 + x_g2*x_g2*y_g2)*0.25*y2m1;
            green02 =-(y_g1*y_g1*y_g1 + y_g2*y_g2*y_g2)/6.0*x2m1;
        }
        double green30, green21, green12, green03; 
        double green40, green31, green22, green13, green04;
        if(o>2){
            double x_g1 = x1 + gauss_x3[0]*x2m1;
            double x_g2 = x1 + gauss_x3[1]*x2m1;
            double x_g3 = x1 + gauss_x3[2]*x2m1;
            double y_g1 = y1 + gauss_x3[0]*y2m1;
            double y_g2 = y1 + gauss_x3[1]*y2m1;
            double y_g3 = y1 + gauss_x3[2]*y2m1;

            green30 = (x_g1*x_g1*x_g1*x_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*gauss_w3[2])*0.25*0.5*y2m1;
            green21 = (x_g1*x_g1*x_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*y_g3*gauss_w3[2])/3.0*0.5*y2m1;
            green12 =-(x_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*y_g3*y_g3*y_g3*gauss_w3[2])/3.0*0.5*x2m1;
            green03 =-(y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.25*0.5*x2m1;

            green40 = (x_g1*x_g1*x_g1*x_g1*x_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*x_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*x_g3*gauss_w3[2])*0.2*0.5*y2m1;
            green31 = (x_g1*x_g1*x_g1*x_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*y_g3*gauss_w3[2])*0.25*0.5*y2m1;
            green22 = (x_g1*x_g1*x_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*y_g3*y_g3*gauss_w3[2])/3.0*0.5*y2m1;
            green13 =-(x_g1*y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.25*0.5*x2m1;
            green04 =-(y_g1*y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       y_g2*y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       y_g3*y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.2*0.5*x2m1;
        }
        if(o==1){
            aux[0] += anticlock*green00*in[0];
        }
        if(o==2){

            double C1 = in[0]*green00 + in[1]*green10 + in[2]*green01;
            double C2 = x_shift*C1 + 
                        in[0]*green10 + in[1]*green20 + in[2]*green11;
            double C3 = y_shift*C1 + 
                        in[0]*green01 + in[1]*green11 + in[2]*green02;

            aux[0] += anticlock*C1;
            aux[1] += anticlock*C2;
            aux[2] += anticlock*C3;
        }
        if(o==3){

            double c1_00 = in[0] - (in[3]+in[5])/12.0;

            double tmp1 = c1_00*green00 + in[1]*green10 + in[2]*green01 +
                          in[3]*green20 + in[4]*green11 + in[5]*green02;
            double tmp2 = c1_00*green10 + in[1]*green20 + in[2]*green11 +
                          in[3]*green30 + in[4]*green21 + in[5]*green12;
            double tmp3 = c1_00*green01 + in[1]*green11 + in[2]*green02 +
                          in[3]*green21 + in[4]*green12 + in[5]*green03;
            double tmp4 = c1_00*green20 + in[1]*green30 + in[2]*green21 +
                          in[3]*green40 + in[4]*green31 + in[5]*green22;
            double tmp5 = c1_00*green11 + in[1]*green21 + in[2]*green12 +
                          in[3]*green31 + in[4]*green22 + in[5]*green13;
            double tmp6 = c1_00*green02 + in[1]*green12 + in[2]*green03 +
                          in[3]*green22 + in[4]*green13 + in[5]*green04;

            double C1 = tmp1;
            double C2 = tmp2 + x_shift*tmp1;
            double C3 = tmp3 + y_shift*tmp1;
            double C4 = tmp4 + 2.0*x_shift*tmp2+(x_shift*x_shift-1.0/12.0)*tmp1;
            double C5 = tmp5 + x_shift*tmp3+y_shift*tmp2+x_shift*y_shift*tmp1;
            double C6 = tmp6 + 2.0*y_shift*tmp3+(y_shift*y_shift-1.0/12.0)*tmp1;

            aux[0] += anticlock*C1;
            aux[1] += anticlock*C2;
            aux[2] += anticlock*C3;
            aux[3] += anticlock*C4;
            aux[4] += anticlock*C5;
            aux[5] += anticlock*C6;

        }
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_compute_integrals_outer_face_f(face<o>& face, double x_star, 
                                    double y_star, int anticlock,
                                    double* out, double* dg_sol,
                                    bool print, int Nx, int Ny, double ax,
                                    double ay, double hx, double hy, 
                                    double* gauss_x2, double* gauss_x3, 
                                    double* gauss_w3){ 

    constexpr size_t Nloc = o*(o+1)/2;

    for(int k=0;k<face.n_sub_outer;++k){

        double x1 = face.intersections[k].x;
        double y1 = face.intersections[k].y;
        double x2 = face.intersections[k+1].x;
        double y2 = face.intersections[k+1].y;

        int su = mysign(x2-x1)*anticlock;
        int sv = mysign(y2-y1)*anticlock;

        int cell_i = (face.intersections[k].i+(sv>0) + 256)/2 - 128;
        int cell_j = (face.intersections[k].j+(su<0) + 256)/2 - 128;

        //periodic bc
        int cell_i_p = (cell_i+Nx)%Nx;
        int cell_j_p = (cell_j+Ny)%Ny;

        if(print){
            #if defined(PRINT_I) && defined(PRINT_J)
            cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
            cout << "x2: " << x2 << ", y2: " << y2 << endl;
            cout << "i: " << cell_i << ", j: " << cell_j;
            cout << ", anticklock: " << anticlock << endl;
            #endif
        }

        leg_segment_integral_f<o>(x1, y1, x2, y2, cell_i, cell_j, x_star, y_star,  
                              &dg_sol[Nloc*(cell_i_p + Nx*cell_j_p)], out, anticlock,
                              ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);
    }

}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_segment_integral_precomp_f(double x1, double y1, double x2, double y2,
                            int cell_i, int cell_j, double x_star, double y_star,
                            leg_adjoint_solution<o>& adj_sol, 
                            leg_precomp_integrals<o>& out, 
                            double anticlock,
                            double ax, double ay, double hx, double hy,
                            double* gauss_x2, double* gauss_x3, double* gauss_w3){ 

    constexpr size_t Nloc = o*(o+1)/2;

    out.is_used=true;

    double clx = ax + hx*(0.5 + cell_i);
    double cly = ay + hy*(0.5 + cell_j);

    x1 = (x1 - clx)/hx;
    y1 = (y1 - cly)/hy;
    x2 = (x2 - clx)/hx;
    y2 = (y2 - cly)/hy;

    double x_shift = (clx-x_star)/hx;
    double y_shift = (cly-y_star)/hy;

    if( fabs(x2-x1) < MACHINE_PREC*10 && fabs(y2-y1) < MACHINE_PREC*10 ){
        //do nothing
    } else {

        double x2m1 = x2-x1;
        double y2m1 = y2-y1;

        double green00 = (x2+x1)*0.5*y2m1;

        double green10, green01, green20, green11, green02; 
        if(o>1){
            double x_g1 = x1 + gauss_x2[0]*x2m1;
            double x_g2 = x1 + gauss_x2[1]*x2m1;
            double y_g1 = y1 + gauss_x2[0]*y2m1;
            double y_g2 = y1 + gauss_x2[1]*y2m1;

            green10 = (x_g1*x_g1 + x_g2*x_g2)*0.25*y2m1;
            green01 =-(y_g1*y_g1 + y_g2*y_g2)*0.25*x2m1;
            green20 = (x_g1*x_g1*x_g1 + x_g2*x_g2*x_g2)/6.0*y2m1;
            green11 = (x_g1*x_g1*y_g1 + x_g2*x_g2*y_g2)*0.25*y2m1;
            green02 =-(y_g1*y_g1*y_g1 + y_g2*y_g2*y_g2)/6.0*x2m1;
        }
        double green30, green21, green12, green03; 
        double green40, green31, green22, green13, green04;
        if(o>2){
            double x_g1 = x1 + gauss_x3[0]*x2m1;
            double x_g2 = x1 + gauss_x3[1]*x2m1;
            double x_g3 = x1 + gauss_x3[2]*x2m1;
            double y_g1 = y1 + gauss_x3[0]*y2m1;
            double y_g2 = y1 + gauss_x3[1]*y2m1;
            double y_g3 = y1 + gauss_x3[2]*y2m1;

            green30 = (x_g1*x_g1*x_g1*x_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*gauss_w3[2])*0.25*0.5*y2m1;
            green21 = (x_g1*x_g1*x_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*y_g3*gauss_w3[2])/3.0*0.5*y2m1;
            green12 =-(x_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*y_g3*y_g3*y_g3*gauss_w3[2])/3.0*0.5*x2m1;
            green03 =-(y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.25*0.5*x2m1;

            green40 = (x_g1*x_g1*x_g1*x_g1*x_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*x_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*x_g3*gauss_w3[2])*0.2*0.5*y2m1;
            green31 = (x_g1*x_g1*x_g1*x_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*x_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*x_g3*y_g3*gauss_w3[2])*0.25*0.5*y2m1;
            green22 = (x_g1*x_g1*x_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*x_g2*x_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*x_g3*x_g3*y_g3*y_g3*gauss_w3[2])/3.0*0.5*y2m1;
            green13 =-(x_g1*y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       x_g2*y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       x_g3*y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.25*0.5*x2m1;
            green04 =-(y_g1*y_g1*y_g1*y_g1*y_g1*gauss_w3[0] + 
                       y_g2*y_g2*y_g2*y_g2*y_g2*gauss_w3[1] + 
                       y_g3*y_g3*y_g3*y_g3*y_g3*gauss_w3[2])*0.2*0.5*x2m1;
        }

        if(o==1){
            out.data[0][0] += anticlock*green00*adj_sol.aux[0][0];
        }
        if(o==2){

            for(int m=0;m<Nloc;++m){

                double c1_00 = adj_sol.aux[m][0] + 
                               adj_sol.aux[m][1]*x_shift + 
                               adj_sol.aux[m][2]*y_shift;

                double C1 = c1_00*green00 + adj_sol.aux[m][1]*green10 
                                          + adj_sol.aux[m][2]*green01;
                double C2 = c1_00*green10 + adj_sol.aux[m][1]*green20 
                                          + adj_sol.aux[m][2]*green11;
                double C3 = c1_00*green01 + adj_sol.aux[m][1]*green11 
                                          + adj_sol.aux[m][2]*green02;

                out.data[m][0] += anticlock*C1;
                out.data[m][1] += anticlock*C2;
                out.data[m][2] += anticlock*C3;
            }
        }
        if(o==3){

            for(int m=0;m<Nloc;++m){

                double c1 = adj_sol.aux[m][0] + 
                            adj_sol.aux[m][1]*x_shift +
                            adj_sol.aux[m][2]*y_shift + 
                            adj_sol.aux[m][3]*(x_shift*x_shift-1.0/12.0) +
                            adj_sol.aux[m][4]*(x_shift*y_shift) + 
                            adj_sol.aux[m][5]*(y_shift*y_shift-1.0/12.0);
                double c2 = adj_sol.aux[m][1] + adj_sol.aux[m][4]*y_shift +
                                            2.0*adj_sol.aux[m][3]*x_shift;
                double c3 = adj_sol.aux[m][2] + adj_sol.aux[m][4]*x_shift + 
                                            2.0*adj_sol.aux[m][5]*y_shift;
                double c4 = adj_sol.aux[m][3];
                double c5 = adj_sol.aux[m][4];
                double c6 = adj_sol.aux[m][5];

                double tmp1 = c1*green00 + c2*green10 + c3*green01 +
                              c4*green20 + c5*green11 + c6*green02;
                double tmp2 = c1*green10 + c2*green20 + c3*green11 +
                              c4*green30 + c5*green21 + c6*green12;
                double tmp3 = c1*green01 + c2*green11 + c3*green02 +
                              c4*green21 + c5*green12 + c6*green03;
                double tmp4 = c1*green20 + c2*green30 + c3*green21 +
                              c4*green40 + c5*green31 + c6*green22;
                double tmp5 = c1*green11 + c2*green21 + c3*green12 +
                              c4*green31 + c5*green22 + c6*green13;
                double tmp6 = c1*green02 + c2*green12 + c3*green03 +
                              c4*green22 + c5*green13 + c6*green04;

                double C1 = tmp1;
                double C2 = tmp2; 
                double C3 = tmp3; 
                double C4 = tmp4 - 1./12.*C1; 
                double C5 = tmp5; 
                double C6 = tmp6 - 1./12.*C1; 

                out.data[m][0] += anticlock*C1;
                out.data[m][1] += anticlock*C2;
                out.data[m][2] += anticlock*C3;
                out.data[m][3] += anticlock*C4;
                out.data[m][4] += anticlock*C5;
                out.data[m][5] += anticlock*C6;
            }

        }
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_compute_integrals_outer_precomp_f(face<o>& face, double x_star, 
                                    double y_star, int anticlock,
                                    leg_adjoint_solution<o>& adj_sol, 
                                    leg_all_integrals<o>& all_int,
                                    bool print, double ax,
                                    double ay, double hx, double hy, 
                                    double* gauss_x2, double* gauss_x3, 
                                    double* gauss_w3){ 

    for(int k=0;k<face.n_sub_outer;++k){

        double x1 = face.intersections[k].x;
        double y1 = face.intersections[k].y;
        double x2 = face.intersections[k+1].x;
        double y2 = face.intersections[k+1].y;

        int su = mysign(x2-x1)*anticlock;
        int sv = mysign(y2-y1)*anticlock;

        int cell_i = (face.intersections[k].i+(sv>0) + 256)/2 - 128;
        int cell_j = (face.intersections[k].j+(su<0) + 256)/2 - 128;

        if(print){
            #if defined(PRINT_I) && defined(PRINT_J)
            cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
            cout << "x2: " << x2 << ", y2: " << y2 << endl;
            cout << "i: " << cell_i << ", j: " << cell_j;
            cout << ", anticklock: " << anticlock << endl;
            #endif
        }

        leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j, x_star, 
                                      y_star, adj_sol, all_int(cell_i,cell_j),
                                      anticlock, ax, ay, hx, hy, 
                                      gauss_x2, gauss_x3, gauss_w3);
    }

}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_get_A_b_lsq_f(double A[o*(o+1)/2][o*(o+1)/2], 
                       double B[o*(o+1)/2][o*(o+1)/2], 
                       double v[o*o][2]){

    if(o==1){
        A[0][0] = 1.0;
        B[0][0] = 1.0;
    }
    if(o==2){
        A[0][0] = 4.0;
        A[0][1] = v[0][0] + v[1][0] + v[2][0] + v[3][0];
        A[0][2] = v[0][1] + v[1][1] + v[2][1] + v[3][1];
        A[1][1] = v[0][0]*v[0][0] + v[1][0]*v[1][0] + 
                  v[2][0]*v[2][0] + v[3][0]*v[3][0];
        A[1][2] = v[0][0]*v[0][1] + v[1][0]*v[1][1] + 
                  v[2][0]*v[2][1] + v[3][0]*v[3][1];
        A[2][2] = v[0][1]*v[0][1] + v[1][1]*v[1][1] + 
                  v[2][1]*v[2][1] + v[3][1]*v[3][1];

        A[1][0] = A[0][1];
        A[2][0] = A[0][2];
        A[2][1] = A[1][2];

        B[0][0] = A[0][0];
        B[0][1] = A[0][1];
        B[0][2] = A[0][2];

        B[1][0] = 0.0;
        B[1][1] = -0.5*v[0][0]
                  +0.5*v[1][0]
                  -0.5*v[2][0]
                  +0.5*v[3][0];
        B[1][2] = -0.5*v[0][1]
                  +0.5*v[1][1]
                  -0.5*v[2][1]
                  +0.5*v[3][1];

        B[2][0] = 0.0;
        B[2][1] = -0.5*v[0][0]
                  -0.5*v[1][0]
                  +0.5*v[2][0]
                  +0.5*v[3][0];
        B[2][2] = -0.5*v[0][1]
                  -0.5*v[1][1]
                  +0.5*v[2][1]
                  +0.5*v[3][1];
    }


    if(o==3){
        //Generalization of the above and below, but extremely slow, 
        //the problem is probably the if statement in the basisf funktion, i.e.,
        //in the innermost for loop. Additionally, the below is able to reduce
        //more operations
        //
        //for(int ii=0;ii<Nloc;++ii){
        //    for(int jj=0;jj<ii+1;++jj){
        //        A[ii][jj] = 0.0;
        //        for(int k=0;k<o*o;++k)
        //            A[ii][jj] += basisf(v[k][0],v[k][1],jj)*
        //                         basisf(v[k][0],v[k][1],ii);
        //        A[jj][ii] = A[ii][jj];
        //    }
        //}

        //double h = 1.0/double(o-1);
        //for(int ii=0;ii<Nloc;++ii){
        //    for(int jj=0;jj<Nloc;++jj){
        //        B[ii][jj] = 0.0;
        //        for(int ky=0;ky<o;++ky)
        //            for(int kx=0;kx<o;++kx)
        //                B[ii][jj] += basisf(v[kx+o*ky][0],v[kx+o*ky][1],jj)*
        //                             basisf(-0.5 + h*kx, -0.5 + h*ky,ii);
        //    }
        //}


        A[0][0] = 9.0;
        A[0][1] = v[0][0] + v[1][0] + v[2][0] + v[3][0] + v[4][0] + 
                  v[5][0] + v[6][0] + v[7][0] + v[8][0];
        A[0][2] = v[0][1] + v[1][1] + v[2][1] + v[3][1] + v[4][1] + 
                  v[5][1] + v[6][1] + v[7][1] + v[8][1];
        A[0][3] = v[0][0]*v[0][0] + v[1][0]*v[1][0] + v[2][0]*v[2][0] + 
                  v[3][0]*v[3][0] + v[4][0]*v[4][0] + v[5][0]*v[5][0] + 
                  v[6][0]*v[6][0] + v[7][0]*v[7][0] + v[8][0]*v[8][0] - 9./12.;
        A[0][4] = v[0][0]*v[0][1] + v[1][0]*v[1][1] + v[2][0]*v[2][1] + 
                  v[3][0]*v[3][1] + v[4][0]*v[4][1] + v[5][0]*v[5][1] + 
                  v[6][0]*v[6][1] + v[7][0]*v[7][1] + v[8][0]*v[8][1];
        A[0][5] = v[0][1]*v[0][1] + v[1][1]*v[1][1] + v[2][1]*v[2][1] + 
                  v[3][1]*v[3][1] + v[4][1]*v[4][1] + v[5][1]*v[5][1] + 
                  v[6][1]*v[6][1] + v[7][1]*v[7][1] + v[8][1]*v[8][1] - 9./12.;
        A[1][0] = A[0][1];
        A[1][1] = A[0][3] + 9./12.;
        A[1][2] = A[0][4];
        A[1][3] = v[0][0]*v[0][0]*v[0][0] + v[1][0]*v[1][0]*v[1][0] + 
                  v[2][0]*v[2][0]*v[2][0] + v[3][0]*v[3][0]*v[3][0] + 
                  v[4][0]*v[4][0]*v[4][0] + v[5][0]*v[5][0]*v[5][0] + 
                  v[6][0]*v[6][0]*v[6][0] + v[7][0]*v[7][0]*v[7][0] + 
                  v[8][0]*v[8][0]*v[8][0] - 1./12.*A[0][1];
        A[1][4] = v[0][0]*v[0][0]*v[0][1] + v[1][0]*v[1][0]*v[1][1] + 
                  v[2][0]*v[2][0]*v[2][1] + v[3][0]*v[3][0]*v[3][1] + 
                  v[4][0]*v[4][0]*v[4][1] + v[5][0]*v[5][0]*v[5][1] + 
                  v[6][0]*v[6][0]*v[6][1] + v[7][0]*v[7][0]*v[7][1] + 
                  v[8][0]*v[8][0]*v[8][1];
        A[1][5] = v[0][0]*v[0][1]*v[0][1] + v[1][0]*v[1][1]*v[1][1] + 
                  v[2][0]*v[2][1]*v[2][1] + v[3][0]*v[3][1]*v[3][1] + 
                  v[4][0]*v[4][1]*v[4][1] + v[5][0]*v[5][1]*v[5][1] + 
                  v[6][0]*v[6][1]*v[6][1] + v[7][0]*v[7][1]*v[7][1] + 
                  v[8][0]*v[8][1]*v[8][1] - 1./12.*A[0][1];
        A[2][0] = A[0][2];
        A[2][1] = A[1][2];
        A[2][2] = A[0][5] + 9./12.;
        A[2][3] = A[1][4] - 1./12.*A[0][2];
        A[2][4] = A[1][5] + 1./12.*A[0][1];
        A[2][5] = v[0][1]*v[0][1]*v[0][1] + v[1][1]*v[1][1]*v[1][1] + 
                  v[2][1]*v[2][1]*v[2][1] + v[3][1]*v[3][1]*v[3][1] + 
                  v[4][1]*v[4][1]*v[4][1] + v[5][1]*v[5][1]*v[5][1] + 
                  v[6][1]*v[6][1]*v[6][1] + v[7][1]*v[7][1]*v[7][1] + 
                  v[8][1]*v[8][1]*v[8][1] - 1./12.*A[0][2];
        A[3][0] = A[0][3];
        A[3][1] = A[1][3];
        A[3][2] = A[2][3];
        A[3][3] = v[0][0]*v[0][0]*v[0][0]*v[0][0] + v[1][0]*v[1][0]*v[1][0]*v[1][0]+
                  v[2][0]*v[2][0]*v[2][0]*v[2][0] + v[3][0]*v[3][0]*v[3][0]*v[3][0]+
                  v[4][0]*v[4][0]*v[4][0]*v[4][0] + v[5][0]*v[5][0]*v[5][0]*v[5][0]+
                  v[6][0]*v[6][0]*v[6][0]*v[6][0] + v[7][0]*v[7][0]*v[7][0]*v[7][0]+
                  v[8][0]*v[8][0]*v[8][0]*v[8][0] + 9./144. - 1./6.*A[1][1];
        A[3][4] = v[0][0]*v[0][0]*v[0][0]*v[0][1] + v[1][0]*v[1][0]*v[1][0]*v[1][1]+
                  v[2][0]*v[2][0]*v[2][0]*v[2][1] + v[3][0]*v[3][0]*v[3][0]*v[3][1]+
                  v[4][0]*v[4][0]*v[4][0]*v[4][1] + v[5][0]*v[5][0]*v[5][0]*v[5][1]+
                  v[6][0]*v[6][0]*v[6][0]*v[6][1] + v[7][0]*v[7][0]*v[7][0]*v[7][1]+
                  v[8][0]*v[8][0]*v[8][0]*v[8][1] - 1./12.*A[1][2];
        A[3][5] = v[0][0]*v[0][0]*v[0][1]*v[0][1] + v[1][0]*v[1][0]*v[1][1]*v[1][1]+
                  v[2][0]*v[2][0]*v[2][1]*v[2][1] + v[3][0]*v[3][0]*v[3][1]*v[3][1]+
                  v[4][0]*v[4][0]*v[4][1]*v[4][1] + v[5][0]*v[5][0]*v[5][1]*v[5][1]+
                  v[6][0]*v[6][0]*v[6][1]*v[6][1] + v[7][0]*v[7][0]*v[7][1]*v[7][1]+
                  v[8][0]*v[8][0]*v[8][1]*v[8][1] - 1./12.*(A[1][1]+A[2][2])+9./144;
        A[4][0] = A[0][4];
        A[4][1] = A[1][4];
        A[4][2] = A[2][4];
        A[4][3] = A[3][4];
        A[4][4] = A[3][5] + 1./12.*(A[1][1] + A[2][2]) - 9./144;
        A[4][5] = v[0][0]*v[0][1]*v[0][1]*v[0][1] + v[1][0]*v[1][1]*v[1][1]*v[1][1]+
                  v[2][0]*v[2][1]*v[2][1]*v[2][1] + v[3][0]*v[3][1]*v[3][1]*v[3][1]+
                  v[4][0]*v[4][1]*v[4][1]*v[4][1] + v[5][0]*v[5][1]*v[5][1]*v[5][1]+
                  v[6][0]*v[6][1]*v[6][1]*v[6][1] + v[7][0]*v[7][1]*v[7][1]*v[7][1]+
                  v[8][0]*v[8][1]*v[8][1]*v[8][1] - 1./12.*A[1][2];
        A[5][0] = A[0][5];
        A[5][1] = A[1][5];
        A[5][2] = A[2][5];
        A[5][3] = A[3][5];
        A[5][4] = A[4][5];
        A[5][5] = v[0][1]*v[0][1]*v[0][1]*v[0][1] + v[1][1]*v[1][1]*v[1][1]*v[1][1]+
                  v[2][1]*v[2][1]*v[2][1]*v[2][1] + v[3][1]*v[3][1]*v[3][1]*v[3][1]+
                  v[4][1]*v[4][1]*v[4][1]*v[4][1] + v[5][1]*v[5][1]*v[5][1]*v[5][1]+
                  v[6][1]*v[6][1]*v[6][1]*v[6][1] + v[7][1]*v[7][1]*v[7][1]*v[7][1]+
                  v[8][1]*v[8][1]*v[8][1]*v[8][1] + 9./144. - 1./6.*A[2][2];


        B[0][0] = 9.0;
        B[0][1] = v[0][0] + v[1][0] + v[2][0] + 
                  v[3][0] + v[4][0] + v[5][0] + 
                  v[6][0] + v[7][0] + v[8][0];
        B[0][2] = v[0][1] + v[1][1] + v[2][1] + 
                  v[3][1] + v[4][1] + v[5][1] + 
                  v[6][1] + v[7][1] + v[8][1];
        B[0][3] = v[0][0]*v[0][0] + v[1][0]*v[1][0] + v[2][0]*v[2][0] + 
                  v[3][0]*v[3][0] + v[4][0]*v[4][0] + v[5][0]*v[5][0] + 
                  v[6][0]*v[6][0] + v[7][0]*v[7][0] + v[8][0]*v[8][0] - 9./12.;
        B[0][4] = v[0][0]*v[0][1] + v[1][0]*v[1][1] + v[2][0]*v[2][1] + 
                  v[3][0]*v[3][1] + v[4][0]*v[4][1] + v[5][0]*v[5][1] + 
                  v[6][0]*v[6][1] + v[7][0]*v[7][1] + v[8][0]*v[8][1];
        B[0][5] = v[0][1]*v[0][1] + v[1][1]*v[1][1] + v[2][1]*v[2][1] + 
                  v[3][1]*v[3][1] + v[4][1]*v[4][1] + v[5][1]*v[5][1] + 
                  v[6][1]*v[6][1] + v[7][1]*v[7][1] + v[8][1]*v[8][1] - 9./12.;

        B[1][0] = 0.0;
        B[1][1] = -0.5*v[0][0] + 0.5*v[2][0] + 
                  -0.5*v[3][0] + 0.5*v[5][0] + 
                  -0.5*v[6][0] + 0.5*v[8][0];
        B[1][2] = -0.5*v[0][1] + 0.5*v[2][1] + 
                  -0.5*v[3][1] + 0.5*v[5][1] + 
                  -0.5*v[6][1] + 0.5*v[8][1];
        B[1][3] = -0.5*v[0][0]*v[0][0] + 0.5*v[2][0]*v[2][0] + 
                  -0.5*v[3][0]*v[3][0] + 0.5*v[5][0]*v[5][0] + 
                  -0.5*v[6][0]*v[6][0] + 0.5*v[8][0]*v[8][0] - 1./12.*B[1][0];
        B[1][4] = -0.5*v[0][0]*v[0][1] + 0.5*v[2][0]*v[2][1] + 
                  -0.5*v[3][0]*v[3][1] + 0.5*v[5][0]*v[5][1] + 
                  -0.5*v[6][0]*v[6][1] + 0.5*v[8][0]*v[8][1];
        B[1][5] = -0.5*v[0][1]*v[0][1] + 0.5*v[2][1]*v[2][1] + 
                  -0.5*v[3][1]*v[3][1] + 0.5*v[5][1]*v[5][1] + 
                  -0.5*v[6][1]*v[6][1] + 0.5*v[8][1]*v[8][1] - 1./12.*B[1][0];

        B[2][0] = 0.0;
        B[2][1] = -0.5*v[0][0] -0.5*v[1][0] -0.5*v[2][0] 
                  +0.5*v[6][0] +0.5*v[7][0] +0.5*v[8][0];
        B[2][2] = -0.5*v[0][1] -0.5*v[1][1] -0.5*v[2][1] 
                  +0.5*v[6][1] +0.5*v[7][1] +0.5*v[8][1];
        B[2][3] = -0.5*v[0][0]*v[0][0] -0.5*v[1][0]*v[1][0] -0.5*v[2][0]*v[2][0] 
                  +0.5*v[6][0]*v[6][0] +0.5*v[7][0]*v[7][0] +0.5*v[8][0]*v[8][0] 
                    - 1./12.*B[2][0];
        B[2][4] = -0.5*v[0][0]*v[0][1] -0.5*v[1][0]*v[1][1] -0.5*v[2][0]*v[2][1]
                  +0.5*v[6][0]*v[6][1] +0.5*v[7][0]*v[7][1] +0.5*v[8][0]*v[8][1];
        B[2][5] = -0.5*v[0][1]*v[0][1] -0.5*v[1][1]*v[1][1] -0.5*v[2][1]*v[2][1]
                  +0.5*v[6][1]*v[6][1] +0.5*v[7][1]*v[7][1] +0.5*v[8][1]*v[8][1] 
                    - 1./12.*B[2][0];

        B[3][0] = 0.75;
        B[3][1] = 1./6.*v[0][0] - 1./12.*v[1][0] + 1./6.*v[2][0] + 
                  1./6.*v[3][0] - 1./12.*v[4][0] + 1./6.*v[5][0] + 
                  1./6.*v[6][0] - 1./12.*v[7][0] + 1./6.*v[8][0];
        B[3][2] = 1./6.*v[0][1] - 1./12.*v[1][1] + 1./6.*v[2][1] + 
                  1./6.*v[3][1] - 1./12.*v[4][1] + 1./6.*v[5][1] + 
                  1./6.*v[6][1] - 1./12.*v[7][1] + 1./6.*v[8][1];
        B[3][3] = 1./6.*v[0][0]*v[0][0] - 1./12.*v[1][0]*v[1][0] + 
                  1./6.*v[2][0]*v[2][0] + 
                  1./6.*v[3][0]*v[3][0] - 1./12.*v[4][0]*v[4][0] + 
                  1./6.*v[5][0]*v[5][0] + 
                  1./6.*v[6][0]*v[6][0] - 1./12.*v[7][0]*v[7][0] +
                  1./6.*v[8][0]*v[8][0] - 1./12.*B[3][0];
        B[3][4] = 1./6.*v[0][0]*v[0][1] - 1./12.*v[1][0]*v[1][1] + 
                  1./6.*v[2][0]*v[2][1] + 
                  1./6.*v[3][0]*v[3][1] - 1./12.*v[4][0]*v[4][1] + 
                  1./6.*v[5][0]*v[5][1] + 
                  1./6.*v[6][0]*v[6][1] - 1./12.*v[7][0]*v[7][1] + 
                  1./6.*v[8][0]*v[8][1];
        B[3][5] = 1./6.*v[0][1]*v[0][1] - 1./12.*v[1][1]*v[1][1] + 
                  1./6.*v[2][1]*v[2][1] + 
                  1./6.*v[3][1]*v[3][1] - 1./12.*v[4][1]*v[4][1] + 
                  1./6.*v[5][1]*v[5][1] + 
                  1./6.*v[6][1]*v[6][1] - 1./12.*v[7][1]*v[7][1] + 
                  1./6.*v[8][1]*v[8][1] - 1./12.*B[3][0];

        B[4][0] = 0.0;
        B[4][1] = 0.25*v[0][0] - 0.25*v[2][0] + 
                 -0.25*v[6][0] + 0.25*v[8][0];
        B[4][2] =  0.25*v[0][1] - 0.25*v[2][1] + 
                  -0.25*v[6][1] + 0.25*v[8][1];
        B[4][3] =  0.25*v[0][0]*v[0][0] - 0.25*v[2][0]*v[2][0] + 
                  -0.25*v[6][0]*v[6][0] + 0.25*v[8][0]*v[8][0];
        B[4][4] =  0.25*v[0][0]*v[0][1] - 0.25*v[2][0]*v[2][1] + 
                  -0.25*v[6][0]*v[6][1] + 0.25*v[8][0]*v[8][1];
        B[4][5] =  0.25*v[0][1]*v[0][1] - 0.25*v[2][1]*v[2][1] + 
                  -0.25*v[6][1]*v[6][1] + 0.25*v[8][1]*v[8][1];

        B[5][0] = 0.75;
        B[5][1] =  1./6.*v[0][0] +  1./6.*v[1][0] +  1./6.*v[2][0] + 
                 -1./12.*v[3][0] - 1./12.*v[4][0] - 1./12.*v[5][0] + 
                   1./6.*v[6][0] +  1./6.*v[7][0] +  1./6.*v[8][0];
        B[5][2] = +  1./6.*v[0][1] + +  1./6.*v[1][1] + +  1./6.*v[2][1] + 
                  - 1./12.*v[3][1] + - 1./12.*v[4][1] + - 1./12.*v[5][1] + 
                  +  1./6.*v[6][1] + +  1./6.*v[7][1] + +  1./6.*v[8][1];
        B[5][3] = +  1./6.*v[0][0]*v[0][0] +  1./6.*v[1][0]*v[1][0] 
                  +  1./6.*v[2][0]*v[2][0] + 
                  - 1./12.*v[3][0]*v[3][0] - 1./12.*v[4][0]*v[4][0] 
                  - 1./12.*v[5][0]*v[5][0] + 
                  +  1./6.*v[6][0]*v[6][0] +  1./6.*v[7][0]*v[7][0] 
                  +  1./6.*v[8][0]*v[8][0] - 1./12.*B[5][0];
        B[5][4] = +  1./6.*v[0][0]*v[0][1] +  1./6.*v[1][0]*v[1][1]
                  +  1./6.*v[2][0]*v[2][1] + 
                  - 1./12.*v[3][0]*v[3][1] - 1./12.*v[4][0]*v[4][1]
                  - 1./12.*v[5][0]*v[5][1] + 
                  +  1./6.*v[6][0]*v[6][1] +  1./6.*v[7][0]*v[7][1]
                  +  1./6.*v[8][0]*v[8][1];
        B[5][5] = +  1./6.*v[0][1]*v[0][1] +  1./6.*v[1][1]*v[1][1]
                  +  1./6.*v[2][1]*v[2][1] + 
                  - 1./12.*v[3][1]*v[3][1] - 1./12.*v[4][1]*v[4][1]
                  - 1./12.*v[5][1]*v[5][1] + 
                  +  1./6.*v[6][1]*v[6][1] +  1./6.*v[7][1]*v[7][1]
                  +  1./6.*v[8][1]*v[8][1] - 1./12.*B[5][0];
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_compute_adjoint_solutions_f(double v[o*o][2], 
                                     leg_adjoint_solution<o>& as){

    constexpr size_t Nloc = o*(o+1)/2;

    double A[Nloc][Nloc];
    double B[Nloc][Nloc];

    leg_get_A_b_lsq_f<o>(A, B, v);

    int P[Nloc];
    LUPDecompose<Nloc>(A,1e-14,P);

    for(int m=0;m<Nloc;++m)
        LUPSolve<Nloc>(A,P,B[m],as.aux[m]);
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_sum_f(double* out, double* aux, double* weight,
               leg_adjoint_solution<o>& as){

    constexpr size_t Nloc = o*(o+1)/2;

    for(int m=0;m<Nloc;++m){

        out[m] = 0.0;

        for(int k=0;k<Nloc;++k){

            out[m] += as.aux[m][k]*aux[k];

        }

        out[m] *= weight[m];
    }
}


//no longer used, computation of phistar done in different steps
template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_compute_phistar_and_sum_f(double* out, double v[o*o][2],
                                   double* aux, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;
    double A[Nloc][Nloc]; 
    double B[Nloc][Nloc];

    leg_get_A_b_lsq_f<o>(A, B, v);
    
    int P[Nloc];
    LUPDecompose<Nloc>(A,1e-14,P);

    for(int m=0;m<Nloc;++m){

        out[m] = 0.0;
        double phi_star[Nloc] = {0};

        LUPSolve<Nloc>(A,P,B[m],phi_star);

        for(int k=0;k<Nloc;++k){

            out[m] += phi_star[k]*aux[k];

        }

        out[m] *= weight[m];
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_finalize_f(double* out, leg_all_integrals<o>& all_int, int Nx, int Ny,
                    double* in, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    double loc_in[MAX_CELLS][MAX_CELLS][Nloc] = {{{0}}};

    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){
    
            if(all_int.pi[s][r].is_used==true){
    
                int rr = all_int.min_i+r;
                int ss = all_int.min_j+s;

                rr += (rr<0) ? Nx : 0;
                rr -= (rr>=Nx) ? Nx : 0;

                ss += (ss<0) ? Ny : 0;
                ss -= (ss>=Ny) ? Ny : 0;

                for(int k=0;k<Nloc;++k){
    
                    loc_in[s][r][k] = in[k + Nloc*(rr+ss*Nx)];
                }
            }
        }
    }


    double out_loc[Nloc] = {0};
    for(int m=0;m<Nloc;++m){
    
        for(int s=0;s<MAX_CELLS;++s){
            for(int r=0;r<MAX_CELLS;++r){
    
                if(all_int.pi[s][r].is_used==true){
    
                    for(int k=0;k<Nloc;++k){
    
                        out_loc[m] += loc_in[s][r][k]*
                                      all_int.pi[s][r].data[m][k]*
                                      weight[m];
                    }
                }
            }
        }
    }

    for(int m=0;m<Nloc;++m)
        out[m] = out_loc[m];
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_finalize_direct_f(double* out, leg_all_integrals<o>& all_int, 
                           int Nx, int Ny, double* in, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    double out_loc[Nloc] = {0};
    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){
    
            if(all_int.pi[s][r].is_used==true){
    
                int rr = all_int.min_i+r;
                int ss = all_int.min_j+s;

                rr += (rr<0) ? Nx : 0;
                rr -= (rr>=Nx) ? Nx : 0;

                ss += (ss<0) ? Ny : 0;
                ss -= (ss>=Ny) ? Ny : 0;

                for(int m=0;m<Nloc;++m){
                    for(int k=0;k<Nloc;++k){

                        out_loc[m] += in[k + Nloc*(rr+ss*Nx)]*
                                      all_int.pi[s][r].data[m][k]*
                                      weight[m];
                    }
                }
            }
        }
    }

    for(int m=0;m<Nloc;++m)
        out[m] = out_loc[m];
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_get_minmax_f(double* fij, double& min_v, double& max_v){

    max_v = -1e+3;
    min_v = +1e+3;
    for(int jj=0;jj<2;++jj){
        for(int ii=0;ii<2;++ii){

            double x = ii;
            double y = jj;

            double v =  fij[0] + fij[1]*(x-0.5) + fij[2]*(y-0.5);

            max_v = max(max_v,v);
            min_v = min(min_v,v);

        }
    }


}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void leg_finalize_with_slope_limiter_f(double* out, leg_all_integrals<o>& all_int,
                                       int Nx, int Ny, double* in, double* weight,
                                       int i, int j, bool print){

    constexpr size_t Nloc = o*(o+1)/2;

    double loc_in[MAX_CELLS][MAX_CELLS][Nloc] = {{{0}}};

    double min_in =  1e+3;
    double max_in = -1e+3;
    //get_minmax_f<o>(&in[Nloc*(i+j*Nx)],min_in,max_in);

    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){

            if(all_int.pi[s][r].is_used==true){

                int rr = all_int.min_i+r;
                int ss = all_int.min_j+s;

                rr += (rr<0) ? Nx : 0;
                rr -= (rr>=Nx) ? Nx : 0;

                ss += (ss<0) ? Ny : 0;
                ss -= (ss>=Ny) ? Ny : 0;

                for(int k=0;k<Nloc;++k){

                    loc_in[s][r][k] = in[k + Nloc*(rr+ss*Nx)];

                    double min_v;
                    double max_v;
                    leg_get_minmax_f<o>(loc_in[s][r],min_v,max_v);

                    min_in = min(min_in,min_v);
                    max_in = max(max_in,max_v);
                }
            }
        }
    }

    double out_loc[Nloc] = {0};
    for(int m=0;m<Nloc;++m){

        for(int s=0;s<MAX_CELLS;++s){
            for(int r=0;r<MAX_CELLS;++r){

                if(all_int.pi[s][r].is_used==true){

                    for(int k=0;k<Nloc;++k){

                        out_loc[m] += loc_in[s][r][k]*
                                      all_int.pi[s][r].data[m][k]*
                                      weight[m];
                    }
                }
            }
        }
    }

    double fmin;
    double fmax;
    leg_get_minmax_f<o>(out_loc,fmin,fmax);

    double theta_min = (min_in - out_loc[0])/(fmin - out_loc[0]);
    double theta_max = (max_in - out_loc[0])/(fmax - out_loc[0]);

    double theta = min(1.0,min(theta_min,theta_max));

    out[0] = out_loc[0];
    for(int m=1;m<Nloc;++m)
        out[m] = theta*out_loc[m];

    if(print){
        double min_out;
        double max_out;
        leg_get_minmax_f<o>(out,min_out,max_out);
        std::cout << std::setprecision(16);
        std::cout << "\n\ni,j: " << i << " " << j << endl;
        std::cout << "min_in: " << min_in << ",\tmax_in: " << max_in << endl;
        std::cout << "fmin:   " << fmin << ",\tfmax:   " << fmax << endl;
        std::cout << "minout: " << min_out << ",\tmaxout: " << max_out << endl;
        std::cout << "mean:   " << out_loc[0] << endl;
        std::cout << "theta_min: " << theta_min << endl;
        std::cout << "theta_max: " << theta_max << endl;
        std::cout << "theta: " << theta << endl;
        std::cout << out_loc[1] << " " << theta*out_loc[1] << endl;
        std::cout << out_loc[2] << " " << theta*out_loc[2] << endl;
        std::cout << std::setprecision(8);
    }


}


//double basisf(double x, double y, int i){
//    if(i==0)
//        return 1.0;
//    if(i==1)
//        return x;
//    if(i==2)
//        return y;
//    if(i==3)
//        return x*x-1.0/12.0;
//    if(i==4)
//        return x*y;
//    if(i==5)
//        return y*y-1.0/12.0;
//    cout << "ERROR: currently only up to o=3 implemented\n";
//    return 1;
//}

