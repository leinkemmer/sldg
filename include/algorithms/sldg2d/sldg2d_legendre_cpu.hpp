#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "legendre_f.hpp"


template<size_t o, typename F1, typename F2>
void leg_compute_upstream_mesh_RK_k(int Nx, int Ny, double ax, double ay,
                                double hx, double hy, double hx_fine, 
                                double hy_fine, F1 a, F2 b, double dt,
                                double t, ext_vect<vertex_star>& vs){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;
    
    #pragma omp parallel for schedule(static)
    for(int j=0;j<e_stride*Ny+1;++j){
        for(int i=0;i<e_stride*Nx+1;++i){
    
            compute_upstream_mesh_RK_f<o>(i,j,vs(i,j),dt,t,ax,ay,
                                          hx,hy,hx_fine,hy_fine,a,b);
        }
    }

}


template<size_t o>
void leg_get_outer_segments_k(int Nx, int Ny, ext_vect<face<o>>& face_lr,
                          ext_vect<face<o>>& face_bt, ext_vect<vertex_star>& vs,
                          double ax, double ay, double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;

    #pragma omp parallel
    {
    #pragma omp for
    for(int j=0;j<Ny+1;++j){
        for(int i=0;i<Nx;++i){
            bool print=false;
            #if defined(PRINT_I) && defined(PRINT_J)
            if(i==PRINT_I && (j==PRINT_J||j==PRINT_J+1)){
                print=true;
                cout << std::setprecision(16);
            }
            #endif
            bool s = find_intersections_f(face_lr(i,j),
                                          vs(i*e_stride,j*e_stride),
                                          vs((i+1)*e_stride,j*e_stride),
                                          ax,ay,hx,hy,print);
            #ifndef NDEBUG
            if(s==false)
                cout << "lr: " << i << "," << j << endl;
            #endif
        }
    }

    #pragma omp for
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx+1;++i){
            bool print=false;
            #if defined(PRINT_I) && defined(PRINT_J) 
            if((i==PRINT_I||i==PRINT_I+1) && j==PRINT_J)
                print=true;
            #endif
            bool s = find_intersections_f(face_bt(i,j),
                                          vs(i*e_stride,j*e_stride),
                                          vs(i*e_stride,(j+1)*e_stride),
                                          ax,ay,hx,hy,print);
            #ifndef NDEBUG
            if(s==false)
                cout << "bt: " << i << "," << j << endl;
            #endif
        }
    }
    }
}


template<size_t o>
void leg_get_inner_segmen_points_k(int Nx, int Ny, ext_vect<element_star<o>>& es,
                               ext_vect<vertex_star>& vs,
                               ext_vect<face<o>>& face_lr,
                               ext_vect<face<o>>& face_bt,
                               double ax, double ay, double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;

    #pragma omp parallel for
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            #if defined(PRINT_I) && defined(PRINT_J)
            bool print=false;
            if(i==PRINT_I&&j==PRINT_J)
                print=true;
            #endif

            es(i,j).min_i = min(min(vs(e_stride*i,e_stride*j).i,
                                    vs(e_stride*(i+1),e_stride*j).i),
                                min(vs(e_stride*(i+1),e_stride*(j+1)).i,
                                    vs(e_stride*i,e_stride*(j+1)).i));
            es(i,j).min_j = min(min(vs(e_stride*i,e_stride*j).j,
                                    vs(e_stride*(i+1),e_stride*j).j),
                                min(vs(e_stride*(i+1),e_stride*(j+1)).j,
                                    vs(e_stride*i,e_stride*(j+1)).j));

            es(i,j).x_center = (vs(e_stride*i,e_stride*j).x + 
                                vs(e_stride*(i+1),e_stride*j).x + 
                                vs(e_stride*(i+1),e_stride*(j+1)).x + 
                                vs(e_stride*i,e_stride*(j+1)).x)*0.25;
            es(i,j).y_center = (vs(e_stride*i,e_stride*j).y + 
                                vs(e_stride*(i+1),e_stride*j).y + 
                                vs(e_stride*(i+1),e_stride*(j+1)).y + 
                                vs(e_stride*i,e_stride*(j+1)).y)*0.25;

            for(int ii=0;ii<MAX_CELLS;++ii){
                es(i,j).idx_h[ii]=0;
                es(i,j).idx_v[ii]=0;
            }

            find_inner_points_f(es(i,j), face_lr(i,j),0);
            find_inner_points_f(es(i,j), face_bt(i+1,j),0);
            find_inner_points_f(es(i,j), face_lr(i,j+1),1);
            find_inner_points_f(es(i,j), face_bt(i,j),1);

            #if defined(PRINT_I) && defined(PRINT_J)
            if(print){
                cout << "\n super_inner_points" << endl;
                cout << "\ninner_segment_h " << endl;
                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int jj=0;jj<MAX_CELLS+1;++jj){
                        cout << es(i,j).inner_points_h[ii][jj].x << " ";
                        cout << es(i,j).inner_points_h[ii][jj].y << " ";
                        cout << es(i,j).inner_points_h[ii][jj].i << " ";
                        cout << es(i,j).inner_points_h[ii][jj].j << "   ";
                    }
                    cout << endl;
                }

                cout << "\ninner_segment_v " << endl;
                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int jj=0;jj<MAX_CELLS+1;++jj){
                        cout << es(i,j).inner_points_v[ii][jj].x << " ";
                        cout << es(i,j).inner_points_v[ii][jj].y << " ";
                        cout << es(i,j).inner_points_v[ii][jj].i << " ";
                        cout << es(i,j).inner_points_v[ii][jj].j << "   ";
                    }
                    cout << endl;
                }
            }
            #endif

            sort_and_complete_inner_f(es(i,j),ax,ay,hx,hy);

            #if defined(PRINT_I) && defined(PRINT_J)
            if(print){
                cout << "\n after sort and complete" << endl;
                cout << "\ninner_segment_h " << endl;
                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int jj=0;jj<MAX_CELLS+1;++jj){
                        cout << es(i,j).inner_points_h[ii][jj].x << " ";
                        cout << es(i,j).inner_points_h[ii][jj].y << " ";
                        cout << es(i,j).inner_points_h[ii][jj].i << " ";
                        cout << es(i,j).inner_points_h[ii][jj].j << "   ";
                    }
                    cout << endl;
                }

                cout << "\ninner_segment_v " << endl;
                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int jj=0;jj<MAX_CELLS+1;++jj){
                        cout << es(i,j).inner_points_v[ii][jj].x << " ";
                        cout << es(i,j).inner_points_v[ii][jj].y << " ";
                        cout << es(i,j).inner_points_v[ii][jj].i << " ";
                        cout << es(i,j).inner_points_v[ii][jj].j << "   ";
                    }
                    cout << endl;
                }
            }
            #endif
        }
    }
}


template<size_t o>
void leg_complete_outer_segments_k(int Nx, int Ny, ext_vect<face<o>>& face_lr,
                               ext_vect<face<o>>& face_bt){

    #pragma omp parallel
    {
    #pragma omp for
    for(int j=0;j<Ny+1;++j){
        for(int i=0;i<Nx;++i){
            for(int k=0;k<face_lr(i,j).n_sub_outer;++k){
                get_idx_of_segment_f(face_lr(i,j).intersections[k],
                                     face_lr(i,j).intersections[k+1]);
            }
        }
    }

    #pragma omp for
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx+1;++i){
            for(int k=0;k<face_bt(i,j).n_sub_outer;++k){
                get_idx_of_segment_f(face_bt(i,j).intersections[k],
                                     face_bt(i,j).intersections[k+1]);
            }
        }
    }
    }
}



template<size_t o>
void leg_compute_integrals_k(double* dg_old, double* aux, 
                         int Nz, int Nx, int Ny, ext_vect<element_star<o>>& es, 
                         ext_vect<face<o>>& face_lr, ext_vect<face<o>>& face_bt, 
                         double ax, double ay, double hx, double hy, 
                         double* gauss_x2, double* gauss_x3, double* gauss_w3){
    
    constexpr size_t Nloc = o*(o+1)/2;

    #pragma omp parallel for
    for(int l=0;l<Nz;++l){
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                bool print=false;
                #if defined(PRINT_I) && defined(PRINT_J)
                if(i==PRINT_I&&j==PRINT_J)
                    print=true;
                #endif

                int offset = Nloc*Nx*Ny*l;

                for(int ii=0;ii<Nloc;++ii)
                    aux[ii + Nloc*(i+j*Nx)+offset] = 0.0;


                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int k=0;k<es(i,j).idx_v[ii];++k){

                        double x1 = es(i,j).inner_points_v[ii][k].x;
                        double y1 = es(i,j).inner_points_v[ii][k].y;
                        double x2 = es(i,j).inner_points_v[ii][k+1].x;
                        double y2 = es(i,j).inner_points_v[ii][k+1].y;

                        int cell_i = es(i,j).inner_points_v[ii][k].i;
                        int cell_j = (es(i,j).inner_points_v[ii][k].j+256)/2-128;
                        int cell_j_p = (cell_j+Ny)%Ny;

                        int cell_i_left = (cell_i+256)/2-128;
                        int cell_i_left_p = (cell_i_left+Nx)%Nx;;
                        int cell_i_right = (cell_i+1+256)/2-128;
                        int cell_i_right_p = (cell_i_right+Nx)%Nx;

                        #if defined(PRINT_I) && defined(PRINT_J)
                        if(print){
                            cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
                            cout << "x2: " << x2 << ", y2: " << y2 << endl;
                            cout << "i_left: " << cell_i_left << ", i_right: "
                                 << cell_i_right << ", j: " << cell_j << endl;
                        }
                        #endif

                        int idx_dg = Nloc*(cell_i_left_p + Nx*cell_j_p) + offset;
                        int idx_aux = Nloc*(i+Nx*(j+Ny*l));
                        leg_segment_integral_f<o>(x1, y1, x2, y2, 
                                            cell_i_left, cell_j,
                                            es(i,j).x_center, es(i,j).y_center,
                                            &dg_old[idx_dg], &aux[idx_aux],
                                            1, ax, ay, hx, hy, 
                                            gauss_x2, gauss_x3, gauss_w3);

                        idx_dg = Nloc*(cell_i_right_p + Nx*cell_j_p) + offset;
                        leg_segment_integral_f<o>(x1, y1, x2, y2, 
                                            cell_i_right, cell_j,
                                            es(i,j).x_center, es(i,j).y_center,
                                            &dg_old[idx_dg], &aux[idx_aux], 
                                            -1, ax, ay, hx, hy, 
                                            gauss_x2, gauss_x3, gauss_w3);

                    }
                }

                for(int ii=0;ii<MAX_CELLS;++ii){
                    for(int k=0;k<es(i,j).idx_h[ii];++k){

                        double x1 = es(i,j).inner_points_h[ii][k].x;
                        double y1 = es(i,j).inner_points_h[ii][k].y;
                        double x2 = es(i,j).inner_points_h[ii][k+1].x;
                        double y2 = es(i,j).inner_points_h[ii][k+1].y;

                        int cell_i = (es(i,j).inner_points_h[ii][k].i+256)/2-128;
                        int cell_i_p = (cell_i+Nx)%Nx;
                        int cell_j = es(i,j).inner_points_h[ii][k].j;

                        int cell_j_top = (cell_j+1+256)/2-128;
                        int cell_j_top_p = (cell_j_top+Ny)%Ny;
                        int cell_j_bottom = (cell_j+256)/2-128;
                        int cell_j_bottom_p = (cell_j_bottom+Ny)%Ny;

                        #if defined(PRINT_I) && defined(PRINT_J)
                        if(print){
                            cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
                            cout << "x2: " << x2 << ", y2: " << y2 << endl;
                            cout << "i: " << cell_i << ", j_top: "
                                 << cell_j_top << ", j_bottom: " 
                                 << cell_j_bottom << endl;
                        }
                        #endif

                        int idx_dg = Nloc*(cell_i_p + Nx*cell_j_top_p) + offset;
                        int idx_aux = Nloc*(i + Nx*(j + Ny*l));
                        leg_segment_integral_f<o>(x1, y1, x2, y2, 
                                            cell_i, cell_j_top, 
                                            es(i,j).x_center, es(i,j).y_center,
                                            &dg_old[idx_dg], &aux[idx_aux],
                                            1, ax, ay, hx, hy, 
                                            gauss_x2, gauss_x3, gauss_w3);

                        idx_dg = Nloc*(cell_i_p + Nx*cell_j_bottom_p) + offset;
                        leg_segment_integral_f<o>(x1, y1, x2, y2, 
                                            cell_i, cell_j_bottom,
                                            es(i,j).x_center, es(i,j).y_center,
                                            &dg_old[idx_dg], &aux[idx_aux],
                                            -1, ax, ay, hx, hy, 
                                            gauss_x2, gauss_x3, gauss_w3);
                    }
                }

                int idx_aux = Nloc*(i + Nx*j) + offset;
                leg_compute_integrals_outer_face_f(face_lr(i,j), es(i,j).x_center, 
                                               es(i,j).y_center, 1, 
                                               &aux[idx_aux], 
                                               dg_old + offset, print, 
                                               Nx, Ny, ax, ay, hx, hy, 
                                               gauss_x2, gauss_x3, gauss_w3);
                leg_compute_integrals_outer_face_f(face_bt(i+1,j), es(i,j).x_center, 
                                               es(i,j).y_center, 1, 
                                               &aux[idx_aux], 
                                               dg_old + offset, print, 
                                               Nx, Ny, ax, ay, hx, hy,
                                               gauss_x2, gauss_x3, gauss_w3);
                leg_compute_integrals_outer_face_f(face_lr(i,j+1), es(i,j).x_center, 
                                               es(i,j).y_center, -1, 
                                               &aux[idx_aux],
                                               dg_old + offset, print, 
                                               Nx, Ny, ax, ay, hx, hy,
                                               gauss_x2, gauss_x3, gauss_w3);
                leg_compute_integrals_outer_face_f(face_bt(i,j), es(i,j).x_center, 
                                               es(i,j).y_center, -1, 
                                               &aux[idx_aux],
                                               dg_old + offset, print, 
                                               Nx, Ny, ax, ay, hx, hy,
                                               gauss_x2, gauss_x3, gauss_w3);
            }
        }
    }
}


template<size_t o>
void leg_compute_adjoint_solutions_k(int Nx, int Ny, ext_vect<vertex_star>& vs,
                                 ext_vect<element_star<o>>& es, 
                                 ext_vect<leg_adjoint_solution<o>>& as,
                                 double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;

    #pragma omp parallel for
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            double v[o*o][2];
            for(int jj=0;jj<o;++jj){
                for(int ii=0;ii<o;++ii){
                    v[ii+jj*o][0] = (vs(e_stride*i+ii, e_stride*j+jj).x - 
                                     es(i,j).x_center)/hx;
                    v[ii+jj*o][1] = (vs(e_stride*i+ii, e_stride*j+jj).y - 
                                     es(i,j).y_center)/hy;
                }
            }

            leg_compute_adjoint_solutions_f(v,as(i,j));

        }
    }
}
 

template<size_t o>
void leg_sum_k(int Nx, int Ny, int Nz, double* out, double* aux, 
               ext_vect<leg_adjoint_solution<o>>& as, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    #pragma omp parallel for
    for(int l=0;l<Nz;++l){
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                leg_sum_f(&out[Nloc*(i+Nx*(j+Ny*l))], 
                          &aux[Nloc*(i+Nx*(j+Ny*l))], 
                          weight, as(i,j));

            }
        }
    }
}


template<size_t o>
void leg_compute_integrals_precomp_f(int Nx, int Ny, 
                                 ext_vect<leg_all_integrals<o>>& ai,
                                 ext_vect<face<o>>& face_lr, 
                                 ext_vect<face<o>>& face_bt,
                                 ext_vect<element_star<o>>& es,
                                 ext_vect<leg_adjoint_solution<o>>& as,
                                 double ax, double ay, double hx, double hy, 
                                 double* gauss_x2, double* gauss_x3, 
                                 double* gauss_w3){

    #pragma omp parallel for
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            bool print=false;
            #if defined(PRINT_I) && defined(PRINT_J)
            if(i==PRINT_I&&j==PRINT_J)
                print=true;
            #endif

            ai.h_data[i+j*Nx].set_to_zero();
            cell_ij_outer_f(face_lr(i,j), 1, 
                            ai.h_data[i+j*Nx].min_i, ai.h_data[i+j*Nx].min_j);
            cell_ij_outer_f(face_bt(i+1,j), 1, 
                            ai.h_data[i+j*Nx].min_i, ai.h_data[i+j*Nx].min_j);
            cell_ij_outer_f(face_lr(i,j+1), -1, 
                            ai.h_data[i+j*Nx].min_i, ai.h_data[i+j*Nx].min_j);
            cell_ij_outer_f(face_bt(i,j), -1, 
                            ai.h_data[i+j*Nx].min_i, ai.h_data[i+j*Nx].min_j);

            leg_compute_integrals_outer_precomp_f<o>(face_lr(i,j), 
                                           es(i,j).x_center,
                                           es(i,j).y_center, 1, 
                                           as.h_data[i+j*Nx], ai.h_data[i+j*Nx],
                                           print, ax, ay, hx, hy, 
                                           gauss_x2, gauss_x3, gauss_w3);
            leg_compute_integrals_outer_precomp_f<o>(face_bt(i+1,j), 
                                           es(i,j).x_center,
                                           es(i,j).y_center, 1, 
                                           as.h_data[i+j*Nx], ai.h_data[i+j*Nx], 
                                           print, ax, ay, hx, hy,
                                           gauss_x2, gauss_x3, gauss_w3);
            leg_compute_integrals_outer_precomp_f<o>(face_lr(i,j+1), 
                                           es(i,j).x_center,
                                           es(i,j).y_center, -1, 
                                           as.h_data[i+j*Nx], ai.h_data[i+j*Nx], 
                                           print, ax, ay, hx, hy,
                                           gauss_x2, gauss_x3, gauss_w3);
            leg_compute_integrals_outer_precomp_f<o>(face_bt(i,j), 
                                           es(i,j).x_center, 
                                           es(i,j).y_center, -1, 
                                           as.h_data[i+j*Nx], ai.h_data[i+j*Nx], 
                                           print, ax, ay, hx, hy,
                                           gauss_x2, gauss_x3, gauss_w3);

            for(int ii=0;ii<MAX_CELLS;++ii){
                for(int k=0;k<es(i,j).idx_v[ii];++k){

                    double x1 = es(i,j).inner_points_v[ii][k].x;
                    double y1 = es(i,j).inner_points_v[ii][k].y;
                    double x2 = es(i,j).inner_points_v[ii][k+1].x;
                    double y2 = es(i,j).inner_points_v[ii][k+1].y;

                    int cell_i = es(i,j).inner_points_v[ii][k].i;
                    int cell_j = (es(i,j).inner_points_v[ii][k].j+256)/2-128;

                    int cell_i_left = (cell_i+256)/2-128;
                    int cell_i_right = (cell_i+1+256)/2-128;

                    #if defined(PRINT_I) && defined(PRINT_J)
                    if(print){
                        cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
                        cout << "x2: " << x2 << ", y2: " << y2 << endl;
                        cout << "i_left: " << cell_i_left << ", i_right: "
                             << cell_i_right << ", j: " << cell_j << endl;
                    }
                    #endif

                    leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, 
                                        cell_i_left, cell_j,
                                        es(i,j).x_center, es(i,j).y_center,
                                        as.h_data[i+j*Nx],
                                        ai.h_data[i+j*Nx](cell_i_left,cell_j),
                                        1, ax, ay, hx, hy, 
                                        gauss_x2, gauss_x3, gauss_w3);

                    leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, 
                                        cell_i_right, cell_j,
                                        es(i,j).x_center, es(i,j).y_center,
                                        as.h_data[i+j*Nx],
                                        ai.h_data[i+j*Nx](cell_i_right,cell_j),
                                        -1, ax, ay, hx, hy, 
                                        gauss_x2, gauss_x3, gauss_w3);

                }
            }

            for(int ii=0;ii<MAX_CELLS;++ii){
                for(int k=0;k<es(i,j).idx_h[ii];++k){

                    double x1 = es(i,j).inner_points_h[ii][k].x;
                    double y1 = es(i,j).inner_points_h[ii][k].y;
                    double x2 = es(i,j).inner_points_h[ii][k+1].x;
                    double y2 = es(i,j).inner_points_h[ii][k+1].y;

                    int cell_i = (es(i,j).inner_points_h[ii][k].i+256)/2-128;
                    int cell_j = es(i,j).inner_points_h[ii][k].j;

                    int cell_j_top = (cell_j+1+256)/2-128;
                    int cell_j_bottom = (cell_j+256)/2-128;

                    #if defined(PRINT_I) && defined(PRINT_J)
                    if(print){
                        cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
                        cout << "x2: " << x2 << ", y2: " << y2 << endl;
                        cout << "i: " << cell_i << ", j_top: "
                             << cell_j_top << ", j_bottom: " 
                             << cell_j_bottom << endl;
                    }
                    #endif

                    leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, 
                                        cell_i, cell_j_top, 
                                        es(i,j).x_center, es(i,j).y_center,
                                        as.h_data[i+j*Nx], 
                                        ai.h_data[i+j*Nx](cell_i,cell_j_top), 
                                        1, ax, ay, hx, hy, 
                                        gauss_x2, gauss_x3, gauss_w3);

                    leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, 
                                        cell_i, cell_j_bottom, 
                                        es(i,j).x_center, es(i,j).y_center,
                                        as.h_data[i+j*Nx],
                                        ai.h_data[i+j*Nx](cell_i,cell_j_bottom),
                                        -1, ax, ay, hx, hy,
                                        gauss_x2, gauss_x3, gauss_w3);


                }
            }
        }
    }
}


template<size_t o>
void leg_finalize_k(double* in, double* out, int Nz, int Nx, int Ny,
                    ext_vect<leg_all_integrals<o>>& ai, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    #pragma omp parallel for
    for(int l=0;l<Nz;++l){
        int offset = l*Nx*Ny*Nloc;
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                //leg_finalize_f<o>(&out[Nloc*(i+j*Nx)+offset], ai.h_data[i+j*Nx],
                //              Nx, Ny, in+offset, weight);
                leg_finalize_direct_f<o>(&out[Nloc*(i+j*Nx)+offset], 
                                         ai.h_data[i+j*Nx],
                                         Nx, Ny, in+offset, weight);
                //bool print=false;
                //#if defined(PRINTM_I) && defined(PRINTM_J)
                //if(i==PRINTM_I && j==PRINTM_J){
                //    print = true;
                //    cout << "\nslope limiter used" << endl;
                //}
                //#endif
                //leg_finalize_with_slope_limiter_f<o>(&out[Nloc*(i+j*Nx)+offset], 
                //                                 ai.h_data[i+j*Nx], Nx, Ny, 
                //                                 in+offset, weight, i, j, print);
            }
        }
    }
}



template<size_t o>
struct sldg2d_legendre_cpu_orig{

    static constexpr size_t Nloc = o*(o+1)/2;
    static constexpr size_t e_stride = (o==1) ? 1 : o-1;

    double ax, ay;
    double bx, by;
    double hx, hy;
    int Nx, Ny;

    double weight[6] = {1.0,12.0,12.0,180.0,144.0,180.0};
  
    double gauss_x2[2] = {(1.0-0.5773502691896258)*0.5,
                          (1.0+0.5773502691896258)*0.5};
    double gauss_x3[3] = {(1.0-0.7745966692414834)*0.5,
                          (1.0+0.0)*0.5,
                          (1.0+0.7745966692414834)*0.5};
    double gauss_w3[3] = {0.555555555555555,
                          0.8888888888888889,
                          0.555555555555555};

    ext_vect<vertex_star> vs;
    ext_vect<face<o>> face_lr;
    ext_vect<face<o>> face_bt;
    ext_vect<element_star<o>> es;
    ext_vect<leg_adjoint_solution<o>> as;

    sldg2d_legendre_cpu_orig(double _ax, double _ay, double _bx, 
                    double _by, int _Nx, int _Ny)
        : ax{_ax}, ay{_ay}, bx{_bx}, by{_by}, Nx{_Nx}, Ny{_Ny}
    {
        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        vs.resize(e_stride*Nx+1,e_stride*Ny+1);

        face_lr.resize(Nx,Ny+1);
        face_bt.resize(Nx+1,Ny);

        es.resize(Nx,Ny);
        as.resize(Nx,Ny);

    } 

    ~sldg2d_legendre_cpu_orig(){

    }


    template<typename F1, typename F2>
    void compute_upstream_mesh_RK(F1 a, F2 b, double dt, double t){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);

        leg_compute_upstream_mesh_RK_k<o>(Nx, Ny, ax, ay, hx, hy, hx_fine, 
                                      hy_fine, a, b, dt, t, vs);
    }


    //TODO
    template<typename T>
    void compute_upstream_mesh_cc(T& F, double dt, int stage){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);

        for(int j=0;j<e_stride*(Ny+1);++j){
            for(int i=0;i<e_stride*(Nx+1);++i){

                double x = ax+i*hx_fine;
                double y = ay+j*hy_fine;

                if(stage==1)
                    F.compute1(x,y,dt,i,j);
                if(stage==2)
                    F.compute2(x,y,dt,i,j);

                vs(i,j).set(x,y,ax,ay,hx,hy);

            }
        }
 
    }


    void get_outer_segments(){

        leg_get_outer_segments_k<o>(Nx, Ny, face_lr, face_bt, vs, ax, ay, hx, hy);
    }


    void get_inner_segment_points(){

        leg_get_inner_segmen_points_k<o>(Nx, Ny, es, vs, face_lr, face_bt,
                                     ax, ay, hx, hy);
    }


    void complete_outer_segments(){

        leg_complete_outer_segments_k<o>(Nx, Ny, face_lr, face_bt);

    }

    void compute_integrals(double* dg_old, double* aux, int Nz){

        leg_compute_integrals_k<o>(dg_old, aux, Nz, Nx, Ny, es, face_lr, face_bt, 
                               ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);
 
    }


    void compute_adjoint_solutions(){

        leg_compute_adjoint_solutions_k<o>(Nx, Ny, vs, es, as, hx, hy);

    }


    void finalize(double* out, double* aux, int Nz){

        leg_sum_k<o>(Nx, Ny, Nz, out, aux, as, weight);

    }


    template<typename F1, typename F2>
    void translate(generic_container<double>& sol, generic_container<double>& aux, 
                   F1 a, F2 b, double dt, double final_T, int Nz){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T 
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        timer t_RK, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        double t=0.0;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_RK.start();
            compute_upstream_mesh_RK(a,b,dt,t);
            t_RK.stop();

            #ifndef NDEBUG
            std::ofstream vertex("test/data_vertex.data");
            for(int j=0;j<e_stride*Ny+1;++j){
                for(int i=0;i<e_stride*Nx+1;++i){
                    vertex << std::setprecision(16);
                    vertex << vs(i,j).x << " " << vs(i,j).y << endl;
                }
            }
            #endif

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals(sol.data(false),aux.data(false),Nz);
            t_ci.stop();

            t_f.start();
            finalize(sol.data(false),aux.data(false),Nz);
            t_f.stop();

            #ifndef NDEBUG
            std::ofstream output("test/data_out"+std::to_string(ii+1)+".data");
            compute_mass_and_write_legendre<o>(ax, ay, hx, hy, Nx, Ny,
                                      sol.data(false), output, 2);
            #endif
        }

        aux.swap(sol);

        //cout << "Timings in milliseconds:\n";
        //cout << "RK: " << t_RK.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "perform the sum: " << t_f.total() << endl
        //     << "total: " << t_RK.total()+t_gos.total()+t_gis.total()+
        //                     t_cos.total()+t_ci.total()+t_f.total()+
        //                     t_cas.total()<<endl;

    }


    template<typename T, typename f>
    void translate_nl(generic_container<double>& in, 
                      generic_container<double>& aux, 
                      generic_container<double>& rk_stage, T& poisson,
                      double final_T, double dt, double c, bool divide, f g, 
                      double mass0){

        int n_steps = ceil(final_T/dt);
        cout << "\n\nN: " << Nx << ", final time: " << final_T 
             << ", step size: " << dt
             << ", number of time steps: " << n_steps << endl;

        double t=0.0;
        timer t_poisson, t_cum, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_poisson.start();
            poisson.solve_legendre(in.data(false),c,g,divide,ii);
            t_poisson.stop();

            t_cum.start();
            compute_upstream_mesh_cc(poisson,dt,1);
            t_cum.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();


            t_ci.start();
            compute_integrals(in.data(false),aux.data(false),1);
            t_ci.stop();

            t_f.start();
            finalize(rk_stage.data(false),aux.data(false),1);
            t_f.stop();

            #ifndef NDEBUG
            std::ofstream output1("data_aux"+std::to_string(ii+1)+".data");
            compute_mass_and_write_legendre<o>(ax, ay, hx, hy, Nx, Ny,
                                      rk_stage.data(false), output1,2);
            #endif

            //order 2
            t_poisson.start();
            poisson.solve_legendre(rk_stage.data(false),c,g,divide,-1);
            t_poisson.stop();

            t_cum.start();
            compute_upstream_mesh_cc(poisson,dt,2);
            t_cum.stop();

            #ifndef NDEBUG
            std::ofstream vertexs("data_vertex1.data");
            for(int j=0;j<Ny+1;++j){
                for(int i=0;i<Nx+1;++i){
                    vertexs << vs(i,j).x << " " << vs(i,j).y << endl;
                }
            }
            #endif

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals(in.data(false),aux.data(false),1);
            t_ci.stop();

            t_f.start();
            finalize(in.data(false),aux.data(false),1);
            t_f.stop();

            #ifndef NDEBUG
            std::ofstream output("data_out"+std::to_string(ii+1)+".data");
            double mass = compute_mass_and_write_legendre<o>(ax,ay,hx,hy,Nx,Ny,
                                                    in.data(false),output, 2);
            cout << "mass diff: " << mass-mass0 << endl;
            #endif

        }

        aux.swap(in);

        cout << "Timings in milliseconds, longer execution due to writing to file\n";
        cout << "solve poisson and differentiate: " << t_poisson.total() << endl
             << "get vertex star: " << t_cum.total() << endl
             << "get outer segments: " << t_gos.total() << endl
             << "get inner segments: " << t_gis.total() << endl
             << "complete outer segm: " << t_cos.total() << endl
             << "compute adjoint sol: " << t_cas.total() << endl
             << "compute integrals: " << t_ci.total() << endl
             << "compute phi and sum: " << t_f.total() << endl
             << "total: " << t_poisson.total()+t_cum.total()+t_gos.total()+
                             t_gis.total()+ t_cos.total()+t_ci.total()+t_f.total() +
                             t_cas.total() << endl;

    }

};



template<size_t o>
struct sldg2d_legendre_cpu{

    static constexpr size_t Nloc = o*(o+1)/2;
    static constexpr size_t e_stride = (o==1) ? 1 : o-1;

    double ax, ay;
    double bx, by;
    double hx, hy;
    int Nx, Ny;

    double weight[6] = {1.0,12.0,12.0,180.0,144.0,180.0};
  
    double gauss_x2[2] = {(1.0-0.5773502691896258)*0.5,
                          (1.0+0.5773502691896258)*0.5};
    double gauss_x3[3] = {(1.0-0.7745966692414834)*0.5,
                          (1.0+0.0)*0.5,
                          (1.0+0.7745966692414834)*0.5};
    double gauss_w3[3] = {0.555555555555555,
                          0.8888888888888889,
                          0.555555555555555};

    ext_vect<vertex_star> vs;
    ext_vect<face<o>> face_lr;
    ext_vect<face<o>> face_bt;
    ext_vect<element_star<o>> es;
    ext_vect<leg_adjoint_solution<o>> as;
    ext_vect<leg_all_integrals<o>> ai;

    sldg2d_legendre_cpu(double _ax, double _ay, double _bx, 
                        double _by, int _Nx, int _Ny)
        : ax{_ax}, ay{_ay}, bx{_bx}, by{_by}, Nx{_Nx}, Ny{_Ny}
    {
        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        vs.resize(e_stride*Nx+1,e_stride*Ny+1);

        face_lr.resize(Nx,Ny+1);
        face_bt.resize(Nx+1,Ny);

        es.resize(Nx,Ny);
        as.resize(Nx,Ny);
        ai.resize(Nx,Ny);

    } 

    ~sldg2d_legendre_cpu(){

    }


    template<typename F1, typename F2>
    void compute_upstream_mesh_RK(F1 a, F2 b, double dt, double t){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);

        leg_compute_upstream_mesh_RK_k<o>(Nx, Ny, ax, ay, hx, hy, hx_fine, 
                                      hy_fine, a, b, dt, t, vs);
    }


    //TODO
    template<typename T>
    void compute_upstream_mesh_cc(T& F, double dt, int stage){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);

        for(int j=0;j<e_stride*(Ny+1);++j){
            for(int i=0;i<e_stride*(Nx+1);++i){

                double x = ax+i*hx_fine;
                double y = ay+j*hy_fine;

                if(stage==1)
                    F.compute1(x,y,dt,i,j);
                if(stage==2)
                    F.compute2(x,y,dt,i,j);

                vs(i,j).set(x,y,ax,ay,hx,hy);

            }
        }
 
    }


    void get_outer_segments(){

        leg_get_outer_segments_k<o>(Nx, Ny, face_lr, face_bt, vs, ax, ay, hx, hy);
    }


    void get_inner_segment_points(){

        leg_get_inner_segmen_points_k<o>(Nx, Ny, es, vs, face_lr, face_bt,
                                     ax, ay, hx, hy);
    }


    void complete_outer_segments(){

        leg_complete_outer_segments_k<o>(Nx, Ny, face_lr, face_bt);

    }


    void compute_adjoint_solutions(){

        leg_compute_adjoint_solutions_k<o>(Nx, Ny, vs, es, as, hx, hy);

    }


    void compute_integrals(){

        leg_compute_integrals_precomp_f<o>(Nx, Ny, ai, face_lr, face_bt, es, 
                          as, ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);

       
    }


    void finalize(double* in, double* out, int Nz){

        leg_finalize_k<o>(in, out, Nz, Nx, Ny, ai, weight);

    }


    template<typename F1, typename F2>
    void translate(generic_container<double>& in, generic_container<double>& out, 
                   F1 a, F2 b, double dt, double final_T, int Nz){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T 
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        timer t_RK, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        double t=0.0;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_RK.start();
            compute_upstream_mesh_RK(a,b,dt,t);
            t_RK.stop();

            #ifndef NDEBUG
            std::ofstream vertex("data_vertex.data");
            for(int j=0;j<e_stride*Ny+1;++j){
                for(int i=0;i<e_stride*Nx+1;++i){
                    vertex << std::setprecision(16);
                    vertex << vs(i,j).x << " " << vs(i,j).y << endl;
                }
            }
            #endif

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals();
            t_ci.stop();

            t_f.start();
            finalize(in.data(false),out.data(false),Nz);
            t_f.stop();

            #ifndef NDEBUG
            std::ofstream output("data_out"+std::to_string(ii+1)+".data");
            compute_mass_and_write_legendre<o>(ax, ay, hx, hy, Nx, Ny,
                                      out.data(false), output, 2);
            #endif

            in.swap(out);

        }

        in.swap(out);

        //cout << "Timings in milliseconds:\n";
        //cout << "RK: " << t_RK.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "perform the sum: " << t_f.total() << endl
        //     << "total: " << t_RK.total()+t_gos.total()+t_gis.total()+
        //                     t_cos.total()+t_ci.total()+t_f.total()+
        //                     t_cas.total()<<endl;

    }


    template<typename T, typename f>
    void translate_nl(generic_container<double>& in, 
                      generic_container<double>& out,
                      generic_container<double>& rk_stage, T& poisson,
                      double final_T, double dt, double c, bool divide, f g, 
                      double UNUSED(mass0)){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T 
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        double t=0.0;
        timer t_poisson, t_cum, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_poisson.start();
            poisson.solve_legendre(in.data(false),c,g,divide,ii);
            t_poisson.stop();

            t_cum.start();
            compute_upstream_mesh_cc(poisson,dt,1);
            t_cum.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();


            t_ci.start();
            compute_integrals();
            t_ci.stop();

            t_f.start();
            finalize(in.data(false),rk_stage.data(false),1);
            t_f.stop();

            #ifndef NDEBUG
            std::ofstream output1("data_aux"+std::to_string(ii+1)+".data");
            compute_mass_and_write_legendre<o>(ax, ay, hx, hy, Nx, Ny,
                                      rk_stage.data(false), output1,2);
            #endif

            //order 2
            t_poisson.start();
            poisson.solve_legendre(rk_stage.data(false),c,g,divide,-1);
            t_poisson.stop();

            t_cum.start();
            compute_upstream_mesh_cc(poisson,dt,2);
            t_cum.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals();
            t_ci.stop();

            t_f.start();
            finalize(in.data(false),out.data(false),1);
            t_f.stop();

            //#ifndef NDEBUG
            //std::ofstream output("data_out"+std::to_string(ii+1)+".data");
            //double mass = compute_mass_and_write_legendre<o>(ax,ay,hx,hy,Nx,Ny,
            //                                        in.data(false),output, 2);
            //cout << "mass diff: " << mass-mass0 << endl;
            //#endif

            in.swap(out);
        }

        in.swap(out);

        //cout << "Timings in milliseconds\n";
        //cout << "solve poisson and differentiate: " << t_poisson.total() << endl
        //     << "get vertex star: " << t_cum.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "compute phi and sum: " << t_f.total() << endl
        //     << "total: " << t_poisson.total()+t_cum.total()+t_gos.total()+
        //                     t_gis.total()+ t_cos.total()+t_ci.total()+t_f.total() +
        //                     t_cas.total() << endl;


    }


};


