#pragma once

#include <generic/common.hpp>
#include <algorithms/sldg2d/generic_sldg2d.hpp>
#include <complex>
#include <generic/fft.hpp>

using cmplx = std::complex<fp>;

struct fftw2d_in_3d {

    static constexpr size_t dx=3;

    vector<cmplx> data_hat;
    fftw_plan plan_forward, plan_inverse;

    int Nr;
    int Ntheta;
    int Nz;
    int len;

    fftw2d_in_3d(int _Nr, int _Ntheta, int _Nz,
                 fp* in, fp* out)
        : Nr{_Nr}, Ntheta{_Ntheta}, Nz{_Nz}
    {


        len = (Ntheta/2+1)*Nz;

        data_hat.resize(len*Nr);

        // Note that if FFTW_ESTIMATE is not provided the input and output arrays
        // might be overriden.
        int n[2] = {Nz, Ntheta}; // row major order
        int onembed[2] = {Nz, Ntheta/2+1};

        plan_forward = fftw_plan_many_dft_r2c(2, n, Nr, in,
                n, Nr, 1, (fftw_complex*)data_hat.data(), onembed, Nr, 1, 
                FFTW_PRESERVE_INPUT | FFTW_ESTIMATE);

        plan_inverse = fftw_plan_many_dft_c2r(2, n, Nr, 
                (fftw_complex*)data_hat.data(), onembed, Nr, 1, 
                out, n, Nr, 1, FFTW_ESTIMATE);
    }


    ~fftw2d_in_3d(){
        fftw_destroy_plan(plan_forward);
        fftw_destroy_plan(plan_inverse);
    }

    
    //modifies data_hat
    void compute_derivative(int dim, array<fp,3> a, array<fp,3> b){

        if(dim<2||dim>3){
            cout << "only derivative in theta or z allowed" << endl;
            exit(1);
        }

        fp normalization = 1.0/(Ntheta*Nz);

        for(int iz=0;iz<Nz;++iz){
            fp freq_z = ((iz<Nz/2+1) ? iz : -(Nz-iz))*2.0*M_PI/(b[2]-a[2]);
            for(int it=0;it<Ntheta/2+1;++it){
                fp freq_t = it*2.0*M_PI/(b[1]-a[1]);
                for(int ir=0;ir<Nr;++ir){

                    cmplx factor = {0, (dim==2) ? freq_t : freq_z };

                    int lidx = ir + Nr*(it + (Ntheta/2+1)*iz);

                    data_hat[lidx] *= factor*normalization;

                }
            }
        }

    }

};


extern "C" {
    //solve tridiagonal symmetric linear system

    //double precision
    extern void zptsv_(int* N, int* NRHS, double* D, cmplx* E, 
                       cmplx* RHS, int* LDB, int* INFO);
    extern void dptsv_(int* N, int* NRHS, double* D, double* E, 
                       double* RHS, int* LDB, int* INFO);

    //single precision
    extern void cptsv_(int* N, int* NRHS, float* D, cmplx* E, 
                       cmplx* RHS, int* LDB, int* INFO);
    extern void sptsv_(int* N, int* NRHS, float* D, float* E, 
                       float* RHS, int* LDB, int* INFO);
}


struct fd1dsolver_cc{

    static constexpr size_t dx = 3;

    int N;

    fp a;
    fp b;
    fp hr;
    fp inv_hsq;

    fp (*n0r)(fp);
    fp (*alpha)(fp, int, int);

    vector<fp> diag;
    vector<cmplx> off;

    fd1dsolver_cc(int _N, fp _a, fp _b, fp (*_n0r)(fp),
                  fp (*_alpha)(fp,int,int))
        : N{_N-2}, a{_a}, b{_b}, n0r{_n0r}, alpha{_alpha}
    {
        //N = N-2!! system has to points less, but h is the same
        hr = (b-a)/fp(N+1);
        a += hr;
        diag.resize(N);
        off.resize(N-1);
        inv_hsq = 1.0/pow(hr,2);

        #ifndef USE_SINGLE_PRECISION
        cout << "double precision used to solve qne" << endl;
        #else
        cout << "single precision used to solve qne" << endl;
        #endif

    }


    void solve_lin_system(fp* rhs, fp (*D)(fp), 
                          fp (*L)(fp) ) {

        vector<fp> ldiag(N);
        vector<fp> loff(N-1);
    
        // main diagonal
        for(int i=0;i<N;i++){
            fp r    = a + i*hr;
            fp rp12 = a + (i+0.5)*hr;
            fp rm12 = a + (i-0.5)*hr;
    
            ldiag[i] = (D(rp12)+D(rm12))*inv_hsq
                      + L(r);
        }
    
        // off diagonal
        for(int i=0;i<N-1;i++) {
            fp r32 = a + (i+0.5)*hr;
            loff[i] = -1.0*D(r32)*inv_hsq;
        }

        int nrhs=1;
        int info; 
        #ifndef USE_SINGLE_PRECISION
        dptsv_(&N, &nrhs, ldiag.data(), loff.data(), rhs+1, &N, &info);
        #else
        sptsv_(&N, &nrhs, ldiag.data(), loff.data(), rhs+1, &N, &info);
        #endif

        //homogeneous dirichlet bc
        rhs[0] = 0.0;
        rhs[N+1] = 0.0;
    
    }


    void solve_lin_system(cmplx* rhohat, fp normalization, int k, int j) {

        // We have to multiply the 1/(n0 r) in front of the Laplacian -1/(n0
        // r) dr(no r dr Phi) to the other side of the equation as otherwise
        // we do not obtain a symmetric matrix (which is required for the
        // used Lapack routine). The n_0 coefficient is absorbed into the
        // function compute_rho. But we still have to normalize the Fourier
        // coefficients.
        #pragma omp parallel
        {
            #pragma omp for schedule(static)
            for(int i=1;i<N+1;i++){
                rhohat[i] *= normalization;
            }
    
            // main diagonal
            #pragma omp for schedule(static)
            for(int i=0;i<N;i++){
                fp r    = a + i*hr;
                fp rp12 = a + (i+0.5)*hr;
                fp rm12 = a + (i-0.5)*hr;
    
                diag[i] = (n0r(rp12)+n0r(rm12))*inv_hsq + alpha(r,k,j);

            }
    
            // off diagonal
            #pragma omp for schedule(static)
            for(int i=0;i<N-1;i++){
                fp r32 = a + (i+0.5)*hr;
                off[i] = -1.0*n0r(r32)*inv_hsq;
            }
        }

        int nrhs=1;
        int info; 
        #ifndef USE_SINGLE_PRECISION
        zptsv_(&N, &nrhs, diag.data(), off.data(), rhohat+1, &N, &info);
        #else
        cptsv_(&N, &nrhs, diag.data(), off.data(), rhohat+1, &N, &info);
        #endif

        //homogeneous dirichlet bc
        rhohat[0] = 0.0;
        rhohat[N+1] = 0.0;
    }


};


enum class interpolation_type{dg2equi,equi2dg};
 

template<size_t o2d, size_t oz>
struct quasi_neutrality {

    static constexpr size_t dx = 3;
    static constexpr size_t Nloc = o2d*o2d;

    array<fp,dx> a;
    array<fp,dx> b;
    array<fp,dx> h;

    int Nr, Ntheta, Nz;

    generic_container<fp> rho;
    generic_container<fp> rho_cc;

    generic_container<fp> phi;
    generic_container<fp> phi_cc;

    generic_container<fp> dr_phi;
    generic_container<fp> dr_phi_cc;

    generic_container<fp> dtheta_phi;
    generic_container<fp> dtheta_phi_cc;

    multi_array<fp,2*dx> dz_phi;
    generic_container<fp> dz_phi_cc;
    
    unique_ptr<fftw2d_in_3d> fft;

    unique_ptr<fd1dsolver_cc> ode_fd;

    double electric_energy;

    quasi_neutrality(array<fp,dx> _a, array<fp,dx> _b,  
                     int _Nr, int _Ntheta, int _Nz,
                     fp (*K)(fp), fp (*alpha)(fp,int,int))
        : a{_a}, b{_b}, Nr{_Nr}, Ntheta{_Ntheta}, Nz{_Nz}
    {

        rho.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        rho_cc.resize((Nr+1)*Ntheta*Nz*oz);
        
        phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dr_phi.resize(Nr*Ntheta*Nloc*Nz*oz);
        dr_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dtheta_phi.resize(Nr*Ntheta*Nloc*Nz*oz);
        dtheta_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dz_phi.resize({Nr,Ntheta,Nz,o2d,o2d,oz});
        dz_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);
        
        h[0] = (b[0]-a[0])/fp(Nr);
        h[1] = (b[1]-a[1])/fp(Ntheta);
        h[2] = (b[2]-a[2])/fp(Nz*oz);

        fft = make_unique<fftw2d_in_3d>(Nr+1,Ntheta,Nz*oz, 
                                        rho_cc.h_data.data(), 
                                        phi_cc.h_data.data());

        ode_fd = make_unique<fd1dsolver_cc>(Nr+1, a[0], b[0], K, alpha);

    }


    void solve(){

        fp normalization = 1.0/(Nz*oz*Ntheta);

        //interpolate rhs from dg to equidistant grid in z and e
        //evaluate at cell corners, which are equidistant, in order to 
        //apply fft

        gt::start("qns_z_dg2equi");
        interpolate_z_dg_to_equi(rho.data(false),rho.data(false));
        gt::stop("qns_z_dg2equi");

        gt::start("qns_dg2cc");
        evaluate_at_grid_corners(rho.data(false),rho_cc.data(false));
        gt::stop("qns_dg2cc");

        gt::start("qns_fft");
        fftw_execute(fft->plan_forward);
        gt::stop("qns_fft");

        //for earch theta,z solve the linear 1d system
        gt::start("qns_solve");
        for(int i=0;i<Nz*oz;++i){
            for(int j=0;j<Ntheta/2+1;++j){
                ode_fd->solve_lin_system(&fft->data_hat[(Nr+1)*(j+i*(Ntheta/2+1))], 
                                         normalization,j,i);
            }
        }
        gt::stop("qns_solve");

        gt::start("qns_fft");
        fftw_execute(fft->plan_inverse);
        gt::stop("qns_fft");

        gt::start("qns_compute_ee");
        compute_electric_energy();
        gt::stop("qns_compute_ee");

        //compute derivatives in r, theta and z, TODO: do with one function
        gt::start("qns_derivative");
        compute_derivative(phi_cc.data(false),dr_phi_cc.data(false),0);
        compute_derivative(phi_cc.data(false),dtheta_phi_cc.data(false),1);
        compute_derivative(phi_cc.data(false),dz_phi_cc.data(false),2);
        gt::stop("qns_derivative");

        //interpolate all three to dg_coordinates
        //first, interpolate z from equi to dg
        gt::start("qns_z_equi2dg");
        interpolate_z_equi_to_dg(dr_phi_cc.data(false), dr_phi_cc.data(false));
        interpolate_z_equi_to_dg(dtheta_phi_cc.data(false), 
                                 dtheta_phi_cc.data(false));
        interpolate_z_equi_to_dg(dz_phi_cc.data(false), dz_phi_cc.data(false));
        interpolate_z_equi_to_dg(phi_cc.data(false), phi_cc.data(false));
        gt::stop("qns_z_equi2dg");

        //second, interpolate r,theta from cc to dg
        gt::start("qns_z_cc2dg");
        interpolate_cc_to_dg(dr_phi_cc.data(false), dr_phi.data(false));
        interpolate_cc_to_dg(dtheta_phi_cc.data(false), dtheta_phi.data(false));
        interpolate_cc_to_dg(dz_phi_cc.data(false), dz_phi.data());
        interpolate_cc_to_dg(phi_cc.data(false), phi.data(false));
        gt::stop("qns_z_cc2dg");

    }


    void interpolate_z_dg_to_equi(fp* in, fp* out){

        vector<fp> nodes = gauss::all_x_scaled01(oz);

        int stride = Nr*Ntheta*Nloc;

        #pragma omp parallel for schedule(static)
        for(int ijm=0;ijm<Nr*Ntheta*Nloc;++ijm){
            for(int i=0;i<Nz;++i){

                int offset = ijm;

                fp cell[oz];

                for(size_t j=0;j<oz;++j){
                    int idx = j+oz*i;
                    cell[j] = in[offset + stride*idx];
                }

                for(size_t j=0;j<oz;++j){

                    fp x = (1.0+2.0*j)/(2.0*fp(oz));
                    fp value = evaluate(x,cell,nodes.data());

                    int idx = j+oz*i;
                    out[offset+stride*idx] = value;
                }
            }
        }
    }


    fp evaluate(fp x, fp* values, fp* nodes){

        fp result{0.0};

        for(size_t i=0;i<oz;++i){

            fp lagrange=1.0;
            for(size_t j=0;j<i;++j)
                lagrange *= (x-nodes[j])/(nodes[i]-nodes[j]);
            for(size_t j=i+1;j<oz;++j)
                lagrange *= (x-nodes[j])/(nodes[i]-nodes[j]);

            result += values[i]*lagrange;
        }

        return result;
    }


    fp basisf(fp x, fp y, int i, int j){
        
        if(o2d==1)
            return 1.0;
    
        if(o2d==2){
            return (x-gauss::x_scaled01(1-i,2))*(y-gauss::x_scaled01(1-j,2))*
                   (3.0*(2.0*i-1)*(2.0*j-1));

        }
    }


    void evaluate_at_grid_corners(fp* rho_leg, fp* rho_cc){

        std::fill_n(rho_cc, (Nr+1)*Ntheta*Nz*oz, 0.0);
        vector<fp> nodes = gauss::all_x_scaled01(o2d);

        #pragma omp parallel for schedule(static)
        for(int l=0;l<Nz*oz;++l){

            int offset_in = Nr*Ntheta*Nloc*l;
            fp* in = rho_leg + offset_in;
            int offset_out = (Nr+1)*Ntheta*l;

            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){

                    for(int jj=0;jj<o2d;++jj){
                        for(int ii=0;ii<o2d;++ii){

                            fp v_in = in[ii+o2d*jj + Nloc*(i+j*Nr)];
                            
                            fp v_bl = v_in*basisf(0.0,0.0,ii,jj);
                            fp v_br = v_in*basisf(1.0,0.0,ii,jj);
                            fp v_tl = v_in*basisf(0.0,1.0,ii,jj);
                            fp v_tr = v_in*basisf(1.0,1.0,ii,jj);

                            //v_bl += v_in*basisf(0.0,0.0,ii,jj)*0.5;
                            //v_br += v_in*basisf(1.0,0.0,ii,jj)*0.5;
                            //v_tl += v_in*basisf(0.0,1.0,ii,jj)*0.5;
                            //v_tr += v_in*basisf(1.0,1.0,ii,jj)*0.5;

                            fp factor = (i==0) ? 0.5 : 0.25;
                            int idx_out = i + (Nr+1)*j + offset_out;
                            rho_cc[idx_out] += v_bl*factor;

                            factor = (i+1==Nr) ? 0.5 : 0.25;
                            idx_out = i+1 + (Nr+1)*j + offset_out;
                            rho_cc[idx_out] += v_br*factor;

                            int idx_j = (j==Ntheta-1) ? 0 : j+1;
                            factor = (i==0) ? 0.5 : 0.25;
                            idx_out = i + (Nr+1)*idx_j + offset_out;
                            rho_cc[idx_out] += v_tl*factor;

                            factor = (i+1==Nr) ? 0.5 : 0.25;
                            idx_out = i+1 + (Nr+1)*idx_j + offset_out;
                            rho_cc[idx_out] += v_tr*factor;

                        }
                    }
                }
            }
        }
    }


    void compute_derivative(fp* in, fp* out, const int dim){

        fp inv_2h = 1.0/(2.0*h[dim]);

        #pragma omp parallel for schedule(static)
        for(int k=0;k<Nz*oz;++k){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    int idx = i+(Nr+1)*(j+Ntheta*k);

                    if(dim==0){
                        int offset = 1;
                        if(i==0){
                            out[idx] = (-3.0*in[idx]+4.0*in[idx+1]-in[idx+2])*
                                       inv_2h;
                        }
                        else if(i==Nr){
                            out[idx] = (3.0*in[idx]-4.0*in[idx-1]+in[idx-2])*
                                       inv_2h;
                        }
                        else{
                            out[idx] = (in[idx+offset]-in[idx-offset])*inv_2h;
                        }

                        //This does not solve the problem on the g formulation
                        //int idxp1 = idx+offset;
                        //int idxm1 = idx-offset;
                        //if(i==0)    idxm1 = idx+(Nr-1)*offset;
                        //if(i==Nr-1) idxp1 = idx-(Nr-1)*offset;

                        //out[idx] = (in[idxp1]-in[idxm1])*inv_2h;
 
                    }
                    if(dim==1){
                        int offset = Nr+1;
                        int idxp1 = idx+offset;
                        int idxm1 = idx-offset;
                        if(j==0)        idxm1 = idx+(Ntheta-1)*offset;
                        if(j==Ntheta-1) idxp1 = idx-(Ntheta-1)*offset;

                        out[idx] = (in[idxp1]-in[idxm1])*inv_2h;
                    }
                    if(dim==2){
                        int offset = Ntheta*(Nr+1);
                        int idxp1 = idx+offset;
                        int idxm1 = idx-offset;;
                        if(k==0)       idxm1 = idx+(Nz*oz-1)*offset;
                        if(k==Nz*oz-1) idxp1 = idx-(Nz*oz-1)*offset;

                        out[idx] = -(in[idxp1]-in[idxm1])*inv_2h;
                    }
                }
            }
        }
    }


    void interpolate_z_equi_to_dg(fp* in, fp* out){

        vector<fp> nodes(oz);
        for(size_t i=0;i<oz;++i)
            nodes[i] = (1.0+2.0*i)/(2.0*fp(oz));

        int stride = (Nr+1)*Ntheta;

        #pragma omp parallel for schedule(static)
        for(int ij=0;ij<(Nr+1)*Ntheta;++ij){
            for(int i=0;i<Nz;++i){

                int offset = ij;

                fp cell[oz];

                for(size_t j=0;j<oz;++j){
                    int idx = j+oz*i;
                    cell[j] = in[offset + stride*idx];
                }

                for(size_t j=0;j<oz;++j){

                    fp x = gauss::x_scaled01(j,oz);
                    fp value = evaluate(x,cell,nodes.data());

                    int idx = j+oz*i;
                    out[offset+stride*idx] = value;
                }
            }
        }
    }


    void interpolate_cc_to_dg(fp* in, fp* out){

        #pragma omp parallel for schedule(static)
        for(int k=0;k<Nz*oz;++k){

            int offset_out = k*Nloc*Nr*Ntheta;
            int offset_in  = k*(Nr+1)*Ntheta;

            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){

                    fp cc[4];
                    for(int jj=0;jj<2;++jj){
                        int idx_j = (j+jj==Ntheta) ? 0 : j+jj;
                        for(int ii=0;ii<2;++ii){
                            cc[ii+2*jj] = in[i+ii + idx_j*(Nr+1) +
                                             offset_in];
                        }
                    }

                    for(int jj=0;jj<o2d;++jj){
                        for(int ii=0;ii<o2d;++ii){

                            fp x = gauss::x_scaled01(ii,o2d);
                            fp y = gauss::x_scaled01(jj,o2d);

                            fp v = cc[0]*(1.0-x)*(1.0-y) + 
                                       cc[1]*(x-0.0)*(1.0-y) +
                                       cc[2]*(1.0-x)*(y-0.0) +
                                       cc[3]*(x-0.0)*(y-0.0);

                            out[ii+o2d*jj + Nloc*(i+j*Nr) + offset_out] = v;

                        }
                    }
                }
            }
        }
    }


    void follow_characteristics_cc1(fp& r, fp& theta, fp dt, 
                                    int i, int j, int k){

        int lidx = i+(Nr+1)*((j%Ntheta)+Ntheta*k);
        fp rr = a[0] + h[0]*i;
        r     += dt*dtheta_phi_cc.h_data[lidx]/rr;
        theta -= dt*dr_phi_cc.h_data[lidx]/rr;
    }


    void follow_characteristics_dg1(fp& r, fp& theta, fp dt, 
                                    int ii, int jj, int i, int j, int k){

        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*k)));
        fp rr = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
        r     += dt*dtheta_phi.h_data[lidx]/rr;
        theta -= dt*dr_phi.h_data[lidx]/rr;
    }
    

    void compute_electric_energy(){

        int r_np_idx = Nr/2;
        fp hh = h[1]*h[2];

        double integral=0.0;

        #pragma omp parallel for schedule(static) reduction(+:integral)
        for(int k=0;k<Nz*oz;++k){
            for(int j=0;j<Ntheta;++j){

                integral += hh*pow(phi_cc.h_data[r_np_idx + (Nr+1)*(j+Ntheta*k)],2);

            }
        }

        electric_energy = sqrt(integral);
    }
};


//template<size_t o2d, size_t oz, size_t ov>
//double compute_rho(double* in,  double* rho,
//                   array<double,4> a, array<double,4> b, array<Index,4> e,
//                   double (*n0r)(double), double perturbation) {
//
//    int Nr = e[0];
//    int Ntheta = e[1];
//    int Nv = e[2];
//    int Nz = e[3];
//
//    double hr = (b[0]-a[0])/double(Nr);
//    double ht = (b[1]-a[1])/double(Ntheta);
//    double hv = (b[2]-a[2])/fp(Nv);
//    double hz = (b[3]-a[3])/double(Nz);
//
//    if(!perturbation){
//        for(int k=0;k<Nz;++k){
//            for(int kk=0;kk<oz;++kk){
//                for(int j=0;j<Ntheta;++j){
//                    for(int i=0;i<Nr;++i){
//                        for(int jj=0;jj<o2d;++jj){
//                            for(int ii=0;ii<o2d;++ii){
//
//                                int lidx_rho = ii+o2d*(jj+o2d*(i+Nr*
//                                               (j+Ntheta*(kk+oz*k))));
//
//                                double r = a[0] + hr*(i+gauss::x_scaled01(ii,o2d));
//
//                                rho[lidx_rho] = -n0r(r);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    } else
//        std::fill_n(rho,Nr*Ntheta*o2d*o2d*Nz*oz,0.0);
//
//    double mass = 0.0;
//    #pragma omp parallel for schedule(static) reduction(+:mass)
//    for(int l=0;l<Nz;++l){
//    for(int ll=0;ll<oz;++ll){
//        for(int k=0;k<Nv;++k){
//        for(int kk=0;kk<ov;++kk){
//            for(int j=0;j<Ntheta;++j){
//                for(int i=0;i<Nr;++i){
//                    for(int jj=0;jj<o2d;++jj){
//                    for(int ii=0;ii<o2d;++ii){
//
//                        int lidx_full = ii+o2d*(jj+o2d*(i+Nr*
//                                        (j+Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));
//
//                        int lidx_rho = ii+o2d*(jj+o2d*(i+Nr*
//                                       (j+Ntheta*(ll+oz*l))));
//
//                        rho[lidx_rho] += in[lidx_full]*hv*gauss::w(kk,ov)*0.5;
//                        mass += in[lidx_full]*hr*0.5*gauss::w(ii,o2d)*
//                                              ht*0.5*gauss::w(jj,o2d)*
//                                              hv*0.5*gauss::w(kk,ov)*
//                                              hz*0.5*gauss::w(ll,oz);
//
//                    }
//                    }
//                }
//            }
//        }
//        }
//    }
//    }
//
//    return mass;
//}

