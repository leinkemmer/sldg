#pragma once

#include <utility>
#include <algorithm>
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <generic/common.hpp>

enum class bc{periodic, dirichlet, homogeneous_dirichlet};
enum class basis_type{legendre, lagrange};

#define UNUSED(x)   x##_UNUSED __attribute__((unused))

//#ifdef __CUDACC__
//#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
//inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
//{
//   if (code != cudaSuccess) 
//   {
//      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
//      if (abort) exit(code);
//   }
//}
//#endif

struct timer_{

    std::chrono::system_clock::time_point begin;
    std::chrono::system_clock::time_point end;
    double tot=0.0;

    void start(){
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        begin = std::chrono::high_resolution_clock::now();
    }

    void stop(){
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> ms_double = end - begin;
        tot += ms_double.count();
    }

    double total(){
        return tot;
    }

    void reset(){
        tot=0.0;
    }

};


//namespace gauss {
//
//    double x(int i, int o);
//    double x_scaled01(int i, int o);
//    double w(int i, int o);
//
//    inline vector<double> all_weights(int o) {
//        vector<double> ws(o);
//        for(int i=0;i<o;i++)
//            ws[i] = w(i,o);
//        return ws;
//    }
//
//    inline vector<double> all_x_scaled01(int o) {
//        vector<double> xs(o);
//        for(int i=0;i<o;i++)
//            xs[i] = x_scaled01(i,o);
//        return xs;
//    }
//}


template<int N>
#ifdef __CUDACC__
__host__ __device__
#endif
void LUPDecompose(double A[N][N], double Tol, int P[N]) {

    for(int i=0;i<N;++i)
        P[i]=i;

    int imax{};
    for(int i=0;i<N;++i) {
        double maxA=0.0;
        imax=i;

        for (int k=i;k<N;++k){
            double absA=fabs(A[k][i]);
            if(absA > maxA) {
                maxA = absA;
                imax = k;
            }
        }

        #ifndef NDEBUG
        //should this be relative?
        if(Tol>maxA){
            #ifndef __CUDACC__
            std::cout << "WARNING: matrix close to be singular\n";
            #endif
        }
        #endif

        if(imax!=i) {
            //pivoting P
            int j = P[i];
            P[i] = P[imax];
            P[imax] = j;

            //pivoting rows of A
            for(int r=0;r<N;++r){
                double tmp = A[i][r];
                A[i][r] = A[imax][r];
                A[imax][r] = tmp;
            }
        }

        for(int j=i+1;j<N;++j) {
            A[j][i] /= A[i][i];

            for(int k=i+1;k<N;++k)
                A[j][k] -= A[j][i] * A[i][k];
        }
    }
}


template<int N>
#ifdef __CUDACC__
__host__ __device__
#endif
void LUPSolve(double A[N][N], int P[N],
              const double* b, double x[N]) {

    for(int i=0;i<N;++i) {
        x[i] = b[P[i]];

        for(int k=0;k<i;++k)
            x[i] -= A[i][k]*x[k];
    }

    for(int i=N-1;i>=0;i--) {
        for(int k=i+1;k<N;k++)
            x[i] -= A[i][k]*x[k];

        x[i] /= A[i][i];
    }
}


template<typename T>
struct ext_vect{

    int N1, N2;
    int batch;
    vector<T> h_data;
    T* d_data = nullptr;

    ext_vect() = default;

    ext_vect(int _N1, int _N2, bool initialize_on_host=true, int _batch=1)
        : N1{_N1}, N2{_N2}, batch{_batch}
    {
        #ifdef __CUDACC__
        gpuErrchk(cudaMalloc(&d_data,N1*N2*batch*sizeof(T)));
        #endif
        if(initialize_on_host)
            h_data.resize(N1*N2*batch);
    }


    #ifdef __CUDACC__
    ~ext_vect(){
        gpuErrchk(cudaFree(d_data));
    }
    #endif


    T* data(bool gpu){
        if(gpu)
            return d_data;
        else
            return h_data.data();
    }


    void resize(int _N1, int _N2, bool initialize_on_host=true, int _batch=1){
        N1 = _N1;
        N2 = _N2;
        batch = _batch;
        #ifdef __CUDACC__
        if(d_data != nullptr)
            gpuErrchk(cudaFree(d_data));
        gpuErrchk(cudaMalloc(&d_data,N1*N2*batch*sizeof(T)));
        #endif
        if(initialize_on_host)
            h_data.resize(N1*N2*batch);
    }


    T& operator()(int i, int j){
        return h_data[i+j*N1];
    }


    void swap(ext_vect<T>& rhs){
        if(rhs.N1==N1 && rhs.N2==N2 && rhs.batch==batch){
            h_data.swap(rhs.h_data);
            std::swap(d_data,rhs.d_data);
        }
    }

    #ifdef __CUDACC__
    void copy_to_host(){
        gpuErrchk(cudaMemcpy(h_data.data(),d_data,N1*N2*batch*sizeof(T),
                             cudaMemcpyDeviceToHost));
    }

    void copy_to_gpu(){
        gpuErrchk(cudaMemcpy(d_data,h_data.data(),N1*N2*batch*sizeof(T),
                             cudaMemcpyHostToDevice));
    }
    #endif
};


template<typename T>
struct generic_container{

    int N;
    vector<T> h_data;
    T* d_data = nullptr;

    generic_container() = default;

    generic_container(int _N)
        : N{_N}
    {
        #ifdef __CUDACC__
        gpuErrchk(cudaMalloc(&d_data,N*sizeof(T)));
        #endif
        h_data.resize(N);
    }


    #ifdef __CUDACC__
    ~generic_container(){
        gpuErrchk(cudaFree(d_data));
    }
    #endif


    T* data(bool gpu){
        if(gpu)
            return d_data;
        else
            return h_data.data();
    }


    void swap(generic_container<T>& rhs){
        if(N==rhs.N){
            h_data.swap(rhs.h_data);
            std::swap(d_data,rhs.d_data);
        }
    }

    void resize(int _N){
        N = _N;
        #ifdef __CUDACC__
        if(d_data != nullptr)
            gpuErrchk(cudaFree(d_data));
        gpuErrchk(cudaMalloc(&d_data,N*sizeof(T)));
        //cout << "gpu malloc generic container" 
        //     << N*sizeof(T)*1e-9 << " GB" << endl;
        #endif
        h_data.resize(N);
    }

    T* begin(){
        return h_data.begin();
    }

    T* end(){
        return h_data.end();
    }

    #ifdef __CUDACC__
    void copy_to_host(){
        gpuErrchk(cudaMemcpy(h_data.data(),d_data,N*sizeof(T),
                             cudaMemcpyDeviceToHost));
    }

    void copy_to_gpu(){
        gpuErrchk(cudaMemcpy(d_data,h_data.data(),N*sizeof(T),
                             cudaMemcpyHostToDevice));

    }
    #endif
};


/*
 * currently not used. If this is required, change from vector to 
 * ext_vec<dg_coeff>&
 *
//currently only second order partial derivatives from third order
//approximation is implemented
template<size_t o_in=3>
void compute_partial_derivatives(vector<double>& in, vector<double>& dx,
                                 vector<double>& dy, int Nx, int Ny,
                                 double hx, double hy){

    if(o_in!=3){
        cout << "ERROR: o_in = " << o_in << " != 3. " << endl;
        exit(1);
    }

    constexpr size_t o_out = o_in-1;
    constexpr size_t Nloc_in = o_in*(o_in+1)/2;
    constexpr size_t Nloc_out = o_out*(o_out+1)/2;

    double inv_hx = 1.0/hx;
    double inv_hy = 1.0/hy;

    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            dx[0 + Nloc_out*(i+j*Nx)] = inv_hx*in[1+Nloc_in*(i+j*Nx)];
            dx[1 + Nloc_out*(i+j*Nx)] = inv_hx*in[3+Nloc_in*(i+j*Nx)]*2.0;
            dx[2 + Nloc_out*(i+j*Nx)] = inv_hx*in[4+Nloc_in*(i+j*Nx)];

            dy[0 + Nloc_out*(i+j*Nx)] = inv_hy*in[2+Nloc_in*(i+j*Nx)];
            dy[1 + Nloc_out*(i+j*Nx)] = inv_hy*in[4+Nloc_in*(i+j*Nx)];
            dy[2 + Nloc_out*(i+j*Nx)] = inv_hy*in[5+Nloc_in*(i+j*Nx)]*2.0;

        }
    }

}


template<size_t o>
void o_to_op1(double* rhs_o, double* rhs_op1, int Nx, int Ny){

    constexpr size_t Nloc_o = o*(o+1)/2;
    constexpr size_t Nloc_op1 = (o+1)*(o+2)/2;

    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){
            for(size_t k=0;k<Nloc_o;++k)
                rhs_op1[k+Nloc_op1*(i+j*Nx)] = rhs_o[k+Nloc_o*(i+j*Nx)];
            for(size_t k=Nloc_o;k<Nloc_op1;++k)
                rhs_op1[k+Nloc_op1*(i+j*Nx)] = 0.0;
        }
    }

}


template<size_t o=4>
void integrate_against_basis(double (*f0)(double,double),int i, int j,
                             double ax, double hx, double ay, double hy,
                             int Nx, double* out){
                                          
    //cout << "i,j: " << i << "," << j << endl;
                                          
    double r1=0;
    double rx=0;
    double ry=0;

    for(size_t jj=0;jj<o;++jj){                 
        for(size_t ii=0;ii<o;++ii){             
                                          
            double x = gauss::x_scaled01(ii,o);
            double y = gauss::x_scaled01(jj,o);

            //cout << "x: " << x << ", y: " << y << endl;
                                          
            r1 += f0((x+i)*hx+ax,(y+j)*hy+ay)*gauss::w(ii,o)*gauss::w(jj,o);
            rx += f0((x+i)*hx+ax,(y+j)*hy+ay)*gauss::w(ii,o)*gauss::w(jj,o)*(x-0.5);
            ry += f0((x+i)*hx+ax,(y+j)*hy+ay)*gauss::w(ii,o)*gauss::w(jj,o)*(y-0.5);
                                          
        }
    }                                     

    out[0+3*(i+Nx*j)] = r1*hx*hy*0.25;    
    out[1+3*(i+Nx*j)] = rx*hx*hy*0.25;
    out[2+3*(i+Nx*j)] = ry*hx*hy*0.25;    

}   
*/


inline void stupidfunc(){
    cout << "hoi" << max(1,2) << " " << min(1,2) << endl;
}
