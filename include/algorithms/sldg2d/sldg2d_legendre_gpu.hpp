#pragma once

#ifdef __CUDACC__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "legendre_f.hpp"


template<size_t o, typename F1, typename F2>
__global__
void leg_compute_upstream_mesh_RK_k(int Nxe, int Nye, vertex_star* vs,
                                double dt, double t, double ax, double ay,
                                double hx, double hy, double hx_fine,
                                double hy_fine, F1 a, F2 b){

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nxe || j>=Nye)
        return;

    compute_upstream_mesh_RK_f<o>(i,j,vs[i+j*Nxe],dt,t,ax,ay,hx,hy,
                                  hx_fine,hy_fine, a, b);

}


template<size_t o>
__global__
void leg_get_outer_segments_k(int Nx, int Ny, face<o>* face_lr, face<o>* face_bt,
                          vertex_star* vs, double ax, double ay, 
                          double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;
    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i<Nx && j<Ny+1)
        find_intersections_f(face_lr[i + j*Nx],
                             vs[i*e_stride + j*e_stride*(Nx*e_stride+1)],
                             vs[(i+1)*e_stride + j*e_stride*(Nx*e_stride+1)],
                             ax,ay,hx,hy,false);
    

    if(i<Nx+1 && j<Ny)
        find_intersections_f(face_bt[i + j*(Nx+1)],
                             vs[i*e_stride + j*e_stride*(Nx*e_stride+1)],
                             vs[i*e_stride + (j+1)*e_stride*(Nx*e_stride+1)],
                             ax,ay,hx,hy,false);

}


template<size_t o>
__global__
void leg_get_inner_segment_points_k(int Nx, int Ny, element_star<o>* es,
                                vertex_star* vs, face<o>* face_lr,
                                face<o>* face_bt, double ax, double ay,
                                double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nx || j>=Ny)
        return;

    int Nxe = e_stride*Nx+1;

    es[i+j*Nx].min_i = min(min(vs[e_stride*i + e_stride*j*Nxe].i,
                               vs[e_stride*(i+1) + e_stride*j*Nxe].i),
                           min(vs[e_stride*(i+1) + e_stride*(j+1)*Nxe].i,
                               vs[e_stride*i + e_stride*(j+1)*Nxe].i));
    es[i+j*Nx].min_j = min(min(vs[e_stride*i + e_stride*j*Nxe].j,
                               vs[e_stride*(i+1) + e_stride*j*Nxe].j),
                           min(vs[e_stride*(i+1) + e_stride*(j+1)*Nxe].j,
                               vs[e_stride*i + e_stride*(j+1)*Nxe].j));

    es[i+j*Nx].x_center = (vs[e_stride*i + e_stride*j*Nxe].x + 
                           vs[e_stride*(i+1) + e_stride*j*Nxe].x + 
                           vs[e_stride*(i+1) + e_stride*(j+1)*Nxe].x + 
                           vs[e_stride*i + e_stride*(j+1)*Nxe].x)*0.25;
    es[i+j*Nx].y_center = (vs[e_stride*i + e_stride*j*Nxe].y + 
                           vs[e_stride*(i+1) + e_stride*j*Nxe].y + 
                           vs[e_stride*(i+1) + e_stride*(j+1)*Nxe].y + 
                           vs[e_stride*i + e_stride*(j+1)*Nxe].y)*0.25;

    for(int ii=0;ii<MAX_CELLS;++ii){
        es[i+j*Nx].idx_h[ii]=0;
        es[i+j*Nx].idx_v[ii]=0;
    }

    find_inner_points_f(es[i+j*Nx], face_lr[i + j*Nx], 0);
    find_inner_points_f(es[i+j*Nx], face_bt[i+1 + j*(Nx+1)], 0);
    find_inner_points_f(es[i+j*Nx], face_lr[i + (j+1)*Nx], 1);
    find_inner_points_f(es[i+j*Nx], face_bt[i + j*(Nx+1)], 1);

    sort_and_complete_inner_f(es[i+j*Nx],ax,ay,hx,hy);

}


template<size_t o>
__global__
void leg_complete_outer_segments_k(int Nx, int Ny, 
                                   face<o>* face_lr, face<o>* face_bt){

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i<Nx && j<Ny+1){
        for(int k=0;k<face_lr[i+j*Nx].n_sub_outer;++k){
            get_idx_of_segment_f(face_lr[i+j*Nx].intersections[k],
                                 face_lr[i+j*Nx].intersections[k+1]);
        }
    }

    if(i<Nx+1 && j<Ny){
        for(int k=0;k<face_bt[i+j*(Nx+1)].n_sub_outer;++k){
            get_idx_of_segment_f(face_bt[i+j*(Nx+1)].intersections[k],
                                 face_bt[i+j*(Nx+1)].intersections[k+1]);
        }
    }

}


template<size_t o>
__global__
void leg_compute_integrals_k(int Nx, int Ny, element_star<o>* es, double* in,
                        double* aux, double ax, double ay, double hx, double hy,
                        double* gauss_x2, double* gauss_x3, double* gauss_w3,
                        face<o>* face_lr, face<o>* face_bt){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    constexpr size_t Nloc = o*(o+1)/2;

    in += k*Nx*Ny*Nloc;
    aux += k*Nx*Ny*Nloc;

    if(i>=Nx || j>=Ny)
        return;
    
    for(int ii=0;ii<Nloc;++ii)
        aux[ii + Nloc*(i+j*Nx)]=0.0;

    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_v[ii];++k){

            double x1 = es[i+j*Nx].inner_points_v[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_v[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_v[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_v[ii][k+1].y;

            int cell_i = es[i+j*Nx].inner_points_v[ii][k].i;
            int cell_j = (es[i+j*Nx].inner_points_v[ii][k].j+256)/2-128;
            int cell_j_p = (cell_j+Ny)%Ny;

            int cell_i_left = (cell_i+256)/2-128;
            int cell_i_left_p = (cell_i_left+Nx)%Nx;;
            int cell_i_right = (cell_i+1+256)/2-128;
            int cell_i_right_p = (cell_i_right+Nx)%Nx;

            leg_segment_integral_f<o>(x1, y1, x2, y2, cell_i_left, cell_j,
                                  es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                  &in[Nloc*(cell_i_left_p + cell_j_p*Nx)],
                                  &aux[Nloc*(i+j*Nx)], 1, ax, ay, hx, hy, 
                                  gauss_x2, gauss_x3, gauss_w3);

            leg_segment_integral_f<o>(x1, y1, x2, y2, cell_i_right, cell_j,
                                  es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                  &in[Nloc*(cell_i_right_p + cell_j_p*Nx)],
                                  &aux[Nloc*(i+j*Nx)], -1, ax, ay, hx, hy, 
                                  gauss_x2, gauss_x3, gauss_w3);

        }
    }

    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_h[ii];++k){

            double x1 = es[i+j*Nx].inner_points_h[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_h[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_h[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_h[ii][k+1].y;

            int cell_i = (es[i+j*Nx].inner_points_h[ii][k].i+256)/2-128;
            int cell_i_p = (cell_i+Nx)%Nx;
            int cell_j = es[i+j*Nx].inner_points_h[ii][k].j;

            int cell_j_top = (cell_j+1+256)/2-128;
            int cell_j_top_p = (cell_j_top+Ny)%Ny;
            int cell_j_bottom = (cell_j+256)/2-128;
            int cell_j_bottom_p = (cell_j_bottom+Ny)%Ny;

            leg_segment_integral_f<o>(x1, y1, x2, y2, cell_i, cell_j_top, 
                                  es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                  &in[Nloc*(cell_i_p + cell_j_top_p*Nx)], 
                                  &aux[Nloc*(i+j*Nx)], 1, ax, ay, hx, hy, 
                                  gauss_x2, gauss_x3, gauss_w3);

            leg_segment_integral_f<o>(x1, y1, x2, y2, cell_i, cell_j_bottom, 
                                  es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                  &in[Nloc*(cell_i_p + cell_j_bottom_p*Nx)], 
                                  &aux[Nloc*(i+j*Nx)], -1, ax, ay, hx, hy, 
                                  gauss_x2, gauss_x3, gauss_w3);
        }
    }

    leg_compute_integrals_outer_face_f(face_lr[i+j*Nx], es[i+j*Nx].x_center, 
                                   es[i+j*Nx].y_center, 1, 
                                   &aux[Nloc*(i+j*Nx)], in, false, 
                                   Nx, Ny, ax, ay, hx, hy, 
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_face_f(face_bt[i+1 + j*(Nx+1)], es[i+j*Nx].x_center,
                                   es[i+j*Nx].y_center, 1, 
                                   &aux[Nloc*(i+j*Nx)], in, false, 
                                   Nx, Ny, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_face_f(face_lr[i+(j+1)*Nx], es[i+j*Nx].x_center, 
                                   es[i+j*Nx].y_center, -1, 
                                   &aux[Nloc*(i+j*Nx)], in, false, 
                                   Nx, Ny, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_face_f(face_bt[i + j*(Nx+1)], es[i+j*Nx].x_center, 
                                   es[i+j*Nx].y_center, -1, 
                                   &aux[Nloc*(i+j*Nx)], in, false, 
                                   Nx, Ny, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);

}


template<size_t o>
__global__
void leg_compute_adjoint_solutions_k(int Nx, int Ny, vertex_star* vs, 
                                 element_star<o>* es, leg_adjoint_solution<o>* as,
                                 double hx, double hy){

    constexpr size_t e_stride = (o==1) ? 1 : o-1;
    
    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nx || j>=Ny)
        return;

    double v[o*o][2];
    int Nxe = Nx*e_stride+1;
    for(int jj=0;jj<o;++jj){
        for(int ii=0;ii<o;++ii){
            v[ii+jj*o][0] = (vs[e_stride*i+ii + (e_stride*j+jj)*Nxe].x - 
                             es[i+j*Nx].x_center)/hx;
            v[ii+jj*o][1] = (vs[e_stride*i+ii + (e_stride*j+jj)*Nxe].y - 
                             es[i+j*Nx].y_center)/hy;
        }
    }

    leg_compute_adjoint_solutions_f(v, as[i+j*Nx]);

}



template<size_t o>
__global__
void leg_finalize_k(int Nx, int Ny, double* out, double* aux, 
                double* weight, leg_adjoint_solution<o>* as){

    constexpr size_t Nloc = o*(o+1)/2;

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    if(i>=Nx || j>=Ny)
        return;

    out += k*Nx*Ny*Nloc;
    aux += k*Nx*Ny*Nloc;

    leg_sum_f(&out[Nloc*(i+j*Nx)], &aux[Nloc*(i+j*Nx)], weight, as[i+j*Nx]);

}


template<size_t o>
__global__
void leg_compute_integrals_precomp_k(int Nx, int Ny, leg_all_integrals<o>* ai, 
                                 face<o>* face_lr, face<o>* face_bt,
                                 element_star<o>* es, leg_adjoint_solution<o>* as,
                                 double ax, double ay, double hx, double hy,
                                 double* gauss_x2, double* gauss_x3, 
                                 double* gauss_w3){

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nx || j>=Ny)
        return;

    ai[i+j*Nx].set_to_zero();
    cell_ij_outer_f(face_lr[i + j*Nx], 1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_bt[i+1+j*(Nx+1)], 1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_lr[i + (j+1)*Nx],-1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_bt[i + j*(Nx+1)],-1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);

    leg_compute_integrals_outer_precomp_f<o>(face_lr[i+j*Nx], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, 1,
                                   as[i+j*Nx], ai[i+j*Nx], false, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_precomp_f<o>(face_bt[i+1 + j*(Nx+1)], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, 1,
                                   as[i+j*Nx], ai[i+j*Nx], false, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_precomp_f<o>(face_lr[i+(j+1)*Nx], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, -1,
                                   as[i+j*Nx], ai[i+j*Nx], false, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);
    leg_compute_integrals_outer_precomp_f<o>(face_bt[i + j*(Nx+1)], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, -1,
                                   as[i+j*Nx], ai[i+j*Nx], false, ax, ay, hx, hy,
                                   gauss_x2, gauss_x3, gauss_w3);



    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_v[ii];++k){

            double x1 = es[i+j*Nx].inner_points_v[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_v[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_v[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_v[ii][k+1].y;

            int cell_i = es[i+j*Nx].inner_points_v[ii][k].i;
            int cell_j = (es[i+j*Nx].inner_points_v[ii][k].j+256)/2-128;

            int cell_i_left = (cell_i+256)/2-128;
            int cell_i_right = (cell_i+1+256)/2-128;

            leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i_left, cell_j,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i_left,cell_j),
                                1, ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);

            leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i_right, cell_j,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i_right,cell_j),
                                -1, ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);

        }
    }
    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_h[ii];++k){

            double x1 = es[i+j*Nx].inner_points_h[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_h[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_h[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_h[ii][k+1].y;

            int cell_i = (es[i+j*Nx].inner_points_h[ii][k].i+256)/2-128;
            int cell_j = es[i+j*Nx].inner_points_h[ii][k].j;

            int cell_j_top = (cell_j+1+256)/2-128;
            int cell_j_bottom = (cell_j+256)/2-128;

            leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j_top,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i,cell_j_top),
                                1, ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);

            leg_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j_bottom,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i,cell_j_bottom),
                                -1, ax, ay, hx, hy, gauss_x2, gauss_x3, gauss_w3);
        }
    }


}


template<size_t o>
__global__
void leg_finalize_precomp_integrals_k(int Nx, int Ny, double* in, double* out, 
                                  int Nz, leg_all_integrals<o>* ai, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    int i = threadIdx.x;
    int j = blockIdx.x;
    int l = blockIdx.y;

    if(i>=Nx || j>=Ny || l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc;

    //finalize_f<o>(&out[Nloc*(i+j*Nx)+offset], ai[i+j*Nx], 
    //              Nx, Ny, in+offset, weight);
    leg_finalize_direct_f<o>(&out[Nloc*(i+j*Nx)+offset], ai[i+j*Nx], 
                         Nx, Ny, in+offset, weight);
}


//TODO: causes this problems...?
__device__
void __syncthreads();

template<size_t o>
__global__
void leg_finalize_with_sm_blocking_k(int Nx, int Ny, double* in, double* out,
                                 int Nz, leg_all_integrals<o>* ai, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    int i = threadIdx.y + blockDim.y*blockIdx.x;
    int j = threadIdx.z + blockDim.z*blockIdx.y;
    int l = blockIdx.z;

    if(i>=Nx || j>=Ny || l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc;

    in += offset;
    out += offset;

    extern __shared__ double local_in[];

    double* loc_in = local_in;
    offset = Nloc*MAX_CELLS*MAX_CELLS;
    loc_in += 2*offset*(threadIdx.y + blockDim.y*threadIdx.z);

    int r = threadIdx.x%MAX_CELLS;
    int s = threadIdx.x/MAX_CELLS;

    int rr = ai[i+j*Nx].min_i + r;
    int ss = ai[i+j*Nx].min_j + s;

    rr += (rr<0) ? Nx : 0;
    rr -= (rr>=Nx) ? Nx : 0;

    ss += (ss<0) ? Ny : 0;
    ss -= (ss>=Ny) ? Ny : 0;


    for(int k=0;k<Nloc;++k){
        if(ai[i+j*Nx].pi[s][r].is_used==true)
            loc_in[k + Nloc*threadIdx.x] = in[k + Nloc*(rr + Nx*ss)];
        loc_in[k + Nloc*threadIdx.x + offset] = 0.0;
    } 

    for(int k=0;k<Nloc;++k)
        for(int l=0;l<Nloc;++l)
            if(ai[i+j*Nx].pi[s][r].is_used==true)
                loc_in[k + Nloc*threadIdx.x + offset] += 
                      loc_in[l + Nloc*threadIdx.x]*ai[i+j*Nx].pi[s][r].data[k][l];

    __syncthreads();    

    if(threadIdx.x < Nloc){
        double out_loc=0.0;
        for(int s=0;s<MAX_CELLS;++s)
            for(int r=0;r<MAX_CELLS;++r)
                out_loc += loc_in[threadIdx.x + Nloc*(r+MAX_CELLS*s) + offset];

        out[threadIdx.x + Nloc*(i+j*Nx)] = out_loc*weight[threadIdx.x];
    } 


}


template<size_t o>
__global__
void leg_finalize_with_sm_nonblocking_k(int Nx, int Ny, double* in, double* out,
                                        int Nz, leg_all_integrals<o>* ai, 
                                        double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    int i = threadIdx.z + blockDim.z*blockIdx.x;
    int j = blockIdx.y;
    int l = blockIdx.z;

    if(i>=Nx || j>=Ny || l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc;

    in += offset;
    out += offset;

    extern __shared__ double local_in[];

    double* loc_in = local_in;
    offset = Nloc*MAX_CELLS*MAX_CELLS;
    loc_in += 2*offset*threadIdx.z;

    int r = threadIdx.x;
    int s = threadIdx.y;

    int rr = ai[i+j*Nx].min_i + r;
    int ss = ai[i+j*Nx].min_j + s;

    rr += (rr<0) ? Nx : 0;
    rr -= (rr>=Nx) ? Nx : 0;

    ss += (ss<0) ? Ny : 0;
    ss -= (ss>=Ny) ? Ny : 0;


    for(int k=0;k<Nloc;++k){
        if(ai[i+j*Nx].pi[s][r].is_used==true)
            loc_in[k + Nloc*(r+s*MAX_CELLS)] = in[k + Nloc*(rr + Nx*ss)];
        loc_in[k + Nloc*(r+s*MAX_CELLS) + offset] = 0.0;
    } 

    for(int k=0;k<Nloc;++k)
        for(int l=0;l<Nloc;++l)
            if(ai[i+j*Nx].pi[s][r].is_used==true)
                loc_in[k + Nloc*(r+s*MAX_CELLS) + offset] += 
                   loc_in[l + Nloc*(r+s*MAX_CELLS)]*ai[i+j*Nx].pi[s][r].data[k][l];

    __syncthreads();    

    int m = threadIdx.x + blockDim.x*threadIdx.y;
    if(m < Nloc){
        double out_loc=0.0;
        for(int r=0;r<MAX_CELLS;++r)
            for(int s=0;s<MAX_CELLS;++s)
                out_loc += loc_in[m + Nloc*(r+MAX_CELLS*s) + offset];

        out[m + Nloc*(i+j*Nx)] = out_loc*weight[m];
    } 
}

//dim3 threads(Nloc*Nloc+x,MAX_CELLS,MAX_CELLS);
//int tot_threads = threads.x*threads.y*threads.z;
//dim3 blocks(Nx,Ny,(Nz+tot_threads-1)/tot_threads);
template<size_t o>
__global__
void leg_finalize_c_in_sm(int Nx, int Ny, double* in, double* out,
                          int Nz, leg_all_integrals<o>* ai, double* weight){

    constexpr size_t Nloc = o*(o+1)/2;

    int i = blockIdx.x;
    int j = blockIdx.y;

    if(i>=Nx || j>=Ny)
        return;

    __shared__ double coefficient_matrices[MAX_CELLS][MAX_CELLS][Nloc][Nloc];

    int r = threadIdx.y;
    int s = threadIdx.z;

    //those threads write the coefficient matrices to the shared memory
    if(threadIdx.x < Nloc*Nloc){

        if(ai[i+j*Nx].pi[s][r].is_used==true){
            (&coefficient_matrices[s][r][0][0])[threadIdx.x] = 
                 (&ai[i+j*Nx].pi[s][r].data[0][0])[threadIdx.x];

        }

    }

    __syncthreads();

    int threadId = threadIdx.x + blockDim.x*(threadIdx.y + blockDim.y*threadIdx.z);
    //if blockIdx.z is zero ai has to be read just once
    int l = threadId + blockIdx.z*(blockDim.x*blockDim.y*blockDim.z);

    if(l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc;

    in += offset;
    out += offset;

    double out_loc[Nloc] = {0};
    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){

            if(ai[i+j*Nx].pi[s][r].is_used==true){

                int rr = ai[i+j*Nx].min_i + r;
                int ss = ai[i+j*Nx].min_j + s;

                rr += (rr<0) ? Nx : 0;
                rr -= (rr>=Nx) ? Nx : 0;

                ss += (ss<0) ? Ny : 0;
                ss -= (ss>=Ny) ? Ny : 0;

                for(int m=0;m<Nloc;++m){
                    for(int k=0;k<Nloc;++k){

                        out_loc[m] += coefficient_matrices[s][r][m][k]*
                                      in[k+Nloc*(rr+Nx*ss)];

                    }
                }
            }
        }
    }

    for(int m=0;m<Nloc;++m)
        out[m + Nloc*(i+j*Nx)] = out_loc[m]*weight[m];

}

template<size_t o>
struct sldg2d_legendre_gpu_orig{

    static constexpr size_t Nloc = o*(o+1)/2;
    static constexpr size_t e_stride = (o==1) ? 1 : o-1;

    double ax, ay;
    double bx, by;
    double hx, hy;
    int Nx, Ny;

    ext_vect<vertex_star> vs;
    ext_vect<face<o>> face_lr;
    ext_vect<face<o>> face_bt;
    ext_vect<element_star<o>> es;
    ext_vect<leg_adjoint_solution<o>> as;

    double* d_weight;
    double* d_gauss_x2;
    double* d_gauss_x3;
    double* d_gauss_w3;


    sldg2d_legendre_gpu_orig(double _ax, double _ay, double _bx, 
               double _by, int _Nx, int _Ny)
        : ax{_ax}, ay{_ay}, bx{_bx}, by{_by}, Nx{_Nx}, Ny{_Ny}
    {
        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        vs.resize(e_stride*Nx+1,e_stride*Ny+1,false);

        face_lr.resize(Nx,Ny+1,false);
        face_bt.resize(Nx+1,Ny,false);

        es.resize(Nx,Ny,false);
        as.resize(Nx,Ny,false);

        double weight_h[6] = {1.0,12.0,12.0,180.0,144.0,180.0};
        double gauss_x2_h[2] = {(1.0-0.5773502691896258)*0.5,
                                (1.0+0.5773502691896258)*0.5};
        double gauss_x3_h[3] = {(1.0-0.7745966692414834)*0.5,
                                (1.0+0.0)*0.5,
                                (1.0+0.7745966692414834)*0.5};
        double gauss_w3_h[3] = {0.555555555555555,
                                0.8888888888888889,
                                0.555555555555555};
        cudaMalloc(&d_weight,6*sizeof(double));
        cudaMalloc(&d_gauss_x2,2*sizeof(double));
        cudaMalloc(&d_gauss_x3,3*sizeof(double));
        cudaMalloc(&d_gauss_w3,3*sizeof(double));
        cudaMemcpy(d_weight,weight_h,6*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_x2,gauss_x2_h,2*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_x3,gauss_x3_h,3*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_w3,gauss_w3_h,3*sizeof(double),cudaMemcpyHostToDevice);

        gpuErrchk(cudaDeviceSynchronize());
        gpuErrchk(cudaPeekAtLastError());
    } 

    ~sldg2d_legendre_gpu_orig(){

    }


    template<typename F1, typename F2>
    void compute_upstream_mesh_RK(F1 a, F2 b, double dt, double t){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);
    
        leg_compute_upstream_mesh_RK_k<o><<<e_stride*Ny+1,e_stride*Nx+1 >>>(
                                e_stride*Nx+1, e_stride*Ny+1, vs.d_data, dt, t,
                                ax, ay, hx, hy, hx_fine, hy_fine, a, b);
    }


    //TODO
    template<typename T>
    void compute_upstream_mesh_cc(T& F, double dt, int stage){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);

        for(int j=0;j<e_stride*(Ny+1);++j){
            for(int i=0;i<e_stride*(Nx+1);++i){

                double x = ax+i*hx_fine;
                double y = ay+j*hy_fine;

                if(stage==1)
                    F.compute1(x,y,dt,i,j);
                if(stage==2)
                    F.compute2(x,y,dt,i,j);

                vs(i,j).set(x,y,ax,ay,hx,hy);

            }
        }
 
    }


    void get_outer_segments(){

        leg_get_outer_segments_k<o><<<Ny+1,Nx+1>>>(Nx, Ny, face_lr.d_data, 
                                               face_bt.d_data,
                                               vs.d_data, ax, ay, hx, hy);
    }


    void get_inner_segment_points(){

        leg_get_inner_segment_points_k<o><<<Ny,Nx>>>(Nx, Ny, es.d_data,
                                vs.d_data, face_lr.d_data, face_bt.d_data, 
                                ax, ay, hx, hy);
    }


    void complete_outer_segments(){

        leg_complete_outer_segments_k<o><<<Ny+1,Nx+1>>>(Nx, Ny, face_lr.d_data, 
                                                    face_bt.d_data);
    }


    void compute_adjoint_solutions(){

        leg_compute_adjoint_solutions_k<o><<<Ny,Nx>>>(Nx, Ny, vs.d_data, es.d_data, 
                                                  as.d_data, hx, hy);
    }


    void compute_integrals(double* in, double* aux, int Nz){

        dim3 blocks(Ny,Nz);
        leg_compute_integrals_k<o><<<blocks,Nx>>>(Nx, Ny, es.d_data, 
                                              in, aux, ax, ay, hx, hy, 
                                              d_gauss_x2, d_gauss_x3, d_gauss_w3, 
                                              face_lr.d_data, face_bt.d_data);
    }


    void finalize(double* out, double* aux, int Nz){

        dim3 blocks(Ny,Nz);
        leg_finalize_k<o><<<blocks,Nx>>>(Nx, Ny, out, aux, d_weight, as.d_data);

    }


    template<typename F1, typename F2>
    void translate(generic_container<double>& sol, generic_container<double>& aux, 
                   F1 a, F2 b, double dt, double final_T, int Nz){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        timer t_RK, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        double t=0.0;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_RK.start();
            compute_upstream_mesh_RK(a,b,dt,t);
            t_RK.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals(sol.data(true),aux.data(true),Nz);
            t_ci.stop();

            t_f.start();
            finalize(sol.data(true),aux.data(true),Nz);
            t_f.stop();

        }

        //cout << "Timings in milliseconds:\n";
        //cout << "RK: " << t_RK.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "perform the sum: " << t_f.total() << endl
        //     << "total: " << t_RK.total()+t_gos.total()+t_gis.total()+
        //                     t_cos.total()+t_ci.total()+t_f.total()+
        //                     t_cas.total()<<endl;


    }


};


template<size_t o>
struct sldg2d_legendre_gpu{

    static constexpr size_t Nloc = o*(o+1)/2;
    static constexpr size_t e_stride = (o==1) ? 1 : o-1;

    double ax, ay;
    double bx, by;
    double hx, hy;
    int Nx, Ny;

    ext_vect<vertex_star> vs;
    ext_vect<face<o>> face_lr;
    ext_vect<face<o>> face_bt;
    ext_vect<element_star<o>> es;
    ext_vect<leg_adjoint_solution<o>> as;
    ext_vect<leg_all_integrals<o>> ai;

    double* d_weight;
    double* d_gauss_x2;
    double* d_gauss_x3;
    double* d_gauss_w3;


    sldg2d_legendre_gpu(double _ax, double _ay, double _bx, 
                        double _by, int _Nx, int _Ny)
        : ax{_ax}, ay{_ay}, bx{_bx}, by{_by}, Nx{_Nx}, Ny{_Ny}
    {
        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        vs.resize(e_stride*Nx+1,e_stride*Ny+1,false);

        face_lr.resize(Nx,Ny+1,false);
        face_bt.resize(Nx+1,Ny,false);

        es.resize(Nx,Ny,false);
        as.resize(Nx,Ny,false);
        ai.resize(Nx,Ny,false);

        double weight_h[6] = {1.0,12.0,12.0,180.0,144.0,180.0};
        double gauss_x2_h[2] = {(1.0-0.5773502691896258)*0.5,
                                (1.0+0.5773502691896258)*0.5};
        double gauss_x3_h[3] = {(1.0-0.7745966692414834)*0.5,
                                (1.0+0.0)*0.5,
                                (1.0+0.7745966692414834)*0.5};
        double gauss_w3_h[3] = {0.555555555555555,
                                0.8888888888888889,
                                0.555555555555555};

        cudaMalloc(&d_weight,6*sizeof(double));
        cudaMalloc(&d_gauss_x2,2*sizeof(double));
        cudaMalloc(&d_gauss_x3,3*sizeof(double));
        cudaMalloc(&d_gauss_w3,3*sizeof(double));
        cudaMemcpy(d_weight,weight_h,6*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_x2,gauss_x2_h,2*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_x3,gauss_x3_h,3*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_w3,gauss_w3_h,3*sizeof(double),cudaMemcpyHostToDevice);

        gpuErrchk(cudaDeviceSynchronize());
        gpuErrchk(cudaPeekAtLastError());

    } 


    ~sldg2d_legendre_gpu(){
        gpuErrchk(cudaFree(d_weight));
        gpuErrchk(cudaFree(d_gauss_x2));
        gpuErrchk(cudaFree(d_gauss_x3));
        gpuErrchk(cudaFree(d_gauss_w3));
    }


    template<typename F1, typename F2>
    void compute_upstream_mesh_RK(F1 a, F2 b, double dt, double t){

        double hx_fine = (bx-ax)/double(e_stride*Nx);
        double hy_fine = (by-ay)/double(e_stride*Ny);
    
        leg_compute_upstream_mesh_RK_k<o><<<e_stride*Ny+1,e_stride*Nx+1 >>>(
                                e_stride*Nx+1, e_stride*Ny+1, vs.d_data, dt, t,
                                ax, ay, hx, hy, hx_fine, hy_fine, a, b);
    }


    //TODO
    //template<typename T>
    //void compute_upstream_mesh_cc(T& F, double dt, int stage){

    //    double hx_fine = (bx-ax)/double(e_stride*Nx);
    //    double hy_fine = (by-ay)/double(e_stride*Ny);

    //    for(int j=0;j<e_stride*(Ny+1);++j){
    //        for(int i=0;i<e_stride*(Nx+1);++i){

    //            double x = ax+i*hx_fine;
    //            double y = ay+j*hy_fine;

    //            if(stage==1)
    //                F.compute1(x,y,dt,i,j);
    //            if(stage==2)
    //                F.compute2(x,y,dt,i,j);

    //            vs(i,j).set(x,y,ax,ay,hx,hy);

    //        }
    //    }
 
    //}


    void get_outer_segments(){

        leg_get_outer_segments_k<o><<<Ny+1,Nx+1>>>(Nx, Ny, face_lr.d_data,
                                               face_bt.d_data,
                                               vs.d_data, ax, ay, hx, hy);
    }


    void get_inner_segment_points(){

        leg_get_inner_segment_points_k<o><<<Ny,Nx>>>(Nx, Ny, es.d_data,
                                vs.d_data, face_lr.d_data, face_bt.d_data, 
                                ax, ay, hx, hy);
    }


    void complete_outer_segments(){

        leg_complete_outer_segments_k<o><<<Ny+1,Nx+1>>>(Nx, Ny, face_lr.d_data, 
                                                    face_bt.d_data);
    }


    void compute_adjoint_solutions(){

        leg_compute_adjoint_solutions_k<o><<<Ny,Nx>>>(Nx, Ny, vs.d_data, es.d_data,
                                                  as.d_data, hx, hy);
    }


    void compute_integrals(){

        leg_compute_integrals_precomp_k<o><<<Ny,Nx>>>(Nx, Ny, ai.d_data, 
                                 face_lr.d_data, face_bt.d_data,
                                 es.d_data, as.d_data, ax, ay, hx, hy,
                                 d_gauss_x2, d_gauss_x3, d_gauss_w3);
    }


    void finalize(double* in, double* out, int Nz){

        //dim3 blocks(Ny,Nz);
        //leg_finalize_precomp_integrals_k<o><<<blocks,Nx>>>(Nx, Ny, in, out, 
        //                                               Nz, ai.d_data, d_weight);

        //int n = 5;
        //dim3 threads(MAX_CELLS*MAX_CELLS,n,n);
        //dim3 blocks((Nx+n-1)/n,(Ny+n-1)/n,Nz);
        //int sm = 2*n*n*MAX_CELLS*MAX_CELLS*Nloc*sizeof(double);
        //leg_finalize_with_sm_blocking_k<<<blocks,threads,sm>>>(Nx, Ny, in, out, 
        //                                          Nz, ai.d_data, d_weight);

        //int n = 14;
        //dim3 threads(MAX_CELLS,MAX_CELLS,n);
        //dim3 blocks((Nx+n-1)/n,Ny,Nz);
        //int sm = 2*n*MAX_CELLS*MAX_CELLS*Nloc*sizeof(double);
        //leg_finalize_with_sm_nonblocking_k<<<blocks,threads,sm>>>(Nx,Ny,in,out,Nz,
        //                                                 ai.d_data, d_weight);
        

        dim3 threads(Nloc*Nloc /*+x ??*/,MAX_CELLS,MAX_CELLS);
        int tot_threads = threads.x*threads.y*threads.z;
        dim3 blocks(Nx,Ny,(Nz+tot_threads-1)/tot_threads);
        leg_finalize_c_in_sm<<<blocks,threads>>>(Nx,Ny,in,out,Nz,ai.d_data,d_weight);
    }


    template<typename F1, typename F2>
    void translate(generic_container<double>& in, generic_container<double>& out, 
                   F1 a, F2 b, double dt, double final_T, int Nz){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        timer t_RK, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        double t=0.0;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_RK.start();
            compute_upstream_mesh_RK(a,b,dt,t);
            t_RK.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals();
            t_ci.stop();

            t_f.start();
            finalize(in.data(true),out.data(true),Nz);
            t_f.stop();

            #ifndef NDEBUG
            vector<double> h_out(Nloc*Nx*Ny);
            gpuErrchk(cudaMemcpy(h_out.data(),out.data(true),
                                 Nloc*Nx*Ny*sizeof(double),
                                 cudaMemcpyDeviceToHost));
            std::ofstream output("data_out"+std::to_string(ii+1)+".data");
            compute_mass_and_write_legendre<o>(ax, ay, hx, hy, Nx, Ny,
                                      h_out.data(), output, 2);
            #endif

            in.swap(out);
        }

        in.swap(out);

        //cout << "Timings in milliseconds:\n";
        //cout << "RK: " << t_RK.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "perform the sum: " << t_f.total() << endl
        //     << "total: " << t_RK.total()+t_gos.total()+t_gis.total()+
        //                     t_cos.total()+t_ci.total()+t_f.total()+
        //                     t_cas.total()<<endl;


    }

    //TODO:
    //template<typename T>
    //void translate(ext_vect<dg_coeff<o>>& in, ext_vect<dg_coeff<o>>& out,
    //               T f, double dt){

    //    auto tmp = [](double x, double y){return x+y;};

    //    f.poisson_solver(in,0.0,tmp,false,-1);
    //    compute_upstream_mesh_cc(f,dt,1);
    //    get_outer_segments();
    //    get_inner_segment_points();
    //    complete_outer_segments();
    //    compute_integrals(in);
    //    finalize(out);

    //    f.poisson_solver(out,0.0,tmp,false,-1);
    //    compute_upstream_mesh_cc(f,dt,2);
    //    get_outer_segments();
    //    get_inner_segment_points();
    //    complete_outer_segments();
    //    compute_integrals(in);
    //    finalize(out);

    //}

};

#endif



    //if(i==14 && j==10){
    //    if(threadIdx.x+threadIdx.y+threadIdx.z==0){
    //        for(int s=0;s<MAX_CELLS;++s){
    //           for(int r=0;r<MAX_CELLS;++r){
    //              printf("r:%i, s:%i \n",r,s);
    //              for(int k=0;k<Nloc;++k){
    //                 for(int l=0;l<Nloc;++l){
    //                     printf("%f ",coefficient_matrices[s][r][k][l]);
    //                 }
    //                 printf("\n");
    //              }
    //           }
    //        }
    //    }
    //}


