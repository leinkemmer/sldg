#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "geometry.hpp"

template<size_t o, typename F0>
void init_lagrange(F0 f0, int Nx, int Ny, double ax, double ay, 
                   double hx, double hy, double* in){

    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            for(int jj=0;jj<o;++jj){
                for(int ii=0;ii<o;++ii){

                    double x = ax + hx*(i + gauss::x_scaled01(ii,o));
                    double y = ay + hy*(j + gauss::x_scaled01(jj,o));

                    in[ii+o*jj + o*o*(i+j*Nx)] = f0(x,y);

                }
            }

        }
    }

}


template<size_t o>
double compute_mass_and_write_lagrange(double ax, double ay, 
                                       double hx, double hy,
                                       int Nx, int Ny, double* in,
                                       std::ofstream& output){

    double mass = 0.0;
    for(int j=0;j<Ny;++j){
        for(int jj=0;jj<o;++jj){
            for(int i=0;i<Nx;++i){
                for(int ii=0;ii<o;++ii){

                    double x = ax + hx*(i + gauss::x_scaled01(ii,o));
                    double y = ay + hy*(j + gauss::x_scaled01(jj,o));

                    double v = in[ii+o*jj + o*o*(i+j*Nx)];
                    mass += v*gauss::w(ii,o)*gauss::w(ii,o)*hx*hy*0.25;

                    output << std::setprecision(16);
                    output << x << " " << y << " " << v << endl;

                }
            }

        }
    }

    return mass;
}


template<size_t o, typename F>
std::pair<double,double> compute_errors_lagrange(F ref, int Nx, int Ny, 
                                                 double ax, double bx,
                                                 double ay, double by,
                                                 double* in){

    double hx = (bx-ax)/double(Nx);
    double hy = (by-ay)/double(Ny);
    
    double maxerr = 0.0;
    double l2err = 0.0;
    for(int j=0;j<Ny;++j){
        for(int i=0;i<Nx;++i){

            for(int jj=0;jj<o;++jj){
                for(int ii=0;ii<o;++ii){

                    double x = ax + hx*(i + gauss::x_scaled01(ii,o));
                    double y = ay + hy*(j + gauss::x_scaled01(jj,o));

                    double exact = ref(x,y);

                    double err = exact - in[ii+o*jj + o*o*(i+j*Nx)];

                    maxerr = std::max(maxerr,fabs(err));
                    l2err += err*err*gauss::w(ii,o)*gauss::w(jj,o);

                }
            }

        }
    }

    //why do they divide by the domainsize??
    l2err *= hx*hy*0.25/(bx-ax)/(by-ay);
    return {sqrt(l2err),maxerr};
}


template<size_t o>
struct lag_upstream_nodes{
         
    double v[o*o][2];
};       
         

template<size_t o>
struct lag_adjoint_solution{

    static constexpr size_t Nloc = o*o;

    double aux[Nloc][Nloc] = {{0}};

}; 


template<size_t o>
struct lag_precomp_integrals{

    static constexpr size_t Nloc = o*o;

    bool is_used=false;
    double data[Nloc][Nloc] = {{0}};

}; 


template<size_t o>
struct lag_all_integrals{

    static constexpr size_t Nloc = o*o;

    int min_i;
    int min_j;

    lag_precomp_integrals<o> pi[MAX_CELLS][MAX_CELLS];

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    void set_to_zero(){
        min_i = 65536;
        min_j = 65536;
        for(size_t r=0;r<MAX_CELLS;++r){
            for(size_t s=0;s<MAX_CELLS;++s){
                pi[r][s].is_used=false;
                for(size_t i=0;i<Nloc;++i){
                    for(size_t j=0;j<Nloc;++j){
                        pi[r][s].data[i][j] = 0.0;
                    }
                }
            }
        }
    }

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    lag_precomp_integrals<o>& operator()(int cell_i, int cell_j){
        return pi[cell_j-min_j][cell_i-min_i];
    }

}; 


template<size_t o,typename F1, typename F2>
#ifdef __CUDACC__
__host__ __device__
#endif
void lag_compute_upstream_node_RK_f(int i, int j, 
                                lag_upstream_nodes<o>& nodal_star,
                                double dt, double t,
                                double ax, double ay,
                                double hx, double hy,
                                double* gauss_x, int ii, int jj,
                                F1 a, F2 b){

    double x0 = ax + hx*(i + gauss_x[ii]);
    double y0 = ay + hy*(j + gauss_x[jj]);

    //Euler
    //x0 = x0 - dt*a(x0,y0,t);
    //y0 = y0 - dt*b(x0,y0,t);

    //RK4
    double k1x = -a(x0,y0,t);
    double k1y = -b(x0,y0,t);
    double k2x = -a(x0+dt*0.5*k1x,y0+dt*0.5*k1y,t-dt*0.5);
    double k2y = -b(x0+dt*0.5*k1x,y0+dt*0.5*k1y,t-dt*0.5);
    double k3x = -a(x0+dt*0.5*k2x,y0+dt*0.5*k2y,t-dt*0.5);
    double k3y = -b(x0+dt*0.5*k2x,y0+dt*0.5*k2y,t-dt*0.5);
    double k4x = -a(x0+dt*k3x,y0+dt*k3y,t-dt);
    double k4y = -b(x0+dt*k3x,y0+dt*k3y,t-dt);

    x0 = x0 + dt*1./6.*(k1x+2.*k2x+2.*k3x+k4x);
    y0 = y0 + dt*1./6.*(k1y+2.*k2y+2.*k3y+k4y);

    nodal_star.v[ii+o*jj][0] = x0; 
    nodal_star.v[ii+o*jj][1] = y0; 

}


#ifdef __CUDACC__
__host__ __device__
#endif
inline double power2(double v){
    return v*v;
}


#ifdef __CUDACC__
__host__ __device__
#endif
inline double power3(double v){
    return v*v*v;
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void lag_segment_integral_precomp_f(double x1, double y1, double x2, double y2,
                            int cell_i, int cell_j, double x_star, double y_star,
                            lag_adjoint_solution<o>& adj_sol, 
                            lag_precomp_integrals<o>& out, double anticlock,
                            double ax, double ay, double hx, double hy){ 

    constexpr size_t Nloc = o*o;

    out.is_used=true;

    double clx = ax + hx*cell_i;
    double cly = ay + hy*cell_j;

    x1 = (x1 - clx)/hx;
    y1 = (y1 - cly)/hy;
    x2 = (x2 - clx)/hx;
    y2 = (y2 - cly)/hy;

    double x_shift = (clx-x_star)/hx;
    double y_shift = (cly-y_star)/hy;

    if( fabs(x2-x1) < MACHINE_PREC*10 && fabs(y2-y1) < MACHINE_PREC*10 ){
        //do nothing
    } else {

        double x2m1 = x2-x1;
        double y2m1 = y2-y1;

        double green00 = (x2+x1)*0.5*y2m1;

        double green10, green01, green20, green11, green02; 
        double green21, green12, green22;

        constexpr double gauss_x2_0 = (1.0-0.5773502691896258)*0.5;
        constexpr double gauss_x2_1 = (1.0+0.5773502691896258)*0.5;
        constexpr double gauss_x3_0 = (1.0-0.7745966692414834)*0.5;
        constexpr double gauss_x3_1 = (1.0+0.0)*0.5;
        constexpr double gauss_x3_2 = (1.0+0.7745966692414834)*0.5;
        constexpr double gauss_w3_0 = 0.555555555555555;
        constexpr double gauss_w3_1 = 0.8888888888888889;
        constexpr double gauss_w3_2 = 0.555555555555555;
        if(o>1){
            double x_g1 = x1 + gauss_x2_0*x2m1;
            double x_g2 = x1 + gauss_x2_1*x2m1;
            double y_g1 = y1 + gauss_x2_0*y2m1;
            double y_g2 = y1 + gauss_x2_1*y2m1;

            green10 = (power2(x_g1) + power2(x_g2))*0.25*y2m1;
            green01 =-(power2(y_g1) + power2(y_g2))*0.25*x2m1;
            green20 = (power3(x_g1) + power3(x_g2))/6.0*y2m1;
            green11 = (power2(x_g1)*y_g1 + power2(x_g2)*y_g2)*0.25*y2m1;
            green02 =-(power3(y_g1) + power3(y_g2))/6.0*x2m1;

            x_g1 = x1 + gauss_x3_0*x2m1;
            x_g2 = x1 + gauss_x3_1*x2m1;
            y_g1 = y1 + gauss_x3_0*y2m1;
            y_g2 = y1 + gauss_x3_1*y2m1;
            double x_g3 = x1 + gauss_x3_2*x2m1;
            double y_g3 = y1 + gauss_x3_2*y2m1;

            green21 = (power3(x_g1)*y_g1*gauss_w3_0 + 
                       power3(x_g2)*y_g2*gauss_w3_1 + 
                       power3(x_g3)*y_g3*gauss_w3_2)/3.0*0.5*y2m1;
            green12 =-(x_g1*power3(y_g1)*gauss_w3_0 + 
                       x_g2*power3(y_g2)*gauss_w3_1 + 
                       x_g3*power3(y_g3)*gauss_w3_2)/3.0*0.5*x2m1;
            green22 = (power3(x_g1)*power2(y_g1)*gauss_w3_0 + 
                       power3(x_g2)*power2(y_g2)*gauss_w3_1 + 
                       power3(x_g3)*power2(y_g3)*gauss_w3_2)/3.0*0.5*y2m1;
        }

        if(o==1){
            out.data[0][0] += anticlock*green00*adj_sol.aux[0][0];
        }
        if(o==2){
            constexpr double num = (gauss_x2_1-gauss_x2_0);
            constexpr double divisor = 1.0/(num*num);
            constexpr double b11 =  divisor*(gauss_x2_1*gauss_x2_1);
            constexpr double b21 =  divisor*(-gauss_x2_1);
            constexpr double b31 =  divisor*(-gauss_x2_1);
            constexpr double b41 =  divisor*(1.0);
            constexpr double b12 = -divisor*(gauss_x2_0*gauss_x2_1);
            constexpr double b22 = -divisor*(-gauss_x2_1);
            constexpr double b32 = -divisor*(-gauss_x2_0);
            constexpr double b42 = -divisor*(1.0);
            constexpr double b13 = -divisor*(gauss_x2_1*gauss_x2_0);
            constexpr double b23 = -divisor*(-gauss_x2_0);
            constexpr double b33 = -divisor*(-gauss_x2_1);
            constexpr double b43 = -divisor*(1.0);
            constexpr double b14 =  divisor*(gauss_x2_0*gauss_x2_0);
            constexpr double b24 =  divisor*(-gauss_x2_0);
            constexpr double b34 =  divisor*(-gauss_x2_0);
            constexpr double b44 =  divisor*(1.0);

            for(int m=0;m<Nloc;++m){

                double c1 = adj_sol.aux[m][0] + 
                            adj_sol.aux[m][1]*x_shift +
                            adj_sol.aux[m][2]*y_shift +
                            adj_sol.aux[m][3]*x_shift*y_shift;
                double c2 = adj_sol.aux[m][1] + adj_sol.aux[m][3]*y_shift;
                double c3 = adj_sol.aux[m][2] + adj_sol.aux[m][3]*x_shift; 
                double c4 = adj_sol.aux[m][3];

                double tmp1 = c1*green00 + c2*green10 + c3*green01 + c4*green11;
                double tmp2 = c1*green10 + c2*green20 + c3*green11 + c4*green21;
                double tmp3 = c1*green01 + c2*green11 + c3*green02 + c4*green12;
                double tmp4 = c1*green11 + c2*green21 + c3*green12 + c4*green22;

                double C1 = b11*tmp1 + b21*tmp2 + b31*tmp3 + b41*tmp4;
                double C2 = b12*tmp1 + b22*tmp2 + b32*tmp3 + b42*tmp4;
                double C3 = b13*tmp1 + b23*tmp2 + b33*tmp3 + b43*tmp4;
                double C4 = b14*tmp1 + b24*tmp2 + b34*tmp3 + b44*tmp4;

                out.data[m][0] += anticlock*C1;
                out.data[m][1] += anticlock*C2;
                out.data[m][2] += anticlock*C3;
                out.data[m][3] += anticlock*C4;
            }
        }
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void lag_compute_integrals_outer_precomp_f(face<o>& face, double x_star,
                                    double y_star, int anticlock,
                                    lag_adjoint_solution<o>& adj_sol, 
                                    lag_all_integrals<o>& all_int,
                                    bool print, double ax,
                                    double ay, double hx, double hy){ 

    for(int k=0;k<face.n_sub_outer;++k){

        double x1 = face.intersections[k].x;
        double y1 = face.intersections[k].y;
        double x2 = face.intersections[k+1].x;
        double y2 = face.intersections[k+1].y;

        int su = mysign(x2-x1)*anticlock;
        int sv = mysign(y2-y1)*anticlock;

        int cell_i = (face.intersections[k].i+(sv>0) + 256)/2 - 128;
        int cell_j = (face.intersections[k].j+(su<0) + 256)/2 - 128;

        if(print){
            #if defined(PRINT_I) && defined(PRINT_J)
            cout << "\nx1: " << x1 << ", y1: " << y1 << endl;
            cout << "x2: " << x2 << ", y2: " << y2 << endl;
            cout << "i: " << cell_i << ", j: " << cell_j;
            cout << ", anticklock: " << anticlock << endl;
            #endif
        }

        lag_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j, 
                                      x_star, y_star, adj_sol, 
                                      all_int(cell_i,cell_j),
                                      anticlock, ax, ay, hx, hy);
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void get_A_b_f(double A[o*o][o*o], double B[o*o][o*o], double v[o*o][2]){

    if(o==1){
        A[0][0] = 1.0;
        B[0][0] = 1.0;
    }
    if(o==2){
        A[0][0] = 1.0;
        A[0][1] = v[0][0];
        A[0][2] = v[0][1];
        A[0][3] = v[0][0]*v[0][1];
        A[1][0] = 1.0;
        A[1][1] = v[1][0];
        A[1][2] = v[1][1];
        A[1][3] = v[1][0]*v[1][1];
        A[2][0] = 1.0;
        A[2][1] = v[2][0];
        A[2][2] = v[2][1];
        A[2][3] = v[2][0]*v[2][1];
        A[3][0] = 1.0;
        A[3][1] = v[3][0];
        A[3][2] = v[3][1];
        A[3][3] = v[3][0]*v[3][1];

        B[0][0] = 1.0;
        B[0][1] = 0.0;
        B[0][2] = 0.0; 
        B[0][3] = 0.0; 
        B[1][0] = 0.0; 
        B[1][1] = 1.0;
        B[1][2] = 0.0;
        B[1][3] = 0.0;
        B[2][0] = 0.0;
        B[2][1] = 0.0;
        B[2][2] = 1.0;
        B[2][3] = 0.0;
        B[3][0] = 0.0;
        B[3][1] = 0.0; 
        B[3][2] = 0.0; 
        B[3][3] = 1.0;
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void lag_compute_adjoint_solutions_f(double v[o*o][2], 
                                     lag_adjoint_solution<o>& as){

    constexpr size_t Nloc = o*o;

    double A[Nloc][Nloc];
    double B[Nloc][Nloc];

    get_A_b_f<o>(A, B, v);

    int P[Nloc];
    LUPDecompose<Nloc>(A,1e-14,P);

    for(int m=0;m<Nloc;++m)
        LUPSolve<Nloc>(A,P,B[m],as.aux[m]);

}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void lag_finalize_direct_f(double* out, lag_all_integrals<o>& all_int, 
                           int Nx, int Ny, double* in, 
                           bc bcx=bc::periodic, 
                           bc bcy=bc::periodic){

    constexpr size_t Nloc = o*o;

    double out_loc[Nloc] = {0};
    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){

            if(all_int.pi[s][r].is_used==true){
    
                int rr = all_int.min_i+r;
                int ss = all_int.min_j+s;


                if(bcx==bc::periodic && bcy==bc::periodic){
                    rr += (rr<0) ? Nx : 0;
                    rr -= (rr>=Nx) ? Nx : 0;

                    ss += (ss<0) ? Ny : 0;
                    ss -= (ss>=Ny) ? Ny : 0;

                    for(int m=0;m<Nloc;++m){
                        for(int k=0;k<Nloc;++k){

                            out_loc[m] += in[k + Nloc*(rr+ss*Nx)]*
                                          all_int.pi[s][r].data[m][k];
                        }
                    }
                }
                else if(bcx==bc::homogeneous_dirichlet && bcy==bc::periodic){
                    double zeros[Nloc] = {0};

                    ss += (ss<0) ? Ny : 0;
                    ss -= (ss>=Ny) ? Ny : 0;

                    double* dat = in;
                    if(rr<0 || rr>=Nx){
                        rr=0;
                        ss=0;
                        dat = zeros;
                    }

                    for(int m=0;m<Nloc;++m){
                        for(int k=0;k<Nloc;++k){

                            out_loc[m] += dat[k + Nloc*(rr+ss*Nx)]*
                                          all_int.pi[s][r].data[m][k];
                        }
                    }
 
                }
                //this is not really correct!!
                else if(bcx==bc::dirichlet && bcy==bc::periodic){

                    ss += (ss<0) ? Ny : 0;
                    ss -= (ss>=Ny) ? Ny : 0;

                    if(rr<0)
                        rr=0;
                    if(rr>=Nx)
                        rr=Nx-1;

                    for(int m=0;m<Nloc;++m){
                        for(int k=0;k<Nloc;++k){

                            out_loc[m] += in[k + Nloc*(rr+ss*Nx)]*
                                          all_int.pi[s][r].data[m][k];
                        }
                    }
 
                }
 
            }
        }
    }

    //if order higher than 2 is used, proper weights have to be used
    //this works for order 1 and 2 since there the weights are the same
    //for each gauss point
    for(int m=0;m<Nloc;++m)
        out[m] = out_loc[m]*((o==2) ? 4.0 : 1.0);
}

