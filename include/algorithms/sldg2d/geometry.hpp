#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "generic_sldg2d.hpp"

#define MAX_INTERSECTION_POINTS 4
#define MAX_CELLS 3
#define MACHINE_PREC 1e-15

struct vertex_star {

    double x,y;
    int i,j;

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    void set(double x_in, double y_in, double ax, 
             double ay, double hx, double hy)
    {
        i = round((x_in-ax)/hx);
        if(fabs(ax+i*hx - x_in) > MACHINE_PREC*(fabs(x_in)+1.0) ){
            x = x_in;
            i = 2*floor((x-ax)/hx);
        } else {
            x = ax+i*hx;
            i = 2*i-1;
        }


        j = round((y_in-ay)/hy);
        if(fabs(ay+j*hy - y_in) > MACHINE_PREC*(fabs(y_in)+1.0) ){
            y = y_in;
            j = 2*floor((y-ay)/hy);
        } else {
            y = ay+j*hy;
            j = 2*j-1;
        }
    }

};


//TODO: should I use an enum here?

//type = 0 -> vertex_star inside cell
//type = 1 -> vertex_star on horizontal e-grid line
//type = 2 -> vertex_star on vertical e-grid line
//type = 3 -> vertex_star on e-grid vertex
//type = 4 -> intersection point on horizonatal e-grid line
//type = 5 -> intersection point on vertical e-grid line
struct intersection_point{

    double x,y;
    int i,j;
    int type;

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    void swap(intersection_point& rhs){
        double tmp = rhs.x;
        rhs.x = x;
        x = tmp;

        tmp = rhs.y;
        rhs.y = y;
        y = tmp;

        tmp = rhs.i;
        rhs.i = i;
        i = tmp;

        tmp = rhs.j;
        rhs.j = j;
        j = tmp;

        tmp = rhs.type;
        rhs.type = type;
        type = tmp;
    }
};


template<size_t o>
struct face{

    int n_sub_outer;
    intersection_point intersections[MAX_INTERSECTION_POINTS+2];

};


template<size_t o>
struct element_star{

    static constexpr size_t Nloc = o*(o+1)/2;

    int min_i;
    int min_j;

    double x_center;
    double y_center;

    int idx_h[MAX_CELLS];
    int idx_v[MAX_CELLS];

    intersection_point inner_points_h[MAX_CELLS][MAX_CELLS+1];
    intersection_point inner_points_v[MAX_CELLS][MAX_CELLS+1];

};


template<size_t o,typename F1, typename F2>
#ifdef __CUDACC__
__host__ __device__
#endif
void compute_upstream_mesh_RK_f(int i, int j, vertex_star& vs,
                                double dt, double t,
                                double ax, double ay, 
                                double hx, double hy, 
                                double hx_fine, double hy_fine, F1 a, F2 b){

    double x0 = ax+i*hx_fine;
    double y0 = ay+j*hy_fine;

    //Euler
    //x0 = x0 - dt*a(x0,y0,t);
    //y0 = y0 - dt*b(x0,y0,t);

    //RK4
    double k1x = -a(x0,y0,t);
    double k1y = -b(x0,y0,t);
    double k2x = -a(x0+dt*0.5*k1x,y0+dt*0.5*k1y,t-dt*0.5);
    double k2y = -b(x0+dt*0.5*k1x,y0+dt*0.5*k1y,t-dt*0.5);
    double k3x = -a(x0+dt*0.5*k2x,y0+dt*0.5*k2y,t-dt*0.5);
    double k3y = -b(x0+dt*0.5*k2x,y0+dt*0.5*k2y,t-dt*0.5);
    double k4x = -a(x0+dt*k3x,y0+dt*k3y,t-dt);
    double k4y = -b(x0+dt*k3x,y0+dt*k3y,t-dt);

    x0 = x0 + dt*1./6.*(k1x+2.*k2x+2.*k3x+k4x);
    y0 = y0 + dt*1./6.*(k1y+2.*k2y+2.*k3y+k4y);

    vs.set(x0,y0,ax,ay,hx,hy);

}


//template<typename T>
//void compute_upstream_mesh_cc(T& F, double dt, int stage){
//
//    double hx_fine = (bx-ax)/double((o-1)*Nx);
//    double hy_fine = (by-ay)/double((o-1)*Ny);
//
//    for(int j=0;j<Ny+1;++j){
//        for(int i=0;i<Nx+1;++i){
//
//            double x = ax+i*hx_fine;
//            double y = ay+j*hy_fine;
//
//            if(stage==1)
//                F.compute1(x,y,dt,i,j);
//            if(stage==2)
//                F.compute2(x,y,dt,i,j);
//
//            vs(i,j).set(x,y,ax,ay,hx,hy);
//
//        }
//    }
//
//}


#ifdef __CUDACC__
__forceinline__ __host__ __device__
#endif
int mysign(double x){
    return (x>0.0)-(x<0.0);
}


#ifdef __CUDACC__
__forceinline__ __host__ __device__
#endif
bool is_odd(int n){
    return n%2!=0;
}


#ifdef __CUDACC__
__forceinline__ __host__ __device__
#endif
bool is_even(int n){
    return n%2==0;
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
bool find_intersections_f(face<o>& face, vertex_star& start, vertex_star& end,
                          double ax, double ay, double hx, double hy, 
                          bool print=false){

    int n=0;
    double times[MAX_INTERSECTION_POINTS+2];

    face.intersections[0].x = start.x;
    face.intersections[0].y = start.y;
    face.intersections[0].i = start.i;
    face.intersections[0].j = start.j;
    face.intersections[0].type = is_odd(start.i)+2*is_odd(start.j);
    times[0] = 0.0;
    n++;

    double u = end.x - start.x;
    double v = end.y - start.y;

    int su = mysign(end.i-start.i);
    int sv = mysign(end.j-start.j);

    int start_i = (start.i + (su>0) + 256)/2 - 128;
    int start_j = (start.j + (sv>0) + 256)/2 - 128;

    int n_intersec_x = su*((end.i+(su<0)+256)/2-128 - start_i);
    int n_intersec_y = sv*((end.j+(sv<0)+256)/2-128 - start_j);

    for(int i=0;i<n_intersec_x;++i){

        double t = (ax + hx*(start_i + su*(i+(su>0))) - start.x)/u;

        #ifndef NDEBUG
        #ifndef __CUDACC__
        if(t<=MACHINE_PREC || t>=1.0-MACHINE_PREC) {
            cout << std::setprecision(16);
            cout << "should never be printed, 4, t: " << t << endl;
            cout << std::setprecision(8);
        }
        #endif
        #endif

        face.intersections[n].x = ax + hx*(start_i + su*(i+(su>0)));
        face.intersections[n].i = 2*(start_i + su*(i+(su>0)))-1;
        face.intersections[n].type = 4;

        double y_tmp = start.y + t*v;
        face.intersections[n].y = y_tmp;
        face.intersections[n].j = 2*floor((y_tmp-ay)/hy);
        if(sv==0) {
            face.intersections[n].j = start.j;
        }

        times[n] = t;

        n++;
    }

    for(int i=0;i<n_intersec_y;++i){

        double t = (ay + hy*(start_j + sv*(i+(sv>0))) - start.y)/v;

        #ifndef NDEBUG
        #ifndef __CUDACC__
        if(t<=MACHINE_PREC || t>=1.0-MACHINE_PREC) {
            cout << std::setprecision(16);
            cout << "should never be printed, 5, t: " << t << endl;
            cout << std::setprecision(8);
        } 
        #endif
        #endif

        face.intersections[n].y = ay + hy*(start_j + sv*(i+(sv>0)));
        face.intersections[n].j = 2*(start_j + sv*(i+(sv>0)))-1;
        face.intersections[n].type = 5;

        double x_tmp = start.x + t*u;
        face.intersections[n].x = x_tmp;
        face.intersections[n].i = 2*floor((x_tmp-ax)/hx);
        if(su==0){
            face.intersections[n].i = start.i;
        }

        times[n] = t;

        //sort based on t
        for(int ii=0;ii<n;++ii){
            if(times[n-ii]<times[n-ii-1]){
                face.intersections[n-ii-1].swap(face.intersections[n-ii]);
                double tmp = times[n-ii-1];
                times[n-ii-1] = times[n-ii];
                times[n-ii] = tmp;
            }
        }
        n++;
    }

    face.intersections[n].x = end.x;
    face.intersections[n].y = end.y;
    face.intersections[n].i = end.i;
    face.intersections[n].j = end.j;
    face.intersections[n].type = is_odd(end.i)+2*is_odd(end.j);

    times[n] = 1.0;
    face.n_sub_outer = n;

    #ifndef __CUDACC__
    if(print){
        cout << "\nintersections before corr: " << face.n_sub_outer << endl;
        cout << "su: " << su << ", sv: " << sv << endl;
        for(int i=0;i<n+1;++i){
            cout << "i: " << i << " - ";
            cout << "x:" << face.intersections[i].x << " "
                 << "y:" << face.intersections[i].y << ", "
                 << "i:" << face.intersections[i].i << " "
                 << "j:" << face.intersections[i].j << ", "
                 << "t: " << times[i] << ", "
                 << "typ:" << face.intersections[i].type << endl;
        }
    }
    #endif


    //correct rounding issue
    for(int i=1;i<n;++i){
        if(face.intersections[i].type==5){
            if(su*face.intersections[i].i < su*face.intersections[i-1].i){
                face.intersections[i].i += 2*su;
            }
            if(su*face.intersections[i].i > su*face.intersections[i-1].i &&
               su*face.intersections[i].i > su*face.intersections[i+1].i){
                face.intersections[i].i -= 2*su;
            }
        }
        if(face.intersections[i].type==4){
            if(sv*face.intersections[i].j < sv*face.intersections[i-1].j){
                face.intersections[i].j += 2*sv;
            }
            if(sv*face.intersections[i].j > sv*face.intersections[i-1].j &&
               sv*face.intersections[i].j > sv*face.intersections[i+1].j){
                face.intersections[i].j -= 2*sv;
            }
        }
    }


    bool sorting=true;
    #ifndef NDEBUG
    //check if segments are ordered properly
    for(int i=0;i<n-1;++i){
        if(su*face.intersections[i].i > su*face.intersections[i+1].i){
            printf("WARNING: sorting went wrong, i\n");
            sorting = false;
        }
        if(sv*face.intersections[i].j > sv*face.intersections[i+1].j){
            printf("WARNING: sorting went wrong, j\n");
            sorting = false;
         }
    }
    if(sorting==false){
        printf("WARNING: sorting=false %i \n",false);

        printf("\nintersections after corr: %i \n",face.n_sub_outer);
        printf("su: %i, sv: %i \n", su, sv);
        for(int i=0;i<n+1;++i){
            printf("i: %i - x: %.17g, y: %.17g, i: %i, j: %i, t: %.17g, typ: %i \n ",
                    i, face.intersections[i].x, face.intersections[i].y,
                    face.intersections[i].i, face.intersections[i].j, 
                    times[i], face.intersections[i].type);
        }
    }
    if(n_intersec_x>2 || n_intersec_y>2){
        printf("n_intersec_x: %i, n_intersec_y: %i \n",
                n_intersec_x, n_intersec_y);
    }
    #endif


    if(print){
    #ifndef __CUDACC__
        cout << "\nintersections after corr: " << face.n_sub_outer << endl;
        cout << "su: " << su << ", sv: " << sv << endl;
        for(int i=0;i<n+1;++i){
            cout << "i: " << i << " - ";
            cout << "x:" << face.intersections[i].x << " "
                 << "y:" << face.intersections[i].y << ", "
                 << "i:" << face.intersections[i].i << " "
                 << "j:" << face.intersections[i].j << ", "
                 << "t: " << times[i] << ", "
                 << "typ:" << face.intersections[i].type << endl;
        }
    #endif
    }

    return sorting;
}
   

template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void find_inner_points_f(element_star<o>& es, face<o>& face, int bound){

    int min_i = es.min_i + is_even(es.min_i);
    int min_j = es.min_j + is_even(es.min_j);

    for(int k=bound;k<face.n_sub_outer+bound;++k){

        int point_type = face.intersections[k].type;

        if(point_type==1 || point_type==3 || point_type==4){

            int idx_seg = (face.intersections[k].i-min_i+256)/2-128;
            int n_ip = es.idx_v[idx_seg];

            es.inner_points_v[idx_seg][n_ip].x = face.intersections[k].x;
            es.inner_points_v[idx_seg][n_ip].y = face.intersections[k].y;
            es.inner_points_v[idx_seg][n_ip].i = face.intersections[k].i;
            es.inner_points_v[idx_seg][n_ip].j = face.intersections[k].j;

            es.idx_v[idx_seg]++;

        }
        if(point_type==2 || point_type==3 || point_type==5){

            int idx_seg = (face.intersections[k].j-min_j+256)/2-128;
            int n_ip = es.idx_h[idx_seg];

            es.inner_points_h[idx_seg][n_ip].x = face.intersections[k].x;
            es.inner_points_h[idx_seg][n_ip].y = face.intersections[k].y;
            es.inner_points_h[idx_seg][n_ip].i = face.intersections[k].i;
            es.inner_points_h[idx_seg][n_ip].j = face.intersections[k].j;

            es.idx_h[idx_seg]++;

        }
    }
}


#ifdef __CUDACC__
__forceinline__ __host__ __device__
#endif
void get_idx_of_segment_f(intersection_point& p1, intersection_point& p2){

    int min_i = min(p1.i,p2.i) + 128;
    int min_j = min(p1.j,p2.j) + 128;

    int diff_i = abs(p1.i-p2.i);
    int diff_j = abs(p1.j-p2.j);

    p1.i = 2*((min_i+diff_i)/2) + (diff_i==0)*is_odd(p1.i) - 128;
    p1.j = 2*((min_j+diff_j)/2) + (diff_j==0)*is_odd(p1.j) - 128;
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void sort_and_complete_inner_f(element_star<o>& es, double ax, double ay,
                               double hx, double hy){

    for(int ii=0;ii<MAX_CELLS;++ii){

        es.idx_v[ii] -= 1;
        if(es.idx_v[ii] < 1) continue;

        #ifndef NDEBUG
        #ifndef __CUDACC__
        if(es.idx_v[ii] > 1)
            cout << "WARNING: more than two points on same segment! "
                 << "Current implementation can not handle non convex "
                 << "upstream cells" << endl;
        #endif
        #endif

        if(es.inner_points_v[ii][0].y > es.inner_points_v[ii][1].y){
            es.inner_points_v[ii][0].swap(es.inner_points_v[ii][1]);
        }

        int ip = (es.inner_points_v[ii][1].j - es.inner_points_v[ii][0].j)/2;

        int start = (es.inner_points_v[ii][0].j+1+256)/2-128 + 1;

        for(int k=0;k<ip;++k){

            double y_new = ay + hy*(start+k);
            es.idx_v[ii]++;
            int idx = es.idx_v[ii];

            es.inner_points_v[ii][idx].x = es.inner_points_v[ii][0].x;
            es.inner_points_v[ii][idx].y = y_new;
            es.inner_points_v[ii][idx].i = es.inner_points_v[ii][0].i;
            es.inner_points_v[ii][idx].j = 2*(start+k)-1; 

            es.inner_points_v[ii][idx].swap(es.inner_points_v[ii][idx-1]);

            get_idx_of_segment_f(es.inner_points_v[ii][idx-2],
                                 es.inner_points_v[ii][idx-1]);
        }

        get_idx_of_segment_f(es.inner_points_v[ii][es.idx_v[ii]-1],
                             es.inner_points_v[ii][es.idx_v[ii]]);
    }
    for(int ii=0;ii<MAX_CELLS;++ii){

        es.idx_h[ii] -= 1;
        if(es.idx_h[ii] < 1) continue;

        #ifndef NDEBUG
        #ifndef __CUDACC__
        if(es.idx_h[ii] > 1)
            cout << "WARNING: more than two points on same segment! "
                 << "Current implementation can not handle non convex "
                 << "upstream cells" << endl;
        #endif
        #endif

        if(es.inner_points_h[ii][0].x > es.inner_points_h[ii][1].x){
            es.inner_points_h[ii][0].swap(es.inner_points_h[ii][1]);
        }

        int ip = (es.inner_points_h[ii][1].i - es.inner_points_h[ii][0].i)/2;

        int start = (es.inner_points_h[ii][0].i+1+256)/2-128 + 1;

        for(int k=0;k<ip;++k){

            double x_new = ax + hx*(start+k);
            es.idx_h[ii]++;
            int idx = es.idx_h[ii];

            es.inner_points_h[ii][idx].x = x_new;
            es.inner_points_h[ii][idx].y = es.inner_points_h[ii][0].y;
            es.inner_points_h[ii][idx].i = 2*(start+k)-1;
            es.inner_points_h[ii][idx].j = es.inner_points_h[ii][0].j; 

            es.inner_points_h[ii][idx].swap(es.inner_points_h[ii][idx-1]);

            get_idx_of_segment_f(es.inner_points_h[ii][idx-2],
                                 es.inner_points_h[ii][idx-1]);
        }

        get_idx_of_segment_f(es.inner_points_h[ii][es.idx_h[ii]-1],
                             es.inner_points_h[ii][es.idx_h[ii]]);
    }
}


template<size_t o>
#ifdef __CUDACC__
__host__ __device__
#endif
void cell_ij_outer_f(face<o>& face, int anticlock, int& min_i, int& min_j){

    for(int k=0;k<face.n_sub_outer;++k){

        double x1 = face.intersections[k].x;
        double y1 = face.intersections[k].y;
        double x2 = face.intersections[k+1].x;
        double y2 = face.intersections[k+1].y;

        int su = mysign(x2-x1)*anticlock;
        int sv = mysign(y2-y1)*anticlock;

        int cell_i = (face.intersections[k].i+(sv>0) + 256)/2 - 128;
        int cell_j = (face.intersections[k].j+(su<0) + 256)/2 - 128;

        min_i = min(min_i,cell_i);
        min_j = min(min_j,cell_j);
    }
}

