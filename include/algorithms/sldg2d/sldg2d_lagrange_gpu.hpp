#pragma once

#ifdef __CUDACC__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "lagrange_f.hpp"


template<size_t o, typename F1, typename F2>
__global__
void lag_compute_upstream_mesh_RK_k(int Nx, int Ny, double ax, double ay,
                                double hx, double hy, double hx_fine, 
                                double hy_fine, F1 a, F2 b, double dt,
                                double t, vertex_star* vs){

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nx+1 || j>=Ny+1)
        return;
    
    compute_upstream_mesh_RK_f<o>(i,j,vs[i+j*(Nx+1)],dt,t,ax,ay,
                                  hx,hy,hx_fine,hy_fine,a,b);

}


template<size_t o, typename F1, typename F2>
__global__
void lag_compute_upstream_nodes_RK_k(int Nx, int Ny, double ax, double ay,
                                double hx, double hy, double* gauss_x, 
                                F1 a, F2 b, double dt, double t, 
                                lag_upstream_nodes<o>* node_star){

    int i = threadIdx.x;
    int j = blockIdx.x;

    if(i>=Nx || j>=Ny)
        return;

    for(int jj=0;jj<o;++jj){
        for(int ii=0;ii<o;++ii){
    
            lag_compute_upstream_node_RK_f<o>(i,j,node_star[i+j*Nx],dt,t,
                                          ax,ay,hx,hy,gauss_x,ii,jj,a,b);
        }
    }

}


template<size_t o>
__global__
void lag_get_outer_segments_k(int Nx, int Ny, face<o>* face_lr,
                          face<o>* face_bt, vertex_star* vs,
                          double ax, double ay, double hx, double hy){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset_flr = Nx*(Ny+1)*k;
    int offset_fbt = (Nx+1)*Ny*k;
    int offset_vs = (Nx+1)*(Ny+1)*k;

    face_lr += offset_flr;
    face_bt += offset_fbt;
    vs += offset_vs;

    if(i<Nx && j<Ny+1)
        find_intersections_f(face_lr[i+j*Nx],
                             vs[i+j*(Nx+1)], vs[i+1+ j*(Nx+1)],
                             ax,ay,hx,hy,false);

    if(i<Nx+1 && j<Ny)
        find_intersections_f(face_bt[i+j*(Nx+1)],
                             vs[i+j*(Nx+1)], vs[i+ (j+1)*(Nx+1)],
                             ax,ay,hx,hy,false);
}


template<size_t o>
__global__
void lag_get_inner_segmen_points_k(int Nx, int Ny, element_star<o>* es,
                               vertex_star* vs, face<o>* face_lr, face<o>* face_bt,
                               double ax, double ay, double hx, double hy){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset_flr = Nx*(Ny+1)*k;
    int offset_fbt = (Nx+1)*Ny*k;
    int offset_vs = (Nx+1)*(Ny+1)*k;
    int offset = Nx*Ny*k;

    face_lr += offset_flr;
    face_bt += offset_fbt;
    vs += offset_vs;
    es += offset;


    if(i>=Nx || j>=Ny)
        return;

    es[i+j*Nx].min_i = min(min(vs[i+j*(Nx+1)].i,
                               vs[i+1+j*(Nx+1)].i),
                           min(vs[i+1+(j+1)*(Nx+1)].i,
                               vs[i+(j+1)*(Nx+1)].i));
    es[i+j*Nx].min_j = min(min(vs[i+j*(Nx+1)].j,
                               vs[i+1+j*(Nx+1)].j),
                           min(vs[i+1+(j+1)*(Nx+1)].j,
                               vs[i+(j+1)*(Nx+1)].j));

    es[i+j*Nx].x_center = (vs[i+j*(Nx+1)].x + 
                           vs[i+1+j*(Nx+1)].x + 
                           vs[i+1+(j+1)*(Nx+1)].x + 
                           vs[i+(j+1)*(Nx+1)].x)*0.25;
    es[i+j*Nx].y_center = (vs[i+j*(Nx+1)].y + 
                           vs[i+1+j*(Nx+1)].y + 
                           vs[i+1+(j+1)*(Nx+1)].y + 
                           vs[i+(j+1)*(Nx+1)].y)*0.25;

    for(int ii=0;ii<MAX_CELLS;++ii){
        es[i+j*Nx].idx_h[ii]=0;
        es[i+j*Nx].idx_v[ii]=0;
    }

    find_inner_points_f(es[i+j*Nx], face_lr[i+j*Nx],0);
    find_inner_points_f(es[i+j*Nx], face_bt[i+1+j*(Nx+1)],0);
    find_inner_points_f(es[i+j*Nx], face_lr[i+(j+1)*Nx],1);
    find_inner_points_f(es[i+j*Nx], face_bt[i+j*(Nx+1)],1);

    sort_and_complete_inner_f(es[i+j*Nx],ax,ay,hx,hy);

}


template<size_t o>
__global__
void lag_complete_outer_segments_k(int Nx, int Ny, face<o>* face_lr,
                                   face<o>* face_bt){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset_flr = Nx*(Ny+1)*k;
    int offset_fbt = (Nx+1)*Ny*k;

    face_lr += offset_flr;
    face_bt += offset_fbt;

    if(i<Nx && j<Ny+1)
        for(int kk=0;kk<face_lr[i+j*Nx].n_sub_outer;++kk){
            get_idx_of_segment_f(face_lr[i+j*Nx].intersections[kk],
                                 face_lr[i+j*Nx].intersections[kk+1]);
        }

    if(i<Nx+1 && j<Ny)
        for(int kk=0;kk<face_bt[i+j*(Nx+1)].n_sub_outer;++kk){
            get_idx_of_segment_f(face_bt[i+j*(Nx+1)].intersections[kk],
                                 face_bt[i+j*(Nx+1)].intersections[kk+1]);
        }
}


template<size_t o>
__global__
void lag_compute_adjoint_solutions_k(int Nx, int Ny, 
                                     lag_upstream_nodes<o>* un,
                                     element_star<o>* es, 
                                     lag_adjoint_solution<o>* as,
                                     double hx, double hy){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset = k*Nx*Ny;
    un += offset;
    es += offset;
    as += offset;

    if(i>=Nx || j>=Ny)
        return;

    double v[o*o][2];
    for(int jj=0;jj<o;++jj){
        for(int ii=0;ii<o;++ii){
            v[ii+jj*o][0] = (un[i+j*Nx].v[ii+jj*o][0] - 
                             es[i+j*Nx].x_center)/hx;
            v[ii+jj*o][1] = (un[i+j*Nx].v[ii+jj*o][1] - 
                             es[i+j*Nx].y_center)/hy;
        }
    }

    lag_compute_adjoint_solutions_f(v,as[i+j*Nx]);

}
 

template<size_t o>
__global__
void lag_compute_integrals_precomp_k(int Nx, int Ny, 
                                 lag_all_integrals<o>* ai,
                                 face<o>* face_lr, face<o>* face_bt,
                                 element_star<o>* es, lag_adjoint_solution<o>* as,
                                 double ax, double ay, double hx, double hy){
    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    if(i>=Nx || j>=Ny)
        return;

    int offset_flr = Nx*(Ny+1)*k;
    int offset_fbt = (Nx+1)*Ny*k;
    int offset = Nx*Ny*k;

    face_lr += offset_flr;
    face_bt += offset_fbt;
    es += offset;
    ai += offset;
    as += offset;

    bool print=false;
    #if defined(PRINT_I) && defined(PRINT_J)
    if(i==PRINT_I&&j==PRINT_J)
        print=true;
    #endif

    ai[i+j*Nx].set_to_zero();
    cell_ij_outer_f(face_lr[i+j*Nx], 1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_bt[i+1+j*(Nx+1)], 1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_lr[i+(j+1)*Nx], -1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);
    cell_ij_outer_f(face_bt[i+j*(Nx+1)], -1, ai[i+j*Nx].min_i, ai[i+j*Nx].min_j);

    lag_compute_integrals_outer_precomp_f<o>(face_lr[i+j*Nx], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, 1, 
                                   as[i+j*Nx], ai[i+j*Nx],
                                   print, ax, ay, hx, hy);
    lag_compute_integrals_outer_precomp_f<o>(face_bt[i+1+j*(Nx+1)], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, 1, 
                                   as[i+j*Nx], ai[i+j*Nx], 
                                   print, ax, ay, hx, hy);
    lag_compute_integrals_outer_precomp_f<o>(face_lr[i+(j+1)*Nx],
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, -1, 
                                   as[i+j*Nx], ai[i+j*Nx], 
                                   print, ax, ay, hx, hy);
    lag_compute_integrals_outer_precomp_f<o>(face_bt[i+j*(Nx+1)], 
                                   es[i+j*Nx].x_center, es[i+j*Nx].y_center, -1, 
                                   as[i+j*Nx], ai[i+j*Nx], 
                                   print, ax, ay, hx, hy);

    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_v[ii];++k){

            double x1 = es[i+j*Nx].inner_points_v[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_v[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_v[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_v[ii][k+1].y;

            int cell_i = es[i+j*Nx].inner_points_v[ii][k].i;
            int cell_j = (es[i+j*Nx].inner_points_v[ii][k].j+256)/2-128;

            int cell_i_left = (cell_i+256)/2-128;
            int cell_i_right = (cell_i+1+256)/2-128;

            lag_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i_left, cell_j,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i_left,cell_j),
                                1, ax, ay, hx, hy);

            lag_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i_right, cell_j,
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i_right,cell_j),
                                -1, ax, ay, hx, hy);

        }
    }

    for(int ii=0;ii<MAX_CELLS;++ii){
        for(int k=0;k<es[i+j*Nx].idx_h[ii];++k){

            double x1 = es[i+j*Nx].inner_points_h[ii][k].x;
            double y1 = es[i+j*Nx].inner_points_h[ii][k].y;
            double x2 = es[i+j*Nx].inner_points_h[ii][k+1].x;
            double y2 = es[i+j*Nx].inner_points_h[ii][k+1].y;

            int cell_i = (es[i+j*Nx].inner_points_h[ii][k].i+256)/2-128;
            int cell_j = es[i+j*Nx].inner_points_h[ii][k].j;

            int cell_j_top = (cell_j+1+256)/2-128;
            int cell_j_bottom = (cell_j+256)/2-128;

            lag_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j_top, 
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i,cell_j_top), 
                                1, ax, ay, hx, hy);

            lag_segment_integral_precomp_f<o>(x1, y1, x2, y2, cell_i, cell_j_bottom, 
                                es[i+j*Nx].x_center, es[i+j*Nx].y_center,
                                as[i+j*Nx], ai[i+j*Nx](cell_i,cell_j_bottom),
                                -1, ax, ay, hx, hy);
        }
    }
}


template<size_t o>
__global__
void lag_finalize_k(double* in, double* out, int Nz, int Nx, int Ny,
                    lag_all_integrals<o>* ai, bc bcx, bc bcy){

    constexpr size_t Nloc = o*o;

    int i = threadIdx.x;
    int j = blockIdx.x;
    int l = blockIdx.y;

    if(i>=Nx || j>=Ny || l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc;

    lag_finalize_direct_f<o>(&out[Nloc*(i+j*Nx)+offset], 
                             ai[i+j*Nx], Nx, Ny, in+offset, bcx, bcy);
}

//TODO: causes this problems...?
__device__
void __syncthreads();

//dim3 threads(Nloc*Nloc+x,MAX_CELLS,MAX_CELLS);
//int tot_threads = threads.x*threads.y*threads.z;
//dim3 blocks(Nx*Ny,(Nz+tot_threads-1)/tot_threads);
//TODO: boundary conditions??
template<size_t o>
__global__
void lag_finalize_c_in_sm(int Nx, int Ny, double* in, double* out,
                          int Nz, lag_all_integrals<o>* ai, 
                          bc UNUSED(bcx), bc UNUSED(bcy)){

    constexpr size_t Nloc = o*o;

    int idx_ij = blockIdx.x;

    __shared__ double coefficient_matrices[MAX_CELLS][MAX_CELLS][Nloc][Nloc];

    int r = threadIdx.y;
    int s = threadIdx.z;
    int k = blockIdx.z;

    int offset_ai = k*Nx*Ny;
    ai += offset_ai;

    //those threads write the coefficient matrices to the shared memory
    if(threadIdx.x < Nloc*Nloc){

        if(ai[idx_ij].pi[s][r].is_used==true){
            (&coefficient_matrices[s][r][0][0])[threadIdx.x] =
                 (&ai[idx_ij].pi[s][r].data[0][0])[threadIdx.x];

        }

    }

    __syncthreads();

    int threadId = threadIdx.x + blockDim.x*(threadIdx.y + blockDim.y*threadIdx.z);
    //if blockIdx.y is zero ai has to be read just once
    int l = threadId + blockIdx.y*(blockDim.x*blockDim.y*blockDim.z);

    if(l>=Nz)
        return;

    int offset = l*Nx*Ny*Nloc + k*Nz*Nx*Ny*Nloc;

    in += offset;
    out += offset;

    double out_loc[Nloc] = {0};
    for(int s=0;s<MAX_CELLS;++s){
        for(int r=0;r<MAX_CELLS;++r){

            if(ai[idx_ij].pi[s][r].is_used==true){

                int rr = ai[idx_ij].min_i + r;
                int ss = ai[idx_ij].min_j + s;

                rr += (rr<0) ? Nx : 0;
                rr -= (rr>=Nx) ? Nx : 0;

                ss += (ss<0) ? Ny : 0;
                ss -= (ss>=Ny) ? Ny : 0;

                for(int m=0;m<Nloc;++m){
                    for(int k=0;k<Nloc;++k){

                        out_loc[m] += coefficient_matrices[s][r][m][k]*
                                      in[k+Nloc*(rr+Nx*ss)];

                    }
                }
            }
        }
    }

    for(int m=0;m<Nloc;++m)
        out[m + Nloc*idx_ij] = out_loc[m]*((o==2) ? 4.0 : 1.0);

}


template<typename T>
__global__
void compute_upstream_mesh_cc1_on_gpu_k(T* F1_cc, T* F2_cc,
                                       double dt, vertex_star* vs,
                                       int Nx, int Ny, double ax, double ay,
                                       double hx, double hy){
    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int lidx = i+(Nx+1)*((j%Ny)+Ny*k);

    fp r     = ax + hx*i;
    fp theta = ay + hy*j;

    int offset_vs = k*(Nx+1)*(Ny+1);
    vs += offset_vs;

    vs[i+j*(Nx+1)].set(r + dt*F1_cc[lidx]/r,
                       theta - dt*F2_cc[lidx]/r,
                       ax, ay, hx, hy);

}


template<size_t o, typename T>
__global__
void compute_upstream_mesh_dg1_on_gpu_k(T* F1_dg, T* F2_dg, 
                                       double dt, double* gauss_x, 
                                       lag_upstream_nodes<o>* un,
                                       int Nx, int Ny, double ax, double ay,
                                       double hx, double hy){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset_un = k*Nx*Ny;
    un += offset_un;

    for(int jj=0;jj<o;++jj){
        for(int ii=0;ii<o;++ii){

            int lidx = ii+o*(jj+o*(i+Nx*(j+Ny*k)));
            fp r     = ax + hx*(i+gauss_x[ii]);
            fp theta = ay + hy*(j+gauss_x[jj]);
        
            un[i+j*Nx].v[ii+o*jj][0] = r + dt*F1_dg[lidx]/r;
            un[i+j*Nx].v[ii+o*jj][1] = theta - dt*F2_dg[lidx]/r;
        }
    }

}


__device__ __forceinline__
fp F_at_point(fp* F, fp r, fp theta, fp ar, fp at, 
              fp hr, fp ht, int Nr, int Ntheta){

        int i = floor((r-ar)/hr);
        int j = floor((theta-at)/ht);

        int ip1 = i+1;
        //TODO: rounding problems here?
        if(i==-1) i=0;
        if(i==Nr+1){
            i=Nr;
            ip1=Nr; //r \approx r1 -> F21,F22 have no impact
        }

        fp F11 = F[i + (Nr+1)*((j+Ntheta)%Ntheta)];//((i+Nx)%Nx,(j+Ny)%Ny);
        fp F12 = F[i + (Nr+1)*((j+1+Ntheta)%Ntheta)];//((i+Nx)%Nx,(j+1+Ny)%Ny);
        fp F21 = F[ip1 + (Nr+1)*((j+Ntheta)%Ntheta)];//((i+1+Nx)%Nx,(j+Ny)%Ny);
        fp F22 = F[ip1 + (Nr+1)*((j+1+Ntheta)%Ntheta)];//((i+1+Nx)%Nx,(j+1+Ny)%Ny);

        fp r1 = ar + i*hr;
        fp r2 = ar + (i+1)*hr;

        fp theta1 = at + j*ht;
        fp theta2 = at + (j+1)*ht;

        return (F11*(r2-r)*(theta2-theta) + F12*(r2-r)*(theta-theta1) +
                F21*(r-r1)*(theta2-theta) + F22*(r-r1)*(theta-theta1))/(hr*ht);
}


template<typename T>
__global__
void compute_upstream_mesh_cc2_on_gpu_k(T* F1_cc, T* F2_cc,
                                       double dt, vertex_star* vs,
                                       int Nx, int Ny, double ax, double ay,
                                       double hx, double hy){
    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int lidx = i+(Nx+1)*((j%Ny)+Ny*k);

    int offset_vs = k*(Nx+1)*(Ny+1);
    vs += offset_vs;

    fp r     = ax + hx*i;
    fp theta = ay + hy*j;

    fp r12     = r + 0.5*dt*F1_cc[lidx]/r;
    fp theta12 = theta - 0.5*dt*F2_cc[lidx]/r;

    int offset = (Nx+1)*Ny*k;
    
    r += dt*F_at_point(&F1_cc[offset],r12,theta12,ax,ay,hx,hy,Nx,Ny)/r12;
    theta -= dt*F_at_point(&F2_cc[offset],r12,theta12,ax,ay,hx,hy,Nx,Ny)/r12;

    vs[i+j*(Nx+1)].set(r, theta, ax, ay, hx, hy);

}


template<size_t o>
__global__
void compute_upstream_mesh_dg2_on_gpu_k(fp* F1_dg, fp* F2_dg, 
                                        fp* F1_cc, fp* F2_cc,
                                        double dt, double* gauss_x, 
                                        lag_upstream_nodes<o>* un,
                                        int Nx, int Ny, double ax, double ay,
                                        double hx, double hy){

    int i = threadIdx.x;
    int j = blockIdx.x;
    int k = blockIdx.y;

    int offset_un = k*Nx*Ny;
    un += offset_un;

    for(int jj=0;jj<o;++jj){
        for(int ii=0;ii<o;++ii){

            int lidx = ii+o*(jj+o*(i+Nx*(j+Ny*k)));
            fp r     = ax + hx*(i+gauss_x[ii]);
            fp theta = ay + hy*(j+gauss_x[jj]);

            fp r12     = r + 0.5*dt*F1_dg[lidx]/r;
            fp theta12 = theta - 0.5*dt*F2_dg[lidx]/r;

            int offset = (Nx+1)*Ny*k;

            r += dt*F_at_point(&F1_cc[offset],r12,theta12,ax,ay,hx,hy,Nx,Ny)/r12;
            theta -= dt*F_at_point(&F2_cc[offset],r12,theta12,ax,ay,hx,hy,Nx,Ny)/r12;

            un[i+j*Nx].v[ii+o*jj][0] = r;
            un[i+j*Nx].v[ii+o*jj][1] = theta;
        }
    }

}


template<size_t o>
struct sldg2d_lagrange_gpu{

    static constexpr size_t Nloc = o*o;

    double ax, ay;
    double bx, by;
    double hx, hy;
    int Nx, Ny;
    int batch;

    double* d_gauss_x2;
    double* d_gauss_x3;
    double* d_gauss_w3;

    ext_vect<vertex_star> vs;
    ext_vect<lag_upstream_nodes<o>> un;
    ext_vect<face<o>> face_lr;
    ext_vect<face<o>> face_bt;
    ext_vect<element_star<o>> es;
    ext_vect<lag_adjoint_solution<o>> as;
    ext_vect<lag_all_integrals<o>> ai;

    sldg2d_lagrange_gpu(double _ax, double _ay, double _bx, 
                        double _by, int _Nx, int _Ny, int _batch=1)
        : ax{_ax}, ay{_ay}, bx{_bx}, by{_by}, Nx{_Nx}, Ny{_Ny}, batch{_batch}
    {
        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        vs.resize(Nx+1,Ny+1,true,batch);
        un.resize(Nx,Ny,true,batch);

        face_lr.resize(Nx,Ny+1,false,batch);
        face_bt.resize(Nx+1,Ny,false,batch);

        es.resize(Nx,Ny,false,batch);
        as.resize(Nx,Ny,false,batch);
        ai.resize(Nx,Ny,false,batch);

        double gauss_x2_h[2] = {(1.0-0.5773502691896258)*0.5,
                                (1.0+0.5773502691896258)*0.5};
        double gauss_x3_h[3] = {(1.0-0.7745966692414834)*0.5,
                                (1.0+0.0)*0.5,
                                (1.0+0.7745966692414834)*0.5};
        double gauss_w3_h[3] = {0.555555555555555,
                                0.8888888888888889,
                                0.555555555555555};

        cudaMalloc(&d_gauss_x2,2*sizeof(double));
        cudaMalloc(&d_gauss_x3,3*sizeof(double));
        cudaMalloc(&d_gauss_w3,3*sizeof(double));
        cudaMemcpy(d_gauss_x2,gauss_x2_h,2*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_x3,gauss_x3_h,3*sizeof(double),cudaMemcpyHostToDevice);
        cudaMemcpy(d_gauss_w3,gauss_w3_h,3*sizeof(double),cudaMemcpyHostToDevice);

        gpuErrchk(cudaDeviceSynchronize());
        gpuErrchk(cudaPeekAtLastError());

    } 

    ~sldg2d_lagrange_gpu(){
        gpuErrchk(cudaFree(d_gauss_x2));
        gpuErrchk(cudaFree(d_gauss_x3));
        gpuErrchk(cudaFree(d_gauss_w3));
    }


    template<typename F1, typename F2>
    void compute_upstream_mesh_RK(F1 a, F2 b, double dt, double t){

        dim3 blocks(Ny+1);
        lag_compute_upstream_mesh_RK_k<o><<<blocks,Nx+1>>>(Nx, Ny, ax, ay, hx, hy, 
                                                  hx, hy, a, b, dt, t, vs.d_data);
    }


    template<typename F1, typename F2>
    void compute_upstream_nodes_RK(F1 a, F2 b, double dt, double t){

        dim3 blocks(Ny);
        lag_compute_upstream_nodes_RK_k<o><<<blocks,Nx>>>(Nx, Ny, ax, ay, hx, 
                                       hy, d_gauss_x2, a, b, dt, t, un.d_data);
    }


    //template<typename T>
    //void compute_upstream_mesh_on_cpu(T& F, double dt, int k, int stage=1){

    //    #pragma omp parallel for schedule(static)
    //    for(int j=0;j<Ny+1;++j){
    //        for(int i=0;i<Nx+1;++i){

    //            double x = ax+i*hx;
    //            double y = ay+j*hy;

    //            if(stage==1)
    //                F.follow_characteristics_cc1(x,y,dt,i,j,k);
    //            else if(stage==2)
    //                F.follow_characteristics_cc2(x,y,dt,i,j,k);
    //            else{
    //                cout << "ERROR: stage=" << stage << endl;
    //                exit(1);
    //            }


    //            vs(i,j).set(x,y,ax,ay,hx,hy);

    //        }
    //    }
    //    vs.copy_to_gpu();

    //    #pragma omp parallel for schedule(static)
    //    for(int j=0;j<Ny;++j){
    //        for(int i=0;i<Nx;++i){

    //            for(int jj=0;jj<o;++jj){
    //                for(int ii=0;ii<o;++ii){

    //                    double x0 = ax + hx*(i + gauss::x_scaled01(ii,o));
    //                    double y0 = ay + hy*(j + gauss::x_scaled01(jj,o));

    //                    if(stage==1)
    //                        F.follow_characteristics_dg1(x0,y0,dt,ii,jj,i,j,k);
    //                    else if(stage==2)
    //                        F.follow_characteristics_dg2(x0,y0,dt,ii,jj,i,j,k);

    //                    un.h_data[i+j*Nx].v[ii+o*jj][0] = x0;
    //                    un.h_data[i+j*Nx].v[ii+o*jj][1] = y0;

    //                }
    //            }
    //        }
    //    }
    //    un.copy_to_gpu();

    //}


    void compute_upstream_mesh_cc_on_gpu(double* F1_cc, double* F2_cc, 
                                      double* F1_dg, double* F2_dg,
                                      double dt, int stage){

        dim3 blocks_cc(Ny+1,batch);
        dim3 blocks_dg(Ny,batch);
        if(stage==1){
            compute_upstream_mesh_cc1_on_gpu_k<<<blocks_cc,Nx+1>>>(F1_cc, F2_cc, 
                                         dt, vs.d_data, Nx, Ny, ax, ay, hx, hy);
            compute_upstream_mesh_dg1_on_gpu_k<o><<<blocks_dg,Nx>>>(F1_dg, F2_dg,
                              dt, d_gauss_x2, un.d_data, Nx, Ny, ax, ay, hx, hy);
        }
        if(stage==2){
            compute_upstream_mesh_cc2_on_gpu_k<<<blocks_cc,Nx+1>>>(F1_cc, F2_cc, 
                                         dt, vs.d_data, Nx, Ny, ax, ay, hx, hy);
            compute_upstream_mesh_dg2_on_gpu_k<o><<<blocks_dg,Nx>>>(F1_dg, F2_dg, 
                                                 F1_cc, F2_cc, dt, d_gauss_x2, 
                                                 un.d_data, Nx, Ny, ax, ay, hx, hy);
        }
    }



    void get_outer_segments(){

        dim3 blocks(Ny+1,batch);
        lag_get_outer_segments_k<o><<<blocks,Nx+1>>>(Nx, Ny, face_lr.d_data, 
                                 face_bt.d_data, vs.d_data, ax, ay, hx, hy);
    }


    void get_inner_segment_points(){

        dim3 blocks(Ny,batch);
        lag_get_inner_segmen_points_k<o><<<blocks,Nx>>>(Nx, Ny, es.d_data, vs.d_data,
                                     face_lr.d_data, face_bt.d_data, ax, ay, hx, hy);
    }


    void complete_outer_segments(){

        dim3 blocks(Ny+1,batch);
        lag_complete_outer_segments_k<o><<<blocks,Nx+1>>>(Nx, Ny, 
                                         face_lr.d_data, face_bt.d_data);
    }


    void compute_adjoint_solutions(){

        dim3 blocks(Ny,batch);
        lag_compute_adjoint_solutions_k<o><<<blocks,Nx>>>(Nx, Ny, un.d_data, 
                                              es.d_data, as.d_data, hx, hy);
    }


    void compute_integrals(){

        dim3 blocks(Ny,batch);
        lag_compute_integrals_precomp_k<o><<<blocks,Nx>>>(Nx, Ny, ai.d_data, 
                                       face_lr.d_data, face_bt.d_data, es.d_data, 
                                       as.d_data, ax, ay, hx, hy);
    }
    
    void finalize(double* in, double* out, int Nz){

        dim3 blocks(Ny,Nz);
        lag_finalize_k<o><<<blocks,Nx>>>(in, out, Nz, Nx, Ny, ai.d_data, 
                                         bc::periodic, bc::periodic);
    }

    void finalize(double* in, double* out, int Nz, bc bcx, bc bcy){

        //dim3 blocks(Ny,Nz);
        //lag_finalize_k<o><<<blocks,Nx>>>(in, out, Nz, Nx, Ny, ai.d_data, bcx, bcy);

        dim3 threads(Nloc*Nloc /*+x ??*/,MAX_CELLS,MAX_CELLS);
        int tot_threads = threads.x*threads.y*threads.z;
        dim3 blocks(Nx*Ny,(Nz+tot_threads-1)/tot_threads,batch);
        lag_finalize_c_in_sm<<<blocks,threads>>>(Nx,Ny,in,out,Nz,ai.d_data,bcx,bcy);
    }


    template<typename F1, typename F2>
    void translate(generic_container<double>& in, 
                   generic_container<double>& out, F1 a, F2 b, 
                   double dt, double final_T, int Nz){

        int n_steps = ceil(final_T/dt);
        //cout << "\n\nN: " << Nx << ", final time: " << final_T 
        //     << ", step size: " << dt
        //     << ", number of time steps: " << n_steps << endl;

        timer t_RK, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
        double t=0.0;
        for(int ii=0;ii<n_steps;++ii){

            if(t+dt>final_T)
                dt = final_T-t;

            t+=dt;

            t_RK.start();
            compute_upstream_mesh_RK(a,b,dt,t);
            compute_upstream_nodes_RK(a,b,dt,t);
            t_RK.stop();

            t_gos.start();
            get_outer_segments();
            t_gos.stop();

            t_gis.start();
            get_inner_segment_points();
            t_gis.stop();

            t_cos.start();
            complete_outer_segments();
            t_cos.stop();

            t_cas.start();
            compute_adjoint_solutions();
            t_cas.stop();

            t_ci.start();
            compute_integrals();
            t_ci.stop();

            t_f.start();
            finalize(in.data(true),out.data(true),Nz);
            t_f.stop();

            #ifndef NDEBUG
            vector<double> h_out(Nloc*Nx*Ny);
            gpuErrchk(cudaMemcpy(h_out.data(),out.data(true),
                                 Nloc*Nx*Ny*sizeof(double),
                                 cudaMemcpyDeviceToHost));

            std::ofstream output("data_out"+std::to_string(ii+1)+".data");
            compute_mass_and_write_lagrange<o>(ax, ay, hx, hy, Nx, Ny,
                                               h_out.data(), output);
            #endif

            in.swap(out);

        }

        in.swap(out);

        //cout << "Timings in milliseconds:\n";
        //cout << "RK: " << t_RK.total() << endl
        //     << "get outer segments: " << t_gos.total() << endl
        //     << "get inner segments: " << t_gis.total() << endl
        //     << "complete outer segm: " << t_cos.total() << endl
        //     << "compute adjoint sol: " << t_cas.total() << endl
        //     << "compute integrals: " << t_ci.total() << endl
        //     << "perform the sum: " << t_f.total() << endl
        //     << "total: " << t_RK.total()+t_gos.total()+t_gis.total()+
        //                     t_cos.total()+t_ci.total()+t_f.total()+
        //                     t_cas.total()<<endl;

    }

    template<typename T>
    void step_nl(double* in, double* out,
                 T& velocity_field, double dt,
                 int Nz, bc bcx=bc::periodic,
                 bc bcy=bc::periodic, int stage=1){
        gt::start("sldg2d_total");

        gt::start("sldg2d_compute_upstream_mesh");
        compute_upstream_mesh_cc_on_gpu(velocity_field.dtheta_phi_cc.d_data, 
                                        velocity_field.dr_phi_cc.d_data, 
                                        velocity_field.dtheta_phi.d_data, 
                                        velocity_field.dr_phi.d_data, 
                                        dt, stage);
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_compute_upstream_mesh");

        //std::ofstream vs_out("data_vs.data");
        //for(int j=0;j<Ny;++j){
        //    for(int i=0;i<Nx;++i) {
        //        vs_out << std::setprecision(16);
        //        vs_out << vs(i,j).x << " " << vs(i,j).y << endl;
        //    }
        //}
        //std::ofstream upst_node("data_un.data");
        //for(int j=0;j<Ny;++j){
        //    for(int i=0;i<Nx;++i){
        //        for(int k=0;k<o*o;++k){
        //            upst_node << std::setprecision(16);
        //            upst_node << un(i,j).v[k][0] << " "
        //                      << un(i,j).v[k][1] << endl;
        //        }
        //    }
        //}

        gt::start("sldg2d_outer_segments");
        get_outer_segments();
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_outer_segments");

        gt::start("sldg2d_inner_segments");
        get_inner_segment_points();
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_inner_segments");

        gt::start("sldg2d_outer_segments");
        complete_outer_segments();
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_outer_segments");

        gt::start("sldg2d_adjoint_s");
        compute_adjoint_solutions();
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_adjoint_s");

        gt::start("sldg2d_integrals");
        compute_integrals();
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_integrals");

        gt::start("sldg2d_finalize");
        finalize(in,out,Nz,bcx,bcy);
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("sldg2d_finalize");

        gt::stop("sldg2d_total");
    }


    //template<typename T, typename f>
    //void translate_nl(double* in, double* out, double* rk_stage, T& poisson,
    //                  double final_T, double dt, double c, bool divide, f g, 
    //                  double mass0){

    //    int n_steps = ceil(final_T/dt);
    //    cout << "\n\nN: " << Nx << ", final time: " << final_T 
    //         << ", step size: " << dt
    //         << ", number of time steps: " << n_steps << endl;

    //    double t=0.0;
    //    timer t_poisson, t_cum, t_gos, t_gis, t_cos, t_ci, t_f, t_cas;
    //    for(int ii=0;ii<n_steps;++ii){

    //        if(t+dt>final_T)
    //            dt = final_T-t;

    //        t+=dt;

    //        t_poisson.start();
    //        poisson.solve(in,c,g,divide,ii);
    //        t_poisson.stop();

    //        t_cum.start();
    //        compute_upstream_mesh_cc(poisson,dt,1);
    //        t_cum.stop();

    //        t_gos.start();
    //        get_outer_segments();
    //        t_gos.stop();

    //        t_gis.start();
    //        get_inner_segment_points();
    //        t_gis.stop();

    //        t_cos.start();
    //        complete_outer_segments();
    //        t_cos.stop();

    //        t_cas.start();
    //        compute_adjoint_solutions();
    //        t_cas.stop();


    //        t_ci.start();
    //        compute_integrals();
    //        t_ci.stop();

    //        t_f.start();
    //        finalize(in,rk_stage,1);
    //        t_f.stop();

    //        #ifndef NDEBUG
    //        std::ofstream output1("data_aux"+std::to_string(ii+1)+".data");
    //        compute_mass_and_write<o>(ax, ay, hx, hy, Nx, Ny,
    //                                  rk_stage, output1,2);
    //        #endif

    //        //order 2
    //        t_poisson.start();
    //        poisson.solve(rk_stage,c,g,divide,-1);
    //        t_poisson.stop();

    //        t_cum.start();
    //        compute_upstream_mesh_cc(poisson,dt,2);
    //        t_cum.stop();

    //        t_gos.start();
    //        get_outer_segments();
    //        t_gos.stop();

    //        t_gis.start();
    //        get_inner_segment_points();
    //        t_gis.stop();

    //        t_cos.start();
    //        complete_outer_segments();
    //        t_cos.stop();

    //        t_cas.start();
    //        compute_adjoint_solutions();
    //        t_cas.stop();

    //        t_ci.start();
    //        compute_integrals();
    //        t_ci.stop();

    //        t_f.start();
    //        finalize(in,out,1);
    //        t_f.stop();

    //        #ifndef NDEBUG
    //        std::ofstream output("data_out"+std::to_string(ii+1)+".data");
    //        double mass = compute_mass_and_write<o>(ax,ay,hx,hy,Nx,Ny,
    //                                                in,output, 2);
    //        cout << "mass diff: " << mass-mass0 << endl;
    //        #endif

    //        std::swap(in,out);
    //    }
    //    cout << "Timings in milliseconds, longer execution due to writing to file\n";
    //    cout << "solve poisson and differentiate: " << t_poisson.total() << endl
    //         << "get vertex star: " << t_cum.total() << endl
    //         << "get outer segments: " << t_gos.total() << endl
    //         << "get inner segments: " << t_gis.total() << endl
    //         << "complete outer segm: " << t_cos.total() << endl
    //         << "compute adjoint sol: " << t_cas.total() << endl
    //         << "compute integrals: " << t_ci.total() << endl
    //         << "compute phi and sum: " << t_f.total() << endl
    //         << "total: " << t_poisson.total()+t_cum.total()+t_gos.total()+
    //                         t_gis.total()+ t_cos.total()+t_ci.total()+t_f.total() +
    //                         t_cas.total() << endl;


    //}


};

#endif
