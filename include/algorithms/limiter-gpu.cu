#pragma once

#include <algorithms/limiter.hpp>
#include <algorithms/dg-gpu.cu>


//this is stupid, why does my IDE not know this function...?
__device__
void __syncthreads();


//<<<(dof_v1,dof_v2*dof_v3),(n_cells)>>>
template<Index o, size_t d, 
         bool (*is_troubled)(fp*) ,
         void (*modify_troubled_cell)(fp* in,fp* out)>
__global__
void k_weno_limiter_x(fp* data, Index* e, 
                      fp* bdry_left, fp* bdry_right, 
                      location loc){

    extern __shared__ fp s_in[];

    int dof_x1 = e[0]*e[d];
    int dof_v1 = e[1]*e[d+1];
    int Nx = e[0];

    int idx_v1 = blockIdx.x;
    if(idx_v1 >= dof_v1)
        return;

    Index offset = dof_x1*idx_v1 + dof_x1*dof_v1*blockIdx.y;

    data += offset;

    int idx = threadIdx.x;
    //two additional boundary cells to set
    for(int k=0;k<o;++k){
        s_in[idx+o] = data[idx];
        idx += blockDim.x;
    }

    if(threadIdx.x<o){
        if(bdry_left==nullptr && bdry_right==nullptr){
            //just set to zero for zero inflow,
            //outflow is not known and thus neglected in the below
            s_in[threadIdx.x] = 0.0;
            s_in[threadIdx.x + o*(Nx+1)] = 0.0;
        } else{
            size_t bdry_idx = o*idx_v1 + o*dof_v1*blockIdx.y;
            s_in[threadIdx.x] = bdry_left[threadIdx.x + bdry_idx]; 
            s_in[threadIdx.x + o*(Nx+1)] = bdry_right[threadIdx.x + bdry_idx];
        }
    }

    __syncthreads();

    int cellx_idx = threadIdx.x;
    if(loc==location::all && 
       ((cellx_idx==Nx-1 && 2*idx_v1>=dof_v1) ||
        (cellx_idx==0    && 2*idx_v1<dof_v1))){
        //if domain is not split, don't treat right and left boundary
    }else if(loc==location::left && (cellx_idx==0 && 2*idx_v1<dof_v1)){
        //on left outflow do nothing
    }else if(loc==location::right && (cellx_idx==Nx-1 && 2*idx_v1>=dof_v1)){ 
        //on right outflow do nothing
    }else{
    //if(cellx_idx>0 && cellx_idx<Nx-1){
        int idx_io =  o*threadIdx.x; //first of the three cells
        if(is_troubled(&s_in[idx_io])){

            fp loc_out[o];
            modify_troubled_cell(&s_in[idx_io],loc_out);

            for(int k=0;k<o;++k)
                data[idx_io+k] = loc_out[k];
        }
    }
 
}


//<<<(dof_x1/tile_x,n,dof_v2*dof_v3),(tile_x,cells_v1/n)>>>
template<Index o, size_t d, 
         bool (*is_troubled)(fp*) ,
         void (*modify_troubled_cell)(fp* in,fp* out)>
__global__
void k_weno_limiter_v1(fp* data_in, fp* data_out, Index* e){

    int dof_x1 = e[0]*e[d];
    int dof_v1 = e[1]*e[d+1];

    int idx_x1 = threadIdx.x + blockDim.x*blockIdx.x;
    int cell_idx_v1 = threadIdx.y + blockDim.y*blockIdx.y;
    if(idx_x1 >= dof_x1 || cell_idx_v1 >= e[1])
        return;

    data_in += dof_x1*dof_v1*blockIdx.z;
    data_out += dof_x1*dof_v1*blockIdx.z;

    fp loc_in[3*o];
    int cell_idx_v1m1 = fast_mod(cell_idx_v1-1,e[1]);
    int cell_idx_v1p1 = fast_mod(cell_idx_v1+1,e[1]);
    for(int i=0;i<o;++i){
        loc_in[0*o+i] = data_in[idx_x1 + dof_x1*(i + o*cell_idx_v1m1)];
        loc_in[1*o+i] = data_in[idx_x1 + dof_x1*(i + o*cell_idx_v1)];
        loc_in[2*o+i] = data_in[idx_x1 + dof_x1*(i + o*cell_idx_v1p1)];
    }

    fp loc_out[o];
    if(is_troubled(loc_in)){
        modify_troubled_cell(loc_in,loc_out);
    } else{
        for(int k=0;k<o;++k)
            loc_out[k] = loc_in[o+k];
    }

    for(int k=0;k<o;++k)
        data_out[idx_x1 + dof_x1*(k + o*cell_idx_v1)] = loc_out[k];
}



//<<<(dof_v1/threads,dof_v2*dof_v3),threads>>>
template<size_t dv>
__global__
void pack_all_for_limiter_xdim(location lrm, fp* bdry_data_left,
                               fp* bdry_data_right,
                               int ratio, fp* d_in, Index* ext){
    constexpr size_t dx = 1; 
    constexpr size_t d = dx+dv;

    Index dof_v1 = ext[1]*ext[d+1];

    int v1_idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(v1_idx>=dof_v1)
        return;

    Index ox = ext[d];
    Index nx = ext[0];
    Index dof_x1 = nx*ox;

    d_in += dof_x1*(v1_idx + dof_v1*blockIdx.y);
    bdry_data_left += ox*(v1_idx + dof_v1*blockIdx.y);
    bdry_data_right += ox*(v1_idx + dof_v1*blockIdx.y);

    if(lrm==location::left) {
        //zero inflow, outflow cell is not considered in the weno limiter
        for(int k=0;k<ox;++k)
            bdry_data_left[k] = 0.0;
        //projection from fine 2 coarse
        for(int k=0;k<ox;k++){
            fp value=0.0;
            for(int l=0;l<ratio;++l){
                for(int m=0;m<ox;++m){
                    int idx = m+ox*(nx-ratio+l);
                    value += d_in[idx]*gauss::get_w(m,ox)*
                          lagrange_dg_gpu((l+gauss::get_x_scaled01(m,ox))/fp(ratio),
                                              k,ox);
                }
            }
            bdry_data_right[k] = value/(gauss::get_w(k,ox)*ratio);
        }
    }
    if(lrm==location::right) {
        //projection from fine 2 coarse
        for(int k=0;k<ox;k++){
            fp value=0.0;
            for(int l=0;l<ratio;++l){
                for(int m=0;m<ox;++m){
                    int idx = m + ox*l;
                    value += d_in[idx]*gauss::get_w(m,ox)*
                          lagrange_dg_gpu((l+gauss::get_x_scaled01(m,ox))/fp(ratio),
                                              k,ox);
                }
            }
            bdry_data_left[k] = value/(gauss::get_w(k,ox)*ratio);
        }
        //zero inflow, outflow cell is not considered in the weno limiter
        for(int k=0;k<ox;++k)
            bdry_data_right[k] = 0.0;
    }
    if(lrm==location::mid){
        //evaluation of coarse to fine

        //left
        for(int k=0;k<ox;++k) {
            fp value=0.0;
            fp x=gauss::get_x_scaled01(k,ox)/fp(ratio);
            for(int l=0;l<ox;++l){
                fp lag=lagrange_dg_gpu(x,l,ox);
                value += d_in[l]*lag;
            }
            bdry_data_left[k] = value;
        }

        //right
        for(int k=0;k<ox;++k) {
            fp value=0.0;
            fp x=(ratio-1+gauss::get_x_scaled01(k,ox))/fp(ratio);
            for(int l=0;l<ox;++l){
                fp lag = lagrange_dg_gpu(x,l,ox);
                value += d_in[l+ox*(nx-1)]*lag;
            }
            bdry_data_right[k] = value;
        }
    }
}


template<size_t dx, size_t dv>
struct weno_1d_limiter{

    static constexpr size_t d = dx+dv;

    fp* d_bdry_left_to_send=nullptr;
    fp* d_bdry_right_to_send=nullptr;
    fp* d_bdry_left=nullptr;
    fp* d_bdry_right=nullptr;

    gpu_array<Index>& d_e;
    size_t size;

    location loc;

    weno_1d_limiter(gpu_array<Index>& _d_e, location _loc)
        : d_e(_d_e), loc(_loc)
    { }

    ~weno_1d_limiter(){
        gpuErrchk(cudaFree(d_bdry_left_to_send));
        gpuErrchk(cudaFree(d_bdry_right_to_send));
        gpuErrchk(cudaFree(d_bdry_left));
        gpuErrchk(cudaFree(d_bdry_right));
    }


    size_t pack(int ratio, multi_array<fp,2*d>& d_in,
                fp*& bdry_left_snd, fp*& bdry_right_snd,
                fp*& bdry_left_rcv, fp*& bdry_right_rcv){

        if(d_bdry_left_to_send==nullptr){
            //memory is allocated once only if the pack function is called
            cout << "start to initialize memory for pack for limiter on gpu.\n";

            //one cell of the x direction and the whole v-domain.
            size = d_e[d];
            for(size_t i=0;i<dv;++i)
                size *= d_e[dx+i]*d_e[d+dx+i];
            size *= sizeof(fp);

            d_bdry_left_to_send = (fp*)gpu_malloc(size);
            d_bdry_right_to_send = (fp*)gpu_malloc(size);
            d_bdry_left = (fp*)gpu_malloc(size);
            d_bdry_right = (fp*)gpu_malloc(size);
            cout << "memory for pack for limiter on gpu initialized.\n";
        }

        int threads = 128;
        int dof_v1 = d_e[1]*d_e[1+d];
        int dof_v23 = 1;
        for(int i=1;i<dv;++i)
            dof_v23 *= d_e[1+i]*d_e[1+i+d];

        dim3 blocks((dof_v1+threads-1)/threads,dof_v23);
        pack_all_for_limiter_xdim<dv><<<blocks,threads>>>(loc,
                                   d_bdry_left_to_send,
                                   d_bdry_right_to_send,
                                   ratio, d_in.data(), d_e.d_data);

        //on left domain, d_bdry_left = d_bdry_left_to_send
        //on right domain, d_bdry_right = d_bdry_right_to_send
        //i.e., zero inflow is set
        if(loc==location::left){
            std::swap(d_bdry_left_to_send,d_bdry_left);
            bdry_left_snd  = nullptr;
            bdry_left_rcv  = nullptr;
            bdry_right_snd = d_bdry_right_to_send;
            bdry_right_rcv = d_bdry_right;
        }
        if(loc==location::right){
            std::swap(d_bdry_right_to_send,d_bdry_right);
            bdry_left_snd  = d_bdry_left_to_send;
            bdry_left_rcv  = d_bdry_left;
            bdry_right_snd = nullptr;
            bdry_right_rcv = nullptr;
        }
        if(loc==location::mid){
            bdry_left_snd  = d_bdry_left_to_send;
            bdry_left_rcv  = d_bdry_left;
            bdry_right_snd = d_bdry_right_to_send;
            bdry_right_rcv = d_bdry_right;
        }

        return size;
    }


    void apply_weno_limiter_1d_gpu(multi_array<fp,2*(dx+dv)>& in,
                                   multi_array<fp,2*(dx+dv)>& out,
                                   int dim,
                                   troubled_cell_indicator tci,
                                   troubled_cell_modifier tcm){

        if(tci==troubled_cell_indicator::none && tcm==troubled_cell_modifier::none){
            in.swap(out);
            return;
        }
        if(tci==troubled_cell_indicator::none || tcm==troubled_cell_modifier::none){
            in.swap(out);
            cout << "WARNING: both the troubled_cell_indicator (" << tci
                 << ") and the troubled_cell_modifier (" << tcm
                 << ") have to be specified in order to apply the limiter. "
                 << "Thus currently no limiter is used\n";
            return;
        }
        if(dx!=1){
            cout << "ERROR: WENO limiter currently only implemented for dx==1, "
                 << "but dx=" << dx << " is used.\n";
            exit(1);
        }

        int o = d_e[d+dim];
        if(o==1) {
            in.swap(out);
            cout << "NOTE: for piecwise constants no limiter required. "
                 << "Operation skipped\n";
        } else if(o==2){
            limiter_tci_tcm<2>(dim, in, out, tci, tcm);
        } else if(o==3) {
            limiter_tci_tcm<3>(dim, in, out, tci, tcm);
        } else if(o==4) {
            limiter_tci_tcm<4>(dim, in, out, tci, tcm);
        } else {
            cout << "ERROR: WENO limiter not implemented for o="
                 << o << ".\n";
            exit(1);
        }
 
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    };

  private:

    template<Index o>
    void limiter_tci_tcm(int dim, multi_array<fp,2*d>& d_in,
                         multi_array<fp,2*d>& d_out,
                         troubled_cell_indicator tci,
                         troubled_cell_modifier tcm){
 
        if(tci==troubled_cell_indicator::mean_err){
            //<1,2,..> corresponds to a threshold of C=1/2
            limiter_tcm<o,mean_troubled_indicator<1,2,o>>(dim, d_in, d_out, tcm);
        } else if(tci==troubled_cell_indicator::minmod){
            limiter_tcm<o,minmod_troubled_indicator<o>>(dim, d_in, d_out, tcm);
        } else{
            cout << "ERROR: indicator " << tci << " not yet implemented\n";
            exit(1);
        }

    }

    template<Index o, bool (*is_troubled)(fp*)>
    void limiter_tcm(int dim, multi_array<fp,2*d>& d_in,
                     multi_array<fp,2*d>& d_out,
                     troubled_cell_modifier tcm){

        if(tcm==troubled_cell_modifier::average){
            k_limiter_dim<o,is_troubled,modifier_average<o>>(dim, d_in, d_out);

        } else if(tcm==troubled_cell_modifier::simple_weno){
            k_limiter_dim<o,is_troubled,modifier_simple_weno<o>>(dim, d_in, d_out);

        } else if(tcm==troubled_cell_modifier::linear_weno){
            //<998,1000> corresponds to gamma_0=0.1, gamma_{-1,1}=0.45
            k_limiter_dim<o,is_troubled,modifier_linear_weno<1,10,o>>(
                                                       dim, d_in, d_out);

        } else if(tcm==troubled_cell_modifier::hweno){
            k_limiter_dim<o,is_troubled,modifier_hweno<o>>(dim, d_in, d_out);

        } else {
            cout << "ERROR: modifier " << tcm << " not yet implemented.\n";
            exit(1);
        }
    }

    template<Index o,
             bool (*is_troubled)(fp*) ,
             void (*modify_troubled_cell)(fp* in,fp* out)>
    void k_limiter_dim(int dim, multi_array<fp,2*d>& d_in,
                       multi_array<fp,2*d>& d_out){

            int dof_x1 = d_e[0]*d_e[d];
            int dof_v1 = d_e[1]*d_e[d+1];
            int dof_v23 = 1;
            for(size_t i=2;i<d;++i)
                dof_v23 *= d_e[i]*d_e[d+i];

            if(dim==0){

                if(d_e[d+dim]>d_e[dim]){
                    cout << "ERROR: number of cells must be larger than "
                         << "values per cell." << endl;
                    exit(1);
                }

                //<<<(dof_v1,dof_v2*dof_v3),(n_cells)>>>
                dim3 blocks(dof_v1,dof_v23);
                dim3 threads(d_e[0]);

                int shared_mem_size = (d_e[0]+2)*o*sizeof(fp);

                k_weno_limiter_x<o,d,is_troubled,modify_troubled_cell>
                    <<<blocks,threads,shared_mem_size>>>(d_in.data(),d_e.d_data,
                                                     d_bdry_left,d_bdry_right,loc);
                d_in.swap(d_out);

            } else if(dim==1){

                //<<<(dof_x1/tile_x,n,dof_v2*dof_v3),(tile_x,cells_v1/n)>>>
                int tile_x = 16;
                int tile_v = 16;
                int parts_x = (dof_x1+tile_x-1)/tile_x;
                int parts_v = (d_e[1]+tile_v-1)/tile_v;
                dim3 blocks(parts_x,parts_v,dof_v23);
                dim3 threads(tile_x,tile_x);

                k_weno_limiter_v1<o,d,is_troubled,modify_troubled_cell>
                    <<<blocks,threads>>>(d_in.data(), d_out.data(), d_e.d_data);
           } else {

               cout << "ERROR: dim>1 in limiter\n";
               exit(1);

           }
    }

};

