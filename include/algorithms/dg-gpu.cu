#pragma once

#include <generic/common.hpp>
#include <algorithms/dg.hpp>

#define UNUSED(x)   x##_UNUSED __attribute__((unused))

template<size_t dx, size_t dv, size_t dim, Index o>
__global__
void k_translate(fp* in, fp* out, fp* A, fp *B, int *_shift,
        int offx1, Index* ext, int bdr_len, fp* boundary_data,
        int* sign, int max_bdr_left, bool sldg_limiter, 
        int driftkinetic) {

    vec3 tid(threadIdx.x,threadIdx.y,threadIdx.z);
    vec3 bid(blockIdx.x,blockIdx.y,blockIdx.z);
    vec3 bdim(blockDim.x,blockDim.y,blockDim.z);
    vec3 gdim(gridDim.x,gridDim.y,gridDim.z);

    extern __shared__ char shared_mem[];
    Index o_sq = o*o;
    fp *s_A, *s_B;
    if((dim < dx && driftkinetic==0) || driftkinetic==3) {
        s_A = (fp*)shared_mem;
        s_B = (fp*)shared_mem + o_sq;
    } else {
        int tile_x = blockDim.x;
        s_A = (fp*)shared_mem + o_sq*threadIdx.x;
        s_B = (fp*)shared_mem + o_sq*threadIdx.x + o_sq*tile_x;
    }

    translate<dx,dv,dim,o>(in,out,A,B,_shift,offx1,tid,bid,bdim,gdim,
        s_A,s_B,ext,bdr_len,boundary_data,sign,max_bdr_left,sldg_limiter,
        driftkinetic);
}


template<size_t dx, size_t dv, size_t dim>
void translate_gpu_exec_kernel_o(int o, dim3 gd, dim3 bd, int shared_mem_size,
        fp* in, fp* out, fp* A, fp *B, int *shift, int bx, Index* d_ext, 
        int bdr_len, fp* boundary_data, int* sign, int max_bdr_left, 
        bool sldg_limiter, int driftkinetic=0) {

    if(o==1)
        k_translate<dx,dv,dim,1><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else if(o==2)
        k_translate<dx,dv,dim,2><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else if(o==3)
        k_translate<dx,dv,dim,3><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else if(o==4)
        k_translate<dx,dv,dim,4><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else if(o==5)
        k_translate<dx,dv,dim,5><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else if(o==6)
        k_translate<dx,dv,dim,6><<<gd,bd,shared_mem_size>>>(
                in,out,A,B,shift,bx,d_ext,bdr_len,boundary_data,
                sign,max_bdr_left,sldg_limiter,driftkinetic);
    else {
        cout << "ERROR: only o=1,2,3,4,5,6 currently implemented" << endl;
        exit(1);
    }

};

template<size_t dim>
void translate_gpu_exec_kernel(Index o, size_t dx, size_t dv, dim3 gd, dim3 bd,
        int shared_mem_size, fp* in, fp* out, fp* A, fp *B, int *shift, int bx, 
        Index* d_ext, int bdr_len, fp* boundary_data, int* sign, int max_bdr_left,
        bool sldg_limiter, int driftkinetic=0){

    if(dx==1 && dv==1)
        translate_gpu_exec_kernel_o<1,1,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left, 
                sldg_limiter);
    else if(dx==1 && dv==3)
        translate_gpu_exec_kernel_o<1,3,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left,
                sldg_limiter);
    else if(dx==2 && dv==2)
        translate_gpu_exec_kernel_o<2,2,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left,
                sldg_limiter);
    else if(dx==2 && dv==3)
        translate_gpu_exec_kernel_o<2,3,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left,
                sldg_limiter);
    else if(dx==3 && dv==3)
        translate_gpu_exec_kernel_o<3,3,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left,
                sldg_limiter);
    else if(dx==3 && dv==1){
        //driftkinetic configuration
        shared_mem_size = 2*o*o*sizeof(fp)*((driftkinetic==3) ? 1 : bd.x);
        translate_gpu_exec_kernel_o<3,1,dim>(o, gd, bd, shared_mem_size, in, out, 
                A, B, shift, bx, d_ext, bdr_len, boundary_data, sign, max_bdr_left,
                sldg_limiter,driftkinetic);
    }
    else {
        cout << "ERROR: only dx=dv=1, dx=dv=2, dx=dv=3, dx=1,dv=3, dx=2,dv=3 "
             << "is currently implemented." << endl;
        exit(1);
    }
}

enum class ga_type {
    all, no_host
};

template<typename T>
struct gpu_array {
    Index length;
    vector<T> h_data;
    T* d_data;

    int device_id;
    ga_type gat;

    gpu_array(Index _length, ga_type _gat=ga_type::all)
        : length(_length), gat(_gat), d_data(nullptr) {
        if(gat == ga_type::all)
            h_data.resize(length);
        d_data = (T*)gpu_malloc(sizeof(T)*length);
        cudaGetDevice(&device_id);
    }

    ~gpu_array(){
        gpuErrchk(cudaFree(d_data)); 
    }

    void resize(Index _length) {
        check_device();

        length = _length;

        if(gat == ga_type::all)
            h_data.resize(length);

        if(d_data != nullptr)
            cudaFree(d_data);
        d_data = (T*)gpu_malloc(sizeof(T)*length);
    }

    template<typename it>
    void set(it first, it last) {
        Index len = last-first;
        if(len != length) {
            cout << "ERROR: set called with iterator of length != container "
                 << "length." << endl;
            exit(1);
        }
        copy(first, last, begin(h_data));
    }

    Index size() {
        return length;
    }

    T* d_ptr() {
        check_device();
        if(gat == ga_type::all)
            copy_to_gpu();
        return d_data;
    }

    T& operator[](Index i) {
        return h_data[i];
    }

    void copy_to_gpu() {
        if((Index)h_data.size() != size()) {
            cout << "ERROR: gpu_array::copy_to_gpu without an argument requires"
                 << " that size()==h_data.size()" << endl;
            exit(1);
        }
        check_device();
        cudaMemcpy(d_data, &h_data[0], sizeof(T)*size(),
                cudaMemcpyHostToDevice);
    }

    void copy_to_gpu(T* ptr, Index len) {
        if(size() < len) {
            cout << "ERROR: gpu_array::copy_to_gpu with two arguments requires "
                 << " that size()==len (size=" << size() << " len=" << len 
                 << ")" << endl;
            exit(1);
        }
        check_device();
        cudaMemcpy(d_data, ptr, sizeof(T)*len, cudaMemcpyHostToDevice);
    }

    void copy_to_host() {
        if((Index)h_data.size() != size()) {
            cout << "ERROR: gpu_array::copy_to_host requires"
                 << " that size()==h_data.size()" << endl;
            exit(1);
        }
        check_device();
        cudaMemcpy(&h_data[0], d_data, sizeof(T)*size(), cudaMemcpyDeviceToHost);
    }

    void check_device() {
        int current_device_id;
        cudaGetDevice(&current_device_id);
        if(current_device_id != device_id) {
            cout << "ERROR: d_ptr() is called from a different device." << endl;
            exit(1);
        }
    }
};


template<size_t dx, size_t dv, size_t d_adv>
struct translate_gpu {
        
    static constexpr size_t d = dx+dv;

    gpu_array<Index> ext;
    Index bdr_max_len;
    const bool sldg_limiter;

    translate_gpu(index_dxdv_no<dx,dv> e, Index _bdr_max_len, 
                  bool _sldg_limiter=false)
        : ext(2*d), bdr_max_len(_bdr_max_len), 
          sldg_limiter(_sldg_limiter)
    {
            copy(e.begin(), e.end(), &ext[0]);
    }

    void translate(Index dim, fp UNUSED(tau), fp UNUSED(h_cell), 
            dg_coeff_generic<d_adv>& dg_c, multi_array<fp,2*(dx+dv)>& d_in, 
            multi_array<fp,2*(dx+dv)>& d_out, int bdr_len, 
            int UNUSED(bdr_tot_len), fp* d_bdry_data, bool UNUSED(bdr_on_gpu), 
            int* d_sign, int max_bdr_left, int driftkinetic=0) {

        if(ext[d+dim]!=4 && sldg_limiter==true)
            cout << "WARNING: limiter is currently only applied with o=4.\n";

        timer tt;
        tt.start();

        array<Index,2*d> e;
        copy(&ext[0], &ext[0]+2*d, begin(e));

        if(!d_in.gpu || !d_out.gpu) {
            cout << "ERROR: for translate_gpu both input and output must reside on "
                 << "the GPU" << endl;
            exit(1);
        }

        Index o = e[d+dim];
        Index o_sq = o*o;
        if(e[dim]*8 < o_sq) {
            cout << "ERROR: current implementation requires e[dim]*8>=e[d+dim]^2" 
                 << endl;
            exit(1);
        }

        #ifdef USE_SINGLE_PRECISION
        array<Index,6> tile_x = {( (d==6) ? 8 : 1), 16, 16, 16, 16, 16};
        #else
        array<Index,6> tile_x = {( (d==6) ? 4 : 1), 16, 16, 16, 16, 16};
        #endif
        while(tile_x[dim] > 1 && e[dim]*tile_x[dim] > 1024)
            tile_x[dim] /= 2;

        array<Index,3> b = create_grid<d>(dim, e, tile_x[dim]);
        dim3 gd( ((d==6) ? b[0]*b[1]*b[2] : b[0]), 
                 ((d==6) ? 1 : b[1]), 
                 ((d==6) ? 1 : b[2]) );
        dim3 bd(tile_x[dim],e[dim]);
        if(d==6 && dim==0){
            bd.z=bd.x;
            bd.x=1;
        }

        if(e[dim]*tile_x[dim] > 1024)
            cout << "WARNING: more than 1024 threads launched per block. "
                 << "Might not be supported by the hardware." << endl;

        int shared_mem_size = 2*o_sq*sizeof(fp)*((dim<Index(dx)) ? 1 : tile_x[dim]);

        gt::start(str(boost::format("translate_gpu_dim%1%")%dim));
        if(dim==0)
            translate_gpu_exec_kernel<0>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(), 
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(),  bdr_len, d_bdry_data,
                    d_sign, max_bdr_left, sldg_limiter);
        else if(dim==1)
            translate_gpu_exec_kernel<1>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(), 
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(), bdr_len, d_bdry_data, 
                    d_sign, max_bdr_left, sldg_limiter);
        else if(dim==2)
            translate_gpu_exec_kernel<2>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(),
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(), bdr_len, d_bdry_data, 
                    d_sign, max_bdr_left, sldg_limiter, driftkinetic);
        else if(dim==3)
            translate_gpu_exec_kernel<3>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(),
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(), bdr_len, d_bdry_data, 
                    d_sign, max_bdr_left, sldg_limiter, driftkinetic);
        else if(dim==4)
            translate_gpu_exec_kernel<4>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(),
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(), bdr_len, d_bdry_data, 
                    d_sign, max_bdr_left, sldg_limiter);
        else if(dim==5)
            translate_gpu_exec_kernel<5>(o, dx, dv, gd,
                    bd, shared_mem_size, d_in.data(),
                    d_out.data(), dg_c.pd_A(), dg_c.pd_B(), dg_c.pd_shift(), 
                    e[0]*e[d+0], ext.d_ptr(), bdr_len, d_bdry_data, 
                    d_sign, max_bdr_left, sldg_limiter);
        else {
            cout << "ERROR: currently only dim<=5 implemented." << endl;
            exit(1);
        }
        cudaDeviceSynchronize(); // this is only to make time measurements sensible
        gpuErrchk( cudaPeekAtLastError() );

        tt.stop();
        //cout << "Timing(GPU): dg_c.comp (dim=" << dim << "): " << tt.total() << " " 
        //     << " bdr_len " << bdr_len << endl;

        gt::stop(str(boost::format("translate_gpu_dim%1%")%dim));
    }

};


template<size_t dx, size_t dv, size_t dim>
__global__
void k_pack_boundary(fp* in, int *_sign, int bdr_len, fp* bdry_to,
        int nv_dim, int ov_dim, int* ext, int max_bdry_left) {
    
    constexpr size_t d = dx+dv;
    
    // Determine the index the current thread works on and the corresponding
    // offset into the input and output arrays
    int i[d], off[d];
    for(size_t k = 0; k < d; k++)
        off[k] = ext[k]*ext[k+d];

    int combined_index = blockIdx.x;
    int divisor = ((d==2) ? 1 : gridDim.x/( (dim==d-1) ? off[d-2] : off[d-1] ));
    int m = 0;
    for(int k = d-1; k >= 0; k--) {
        if(k == dim) {
            i[k] = threadIdx.y;
            m++;
        } else {
            if (k==1-m) {
                i[k] = threadIdx.x + combined_index*blockDim.x;
                if (i[k] >= off[k])
                    return;
            } else {
                i[k] = combined_index/divisor;
                combined_index -= i[k]*divisor;
                divisor /= (off[k-1-(k-1==dim)]);
            }
        }
    }

    int offset_s, bdry_offset_dim, bdry_stride_dim;
    size_t offset_in = get_bdry_info<dx,dv,dim>(i, off, offset_s, 
                               bdry_offset_dim, bdry_stride_dim);

    in += offset_in;

    int sign = _sign[offset_s];

    size_t stride_in=1;
    for(size_t k=0;k<dim;++k)
        stride_in *= off[k];
 
    fp* local_bdry = bdry_to + bdr_len*ov_dim*bdry_offset_dim;
    if(sign < 0) {
        local_bdry += bdr_len*ov_dim*bdry_stride_dim*(abs(sign)-1);
        for(int k=0;k<ov_dim;k++)
            local_bdry[ov_dim*i[dim]+k] = in[(ov_dim*i[dim]+k)*stride_in];
    } else {
        local_bdry += bdr_len*ov_dim*bdry_stride_dim*(sign-1 + max_bdry_left);
        for(int k=0;k<ov_dim;k++)
            local_bdry[ov_dim*i[dim]+k] = in[(ov_dim*(nv_dim-1-i[dim])+k)
                *stride_in];
    }

}


__device__
fp lagrange_gpu(fp x, int j, int o, fp* gx){
    fp prod = 1.0;;
    for(int i=0;i<j;++i) prod*=(x-gx[i])/(gx[j]-gx[i]);
    for(int i=j+1;i<o;++i) prod*=(x-gx[i])/(gx[j]-gx[i]);
    return prod;
}


template<size_t dx, size_t dv, size_t dim>
__global__
void k_pack_boundary_f2c(fp* in, int *_sign, int bdr_len, fp* bdry_to,
        int nv_dim, int ov_dim, int* ext, int max_bdry_left,
        int ratio, location const lr, fp* gauss_x, fp* gauss_w) {

    constexpr size_t d = dx+dv;

    // Determine the index the current thread works on and the corresponding
    // offset into the input and output arrays
    int i[d], off[d];
    for(size_t k = 0; k < d; k++)
        off[k] = ext[k]*ext[k+d];

    int combined_index = blockIdx.x;
    int divisor = ((d==2) ? 1 : gridDim.x/( (dim==d-1) ? off[d-2] : off[d-1] ));
    int m = 0;
    for(int k = d-1; k >= 0; k--) {
        if(k == dim) {
            i[k] = threadIdx.y;
            m++;
        } else {
            if (k==1-m) {
                i[k] = threadIdx.x + combined_index*blockDim.x;
                if (i[k] >= off[k])
                    return;
            } else {
                i[k] = combined_index/divisor;
                combined_index -= i[k]*divisor;
                divisor /= (off[k-1-(k-1==dim)]);
            }
        }
    }

    int offset_s, bdry_offset_dim, bdry_stride_dim;
    size_t offset_in = get_bdry_info<dx,dv,dim>(i, off, offset_s,
                               bdry_offset_dim, bdry_stride_dim);

    in += offset_in;

    int sign = _sign[offset_s];

    size_t stride_in=1;
    for(size_t k=0;k<dim;++k)
        stride_in *= off[k];

    fp* local_bdry = bdry_to + bdr_len*ov_dim*bdry_offset_dim;
    if(sign < 0) {
        local_bdry += bdr_len*ov_dim*bdry_stride_dim*(abs(sign)-1);

        if(lr == location::left){
            for(int k=0;k<ov_dim;k++)
                local_bdry[ov_dim*i[dim]+k] = 0.0;
        } else {
            for(int k=0;k<ov_dim;k++){
                //local_bdry[ov_dim*i[dim]+k] = in[(ov_dim*i[dim]+k)*stride_in];
                fp value=0.0;
                for(int l=0;l<ratio;++l){
                    for(int m=0;m<ov_dim;++m){
                        int idx = (m + ov_dim*(l + i[dim]*ratio))*stride_in;
                        value += in[idx]*gauss_w[m]*
                                 lagrange_gpu((l+gauss_x[m])/fp(ratio),
                                              k,ov_dim,gauss_x);
                    }
                }
                local_bdry[ov_dim*i[dim]+k] = value/(gauss_w[k]*ratio);
            }
        }
    } else {
        local_bdry += bdr_len*ov_dim*bdry_stride_dim*(sign-1 + max_bdry_left);

        if(lr == location::right){
            for(int k=0;k<ov_dim;k++)
               local_bdry[ov_dim*i[dim]+k] = 0.0;
        } else{
            for(int k=0;k<ov_dim;k++){
                //local_bdry[ov_dim*i[dim]+k] = in[(ov_dim*(nv_dim-1-i[dim])+k)
                //    *stride_in];
                fp value=0.0;
                for(int l=0;l<ratio;++l){
                    for(int m=0;m<ov_dim;++m){
                        int idx = (m+ov_dim*(nv_dim-ratio*(1+i[dim])+l))*stride_in;
                        value += in[idx]*gauss_w[m]*
                                 lagrange_gpu((l+gauss_x[m])/fp(ratio),
                                              k,ov_dim,gauss_x);
                    }
                }
                local_bdry[ov_dim*i[dim]+k] = value/(gauss_w[k]*ratio);
            }
        }
    }

}



//<<<(dof_v1/128),(bdr_len_coarse,128)>>>
template<size_t o>
__global__
void k_unpack_coarse2fine(fp* d_bdry_data_in, fp* d_bdry_data_out, 
                          int ratio, int nx, int dof_v1,
                          int* psign, int max_bdry_left, int bdr_len){

    //int bdr_len = blockDim.x;

    int idx_v1 = threadIdx.y + blockDim.y*blockIdx.x;
    if(idx_v1>=dof_v1)
        return;

    int signv = psign[idx_v1];

    int bl = threadIdx.x;
    int bdr_len_coarse = blockDim.x;

    fp hx = 1.0/fp(ratio);

    int istar = (signv>0) ? -bl-1 : bl+nx;
    fp* local_bdry_in = unpack_sorted_bdry(signv, 0,
                      1, bdr_len_coarse, istar, o, nx,
                      max_bdry_left, d_bdry_data_in);

    fp loc_in[o];
    for(Index j=0;j<o;++j)
        loc_in[j] = local_bdry_in[j];

    for(int r=0;r<ratio;++r){
        int _istar = (signv>0) ? -r-1-bl*ratio : r+nx+bl*ratio;
        fp* local_bdry_out = unpack_sorted_bdry(signv, 0,
                          1, bdr_len, _istar, o, nx,
                          max_bdry_left, d_bdry_data_out);
        for(Index j=0;j<o;++j){

            int ridx = r;
            if(signv>0)
                ridx = ratio-1-r;

            fp x = hx*(ridx + gauss::get_x_scaled01(j,o));

            fp value=0.0;
            for(int s=0;s<o;++s)
                value += loc_in[s]*lagrange_dg_gpu(x,s,o);
            local_bdry_out[j] = value;
        }
    }

}


//implemented for testing purpose only
template<size_t dx, size_t dv, size_t dim>
__global__
void k_resort_boundary(fp* bdry_to_in, int *_sign, int bdr_len, fp* bdry_to_out,
                       int UNUSED(nv_dim), int UNUSED(offx1), Index ov_dim, 
                       int* ext, int max_bdry_left) {
    
    constexpr size_t d = dx+dv;
    
    // Determine the index the current thread works on and the corresponding
    // offset into the input and output arrays
    int i[d], off[d];
    for(size_t k = 0; k < d; k++)
        off[k] = ext[k]*ext[k+d];

    int combined_index = blockIdx.x;
    int divisor = ((d==2) ? 1 : gridDim.x/( (dim==d-1) ? off[d-2] : off[d-1] ));
    int m = 0;
    for(int k = d-1; k >= 0; k--) {
        if(k == dim) {
            i[k] = threadIdx.y;
            m++;
        } else {
            if (k==1-m) {
                i[k] = threadIdx.x + combined_index*blockDim.x;
                if (i[k] >= off[k])
                    return;
            } else {
                i[k] = combined_index/divisor;
                combined_index -= i[k]*divisor;
                divisor /= (off[k-1-(k-1==dim)]);
            }
        }
    }

    size_t stride_xbdry=1, off_xbdry=0;
    size_t stride_vbdry=1, off_vbdry=0;
    for(size_t k=0;k<dx;++k){
        if(k!=dim){
            off_xbdry += i[k]*stride_xbdry;
            stride_xbdry *= off[k];
        }
    }
    for(size_t k=dx;k<d;++k){
        if(k!=dim){
            off_vbdry += i[k]*stride_vbdry;
            stride_vbdry *= off[k];
        }
    }

    int offset_s = ((dim<dx) ? off_vbdry : off_xbdry);
    int sign = _sign[offset_s];
        
    size_t stride_in=1;
    for(size_t k=0;k<dim;++k)
        stride_in *= off[k];

    int bdry_offset_out = off_xbdry + stride_xbdry*off_vbdry;
    fp* local_bdry_out = bdry_to_out + bdr_len*ov_dim*bdry_offset_out;
  
    int bdry_offset_dim = ((dim<dx) ? off_xbdry : off_vbdry );
    int bdry_stride_dim = ((dim<dx) ? stride_xbdry : stride_vbdry );
    fp* local_bdry_in = bdry_to_in + bdr_len*ov_dim*bdry_offset_dim;
    
    if(sign < 0) {
        local_bdry_in += bdr_len*ov_dim*bdry_stride_dim*(abs(sign)-1);
        for(int k=0;k<ov_dim;k++)
            local_bdry_out[ov_dim*i[dim]+k] = local_bdry_in[ov_dim*i[dim]+k];
    } else {
        local_bdry_in += bdr_len*ov_dim*bdry_stride_dim*(sign-1 + max_bdry_left);
        for(int k=0;k<ov_dim;k++)
            local_bdry_out[ov_dim*i[dim]+k] = local_bdry_in[ov_dim*i[dim]+k];
    }
    
}


template<size_t d_adv>
struct sign_gpu {
    vector<int> sign;
    int *d_sign;

    Index N;
    int max_idx_left{};
    int max_idx_right{};
    
    sign_gpu(array<Index,2*d_adv> x_or_v_part) : N(prod(x_or_v_part)) {
        sign.resize(N);
        d_sign = (int*)gpu_malloc(sizeof(int)*N);
    }

    ~sign_gpu() {
        cudaFree(d_sign);
    }

    void compute(multi_array<fp,2*d_adv>& F) {        

        int idx_left=0;
        int idx_right=0;
        array<Index,2*d_adv> e = F.shape();
        for(auto i : range<2*d_adv>(e)) {
            Index offset
                = linearize_idx(to_memorder<d_adv>(i),to_memorder<d_adv>(e));
            sign[offset] = (F(i) >= 0.0) ? ++idx_right : --idx_left;
        }
        max_idx_left  = abs(idx_left);
        max_idx_right = idx_right;
    }

    void copy_to_gpu() {
        cudaMemcpy(d_sign, &sign[0], sizeof(int)*N, cudaMemcpyHostToDevice);
    }
};


template<size_t dx, size_t dv>
struct pack_boundary_from_gpu {
        
    static constexpr size_t d = dx+dv;

    Index bdr_max_len;
    gpu_array<int> ext;
    array<Index,2*d> e;

    pack_boundary_from_gpu(index_dxdv_no<dx,dv> _e, Index _bdr_max_len) 
        : bdr_max_len(_bdr_max_len), ext(2*d) {
        
        e = _e.get_array();
        copy(begin(e),end(e),&ext[0]);
    }

    
    void pack(Index dim, int* d_sign, multi_array<fp,2*(dx+dv)>& d_in, int bdr_len, 
              fp* d_bdr_to, int bdr_max_left) {

        gt::start("pack_boundary");
        if(!d_in.gpu) {
            cout << "ERROR: for pack_boundary_from_gpu the input must reside on "
                    "the GPU" << endl;
            exit(1);
        }

        timer t1; 
        t1.start();

        int tile_x = 32;
        array<Index,3> b = create_grid<d>(dim, e, tile_x);
        int blocks = prod(b);
        dim3 gd(blocks);
        dim3 bd(tile_x,bdr_len);

        if(dim == 0)
            k_pack_boundary<dx,dv,0><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else if(dim == 1)
            k_pack_boundary<dx,dv,1><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else if(dim == 2)
            k_pack_boundary<dx,dv,2><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else if(dim == 3)
            k_pack_boundary<dx,dv,3><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else if(dim == 4)
            k_pack_boundary<dx,dv,4><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else if(dim == 5)
            k_pack_boundary<dx,dv,5><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left);
        else {
            cout << "ERROR: pack_boundary_from_gpu is currently only implemented "
                 << "for dim=0,1,2,3,4,5." << endl;
        }
        cudaDeviceSynchronize();
        gpuErrchk( cudaPeekAtLastError() );

        t1.stop();
        cout << "pack_boundary_from_gpu: " << t1.total() << endl;
        
        gt::stop("pack_boundary");
    }


    //by setting ratio=1, this function does something similar as the
    //pack function above. Should I merge these two functions..?
    void pack_fine2coarse(Index dim, int* d_sign, multi_array<fp,2*(dx+dv)>& d_in,
                          int bdr_len, fp* d_bdr_to, int bdr_max_left,
                          int ratio, location lr) {

        int o = e[d+dim];
        gpu_array<fp> gauss_x(o);
        gpu_array<fp> gauss_w(o);
        for(int i=0;i<o;++i){
            gauss_x[i] = gauss::x_scaled01(i,o);
            gauss_w[i] = gauss::w(i,o);
        }

        gt::start("pack_boundary_fine2coarse");
        if(!d_in.gpu) {
            cout << "ERROR: for pack_boundary_from_gpu_fine2coarse "
                 << "the input must reside on the GPU" << endl;
            exit(1);
        }

        timer t1;
        t1.start();

        int tile_x = 32;
        array<Index,3> b = create_grid<d>(dim, e, tile_x);
        int blocks = prod(b);
        dim3 gd(blocks);
        dim3 bd(tile_x,bdr_len);

        if(dim == 0)
            k_pack_boundary_f2c<dx,dv,0><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else if(dim == 1)
            k_pack_boundary_f2c<dx,dv,1><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else if(dim == 2)
            k_pack_boundary_f2c<dx,dv,2><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else if(dim == 3)
            k_pack_boundary_f2c<dx,dv,3><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else if(dim == 4)
            k_pack_boundary_f2c<dx,dv,4><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else if(dim == 5)
            k_pack_boundary_f2c<dx,dv,5><<<gd,bd>>>(d_in.data(),d_sign,bdr_len,
                    d_bdr_to,e[dim],e[d+dim],ext.d_ptr(),bdr_max_left,ratio,lr,
                    gauss_x.d_ptr(),gauss_w.d_ptr());
        else {
            cout << "ERROR: pack_boundary_from_gpu is currently only implemented "
                 << "for dim=0,1,2,3,4,5." << endl;
        }
        cudaDeviceSynchronize();
        gpuErrchk( cudaPeekAtLastError() );

        t1.stop();
        cout << "pack_boundary_from_gpu_f2c: " << t1.total() << endl;

        gt::stop("pack_boundary_fine2coarse");
    }


    void unpack_coarse2fine(Index dim, fp* d_bdr_data, 
                            int bdr_len_coarse, int bdr_len, int ratio,
                            int* d_sign, Index _bdr_max_len, int max_bdry_left) {

        if(dim!=0 || dx!=1){
            cout << "ERROR: currently only implemented for dim==0, "
                 << "dx=1." << endl;
            exit(1);
        }
        if(bdr_max_len != _bdr_max_len){
            cout << "ERROR: differences in bdr_max_len (" << bdr_max_len 
                 << ", " << _bdr_max_len << ")\n.";
            exit(1);
        }

        gt::start("unpack_boundary_coarse2fine");

        int o = e[d+dim];

        gpu_array<fp> gauss_x(o);
        gpu_array<fp> gauss_w(o);
        for(int i=0;i<o;++i){
            gauss_x[i] = gauss::x_scaled01(i,o);
            gauss_w[i] = gauss::w(i,o);
        }

        int dof_v1 = e[1]*e[d+1];
        int dof_v23= 1;
        for(int i=1;i<dv;++i)
            dof_v23 *= e[i]*e[d+1];

        int num_ext_threads = 128;
        dim3 threads(bdr_len_coarse,num_ext_threads);
        dim3 blocks((dof_v1+num_ext_threads-1)/num_ext_threads,dof_v23);

        //I have to decide how I transfer the data for this value
        size_t offset = bdr_max_len*ratio;

        if(o==1)
            k_unpack_coarse2fine<1><<<blocks,threads>>>(
                          d_bdr_data+offset, d_bdr_data, 
                          ratio, e[0], dof_v1,
                          d_sign, max_bdry_left, bdr_len);
        else if(o==2)
            k_unpack_coarse2fine<2><<<blocks,threads>>>(
                          d_bdr_data+offset, d_bdr_data, 
                          ratio, e[0], dof_v1,
                          d_sign, max_bdry_left, bdr_len);
        else if(o==3)
            k_unpack_coarse2fine<3><<<blocks,threads>>>(
                          d_bdr_data+offset, d_bdr_data, 
                          ratio, e[0], dof_v1,
                          d_sign, max_bdry_left, bdr_len);
        else if(o==4)
            k_unpack_coarse2fine<4><<<blocks,threads>>>(
                          d_bdr_data+offset, d_bdr_data, 
                          ratio, e[0], dof_v1,
                          d_sign, max_bdry_left, bdr_len);
        else {
            cout << "ERROR: unpack boundary coarse to fine not yet "
                 << "implemented for o>4.\n";
            exit(1);
        }
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());

        gt::stop("unpack_boundary_coarse2fine");
    }



    //for testing purpose only
    void rearrange(Index dim, int bdr_len, int bdr_tot_len, fp* d_bdr_to_in,
                   int* d_sign, int max_idx_left){

        int tile_x = 32;
        array<Index,3> b = create_grid<d>(dim, e, tile_x);
        int blocks = prod(b);
        dim3 gd(blocks);
        dim3 bd(tile_x,bdr_len);

        gpu_array<fp> xxx(bdr_max_len, ga_type::no_host);
        fp* d_bdr_to_out = xxx.d_data;

        timer trearrange;
        trearrange.start();
        if(dim == 0)
            k_resort_boundary<dx,dv,0><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],
                    ext.d_ptr(),max_idx_left);
        else if(dim == 1)
            k_resort_boundary<dx,dv,1><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],ext.d_ptr(),
                    max_idx_left);
        else if(dim == 2)
            k_resort_boundary<dx,dv,2><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],ext.d_ptr(),
                    max_idx_left);
        else if(dim == 3)
            k_resort_boundary<dx,dv,3><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],ext.d_ptr(),
                    max_idx_left);
        else if(dim == 4)
            k_resort_boundary<dx,dv,4><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],ext.d_ptr(),
                    max_idx_left);
        else if(dim == 5)
            k_resort_boundary<dx,dv,5><<<gd,bd>>>(d_bdr_to_in,d_sign,bdr_len,
                    d_bdr_to_out,e[dim],e[0]*e[d+0],e[d+dim],ext.d_ptr(),
                    max_idx_left);
        else {
            cout << "ERROR: rearrange is currently only implemented "
                 << "for dim=0,1,2,3,4,5." << endl;
        }
        cudaDeviceSynchronize();
        gpuErrchk( cudaPeekAtLastError() );
        trearrange.stop();
        cout << "trearrange: " << trearrange.total() << endl;

        timer t3; t3.start();
        gpuErrchk( cudaMemcpy(d_bdr_to_in, d_bdr_to_out, bdr_tot_len*sizeof(fp),
                    cudaMemcpyDeviceToDevice) );
                            gpuErrchk( cudaPeekAtLastError() );
        cudaDeviceSynchronize();
        t3.stop();
        cout << "cpy d2d: " << t3.total() << endl;
        //std::swap(d_bdr_to_in,d_bdr_to_out);
    }
};




template<size_t d, fp (*f0)(fp*)>
__global__
void init_gpu(fp* d_in, fp* a, fp* b,
              Index* e, fp* gauss_x, size_t N){
    size_t idx = threadIdx.x + blockIdx.x*blockDim.x;
    if(idx < N){
        fp x[d];
        int offset=0;
        size_t _idx = idx;
        for(size_t i=0;i<d;++i) {
            fp hx = (b[i]-a[i])/fp(e[i]);
            int o = _idx%e[i+d];
            _idx /= e[i+d];
            fp xx = gauss_x[o+offset];
            int n = _idx%e[i];
            x[i]  = a[i] + n*hx + xx*hx;
            _idx /= e[i];
            offset += e[d+i];
        }
        d_in[idx] = f0(x);
    }
}


template<size_t dx, size_t dv, int typ, typename T>
__global__
void source_gpu(fp* d_in, fp* d_out, fp* a, fp* b, Index* e, 
                fp* gauss_x, fp dt, fp time, T source, size_t N){

    constexpr size_t d = dx+dv;

    size_t idx = threadIdx.x + blockIdx.x*blockDim.x;
    if(idx < N){
        fp x[d];
        int offset=0;
        size_t _idx = idx;
        for(size_t i=0;i<d;++i) {
            fp hx = (b[i]-a[i])/fp(e[i]);
            int o = _idx%e[i+d];
            _idx /= e[i+d];
            fp xx = gauss_x[o+offset];
            int n = _idx%e[i];
            x[i]  = a[i] + n*hx + xx*hx;
            _idx /= e[i];
            offset += e[d+i];
        }
        if(typ==0)
            d_out[idx] = d_in[idx] + dt*source.constant_source(x,time);
        if(typ==1)
            d_out[idx] = d_in[idx] + dt*source.time_dependent_source(x,time);
    }
}


///<<<(o1,dof_v1,dof_v2*dof_v3),number_cells_x1>>>
template<size_t dv>
__global__
void BGK_gpu(fp* d_fnp1, fp* d_fn, fp* d_fnp12, fp dt, fp nu, 
             fp* d_rho, fp* d_u, fp* d_T, fp* a, fp* b, Index* e){

    constexpr size_t dx = 1;
    constexpr size_t d = dx+dv;

    int idx_x=threadIdx.x + blockDim.x*blockIdx.x;

    fp rho = d_rho[idx_x]; 
    fp u = d_u[idx_x];
    fp T = d_T[idx_x];

    int global_idx = idx_x + e[0]*e[d]*(blockIdx.y + gridDim.y*blockIdx.z);

    if(rho<1e-14 || T<1e-14){
        //do not apply a collision term in x-regions 
        //where the density is slightly negative
        d_fnp1[global_idx] = d_fn[global_idx];
        return;
    }

    int v_idx[2*dv];
    v_idx[0]  = blockIdx.y/e[d+dx];
    v_idx[dv] = blockIdx.y-v_idx[0]*e[d+dx];

    int v23 = blockIdx.z;
    for(size_t i=1;i<dv;++i){
        v_idx[dv+i] = v23%e[d+dx+i];
        v23 /= e[d+dx+i];
        v_idx[i] = v23%e[dx+i];
        v23 /= e[dx+i]; 
    }

    fp v[dv];
    for(size_t i=0;i<dv;++i)
        v[i] = a[dx+i] + (b[dx+i]-a[dx+i])/fp(e[dx+i])*(v_idx[i] + 
               gauss::get_x_scaled01(v_idx[dv+i],e[d+dx+i]));

    fp M = rho/pow(2.0*M_PI*T,0.5*dv)*exp(-(v[0]-u)*(v[0]-u)/(2.0*T));

    for(size_t i=1;i<dv;++i) 
        M *= exp(-v[i]*v[i]/(2.0*T));


    d_fnp1[global_idx] = d_fn[global_idx] + nu*dt*(M-d_fnp12[global_idx]);
}


