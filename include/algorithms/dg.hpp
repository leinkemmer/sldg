#pragma once

#include <generic/common.hpp>
#include <container/domain.hpp>
#include <algorithms/limiter.hpp>


// TODO: is this the same as neg_modulo?
#ifdef __CUDACC__
__host__ __device__
#endif
int get_bdr_idx(int j, int n) {
    // assume that j is not in {0,...,n-1}
    if(j>n-1)  return j-n;
    else       return -j-1;
}

/// This enum is used in the translate1d function to specify for which cells the
/// computation should be performed.
enum translate1d_mode {
    TM_ALL,       ///< The computation is done for all the data stored.
    TM_INTERIOR,  ///< Only the data are processed that do NOT require boundary
                  /// data from different MPI processes.
    TM_BOUNDARY   ///< Only the data are processes that DO require boundary data
                  /// from different MPI processes.
};


enum reuse_mode {
    RM_NO_REUSE,
    RM_REUSE
};


template<Index o>
struct dg_coeff {
    array<fp,o> scale;
    array<fp,o> xi;
    array<fp,o> w;
    array<fp,o> inv_w;

    dg_coeff() {
        for(Index j=0;j<o;j++) {
            fp prod=1.0;
            for(Index i=0;i<j;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            for(Index i=j+1;i<o;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            scale[j]=prod;
        }

        for(Index j=0;j<o;j++) {
            xi[j]=gauss::x_scaled01(j,o);
            w[j] =gauss::w(j,o);
            inv_w[j]=1.0/gauss::w(j,o);
        }
    }

    fp lagrange(fp x, int j) {
        fp prod=1.0;
        for(Index i=0;i<j;i++)   prod*=(x-xi[i]);
        for(Index i=j+1;i<o;i++) prod*=(x-xi[i]);
        return prod*scale[j];
    }

    // assumes that l is initialized to 1
    // computes all lagrange polynomials at a fixed point
    void lagrange_all(fp x, fp* l) {
        // normalize
        for(Index i=0;i<o;i++)
            l[i] = scale[i];
        fp p = 1.0;
        // forward step: compute the part before j (in the notation of
        // lagrange(x,j))
        for(Index i=0;i<o-1;i++) {
            p *= (x-xi[i]);
            l[i+1] *= p;
        }
        // backward step: compute the part after j
        p = 1.0;
        for(Index i=o-1;i>0;i--) {
            p *= (x-xi[i]);
            l[i-1] *= p;
        }
    }

    fp l[4][o];
    fp A[o][o];  fp B[o][o];

    void set(fp alpha) {
        fill(&A[0][0], &A[0][0]+o*o, 0.0);
        fill(&B[0][0], &B[0][0]+o*o, 0.0);

        for(Index r=0;r<o;r++) {
            fp x  = xi[r];
            fp xw = w[r];

            lagrange_all(alpha+x*(1-alpha),&l[0][0]);
            lagrange_all(x*(1-alpha),&l[1][0]);
            lagrange_all(alpha*x,&l[2][0]);
            lagrange_all(alpha*(x-1)+1,&l[3][0]);

            // slow
            //for(int i=0;i<o;i++) {
                //l[0][i] = lagrange(alpha+x*(1-alpha),i);
                //l[1][i] = lagrange(x*(1-alpha),i);
                //l[2][i] = lagrange(alpha*x,i);
                //l[3][i] = lagrange(alpha*(x-1)+1,i);
            //}

            for(Index j=0;j<o;j++) {
                #pragma omp simd
                for(Index jp=0;jp<o;jp++) {
                    A[j][jp] += xw*l[0][jp]*l[1][j];
                    B[j][jp] += xw*l[2][jp]*l[3][j];
                }
            }
        }

        for(Index j=0;j<o;j++) {
            for(Index jp=0;jp<o;jp++) {
                A[j][jp] *= (1-alpha)*inv_w[j];
                B[j][jp] *= alpha*inv_w[j];
            }
        }
    }

};


#ifdef __CUDACC__
__host__ __device__
#endif
void dist_to_alpha(fp dist, int& shift, fp& alpha) {
    if(dist > 0) {
        shift = int(dist)+1;
        alpha = 1.0-(dist-shift+1);
    } else {
        shift = int(dist);
        alpha = abs(dist-shift);
    }
}


fp lagrange_derivative(fp x, int j, int o) {
    fp sum=0.0;
    for(int m=0;m<o;m++) {
        if(m != j) {
            fp prod=1.0;
            for(int i=0;i<o;i++)
                if(i != j && i != m)
                    prod*=(x-gauss::x_scaled01(i,o))
                        /(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));

            sum += prod/(gauss::x_scaled01(j,o)-gauss::x_scaled01(m,o));
        }
    }
    return sum;
}

// ---------------------------------------------------------------
template<size_t dx>
struct dg_local_poisson_matrix {
    using domain_x = multi_array<fp,dx+dx>;

    virtual fp dl_g(int i, int k) const = 0;

    virtual fp compute(domain_x& in, array<Index,dx+dx>& idx, 
                       const array<Index,dx+dx>& e_xpart ) = 0;
    virtual fp compute(fp* __restrict__ in, array<Index,dx+dx>& idx, 
                       Index lin_idx, const array<Index,dx+dx>& e_xpart,
                       array< vector<fp>, dx>& bdry_left, 
                       array< vector<fp>, dx>& bdry_right) = 0;

    virtual ~dg_local_poisson_matrix() {};
};

template<size_t dx, Index o, size_t dim>
struct dg_local_matrix : public dg_local_poisson_matrix<dx> { 
    using domain_x = multi_array<fp,dx+dx>;

    fp M[o][o];
    fp L[o][o];
    fp R[o][o];

    fp dL_g[o][o];//dL_g[i][k] = dL_i(gx(k))

    vector<fp> GW;
    array<Index,dx+dx> e_xpart;    

    fp HH;
    Index e1;
    Index stride_N;
    Index stride_o;

    dg_local_matrix() = default;

    dg_local_matrix(fp sigma, array<fp,dx> a,
                    array<fp,dx> b, array<Index,dx+dx> e)
        : e_xpart(e), HH(1.0), e1(e[dx+(dim+1)%dx]), stride_N(1), stride_o(1)
    {

        for(size_t i = 0; i < dim; ++i)
            stride_o *= e[i]*e[dx+i];
        stride_N = stride_o*e[dx+dim];

        dg_coeff<o> dg_c;
        fp inv_h2 = fp(e[dim])/(b[dim]-a[dim]);
        inv_h2 *= inv_h2;
        size_t gw_size = 1.0;

        for(size_t i = 0; i < dx; ++i)
            if(i != dim) {
                HH *= (b[i]-a[i])/fp(e[i]);
                gw_size *= e[dx+i];
            }

        GW.resize(gw_size);
        fill(GW.begin(),GW.end(),1.0);
        size_t idx;            
        int I;
        for(size_t i = 0; i < gw_size; ++i){
            I = i%e1;
            for(size_t j = 0; j < dx-1; j++){
                idx = (dim+j+1)%dx;
                GW[i] *= gauss::w(I,e[dx+idx])*0.5;
                I = i/e1;
            }
        }

        fp dL_0[o];
        fp dL_1[o];
        fp  L_0[o];
        fp  L_1[o]; 
        dg_c.lagrange_all(1.0, &L_1[0]);
        dg_c.lagrange_all(0.0, &L_0[0]);
        for(int i = 0; i < o; ++i) {
            dL_0[i] = lagrange_derivative(0, i, o);
            dL_1[i] = lagrange_derivative(1, i, o);
            for (int k = 0; k < o; ++k) {
                dL_g[i][k] = lagrange_derivative(dg_c.xi[k], i, o);
            }
        }

        for(int i = 0; i < o; ++i)
            for(int j = 0; j < o; ++j) {
                M[i][j] = 0.0;
                for(int k = 0; k < o; ++k)
                    M[i][j] += 0.5*dL_g[i][k]*dL_g[j][k]*dg_c.w[k]*inv_h2;

                L[i][j]  = ( 0.5*dL_1[j]*L_0[i] - 0.5*L_1[j]*dL_0[i] -
                            sigma*L_1[j]*L_0[i] )*inv_h2;
                R[i][j]  = (-0.5*dL_0[j]*L_1[i] + 0.5*L_0[j]*dL_1[i] -
                            sigma*L_0[j]*L_1[i] )*inv_h2;
                M[i][j] += ( 0.5*dL_0[j]*L_0[i] + 0.5*L_0[j]*dL_0[i] + 
                            sigma*L_0[j]*L_0[i] 
                            -0.5*dL_1[j]*L_1[i] - 0.5*L_1[j]*dL_1[i] + 
                            sigma*L_1[j]*L_1[i] )*inv_h2;
            } 
    } 

    fp select_GW(const array<Index,dx+dx>& idx) {
        int I = ( dx>1 ? idx[dx+(dim+1)%dx] : 0) + ( dx>2 ? idx[dx+(dim+2)%dx]*e1 : 0 );
        return GW[I];
    }

    fp compute(domain_x& in, array<Index,dx+dx>& idx, const array<Index,dx+dx>& e_xpart) {
        Index j = idx[dx+dim];
        array<Index, dx+dx> idxm = idx;
        array<Index, dx+dx> idxr = idx;
        array<Index, dx+dx> idxl = idx;
        idxr[dim] = (idx[dim]+1>=e_xpart[dim]) ? 0 : idx[dim]+1;
        idxl[dim] = (idx[dim]==0) ? e_xpart[dim]-1 : idx[dim]-1;

        fp gw = select_GW(idx);

        fp result = 0.0;
        for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
            idxm[dx+dim] = i;
            idxr[dx+dim] = i;
            idxl[dx+dim] = i;

            result += ( M[ j ][ i ]*in(idxm) + 
                        R[ j ][ i ]*in(idxr) +
                        R[ i ][ j ]*in(idxl)   )*HH*gw;
        }
        return result;
    }
    

    /* 
    //faster than the above
    fp compute(fp __restrict__* in, array<Index,dx+dx>& idx, Index lin_idx, 
               const array<Index,dx+dx>& e_xpart ) {
        Index k = idx[dim]; 
        Index j = idx[dx+dim]; 

        Index lin_idxm = lin_idx - j*stride_o;

        Index lin_idxr = lin_idxm + ( (k+1>=e_xpart[dim]) ?    -k*stride_N :  stride_N );
        Index lin_idxl = lin_idxm + ( (k==0) ? (e_xpart[dim]-1-k)*stride_N : -stride_N );
        fp gw = select_GW(idx);

        double result = 0.0;
        for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
            result += ( M[ j ][ i ]*in[lin_idxm + i*stride_o] + 
                        R[ j ][ i ]*in[lin_idxr + i*stride_o] +
                        R[ i ][ j ]*in[lin_idxl + i*stride_o]   )*HH*gw;
        }   
        return result;
    }
    */


    fp compute(fp* __restrict__ in, array<Index,dx+dx>& idx, Index lin_idx, 
               const array<Index,dx+dx>& e_xpart,
               array< vector<fp>, dx>& bdry_right,
               array< vector<fp>, dx>& bdry_left ) {
        Index k = idx[dim]; 
        Index j = idx[dx+dim]; 

        Index lin_idxm = lin_idx - j*stride_o;
        double result = 0.0;
        fp gw = select_GW(idx);

        if (k+1>=e_xpart[dim]) {
            Index lin_idxl = lin_idxm - stride_N;
            Index bd_index = get_index_slice<dx>(dim,idx,e_xpart);
            for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
                result += ( M[ j ][ i ]*in[lin_idxm + i*stride_o] + 
                            R[ j ][ i ]*bdry_right[dim][i + bd_index] +
                            R[ i ][ j ]*in[lin_idxl + i*stride_o]   )*gw;
            }  
        } else if (k == 0) { 
            Index lin_idxr = lin_idxm + stride_N;
            Index bd_index = get_index_slice<dx>(dim,idx,e_xpart);
            for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
                result += ( M[ j ][ i ]*in[lin_idxm + i*stride_o] + 
                            R[ j ][ i ]*in[lin_idxr + i*stride_o] +
                            R[ i ][ j ]*bdry_left[dim][i + bd_index]   )*gw;
            }  
        } else {
            Index lin_idxr = lin_idxm + stride_N;
            Index lin_idxl = lin_idxm - stride_N;
            for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
                result += ( M[ j ][ i ]*in[lin_idxm + i*stride_o] + 
                            R[ j ][ i ]*in[lin_idxr + i*stride_o] +
                            R[ i ][ j ]*in[lin_idxl + i*stride_o]   )*gw;
            }
        }  
        return result;
    }


    fp dl_g(int i, int k) const {
        return dL_g[i][k];
    }
};




template<size_t dx>
unique_ptr< dg_local_poisson_matrix<dx> > make_dg_local_poisson(Index o, size_t dim, fp sigma, 
                           const array<fp,dx>& a, const array<fp,dx>& b, const array<Index,dx+dx>& e) {
    if(o==2) {
        if(dim==0)      return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,2,0>(sigma, a,b,e));
        else if(dim==1) return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,2,1>(sigma, a,b,e));
        else            return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,2,2>(sigma, a,b,e));
    } else if(o==3) {
        if(dim==0)      return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,3,0>(sigma, a,b,e));
        else if(dim==1) return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,3,1>(sigma, a,b,e));
        else            return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,3,2>(sigma, a,b,e));
    } else if(o==4) {
        if(dim==0)      return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,4,0>(sigma, a,b,e));
        else if(dim==1) return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,4,1>(sigma, a,b,e));
        else            return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,4,2>(sigma, a,b,e));
    } else if(o==5) {
        if(dim==0)      return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,5,0>(sigma, a,b,e));
        else if(dim==1) return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,5,1>(sigma, a,b,e));
        else            return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,5,2>(sigma, a,b,e));
    } else if(o==6) {
        if(dim==0)      return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,6,0>(sigma, a,b,e));
        else if(dim==1) return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,6,1>(sigma, a,b,e));
        else            return unique_ptr<dg_local_poisson_matrix<dx>>(new dg_local_matrix<dx,6,2>(sigma, a,b,e));
    } else {
        cout << "ERROR: make_dg_local_poisson does not support o=" << o << endl;
        exit(1);
    }
}


//-------------------------------------------------------------

/// Perform a 1d translation with the discontinuous Galerkin scheme.
/// - Negative values of dist correspond to a translation to the left and
///   positive values correspond to a translation to the right. For example,
///   dist=2.5 is mapped to shift=3 and alpha=0.5 as a positive alpha value
///   corresponds to a translation to the left.
/// - The function takes multi_array views as the in and output and can thus be
///   applied in an arbitrary dimension.
/// - The boundary data are specified in a linear storage format (see
///   domain::pack_boundary and domain::unpack_boundary). A NULL pointer may be
///   specified for translate1d_mode=TM_INTERIOR.
template<int d, int order>
void translate1d(typename domain<d>::view& u, typename domain<d>::view& out,
                 fp* boundary, int boundary_size, fp dist, dg_coeff<order>& c, 
                 translate1d_mode tm=TM_ALL, reuse_mode reuse=RM_NO_REUSE, 
                 int block_idx=0, int block_size=0) {
    int n=u.shape()[0]; int o=u.shape()[1];
    assert((int(abs(dist))+1)*o<=boundary_size);
    int shift; fp alpha;
    dist_to_alpha(dist, shift, alpha);

    // sometimes it is desirable to process the interior and the boundary parts
    // separately (for example to interleave computation and communication)
    int start=0; int end=n;
    if(block_size == 0) {
        if(tm == TM_INTERIOR) {
            if(dist>0)  start=shift;
            else        end=n-abs(shift)-1;

        } else if(tm == TM_BOUNDARY) {
            if(dist>0)  end=shift;
            else        start=n-abs(shift)-1;
        }
    } else {
        // Cache blocked
        start = block_size*block_idx;
        end   = min(block_size*(block_idx+1),n);
        if(tm == TM_INTERIOR) {
            if(dist>0) start = max(start,shift);
            else       end   = min(end, n-abs(shift)-1);
        }
    }

    /*
    // Reference implementation but not as fast
    // compute the coefficients
    fp A[o][o];
    fp B[o][o];
    for (int j = 0; j < o; j++) {
        for (int jp = 0; jp < o; jp++) {
            A[j][jp] = 0.0;
            B[j][jp] = 0.0;
            for (int r = 0; r < o; r++) {
                A[j][jp] += gauss::w(r, o) * lagrange(alpha + gauss::x_scaled01(r, o) * (1 - alpha), jp, o) *
                            lagrange(gauss::x_scaled01(r, o) * (1 - alpha), j, o);
                B[j][jp] += gauss::w(r, o) * lagrange(alpha * gauss::x_scaled01(r, o), jp, o) *
                            lagrange(alpha * (gauss::x_scaled01(r, o) - 1) + 1, j, o);
            }
            A[j][jp] *= (1 - alpha) / gauss::w(j, o);
            B[j][jp] *= alpha / gauss::w(j, o);
        }
    }
    */
    if(reuse == RM_NO_REUSE)
        c.set(alpha);

    // start the actual computation
    // this is also the reference implementation which works just as well
    // for the interior of the domain (but the code below is more optimized).
    if(tm != TM_INTERIOR) {
        for(int i=start;i<end;i++) {
            for(int j=0;j<o;j++) {
                fp s1=0.0; fp s2=0.0;
                for(int jp=0;jp<o;jp++) {
                    fp ustar; int istar=i-shift;

                    if(istar<n && istar>=0)
                        ustar=u(istar,jp);
                    else
                        ustar=boundary[o*get_bdr_idx(istar,n)+jp];
                    fp ustarp1; int istarp1=i-shift+1;
                    if(istarp1<n && istarp1>=0)
                        ustarp1=u(istarp1,jp);
                    else
                        ustarp1=boundary[o*get_bdr_idx(istarp1,n)+jp];
                    s1 += ustar*c.A[j][jp];
                    s2 += ustarp1*c.B[j][jp];
                }
                out(i,j) = s1 + s2;
            }
        }
    } else {
        fp ustar[order], ustarp1[order];
        for(int i=start;i<end;i++) {
            int istar=i-shift;
            int istarp1=i-shift+1;

            for(int jp=0;jp<order;jp++) {
                ustar[jp]   = u(istar,jp);
                ustarp1[jp] = u(istarp1,jp);
            }

            for(int j=0;j<order;j++) {
                fp s=0.0;
                for(int jp=0;jp<order;jp++) {
                    s += c.A[j][jp]*ustar[jp];
                    s += c.B[j][jp]*ustarp1[jp];
                }
                out(i,j) = s;
            }
        }
    }
}

// This is the significantly slower reference implementation.
template<int d>
void translate1d(typename domain<d>::view& u, typename domain<d>::view& out,
                 fp* boundary, int boundary_size, fp dist,
                 translate1d_mode tm=TM_ALL, int block_idx=0, 
                 int block_size=0) {

    int n=u.shape()[0]; int o=u.shape()[1];
    assert((int(abs(dist))+1)*o<=boundary_size);
    int shift; fp alpha;
    dist_to_alpha(dist, shift, alpha);
    // sometimes it is desirable to process the interior and the boundary parts
    // separately (for example to interleave computation and communication)
    int start=0; int end=n;
    if(block_size == 0) {
        if(tm == TM_INTERIOR) {
            if(dist>0)  start=shift;
            else        end=n-abs(shift)-1;

        } else if(tm == TM_BOUNDARY) {
            if(dist>0)  end=shift;
            else        start=n-abs(shift)-1;
        }
    } else {
        // Cache blocked
        start = block_size*block_idx;
        end = block_size*(block_idx+1);
        if(tm == TM_INTERIOR) {
            start = max(start,shift);
            end   = min(end, n-abs(shift)-1);
        }
    }

    fp A[o][o];
    fp B[o][o];
    for (int j = 0; j < o; j++) {
        for (int jp = 0; jp < o; jp++) {
            A[j][jp] = 0.0;
            B[j][jp] = 0.0;
            for (int r = 0; r < o; r++) {
                double xx = gauss::x_scaled01(r, o)*(1 - alpha);
                A[j][jp] += gauss::w(r, o) * lagrange(alpha+xx, jp, o) *
                            lagrange(xx, j, o);
                B[j][jp] += gauss::w(r, o) 
                    * lagrange(alpha*gauss::x_scaled01(r, o), jp, o) 
                    * lagrange(alpha * (gauss::x_scaled01(r, o) - 1) + 1, j, o);
            }
            A[j][jp] *= (1 - alpha) / gauss::w(j, o);
            B[j][jp] *= alpha / gauss::w(j, o);
        }
    }

    for(int i=start;i<end;i++) {
        for(int j=0;j<o;j++) {
            fp s1=0.0; fp s2=0.0;
            for(int jp=0;jp<o;jp++) {
                fp ustar; int istar=i-shift;

                if(istar<n && istar>=0)
                    ustar=u(istar,jp);
                else
                    ustar=boundary[o*get_bdr_idx(istar,n)+jp];
                fp ustarp1; int istarp1=i-shift+1;
                if(istarp1<n && istarp1>=0)
                    ustarp1=u(istarp1,jp);
                else
                    ustarp1=boundary[o*get_bdr_idx(istarp1,n)+jp];
                s1 += ustar*A[j][jp];
                s2 += ustarp1*B[j][jp];
            }
            out(i,j) = s1 + s2;
        }
    }

}


/// Retrieve the value of o equidistant points in the interval [0,1].
fp equi_x(int j, int o) {
    return (2*j+1)/fp(2+2*(o-1));
}

/// Evaluate the Lagrange polynomials up to degree o-1 for equidistant points in
/// the interval [0,1].
fp lagrange_equidistant(fp x, int j, int o) {
    fp prod=1.0;
    for(int i=0;i<j;i++)   prod*=(x-equi_x(i,o))/(equi_x(j,o)-equi_x(i,o));
    for(int i=j+1;i<o;i++) prod*=(x-equi_x(i,o))/(equi_x(j,o)-equi_x(i,o));
    return prod;
}

/// Compute a Lagrange interpolation at equidistant points for polynomials up to
/// degree o-1 and equidistant points.
template<typename iterator>
fp interpolate1d(fp x, int o, iterator val) {
    fp r=0.0;
    for(int j=0;j<o;j++)
        r+=val[j]*lagrange_equidistant(x,j,o);
    return r;
}


// Evaluate the first derivative
template<typename iterator>
fp evaluate_derivative_1d(fp x, int o, iterator val) {
    fp r=0.0;
    for(int j=0;j<o;j++)
        r+=val[j]*lagrange_derivative(x,j,o);
    return r;
    /*
       fp r=0.0;
       for(int j=0;j<o;j++) {
       fp c=0.0;
       for(int m=0;m<j;m++)   c+=1.0/(x-gauss::x_scaled01(m,o));
       for(int m=j+1;m<o;m++) c+=1.0/(x-gauss::x_scaled01(m,o));
       cout << "c: " << j << " " << c << endl;
       r+=val[j]*c*lagrange(x,j,o);
       }
       return r;
       */
}

struct dg_evaluator {
    vector<fp> scale, xi;
    int o;

    dg_evaluator(fp _o) : o(_o) {
        scale.resize(o);
        for(int j=0;j<o;j++) {
            fp prod=1.0;
            for(int i=0;i<j;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            for(int i=j+1;i<o;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            scale[j]=prod;
        }

        xi.resize(o);
        for(int j=0;j<o;j++)
            xi[j]=gauss::x_scaled01(j,o);
    }

    fp lagrange(fp x, int j) {
        fp prod=1.0;
        for(int i=0;i<j;i++)   prod*=(x-xi[i]);
        for(int i=j+1;i<o;i++) prod*=(x-xi[i]);
        return prod*scale[j];
    }

    fp eval(fp x, fp* u) {
        fp r=0.0;
        for(int j=0;j<o;j++)
            r+=u[j]*lagrange(x,j);

        return r;
    }
};


//
// This is the new and faster implementation of translate. It works on the GPU
// and the CPU.
//


#ifdef __CUDACC__
__device__ __host__
#endif
int fast_mod(int i, int n) {
    if(i >= n) i -= n;
    else if(i < 0) i += n;
    return i;
}


template<size_t dim, size_t dx, size_t dv>
#ifdef __CUDACC__
__host__ __device__
#endif
int get_idx_into_AB(int* i, int* offset) {
    int idx_AB=0;
    if(dim < dx) {
        int stride = 1;
        for(size_t k=0;k<dv;k++) {
            idx_AB += i[dx+k]*stride;
            stride *= offset[dx+k];
        }
    } else {
        int stride = 1;
        for(size_t k=0;k<dx;k++) {
            idx_AB += i[k]*stride;
            stride *= offset[k];
        }
    }
    return idx_AB;
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp* unpack_boundary_gpu(int istar, int nv, int offset, int bdr_len, int o,
        fp* boundary_left, fp* boundary_right) {
    fp* local_bdry;
    if(istar < 0)
        local_bdry = boundary_left + bdr_len*o*offset;
    else
        local_bdry = boundary_right + bdr_len*o*offset;

    return local_bdry + o*get_bdr_idx(istar,nv);
}


template<size_t dx, size_t dv, size_t dim>
#ifdef __CUDACC__
__host__ __device__
#endif
size_t get_bdry_info(int i[dx+dv], int offset[dx+dv], int& offset_s, 
                   int& bdry_offset_dim, int& bdry_stride_dim){

    constexpr size_t d = dx+dv;

    int off_xbdry=0, stride_xbdry=1;
    int off_vbdry=0, stride_vbdry=1;
    size_t off_x=0, stride_x=1;
    size_t off_v=0, stride_v=1;
    for(size_t k=0;k<dx;++k){
        if(k!=dim){
            off_x     += i[k]*stride_x;
            off_xbdry += i[k]*stride_xbdry;
            stride_xbdry *= offset[k];
        }
        stride_x *= offset[k];
    }
    for(size_t k=dx;k<d;++k){
        if(k!=dim){
            off_v     += i[k]*stride_v;
            off_vbdry += i[k]*stride_vbdry;
            stride_vbdry *= offset[k];
        }
        stride_v *= offset[k];
    }
    offset_s = ((dim<dx) ? off_vbdry : off_xbdry );
    bdry_offset_dim = ((dim<dx) ? off_xbdry : off_vbdry );
    bdry_stride_dim = ((dim<dx) ? stride_xbdry : stride_vbdry );

    return off_x + stride_x*off_v;
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp* unpack_sorted_bdry(int sign, int bdry_offset_dim, int bdry_stride_dim,
                       int bdr_len, int istar, int o, int nv, int max_bdry_left,
                       fp* boundary_data) {

    fp* local_bdry = boundary_data;
    if(istar < 0) 
        //right boundary form left node/block
        local_bdry += bdr_len*o*(bdry_offset_dim + bdry_stride_dim*(sign-1 + max_bdry_left));
    else 
        //left boundary from rhight node/block
        local_bdry += bdr_len*o*(bdry_offset_dim + bdry_stride_dim*(abs(sign)-1));
    
    return local_bdry + o*get_bdr_idx(istar,nv);
}

template<size_t d>
#ifdef __CUDACC__
__host__ __device__
#endif
int get_bdry_offset(size_t dim, int i[d], int offset[d]) {
    int  ret=0;
    int  stride=1;

    for(size_t k=0;k<d;k++) {
        if(k != dim) {
            ret    += i[k]*stride;
            stride *= offset[k];
        }
    }
    return ret;
}

struct vec3 {
    int x, y, z;

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    vec3(fp _x, fp _y, fp _z) : x(_x), y(_y), z(_z) {}

    // This is an odd implementation of the presentstructure. It would be more
    // natural to safe x, y, z into an array of size 3 but that does interfer
    // with the optimization in the CUDA kernel (i.e. performance is much
    // worse).
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    int operator[](size_t m) const {
        if(m==0)      return x;
        else if(m==1) return y;
        else          return z;
    }
};

template<size_t d>
#ifdef __CUDACC__
__host__ __device__
#endif
size_t get_global_offset(size_t dim, int* i, int* offset) {
    size_t global_offset = 0;
    size_t stride = 1;
    for(size_t k=0;k<d;k++) {
        global_offset += i[k]*stride*(dim!=k);
        stride *= offset[k];
    }
    return global_offset;
}


//TODO: implementing driftkinetic in this way results in a ugly code..
//      or change driftkinitic memory management or introduce the 
//      get_idx_AB from outside
//
template<size_t dx, size_t dv, size_t dim, Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void translate(fp* __restrict__ in, fp* __restrict__ out, fp* __restrict__ A,
        fp* __restrict__ B, int* __restrict__ _shift, int offx1,
        const vec3& threadIdx, const vec3& blockIdx, const vec3& blockDim,
        const vec3& gridDim, fp* __restrict__ s_A, fp* __restrict__ s_B,
        Index* ext,  int bdr_len=0, fp* boundary_data=nullptr, 
        int* d_sign=nullptr, int max_bdry_left=0, bool sldg_limiter=false,
        int driftkinetic=0) {

    constexpr size_t d = dx+dv;

    // Determine the index the current thread works on and the corresponding
    // offset into the input and output arrays

    //Original implementation works better (less divisions), however, 
    //there is a problem in 6D since there we have to many blocks 
    //in the Z-drirections, which is limited by 2^16
 
    int i[d], offset[d];
    int n_dim = blockDim.y;
    if (d<6) {
        if(dim==0) {
            i[0]      = threadIdx.y; // this is a cell index
        } else {
            int tile_x = blockDim.x;
            i[0]       = threadIdx.x + tile_x*blockIdx.x;
        }
        offset[0] = offx1;

        if(dim!=0 && i[0] >= offx1)
            return;

        size_t m = (dim==0) ? 0 : 1;
        int counter = blockIdx.z;
        for(size_t k=1;k<d;k++) {
            if(k==dim) {
                i[k]      = threadIdx.y; // this is a cell index
                offset[k] = ext[d+k]*n_dim;
            } else if(m<=1) {
                i[k]      = blockIdx[m];
                offset[k] = gridDim[m];
                m++;
            } else {
                offset[k] = ext[k]*ext[d+k];
                if(k==d-1 || (dim==d-1 && k==d-2)) {
                    i[k] = counter;
                } else {
                    int dof_next = (k+1==dim) ? 
                        ext[k+2]*ext[d+k+2] : ext[k+1]*ext[d+k+1];
                    i[k] = counter/dof_next;
                    counter -= i[k]*dof_next;
                }
            }
        }
    } else {

        for(size_t k=0;k<d;++k)
            offset[k] = ext[k]*ext[k+d];

        size_t combined_idx = blockIdx.x;
        if(dim==0)
            i[0] = threadIdx.y; //this is a cell index
        else {
            int tile_x = blockDim.x;
            int bloc_x = offset[0]/tile_x + (offset[0]%tile_x!=0);
            int rem = combined_idx%bloc_x;
            combined_idx /= bloc_x;
            i[0] = threadIdx.x + tile_x*rem;
            if(i[0]>=offset[0])
                return;
        }

        for(size_t k=1;k<d;++k){
            if(k==dim){
                i[k] = threadIdx.y; //this is a cell index
            } else {
                if(dim==0 && k==1){
                    int b_x = offset[1]/blockDim.z + (offset[1]%blockDim.z!=0);
                    int rem = combined_idx%b_x;
                    i[1] = threadIdx.z + blockDim.z*rem; 
                    //if(i[1]>=offset[1])
                    //    return;
                    combined_idx /= b_x;
                }else{
                    i[k] = combined_idx%offset[k];
                    combined_idx /= offset[k];
                }
            }
        }
        if(i[5]>=offset[5] || i[1]>=offset[1])
            return;

        //for(size_t k = 0; k < d; k++)
        //    offset[k] = ext[k]*ext[k+d];

        //int combined_index = blockIdx.x;
        //int divisor = ((d==2) ? 1 : gridDim.x/( 
        //            (dim==d-1) ? offset[d-2] : offset[d-1] ));
        //int m = 0;
        //for(int k = d-1; k >= 0; k--) {
        //    if(k == dim) {
        //        i[k] = threadIdx.y;
        //        m++;
        //    } else {
        //        if (k==1-m) {
        //            i[k] = threadIdx.x + combined_index*blockDim.x;
        //            if (i[k] >= offset[k])
        //                return;
        //        } else {
        //            i[k] = combined_index/divisor;
        //            combined_index -= i[k]*divisor;
        //            divisor /= (offset[k-1-(k-1==dim)]);
        //        }
        //    }
        //}
    }

    // in and out are only accessed locally
    // from here on in,out point at the beginning of a slice in dim
    size_t global_offset = get_global_offset<d>(dim, i, offset);
    in  += global_offset;
    out += global_offset;

    // load the appropriate A, B, shift
    int idx_AB;
    if(driftkinetic==0)
       idx_AB = get_idx_into_AB<dim,dx,dv>(i, offset);
    if(driftkinetic==2)
        idx_AB = i[0] + offset[0]*(i[1]+offset[1]*i[3]);
    if(driftkinetic==3)
        idx_AB = i[2];

    #ifdef __CUDA_ARCH__
    // make sure that everything is initialized correctly even if 
    // blockDim.y < o*o
    if(driftkinetic==0){
        int idx = threadIdx.y;
        if(dim>=dx || threadIdx.x==0) {
            for(int k=0;k<8;k++) {
                if(idx < o*o) {
                    s_A[idx] = A[idx + o*o*idx_AB];
                    s_B[idx] = B[idx + o*o*idx_AB];
                }
                idx += blockDim.y;
            }
        }
    }
    else if(driftkinetic==2){
        int idx = threadIdx.y;
        for(int k=0;k<8;k++) {
            if(idx < o*o) {
                s_A[idx] = A[idx + o*o*idx_AB];
                s_B[idx] = B[idx + o*o*idx_AB];
            }
            idx += blockDim.y;
        }
    }
    else if(driftkinetic==3){
        int idx = threadIdx.y;
        if(threadIdx.x==0) {
            for(int k=0;k<8;k++) {
                if(idx < o*o) {
                    s_A[idx] = A[idx + o*o*idx_AB];
                    s_B[idx] = B[idx + o*o*idx_AB];
                }
                idx += blockDim.y;
            }
        }
    }
    int shift = _shift[idx_AB];
    //else{
    //    s_A = A + o*o*idx_AB;
    //    s_B = B + o*o*idx_AB;
    //}
    #else
    s_A = A + o*o*idx_AB;
    s_B = B + o*o*idx_AB;
    int shift = _shift[idx_AB];
    #endif

    // load the necessary data
    int stride = 1;
    for(size_t k=1;k<=dim;k++)
        stride *= offset[k-1];

    fp in_istar[o], in_istarp1[o];
    int istar, istarp1;
    if(bdr_len == 0) {
        //periodic
        istar = fast_mod(i[dim]-shift,n_dim);
        istarp1 = fast_mod(istar+1,n_dim);

        for(int k=0;k<o;k++) {
            in_istar[k]   = in[(istar*o+k)*stride];
            in_istarp1[k] = in[(istarp1*o+k)*stride];
        }
    } else if(bdr_len == -1){
        //zero inflow
        istar = i[dim]-shift;
        if(istar>=0 && istar<n_dim){
            for(int k=0;k<o;k++)
                in_istar[k] = in[(istar*o+k)*stride];
        } else {
            for(int k=0;k<o;k++)
                in_istar[k] = 0.0;
        }

        istarp1 = istar+1;
        if(istarp1>=0 && istarp1<n_dim){
            for(int k=0;k<o;k++)
                in_istarp1[k] = in[(istarp1*o+k)*stride];
        } else {
            for(int k=0;k<o;k++)
                in_istarp1[k] = 0.0;
        }
    } else {

        istar = i[dim]-shift;
        if(istar >= 0 && istar < n_dim) {
            for(int k=0;k<o;k++)
                in_istar[k] = in[(istar*o+k)*stride];
        } else {
            int offset_s, bdry_offset_dim, bdry_stride_dim;
            get_bdry_info<dx,dv,dim>(i, offset, offset_s, 
                                     bdry_offset_dim, bdry_stride_dim);
            int sign = d_sign[offset_s];

            fp* local_bdry = unpack_sorted_bdry(sign, bdry_offset_dim, 
                    bdry_stride_dim, bdr_len, istar, o, n_dim, 
                    max_bdry_left, boundary_data);

            for(int k=0;k<o;k++) 
                in_istar[k] = local_bdry[k];
        }
            
        istarp1 = i[dim]-shift+1;
        if(istarp1 >= 0 && istarp1 < n_dim) {
            for(int k=0;k<o;k++)
                in_istarp1[k] = in[(istarp1*o+k)*stride];
        } else {
            int offset_s, bdry_offset_dim, bdry_stride_dim;
            get_bdry_info<dx,dv,dim>(i, offset, offset_s, 
                                     bdry_offset_dim, bdry_stride_dim);
            int sign = d_sign[offset_s];

            fp* local_bdry = unpack_sorted_bdry(sign, bdry_offset_dim, 
                    bdry_stride_dim, bdr_len, istarp1, o, n_dim, 
                    max_bdry_left, boundary_data);

            for(int k=0;k<o;k++)
                in_istarp1[k] = local_bdry[k];
        }
    }

    #ifdef __CUDA_ARCH__
    __syncthreads();
    #else
    // not required as on the cpu data are loaded in order
    #endif


    // do the computation f^{n+1}_i = A*f^n_is + B*f^n_{is+1}
    fp s_dbl[o];
    for(int j=0;j<o;j++) {
        s_dbl[j]=0.0;
        for(int l=0;l<o;l++) {
            s_dbl[j] += s_A[j*o+l]*(in_istar[l]) + 
                        s_B[j*o+l]*(in_istarp1[l]);
        }
    }


    if(sldg_limiter && o==4){

        fp alpha = 0.0;
        fp mean = 0.0;
        fp mean1 = 0.0;
        fp mean2 = 0.0;
        for(int j=0;j<o;j++) {
            for(int l=0;l<o;l++) {
                alpha += s_B[j*o+l]*gauss::get_w(j,o);
            }
            mean += s_dbl[j]*gauss::get_w(j,o)*0.5;
            mean1 += in_istar[j]*gauss::get_w(j,o)*0.5;
            mean2 += in_istarp1[j]*gauss::get_w(j,o)*0.5;
        }
        alpha *= 0.5;

        fp aux[o];
        for(int i=0;i<o;++i){
            aux[i] = 1.0;
            for(int j=0;j<i;++j)
                aux[i] *= (gauss::get_x_scaled01(i,o)-gauss::get_x_scaled01(j,o));
            for(int j=i+1;j<o;++j)
                aux[i] *= (gauss::get_x_scaled01(i,o)-gauss::get_x_scaled01(j,o));
            aux[i] = 1.0/aux[i];
        }

        //TODO: will this be a problem for performance??
        fp Idelta = mean_indicator_sldg<o>(alpha,in_istar,in_istarp1,s_dbl,aux);

        fp a1, a2;
        fp tmp;
        fp maxin = -2.0;
        fp minin =  2.0;
        maxmin_o4(in_istar,alpha,1.0,aux,maxin,minin,a1,tmp);
        maxmin_o4(in_istarp1,0.0,alpha,aux,maxin,minin,tmp,a2);

        //fp beta1 = compute_beta<o>(in_istar);
        //fp beta2 = compute_beta<o>(in_istarp1);
        //fp betao = compute_beta<o>(s_dbl);

        //constexpr fp factor = 1.0;
        //if(factor*betao>beta1+beta2){
        
        //if(Idelta>0.5){

        //    fp omega = 0.0;//(beta1+beta2)/(factor*betao);
        //    fp s1 = mean-a1;
        //    fp s2 = a2-mean;
        //    fp slope = (sign(s1)+sign(s2))*min(abs(s1),abs(s2));
        //    for(int k=0;k<o;++k)
        //        s_dbl[k] = omega*s_dbl[k] + (1.0-omega)*
        //                   (mean + slope*(gauss::get_x_scaled01(k,o)-0.5));
        //}

        //bool b1 = true;
        //if(istar<0 || istarp1>=n_dim)
        //    b1 = false;

        //criteria seems to work well
        if(Idelta>0.5){

            fp maxout = -2.0;
            fp minout =  2.0;
            maxmin_o4(s_dbl,0.0,1.0,aux,maxout,minout,tmp,tmp);

            //abs should not be required since 
            //-maxin and maxout should be greater than mean
            //-minin and minout should be smaller than mean
            fp theta = min(min(abs((maxin - mean)/(maxout - mean)),
                               abs((mean - minin)/(mean - minout))),fp(1.0));

            for(int k=0;k<o;++k)
                s_dbl[k] = theta*(s_dbl[k]-mean)+mean; 
         
            //for(int k=0;k<o;++k)
            //    s_dbl[k] = mean; 

            ////not working!!
            //fp s = sign(a2-a1);
            //fp slope = min(s*(mean-a1),s*(a2-mean));
            //for(int k=0;k<o;++k)
            //    s_dbl[k] = 2.0*s*slope*(gauss::get_x_scaled01(k,o)-0.5) + mean;


            ////partially working
            //fp beta1 = compute_beta<o>(in_istar);
            //fp beta0 = compute_beta<o>(s_dbl);
            //fp beta2 = compute_beta<o>(in_istarp1);

            //fp omega1 = 0.001/pow2(1e-6 + beta1);
            //fp omega0 = 0.998/pow2(1e-6 + beta0);
            //fp omega2 = 0.001/pow2(1e-6 + beta2);

            //fp isumm = 1.0/(omega0+omega1+omega2);
            //omega1 *= isumm;
            //omega0 *= isumm;
            //omega2 *= isumm;

            //for(Index k=0;k<o;++k)
            //    s_dbl[k] = omega1*(in_istar[k]-mean1+mean) + 
            //               omega0*(s_dbl[k]) + 
            //               omega2*(in_istarp1[k]-mean2+mean);
             
            ////partially working. Results somehow in zigzag flux
            //for(int k=0;k<o;++k){
            //    in_istar[k] = (mean-mean1)/alpha*
            //                  (gauss::get_x_scaled01(k,o) - 0.5) + mean;
            //    in_istarp1[k] = (mean2-mean)/(1.0-alpha)*
            //                    (gauss::get_x_scaled01(k,o) - 0.5) + mean;
            //    s_dbl[k] = (s_dbl[k]-0.001*in_istar[k]-0.001*in_istarp1[k])/0.998;
            //} 

            //fp beta1 = compute_beta<o>(in_istar);
            //fp beta2 = compute_beta<o>(in_istarp1);
            //fp beta0 = compute_beta<o>(s_dbl);

            //fp omega1 = 0.001/pow2(1e-6 + beta1);
            //fp omega0 = 0.998/pow2(1e-6 + beta0);
            //fp omega2 = 0.001/pow2(1e-6 + beta2);

            //fp isumm = 1.0/(omega0+omega1+omega2);
            //omega0 *= isumm;
            //omega1 *= isumm;
            //omega2 *= isumm;

            //for(Index k=0;k<o;++k)
            //    s_dbl[k] = omega1*in_istar[k] + 
            //               omega0*s_dbl[k] + 
            //               omega2*in_istarp1[k];

        }
        //if(!b1){
            //if(istar<0){
            //    for(int k=0;k<o;++k)
            //        s_dbl[k] = a2*gauss::get_x_scaled01(k,o);
            //} else if(istarp1>=n_dim) {
            //    for(int k=0;k<o;++k)
            //        s_dbl[k] = -a1*(gauss::get_x_scaled01(k,o)-1.0);
            //}

        //    if(istar<0 || istarp1>=n_dim){

        //        fp s1 = mean-a1;
        //        fp s2 = a2-mean;
        //        fp slope = (sign(s1)+sign(s2))*min(abs(s1),abs(s2));
        //        for(int k=0;k<o;++k)
        //            s_dbl[k] = (mean + slope*(gauss::get_x_scaled01(k,o)-0.5));
        //    }
        //}
    }

    //positivity limiter
    //{
    //    fp mean = 0.0;
    //    fp min_out = 10;
    //    for(int k=0;k<o;++k){
    //        min_out = min(min_out,s_dbl[k]);
    //        mean += s_dbl[k]*gauss::get_w(k,o)*0.5;
    //    }
    //    fp theta = min(abs(mean/(min_out-mean)),1.0);
    //    for(int k=0;k<o;++k)
    //        s_dbl[k] = theta*(s_dbl[k]-mean)+mean; 
    //}

    //write back to memory
    for(int k=0;k<o;k++)
        out[(i[dim]*o+k)*stride] = s_dbl[k]; 
}


template<size_t d_adv>
struct dg_coeff_generic {
    virtual fp* p_A()=0;
    virtual fp* p_B()=0;
    virtual int* p_shift()=0;

    virtual fp* pd_A()=0;
    virtual fp* pd_B()=0;
    virtual int* pd_shift()=0;

    virtual void compute(fp tau, fp h_cell, multi_array<fp,2*d_adv>& F)=0;
    virtual void copy_to_gpu()=0;

    virtual ~dg_coeff_generic() {}
};


#ifndef USE_BOOST_MULTIARRAY
template<size_t d_adv, Index o>
struct dg_coeff_gpu : dg_coeff_generic<d_adv> {
    vector<fp> A, B;
    vector<int> shift;
    fp *d_A, *d_B;
    int *d_shift;
    fp additional_scalar_speed;

    Index N;

    fp* p_A() {
        return &A[0];
    }

    fp* p_B() {
        return &B[0];
    }

    int* p_shift() {
        return &shift[0];
    }
    
    fp* pd_A() {
        return d_A;
    }

    fp* pd_B() {
        return d_B;
    }

    int* pd_shift() {
        return d_shift;
    }


    dg_coeff_gpu(Index _N, fp additional_speed) 
        : N(_N), additional_scalar_speed(additional_speed) {
        A.resize(o*o*N);
        B.resize(o*o*N);
        shift.resize(N);

        #ifdef __CUDACC__
        d_A = (fp*)gpu_malloc(sizeof(fp)*o*o*N);
        d_B = (fp*)gpu_malloc(sizeof(fp)*o*o*N);
        d_shift = (int*)gpu_malloc(sizeof(int)*N);
        #endif
    }

    ~dg_coeff_gpu() {
        #ifdef __CUDACC__
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_shift);
        #endif
    }

    void compute(fp tau, fp h_cell, multi_array<fp,2*d_adv>& F) {

        array<Index,2*d_adv> e = F.shape();
        #pragma omp parallel for schedule(static)
        for(Index k=0;k<e[d_adv-1];++k){
            array<Index,2*d_adv> e_max = e;
            e_max[d_adv-1] = 1;
            array<Index,2*d_adv> _e = to_memorder<d_adv>(e_max);

            dg_coeff<o> dg_c_loc;

            for(auto _i : range<2*d_adv>(_e)) {
                array<Index,2*d_adv> i = from_memorder<d_adv>(_i);
                i[d_adv-1] = k;

                fp dist = tau*additional_scalar_speed*F( i )/h_cell;

                int _shift; fp alpha;
                dist_to_alpha(dist, _shift, alpha);

                dg_c_loc.set(alpha);

                Index offset 
                    = linearize_idx(to_memorder<d_adv>(i),to_memorder<d_adv>(e));

                memcpy(&A[o*o*offset], dg_c_loc.A, sizeof(fp)*o*o);
                memcpy(&B[o*o*offset], dg_c_loc.B, sizeof(fp)*o*o);
                shift[offset] = _shift;

            }
        }
    }

    void copy_to_gpu() {
        #ifdef __CUDACC__
        cudaMemcpy(d_A, &A[0], sizeof(fp)*o*o*N, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, &B[0], sizeof(fp)*o*o*N, cudaMemcpyHostToDevice);
        cudaMemcpy(d_shift, &shift[0], sizeof(int)*N, cudaMemcpyHostToDevice);
        #endif
    }

};


template<size_t d_adv>
unique_ptr<dg_coeff_generic<d_adv> > make_dg_coeff_gpu(Index o, Index N,
                                                 fp additional_scalar_speed=1.0) {
    if(o==1)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 1>(N,additional_scalar_speed));
    else if(o==2)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 2>(N,additional_scalar_speed));
    else if(o==3)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 3>(N,additional_scalar_speed));
    else if(o==4)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 4>(N,additional_scalar_speed));
    else if(o==5)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 5>(N,additional_scalar_speed));
    else if(o==6)
        return unique_ptr<dg_coeff_generic<d_adv>>(new dg_coeff_gpu<d_adv, 6>(N,additional_scalar_speed));
    else {
        cout << "ERROR: make_dg_coeff_gpu does not support o=" << o << endl;
        exit(1);
    }
}


#ifdef __CUDACC__
template<size_t o>
__device__
void lagrange_all(fp x, fp* l, fp* scale, fp* xi) {
    // normalize
    for(Index i=0;i<o;i++)
        l[i] = scale[i];
    fp p = 1.0;
    // forward step: compute the part before j (in the notation of
    // lagrange(x,j))
    for(Index i=0;i<o-1;i++) {
        p *= (x-xi[i]);
        l[i+1] *= p;
    }
    // backward step: compute the part after j
    p = 1.0;
    for(Index i=o-1;i>0;i--) {
        p *= (x-xi[i]);
        l[i-1] *= p;
    }
}


template<size_t d_adv, size_t o>
__global__
void compute_AB_k(fp tau, fp h_cell, fp* F, Index N, fp* xi, fp* w, fp* scale,
                  fp* inv_w, fp* d_A, fp* d_B, int* d_shift){

    int idx = threadIdx.x + blockIdx.x*blockDim.x;

    //printf("idx: %i \n", idx);

    if(idx>=N)
        return;

    fp dist = tau*F[idx]/h_cell;

    int _shift; fp alpha;
    dist_to_alpha(dist, _shift, alpha);

    d_A += o*o*idx;
    d_B += o*o*idx;

    d_shift[idx] = _shift;

    for(int ii=0;ii<o*o;++ii){
        d_A[ii] = 0.0;
        d_B[ii] = 0.0;
    }

    fp l[4][o];
    for(Index r=0;r<o;r++) {
        fp x  = xi[r];
        fp xw = w[r];

        lagrange_all<o>(alpha+x*(1-alpha),&l[0][0],scale,xi);
        lagrange_all<o>(x*(1-alpha),&l[1][0],scale,xi);
        lagrange_all<o>(alpha*x,&l[2][0],scale,xi);
        lagrange_all<o>(alpha*(x-1)+1,&l[3][0],scale,xi);

        for(Index j=0;j<o;j++) {
            for(Index jp=0;jp<o;jp++) {
                d_A[o*j+jp] += xw*l[0][jp]*l[1][j];
                d_B[o*j+jp] += xw*l[2][jp]*l[3][j];
            }
        }
    }

    for(Index j=0;j<o;j++) {
        for(Index jp=0;jp<o;jp++) {
            d_A[j*o+jp] *= (1-alpha)*inv_w[j];
            d_B[j*o+jp] *= alpha*inv_w[j];
        }
    }

    //if(idx<32){

    //    for(Index j=0;j<o;j++) {
    //        for(Index jp=0;jp<o;jp++) {
    //            printf("idx: %i, jp+j*o: %i, A: %f, B: %f\n", 
    //                    idx, int(jp+o*j), d_A[j*o+jp], d_B[j*o+jp]);
    //        }
    //    }
    //}
}


template<size_t d_adv, Index o>
struct dg_coeff_on_gpu : dg_coeff_generic<d_adv>{
    
    Index N;
    fp* d_A;
    fp* d_B;
    int* d_shift;

    fp* d_xi;
    fp* d_w;
    fp* d_scale;
    fp* d_inv_w;

    fp additional_scalar_speed;

    dg_coeff_on_gpu(Index _N, fp scalar_speed) 
        : N(_N), additional_scalar_speed(scalar_speed) {
        d_A = (fp*)gpu_malloc(sizeof(fp)*o*o*N);
        d_B = (fp*)gpu_malloc(sizeof(fp)*o*o*N);
        d_shift = (int*)gpu_malloc(sizeof(int)*N);

        array<fp,o> scale;
        array<fp,o> xi;
        array<fp,o> w;
        array<fp,o> inv_w;

        for(Index j=0;j<o;j++) {
            fp prod=1.0;
            for(Index i=0;i<j;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            for(Index i=j+1;i<o;i++)
                prod*=1.0/(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
            scale[j]=prod;
        }

        for(Index j=0;j<o;j++) {
            xi[j]=gauss::x_scaled01(j,o);
            w[j] =gauss::w(j,o);
            inv_w[j]=1.0/gauss::w(j,o);
        }

        cout << xi << endl << w << endl << scale << endl << inv_w << endl;

        d_xi = (fp*)gpu_malloc(sizeof(fp)*o);
        d_w  = (fp*)gpu_malloc(sizeof(fp)*o);
        d_scale = (fp*)gpu_malloc(sizeof(fp)*o);
        d_inv_w = (fp*)gpu_malloc(sizeof(fp)*o);

        gpuErrchk(cudaMemcpy(d_xi, xi.data(), sizeof(fp)*o,
                             cudaMemcpyHostToDevice));
        gpuErrchk(cudaMemcpy(d_w, w.data(), sizeof(fp)*o,
                             cudaMemcpyHostToDevice));
        gpuErrchk(cudaMemcpy(d_scale, scale.data(), sizeof(fp)*o,
                             cudaMemcpyHostToDevice));
        gpuErrchk(cudaMemcpy(d_inv_w, inv_w.data(), sizeof(fp)*o,
                             cudaMemcpyHostToDevice));

        gpuErrchk(cudaDeviceSynchronize());
    }


    ~dg_coeff_on_gpu() {
        cudaFree(d_A);
        cudaFree(d_B);
        cudaFree(d_shift);
        cudaFree(d_xi);
        cudaFree(d_w);
        cudaFree(d_scale);
        cudaFree(d_inv_w);
    }


    void copy_to_gpu(){
        cout << "dg_coeff_on_gpu already on gpu" << endl;
    }

    fp* p_A(){
        cout << "should not be called" << endl;
        return nullptr;
    }
    fp* p_B(){
        cout << "should not be called" << endl;
        return nullptr;
    }
    int* p_shift(){
        cout << "should not be called" << endl;
        return nullptr;
    }

    fp* pd_A() {
        return d_A;
    }

    fp* pd_B() {
        return d_B;
    }

    int* pd_shift() {
        return d_shift;
    }

    void compute(fp tau, fp h_cell, multi_array<fp,2*d_adv>& F){

        int threads = 128;
        int blocks = (N+threads-1)/threads;

        compute_AB_k<d_adv,o><<<blocks,threads>>>(tau*additional_scalar_speed, 
                                       h_cell, F.data(), N, d_xi, d_w, d_scale, 
                                       d_inv_w, d_A, d_B, d_shift);
        cudaDeviceSynchronize();
    }

};


template<size_t d_adv>
unique_ptr<dg_coeff_generic<d_adv> > make_dg_coeff_on_gpu(Index o, Index N,
                                                          fp scalar_speed=1.0) {

    if(o==1)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,1>(N,scalar_speed));
    else if(o==2)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,2>(N,scalar_speed));
    else if(o==3)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,3>(N,scalar_speed));
    else if(o==4)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,4>(N,scalar_speed));
    else if(o==5)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,5>(N,scalar_speed));
    else if(o==6)
        return unique_ptr<dg_coeff_generic<d_adv>>(
                               new dg_coeff_on_gpu<d_adv,6>(N,scalar_speed));
    else {
        cout << "ERROR: make_dg_coeff_on_gpu does not support o=" << o << endl;
        exit(1);
    }
}
#endif


#endif

template<size_t d>
array<Index,3> create_grid(Index dim, array<Index,2*d> e, int tile_x) {
    array<Index,d-1> dofs = dof<d-1>(slice_no<d>(dim,e));
    array<Index,3> b;
    fill(begin(b), end(b), 1);

    for(size_t i=0;i<min(dofs.size(),(size_t)2);i++)
        b[i] = dofs[i];

    if(d >= 4) {
        for(size_t i=min(dofs.size(),(size_t)2);i<dofs.size();i++)
            b[2] *= dofs[i];
    }
    

    int block_x = b[0]/tile_x + (b[0]%tile_x!=0);
    b[0] = block_x;

    return b;
}

template<size_t dx, size_t dv>
void BGK_cpu(multi_array<fp,2*(dx+dv)>& f_out, multi_array<fp,2*(dx+dv)>& f_in, 
             multi_array<fp,2*(dx+dv)>& f_mid, fp dt, fp nu, 
             multi_array<fp,2*dx>& rho, multi_array<fp,2*dx>& meanvelocity, 
             multi_array<fp,2*dx>& temperature, array<fp,dx+dv> a, 
             array<fp,dx+dv> h, array<Index,2*(dx+dv)> e){

    constexpr size_t d = dx+dv;

    if(dx!=1)
        return;


    array<Index,2*d> idx = {0};
    do {

        std::ofstream maxwellian("maxwellian.data");

        array<Index,2*dx> idx_x = {idx[0],idx[d]};

        fp v[dv];
        for(size_t i=0;i<dv;++i)
            v[i] = a[dx+i] + h[dx+i]*(idx[dx+i] + 
                   gauss::get_x_scaled01(idx[d+dx+i],e[d+dx+i]));

        fp r = rho(idx_x);
        fp u = meanvelocity(idx_x);
        fp T = temperature(idx_x);

        fp M = r/pow(2.0*M_PI*T,0.5*dv)*exp(-(v[0]-u)*(v[0]-u)/(2.0*T));

        for(size_t i=1;i<dv;++i){
            M *= exp(-v[i]*v[i]/(2.0*T));
            cout << "never in here" << endl;
        }

        f_out(idx) = f_in(idx) + dt*nu*(M-f_mid(idx));

        fp x = a[0] + h[0]*(idx_x[0]+gauss::x_scaled01(idx_x[dx],e[dx+d]));
        maxwellian << x << " " << v[0] << " " << M << " " 
                   << u << " " << T << endl;

    }while(iter_next_memorder(idx,e));

}
