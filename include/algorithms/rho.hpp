#pragma once

#include <generic/common.hpp>
#include <container/domain.hpp>
#include <algorithms/dg.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif

// Reference implementation to compute rho but quite slow.
//fill(rho.data(), rho.data()+rho.num_elements(), -1.0);
//array<Index,d> idx_x;
//array<Index,2*d> idx; fill(idx.begin(), idx.end(), 0);
//do {
//    fp w=1.0;
//    for(int i=0;i<d/2;i++) {
//        idx_x[i]=idx[i]; idx_x[d/2+i]=idx[d+i];
//        fp dv=(b[d/2+i]-a[d/2+i])/fp(e[d/2+i]);
//        w*=0.5*dv*gauss::w(idx[d+d/2+i],ov);
//    }
//    rho(idx_x)+=w*work.data(idx);
//} while(iter_next(idx,e));


template<size_t d>
vector<vector<fp>> all_weights(array<Index,d> eo) {
    int offset=0;
    vector<vector<fp>> weights;
    for(size_t i=0;i<d;i++) {
        weights.push_back(gauss::all_weights(eo[i]));
        offset += eo[i];
    }
    return weights;
}

template<size_t d>
vector<vector<fp>> all_x_scaled01(array<Index,d> eo) {
    int offset=0;
    vector<vector<fp>> x_scaled01;
    for(size_t i=0;i<d;i++) {
        x_scaled01.push_back(gauss::all_x_scaled01(eo[i]));
        offset += eo[i];
    }
    return x_scaled01;
}

// compute rho locally (parallelized using OpenMP). The reduction step has to
// be done separately.
template<size_t dx, size_t dv>
void compute_rho_omp(domain<dx+dv>& work,
        vector< multi_array<fp,2*dx> >& rho_local) {

    constexpr size_t d = dx+dv;
    int max_threads=1;
    #ifdef _OPENMP 
    max_threads = omp_get_max_threads();
    #endif
    for(int i=0;i<max_threads;i++) {
        auto& rho = rho_local[i];
        fill(rho.data(), rho.data()+rho.num_elements(), 0.0);
    }

    const Index single_out = Index(d)-1;
    index_dxdv_no<dx,dv> e(work.extension);
    // reduction is done by hand here since we have to reduce an entire array
    // and not all compilers support that yet...
    #pragma omp parallel for schedule(static)
    for(Index i=0;i<e[single_out];i++) {
        array<Index,2*d> max1=e.get_array();
        max1[single_out]=1;
        max1[0] = 1;
        max1[d] = 1;
        array<Index,2*d> idx;
        fill(idx.begin(),idx.end(),0);

        fp deltav = 1.0;
        for(size_t k=0;k<dv;k++)
            deltav *= (work.b[dx+k]-work.a[dx+k])/fp(e[dx+k]);

        vector<vector<fp>> weights = all_weights(e.o_part());

        do {
            array<Index,2*d> _idx = idx;
            _idx[single_out]=i;

            // Innermost loops is done explicitly to profit from cache locality.
            for(Index j=0;j<e[0];j++) {
                _idx[0] = j;
                for(Index o=0;o<e[d];o++) {
                    _idx[d] = o;

                    array<Index,2*dx> idx_x = index_dxdv_no<dx,dv>(_idx).x_part();

                    fp w=1.0;
                    for(size_t i=0;i<dv;i++) 
                        w *= 0.5*weights[dx+i][_idx[d+dx+i]];
                    w *= deltav;

                    int thread_num=0;
                    #ifdef _OPENMP
                    thread_num = omp_get_thread_num();
                    #endif
                    rho_local[thread_num](idx_x)+=w*work.data(_idx);
                }
            }
        } while(iter_next_memorder(idx,max1));
    }
}


// reference implementation but very slow
//{
//    fp mass2=0.0; fp momentum2=0.0; fp l12=0.0; fp l22=0.0;
//    fp K_energy2=0.0; fp entropy2=0.0;
//    array<Index,2*d> idx; fill(idx.begin(),idx.end(),0);
//    array<fp,d> x;
//    // The most expensive part here is to compute the entropy
//    do {
//        fp w=1.0;
//        for(int i=0;i<d;i++) {
//            fp dx=(b[i]-a[i])/fp(e[i]);
//            w*=0.5*dx*gauss::w(idx[d+i],e[d+i]);
//            x[i] = f.a[i] + idx[i]*dx + dx*gauss::x_scaled01(idx[d+i],e[d+i]);
//        }
//        mass2 += w*f.data(idx);
//        l12 += w*abs(f.data(idx));
//        l22 += w*pow(f.data(idx),2);
//        fp fc = max(f.data(idx),1e-32);
//        entropy2 += w*fc*log(fc);
//        fp vsq=0.0; for(int i=0;i<d/2;i++) vsq+=pow(x[d/2+i],2);
//        K_energy2 += 0.5*w*f.data(idx)*vsq;
//        momentum2 += w*x[d/2]*f.data(idx);
//    } while(iter_next(idx,e));

//    cout << mass-mass2 << " " << l1-l12 << " " << l2-l22 << " " 
//         << K_energy-K_energy2 << " " << momentum-momentum2 << endl;
//}

enum class reduction_type_cpu{rho_invariants_fluxes, meanvelocity, temperature};

template<size_t dx, size_t dv, reduction_type_cpu reduction_type>
void reduce_omp(domain<dx+dv>& work,
        vector< multi_array<fp,2*dx> >& rho_local, 
        vector< multi_array<fp,2*dx> >& u_local, 
        vector< multi_array<fp,2*dx> >& T_local, 
        fp& mass, fp& momentum, fp& l1, fp& l2, fp& K_energy, fp& entropy,
        fp& particle_flux_left, fp& particle_flux_right, 
        fp& energy_flux_left, fp& energy_flux_right) {

    constexpr size_t d = dx+dv;

    // Since we sum a large amount of data we use double for the reduction.
    // Otherweise round-off errors are a problem for single precision
    // computations.
    double _mass=0.0, _momentum=0.0, _l1=0.0, _l2=0.0, _K_energy=0.0, _entropy=0.0;
    double _particle_flux_left=0.0, _particle_flux_right=0.0;
    double _energy_flux_left=0.0, _energy_flux_right=0.0;

    // for rho
    int max_threads=1;
    #ifdef _OPENMP 
    max_threads = omp_get_max_threads();
    #endif
    for(int i=0;i<max_threads;i++) {
        if(reduction_type== reduction_type_cpu::rho_invariants_fluxes){
            auto& rho = rho_local[i];
            fill(rho.data(), rho.data()+rho.num_elements(), 0.0);
        }
        if(reduction_type== reduction_type_cpu::meanvelocity){
            auto& u = u_local[i];
            fill(u.data(), u.data()+u.num_elements(), 0.0);
        }
        if(reduction_type== reduction_type_cpu::temperature){
            auto& T = T_local[i];
            fill(T.data(), T.data()+T.num_elements(), 0.0);
        }
    }

    // mass: 0.09 s, mass+l1+l2: 0.12, all except entropy: 0.17
    const Index single_out = Index(dx+dv)-1;
    index_dxdv_no<dx,dv> e(work.extension);
    #pragma omp parallel for schedule(static) \
    reduction(+:_mass,_l1,_l2,_K_energy,_momentum,_entropy,_particle_flux_left,_particle_flux_right,_energy_flux_left,_energy_flux_right)
    for(Index i=0;i<e[single_out];i++) {
        // for rho
        fp deltav = 1.0;
        for(size_t k=0;k<dv;k++) {
            deltav *= (work.b[dx+k]-work.a[dx+k])/fp(e[dx+k]);
        }
        
        vector<fp> deltax(dx+dv);
        for(size_t k=0;k<dx+dv;k++)
            deltax[k] = (work.b[k]-work.a[k])/fp(e[k]);

        vector<vector<fp>> weights   = all_weights(e.o_part());
        vector<vector<fp>> positions = all_x_scaled01(e.o_part());

        array<Index,2*d> max_io = e.get_array();
        max_io[0] = 1;
        max_io[d] = 1;
        max_io[single_out] = 1;
        array<Index,2*d> max = to_memorder<d>(max_io);
        // TODO: is that we have use the full index here a problem for
        // performance?
        for(auto j : range<2*d>(max)) {
            array<Index,2*d> _idx = from_memorder<d>(j);
            _idx[single_out] = i;

            fp w1=1.0;
            array<fp,dx+dv> x;
            for(size_t i=1;i<dx+dv;i++) {
                w1 *= 0.5*deltax[i]*weights[i][_idx[dx+dv+i]];
                x[i] = work.a[i] + _idx[i]*deltax[i]
                       + deltax[i]*positions[i][_idx[dx+dv+i]];
            }
            w1 *= 0.5*deltax[0];

            int v1idx = _idx[d+dx] + _idx[dx]*e[d+dx];

            for(Index k=0;k<e[0];k++) {
                _idx[0] = k;
                for(Index o=0;o<e[d];o++) {
                    _idx[d] = o;

                    // for invariants
                    x[0] = work.a[0] + _idx[0]*deltax[0] 
                           + deltax[0]*positions[0][o];
                    fp w = w1*weights[0][o];
                    fp val = work.data(_idx);

                    // for rho
                    array<Index,2*dx> idx_x = index_dxdv_no<dx,dv>(_idx).x_part();
                    fp wx=1.0;
                    for(size_t i=0;i<dv;i++)
                        wx *= 0.5*weights[dx+i][_idx[dx+dv+dx+i]];
                    wx *= deltav;

                    int thread_num=0;
                    #ifdef _OPENMP
                    thread_num = omp_get_thread_num();
                    #endif
 
                    if(reduction_type==reduction_type_cpu::rho_invariants_fluxes){

                        _mass += w*val;
                        _l1 += w*abs(val);
                        _l2 += w*val*val;
                        //fp fc = max(val,1e-32);
                        //_entropy += w*fc*log(fc);
                        fp vsq=0.0;
                        for(size_t i=0;i<dv;i++)
                            vsq+=x[dx+i]*x[dx+i];
                        _K_energy += 0.5*w*val*vsq;
                        _momentum += w*x[dx]*val;


                        if(k==0 && o==0){
                            if(v1idx<e[dx]*e[d+dx]*0.5){
                                _particle_flux_left += wx*x[dx]*val;
                                _energy_flux_left += wx*x[dx]*vsq*val*0.5;
                            }
                        }
                        if(k==e[0]-1 && o==e[d]-1){
                            if(v1idx>=e[dx]*e[d+dx]*0.5){
                                _particle_flux_right += wx*x[dx]*val;
                                _energy_flux_right += wx*x[dx]*vsq*val*0.5;
                            }
                        }

                        rho_local[thread_num](idx_x)+=wx*val;
                    }

                    if(reduction_type==reduction_type_cpu::meanvelocity){
                        fp rho_x = (rho_local[0](idx_x)+1e-13);
                        u_local[thread_num](idx_x)+=wx*x[dx+0]*val/rho_x;
                    }
                    if(reduction_type==reduction_type_cpu::temperature){
                        fp rho_x = (rho_local[0](idx_x)+1e-13);
                        fp u = u_local[0](idx_x);
                        T_local[thread_num](idx_x) +=
                            wx*(x[dx+0]-u)*(x[dx+0]-u)*val/rho_x;
                    }
 
                }
            }
        }
    }

    mass = _mass;
    momentum = _momentum;
    l1 = _l1;
    l2 = _l2;
    K_energy = _K_energy;
    entropy = _entropy;

    particle_flux_left = _particle_flux_left;
    particle_flux_right = _particle_flux_right;
    energy_flux_left = _energy_flux_left;
    energy_flux_right = _energy_flux_right;
}


template<size_t dx, size_t dv>
void compute_invariants_and_rho_omp(domain<dx+dv>& work,
        vector< multi_array<fp,2*dx> >& rho_local, fp& mass,
        fp& momentum, fp& l1, fp& l2, fp& K_energy,
        fp& entropy, fp& particle_flux_left, fp& particle_flux_right,
        fp& energy_flux_left, fp& energy_flux_right) {

    constexpr reduction_type_cpu rt = reduction_type_cpu::rho_invariants_fluxes;
    reduce_omp<dx,dv,rt>(work, rho_local, rho_local, rho_local, mass,
        momentum, l1, l2, K_energy, entropy, particle_flux_left, 
        particle_flux_right, energy_flux_left, energy_flux_right);
}

template<size_t dx, size_t dv>
void compute_mean_velocity(domain<dx+dv>& work,
        vector< multi_array<fp,2*dx> >& rho_local, 
        vector< multi_array<fp,2*dx> >& u_local) {

    constexpr reduction_type_cpu rt = reduction_type_cpu::meanvelocity;
    fp dummy;
    reduce_omp<dx,dv,rt>(work, rho_local, u_local, u_local, dummy,
        dummy, dummy, dummy, dummy, dummy, dummy, dummy, dummy, dummy);
}

template<size_t dx, size_t dv>
void compute_temperature(domain<dx+dv>& work,
        vector< multi_array<fp,2*dx> >& rho_local,
        vector< multi_array<fp,2*dx> >& u_local,
        vector< multi_array<fp,2*dx> >& T_local) {

    constexpr reduction_type_cpu rt = reduction_type_cpu::temperature;
    fp dummy;
    reduce_omp<dx,dv,rt>(work, rho_local, u_local, T_local, dummy,
        dummy, dummy, dummy, dummy, dummy, dummy, dummy, dummy, dummy);
}


template<size_t d>
void interpolate_to_equidistant_grid(multi_array<fp,2*d>& rho,
        vector<fp>& out, array<Index,2*d> ex, vector<fp>& tmp) {

    // copy rho into tmp
    array<Index,2*d> ex_mo = to_memorder<d>(ex);
    for(auto i : range<2*d>(ex)) {
        array<Index,2*d> i_mo = to_memorder<d>(i);
        tmp[linearize_idx(i_mo,ex_mo)] = rho(i);
    }

    // interpolate along each of the d directions
    for(size_t k=0;k<d;k++) {
        Index o = ex[d+k];

        array<Index,2*d> ex_mo_p = ex_mo;
        ex_mo_p[2*d-1] = 1;
        #pragma omp parallel for
        for(Index p=0;p<ex_mo[2*d-1];p++) {
            vector<fp> tmpo(o);
            for(auto _i_mo : range<2*d>(ex_mo_p)) {
                array<Index,2*d> i_mo = _i_mo;
                i_mo[2*d-1] = p;

                // populate tmpo
                for(Index m=0;m<o;m++) {
                    array<Index, 2*d> idx = i_mo;
                    idx[2*k] = m;
                    tmpo[m] = tmp[linearize_idx(idx,ex_mo)];
                }

                out[linearize_idx(i_mo,ex_mo)] = 
                    evaluate1d(equi_x(i_mo[2*k],o),o,&tmpo[0]);
            }
        }

        tmp = out;
    }
}

// interpolate from equidistant to dG grid
template<size_t d>
void interpolate_to_dg_grid(const array< vector<fp>, d>& in, 
        array< multi_array<fp,2*d>, d>& E, array<Index,2*d> ex,
        array< multi_array<fp,2*d>, d>& E_tmp) {


    array<Index,2*d> ex_mo = to_memorder<d>(ex);
    // copy in into E_tmp
    for(auto i : range<2*d>(ex)) {
        array<Index,2*d> i_mo = to_memorder<d>(i);

        for(size_t k=0;k<d;k++)
            E_tmp[k](i) = in[k][linearize_idx(i_mo,ex_mo)];
    }

    // interpolate each of the d directions
    for(size_t k=0;k<d;k++) {
        Index o = ex[d+k];

        for(size_t l=0;l<d;l++) {

            array<Index,2*d> ex_p = ex;
            ex_p[2*d-1] = 1;
            #pragma omp parallel for
            for(Index p=0;p<ex[2*d-1];p++) {
                vector<fp> tmpo(o);
                
                for(auto _i : range<2*d>(ex_p)) {
                    array<Index,2*d> i = _i;
                    i[2*d-1] = p;

                    for(Index m=0;m<o;m++) {
                        array<Index, 2*d> idx = i;
                        idx[d+k] = m;
                        tmpo[m] = E_tmp[l](idx);
                    }
                    fp xj = gauss::x_scaled01(i[d+k],o);
                    E[l](i) = interpolate1d(xj,o,&tmpo[0]);
                }
            }

            E_tmp[l] = E[l];
        }
    }
}

