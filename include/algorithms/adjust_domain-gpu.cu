#pragma once

#include <cub/cub.cuh> 
#include <generic/common.hpp>
#include <algorithms/dg-gpu.cu>

struct Max_abs {
    template <typename T>
    __device__ __forceinline__
    T operator()(const T &a, const T &b) const {
        return (abs(b) > abs(a)) ? abs(b) : abs(a);
    }
};

template<size_t d>
struct max_abs_v_stride{

    char* d_temp_storage = nullptr;
    size_t temp_storage_bytes = 0;
    fp *d_max_abs = nullptr;    
    Max_abs max_abs_op;

    max_abs_v_stride(multi_array<fp,2*d>& d_f, Index dof_x) {

        fp init = 0.0;
        //Allocate memory for the solution
        gpuErrchk(cudaMalloc(&d_max_abs,sizeof(fp)));

        // Determine temporary device storage requirements
        cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, 
                                  d_f.data(), 
                                  d_max_abs, dof_x, max_abs_op, init);

        // Allocate temporary storage
        gpuErrchk(cudaMalloc(&d_temp_storage, temp_storage_bytes));
    }

    ~max_abs_v_stride(){
        gpuErrchk(cudaFree(d_max_abs));
        gpuErrchk(cudaFree(d_temp_storage));
    }

    fp get(Index idx, Index dof_x, multi_array<fp,2*d>& d_f){

        // Run reduction
        Index offset = idx*dof_x;
        fp init = 0.0;
        cub::DeviceReduce::Reduce(d_temp_storage, temp_storage_bytes, 
                                  d_f.data() + offset, 
                                  d_max_abs, dof_x, max_abs_op, init);
        fp max_abs_value;
        gpuErrchk(cudaMemcpy(&max_abs_value,d_max_abs,
                             sizeof(double),cudaMemcpyDeviceToHost));

        return max_abs_value;
    }

};

__device__
void __syncthreads();

//<<<(tile_x,num_cell_v1,dof_v2v3),dof_x/tile_x>>>
template<size_t dx, size_t dv, Index ov>
__global__
void k_project_on_finer_grid_v1(fp av1_old, fp bv1_old, fp av1_new, fp bv1_new,
                                Index* e, fp reduced_cell, fp* f_in, fp* f_out){

    constexpr size_t d = dx+dv;

    //+1 to avoid bank conflicts..?
    __shared__ fp A[ov*ov+1];
    __shared__ fp B[ov*ov+1];


    size_t dof_x = e[0]*e[d];
    size_t dof_v = e[1]*e[d+1];

    size_t dof_x_idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(dof_x_idx>=dof_x)
        return;

    f_in += blockIdx.z*dof_x*dof_v;
    f_out += blockIdx.z*dof_x*dof_v;

    Index iv = blockIdx.y;

    fp h_new = (bv1_new-av1_new)/fp(e[1]);
    fp h_old = (bv1_old-av1_old)/fp(e[1]);

    fp vl = av1_new + iv*h_new;
    fp vr = av1_new + (iv+1)*h_new;

    int i_old1 = ((vl - av1_old)*(1.0+1e-13))/h_old;
    int i_old2 = ((vr - av1_old)*(1.0-1e-13))/h_old;

    if(threadIdx.x < ov*ov){

        fp vl_scaled = (vl - (av1_old + i_old1*h_old))/h_old;
        fp vr_scaled = (vr - (av1_old + i_old2*h_old))/h_old;

        fp a1 = vl_scaled;
        fp b1 = (i_old1 != i_old2) ? 1.0 : vr_scaled;

        fp a2 = 0.0;
        fp b2 = (i_old1 == i_old2) ? 0.0 : vr_scaled;

        fp hl = b1-a1;
        fp hr = b2-a2;

        int i = threadIdx.x/ov;
        int j = threadIdx.x-i*ov;

        A[threadIdx.x] = 0.0;
        B[threadIdx.x] = 0.0;

        for(int k=0;k<ov;++k){

            fp xl_k = a1 + hl*gauss::get_x_scaled01(k,ov);
            A[threadIdx.x] += gauss::get_w(k,ov)*hl*0.5*
                              lagrange_dg_gpu(xl_k,j,ov)*
                              lagrange_dg_gpu((xl_k-a1)/reduced_cell,i,ov);

            if(i_old1!=i_old2){
                fp xr_k = hr*gauss::get_x_scaled01(k,ov);
                B[threadIdx.x] += gauss::get_w(k,ov)*hr*0.5*
                                  lagrange_dg_gpu(xr_k,j,ov)*
                                  lagrange_dg_gpu((xr_k+(1.0-a1))/
                                                        reduced_cell,i,ov);
            }
        }
    }
    
    fp local_in[2*ov];
    for(Index k=0;k<2*ov;++k)
        local_in[k] = f_in[dof_x_idx + (k+i_old1*ov)*dof_x];

    __syncthreads();

    fp local_out[ov];
    fp mean = 0.0; 
    fp min_out = 1e+5; 
    for(int i=0;i<ov;++i){
        local_out[i] = 0.0;
        for(int j=0;j<ov;++j){
            local_out[i] += A[j+ov*i]*local_in[j] + 
                            B[j+ov*i]*local_in[ov+j];
        }
        local_out[i] *= 2.0/(gauss::get_w(i,ov)*reduced_cell);
        mean += local_out[i]*gauss::get_w(i,ov)*0.5;
        min_out = min(min_out,local_out[i]);
        //max_out = max(max_out,local_out[i]);
    }
    
    //fp theta = min(abs(mean/(min_out-mean)),1.0);

    for(int i=0;i<ov;++i)
        f_out[dof_x_idx + (i+iv*ov)*dof_x] = local_out[i];
        //f_out[dof_x_idx + (i+iv*ov)*dof_x] = theta*(local_out[i]-mean)+mean;
}


template<size_t dx, size_t dv>
void project_on_finer_grid_gpu(fp av1_old, fp bv1_old, fp av1_new, fp bv1_new,
                               array<Index,2*(dx+dv)> _e, fp reduced_cell, 
                               multi_array<fp,2*(dx+dv)>& f_in, 
                               multi_array<fp,2*(dx+dv)>& f_out){

    constexpr size_t d = dx+dv;

    gpu_array<Index> e(2*d);
    e.set(_e.begin(),_e.end());

    int dof_v2v3 = 1;
    for(int i=2;i<d;++i)
        dof_v2v3 *= e[i]*e[d+i];

    int threads = e[0]*e[d+0];
    int tile_x = 1;
    if(threads>512){
        int divisor = threads/512+1;
        threads = (threads+divisor-1)/divisor; 
        tile_x *= divisor;
    }

    dim3 blocks(tile_x,e[1],dof_v2v3);

    if(e[d+1]==1)
        k_project_on_finer_grid_v1<dx,dv,1><<<blocks,threads>>>(
                                av1_old, bv1_old, av1_new, bv1_new, e.d_ptr(), 
                                reduced_cell, f_in.data(), f_out.data());
    else if(e[d+1]==2)
        k_project_on_finer_grid_v1<dx,dv,2><<<blocks,threads>>>(
                                av1_old, bv1_old, av1_new, bv1_new, e.d_ptr(), 
                                reduced_cell, f_in.data(), f_out.data());
    else if(e[d+1]==3)
        k_project_on_finer_grid_v1<dx,dv,3><<<blocks,threads>>>(
                                av1_old, bv1_old, av1_new, bv1_new, e.d_ptr(), 
                                reduced_cell, f_in.data(), f_out.data());
    else if(e[d+1]==4)
        k_project_on_finer_grid_v1<dx,dv,4><<<blocks,threads>>>(
                                av1_old, bv1_old, av1_new, bv1_new, e.d_ptr(), 
                                reduced_cell, f_in.data(), f_out.data());
    else if(e[d+1]==5)
        k_project_on_finer_grid_v1<dx,dv,5><<<blocks,threads>>>(
                                av1_old, bv1_old, av1_new, bv1_new, e.d_ptr(), 
                                reduced_cell, f_in.data(), f_out.data());
    else{
        cout << "ERROR: projection on finer grid currently only implemented "
             << "for o<6.\n";
        exit(1);
    }

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

}


