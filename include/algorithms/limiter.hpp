#pragma once

#include <generic/common.hpp>
#include <algorithms/sldg2d/generic_sldg2d.hpp>


#ifdef __CUDACC__
__host__ __device__
#endif
inline fp lagrange_dg_gpu(fp x, int j, int o){

    fp prod = 1.0;;
    for(int i=0;i<j;++i)   
        prod*=(x-gauss::get_x_scaled01(i,o))/
              (gauss::get_x_scaled01(j,o)-gauss::get_x_scaled01(i,o));
    for(int i=j+1;i<o;++i) 
        prod*=(x-gauss::get_x_scaled01(i,o))/
              (gauss::get_x_scaled01(j,o)-gauss::get_x_scaled01(i,o));

    return prod;
}


//coeff (a_k, a_(k-1), ..., a_1, a_0)
//from poly a_k*x^k + ... a_1*x^1 + a_0
template<int k>
#ifdef __CUDACC__
__host__ __device__
#endif
fp evaluate_standard_basis(fp x, fp* coeff){
    fp y = coeff[0];
    for(size_t i=0;i<k;++i){
        y = x*y + coeff[i+1];
    }
    return y;
}


#ifdef __CUDACC__
__host__ __device__
#endif
inline void standard_basis_coeff_o4(fp* values_in, fp* coeff_out){

    coeff_out[0] = -7.420540068038948*values_in[0] +
                    1.879544940755506e+1*values_in[1] +
                   -1.879544940755506e+1*values_in[2] +
                    7.420540068038948*values_in[3];
    coeff_out[1] =  1.432585835417189e+1*values_in[0] -
                    3.138822236344606e+1*values_in[1] +
                    2.499812585921912e+1*values_in[2] -
                    7.935761849944954*values_in[3];
    coeff_out[2] = -8.546023607872199*values_in[0] +
                    1.380716692568958e+1*values_in[1] +
                   -7.417070421462640*values_in[2] +
                    2.155927103645262*values_in[3];
    coeff_out[3] =  1.526788125457267*values_in[0] -
                    8.136324494869274e-1*values_in[1] +
                    4.007615203116505e-01*values_in[2] -
                    1.139171962819900e-01*values_in[3];
}


//max_v and min_v have to be set appropriately before passing!!
#ifdef __CUDACC__
__host__ __device__
#endif
inline void maxmin_o4(fp* values_in, fp a, fp b, fp* coeff, 
                      fp& max_v, fp& min_v, fp& fl, fp& fr){

    constexpr int o=4;
    constexpr fp tol = 1e-13;

    standard_basis_coeff_o4(values_in,coeff);

    fp mean_coeff = (abs(coeff[0])+abs(coeff[1])+abs(coeff[2])+abs(coeff[3]))/4.0;

    fl = evaluate_standard_basis<o-1>(a,coeff);
    min_v = min(min_v, fl);
    max_v = max(max_v, fl);

    fr = evaluate_standard_basis<o-1>(b,coeff);
    min_v = min(min_v, fr);
    max_v = max(max_v, fr);

    //make sure we deal with a cubic polynomial
    //otherwise we divide by 0 in finding x_min, x_max
    if(fabs(coeff[0]/mean_coeff)>tol){
        fp delta = coeff[1]*coeff[1] - 3.0*coeff[0]*coeff[2];
        if(delta>0){
            fp x1 = (-coeff[1] - sqrt(delta))/(3.0*coeff[0]);
            if(x1>a && x1<b){
                fp in_interior = evaluate_standard_basis<o-1>(x1,coeff);
                min_v = min(min_v, in_interior);
                max_v = max(max_v, in_interior);
            }

            fp x2 = (-coeff[1] + sqrt(delta))/(3.0*coeff[0]);
            if(x2>a && x2<b){
                fp in_interior = evaluate_standard_basis<o-1>(x2,coeff);
                min_v = min(min_v, in_interior);
                max_v = max(max_v, in_interior);
            }
        }
    } else{
        if(fabs(coeff[1]/mean_coeff)>tol){
            //polynomial is quadratic
            fp x = -coeff[2]/(2.0*coeff[1]);
            if(x>a && x<b){
                fp in_interior = evaluate_standard_basis<o-1>(x,coeff);
                min_v = min(min_v, in_interior);
                max_v = max(max_v, in_interior);
            }
        }
        //no other if else because when the polynomial is linear,
        //max and min lie on the boundary which are already computed
    }
}


inline
void maxmin_o4_with_points(fp* values_in1, fp* values_in2, fp* values_out,
                           fp alpha, fp& max_in, fp& min_in,
                           fp& max_out, fp& min_out){
    constexpr int o = 4;
    int number_points = 200;
    fp hx = 1.0/fp(number_points-1);
    constexpr fp scale[4] = {-7.420540068038946, 18.79544940755506,
                             -18.79544940755506, 7.420540068038949};

    for(int nn=0;nn<number_points;++nn){
        fp x = nn*hx;

        fp l[4] = {scale[0],scale[1],scale[2],scale[3]};     
        fp p = 1.0;

        for(int k=0;k<o-1;++k){
            p*= (x-gauss::get_x_scaled01(k,o));
            l[k+1] *= p;
        }
        p = 1.0;
        for(int k=o-1;k>0;--k){
            p*= (x-gauss::get_x_scaled01(k,o));
            l[k-1] *= p;
        }

        fp y_i = 0.0;
        fp y_o = 0.0;
        for(int k=0;k<o;++k){
            y_o += values_out[k]*l[k]; 
            if(x<alpha)
                y_i += values_in2[k]*l[k]; 
            else
                y_i += values_in1[k]*l[k];
        }
        
        max_in = max(max_in, y_i);
        min_in = min(min_in, y_i);

        max_out = max(max_out, y_o);
        min_out = min(min_out, y_o);

    }
}



#ifdef __CUDACC__
__host__ __device__
#endif
inline void compute_theta_o4(fp* values_in1, fp* values_in2, fp* values_out, 
                             fp alpha, fp& theta, fp& mean){

    constexpr int o = 4;

    fp coeff_i1[o];
    fp coeff_i2[o];
    fp coeff_o[o];

    fp max_in = -200.0;
    fp min_in =  200.0;
    fp tmp;
    maxmin_o4(values_in1, alpha, 1.0, coeff_i1, max_in, min_in, tmp, tmp);
    maxmin_o4(values_in2, 0.0, alpha, coeff_i2, max_in, min_in, tmp, tmp);

    fp max_out = -200.0;
    fp min_out =  200.0;
    maxmin_o4(values_out, 0.0, 1.0, coeff_o, max_out, min_out, tmp, tmp);

    //fp max_in2 = -1000;
    //fp min_in2 = 1000;
    //fp max_out2 = -1000;
    //fp min_out2 = 1000;
    //maxmin_o4_with_points(values_in1, values_in2, values_out,
    //                      alpha, max_in2, min_in2, max_out2, min_out2);

    //fp err_Mi = max_in2-max_in;
    //fp err_mi = min_in2-min_in;
    //fp err_Mo = max_out2-max_out;
    //fp err_mo = min_out2-min_out;

    //fp theta2 = min(min((max_in - mean)/(max_out - mean),
    //                    (mean - min_in)/(mean - min_out)),1.0);
    //if(theta2<0.0)
    //    cout << "err_Max_in: " << err_Mi << ", err_min_in: " << err_mi 
    //         << ", err_Max_out: "<< err_Mo << " err_min_out: " << err_mo << endl;

    mean = 0.0;
    for(int i=0;i<o;++i)
        mean += values_out[i]*gauss::get_w(i,o)*0.5;

    //if(abs(mean)>1e-16){

        //fp theta2 = min(min((max_in - mean)/(max_out - mean),
        //                    (mean - min_in)/(mean - min_out)),1.0);

    theta = min(min(abs((max_in - mean)/(max_out - mean)),
                    abs((mean - min_in)/(mean - min_out))),fp(1.0));

        //if(theta<0 || theta2!=theta){
        //    cout << "\n\ntheta: " << std::setprecision(15) << theta << endl;
        //    cout << "min_in: " << min_in << ", max_in: " << max_in << endl;
        //    cout << "min_out: " << min_out << ", max_out: " << max_out << endl;
        //    cout << "coeff_i1: " << coeff_i1[0] << ", "  << coeff_i1[1] 
        //         << ", "  << coeff_i1[2] << ", " << coeff_i1[3] << endl;
        //    cout << "coeff_i2: " << coeff_i2[0] << ", "  << coeff_i2[1] 
        //         << ", "  << coeff_i2[2] << ", " << coeff_i2[3] << endl;
        //    cout << "coeff_o: " << coeff_o[0] << ", "  << coeff_o[1] 
        //         << ", "  << coeff_o[2] << ", " << coeff_o[3] << endl;
        //    cout << "alpha: " << alpha << endl;
        //}
    //} else { 
    //    theta = 1.0;
    //    mean = 0.0;
    //}
}
  

inline
#ifdef __CUDACC__
__host__ __device__
#endif
void limit_poly_o4(fp* values_in){

    constexpr int o = 4;

    fp coeff[o];

    fp max_in = -2.0;
    fp min_in =  2.0;
    fp tmp;
    maxmin_o4(values_in, 0.0, 1.0, coeff, max_in, min_in, tmp, tmp);

    fp mean = (3.478548451374540e-01*values_in[0] +
               6.521451548625460e-01*values_in[1] +
               6.521451548625460e-01*values_in[2] +
               3.478548451374540e-01*values_in[3])*0.5;

    fp theta = abs(min_in-mean)<1e-13 ? 0.0 : 
                         min(abs(mean/(min_in-mean)),fp(1.0));

        //cout << "mean: " << mean << endl;
        //cout << "min: " << min_in << endl;
        //cout << "theta: "<< theta << endl;
        //cout << values_in[0] << " " << values_in[1] << " "
        //     << values_in[2] << " " << values_in[3] << endl;

    for(int l=0;l<o;++l)
        values_in[l] = theta*(values_in[l]-mean) + mean;
}


template<int o>
#ifdef __CUDACC__
__host__ __device__
#endif
fp compute_relative_maxerr_projection(fp alpha, fp* out, fp* in){

    fp gx[o];
    for(int i=0;i<o;++i)
        gx[i] = gauss::get_x_scaled01(i,o);

    fp x = 0.0;
    int iter;
    for(iter=0;iter<o+1 && x<alpha;++iter)
        x = gx[iter%o];
    iter--;

    fp maxerr = 0.0;
    for(int i=0;i<o;++i){
        x = x - alpha + ((i+iter>=o) ? 1.0 : 0.0);
        fp f_at_x = 0;
        for(int k=0;k<o;++k){
            fp lag=1.0;
            for(int l=0;l<k;++l)
                lag *= (gx[l]-x)/(gx[l]-gx[k]);
            for(int l=k+1;l<o;++l)
                lag *= (gx[l]-x)/(gx[l]-gx[k]);
            f_at_x += out[k]*lag;
        }
        fp in_ref = in[iter+i];
        maxerr = max(maxerr,abs((f_at_x-in_ref)/in_ref));
        x = gx[(iter+i+1)%o];
    }

    return maxerr;
}


#ifdef __CUDACC__
__host__ __device__
#endif
inline int sign(fp a){
    return (a>fp(0))-(a<fp(0));
}

#ifdef __CUDACC__
__host__ __device__
#endif
inline fp pow2(fp x){
    return x*x;
}


#ifdef __CUDACC__
__host__ __device__
#endif
inline fp minmod(fp f1, fp f2, fp f3){
    int s1 = sign(f1);
    int s2 = sign(f2);
    int s3 = sign(f3);
    int s = (s1+s2+s3)/3;
    return s*min(f1*s1,min(f2*s2,f3*s3));
}


//M =  depends on the problem the say. 
//M = 1,100,300,500,... possible choices, I don't like this..
#ifdef __CUDACC__
__host__ __device__
#endif
inline fp modified_minmod(fp f1, fp f2, fp f3, fp M, fp h){
    if(abs(f1) < M*h*h)
        return f1;
    else
        return minmod(f1,f2,f3);
}

template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
bool minmod_troubled_indicator(fp* f){

    fp mean_u_0 = 0.0;
    fp mean_u_1 = 0.0;
    fp mean_u_2 = 0.0;
    
    fp u_1p12 = 0.0;
    fp u_1m12 = 0.0;
    
    for(size_t j=0;j<o;++j){
        u_1p12 += f[j+o]*lagrange_dg_gpu(1.0,j,o);
        u_1m12 += f[j+o]*lagrange_dg_gpu(0.0,j,o);
        mean_u_0 += f[j+0*o]*gauss::get_w(j,o)*0.5;
        mean_u_1 += f[j+1*o]*gauss::get_w(j,o)*0.5;
        mean_u_2 += f[j+2*o]*gauss::get_w(j,o)*0.5;
    }

    fp u_bar_p = u_1p12 - mean_u_1;
    fp u_bar_m = mean_u_1 - u_1m12;
    
    fp delta_p = mean_u_2 - mean_u_1;
    fp delta_m = mean_u_1 - mean_u_0;
    
    fp u_bar_p_mod = minmod(u_bar_p,delta_p,delta_m);
    fp u_bar_m_mod = minmod(u_bar_m,delta_p,delta_m);

    if((u_bar_p_mod != u_bar_p)||(u_bar_m_mod != u_bar_m)){
        return true; 
    }
    else {
        return false;
    }
}


template<int nom, int denom, Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
bool mean_troubled_indicator(fp* f){

    fp mean1 = 0;
    fp mean0 = 0;
    fp mean2 = 0;
    fp mean_extension_1 = 0;
    fp mean_extension_2 = 0;

    for(Index k=0;k<o;++k){
        mean1 += f[k+0*o]*gauss::get_w(k,o)*0.5;
        mean0 += f[k+1*o]*gauss::get_w(k,o)*0.5;
        mean2 += f[k+2*o]*gauss::get_w(k,o)*0.5;
        for(Index j=0;j<o;++j){
            mean_extension_1 += f[k]*lagrange_dg_gpu(
                                     gauss::get_x_scaled01(j,o)+1,k,o)*
                                     gauss::get_w(j,o)*0.5;
            mean_extension_2 += f[k+2*o]*lagrange_dg_gpu(
                                         gauss::get_x_scaled01(j,o)-1,k,o)*
                                         gauss::get_w(j,o)*0.5;
        }
    }

    fp max_mean = max(max(abs(mean1),abs(mean2)),abs(mean0));

    if(max_mean<1e-14)
        return false;

    fp mean_extension_err = (abs(mean_extension_1-mean0) + 
                             abs(mean_extension_2-mean0))/max_mean;

    if(mean_extension_err>fp(nom)/fp(denom))
        return true;
    else
        return false;
}


template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
fp mean_indicator_sldg(fp alpha, fp* fin1, fp* fin2, fp* out, 
                       fp* scale){

    fp meanexpand_f1=0.0;
    fp meanexpand_f2=0.0;
    fp meanf1 = 0.0;
    fp meanf2 = 0.0;
    for(int i=0;i<o;++i){
        meanf1 += fin1[i]*gauss::get_w(i,o)*0.5;
        meanf2 += fin2[i]*gauss::get_w(i,o)*0.5;
        fp x1 = gauss::get_x_scaled01(i,o)-alpha;
        fp x2 = gauss::get_x_scaled01(i,o)+1.0-alpha;
        for(int k=0;k<o;++k){
            fp l1=1.0;
            fp l2=1.0;
            for(int l=0;l<k;++l){
                l1 *= (x1-gauss::get_x_scaled01(l,o));
                l2 *= (x2-gauss::get_x_scaled01(l,o));
            }
            for(int l=k+1;l<o;++l){
                l1 *= (x1-gauss::get_x_scaled01(l,o));
                l2 *= (x2-gauss::get_x_scaled01(l,o));
            }
            meanexpand_f1 += out[k]*l1*scale[k]*gauss::get_w(i,o)*0.5;
            meanexpand_f2 += out[k]*l2*scale[k]*gauss::get_w(i,o)*0.5;
        }
    }
    fp maxmean = max(abs(meanf1),abs(meanf2));

    return (maxmean<1e-13) ? 0.0 : 
           (abs(meanexpand_f1-meanf1) + abs(meanexpand_f2-meanf2))/maxmean;
}


//computes d/dx phi_i^o(gauss::x_scaled01(k,o-1))
//where phi_i^o i is the i-th lagrange basis polynomial of degree o-1
//o<=4
#ifdef __CUDACC__
__host__ __device__
#endif
inline
fp precomp_lagrange_derivative(int o, int i, int gauss_x_idx){

    constexpr int omax=4;
    //assert(o<=omax && i<o && gauss_x_idx<o-1);

    static const fp all_lag_deriv_o4[4*3] = {
                                      -5.5996870694999625,  7.4483575854625065,
                                      -2.4986101171566743,  0.64993960119412996,
                                       0.21442969527047969,-3.4844683820901876,
                                       3.4844683820901876, -0.21442969527047994,
                                      -0.64993960119413007, 2.4986101171566752,
                                      -7.4483575854625101,  5.5996870694999643};
    static const fp all_lag_deriv_o3[3*2] = {
                                      -3.2154953460345581, 3.8490017945975050,
                                      -0.6335064485629471, 0.63350644856294668,
                                      -3.8490017945975045, 3.2154953460345581};
    static const fp all_lag_deriv_o2[2*1] = {
                                      -1.7320508075688774, 1.7320508075688774};
    static fp const * const all_lag_deriv[3] = {all_lag_deriv_o4,
                                                all_lag_deriv_o3,
                                                all_lag_deriv_o2};
    int idx = i+o*gauss_x_idx;
    return all_lag_deriv[omax-o][idx];
}


inline
#ifdef __CUDACC__
__host__ __device__
#endif
fp lagrange_derivative_gpu(fp x, int j, Index o) {

    fp sum=0.0;
    for(int m=0;m<o;m++) {
        if(m != j) {
            fp prod=1.0;
            for(int i=0;i<o;i++)
                if(i != j && i != m)
                    prod*=(x-gauss::get_x_scaled01(i,o))
                  /(gauss::get_x_scaled01(j,o)-gauss::get_x_scaled01(i,o));

            sum += prod/(gauss::get_x_scaled01(j,o)-gauss::get_x_scaled01(m,o));
        }
    }
    return sum;
}


template<size_t o, bool modified=false>
#ifdef __CUDACC__
__host__ __device__
#endif
fp compute_beta(fp* f){

    //o>4 not supported
    fp beta = 0.0;

    fp derivative[o]; //o-1 not allowed in device code

    //modifying input not allowed since it is used in the WENO interpolation!
    fp u[o];
    for(int k=0;k<o;++k){
        u[k] = f[k];
    }

    fp l = 1.0;
    for(int s=1;s<o;++s){

        fp integral = 0.0;
        for(int k=0;k<o-s;++k){

            fp tmp = 0.0;
            for(int i=0;i<o-s+1;++i){
                fp lagrange_deriv = precomp_lagrange_derivative(o-s+1,i,k);
                tmp += u[i]*lagrange_deriv;
            }

            integral += tmp*tmp*0.5*gauss::get_w(k,o-s);

            derivative[k] = tmp;
        }

        beta += integral*l*l;
        
        if(modified)
            l /= fp(s+1);

        for(int k=0;k<o-s;++k){
            u[k] = derivative[k];
        }
    }

    return beta;
}


template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_simple_weno(fp* in, fp* out){

    fp beta[3];
    beta[0] = compute_beta<o>(&in[0]);
    beta[1] = compute_beta<o>(&in[o]);
    beta[2] = compute_beta<o>(&in[2*o]);

    fp mean0 = 0;
    fp mean1 = 0;
    fp mean2 = 0;
    for(Index i =0;i<o;++i){
        mean0 += in[i+0*o]*gauss::get_w(i,o)*0.5;
        mean1 += in[i+1*o]*gauss::get_w(i,o)*0.5;
        mean2 += in[i+2*o]*gauss::get_w(i,o)*0.5;
    }

    constexpr fp gamma0 = 0.001;
    constexpr fp gamma1 = 0.998;
    constexpr fp gamma2 = 0.001;

    fp omega0 = gamma0/pow2(1e-6 + beta[0]);
    fp omega1 = gamma1/pow2(1e-6 + beta[1]);
    fp omega2 = gamma2/pow2(1e-6 + beta[2]);

    fp isumm = 1./(omega0+omega1+omega2);
    omega0 *= isumm;
    omega1 *= isumm;
    omega2 *= isumm;

    for(Index k=0;k<o;++k)
        out[k] = omega0*(in[k]-mean0+mean1) + 
                 omega1*in[k+o] + 
                 omega2*(in[k+2*o]-mean2+mean1);
}


template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_average(fp* in, fp* out){

    fp mean1 = 0;
    for(Index i =0;i<o;++i){
        mean1 += in[i+1*o]*gauss::get_w(i,o)*0.5;
    }

    for(Index i=0;i<o;++i){
        out[i] = mean1;
    }
}

template<int nom, int denom, Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_linear_weno(fp* in, fp* out){

    constexpr fp gamma0 = fp(nom)/fp(denom);
    constexpr fp gamma1 = (1.0-gamma0)*0.5;
    constexpr fp gamma2 = gamma1;

    fp mean1 = 0;
    fp mean0 = 0;
    fp mean2 = 0;
    for(Index i =0;i<o;++i){
        mean1 += in[i+0*o]*gauss::get_w(i,o)*0.5;
        mean0 += in[i+1*o]*gauss::get_w(i,o)*0.5;
        mean2 += in[i+2*o]*gauss::get_w(i,o)*0.5;
    }

    fp local_in[3*o];
    for(int i=0;i<o;++i){
        fp x = gauss::get_x_scaled01(i,o);
        local_in[i+0*o] = (mean0-mean1)*(x+0.5)+mean1; //p1tilde
        local_in[i+2*o] = (mean2-mean0)*(x-0.5)+mean0; //p2tilde
        //p0tilde
        local_in[i+1*o] = (in[i+1*o] - gamma1*local_in[i+0*o] - 
                                       gamma2*local_in[i+2*o])/gamma0;
    }

    fp beta[3];
    beta[0] = compute_beta<o>(&local_in[0]);
    beta[1] = compute_beta<o>(&local_in[o]);
    beta[2] = compute_beta<o>(&local_in[2*o]);

    fp tau = pow2(0.5*(abs(beta[1]-beta[0]) + abs(beta[1]-beta[2])));

    fp omega1 = gamma1*(1.0 + tau/(1e-6 + beta[0]));
    fp omega0 = gamma0*(1.0 + tau/(1e-6 + beta[1]));
    fp omega2 = gamma2*(1.0 + tau/(1e-6 + beta[2]));

    //fp omega1 = gamma1/pow2(1e-6 + beta[0]);
    //fp omega0 = gamma0/pow2(1e-6 + beta[1]);
    //fp omega2 = gamma2/pow2(1e-6 + beta[2]);

    fp isumm = 1./(omega0+omega1+omega2);
    omega0 *= isumm;
    omega1 *= isumm;
    omega2 *= isumm;

    for(int i=0;i<o;++i)
        out[i] = omega1*local_in[i] + omega0*local_in[i+o] + omega2*local_in[i+2*o];

}


template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_sldg_lweno(fp alpha, fp* in1, fp* in2, fp* out){

    //if alpha is small or close to 1, the projection should not differ to 
    //much from one of the input polynomials, and the limiter is not applied
    if(alpha<1e-2 || alpha>1.0-1e-2)
        return;

    constexpr fp gamma0 = 0.998;
    constexpr fp gamma1 = 0.001;
    constexpr fp gamma2 = 0.001;

    fp mean0=0.0;
    fp mean1=0.0;
    fp mean2=0.0;
    for(int i=0;i<o;++i){
        mean0 += out[i]*gauss::get_w(i,o)*0.5;
        mean1 += in1[i]*gauss::get_w(i,o)*0.5;
        mean2 += in2[i]*gauss::get_w(i,o)*0.5;
    }

    for(int i=0;i<o;++i){
      in1[i] = (mean0-mean1)/alpha*
               (gauss::get_x_scaled01(i,o)+alpha-0.5) + mean1;
      in2[i] = (mean2-mean0)/(1.0-alpha)*
               (gauss::get_x_scaled01(i,o)-0.5) + mean0;
      out[i] = 1.0/gamma0*(out[i] - gamma1*in1[i] - gamma2*in2[i]);
    }

    fp beta0 = compute_beta<o>(out);
    fp beta1 = compute_beta<o>(in1);
    fp beta2 = compute_beta<o>(in2);

    fp r0 = gamma0/(pow2(1e-6+beta0));
    fp r1 = gamma1/(pow2(1e-6+beta1));
    fp r2 = gamma2/(pow2(1e-6+beta2));

    fp normalize = 1.0/(r0+r1+r2);

    for(int k=0;k<o;k++)
        out[k] = normalize*r0*out[k] +
                 normalize*r1*in1[k] +
                 normalize*r2*in2[k];
}


template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_sldg_meanparabola(fp alpha, fp* in1, fp* in2, fp* out){
    //if alpha is small or close to 1, the projection should not differ to 
    //much from one of the input polynomials, and the limiter is not applied
    if(alpha<1e-2 || alpha>1.0-1e-2)
        return;

    fp mean1 = 0.0;
    fp mean2 = 0.0;
    fp meanalpha = 0.0;
    for(int i=0;i<o;++i){
        mean1 += in1[i]*gauss::get_w(i,o)*0.5;
        mean2 += in2[i]*gauss::get_w(i,o)*0.5;
        meanalpha += out[i]*gauss::get_w(i,o)*0.5;
    }

    //coefficients computed with Mathematica
    
    fp A[3][3];
    A[0][0] = 1.0/alpha;
    A[0][1] = 1.0/(1.0 - alpha);
    A[0][2] = 1.0/((alpha-1)*alpha);
    A[1][0] = -(2.0+alpha)/alpha;
    A[1][1] = (1.0+alpha)/(alpha-1.0);
    A[1][2] = 2.0/(alpha-alpha*alpha);
    A[2][0] = (1.5+2.0/(3.0*alpha));
    A[2][1] = (1.0+3.0*alpha)/(6.0-6.0*alpha);
    A[2][2] = 2.0/(3.0*(alpha-1.0)*alpha);

    fp a = A[0][0]*mean1 + A[0][1]*mean2 + A[0][2]*meanalpha;
    fp b = A[1][0]*mean1 + A[1][1]*mean2 + A[1][2]*meanalpha;
    fp c = A[2][0]*mean1 + A[2][1]*mean2 + A[2][2]*meanalpha;

    //fp a = 1.0/alpha*mean1 + 1.0/(1.0 - alpha)*mean2 + 
    //       1.0/((alpha-1)*alpha)*meanalpha;
    //fp b = -(2.0+alpha)/alpha*mean1 + (1.0+alpha)/(alpha-1.0)*mean2 + 
    //       2.0/(alpha-alpha*alpha)*meanalpha;
    //fp c = (1.5+2.0/(3.0*alpha))*mean1 + (1.0+3.0*alpha)/(6.0-6.0*alpha)*mean2 +
    //       2.0/(3.0*(alpha-1.0)*alpha)*meanalpha;

    for(int i=0;i<o;++i){
        fp x = alpha + gauss::get_x_scaled01(i,o);
        in1[i] = a*x*x + b*x + c;
    }

    fp beta_parabola = compute_beta<o>(in1);
    fp beta_out = compute_beta<o>(out);

    fp omega_par = 0.0005/pow2(1e-6+beta_parabola);
    fp omega_out = 0.9995/pow2(1e-6+beta_out);

    fp normalize = 1.0/(omega_par+omega_out);

    for(int i=0;i<o;++i)
        out[i] = omega_par*normalize*in1[i] + omega_out*normalize*out[i];

}


#ifdef __CUDACC__
__host__ __device__
#endif
inline void constrained_poly_minimization_o4(fp* in, fp* out, fp lr, fp mean0){

    //finds a polynomial which has the mean mean0 in cell I_0, and where
    //the L2 error with in is minimal on cell I_(1+lr)
    //see paper: Runge-Kutta Discontinuous Galerkin Method with a 
    //           Simple and Compact Hermite WENO Limiter
    //
    //computes out(1:3) = (A'*A)^(-1)*(A'*b) -> minimization
    //         out(0) = f1(out(1:3),mean) -> constraint on mean
    //
    //A = (f1;eye(gw(1:3))), b = \int in, b(0) = b(0)-f2(mean)

    constexpr int o=4;
    fp cc[o]={0.0};
    fp loc_in[o]; //avoid changing input
    for(int i=0;i<o;++i){
        for(int k=0;k<o;++k){
            cc[i] += lagrange_dg_gpu(lr+gauss::get_x_scaled01(k,o),i,o)*
                     gauss::get_w(k,o)*0.5; 
        }
        loc_in[i] = in[i]*gauss::get_w(i,o)*0.5;
    }
    loc_in[0] -= mean0/cc[0]*gauss::get_w(0,o)*0.5;

    fp a = -cc[1]/cc[0]*gauss::get_w(0,o)*0.5;
    fp b = -cc[2]/cc[0]*gauss::get_w(0,o)*0.5;
    fp c = -cc[3]/cc[0]*gauss::get_w(0,o)*0.5;
    fp d2 = gauss::get_w(1,o)*0.5;
    fp d3 = gauss::get_w(2,o)*0.5;
    fp d4 = gauss::get_w(3,o)*0.5;

    fp b1 = a*loc_in[0] + d2*loc_in[1];
    fp b2 = b*loc_in[0] + d3*loc_in[2];
    fp b3 = c*loc_in[0] + d4*loc_in[3];

    //generated by a mathematica script
    fp nom = 1.0/(c*c*d2*d2*d3*d3 + (b*b*d2*d2+(a*a+d2*d2)*d3*d3)*d4*d4);
    fp a11 = ( c*c*d3*d3 + (b*b+d3*d3)*d4*d4 )*nom;
    fp a12 = (-a*b*d4*d4                     )*nom;
    fp a13 = (-a*c*d3*d3                     )*nom;
    fp a22 = ( c*c*d2*d2 + (a*a+d2*d2)*d4*d4 )*nom;
    fp a23 = (-b*c*d2*d2                     )*nom;
    fp a33 = ( b*b*d2*d2 + (a*a+d2*d2)*d3*d3 )*nom;

    loc_in[1] = (a11*b1 + a12*b2 + a13*b3);
    loc_in[2] = (a12*b1 + a22*b2 + a23*b3);
    loc_in[3] = (a13*b1 + a23*b2 + a33*b3);
    loc_in[0] = mean0/cc[0] - loc_in[1]*cc[1]/cc[0]
                            - loc_in[2]*cc[2]/cc[0]
                            - loc_in[3]*cc[3]/cc[0];

    for(int i=0;i<o;++i){
        out[i] = 0.0;
        for(int k=0;k<o;++k){
            out[i] += loc_in[k]*lagrange_dg_gpu(lr+gauss::get_x_scaled01(i,o),k,o);
        }
    }
}
 

template<Index o>
#ifdef __CUDACC__
__host__ __device__
#endif
void modifier_hweno(fp* in, fp* out){

    fp* f0_tilde = &in[o];
    fp mean0=0.0;
    for(int i=0;i<o;++i)
        mean0 += gauss::get_w(i,o)*0.5*f0_tilde[i];

    fp f1_tilde[o];
    constrained_poly_minimization_o4(&in[0], f1_tilde, 1.0, mean0);
    fp f2_tilde[o];
    constrained_poly_minimization_o4(&in[2*o], f2_tilde, -1.0, mean0);
    
    fp beta[3];
    beta[0] = compute_beta<o,true>(f0_tilde);
    beta[1] = compute_beta<o,true>(f1_tilde);
    beta[2] = compute_beta<o,true>(f2_tilde);

    constexpr fp gamma1 = 0.001;
    constexpr fp gamma0 = 0.998;
    constexpr fp gamma2 = 0.001;

    fp omega1 = gamma1/pow2(1e-6 + beta[1]);
    fp omega0 = gamma0/pow2(1e-6 + beta[0]);
    fp omega2 = gamma2/pow2(1e-6 + beta[2]);

    fp isumm = 1./(omega0+omega1+omega2);
    omega0 *= isumm;
    omega1 *= isumm;
    omega2 *= isumm;

    for(Index k=0;k<o;++k)
        out[k] = omega1*f1_tilde[k] + omega0*f0_tilde[k] + omega2*f2_tilde[k];
}



//slow cpu limiter
template<size_t o, size_t d, size_t dim,
         bool (*is_troubled)(fp*),
         void (*modify_troubled_cell)(fp* in, fp* out)>
void weno_limiter_1d_k(fp* in, fp* out, array<Index,2*d> e){

    Index Nx = e[0];
    Index Nv = e[1];
    Index ox = e[0+d];
    Index ov = e[1+d];

    if(o!=e[d+dim]){
        cout << "ERROR: o=" << o << ", e[d+dim]=" << e[d+dim] << endl; 
        exit(1);
    }

    Index N = e[dim];
    Index stride = (dim==0) ? 1 : Nx*ox;
    Index N_other = (dim==0) ? Nv : Nx;
    Index o_other = (dim==0) ? ov : ox; 

    Index dof_v2v3 = 1;
    for(int i=2;i<d;++i)
        dof_v2v3 *= e[i]*e[d+i];

    for(Index iter=0;iter<dof_v2v3;++iter){
        
        in += iter*Nx*Nv*ox*ov;

        #pragma omp parallel for schedule(guided,8)
        for(Index i_other=0;i_other<N_other;++i_other){
            for(Index j_other=0;j_other<o_other;++j_other){

                Index other_lidx = j_other + i_other*o_other;
                Index offset = other_lidx*((dim==0) ? Nx*ox : 1);

                for(Index i=0;i<N;++i){

                    //no data on the outflow cell available


                    array<fp,3*o> local_in;
                    fp* u_0 = &local_in[0*o];
                    fp* u_1 = &local_in[1*o];
                    fp* u_2 = &local_in[2*o];

                    Index im1 = (dim==1) ? ((i-1<0) ? N-1 : i-1 )
                                         : max(0L,i-1);
                    Index ip1 = (dim==1) ? ((i+1==N) ? 0 : i+1 )
                                         : min(i+1,N-1);

                    //in the x-direction, inflow = 0 and outflow is skipped
                    for(size_t j=0;j<o;++j){
                        u_0[j] = in[stride*(j+im1*o) + offset];
                        u_1[j] = in[stride*(j+o*i) + offset];
                        u_2[j] = in[stride*(j+ip1*o) + offset];
                    }
                    if(dim==0 && i==N-1 && 2*other_lidx<N_other*o_other)
                        std::fill_n(u_2,o,0.0);
                    if(dim==0 && i==0 && 2*other_lidx>=N_other*o_other)
                        std::fill_n(u_0,o,0.0);

                    bool not_outflow = true;
                    if(dim==0 && i==N-1 && 2*other_lidx>=N_other*o_other)
                        not_outflow = false;
                    if(dim==0 && i==0 && 2*other_lidx<N_other*o_other)
                        not_outflow = false;

                    array<fp,o> local_out;
                    if(is_troubled(local_in.data()) && not_outflow){
                        //if( (dim==0 && i==N-1 && 2*other_lidx>=N_other*o_other) ||
                        //    (dim==0 && i==0   && 2*other_lidx<N_other*o_other)){
                        //    //do nothing on last outflow since data of a 
                        //    //neighbour is missing
                        //    for(size_t j=0;j<o;++j)
                        //        local_out[j] = u_1[j];
                        //}else
                            modify_troubled_cell(local_in.data(),local_out.data());
                    } else {
                        for(size_t j=0;j<o;++j)
                            local_out[j] = u_1[j];
                    }
                    
                    for(size_t j=0;j<o;++j)
                        out[stride*(j+i*o) + offset] = local_out[j];
                }
            }
        }
    }
}


template<size_t o, size_t d,
         bool (*is_troubled)(fp*),
         void (*modify_troubled_cell)(fp* in, fp* out)>
void weno_limiter_1d_dim(int dim, fp* in, fp* out, 
                         array<Index,2*d> e){

    if(dim==0){
        weno_limiter_1d_k<o,d,0,is_troubled,modify_troubled_cell>(in, out, e);
    }
    else if(dim==1){
        weno_limiter_1d_k<o,d,1,is_troubled,modify_troubled_cell>(in, out, e);
    }
    else{
        cout << "ERROR: WENO limiter for dim>1 not yet implemented!\n";
        exit(1);
    }

}

template<size_t d>
void weno_limiter_1d_k(Index o, int dim, 
                       multi_array<fp,2*d>& in, 
                       multi_array<fp,2*d>& out, 
                       array<Index,2*d> e, 
                       troubled_cell_indicator tci,
                       troubled_cell_modifier tcm){

    if(tci==troubled_cell_indicator::none && tcm==troubled_cell_modifier::none){
        in.swap(out);
        return;
    }
    if(tci==troubled_cell_indicator::none || tcm==troubled_cell_modifier::none){
        in.swap(out);
        cout << "WARNING: both the troubled_cell_indicator (" << tci << ") and "
             << "the troubled_cell_modifier (" << tcm << ") have to be specified "
             << "in order to apply the limiter. Thus currently no limiter is used\n";
        return;
    }

    if(o==1){
        in.swap(out);
        cout << "NOTE: for piecwise constants no limiter required. "
             << "Operation skipped\n";
    } else if(o==2){
        if(tci==troubled_cell_indicator::mean_err && 
           tcm==troubled_cell_modifier::average)
            weno_limiter_1d_dim<2,d,mean_troubled_indicator<1,2,2>,
                                    modifier_average<2>>(dim, in.data(), 
                                                         out.data(), e);
        else{
            cout << "not yet implemented " << endl;
            exit(1);
        }
    } else if(o==3) {
        if(tci==troubled_cell_indicator::mean_err && 
           tcm==troubled_cell_modifier::average)
            weno_limiter_1d_dim<3,d,mean_troubled_indicator<1,2,3>,
                                    modifier_average<3>>(dim, in.data(), 
                                                         out.data(), e);
        else {
            cout << "not yet implemented " << endl;
            exit(1);
        }
    } else if(o==4) {
        if(tci==troubled_cell_indicator::mean_err && 
           tcm==troubled_cell_modifier::average)
            weno_limiter_1d_dim<4,d,mean_troubled_indicator<1,2,4>,
                                    modifier_average<4>>(dim, in.data(), 
                                                         out.data(), e);
        else if(tci==troubled_cell_indicator::minmod && 
                tcm==troubled_cell_modifier::simple_weno)
            weno_limiter_1d_dim<4,d,minmod_troubled_indicator<4>,
                                    modifier_simple_weno<4>>(dim, in.data(), 
                                                             out.data(), e);
        else if(tci==troubled_cell_indicator::mean_err && 
                tcm==troubled_cell_modifier::simple_weno)
            weno_limiter_1d_dim<4,d,mean_troubled_indicator<1,2,4>,
                                    modifier_simple_weno<4>>(dim, in.data(), 
                                                             out.data(), e);
        else if(tci==troubled_cell_indicator::mean_err && 
                tcm==troubled_cell_modifier::linear_weno)
            weno_limiter_1d_dim<4,d,mean_troubled_indicator<1,2,4>,
                                  modifier_linear_weno<1,10,4>>(dim, in.data(), 
                                                                    out.data(), e);
        else if(tci==troubled_cell_indicator::minmod && 
                tcm==troubled_cell_modifier::linear_weno)
            weno_limiter_1d_dim<4,d,minmod_troubled_indicator<4>,
                                  modifier_linear_weno<1,10,4>>(dim, in.data(), out.data(), e);
        else if(tci==troubled_cell_indicator::mean_err && 
                tcm==troubled_cell_modifier::hweno)
            weno_limiter_1d_dim<4,d,mean_troubled_indicator<1,2,4>,
                                  modifier_hweno<4>>(dim, in.data(), out.data(), e);
        else if(tci==troubled_cell_indicator::minmod && 
                tcm==troubled_cell_modifier::hweno)
            weno_limiter_1d_dim<4,d,minmod_troubled_indicator<4>,
                                  modifier_hweno<4>>(dim, in.data(), out.data(), e);
 
        else{
            cout << "not yet implemented " << endl;
            exit(1);
        }
    } else {
        cout << "ERROR: not yet implemented for o>4." << endl;
        exit(1);
    }

}


template<typename T, size_t d=2>
void visualize_indicator2d_x(fp time, multi_array<fp,2*d>& in, 
                                           array<Index,2*d> e, 
                                           array<fp,2> a, array<fp,2> b, 
                                           T indicator){
    //should I use multi_array_view here..?


    for(size_t i=0;i<d;++i)
        if(e[d+i]>4){
            cout << "ERROR: visualize indicator currently only allows o<=4.\n";
            exit(1);
        }

    if(e[d]!=e[d+1]){
        cout << "ERROR: currently order has to be the same in every direction.\n";
        exit(1);
    }
    fp hx = (b[0]-a[0])/fp(e[0]);
    fp hv = (b[1]-a[1])/fp(e[1]);

    indicator(in.data());

    Index o = e[d];
    Index Nx = e[0];
    Index Nv = e[1];
    std::ofstream output("visualize_indicator_x"+std::to_string(time)+".data");
    for(Index j=0;j<Nv;++j){
        for(Index k=0;k<o;++k){
            for(Index i=0;i<Nx;++i){

                int offset = i*o + Nx*o*(k+o*j);
                array<fp,4> loc_data;
                fp mean=0.0;
                for(int l=0;l<o;++l){
                    loc_data[l] = in.v[offset + l];
                    mean += 0.5*gauss::get_w(l,o)*loc_data[l];
                }

                //bool b = indicator(loc_data.data());

                //array<fp,4> lag_0i = {1.526788125457267,
                //                     -8.136324494869278e-01,
                //                      4.007615203116507e-01,
                //                     -1.139171962819900e-01};
                //fp f_left = 0.0;
                //fp f_right = 0.0;
                //for(int l=0;l<o;++l){
                //    f_left += loc_data[l]*lag_0i[o-1-l];
                //    f_right += loc_data[o+l]*lag_0i[l];
                //}
                //fp jump = f_left-f_right;
                //bool b = (jump>1e-2) ? true : false;
                //

                for(int l=0;l<o;++l)
                    loc_data[l] /= mean;

                fp smoothness_indicator = compute_beta<4>(loc_data.data());
                bool b = false;
                if(smoothness_indicator>100){
                    b = true;
                }

                fp x = a[0] + hx*(i+0.5);
                fp v = a[1] + hv*(j+gauss::x_scaled01(k,o));

                output << x << " " << v << " " << b << " " 
                       << smoothness_indicator << " " 
                       << smoothness_indicator*mean << " " 
                       << mean << endl;
                if(i==Nx-2)
                    output << endl;
            }
        }
    }
}
