#pragma once

#include <generic/common.hpp>
#include <algorithms/dg.hpp>


template<size_t dx, size_t dv>
fp check_max_abs_v(Index idx, Index dof_x, multi_array<fp,2*(dx+dv)>& f_in){

    Index offset = dof_x*idx;

    //cout << "offset: " << offset << endl;

    fp maxabs = 0.0;
    for(Index k=0;k<dof_x;++k){
        maxabs = std::max(maxabs,abs(f_in.v[offset + k]));
    }

    //cout << "maxabs in check max: " << maxabs << endl;
    return maxabs;
}


template<size_t dx, size_t dv>
void interpolate_on_finer_grid(array<fp,dv> av_old, array<fp,dv> bv_old, 
                               array<fp,dv> av_new, array<fp,dv> bv_new, 
                               array<Index,2*(dx+dv)>& e,
                               multi_array<fp,2*(dx+dv)>& f_in, 
                               multi_array<fp,2*(dx+dv)>& f_out){

    constexpr size_t d = dx+dv;

    if(dx!=1 || dv!=1){
        cout << "error\n";
        exit(1);
    }

    array<fp,dv> h_old;
    array<fp,dv> h_new;
    for(size_t i=0;i<dv;++i){
        h_old[i] = (bv_old[i] - av_old[i])/fp(e[dx+i]);
        h_new[i] = (bv_new[i] - av_new[i])/fp(e[dx+i]);
    }

    dg_evaluator lagrange_interpolate(e[d+dx]);

    Index dof_x=1.0;
    for(size_t i=0;i<dx;++i)
        dof_x *= e[i]*e[d];

    Index Nx = e[0];
    Index Nv = e[dx];
    Index ox = e[d];
    Index ov = e[d+dx];

    //cout << "avold: " << av_old[0] << ", h_old: " << h_old[0] 
    //     << ", h_new: " << h_new[0] << endl;

    timer t_adjust;

    t_adjust.start();

    array<Index,2*dv> ev = {e[dx],e[d+dx]};

    for(Index iv=0;iv<Nv;++iv){
        for(Index jv=0;jv<ov;++jv){

            fp v_new = av_new[0] + h_new[0]*( iv + gauss::x_scaled01(jv,ov));

            Index i_old = (v_new - av_old[0])/h_old[0];

            fp values[ ov ];

            fp v_new_scaled = (v_new - (av_old[0] + i_old*h_old[0]))/h_old[0];

            for(Index ix=0;ix<Nx;++ix){
                for(Index jx=0;jx<ox;++jx){

                    for(Index k=0;k<ov;++k){
                        values[k] = f_in.v[(jx+ox*ix) + i_old*dof_x*ov + k*dof_x];
                    }

                    fp new_v = lagrange_interpolate.eval(v_new_scaled, values);
                    f_out({ix,iv,jx,jv}) =  new_v;

                }
            }

        }
    }


    t_adjust.stop();

    cout << "t_adjust: " << t_adjust.total() << endl;

}




template<size_t dx, size_t dv>
void project_on_finer_grid(array<fp,dv> av_old, array<fp,dv> bv_old, 
                           array<fp,dv> av_new, array<fp,dv> bv_new, 
                           array<Index,2*(dx+dv)> e, 
                           fp reduced_cell,
                           multi_array<fp,2*(dx+dv)>& f_in, 
                           multi_array<fp,2*(dx+dv)>& f_out){

    constexpr size_t d = dx+dv;

    if(dx!=1 || dv!=1){
        cout << "error: currently only dx=1 and dv=1 allowed\n";
        exit(1);
    }

    assert(reduced_cell>0.0);

    array<fp,dv> h_old;
    array<fp,dv> h_new;
    for(size_t i=0;i<dv;++i){
        h_old[i] = (bv_old[i] - av_old[i])/fp(e[dx+i]);
        h_new[i] = (bv_new[i] - av_new[i])/fp(e[dx+i]);
    }

    dg_evaluator lagrange_interpolate(e[d+dx]);

    Index Nx = e[0];
    Index Nv = e[dx];
    Index ox = e[d];
    Index ov = e[d+dx];

    timer t_adjust;

    t_adjust.start();

    Index dof_x = Nx*ox;

    for(Index iv=0;iv<Nv;++iv){

        fp vl = av_new[0] + iv*h_new[0];
        fp vr = av_new[0] + (iv+1)*h_new[0];

        int i_old1 = ((vl - av_old[0])*(1.0+1e-13))/h_old[0];
        int i_old2 = ((vr - av_old[0])*(1.0-1e-13))/h_old[0];
        
        fp vl_scaled = (vl - (av_old[0] + i_old1*h_old[0]))/h_old[0];
        fp vr_scaled = (vr - (av_old[0] + i_old2*h_old[0]))/h_old[0];
        
        fp a1 = vl_scaled;
        fp b1 = (i_old1 != i_old2) ? 1.0 : vr_scaled;

        fp a2 = 0.0;
        fp b2 = (i_old1 == i_old2) ? 0.0 : vr_scaled;

        fp A[ov][ov];
        fp B[ov][ov];

        fp hl = b1-a1;
        fp hr = b2-a2;

        //cout << "\niv: " << iv << endl;
        //cout << "vl: " << vl << ", vr: " << vr 
        //     << ", i_old1: " << i_old1 << ", i_old2: " << i_old2 
        //     << ", vl_sc: " << vl_scaled << ", vr_sc: " << vr_scaled << "\n"
        //     << "a1: " << a1 << ", b1: " << b1 << ", a2: " << a2 << ", b2: "
        //     << b2 << ", hl: " << hl << ", hr: " << hr << endl;

        for(int i=0;i<ov;++i){
            for(int j=0;j<ov;++j){
                A[i][j] = 0.0;
                B[i][j] = 0.0;
                for(int k=0;k<ov;++k){

                    fp xl_k = a1 + hl*gauss::x_scaled01(k,ov);
                    A[i][j] += lagrange(xl_k,j,ov)*
                               lagrange((xl_k-a1)/reduced_cell,i,ov)*
                               gauss::w(k,ov)*hl*0.5;

                    if(i_old1!=i_old2){
                        fp xr_k = hr*gauss::x_scaled01(k,ov);
                        B[i][j] += lagrange(xr_k,j,ov)*
                                   lagrange((xr_k+(1.0-a1))/reduced_cell,i,ov)*
                                   gauss::w(k,ov)*hr*0.5;
                    }

                }
            }
        }

        //if(iv==0){
        //    cout << "A cpu: " << endl;
        //    for(int i=0;i<ov;++i){
        //        for(int j=0;j<ov;++j){
        //            cout << A[i][j] << " ";
        //        }
        //        cout << endl;
        //    }
        //    cout << "B cpu: " << endl;
        //    for(int i=0;i<ov;++i){
        //        for(int j=0;j<ov;++j){
        //            cout << B[i][j] << " ";
        //        }
        //        cout << endl;
        //    }
        //}
 
        for(Index ix=0;ix<Nx;++ix){
            for(Index jx=0;jx<ox;++jx){

                fp in[2*ov];
                for(int i=0;i<2*ov;++i)
                    in[i] = f_in.v[(i+i_old1*ov)*dof_x + (jx+ox*ix)];

                fp out[ov];
                fp mean = 0.0;
                fp min_out = 1e+5;
                for(int i=0;i<ov;++i){
                    out[i] = 0.0;
                    for(int j=0;j<ov;++j){
                        out[i] += A[i][j]*in[j] + B[i][j]*in[ov+j];
                    }
                    out[i] *= 2.0/(gauss::w(i,ov)*reduced_cell);
                    mean += 0.5*out[i]*gauss::w(i,ov);
                    min_out= min(min_out,out[i]);
                }

                //fp theta = min(abs(mean/(min_out-mean)),fp(1.0));

                for(int i=0;i<ov;++i)
                    f_out.v[(i+iv*ov)*dof_x + (jx+ox*ix)] = out[i];

                    //f_out.v[(i+iv*ov)*dof_x + (jx+ox*ix)] = theta*(out[i]-mean)+mean;
            }
        }
    }


    t_adjust.stop();

    cout << "t_adjust: " << t_adjust.total() << endl;

}

