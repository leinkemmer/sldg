#pragma once

#include <generic/common.hpp>
#include <container/domains-mixed.hpp>

struct cubicspline_1d_periodic {
    gsl_interp_accel *acc;
    gsl_spline* spline;
    vector<fp> x,y;
    int n;

    cubicspline_1d_periodic(int _n) : n(_n) {
        acc = gsl_interp_accel_alloc();
        spline = gsl_spline_alloc(gsl_interp_cspline_periodic,n+1);
        x.resize(n+1);
        y.resize(n+1);
    }

    ~cubicspline_1d_periodic() {
        gsl_spline_free(spline);
        gsl_interp_accel_free(acc);
    }

    template<typename ArrayX, typename ArrayY>
        void construct(ArrayX itx, ArrayY ity) {
            for(int i=0;i<n;i++) {
                x[i] = itx[i];
                y[i] = ity[i];
            }
            x[n] = itx[n];
            y[n] = ity[0];

            gsl_spline_init(spline, &x[0], &y[0],x.size());
        }

    fp operator()(fp x) {
        if(x < this->x[0])
            x += this->x[n]-this->x[0];
        return gsl_spline_eval(spline,x,acc);
    }
};

struct dg_x_array {
    int o;

    dg_x_array(int _o) {
        o = _o;
    }

    fp operator[](int i) {
        int i_cell = i/o;
        int i_quad = i%o;
        return i_cell + gauss::x_scaled01(i_quad,o);
    }
};

void to_equi(domain2d_dG& in, domain2d_equi& out) {
    cubicspline_1d_periodic spline(in.nx*in.ox);

    for(int j=0;j<in.Ny;j++) {
        dg_x_array x_it(in.ox);
        spline.construct(x_it,&in.data[j][0][0]);

        fp deltax = 1.0/fp(in.ox);
        for(int i=0;i<in.nx*in.ox;i++) {
            out(i, j) = spline(deltax*i);
        }
    }
}


void to_equi_directeval(domain2d_dG& in, domain2d_equi& out) {

    for(int j=0;j<in.Ny;j++) {
        fp deltax = 1.0/fp(in.ox);
        for(int i=0;i<in.nx;i++) {
            for(int o=0;o<in.ox;o++) {
                fp x = o*deltax;
                out(i*in.ox + o, j) = evaluate1d(x, in.ox, &in.data[j][i][0]);
            }
        }
    }
}


struct equi_x_array {
    fp deltax;

    equi_x_array(int o) {
        deltax = 1.0/fp(o);
    }

    fp operator[](int i) {
        return deltax*i;
    }
};

void to_dG(domain2d_equi& in, domain2d_dG& out) {
    cubicspline_1d_periodic spline(in.Nx);

    for(int j=0;j<in.Ny;j++) {
        equi_x_array x_it(out.ox);
        spline.construct(x_it,&in(0,j));

        for(int i=0;i<in.nx;i++)
            for(int o=0;o<out.ox;o++) {
                fp x = fp(i) + gauss::x_scaled01(o,out.ox);
                out.data[j][i][o] = spline(x);
            }
    }
}


void to_dG_lagrange(domain2d_equi& in, domain2d_dG& out, int order) {
    int ox = out.ox;

    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_interp *workspace = gsl_interp_alloc(gsl_interp_polynomial, order);

    int start = -(order-1)/2;

    fp deltax = 1.0/fp(ox);
    vector<fp> _x(order), _y(order);
    for(int k=0;k<order;k++)
        _x[k] = (start + k)*deltax;

    for(int j=0;j<in.Ny;j++) {
        for(int i=0;i<in.nx;i++)
            for(int o=0;o<ox;o++) {
                int idx = i*ox + o;

                for (int k=0;k<order;k++)
                    _y[k] = in(neg_modulo(idx+start+k,in.Nx),j);
                gsl_interp_init(workspace, &_x[0], &_y[0], order);

                fp x = gauss::x_scaled01(o,ox) - o*deltax;
                out.data[j][i][o] = gsl_interp_eval(workspace, &_x[0], &_y[0],
                        x, acc);
            }
    }

    gsl_interp_free(workspace);
    gsl_interp_accel_free(acc);
}

void populate_x_lagrange(int i, int o, int ox, int order, int start,
        vector<fp>& _x) {

    for(int k=0;k<order;k++) {
        int offset = o + start + k;
        int cell_offset, new_o;
        if(offset >= 0) {
            cell_offset = offset/ox;
            new_o = offset%ox;
        } else {
            cell_offset = (offset+1)/ox - 1;
            new_o = -cell_offset*ox + offset;
        }
        _x[k] = fp(cell_offset) + gauss::x_scaled01(new_o,ox) 
                - gauss::x_scaled01(o,ox);
    }
}

void to_equi_lagrange(domain2d_dG& in, domain2d_equi& out, int order) {
    int ox = out.ox;
    int Nx = out.Nx;

    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_interp *workspace = gsl_interp_alloc(gsl_interp_polynomial, order);

    vector<fp> _x(order), _y(order);

    fp deltax = 1.0/fp(ox);
    for(int j=0;j<in.Ny;j++) {
        for(int i=0;i<in.nx;i++)
            for(int o=0;o<ox;o++) {

                int start = -(order-1)/2;
                populate_x_lagrange(i, o, ox, order, start, _x);

                fp* ptr = &in.data[j][0][0];
                for (int k=0;k<order;k++)
                    _y[k] = ptr[neg_modulo(i*ox + o + start + k,Nx)];

                gsl_interp_init(workspace, &_x[0], &_y[0], order);

                fp x = o*deltax - gauss::x_scaled01(o,ox);
                int idx = i*ox + o;
                out(idx, j) = gsl_interp_eval(workspace, &_x[0], &_y[0], x, acc);
            }
    }

    gsl_interp_free(workspace);
    gsl_interp_accel_free(acc);
}

