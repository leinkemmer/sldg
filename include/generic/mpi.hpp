#pragma once

#include <mpi.h>

#include <iostream>

#include "utility.hpp"

#ifdef USE_SINGLE_PRECISION
#define MPI_FP MPI_FLOAT
#else
#define MPI_FP MPI_DOUBLE
#endif


void mpi_init(bool multithreading=false) {
    int provided;
    if(multithreading==false)
        MPI_Init(NULL, NULL);
    else
        MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);

    if(provided != MPI_THREAD_MULTIPLE) {
        cout << "WARNING: requested MPI_THREAD_MULTIPLE but only " << provided
             << " is available" << endl;
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank==0) {
        if(multithreading==false)
            cout << "Multithreading for MPI is disabled" << endl;
        else
            cout << "Provided support for multithreaded MPI: " << provided 
                 << endl;
    }
}

void mpi_destroy() {
    MPI_Finalize();
}


/// This class manages the distribution of the computational domain to different
/// MPI processes. That includes the start and extent of the physical domain
/// stored on an MPI process as well as providing a convenient translation from
/// multi dimensional indices to the underlying MPI rank.
template<Index d>
struct mpi_process {

    int rank, rank_v, rank_x;
    int total_nodes;
    array<Index,d> node_id;
    array<Index,d> num_nodes;
    array<Index,d> left_rank;
    array<Index,d> right_rank;

    MPI_Comm MPI_COMM_V, MPI_COMM_X;

    mpi_process() {
        MPI_Comm_size(MPI_COMM_WORLD, &total_nodes);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    }

    /// Based on the number of nodes in each dimension this function initializes
    /// the multi dimensional index of the current MPI process and determines
    /// the neighboring MPI processes.
    void init(array<Index,d> _num_nodes, Index dx = 0, Index dv = 0) {

        num_nodes = _num_nodes;
        if(prod(num_nodes)!=total_nodes) {
            cout << "ERROR: number of MPI processes does not match number of "
                 << "nodes specified (num_nodes=" << num_nodes << "  num_mpi_processes="
                 << total_nodes << ")" << endl;
            exit(1);
        }
        int _rank=rank;
        for(Index i=0;i<d;i++) {
            node_id[i]=_rank%num_nodes[i];
            _rank/=num_nodes[i];
        }
        for(Index i=0;i<d;i++) {
            {
                array<Index,d> id = node_id;
                node_id[i]--;
                left_rank[i]  = node_id_to_rank(id);
            }
            {
                array<Index,d> id = node_id;
                node_id[i]++;
                right_rank[i] = node_id_to_rank(id);
            }
        }

        //Communicator that collects all processes with the same 
        //x-Domain, used for the reduction of rho
        vector<Index> idx_x(dx), nodes_x(dx);        
        for(Index i = 0; i < dx; ++i){
            idx_x[i] = node_id[i];
            nodes_x[i] = num_nodes[i];
        }
        Index x_id = linearize_idx(idx_x, nodes_x, dx);
        MPI_Comm_split(MPI_COMM_WORLD, x_id, rank, &MPI_COMM_V);

        MPI_Comm_rank(MPI_COMM_V, &rank_v);

        //Communicator that collects all processes with the same
        //v-Domain, used in poisson_solver in CG to calculate 
        //the inner product (reduction over the whole x domain)
        vector<Index> idx_v(dv), nodes_v(dv);        
        for(Index i = 0; i < dv; ++i){
            idx_v[i] = node_id[dx+i];
            nodes_v[i] = num_nodes[dx+i];
        }
        Index v_id = linearize_idx(idx_v, nodes_v, dv);
        MPI_Comm_split(MPI_COMM_WORLD, v_id, rank, &MPI_COMM_X);

        MPI_Comm_rank(MPI_COMM_X, &rank_x);
    }

    Index node_id_to_rank(array<Index,d> id) {
        Index rank = 0; Index multiplier=1;
        for(Index i=0;i<d;i++) {
            rank+=id[i]*multiplier; multiplier*=num_nodes[i];
        }
        return rank;
    }

    Index rank_left(Index i) {
        array<Index,d> tmp = node_id;
        tmp[i]--;
        if(tmp[i]<0)
            tmp[i]=num_nodes[i]-1;
        return node_id_to_rank(tmp);
    }

    Index rank_right(Index i) {
        array<Index,d> tmp = node_id;
        tmp[i]++;
        if(tmp[i]>=num_nodes[i])
            tmp[i]=0;
        return node_id_to_rank(tmp);
    }
    
    /// This functions ensures that the strings s (which is different on each
    /// MPI process, in general) are not mangled by the MPI implementation.
    void print(string s,MPI_Comm comm=MPI_COMM_WORLD,int num_nodes=-1) {
        if(num_nodes==-1)
            num_nodes=total_nodes;
        MPI_Barrier(comm);
        if (rank == 0) {
            cout << s << endl;
            for(int i=1;i<num_nodes;i++) {
                MPI_Status status; int len;
                MPI_Probe(i, 0, comm, &status);
                MPI_Get_count(&status, MPI_CHAR, &len);
                string msg(len, ' ');
                MPI_Recv(&msg[0], len, MPI_CHAR, i, 0, comm, &status);
                cout << msg << endl;
            }
        } else {
            MPI_Send(&s[0], s.size(), MPI_CHAR, 0, 0, comm);
        }
        MPI_Barrier(comm);
    }

private:
    Index linearize_idx(vector<Index> idx, vector<Index> e, size_t dd){
        Index k=0;
        Index stride = 1;
        for(size_t i=0;i<dd;i++) {
            k += stride*idx[i];
            stride *= e[i];
        }
        return k;
    }
};

