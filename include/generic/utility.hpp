#pragma once

#include <iostream>
#include <sstream>
#include <array>
using std::array;

#include <generic/common.hpp>


/// We overload the << operator for boost::array. This allows us to print
/// boost::array using the standard cout syntax.
template<class T, size_t d>
std::ostream& operator<<(std::ostream& os, const array<T,d>& a) {
    os << "("; 
    for(size_t i=0;i<d;i++)
        os << a[i] << (i==d-1 ? "" : ",");
    os <<  ")";
    return os;
}

/// In C++ the modulo % operator for negative integers yields a negative value.
/// Thus, the modulo operator can not be used to compute array indices. The
/// neg_modulo function handles this case separately and ensures that for
/// -n <= x <= n we obtain the correct array index (for periodic boundary
/// conditions)
int neg_modulo(int i, int n) {
    int a = i % n;
    if(a < 0)
        a += n;
    return a;
}

/// In this namespace functions pertaining to retrieve the Gauss-Legendre nodes
/// and the corresponding quadrature weights are collected. All these values are
/// precomputed and the corresponding functions call thus only require a single
/// load from memory.
namespace gauss {
    /// Retrieve the i-th Gauss-Legendre node for the polynomial of degree o-1
    /// defined in the interval [-1,1].
    fp x(int i, int o);
    /// Retrieve the i-th Gauss-Legendre node for the polynomial of degree o-1
    /// defined in the interval [0,1].
    fp x_scaled01(int i, int o);
    /// Retrieve the i-th Gauss-Legendre weight for the polynomials of degree
    /// o-1.
    fp w(int i, int o);

    vector<fp> all_weights(int o) {
        vector<fp> ws(o);
        for(int i=0;i<o;i++)
            ws[i] = w(i,o);
        return ws;
    }
    
    vector<fp> all_x_scaled01(int o) {
        vector<fp> xs(o);
        for(int i=0;i<o;i++)
            xs[i] = x_scaled01(i,o);
        return xs;
    }
}


template<typename T>
string to_str(T v) {
    std::stringstream ss;
    ss << v;
    return ss.str();
}

/// This function facilitates the implementation of dimension independent
/// loops. The variable idx is the loop counter. To control the iteration max
/// (the upper bound of the loop indices) and start (the reset value) are
/// specified.
///
/// Example: A simple example in two dimensions which iterates over the values
/// idx=(2,2),(2,3),(3,2),(3,3).
/// array<int,2> idx = {2,2};
/// array<int,2> max = {4,4};
/// array<int,2> start = {2,2};
/// do {
///     a(idx) = ...;
/// } while(iter_next(idx,max,start);
template<size_t n>
bool iter_next(array<Index,n> &idx, array<Index,n> max,
        array<Index,n>* start = NULL) {
    size_t j;
    for(j=0;j<n;j++) {
        idx[j]++;
        if(idx[j] < max[j])
            break;
        idx[j] = start == NULL ? 0 : (*start)[j];
    }
    return j < n;
}

template<size_t n>
bool iter_next_memorder(array<Index,n> &idx, array<Index,n> max,
        array<Index,n>* start=NULL) {
    static_assert(n % 2 == 0, "iter_next_memorder only works for indices with "
            "a size divisible by two.");

    size_t j;
    for(j=0;j<n/2;j++) {
        // first do the index corresponding to o
        idx[n/2+j]++;
        if(idx[n/2+j] < max[n/2+j])
            break;
        idx[n/2+j] = start == NULL ? 0 : (*start)[n/2+j];

        // then do index corresponding to N
        idx[j]++;
        if(idx[j] < max[j])
            break;
        idx[j] = start == NULL ? 0 : (*start)[j];
    }

    return j < n/2;
}

template<size_t d>
array<Index,2*d> to_memorder(array<Index,2*d> in) {
    array<Index,2*d> out;
    for(size_t i=0;i<d;i++) {
        out[2*i]   = in[d+i];
        out[2*i+1] = in[i];
    }
    return out;
}

template<size_t d>
array<Index,2*d> from_memorder(array<Index,2*d> in) {
    array<Index,2*d> out;
    for(size_t i=0;i<d;i++) {
        out[d+i] = in[2*i];
        out[i]   = in[2*i+1];
    }
    return out;
}

// product of an array of numbers
template<class T, size_t d>
T prod(array<T,d> a) {
    T p=1;
    for(size_t i=0;i<d;i++)
        p*=a[i];
    return p;
}

template<class T, size_t d>
T sum(array<T,d> a, size_t up=d) {
    T p=T(0);
    for(size_t i=0;i<up;i++)
        p+=a[i];
    return p;
}

/// This function distributes the number of cores available to the d dimensions
/// of the problem. At the moment we assume that the number of cores can be
/// written as 2^k or as k^d for some integer k.
template<size_t d, class Number>
void distribute(Number total_cores, array<Number,d>& distribution) {
    // try to decompose as n^d
    Number root=round(pow(total_cores,1./fp(d)));
    for(size_t i=0;i<d;i++)
        distribution[i]=root;
    if(prod(distribution)==total_cores)
        return;
    // if that fails we try to decompose as 2^n
    Number remaining_cores=total_cores;
    for(size_t i=0;i<d;i++) {
        distribution[i]=
            pow(2,Index(round(log(fp(remaining_cores))/log(2)))/(d-i));
        remaining_cores/=distribution[i];
    }
    if(prod(distribution)!=total_cores) {
        cout << "ERROR: please specify a number of nodes which can be "
             << "decomposed as 2^n or n^d." << endl;
        exit(1);
    }
}

template<size_t d>
void global_ab_to_local_ab(array<fp,d>& a, array<fp,d>& b,
        array<Index,d> node_id, array<Index,d> num_nodes) {
    array<fp, d> aa = a;
    array<fp, d> bb = b;
    for (size_t i=0;i<d;i++) {
        a[i] = aa[i] + node_id[i]*(bb[i]-aa[i])/fp(num_nodes[i]);
        b[i] = aa[i] + (node_id[i]+1)*(bb[i]-aa[i])/fp(num_nodes[i]);
    }
}

/// This functions returns a file name (based on name) that is different for
/// each MPI process and reflects the d-dimensional index of the slice of data
/// the process is working on.
template<size_t d>
string filename(string name, array<Index,d> id) {
    std::stringstream ss; ss << name << "-";
    for(size_t i=0;i<d;i++) ss << id[i] << ((i!=d-1)?",":".");
    ss << "data";
    return ss.str();
}

template<Index d>
array<Index,2*d> unslice(Index dim, const array<Index,2*d-2>& in, Index k,
        Index o) {
    array<Index,2*d> out;
    Index j=0;
    for(Index i=0;i<d;i++) {
        if(i!=dim) {
            out[i]=in[j];
            out[d+i]=in[d-1+j];
            j++;
        }
    }
    out[dim]   = k;
    out[d+dim] = o;
    return out;
}

/// This function computed the linear offset given a d-dimensional index.
template<class T, size_t d>
T offset(array<T,d>& idx, array<T,d>& extension) {
    T global_offset=0; T prod=1;
    for(size_t i=0;i<d;i++) {
        global_offset+=idx[i]*prod;
        prod*=extension[i];
    }
    return global_offset;
}



/// This function evaluates the i-th Lagrange polynomial of degree o-1 at the
/// point x.
fp lagrange(fp x, int j, int o) {
    fp prod=1.0;
    for(int i=0;i<j;i++)   prod*=(x-gauss::x_scaled01(i,o))
        /(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
    for(int i=j+1;i<o;i++) prod*=(x-gauss::x_scaled01(i,o))
        /(gauss::x_scaled01(j,o)-gauss::x_scaled01(i,o));
    return prod;
}

/// Evaluate the function specified by the coefficients given in the Legendre
/// basis up to polynomials of degree o-1.
template<typename iterator>
fp evaluate1d(fp x, int o, iterator val) {
    fp r=0.0;
    for(int j=0;j<o;j++)
        r+=val[j]*lagrange(x,j,o);
    return r;
}

fp sech(fp x) {
    return 1/cosh(x);
}


template<int d>
struct range {
    array<Index,d> upper_limit;
    array<Index,d> current;

    range(array<Index, d> _upper_limit) : upper_limit(_upper_limit) {
        fill(current.begin(), current.end(), 0);
    }

    range(std::initializer_list<Index> il) {
        if(il.size() != d) {
            cout << "ERROR: initializer list in ctor of range must have "
                 << "exactly d elements" << endl;
            exit(1);
        }
        std::copy(il.begin(), il.end(), upper_limit.begin());
        fill(current.begin(), current.end(), 0);
    }

    template<typename... Ints> 
    range(Ints&&... ul) {
        static_assert(sizeof...(Ints) == d,
                "wrong number of arguments to range ctor.");
        upper_limit = array<Index,d>({ul...});
        fill(current.begin(), current.end(), 0);
    }

    range& operator++() {
        for(Index k=0;k<d-1;k++) {
            current[k]++;
            if(current[k] < upper_limit[k])
                return *this;
            current[k]=0;
        }
        current[d-1]++;
        return *this;
    }

    range begin() {
        return range(upper_limit);
    }

    range end() {
        range ut(upper_limit);
        fill(ut.current.begin(), ut.current.end(), 0);
        ut.current[d-1] = upper_limit[d-1];
        return ut;
    }

    array<Index,d>& operator*() {
        return current;
    }

    bool operator!=(const range& b) {
        bool ret = true;
        for(Index k=0;k<d;k++)
            ret = ret && (current[k] == b.current[k]);
        return !ret;
    }
};

template<typename T, size_t d>
T error_inf(const array<T,d>& a, const array<T,d>& b) {
    T m = 0.0;
    for(size_t i=0;i<d;i++) {
        T defect = a[i]-b[i];
        m = max(m,(defect>0) ? defect : -defect);
    }
    return m;
}


template<Index d>
array<Index,d> dof(array<Index,2*d> a) {
    array<Index,d> out;
    for(int i=0;i<d;i++)
        out[i] = a[i]*a[d+i];
    return out;
}

template<size_t dx, size_t dv>
array<Index, dx+dv> dof(index_dxdv_no<dx,dv> no) {
    return dof<dx+dv>(no.get_array());
}


template<typename T>
T reverse(T a) {
    std::reverse(begin(a), end(a));
    return a;
}

// slice removes the entry dim from an array
template<size_t d>
array<Index,d-1> slice(size_t dim, array<Index,d> in) {
    array<Index,d-1> out;
    int j=0;
    for(size_t i=0;i<d;i++) {
        if(i!=dim) {
            out[j]=in[i];
            j++;
        }
    }
    return out;
}

// Compute a linear index from index idx and size e
template<size_t d>
Index linearize_idx(array<Index,d> idx, array<Index,d> e) {
    Index k=0;
    Index stride = 1;
    for(size_t i=0;i<d;i++) {
        k += stride*idx[i];
        stride *= e[i];
    }
    return k;
}

// parser for certain command line arguments
template<size_t d>
array<Index,d> parse(string s) {
    array<Index,d> out;
    std::istringstream iss(s);
    for(size_t i=0;i<d;i++) {
        if(iss.eof()) {
            cout << "ERROR: not enough dof provided to parse" << endl;
            exit(1);
        }
        iss >> out[i];
    }

    if(!iss.eof()) {
        cout << "ERROR: to many dof provided to parse" << endl;
        exit(1);
    }
    return out;
}


#ifdef __CUDACC__

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line,
        bool abort=true)
{
    if (code != cudaSuccess) 
    {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code),
                file, line);
        if (abort) exit(code);
    }
}

void* gpu_malloc(size_t size) {
    cout << "gpu_malloc: " << size/1e9 << " GB" << endl;
    void *p;
    if(cudaMalloc(&p, size) == cudaErrorMemoryAllocation) {
        cout << "ERROR: allocating " << size/1e9 << " GB on the gpu failed"
             << endl;
        exit(1);
    }
    return p;
}


namespace gauss{

    __constant__ fp d_gauss_x_1[1]={0};
    __constant__ fp d_gauss_x_2[2]={-0.5773502691896258,0.5773502691896258};
    __constant__ fp d_gauss_x_3[3]={-0.7745966692414834,0,0.7745966692414834};
    __constant__ fp d_gauss_x_4[4]={-0.8611363115940526,-0.3399810435848563,
                                     0.3399810435848563,0.8611363115940526};
    __constant__ fp d_gauss_x_5[5]={-0.9061798459386640,-0.5384693101056831,0,
                                     0.5384693101056831,0.9061798459386640};
    __constant__ fp d_gauss_x_6[6]={-0.9324695142031520,-0.6612093864662645,
                                    -0.2386191860831969,0.2386191860831969,
                                     0.6612093864662645,0.9324695142031520};

    __constant__ fp* d_gauss_x[6] = {d_gauss_x_1, d_gauss_x_2, d_gauss_x_3, 
                                     d_gauss_x_4, d_gauss_x_5, d_gauss_x_6};

    __constant__ fp d_gauss_w_1[1]={2.000000000000000};
    __constant__ fp d_gauss_w_2[2]={1.000000000000000,1.000000000000000};
    __constant__ fp d_gauss_w_3[3]={0.555555555555555,0.8888888888888889,
                                    0.555555555555555};
    __constant__ fp d_gauss_w_4[4]={0.347854845137454,0.652145154862546,
                                    0.652145154862546,0.347854845137454};
    __constant__ fp d_gauss_w_5[5]={0.23692688505619,0.47862867049937,
                                    0.5688888888888889,0.47862867049937,
                                    0.23692688505619};
    __constant__ fp d_gauss_w_6[6]={0.17132449237917,0.36076157304814,
                                    0.467913934572691,0.467913934572691,
                                    0.36076157304814,0.17132449237917};

    __constant__ fp* d_gauss_w[6] = {d_gauss_w_1, d_gauss_w_2, d_gauss_w_3, 
                                     d_gauss_w_4, d_gauss_w_5, d_gauss_w_6};

    __device__
    fp x_scaled01_gpu(int i, int o){
        return 0.5*(d_gauss_x[o-1][i]+1.0);
    }

    __device__
    fp w_gpu(int i, int o){
        return d_gauss_w[o-1][i];
    }

}
#endif



//This is probably only working as long as there is only one compilation unit
//If the code is split up (in future if compilation times become unbearable), 
//the flag -dc is required
namespace gauss{

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    fp get_x_scaled01(int i, int o){
        #ifdef __CUDA_ARCH__
        return x_scaled01_gpu(i,o);
        #else
        return x_scaled01(i,o);
        #endif
    }

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    fp get_w(int i, int o){
        #ifdef __CUDA_ARCH__
        return w_gpu(i,o);
        #else
        return w(i,o);
        #endif
    }

}
