#pragma once

#include <fftw3.h>

#ifdef USE_SINGLE_PRECISION
#define fftw_complex fftwf_complex
#define fftw_plan fftwf_plan
#define fftw_plan_dft_r2c fftwf_plan_dft_r2c
#define fftw_plan_dft_c2r fftwf_plan_dft_c2r
#define fftw_destroy_plan fftwf_destroy_plan
#define fftw_execute fftwf_execute
#define fftw_alloc_real fftwf_alloc_real
#define fftw_alloc_complex fftwf_alloc_complex
#define fftw_plan_many_dft_r2c fftwf_plan_many_dft_r2c
#define fftw_plan_many_dft_c2r fftwf_plan_many_dft_c2r
#endif

