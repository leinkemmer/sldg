#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <array>
#include <vector>
#include <numeric>
#include <cmath>
#include <complex>
#include <fstream>
#include <algorithm>
#include <memory>
#include <cstddef>
#include <assert.h>
using std::cout;
using std::endl;
using std::string;
using std::array;
using std::vector;
using std::complex;
using std::min;
using std::max;
using std::abs;
using std::fill;
using std::accumulate;
using std::copy;
using std::unique_ptr;

#ifdef USE_SINGLE_PRECISION
typedef float fp;
#else
typedef double fp;
#endif

// This specifies the index type used for arrays and such
typedef ptrdiff_t Index;

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

enum class location{
    left, right, mid, all
};

enum class troubled_cell_indicator{
    none, mean_err, minmod
};

enum class troubled_cell_modifier{
    none, average, simple_weno, linear_weno, hweno
};

std::ostream& operator<<(std::ostream& out, troubled_cell_indicator tci){
    string s{};
    if(tci==troubled_cell_indicator::none)
        s = "tci:none";
    else if(tci==troubled_cell_indicator::mean_err)
        s = "tci:mean_err";
    else if(tci==troubled_cell_indicator::minmod)
        s = "tci:minmod";
    return out << s;
}

std::ostream& operator<<(std::ostream& out, troubled_cell_modifier tcm){
    string s{};
    if(tcm==troubled_cell_modifier::none)
        s = "tcm:none";
    else if(tcm==troubled_cell_modifier::average)
        s = "tcm:average";
    else if(tcm==troubled_cell_modifier::simple_weno)
        s = "tcm:simple_weno";
    else if(tcm==troubled_cell_modifier::linear_weno)
        s = "tcm:linear_weno";
    else if(tcm==troubled_cell_modifier::hweno)
        s = "tcm:hweno";
    return out << s;
}



/// This function removes the entry dim and d+dim from the input array (in) and
/// stores the remaining entries in the output array (out). This is a helper
/// function that is used to prepare boundary data in the Vlasov-Poisson
/// solver.
template<Index d>
array<Index,2*d-2> slice_no(Index dim, array<Index,2*d>& in) {
    array<Index,2*d-2> out;
    int j=0;
    for(Index i=0;i<d;i++) {
        if(i!=dim) {
            out[j]=in[i];
            out[j+(d-1)]=in[i+d];
            j++;
        }
    }
    return out;
}

template<size_t dx>
Index get_index_slice(size_t dim, array<Index,dx+dx>& idx, const array<Index,dx+dx>& max){
    Index j = 0;
    Index stride = 1;        
    for(size_t i = 0; i < dx+dx; ++i) {
        if( (i!=dim) && (i!=dim+dx) ) {
            j += idx[i]*stride;
            stride *= max[i];
        }
    }
    return j*max[dx+dim];
}

template<size_t dx, size_t dv> 
struct index_dxdv_no {
    using array_type = array<Index,2*(dx+dv)>;
    array_type data;
    
    index_dxdv_no() {}
    
    index_dxdv_no(std::initializer_list<Index> in) {
        assert(in.size() == data.size());
        copy(in.begin(), in.end(), data.begin());
    }

    index_dxdv_no(array_type in) {
        data = in;
    }

    array_type& get_array() {
        return data;
    }

    Index& operator[](Index i) {
        return data[i];
    }
    
    Index size() {
        return data.size();
    }

    array<Index,2*dx> x_part() {
        array<Index,2*dx> xpart;
        for(size_t i=0;i<dx;i++) {
            xpart[i]    = data[i];
            xpart[dx+i] = data[dx+dv+i]; 
        }
        return xpart;
    }

    array<Index,2*dv> v_part() {
        array<Index,2*dv> vpart;
        for(size_t i=0;i<dv;i++) {
            vpart[i]    = data[dx+i];
            vpart[dv+i] = data[dx+dv+dx+i]; 
        }
        return vpart;
    }

    array<Index,dx+dv> n_part() {
        array<Index,dx+dv> npart;
        for(size_t i=0;i<dx+dv;i++)
            npart[i] = data[i];
        return npart;
    }

    array<Index,dx+dv> o_part() {
        array<Index,dx+dv> opart;
        for(size_t i=0;i<dx+dv;i++)
            opart[i] = data[dx+dv+i];
        return opart;
    }

    auto begin() -> decltype(data.begin()) {
        return data.begin();
    }

    auto end() -> decltype(data.end()) {
        return data.end();
    }

    array<Index,2*(dx+dv)-2> slice(Index dim) {
        return ::slice_no<dx+dv>(dim,data);
    }
};

template<typename T, size_t dx, size_t dv>
struct array_dxdv {
    using array_type = array<T,dx+dv>;
    array_type data;
   
    array_dxdv() {}

    array_dxdv(std::initializer_list<T> in) {
        assert(in.size() == data.size());
        copy(in.begin(), in.end(), data.begin());
    }
    
    array_type& get_array() {
        return data;
    }

    T& operator[](Index i) {
        return data[i];
    }

    Index size() const {
        return data.size();
    }

    array<T,dv> v_part() const {
        array<T,dv> vpart;
        for(size_t i=0;i<dv;i++)
            vpart[i] = data[dx+i];
        return vpart;
    }

    array<T,dx> x_part() const {
        array<T,dx> xpart;
        for(size_t i=0;i<dx;i++)
            xpart[i] = data[i];
        return xpart;
    }

    auto begin() -> decltype(data.begin()) {
        return data.begin();
    }

    auto end() -> decltype(data.end()) {
        return data.end();
    }
};


#include <generic/utility.hpp>

template<typename T, size_t dx, size_t dv>
std::ostream& operator<<(std::ostream& os, array_dxdv<T,dx,dv> a) {
    os << a.get_array();
    return os;
}

template<size_t dx, size_t dv>
std::ostream& operator<<(std::ostream& os, index_dxdv_no<dx,dv> a) {
    os << a.get_array();
    return os;
}

#include <generic/timer.hpp>
#ifndef GENERIC_NO_STORAGE
#include <generic/storage.hpp>
#endif

#include <boost/format.hpp>

