#pragma once

#include <pnetcdf.h>
#include <generic/common.hpp>

#define NC_CHECK(e) { \
    int res = e; \
    if(res != NC_NOERR) { \
        cout << "ERROR NetCDF " << __FILE__ << ":" << __LINE__ << " message: " \
             << ncmpi_strerror(res) << endl; \
    exit(1); } }


enum class nc_data_mode {
    collective, independent
};

struct NCWriterVariable {
    int ncfile, vid;

    NCWriterVariable(int _ncfile, int _vid) : ncfile(_ncfile), vid(_vid) {}

    template<size_t d>
    void write(array<Index,d> start, array<Index,d> shape,
            fp* data, nc_data_mode mode=nc_data_mode::collective) {

        vector<MPI_Offset> _start(start.begin(),start.end());
        vector<MPI_Offset> _shape(shape.begin(),shape.end());

        //the implementation below is required in order to reduce the
        //amount of individual Requests which is limited to 2GB, see
        //https://parallel-netcdf.github.io/wiki/FileLimits.html.
        //The loop splits up the entire data along the last dimension,
        //such that dof[last_dim] times a n-1-dimensional set is written,
        //which should be smaller than 2GB if a reasonable grid is choosen.
        //Note that this routine is called with reverse(start),reverse(shape)
        //
        //This has no impact on writing the electric field since
        //there shape[0] is always 1.

        //shape[0] since arrays are stored in column major form
        Index loop_over = shape[0];
        _shape[0] = ((MPI_Offset)1);

        Index num_elements = 1;
        for(Index j=1;j<d;++j)
            num_elements *= shape[j];

        for(Index i=0;i<loop_over;++i){
            if(mode == nc_data_mode::collective) {
                #ifdef USE_SINGLE_PRECISION
                NC_CHECK( ncmpi_put_vara_float_all(ncfile, vid, &_start[0],
                            &_shape[0], data + i*num_elements ) );
                #else
                NC_CHECK( ncmpi_put_vara_double_all(ncfile, vid, &_start[0],
                            &_shape[0], data + i*num_elements ) );
                #endif
            } else {
                NC_CHECK( ncmpi_begin_indep_data(ncfile) );
                #ifdef USE_SINGLE_PRECISION
                NC_CHECK( ncmpi_put_vara_float(ncfile, vid, &_start[0], 
                            &_shape[0], data) );
                #else
                NC_CHECK( ncmpi_put_vara_double(ncfile, vid, &_start[0], 
                            &_shape[0], data) );
                #endif
                NC_CHECK( ncmpi_end_indep_data(ncfile) );
            }
            _start[0] += 1;
        }

    }

    void write(const vector<fp>& v) {
        MPI_Offset start = 0;
        MPI_Offset shape = v.size();
        NC_CHECK( ncmpi_begin_indep_data(ncfile) );
        #ifdef USE_SINGLE_PRECISION
        NC_CHECK( ncmpi_put_vara_float(ncfile, vid, &start, &shape, &v[0]) );
        #else
        NC_CHECK( ncmpi_put_vara_double(ncfile, vid, &start, &shape, &v[0]) );
        #endif
        NC_CHECK( ncmpi_end_indep_data(ncfile) );
    }
};


struct NCWriterParallel {
    int ncfile;
    std::map<string,int> dimids;

    NCWriterParallel(string filename, vector<string> d_names, 
            vector<Index> global_ext) {
        NC_CHECK( ncmpi_create(MPI_COMM_WORLD, filename.c_str(),
                    NC_CLOBBER|NC_64BIT_OFFSET, MPI_INFO_NULL, &ncfile) );

        vector<MPI_Offset> _global_ext(global_ext.begin(),global_ext.end());
        for(size_t i=0;i<d_names.size();i++) {
            int dimid;
            NC_CHECK( ncmpi_def_dim(ncfile, d_names[i].c_str(), _global_ext[i],
                        &dimid) );
            dimids[d_names[i]] = dimid;
        }
    }

    NCWriterVariable add_variable(string name, vector<string> used_dims) {
        vector<int> used_dimids;
        for(string key : used_dims)
            used_dimids.push_back(dimids[key]);

        int vid;
        #ifdef USE_SINGLE_PRECISION
        NC_CHECK( ncmpi_def_var(ncfile, name.c_str(), NC_FLOAT,
                    used_dimids.size(), &used_dimids[0], &vid) );
        #else
        NC_CHECK( ncmpi_def_var(ncfile, name.c_str(), NC_DOUBLE,
                    used_dimids.size(), &used_dimids[0], &vid) );
        #endif
        return NCWriterVariable(ncfile, vid);
    }

    void end_definitions() {
        NC_CHECK( ncmpi_enddef(ncfile) );
    }

    ~NCWriterParallel() {
        NC_CHECK( ncmpi_close(ncfile) );
    }
};

