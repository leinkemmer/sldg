#pragma once

#include <array>
#include <vector>
#include <algorithm>
using std::max;

#include <generic/utility.hpp>

#ifdef __CUDACC__
#include <cuda.h>
#endif

template<class T>
struct multi_array_view {

    multi_array_view(T* _v, Index _stride, array<Index,2> _e)
        : v(_v), stride(_stride), e(_e) {
    }

    Index linear_idx(array<Index,2> idx) {
        return stride*(e[1]*idx[0] + idx[1]);
    }

    T& operator()(array<Index,2> idx) {
        return v[linear_idx(idx)];
    }
    
    template<typename... Ints> 
    T& operator()(Ints&&... idx) {
        static_assert(sizeof...(Ints) == 2, 
                "wrong number of arguments to operator().");
        return v[linear_idx(array<Index,2>({idx...}))];
    }

    array<Index,2> shape() {
        return e;
    }
private:
    T* v;
    Index stride;
    array<Index,2> e;
};

template<class T, size_t d>
struct multi_array {
    array<Index,d> e{}; //sets entries of e to zero
    T* v = nullptr;
    bool gpu;

    multi_array(bool _gpu=false) : gpu(_gpu) {}

    multi_array(array<Index,d> _e, bool _gpu=false) : gpu(_gpu) {
        static_assert(d%2 == 0, "d in multi_array must be a multiple of 2");
        resize(_e);
    }

    // copy constructor
    multi_array(const multi_array& ma) {
        gpu = ma.gpu;
        resize(ma.e);
        if(!gpu) {
            copy(ma.data(), ma.data()+ma.num_elements(), v);
        } else {
            #ifdef __CUDACC__
            cudaMemcpy(v, ma.data(), sizeof(T)*ma.num_elements(),
                    cudaMemcpyDeviceToDevice);
            #else
            cout << "ERROR: compiled without GPU support" << __FILE__ << ":" 
                 << __LINE__ << endl;
            exit(1);
            #endif
        }
    }

    // assignment operator
    multi_array& operator=(const multi_array& ma) {
        if(ma.v!=v){ //no self assignment
            resize(ma.e);
            if(!ma.gpu && !gpu) { // both on CPU
                copy(ma.data(), ma.data()+ma.num_elements(), v);
            } else {
                #ifdef __CUDACC__
                if(!gpu)         // dst on CPU
                    cudaMemcpy(v, ma.data(), sizeof(T)*ma.num_elements(),
                            cudaMemcpyDeviceToHost);
                else if(!ma.gpu) // src on CPU
                    cudaMemcpy(v, ma.data(), sizeof(T)*ma.num_elements(),
                            cudaMemcpyHostToDevice);
                else             // both src and dst on GPU
                    cudaMemcpy(v, ma.data(), sizeof(T)*ma.num_elements(),
                            cudaMemcpyDeviceToDevice);
                #else
                cout << "ERROR: compiled without GPU support" << endl;
                exit(1);
                #endif
            }
        }
        return *this;
    }

    void resize(array<Index,d> _e) {
        //resize only if number of elements is different, 
        //the shape does not matter
        if(prod(e)!=prod(_e)){
            Index num_elements = prod(_e);
            if(!gpu) {
                free(v);
                v = (T*)aligned_alloc(64,sizeof(T)*num_elements);
            } else {
                #ifdef __CUDACC__
                cudaFree(v);
                v = (fp*)gpu_malloc(sizeof(T)*num_elements);
                #else
                cout << "ERROR: compiled without GPU support" << __FILE__ << ":" 
                     << __LINE__ << endl;
                exit(1);
                #endif
            }
        }
        e = _e;
    }

    ~multi_array() {
        if(v != nullptr) {
            if(!gpu) {
                free(v);
            } else {
                #ifdef __CUDACC__
                cudaFree(v);
                #else
                cout << "ERROR: compiled without GPU support" << __FILE__ << ":"
                     << __LINE__ << endl;
                exit(1);
                #endif
            }
        }
    }

    void swap(multi_array& x) {
        std::swap(v, x.v);
        std::swap(e, x.e);
        std::swap(gpu, x.gpu);
    }

    Index linear_idx(array<Index,d> idx) {
        Index k=0;
        Index stride = 1;
        for(size_t i=0;i<d/2;i++) {
            k += stride*(e[d/2+i]*idx[i] + idx[d/2+i]);
            stride *= e[i]*e[d/2+i];
        }
        assert(k < num_elements());
        return k;
    }

    T& operator()(array<Index,d> idx) {
        return v[linear_idx(idx)];
    }

    // TODO: if called as (z,0) this gives a -Wnarrowing warning. These warnings
    // are turned off in the build system at the moment.
    template<typename... Ints>
    T& operator()(Ints&&... idx) {
        static_assert(sizeof...(Ints) == d, "wrong number of arguments to ().");
        return v[linear_idx(array<Index,d>({idx...}))];
    }

    array<Index,d> shape() {
        return e;
    }

    // This is the shape of the array stored in memory.
    array<Index,d> real_shape() {
        array<Index,d> rs;
        for(size_t i=0;i<d/2;i++) {
            rs[2*i]   = e[i];
            rs[2*i+1] = e[d/2+i];
        }
        return rs;
    }

    T* data() const {
        return &v[0];
    }

    Index num_elements() const {
        return prod(e);
    }

    // ------------------------------------------------------
    T* begin() const noexcept {
        return &v[0];
    }

    T* end() const noexcept {
        return &v[num_elements()];
    }

};

template<class T, size_t d>
multi_array_view<T> slice_array(multi_array<T,d>& in, size_t dim,
        array<Index,d-2> idx_other) {
    Index stride = 1;
    for(size_t i=0;i<dim;i++)
        stride *= in.e[i]*in.e[d/2+i];

    array<Index,d> idx;
    Index k=0;
    for(size_t i=0;i<d/2;i++) {
        if(i != dim) {
            idx[i]     = idx_other[k];
            idx[d/2+i] = idx_other[d/2-1+k];
            k++;
        } else {
            idx[i]     = 0;
            idx[d/2+i] = 0;
        }
    }

    return multi_array_view<T>(&in(idx), stride, {in.e[dim],in.e[d/2+dim]});
}


/// Compute the infinity norm (maximum norm) for the difference of two
/// multi_arrays.
template<Index d>
fp inf_norm(multi_array<fp, 2*d> data0, multi_array<fp, 2*d> data1) {
    fp m=0.0;
    fp* it1=data0.data(); fp* it2=data1.data();
    while(it1!=data0.data()+data0.num_elements()) {
        m = max(m,abs(*it1-*it2));
        it1++; it2++;
    }
    return m;
}

template<size_t d>
fp max_element_abs(const multi_array<fp,d>& F) {
    auto cmp = [](fp a, fp b) {
        return fabs(a)<fabs(b);
    };
    return fabs(*std::max_element(F.data(),F.data()+F.num_elements(),cmp));
}

template<size_t d>
fp inner_product(const multi_array<fp,d>& ma1, const multi_array<fp,d>& ma2) {
        return std::inner_product(ma1.begin(), ma1.end(), ma2.begin(), fp(0.0))/double(ma1.num_elements());
}
