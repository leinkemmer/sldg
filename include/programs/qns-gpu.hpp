#pragma once

#ifdef __CUDACC__

#include <generic/common.hpp>
#include <programs/sldg2d/generic_sldg2d.hpp>
#include <cufft.h>
#include <cusparse_v2.h>


#ifndef USE_SINGLE_PRECISION
using fp = double;
using cmplx = cufftDoubleComplex;
#else
using fp = float;
using cmplx = cufftComplex;
#endif


inline const char* cufftGetErrorString(cufftResult_t status){

    if (status == CUFFT_SUCCESS){
        return "The cuFFT operation was successful.\n";
    } else if (status == CUFFT_INVALID_PLAN){
        return "cuFFT was passed an invalid plan handle.\n";
    } else if (status == CUFFT_ALLOC_FAILED){
        return "cuFFT failed to allocate GPU or CPU memory.\n";
    } else if (status == CUFFT_INVALID_TYPE){
        return "No longer used.\n";
    } else if (status == CUFFT_INVALID_VALUE) {
        return "User specified an invalid pointer or parameter.\n";
    } else if (status == CUFFT_INTERNAL_ERROR) {
        return "Driver or internal cuFFT library error.\n";
    } else if (status == CUFFT_EXEC_FAILED) {
        return "Failed to execute an FFT on the GPU.\n";
    } else if (status == CUFFT_SETUP_FAILED) {
        return "The cuFFT library failed to initialize.\n";
    } else if (status == CUFFT_INVALID_SIZE) {
        return "User specified an invalid transform size.\n";
    } else if (status == CUFFT_UNALIGNED_DATA) {
        return "No longer used.\n";
    } else if (status == CUFFT_INCOMPLETE_PARAMETER_LIST) {
        return "Missing parameters in call.\n";
    } else if (status == CUFFT_INVALID_DEVICE) {
        return "Execution of a plan was on different GPU than plan creation.\n";
    } else if (status == CUFFT_PARSE_ERROR) {
        return "Internal plan database error.\n";
    } else if (status == CUFFT_NO_WORKSPACE) {
        return "No workspace has been provided prior to plan execution.\n";
    } else if (status == CUFFT_NOT_IMPLEMENTED) {
        return "Function does not implement functionality for parameters given.\n";
    } else if (status == CUFFT_LICENSE_ERROR) {
        return "Used in previous versions.\n";
    } else if (status == CUFFT_NOT_SUPPORTED) {
        return "Operation is not supported for parameters given.\n";
    } else {
        return "NO VALID ERROR CODE\n";
    }
}


#define cufftErrchk(ans) { cufftAssert((ans), __FILE__, __LINE__); }
inline void cufftAssert(cufftResult_t code, const char *file, int line)
{
   if (code != CUFFT_SUCCESS)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cufftGetErrorString(code), file, line);
      exit(code);
   }
}


#define cusparseErrchk(ans) { cusparseAssert((ans), __FILE__, __LINE__); }
inline void cusparseAssert(cusparseStatus_t code, const char *file, int line)
{
   if (code != CUSPARSE_STATUS_SUCCESS)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n",
              cusparseGetErrorString(code), file, line);
      exit(code);
   }
}


struct fft_2d_in_3d_gpu{

    static constexpr int rank = 2;

    int Ntheta;
    int Nz;
    int batch;

    //distance between successive input elements 
    //in innermost dimension
    int istride;
    int ostride;

    //distance between first element of two consecutive 
    //signals in a batch of input/output data
    const int idist{1};
    const int odist{1};

    cufftHandle cufft_plan_forward;
    cufftHandle cufft_plan_backward;

    fft_2d_in_3d_gpu(int Nrp1, int _Ntheta, int _Nz)
        : Ntheta{_Ntheta}, Nz{_Nz}, batch{Nrp1},
          istride{Nrp1}, ostride{Nrp1}
    {

        int n[rank] = {Nz, Ntheta};

        //pointer that indicates storage dimensions of 
        //input/output data, reverse everywhere!!!
        int inembed[rank] = {Nrp1*Nz, Ntheta};
        int onembed[rank] = {Nrp1*Nz, Ntheta/2+1};

        #ifndef USE_SINGLE_PRECISION
        cufftErrchk (cufftPlanMany(&cufft_plan_forward, rank, n,
                          inembed,istride,idist,
                          onembed,ostride,odist,
                          CUFFT_D2Z,batch) );

        cufftErrchk (cufftPlanMany(&cufft_plan_backward, rank, n,
                          onembed,ostride,odist,
                          inembed,istride,idist,
                          CUFFT_Z2D,batch) );
        #else
        cufftErrchk (cufftPlanMany(&cufft_plan_forward, rank, n,
                          inembed,istride,idist,
                          onembed,ostride,odist,
                          CUFFT_R2C,batch) );

        cufftErrchk (cufftPlanMany(&cufft_plan_backward, rank, n,
                          onembed,ostride,odist,
                          inembed,istride,idist,
                          CUFFT_C2R,batch) );
        #endif

    }

    void forward(fp* d_in, cmplx* d_out){

        cufftErrchk (cufftExecD2Z(cufft_plan_forward,
                                  d_in, d_out) );

    }

    void backward(cmplx* d_in, fp* d_out){

        cufftErrchk (cufftExecZ2D(cufft_plan_backward,
                                  d_in, d_out) );

    }

    ~fft_2d_in_3d_gpu(){
        cufftErrchk( cufftDestroy(cufft_plan_forward) );
        cufftErrchk( cufftDestroy(cufft_plan_backward) );
    }

};


struct n0r_f{

    double operator()(double r){
        return r;
    }
};

struct alpha_f{

    double operator()(double r, int j, int k){
        return r+j+k;
    }

};


//uses the gtsv2StridedBatch routine from cuSparse
//TODO: what is the difference in performance between the gtsvInterleavedBatch??
template<size_t oz>
struct ode_fd_cc_gpu{

    int Nrp1, Ntheta, Nz;
    fp ar, hr;

    fft_2d_in_3d_gpu fft;
    generic_container<cmplx> rhohat;

    cusparseHandle_t cusparse_handle;
    generic_container<cmplx> dl;
    generic_container<cmplx> dm;
    generic_container<cmplx> du;
    char* d_buffer;

    ode_fd_cc_gpu(int _Nrp1, int _Ntheta, int _Nz,
                  fp _ar, fp _hr, n0r_f n0r, alpha_f alpha)
        : Nrp1{_Nrp1}, Ntheta{_Ntheta}, Nz{_Nz*oz}, 
          ar{_ar}, hr{_hr}, fft{Nrp1,Ntheta,Nz}
    {
        cusparseErrchk( cusparseCreate(&cusparse_handle) );
        size_t bufferSizeInBytes;

        dl.resize((Nrp1-2)*(Ntheta/2+1)*Nz);
        dm.resize((Nrp1-2)*(Ntheta/2+1)*Nz);
        du.resize((Nrp1-2)*(Ntheta/2+1)*Nz);

        #ifdef USE_SINGLE_PRECISION
        cusparseErrchk(cusparseCgtsv2StridedBatch_bufferSizeExt(
                        cusparse_handle, Nrp1-2, dl.data(true), dm.data(true), 
                        du.data(true), rhohat.data(true)+1,
                        (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #else
        cusparseErrchk(cusparseZgtsv2StridedBatch_bufferSizeExt(
                       cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                       du.data(true), rhohat.data(true)+1,
                       (Ntheta/2+1)*Nz, Nrp1, &bufferSizeInBytes));
        #endif
        
        gpuErrchk( cudaMalloc(&d_buffer, bufferSizeInBytes));

        init(n0r,alpha);
    }


    void init(n0r_f n0r, alpha_f alpha){

        std::fill(dl.h_data.begin(),dl.h_data.end(),0.0);
        std::fill(dm.h_data.begin(),dl.h_data.end(),0.0);
        std::fill(du.h_data.begin(),dl.h_data.end(),0.0);

        for(int k=0;k<Nz;++k)
            for(int j=0;j<Ntheta/2+1;++j)
                for(int i=0;i<Nrp1-2;++i){
                    
                    fp r    = ar + hr*(i+1.0);
                    fp rp12 = ar + hr*(i+1.5);
                    fp rm12 = ar + hr*(i-1.5);

                    fp mid = n0r(r+rp12+rm12);
                    dm.h_data[i+(Nrp1-2)*(j+(Ntheta/2+1)*k)] = {mid,0.0};

                    if(i<Nrp1-3){
                        fp off = alpha(1.0,j,k);
                        du.h_data[i+  (Nrp1-2)*(j+(Ntheta/2+1)*k)] = {off,0};
                        dl.h_data[i+1+(Nrp1-2)*(j+(Ntheta/2+1)*k)] = {off,0};
                    }
                }

    }

    void solve_qns(fp* rho, fp* qhi){

        fft.forward(rho,rhohat.data(true));

        #ifdef USE_SINGLE_PRECISION
        cusparseErrchk(cusparseCgtsv2StridedBatch(
                    cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                    du.data(true), rhohat.data(true)+1, (Ntheta/2+1)*Nz,
                    Nrp1, d_Buffer));
        #else
        cusparseErrchk(cusparseZgtsv2StridedBatch(
                    cusparse_handle, Nrp1-2, dl.data(true), dm.data(true),
                    du.data(true), rhohat.data(true)+1, (Ntheta/2+1)*Nz,
                    Nrp1, d_buffer));
        #endif

        fft.backward(rhohat.data(false),qhi);

    }

};

template<size_t o2d, size_t oz, size_t ov>
void compute_rhs(generic_container<fp>& in, generic_container<fp>& rho){
    cout << in.N << rho.N << endl;
} 

template<size_t o2d, size_t oz>
struct quasi_neutrality_gpu{

    array<fp,3> a;
    array<fp,3> b;
    array<fp,3> h;
    generic_container<fp> gauss_o2d;
    generic_container<fp> gauss_oz;

    int Nr, Ntheta, Nz;

    generic_container<fp> rho;
    generic_container<fp> rho_cc;

    generic_container<fp> phi_cc;

    generic_container<fp> dr_phi;
    generic_container<fp> dr_phi_cc;
    
    generic_container<fp> dtheta_phi;
    generic_container<fp> dtheta_phi_cc;

    generic_container<fp> dz_phi;
    generic_container<fp> dz_phi_cc;

    unique_ptr<ode_fd_cc_gpu<oz>> ode_fd;

    fp electric_energy;

    quasi_neutrality_gpu(array<fp,3> _a, array<fp,3> _b,
                         int _Nr, int _Ntheta, int _Nz,
                         n0r_f n0r, alpha_f alpha)
        : a{_a}, b{_b}, Nr{_Nr}, Ntheta{_Ntheta}, Nz{_Nz}
    {

        h[0] = (b[0]-a[0])/double(Nr);
        h[1] = (b[1]-a[1])/double(Ntheta);
        h[2] = (b[2]-a[2])/double(Nz*oz);

        gauss_o2d.h_data = gauss::all_x_scaled01(o2d);
        gauss_o2d.copy_to_gpu();
        gauss_oz.h_data = gauss::all_x_scaled01(oz);
        gauss_oz.copy_to_gpu();

        rho.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        rho_cc.resize((Nr+1)*Ntheta*Nz*oz);

        phi_cc.resize((Nr+1)*Ntheta*Nz*oz);
        
        dr_phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        dr_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dtheta_phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        dtheta_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dz_phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        dz_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        ode_fd = make_unique<ode_fd_cc_gpu<oz>>(Nr+1, Ntheta, Nz, a[0], 
                                                h[0], n0r, alpha);
        
    }


    void solve(generic_container<fp>& in, size_t ov){

        if(ov==2)
            compute_rhs<o2d,oz,2>(in,rho);
        if(ov==3)
            compute_rhs<o2d,oz,3>(in,rho);
        if(ov==4)
            compute_rhs<o2d,oz,4>(in,rho);
    
        //interpolate_z_dt_to_equi<<<(Nr*o2d,Ntheta*o2d),Nz>>>(
        //                                        rho_dg_nodes.data(true),
        //                                        rho_dg_nodes.data(true));
        //evaluate_at_grid_corners<<<(Nr,Ntheta),Nz*oz>>>(rho_dg_nodes.data(true),
        //                                           rho.data(true);

        ode_fd.solve(rho_cc.data(true),phi_cc.data(true));

        //compute_electric_energy();

        //compute_all_derivatives( ); 

        //interpolate_z_equi_to_dg(dr_phi_cc.data(true), dr_phi_cc.data(true);
        //interpolate_z_equi_to_dg(dtheta_phi_cc.data(true),
        //                         dtheta_phi_cc.data(true);
        //interpolate_z_equi_to_dg(dz_phi_cc.data(true), dz_phi_cc.data(true);

        //interpolate_cc_to_dg(dr_phi_cc.data(true), dr_phi.data(true));
        //interpolate_cc_to_dg(dr_phi_cc.data(true), dr_phi.data(true));
        //interpolate_cc_to_dg(dr_phi_cc.data(true), dr_phi.data(true));

        //copy dz_phi to host;
 
    }


};



#endif
