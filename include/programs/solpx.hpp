#include <programs/poisson_solvers.hpp>
#include <programs/vp-block-list.hpp>

struct fluxes{
    fp ele_flux_right;
    fp ion_flux_right;
};

template<size_t dx, size_t dv, fp (*init_f)(fp*)>
struct scrape_off_layer_px{

    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;

    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;
    using domain_x = multi_array<fp,dx_size>;
    using efield   = array< domain_x, dx>;

    vec_d a_ele, b_ele;
    vec_d a_ion, b_ion;

    index_no e;
    //TODO:
    //index_no e_ele_interior;
    //index_no e_ele_bdry;
    //index_no e_ions;

    fp final_T;
    fp tau;
    fp time;

    source_f<dx,dv> source_func;

    vector<domain_x> rho;

    domain_x charge_density;
    domain_x charge_density_write;

    vector<efield> E;
    efield E_global;

    unique_ptr< poisson_solver<dx> > poisson; 

    unique_ptr< vp_block_list<dx,dv,init_f> > blocks;

    fp sqrt_ei_mass_ratio;
    Index bdr_max_length;
    int number_snapshots;
    bool adjust_domain;
    bool BGK_collision_term;
    fp collision_rate;
    fp collision_rate_ions;
    fp collision_rate_electrons;

    troubled_cell_indicator tci;
    troubled_cell_modifier tcm;

    int num_ele_domains;
    vector<int> bdry_cell_ratios;

    bool gpu;
    int num_gpus;

    string filename;

    scrape_off_layer_px(vec_d _a, vec_d _b, index_no _e, 
                        fp _final_T, fp _tau, source_f<dx,dv> sf, 
                        fp ie_mass_ratio, fp collision_rate,
                        int _bdry_cell_ratio,
                        int _number_snapshots, 
                        bool _adjust_domain,
                        troubled_cell_indicator _tci, troubled_cell_modifier _tcm,
                        bool sldg_limiter, int _num_ele_domains,
                        bool _gpu, int _num_gpus)
        : a_ele{_a}, b_ele{_b}, a_ion{_a}, b_ion{_b},
          e{_e}, final_T{_final_T}, tau{_tau}, source_func{sf},
          number_snapshots{_number_snapshots},  
          adjust_domain{_adjust_domain},
          tci{_tci}, tcm{_tcm}, 
          num_ele_domains{_num_ele_domains}, gpu{_gpu}, num_gpus{_num_gpus}
    {
        gt::start("initialization");

        sqrt_ei_mass_ratio = 1.0/sqrt(ie_mass_ratio);
        cout << "sqrt(m_e/m_i): " << sqrt_ei_mass_ratio << "\n";

        if(!(num_ele_domains==1 || num_ele_domains==3)){
            cout << "ERROR: electron domain in x direction can only "
                 << "be partitioned in 1 or 3 domains.\n";
            exit(1);
        }

        cout << "BGK collision term ";
        if(collision_rate!=0.0){
            BGK_collision_term=true;
            collision_rate_electrons = collision_rate;
            collision_rate_ions = collision_rate*sqrt_ei_mass_ratio;
            cout << "used with a electron collision rate of "
                 << collision_rate_electrons
                 << " and a ion collision rate of "
                 << collision_rate_ions << "." << endl;
        } else {
            BGK_collision_term=false;
            cout << "not used" << endl;
        }

        bdry_cell_ratios.resize(num_ele_domains+1);
        vector<location> locations(num_ele_domains+1);
        if(num_ele_domains==3){
            bdry_cell_ratios[0] = _bdry_cell_ratio;
            bdry_cell_ratios[1] = 1;
            bdry_cell_ratios[2] = _bdry_cell_ratio;
            bdry_cell_ratios[3] = 1;
            locations[0] = location::left;
            locations[1] = location::mid;
            locations[2] = location::right;
            locations[3] = location::all;
            //currently the total number of cells = 3*e[0]
            poisson = make_poisson_1d_SIPG<dx>(num_ele_domains*e[0],e[d],e[d],
                                               a_ion[0],b_ion[0],
                                               e[0],_bdry_cell_ratio);
        } else {
            if(_bdry_cell_ratio!=1){
                cout << "INFO: using no domain partition in x (num_ele_domains=1) "
                     << "bdry_cell_ratio can not be applied and is set to 1.\n";
            }
            bdry_cell_ratios[0] = 1;
            bdry_cell_ratios[1] = 1;
            locations[0] = location::all;
            locations[1] = location::all;
            poisson = make_poisson_1d_SIPG<dx>(num_ele_domains*e[0],e[d],e[d],
                                               a_ion[0],b_ion[0]);
        }

        bdr_max_length = 3*prod(slice(0,e.get_array()));
        //assumes a CFL<3 on coarse grid
        cout << "bdr_max_length: " << bdr_max_length << endl;

        array<Index,2*dx> ex_global = {num_ele_domains*e[0],e[d]};

        charge_density.resize(ex_global);
        charge_density_write.resize(ex_global);

        rho.resize(num_ele_domains+1);
        E_global[0].resize(ex_global);
        E.resize(num_ele_domains+1);
        for(int i=0;i<num_ele_domains+1;++i)
            E[i][0].resize(e.x_part());

        enable_peer_to_peer();

        blocks.reset( new vp_block_list<dx,dv,init_f>( _a, _b, e, gpu,
                           num_gpus, bdr_max_length, sldg_limiter, 
                           bdry_cell_ratios, locations, sqrt_ei_mass_ratio,
                           num_ele_domains, BGK_collision_term) );
        blocks->init();

        std::stringstream ss;
        ss << tci << "_" << tcm;
        filename = "solpx-evolution"+std::to_string(e[0])+"-"+std::to_string(e[2])+
                   "_"+std::to_string(e[1])+"-"+std::to_string(e[3])+
                   "_"+std::to_string(final_T)+"_"+std::to_string(tau)+
                   "_"+ss.str()+"_sldglim"+std::to_string(sldg_limiter)+
                   "_numxdom"+std::to_string(num_ele_domains)+
                   "_collisionrate"+std::to_string(collision_rate)+
                   ((gpu) ? "_gpu" : "")+".data";

        cout << "filename: " << filename << endl;

        std::ofstream fs(filename);
        fs << "#time  electron_particle_flux_right  ion_particle_flux_right " 
           << " v_domain_electrons  v_domain_ions" << endl;
        fs.close();

        gt::stop("initialization");
    }

    ~scrape_off_layer_px(){

    }

    void run(){

        int n = ceil(final_T/tau);

        time = 0;

        timer t_timestep;

        int every = n+2;
        if(number_snapshots!=0)
            every = max(n/number_snapshots,1);

        gt::start("total");
        for(int i=0;i<n;++i){

            write_variables(time,filename); //solves the poisson problem

            if(enable_output && number_snapshots!=0 && i%every==0){
                gt::start("write_densities");
                cout << "time: " << time << endl;
                write_densities(time);
                write_charge_densities(time);
                write_electric_field_and_potential(time);
                gt::stop("write_densities");
            }

            if(time+tau>final_T){
                tau = final_T-time;
            }

            t_timestep.start();
            gt::start("timestep");
            if(BGK_collision_term==true)
                predictor_corrector_timestep(tau,time);
            else
                strang_timestep(tau,time);
            gt::stop("timestep");
            t_timestep.stop();

            time+=tau;

        }

        write_variables(time,filename); 

        if(enable_output && number_snapshots!=0){
            gt::start("write_densities");
            cout << "time: " << time << endl;
            write_densities(time);
            write_charge_densities(time);
            write_electric_field_and_potential(time);
            gt::stop("write_densities");
        }
        gt::stop("total");

        std::ofstream global_timer("global_timer"+filename);
        global_timer << gt::sorted_output();
 
        cout << "time steps took: " << t_timestep.total() << endl;
    }


    void strang_timestep(fp tau, fp time){

        if(source_func.is_used()){
            gt::start("source");
            blocks->source(0.5*tau, time+0.5*tau, source_func);
            blocks->swap();
            gt::stop("source");
        }

        gt::start("bdry_transfer_advection");
        set_zero_inflow_bc(0.5*tau);
        gt::stop("bdry_transfer_advection");

        gt::start("advection_x");
        blocks->advection_v(0,0.5*tau);
        blocks->swap();
        gt::stop("advection_x");

        gt::start("boundary_transfer_limiter");
        transfer_for_weno_limiter();
        gt::stop("boundary_transfer_limiter");
        gt::start("weno_limiter_x");
        blocks->weno_limiter_1d_electrons(0,tci,tcm);
        gt::stop("weno_limiter_x");

        gt::start("gauss_law");
        gauss_law();
        gt::stop("gauss_law");

        set_periodic_bc();

        gt::start("advection_v");
        blocks->advection_E(1,tau,E);
        blocks->swap();
        gt::stop("advection_v");

        gt::start("weno_limiter_v");
        blocks->weno_limiter_1d_electrons(1,tci,tcm);
        gt::stop("weno_limiter_v");

        gt::start("bdry_transfer_advection");
        set_zero_inflow_bc(0.5*tau);
        gt::stop("bdry_transfer_advection");

        gt::start("advection_x");
        blocks->advection_v(0,0.5*tau);
        blocks->swap();
        gt::stop("advection_x");
        
        //auto in = blocks->blocks[0]->get_data();
        //visualize_indicator2d_x(time,in.data,e.get_array(),
        //                        a_ele.get_array(), b_ele.get_array(),
        //                        mean_troubled_indicator<1,2,4>);

        gt::start("boundary_transfer_limiter");
        transfer_for_weno_limiter();
        gt::stop("boundary_transfer_limiter");
        gt::start("weno_limiter_x");
        blocks->weno_limiter_1d_electrons(0,tci,tcm);
        gt::stop("weno_limiter_x");

        if(source_func.is_used()){
            gt::start("source");
            blocks->source(0.5*tau, time+0.5*tau, source_func);
            blocks->swap();
            gt::stop("source");
        }

        if(adjust_domain && time>=source_func.start_adjust_domain){
            fp percent_reduce=0.1;
            gt::start("check_reduce_domain");
            array<bool,2> reduce=blocks->check_reduce_domain_bounds(percent_reduce);
            gt::stop("check_reduce_domain");
            gt::start("reduce_domain");
            blocks->reduce_domain_bounds(reduce, percent_reduce,
                                         a_ele, b_ele, a_ion, b_ion);
            gt::stop("reduce_domain");
        }

    }


    void predictor_corrector_timestep(fp tau, fp time){

        // PREDICTOR STEP -----------------------------------------------
        //electric field (at time t^n) computed in write variables
        //right before calling this function

        //copies solution at time t^n which is then the starting point
        //of the corrector step
        gt::start("copy_to_aux");
        blocks->copy_to_aux();
        gt::stop("copy_to_aux");

        if(source_func.is_used()){
            gt::start("source");
            blocks->source(0.5*tau, time, source_func);
            blocks->swap();
            gt::stop("source");
        }

        gt::start("bdry_transfer_advection");
        set_zero_inflow_bc(0.5*tau);
        gt::stop("bdry_transfer_advection");

        gt::start("advection_x");
        blocks->advection_v(0,0.5*tau);
        blocks->swap();
        gt::stop("advection_x");

        gt::start("boundary_transfer_limiter");
        transfer_for_weno_limiter();
        gt::stop("boundary_transfer_limiter");
        gt::start("weno_limiter_x");
        blocks->weno_limiter_1d_electrons(0,tci,tcm);
        gt::stop("weno_limiter_x");

        set_periodic_bc();

        gt::start("advection_v");
        blocks->advection_E(1,0.5*tau,E);
        blocks->swap();
        gt::stop("advection_v");

        gt::start("weno_limiter_v");
        blocks->weno_limiter_1d_electrons(1,tci,tcm);
        gt::stop("weno_limiter_v");

        gt::start("BGK_collision");
        blocks->BGK_collision(0.5*tau,collision_rate_electrons,collision_rate_ions);
        blocks->swap();
        gt::stop("BGK_collision");

        //computes the electric field at time t^(n+1/2)
        gt::start("gauss_law");
        gauss_law();
        gt::stop("gauss_law");


        // CORRECTOR STEP ----------------------------------------------------

        //swaps in (which contains the first order solution at time t^(n+1/2))
        //with auxiliary memory (which is the second order solution at time t^n)
        gt::start("swap_with_aux");
        blocks->swap_with_aux();
        gt::stop("swap_with_aux");

        if(source_func.is_used()){
            gt::start("source");
            blocks->source(0.5*tau, time+0.5*tau, source_func);
            blocks->swap();
            gt::stop("source");
        }

        gt::start("BGK_collision");
        blocks->BGK_collision(0.5*tau,collision_rate_electrons,collision_rate_ions);
        blocks->swap();
        gt::start("BGK_collision");

        gt::start("bdry_transfer_advection");
        set_zero_inflow_bc(0.5*tau);
        gt::stop("bdry_transfer_advection");

        gt::start("advection_x");
        blocks->advection_v(0,0.5*tau);
        blocks->swap();
        gt::stop("advection_x");

        gt::start("boundary_transfer_limiter");
        transfer_for_weno_limiter();
        gt::stop("boundary_transfer_limiter");
        gt::start("weno_limiter_x");
        blocks->weno_limiter_1d_electrons(0,tci,tcm);
        gt::stop("weno_limiter_x");

        set_periodic_bc();

        //E is not computed here as in strang splitting,
        //but at the end of the corrector term

        gt::start("advection_v");
        blocks->advection_E(1,tau,E);
        blocks->swap();
        gt::stop("advection_v");

        gt::start("weno_limiter_v");
        blocks->weno_limiter_1d_electrons(1,tci,tcm);
        gt::stop("weno_limiter_v");

        gt::start("bdry_transfer_advection");
        set_zero_inflow_bc(0.5*tau);
        gt::stop("bdry_transfer_advection");

        gt::start("advection_x");
        blocks->advection_v(0,0.5*tau);
        blocks->swap();
        gt::stop("advection_x");

        gt::start("boundary_transfer_limiter");
        transfer_for_weno_limiter();
        gt::stop("boundary_transfer_limiter");
        gt::start("weno_limiter_x");
        blocks->weno_limiter_1d_electrons(0,tci,tcm);
        gt::stop("weno_limiter_x");

        gt::start("BGK_collision");
        blocks->BGK_collision(0.5*tau,collision_rate_electrons,collision_rate_ions);
        blocks->swap();
        gt::start("BGK_collision");

        if(source_func.is_used()){
            gt::start("source");
            blocks->source(0.5*tau, time+0.5*tau, source_func);
            blocks->swap();
            gt::stop("source");
        }

        //domain adjusted only at the end
        if(adjust_domain && time>=source_func.start_adjust_domain){
            fp percent_reduce=0.1;
            gt::start("check_reduce_domain");
            array<bool,2> reduce=blocks->check_reduce_domain_bounds(percent_reduce);
            gt::stop("check_reduce_domain");
            gt::start("reduce_domain");
            blocks->reduce_domain_bounds(reduce, percent_reduce,
                                         a_ele, b_ele, a_ion, b_ion);
            gt::stop("reduce_domain");
        }

    };

    void set_periodic_bc() {
        // We just set the boundary length to zero
        for(Index i=0;i<blocks->total_blocks;++i)
            blocks->blocks[i]->get_boundary_storage(0);
    }

    void set_zero_inflow_bc(fp tau) {
        // We just set the boundary length for the ions to -1
        // and have to send the boundary data from the electrons if required,
        if(num_ele_domains==1)
            blocks->blocks[0]->get_boundary_storage(-1);
        else
            transfer_data_electrons(tau);
        blocks->blocks[num_ele_domains]->get_boundary_storage(-1);
    }

    fluxes gauss_law(){

        gt::start("reduction_operation");
        fp electron_particle_flux_left;
        fp electron_particle_flux_right;
        fp ion_particle_flux_left;
        fp ion_particle_flux_right;

        //only if the BGK collision term is used, the mean velocity u and
        //the temperature is required
        bool compute_u_and_T = BGK_collision_term;

        rho = blocks->compute_rho_invariants_fluxes(
                                               electron_particle_flux_left,
                                               electron_particle_flux_right,
                                               ion_particle_flux_left,
                                               ion_particle_flux_right,
                                               compute_u_and_T);
        gt::stop("reduction_operation");

        dg_evaluator dg_interpolator(e[d]);
        fp h_ions = (b_ion[0]-a_ion[0])/fp(e[0]);
        
        vector<fp> a,b,h;
        get_hab(h,a,b);

        //interpolates the ion_density to the fine grid of the electrons
        Index ox = e[d];
        domain_x& ion_charge = rho[num_ele_domains];
        size_t local_dof_x = e[0]*e[d];
        for(int k=0;k<num_ele_domains;++k){
            for(Index i=0;i<e[0];++i){
                for(Index j=0;j<ox;++j){

                    Index idx = j+i*ox + k*local_dof_x;

                    fp x = a[k] + h[k]*(i + gauss::x_scaled01(j,ox));
                    
                    int ii = (x-a_ion[0])/h_ions;

                    vector<fp> loc_ion_charge(ox);
                    for(Index jj=0;jj<ox;++jj)
                        loc_ion_charge[jj] = ion_charge.v[jj + ox*ii];

                    x = (x - a_ion[0])/h_ions - ii;

                    fp ion_charge_at_x = 
                        dg_interpolator.eval(x,loc_ion_charge.data());

                    charge_density.v[idx] = ion_charge_at_x - rho[k].v[j+i*ox];
                    charge_density_write.v[idx] = charge_density.v[idx];
                }
            }
        }

        gt::start("solve_poisson");
        poisson->solve(E_global,charge_density);
        gt::stop("solve_poisson");

        for(size_t k=0;k<num_ele_domains;++k){
            for(size_t l=0;l<local_dof_x;++l)
                E[k][0].v[l] = E_global[0].v[l + k*local_dof_x];
        }

        for(Index i=0;i<e[0];++i){
            for(Index j=0;j<ox;++j){

                Index idx = j+i*ox;

                fp x = a_ion[0] + h_ions*(i + gauss::x_scaled01(j,ox));

                int k;
                if(x<b[0])
                    k = 0;
                else if(x<b[1])
                    k = 1;
                else
                    k = 2;

                int ii = (x-a[k])/h[k];

                vector<fp> loc_E(ox);
                for(Index jj=0;jj<ox;++jj)
                    loc_E[jj] = E_global[0].v[jj + ox*(ii+k*e[0])];

                x = (x-a[k])/h[k] - ii;
                E[num_ele_domains][0].v[idx] = dg_interpolator.eval(x,loc_E.data());
            }
        }

        return {electron_particle_flux_right, ion_particle_flux_right};
    }


    void transfer_data_electrons(fp tau){

        bdr_len_info info_l = blocks->blocks[0]->pack_boundary_fine2coarse(
                                                 tau, bdr_max_length);
        bdr_len_info info_m = blocks->blocks[1]->pack_boundary(0,tau,bdr_max_length);
        bdr_len_info info_r = blocks->blocks[2]->pack_boundary_fine2coarse(
                                                          tau, bdr_max_length); 

        Index bdr_len = info_m.bdr_len;
        Index bdr_tot_l = info_m.bdr_tot_left;
        Index bdr_tot_len = info_m.bdr_tot_len;
        Index bdr_tot_r = bdr_tot_len-bdr_tot_l;

        cout << "bdr_len: " << bdr_len << endl;

        fp* snd_from_left  = blocks->blocks[0]->get_boundary_to_send();
        fp* snd_from_mid   = blocks->blocks[1]->get_boundary_to_send();
        fp* snd_from_right = blocks->blocks[2]->get_boundary_to_send();

        int ratio = bdry_cell_ratios[0];
        if(ratio!=bdry_cell_ratios[2]){
            cout << "ERROR: ratio left != ratio right.\n";
            exit(1);
        }
        size_t memory_shift=0;
        if(ratio>1){
            memory_shift = ratio*bdr_max_length;
        }

        fp* rcv_on_left  = blocks->blocks[0]->get_boundary_storage(bdr_len*ratio);
        fp* rcv_on_mid   = blocks->blocks[1]->get_boundary_storage(bdr_len);
        fp* rcv_on_right = blocks->blocks[2]->get_boundary_storage(bdr_len*ratio);
        rcv_on_left  += memory_shift;
        rcv_on_right += memory_shift;

        if(gpu){
            #ifdef __CUDACC__
            cudaDeviceSynchronize();
            gpuErrchk(cudaPeekAtLastError());

            //we are on the same node, so the data is transfered using CUDA

            cudaMemcpy(rcv_on_left,snd_from_mid,
                       bdr_tot_l*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaMemcpy(rcv_on_left+bdr_tot_l,snd_from_right+bdr_tot_l,
                       bdr_tot_r*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaMemcpy(rcv_on_mid,snd_from_right,
                       bdr_tot_l*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaMemcpy(rcv_on_mid+bdr_tot_l,snd_from_left+bdr_tot_l,
                       bdr_tot_r*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaMemcpy(rcv_on_right,snd_from_left,
                       bdr_tot_l*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaMemcpy(rcv_on_right+bdr_tot_l,snd_from_mid+bdr_tot_l,
                       bdr_tot_r*sizeof(fp),cudaMemcpyDeviceToDevice);

            cudaDeviceSynchronize();
            gpuErrchk(cudaPeekAtLastError());
            #endif

        } else{
            //transfer on the same node using 1 MPI process is just a simple copy
            copy(snd_from_mid,snd_from_mid+bdr_tot_l,rcv_on_left);

            copy(snd_from_right+bdr_tot_l,snd_from_right+bdr_tot_len,
                 rcv_on_left+bdr_tot_l);

            copy(snd_from_right,snd_from_right+bdr_tot_l,rcv_on_mid);

            copy(snd_from_left+bdr_tot_l,snd_from_left+bdr_tot_len,
                 rcv_on_mid+bdr_tot_l);

            copy(snd_from_left,snd_from_left+bdr_tot_l,rcv_on_right);
            
            copy(snd_from_mid+bdr_tot_l,snd_from_mid+bdr_tot_len,
                 rcv_on_right+bdr_tot_l);
        }

        if(ratio>1){
            blocks->blocks[0]->unpack_boundary_coarse2fine(bdr_max_length);
            blocks->blocks[2]->unpack_boundary_coarse2fine(bdr_max_length);
        }
    }

    void transfer_for_weno_limiter(){

        if(tci==troubled_cell_indicator::none||
           tcm==troubled_cell_modifier::none||
           num_ele_domains==1){
            //nothing as no weno limiter is used.
        } else {
            if(!gpu){
                cout << "ERROR: weno limiter in the parallel case not yet "
                     << "implemented using CPUs.\n";
                exit(1);
            } else {
                #ifdef __CUDACC__ 
                vector<weno_bdry_transfer_info> info(num_ele_domains);
                timer t_pack_weno, t_transfer_weno;

                t_pack_weno.start();
                for(int i=0;i<num_ele_domains;++i)
                    info[i] = blocks->blocks[i]->pack_for_weno_limiter(
                                                         bdry_cell_ratios[0]);
                t_pack_weno.stop();

                t_transfer_weno.start();
                size_t size = info[0].size;
                gpuErrchk(cudaMemcpy(info[0].bdry_right_rcv, info[1].bdry_left_snd,
                                     size, cudaMemcpyDeviceToDevice));
                gpuErrchk(cudaMemcpy(info[1].bdry_left_rcv, info[0].bdry_right_snd,
                                     size, cudaMemcpyDeviceToDevice));
                gpuErrchk(cudaMemcpy(info[1].bdry_right_rcv, info[2].bdry_left_snd,
                                     size, cudaMemcpyDeviceToDevice));
                gpuErrchk(cudaMemcpy(info[2].bdry_left_rcv, info[1].bdry_right_snd,
                                     size, cudaMemcpyDeviceToDevice));
                t_transfer_weno.stop();

                cout << "pack for weno: " << t_pack_weno.total() << ", "
                     << "transfer for weno: " << t_transfer_weno.total() << ", "
                     << "size: " << size << endl;
                #endif
            }
        }
    }

    void write_variables(fp t, string filename){

        gt::start("gauss_law");
        fluxes fl = gauss_law(); 
        gt::stop("gauss_law");
        if(enable_output){
            std::ofstream fs(filename.c_str(), std::ofstream::app);
            fs.precision(16);
            fs << t << " " << fl.ele_flux_right << " " << fl.ion_flux_right << " "
               << b_ele[1] << " " << b_ion[1] << endl;
            fs.close();
        }
    }

    void write_charge_densities(fp t){

        vector<fp> a,b,h;
        get_hab(h,a,b);

        Index N = e[0];
        Index ox = e[dx+dv];
        std::ofstream tot_c("data_tot_c"+std::to_string(t)+".data");
        std::ofstream ele_c("data_ele_c"+std::to_string(t)+".data");
        for(size_t k=0;k<num_ele_domains;++k){
            for(Index i=0;i<N;++i){
                for(Index j=0;j<ox;++j){

                    fp x = a[k] + h[k]*(i+gauss::x_scaled01(j,ox));

                    tot_c << x << " "
                          << charge_density_write.v[j+ox*(i+k*N)] << endl;
                    ele_c << x << " " 
                          << rho[k].v[j+ox*i] << endl;
                }
            }
        }
 
        std::ofstream ion_c("data_ion_c"+std::to_string(t)+".data");
        fp h_ions = (b_ion[0]-a_ion[0])/fp(N);
        for(Index i=0;i<N;++i){
            for(Index j=0;j<ox;++j){

                fp x = a_ion[0] + h_ions*(i+gauss::x_scaled01(j,ox));

                Index lidx = j+ox*i;
                ion_c << x << " " << rho[num_ele_domains].v[lidx] << endl;
            }
        }
 
    }

    void write_densities(fp t){

        size_t Nx = e[0];
        size_t Nv = e[1];
        size_t ox = e[2];
        size_t ov = e[3];

        auto data_ions = blocks->blocks[num_ele_domains]->get_data();
        std::ofstream ion_d("data_ion_d"+std::to_string(t)+".data");
        
        double hx_ions = (b_ion[0]-a_ion[0])/double(Nx);
        double hv_ions = (b_ion[1]-a_ion[1])/double(Nv);
        for(size_t j=0;j<Nv;++j){
            for(size_t jj=0;jj<ov;++jj){
                for(size_t i=0;i<Nx;++i){
                    for(size_t ii=0;ii<ox;++ii){

                        size_t lidx = ii+ox*(i+Nx*(jj+ov*j));

                        double x = a_ion[0] + 
                                   hx_ions*(i+gauss::x_scaled01(ii,ox));
                        double v = a_ion[1] + 
                                   hv_ions*(j+gauss::x_scaled01(jj,ov));

                        ion_d << x << " " << v << " " 
                              << data_ions.data.v[lidx] << endl;
                        if(ii==ox-1 && i==Nx-1)
                            ion_d << endl;
                    }
                }
            }
        }

        vector<domain<d>> data_ele;
        for(int i=0;i<num_ele_domains;++i)
            data_ele.push_back(blocks->blocks[i]->get_data());

        std::ofstream ele_d("data_ele_d"+std::to_string(t)+".data");

        vector<fp> h,a,b;
        get_hab(h,a,b);

        double hv_ele = (b_ele[1]-a_ele[1])/double(Nv);
        for(size_t j=0;j<Nv;++j){
            for(size_t jj=0;jj<ov;++jj){
                for(size_t k=0;k<num_ele_domains;++k){
                    for(size_t i=0;i<Nx;++i){
                        for(size_t ii=0;ii<ox;++ii){

                            size_t lidx = ii+ox*(i+Nx*(jj+ov*j));

                            fp x = a[k] + h[k]*(i+gauss::x_scaled01(ii,ox));
                            fp v = a_ele[1] + hv_ele*(j+gauss::x_scaled01(jj,ov));

                            fp density_value = data_ele[k].data.v[lidx];

                            ele_d << x << " " << v << " " 
                                  << density_value << endl;

                            if(ii==ox-1 && i==Nx-1 && k==num_ele_domains-1)
                                ele_d << endl;
                        }
                    }
                }
            }
        }
    }

    void write_electric_field_and_potential(fp t){

        vector<fp> a,b,h;
        get_hab(h,a,b);

        Index N = e[0];
        Index ox = e[dx+dv];
        std::ofstream ef_global("data_efield"+std::to_string(t)+".data");
        std::ofstream epot_global("data_epotential"+std::to_string(t)+".data");
        for(int k=0;k<num_ele_domains;++k){
            for(Index i=0;i<N;++i){
                for(Index j=0;j<ox;++j){
      
                    fp x = a[k] + h[k]*(i+gauss::x_scaled01(j,ox));

                    Index lidx = j+ox*(i+k*N);
                    ef_global << x << " " << E_global[0].v[lidx] << endl;
                    epot_global << x << " " << charge_density.v[lidx] << endl;
                }
            }
        }
 
        std::ofstream efield_ions("data_efield_ions"+std::to_string(t)+".data");
        fp h_ions = (b_ion[0]-a_ion[0])/fp(N);
        for(Index i=0;i<N;++i){
            for(Index j=0;j<ox;++j){
      
                fp x = a_ion[0] + h_ions*(i+gauss::x_scaled01(j,ox));

                Index lidx = j+ox*i;
                efield_ions << x << " " << E[num_ele_domains][0].v[lidx] << endl;
            }
        }
    }

    void get_hab(vector<fp>& h, vector<fp>& a, vector<fp>& b) {

        h.resize(num_ele_domains);
        a.resize(num_ele_domains);
        b.resize(num_ele_domains);

        fp nd = (num_ele_domains==1) ? 0.0 : 1.0;
        fp hinterior = (b_ele[0]-a_ele[0])/
                       (fp(e[0])*(1.0+nd*2.0/fp(bdry_cell_ratios[0])));
        for(int i=0;i<num_ele_domains;++i)
            h[i] = hinterior/fp(bdry_cell_ratios[i]);

        a[0] = a_ele[0];
        for(int i=1;i<num_ele_domains;++i){
            a[i] = a[i-1]+e[0]*h[i-1];
            b[i-1] = a[i];
        }
        b[num_ele_domains-1] = b_ele[0];

    }

  private:

    void enable_peer_to_peer() {
        #ifdef __CUDACC__
        int device_count;
        cudaGetDeviceCount(&device_count);

        for(int i=0;i<device_count;i++) {
            cudaSetDevice(i);
            for(int j=0;j<device_count;j++) {
                int is_able;
                cudaDeviceCanAccessPeer(&is_able, i, j);
                cout << "device i=" << i << " j=" << j
                     << " able-peer-to-peer=" << is_able << endl;
                if(is_able) // automatically excludes i=j
                    gpuErrchk( cudaDeviceEnablePeerAccess(j, 0) );
            }
        }

        cudaSetDevice(0);
        #endif
    }

};

