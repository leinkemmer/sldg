#pragma once

#include <generic/common.hpp>
#include <container/domain.hpp>
#include <programs/vp-block.hpp>
#include <algorithms/dg.hpp>
#include <algorithms/rho.hpp>

#include <algorithms/adjust_domain.hpp>
#include <algorithms/limiter.hpp>


#ifdef _OPENMP
#include <omp.h>
#endif

template<size_t dx, size_t dv, fp (*f0)(fp*)>
struct vp_block_cpu : vp_block<dx,dv,f0> {
    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;
    
    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;
    using domain_x = multi_array<fp,2*dx>;
    using efield   = array< domain_x, dx>;

    vec_d a, b;
    index_no e;

    array<multi_array<fp,2*dv>,dv> v_values;

    Index bdr_len; // updated in get_boundary

    vector<fp> bdr_data;
    vector<fp> bdr_to_send;

    vector<int> sign_v;
    vector<int> sign_E;
    int max_bdry_left;

    domain<d> in, out;

    vector<double> tau_used;
    vector<bool> domain_changed;

    vector< unique_ptr<dg_coeff_generic<dv>> > dg_c_x;
    vector< unique_ptr<dg_coeff_generic<dx>> > dg_c_v;

    domain_x rho;
    domain_x meanvelocity;
    domain_x temperature;
    location loc;

    vp_block_cpu(vec_d _a, vec_d _b, index_no _e, Index bdr_max_length,
                 bool _sldg_limiter=false, int _bdry_cell_ratio=1,
                 location _loc=location::all, fp additional_scalar_speed_x=1.0,
                 fp additional_scalar_speed_v=1.0) 
        : in(_e.get_array(), _a.get_array(), _b.get_array()),
          out(_e.get_array(), _a.get_array(), _b.get_array()),
          tau_used(dx), dg_c_x(dx), dg_c_v(dv), 
          sldg_limiter(_sldg_limiter), bdry_cell_ratio(_bdry_cell_ratio),
          loc(_loc)
    {
        a=_a; b=_b; e=_e;

        init_v_values(a,b,e.get_array(),v_values);

        int max_threads=1;
        #ifdef _OPENMP
        max_threads = omp_get_max_threads();
        #endif
        rho_reduction.resize(max_threads);
        meanvelocity_reduction.resize(max_threads);
        temperature_reduction.resize(max_threads);
        for(int i=0;i<max_threads;i++){
            rho_reduction[i].resize(e.x_part());
            meanvelocity_reduction[i].resize(e.x_part());
            temperature_reduction[i].resize(e.x_part());
        }
        rho.resize(e.x_part());
        meanvelocity.resize(e.x_part());
        temperature.resize(e.x_part());

        int required_memory=1;
        if(bdry_cell_ratio>1)
            required_memory = bdry_cell_ratio+1;
        bdr_data.resize(bdr_max_length*required_memory);

        bdr_len = 0;
        
        fill(begin(tau_used), end(tau_used), -1.0);
        domain_changed.resize(dv);
        fill(begin(domain_changed), end(domain_changed), false);

        for(size_t dim=0;dim<dx;dim++) {
            Index N = prod(e.v_part());
            dg_c_x[dim] = make_dg_coeff_gpu<dv>(e[d+dim], N,
                                                additional_scalar_speed_x);
        }
        
        for(size_t dim=0;dim<dv;dim++) {
            Index N = prod(e.x_part());
            dg_c_v[dim] = make_dg_coeff_gpu<dx>(e[d+dx+dim], N,
                                                additional_scalar_speed_v);
        }
        
        sign_v.resize(prod(e.v_part()));
        sign_E.resize(prod(e.x_part()));

        bdr_to_send.resize(bdr_max_length);
        assert(bdry_cell_ratio > 0);
    }

    // Initialization with first touch
    void init() {
        init_firsttouch(f0,in);
    }

    void advection_v(int dim, fp tau) {
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        
        gt::start("dg_c_x");
        if(domain_changed[dim] || tau_used[dim] != tau || tau_used[dim] <= 0.0){

            if(domain_changed[dim]==true){
                update_v_values<dv>(a[dx+dim],b[dx+dim],e.v_part(),
                                    v_values[dim],dim);
                domain_changed[dim]=false;
            }

            dg_c_x[dim]->compute(tau, dd, v_values[dim]);
            tau_used[dim] = tau;
        }
        gt::stop("dg_c_x");

        advection<dv>(dim,in,out,dg_c_x[dim].get(),sign_v,max_bdry_left);
    }

    void advection_E(int dim, fp tau, efield& E) {
        fp dd=(b[dim]-a[dim])/fp(e[dim]);

        gt::start("dg_c_v");
        dg_c_v[dim-dx]->compute(tau, dd, E[dim-dx]);
        gt::stop("dg_c_v");

        advection<dx>(dim,in,out,dg_c_v[dim-dx].get(),sign_E,max_bdry_left);
    }

    void swap() {
        in.data.swap(out.data);
    }

    void source(fp dt, fp t, source_f<dx,dv> f){
        source_kernel(dt,t,f);
    }

    bdr_len_info pack_boundary(Index dim, fp tau, multi_array<fp,2*dx>& F,
                               Index bdr_max_length) {

        gt::start("pack_boundary");

        assert(dim >= Index(dx));

        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp alpha_max = tau*max_element_abs(F)/dd;
        Index bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);
        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: bdr_tot_len > bdr_max_length" << endl;
            exit(1);
        }

        max_bdry_left = compute_sign<dx>(sign_E,F);

        pack_bdry(dim,in,bdr_len,&bdr_to_send[0],sign_E,max_bdry_left);

        Index bdr_tot_left = o*bdr_len*max_bdry_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        gt::stop("pack_boundary");
        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    bdr_len_info pack_boundary(Index dim, fp tau, Index bdr_max_length) {
        //The following does not work when dx!=dv        
        //return pack_boundary(dim, tau, v_values[dim], bdr_to);

        assert(dim < Index(dx));

        gt::start("pack_boundary");
        multi_array<fp,2*dv>& F = v_values[dim];
        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp max_element = max(fabs(*F.begin()),fabs(*(F.end()-1)));
        fp alpha_max = tau*max_element/dd;
        Index bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);
        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: bdr_tot_len > bdr_max_length" << endl;
            exit(1);
        }
              
        max_bdry_left = compute_sign<dv>(sign_v,F);

        pack_bdry(dim,in,bdr_len,&bdr_to_send[0],sign_v,max_bdry_left);

        Index bdr_tot_left = o*bdr_len*max_bdry_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        gt::stop("pack_boundary");
        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    //collects projection of fine grid to coarse grid in 
    //order to reduce the amount of data to transfer
    bdr_len_info pack_boundary_fine2coarse(fp tau, Index bdr_max_length) {
        
        constexpr Index dim = 0; 

        assert(dim < Index(dx));

        gt::start("pack_boundary");
        multi_array<fp,2*dv>& F = v_values[dim];
        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp max_element = max(fabs(*F.begin()),fabs(*(F.end()-1)));
        fp alpha_max = tau*max_element/(dd*bdry_cell_ratio);
        Index bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);
        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: bdr_tot_len > bdr_max_length" << endl;
            exit(1);
        }
              
        max_bdry_left = compute_sign<dv>(sign_v,F);

        pack_bdry_f2c(dim,in,bdr_len,&bdr_to_send[0],sign_v,max_bdry_left,bdry_cell_ratio,loc);

        Index bdr_tot_left = o*bdr_len*max_bdry_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        gt::stop("pack_boundary");
        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    void unpack_boundary_coarse2fine(Index bdr_max_length){
        //the following is not required as this function has to be called
        //after a pack routine which has to compute the sign
        multi_array<fp,2*dv>& F = v_values[0];
        max_bdry_left = compute_sign<dv>(sign_v,F);
        int bdr_len_coarse = bdr_len/bdry_cell_ratio;
        unpack_bdry_c2f(0,bdr_max_length,sign_v,bdr_len_coarse);
    }

    fp* get_boundary_storage(Index _bdr_len) {
        bdr_len = _bdr_len;
        return &bdr_data[0];
    }

    fp* get_boundary_to_send(){
        return &bdr_to_send[0];
    }

    domain_x compute_rho() {
        // compute rho on each OpenMP thread
        compute_rho_omp<dx,dv>(in, rho_reduction);

        // Perform the integration in v-space
        fill(rho.data(), rho.data()+rho.num_elements(), 0.0);

        // Now the actual reduction
        int max_threads=1;
        #ifdef _OPENMP
        max_threads = omp_get_max_threads();
        #endif
        for(int i=0;i<max_threads;i++)
            for(Index j=0;j<rho.num_elements();j++)
                rho.v[j] += rho_reduction[i].v[j];

        return rho;
    }

    domain_x compute_rho_and_invariants(fp& mass, fp& momentum, fp& l1,
            fp& l2, fp& K_energy, fp& entropy) {

        fp dummy;
        compute_invariants_and_rho_omp<dx,dv>(in, rho_reduction, mass, momentum,
                l1, l2, K_energy, entropy, dummy, dummy, dummy, dummy);

        fill(rho.data(), rho.data()+rho.num_elements(), 0.0);

        // Now the actual reduction
        int max_threads=1;
        #ifdef _OPENMP
        max_threads = omp_get_max_threads();
        #endif
        for(int i=0;i<max_threads;i++)
            for(Index j=0;j<rho.num_elements();j++)
                rho.v[j] += rho_reduction[i].v[j];

        return rho;
    }

    domain_x compute_rho_invariants_fluxes(fp& mass, fp& momentum, fp& l1,
            fp& l2, fp& K_energy, fp& entropy,
            fp& particle_flux_left, fp& particle_flux_right,
            fp& energy_flux_left, fp& energy_flux_right,
            bool compute_meanvelocity_and_temperature) {

        compute_invariants_and_rho_omp<dx,dv>(in, rho_reduction, mass, 
                                       momentum, l1, l2, K_energy, entropy, 
                                       particle_flux_left, particle_flux_right,
                                       energy_flux_left, energy_flux_right);

        int max_threads=1;
        #ifdef _OPENMP
        max_threads = omp_get_max_threads();
        #endif
        for(int i=1;i<max_threads;i++)
            for(Index j=0;j<rho.num_elements();j++)
                rho_reduction[0].v[j] += rho_reduction[i].v[j];

        if(compute_meanvelocity_and_temperature){

            compute_mean_velocity<dx,dv>(in, rho_reduction, 
                                         meanvelocity_reduction);

            for(int i=1;i<max_threads;i++)
                for(Index j=0;j<rho.num_elements();j++)
                    meanvelocity_reduction[0].v[j] += meanvelocity_reduction[i].v[j];

            compute_temperature<dx,dv>(in, rho_reduction,
                                       meanvelocity_reduction,
                                       temperature_reduction);

            for(int i=1;i<max_threads;i++)
                for(Index j=0;j<rho.num_elements();j++)
                    temperature_reduction[0].v[j] += temperature_reduction[i].v[j];

            meanvelocity = meanvelocity_reduction[0];
            temperature = temperature_reduction[0];

        }

        rho = rho_reduction[0];

        return rho;
    }

    void collision_BGK(fp dt, fp nu){

        array<fp,dx+dv> h;
        for(size_t i=0;i<dx+dv;++i)
            h[i] = (b[i]-a[i])/fp(e[i]);

        //TODO: parallelize with OpenMP
        BGK_cpu<dx,dv>(out.data, in.data, in.data, dt, nu, rho, 
                       meanvelocity, temperature, a.get_array(), h, e.get_array());
    }

    void collision_BGK(fp dt, fp nu, multi_array<fp,2*(dx+dv)>& fhalf){

        array<fp,dx+dv> h;
        for(size_t i=0;i<dx+dv;++i)
            h[i] = (b[i]-a[i])/fp(e[i]);

        //TODO: parallelize with OpenMP
        BGK_cpu<dx,dv>(out.data, in.data, fhalf, dt, nu, rho, 
                       meanvelocity, temperature, a.get_array(), h, e.get_array());
    }


    domain<d>& get_data() {
        return in;
    }

    bool check_reduce_domain_bounds(fp percent_reduce){
        
        if(dv!=1){
            cout << "ERROR: currently not implemented for dv>1\n";
            cout << endl; 
        }

        fp tol = 1e-14;

        //percent reduce*0.5 since domain is made smaller on both ends
        //addition of e[1]*0.05 adds buffer of 5% to ensure f (and its
        //derivatives) are zero at the end of the domain to avoid 
        //undesired effects of the advection in the velocity domain
        int cells_lost = e[1]*percent_reduce*0.5 + e[1]*0.05;
        Index i_left = cells_lost;
        Index i_right = e[1]-cells_lost;

        Index ov = e[d+dx];
        Index dof_x = e[0]*e[d];

        fp abs_l = check_max_abs_v<dx,dv>(i_left*ov,dof_x,in.data);
        fp abs_r = check_max_abs_v<dx,dv>(i_right*ov-1,dof_x,in.data);

        return (abs_l<tol && abs_r<tol);
    }

    void reduce_domain_bounds(fp percent_reduce){
        
        if(dv!=1){
            cout << "ERROR: currently not implemented for dv>1\n";
            cout << endl; 
        }


        //percent reduce*0.5 since domain is made smaller on both ends
        //addition of e[1]*0.05 adds buffer of 5% to ensure f (and its
        //derivatives) are zero at the end of the domain to avoid 
        //undesired effects of the advection in the velocity domain
        int cells_lost = e[1]*percent_reduce*0.5 + e[1]*0.05;
        Index i_left = cells_lost;
        Index i_right = e[1]-cells_lost;

        fp a_old = a[1];
        fp b_old = b[1];
        fp a_new = a_old*( 1.0 - percent_reduce );
        fp b_new = b_old*( 1.0 - percent_reduce );

        cout << "reduce domain bounds: "  
             << "\ni_left: " << i_left << ", i_right: " << i_right 
             << "\npercent: " << percent_reduce 
             << "\na_new: " << a_new << ", b_new: " << b_new
             << "\na_old: " << a_old << ", b_old: " << b_old << "\n\n";

        project_on_finer_grid<dx,dv>({a_old},{b_old},{a_new},{b_new},
                              e.get_array(), 1.0-percent_reduce, 
                              in.data, out.data);

        a[1] = a_new;
        b[1] = b_new;
        in.a[1] = a_new;
        in.b[1] = b_new;
        out.a[1] = a_new;
        out.b[1] = b_new;

        domain_changed[0] = true;
    }

    void weno_limiter_1d(int dim, troubled_cell_indicator tci,
                         troubled_cell_modifier tcm){

        Index o = e[d+dim];

        weno_limiter_1d_k<d>(o,dim,in.data,out.data,
                             e.get_array(),tci,tcm);

    }

    weno_bdry_transfer_info pack_for_weno_limiter(int grid_size_ratio){
        cout << "ERROR: pack_for_weno_limiter not yet implemented on the cpu.\n";
        exit(1);
        weno_bdry_transfer_info wbti;
        wbti.size = grid_size_ratio;
        return wbti;
    }

    void swap_data(multi_array<fp,2*(dx+dv)>& f){
        in.data.swap(f);
    }

    void copy_data(multi_array<fp,2*(dx+dv)>& f){
        f = in.data;
    }

private:
    vector< domain_x > rho_reduction;
    vector< domain_x > meanvelocity_reduction;
    vector< domain_x > temperature_reduction;
    bool sldg_limiter;
    int bdry_cell_ratio;

    template<int dim>
    void run_kernel(Index o, vec3 gdim, int tile_x, domain<d>& in, domain<d>& out,
            fp* pA, fp* pB, int* pshift, vector<int>& sign, int max_bdr_left) {
        if(o==1)
            run_kernel_o<dim,1>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else if(o==2)
            run_kernel_o<dim,2>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else if(o==3)
            run_kernel_o<dim,3>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else if(o==4)
            run_kernel_o<dim,4>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else if(o==5)
            run_kernel_o<dim,5>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else if(o==6)
            run_kernel_o<dim,6>(gdim, tile_x, in, out, pA, pB, pshift,
                                sign, max_bdr_left);
        else {
            cout << "ERROR: only o=1,2,3,4,5,6 currently implemented" << endl;
            exit(1);
        }
    }

    template<int dim, Index o>
    void run_kernel_o(vec3 gdim, int tile_x, domain<d>& in, domain<d>& out,
            fp* pA, fp* pB, int* pshift, vector<int>& sign, int max_bdr_left) {

        vec3 gd( ( (d<6) ? gdim[0] : gdim[0]*gdim[1]*gdim[2] ), 
                 ( (d<6) ? gdim[1] : 1 ), ( (d<6) ? gdim[2] : 1 ) );
        vec3 bd(tile_x,e[dim],1);
        if(d==6 && dim==0){
            bd.z = bd.x;
            bd.x = 1;
        }

        #pragma omp parallel for schedule(static)
        for(int i_omp=0;i_omp<gdim[2];i_omp++) {
            for(int iy=0;iy<gdim[1];iy++) {
                for(int ix=0;ix<gdim[0];ix++) {
                    vec3 bid( ( (d==6) ? ix + iy*gdim[0] + i_omp*gdim[0]*gdim[1] : ix ), ( (d==6) ? 0 : iy ), ( (d==6) ? 0 : i_omp) );

                    for(int i=0;i<e[dim];i++)
                        for(int it=0;it<tile_x;it++){
                                vec3 tid(it,i,0);
                                if(d==6 && dim==0){
                                    tid.x = 0;
                                    tid.z = it;
                                }
                                translate<dx,dv,dim,o>(
                                    in.data.data(), out.data.data(),
                                    pA, pB, pshift, e[0]*e[d+0],
                                    tid, bid, bd, gd, 
                                    nullptr, nullptr, &e[0], bdr_len, 
                                    &bdr_data[0], &sign[0], max_bdr_left,
                                    sldg_limiter);
                        }
                }
            }
        }
    }


    // assumes the communication has already happened
    template<size_t d_adv>
    void advection(size_t dim, domain<d>& in, domain<d>& out,
                   dg_coeff_generic<d_adv>* dg_c, vector<int>& sign,
                   int max_bdr_left) {

        int tile_x  = (dim==0) ? ((d==6) ? 4 : 1) : 16; //tile_x[1] != 1 !!!!
        array<Index,3> b = create_grid<d>(dim, e.get_array(), tile_x);
        vec3 gdim(b[0],b[1],b[2]);

        if(dim >= d) {
            cout << "ERROR: dim>=d in advection() is not valid" << endl;
            exit(1);
        }
        if(e[d+dim]!=4 && sldg_limiter==true)
            cout << "WARNING: sldg projection limiter only implemented for o=4\n";

        Index o = e[d+dim];

        gt::start(str(boost::format("advection_dim%1%")%dim));
        if(dim == 0)
            run_kernel<0>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else if(dim == 1)
            run_kernel<1>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else if(dim == 2)
            run_kernel<2>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else if(dim == 3)
            run_kernel<3>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else if(dim == 4)
            run_kernel<4>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else if(dim == 5)
            run_kernel<5>(o, gdim, tile_x, in, out, dg_c->p_A(), dg_c->p_B(),
                    dg_c->p_shift(), sign, max_bdr_left);
        else {
            cout << "ERROR: only dim<=6 is implemented" << endl;
            exit(1);
        }
        gt::stop(str(boost::format("advection_dim%1%")%dim));
    }
    
    template<size_t dim>
    void pack_boundary_kernel(fp* in, Index bdr_len, fp* bdry_to, 
                              vector<int>& psign, int bdry_max_left) {
        
        constexpr size_t d = dx+dv;
        
        int off[d];
        size_t stride_in=1;
        for(size_t i=0;i<d;i++){
            off[i]=e[d+i]*e[i];

            if(i<dim)
                stride_in *= off[i];
        }

        array<Index,2*d> max=e.get_array();

        int n=e[dim];
        int o=e[d+dim];

        Index single_out=dx+dv-1;
        if(dim == dx+dv-1)
            single_out = dx+dv-2;

        max[dim]   = bdr_len;
        max[d+dim] = o;
        max[single_out] = 1;

        #pragma omp parallel for schedule(static)
        for(Index k=0;k<e[single_out];k++)
        for(auto idx : range<2*d>(max)) {
            idx[single_out] = k;

            int i[d];
            for(size_t k=0;k<d;++k)
                if(k!=dim)
                    i[k] = idx[d+k] + e[d+k]*idx[k];

            int offset_s, bdry_offset_dim, bdry_stride_dim;
            size_t offset_in = get_bdry_info<dx,dv,dim>(i, off, offset_s,
                                       bdry_offset_dim, bdry_stride_dim);

            fp* p_in = in + offset_in;

            int sign = psign[offset_s];

            fp* local_bdry = bdry_to + bdr_len*o*bdry_offset_dim;
            if(sign < 0) {
                local_bdry += bdr_len*o*bdry_stride_dim*(abs(sign)-1);
                for(int k=0;k<o;++k)
                    local_bdry[o*idx[dim]+k] = p_in[(o*idx[dim]+k)*stride_in];
            } else {
                local_bdry += bdr_len*o*bdry_stride_dim*(sign-1 + bdry_max_left);
                for(int k=0;k<o;++k)
                    local_bdry[o*idx[dim]+k] = p_in[(o*(n-1-idx[dim])+k)*stride_in];
            }

            idx[single_out] = 0;
        }
    }

    void pack_bdry(size_t dim, domain<d>& in, Index bdr_len, 
                   fp* bdry_to, vector<int>& psign, int bdry_max_left) {

        gt::start(str(boost::format("pack_bdry_cpu_dim%1%")%dim));
        if(dim == 0)
            pack_boundary_kernel<0>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else if(dim == 1)
            pack_boundary_kernel<1>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else if(dim == 2)
            pack_boundary_kernel<2>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else if(dim == 3)
            pack_boundary_kernel<3>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else if(dim == 4)
            pack_boundary_kernel<4>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else if(dim == 5)
            pack_boundary_kernel<5>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left);
        else {
            cout << "ERROR: only dim<=6 is implemented" << endl;
            exit(1);
        }
        gt::stop(str(boost::format("pack_bdry_cpu_dim%1%")%dim));
    }
    
    template<size_t d_adv>
    int compute_sign(vector<int>& sign, multi_array<fp,2*d_adv>& F) {        

        int idx_left=0;
        int idx_right=0;
        array<Index,2*d_adv> e = F.shape();
        for(auto i : range<2*d_adv>(e)) {
            Index offset
                = linearize_idx(to_memorder<d_adv>(i),to_memorder<d_adv>(e));
            sign[offset] = (F(i) >= 0.0) ? ++idx_right : --idx_left;
        }
        return abs(idx_left);
    }

    template<size_t dim>
    void pack_boundary_f2c_kernel(fp* in, Index bdr_len, fp* bdry_to, 
                                  vector<int>& psign, int bdry_max_left,
                                  int ratio, location const lr) {
        
        constexpr size_t d = dx+dv;
        
        int off[d];
        size_t stride_in=1;
        for(size_t i=0;i<d;i++){
            off[i]=e[d+i]*e[i];

            if(i<dim)
                stride_in *= off[i];
        }

        array<Index,2*d> max=e.get_array();

        int n=e[dim];
        int o=e[d+dim];

        Index single_out=dx+dv-1;
        if(dim == dx+dv-1)
            single_out = dx+dv-2;

        max[dim]   = bdr_len;
        max[d+dim] = o;
        max[single_out] = 1;

        #pragma omp parallel for schedule(static)
        for(Index k=0;k<e[single_out];k++)
        for(auto idx : range<2*d>(max)) {
            idx[single_out] = k;


            int i[d];
            for(size_t k=0;k<d;++k)
                if(k!=dim)
                    i[k] = idx[d+k] + e[d+k]*idx[k];

            int offset_s, bdry_offset_dim, bdry_stride_dim;
            size_t offset_in = get_bdry_info<dx,dv,dim>(i, off, offset_s,
                                       bdry_offset_dim, bdry_stride_dim);

            fp* p_in = in + offset_in;

            int sign = psign[offset_s];


            fp* local_bdry = bdry_to + bdr_len*o*bdry_offset_dim;

            if(sign < 0) {
                local_bdry += bdr_len*o*bdry_stride_dim*(abs(sign)-1);

                if(lr == location::left){
                    for(int k=0;k<o;k++)
                        local_bdry[o*idx[dim]+k] = 0.0;
                } else {
                    for(int k=0;k<o;k++){
                        //local_bdry[o*idx[dim]+k] = p_in[(o*idx[dim]+k)*stride_in];
                        fp value=0.0;
                        for(int l=0;l<ratio;++l){
                            for(int m=0;m<o;++m){
                                int lidx = (m + o*(l + idx[dim]*ratio))*stride_in;
                                value += p_in[lidx]*gauss::w(m,o)*lagrange(
                                         (l+gauss::x_scaled01(m,o))/fp(ratio),k,o);
                            }
                        }
                        local_bdry[o*idx[dim]+k] = value/(gauss::w(k,o)*ratio);
                    }
                }
            } else {
                local_bdry += bdr_len*o*bdry_stride_dim*(sign-1 + bdry_max_left);

                if(lr == location::right){
                    for(int k=0;k<o;k++)
                       local_bdry[o*idx[dim]+k] = 0.0;
                } else{
                    for(int k=0;k<o;k++){
                        //local_bdry[o*idx[dim]+k] = 
                        //    p_in[(o*(n-1-idx[dim])+k)*stride_in];
                        fp value=0.0;
                        for(int l=0;l<ratio;++l){
                            for(int m=0;m<o;++m){
                                int lidx = (m+o*(n-ratio*(1+idx[dim])+l))*stride_in;
                                value += p_in[lidx]*gauss::w(m,o)*lagrange(
                                         (l+gauss::x_scaled01(m,o))/fp(ratio),k,o);
                            }
                        }
                        local_bdry[o*idx[dim]+k] = value/(gauss::w(k,o)*ratio);
                    }
                }
            }

            idx[single_out] = 0;
        }
    }


    void pack_bdry_f2c(size_t dim, domain<d>& in, Index bdr_len, 
                   fp* bdry_to, vector<int>& psign, int bdry_max_left,
                   int ratio, location lr) {

        gt::start(str(boost::format("pack_bdry_cpu_dim%1%")%dim));
        if(dim == 0)
            pack_boundary_f2c_kernel<0>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else if(dim == 1)
            pack_boundary_f2c_kernel<1>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else if(dim == 2)
            pack_boundary_f2c_kernel<2>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else if(dim == 3)
            pack_boundary_f2c_kernel<3>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else if(dim == 4)
            pack_boundary_f2c_kernel<4>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else if(dim == 5)
            pack_boundary_f2c_kernel<5>(in.data.data(), bdr_len, bdry_to, 
                                    psign, bdry_max_left, ratio, lr);
        else {
            cout << "ERROR: only dim<=6 is implemented" << endl;
            exit(1);
        }
        gt::stop(str(boost::format("pack_bdry_cpu_dim%1%")%dim));
    }
 
    template<Index o>
    void unpack_bdry_c2f_k(size_t dim, Index bdry_max_length, 
                           vector<int>& psign, int bdr_len_coarse){
        
        if(dim!=0){
            cout << "ERROR: unpack bdry coarse to fine not yet implemented "
                 << "for dim > 0.\n";
            exit(1);
        }
        int n_dim = e[0]; //assuming dim==0
        Index dof_v = prod(e.v_part());
        int ratio = bdry_cell_ratio;

        fp hx = 1.0/fp(ratio);

        dg_evaluator dgeval(o);

        int off[2];
        for(size_t i=0;i<d;i++)
            off[i]=e[d+i]*e[i];

        cout << "dof_v: " << dof_v << endl;
        cout << "bl_coarse: " << bdr_len_coarse << endl;

        fp* bdry_data = bdr_data.data();
        fp* bdry_data_in = bdry_data + ratio*bdry_max_length;

        for(Index v_idx=0;v_idx<dof_v;++v_idx){

            int i[2]; i[0] = 0; i[1] = v_idx; //assuming dim==0

            int offset_s, bdry_offset_dim, bdry_stride_dim;
            get_bdry_info<dx,dv,0>(i,off,offset_s,bdry_offset_dim,bdry_stride_dim);
            int signv = psign[offset_s];

            for(int bl=0;bl<bdr_len_coarse;++bl){

                int istar = (signv>0) ? -bl-1 : bl+n_dim;
                fp* local_bdry_in = unpack_sorted_bdry(signv, bdry_offset_dim, 
                                  bdry_stride_dim, bdr_len_coarse, istar, o, n_dim,
                                  max_bdry_left, bdry_data_in);

                array<fp,o> loc_in;
                for(Index j=0;j<o;++j)
                    loc_in[j] = local_bdry_in[j];

                for(int r=0;r<ratio;++r){
                    int _istar = (signv>0) ? -r-1-bl*ratio : r+n_dim+bl*ratio;
                    fp* local_bdry_out = unpack_sorted_bdry(signv, bdry_offset_dim,
                                      bdry_stride_dim, bdr_len, _istar, o, n_dim,
                                      max_bdry_left, bdry_data);
                    for(Index j=0;j<o;++j){

                        int ridx = r;
                        if(signv>0)
                            ridx = ratio-1-r;

                        fp x = hx*(ridx + gauss::x_scaled01(j,o));

                        local_bdry_out[j] = dgeval.eval(x,loc_in.data());
                    }
                }
            }
        }
    }

    void unpack_bdry_c2f(int dim, Index bdry_max_length, vector<int>& psign,
                         int bdr_len_coarse){
        Index o = e[d];
        if(o==1)
            unpack_bdry_c2f_k<1>(dim, bdry_max_length, psign, bdr_len_coarse);
        else if(o==2)
            unpack_bdry_c2f_k<2>(dim, bdry_max_length, psign, bdr_len_coarse);
        else if(o==3)
            unpack_bdry_c2f_k<3>(dim, bdry_max_length, psign, bdr_len_coarse);
        else if(o==4)
            unpack_bdry_c2f_k<4>(dim, bdry_max_length, psign, bdr_len_coarse);
        else{
            cout << "ERROR: unpack boundary not yet implemented for o>4.\n";
            exit(1);
        }
    }

    void source_kernel(fp dt, fp t, source_f<dx,dv> source){
    
        Index single_out = dx;
        #pragma omp parallel for schedule(static)
        for(Index i=0;i<e[single_out];i++) {
            array<Index,2*d> max1=e.get_array();
            max1[single_out]=0;
            array<Index,2*d> _idx={0};

            fp x[d];

            do {
                array<Index,2*d> idx = _idx;
                idx[single_out] = i;

                for(size_t i=0;i<d;i++) {
                    fp hx = (b[i]-a[i])/fp(e[i]);
                    fp xx = gauss::x_scaled01(idx[d+i],e[d+i]);
                    x[i] = a[i] + idx[i]*hx + hx*xx;
                }
                if(source.type==source_type::time_dependent_source)
                    out.data(idx) = in.data(idx) + 
                                    dt*source.time_dependent_source(x,t);
                if(source.type==source_type::constant_source)
                    out.data(idx) = in.data(idx) + 
                                    dt*source.constant_source(x,t);
            } while(iter_next_memorder(_idx,max1));
        }

    }

};

