#pragma once

#include <generic/common.hpp>
#include <algorithms/sldg2d/generic_sldg2d.hpp>


struct quasi_neutrality{

    static constexpr size_t dx=3;

    array<fp,dx> a;
    array<fp,dx> b;
    array<fp,dx> h;

    int Nr, Ntheta, Nz;

    generic_container<fp> rho;
    generic_container<fp> rho_cc;

    generic_container<fp> phi;
    generic_container<fp> phi_cc;

    generic_container<fp> dr_phi;
    generic_container<fp> dr_phi_cc;
  
    generic_container<fp> dtheta_phi;
    generic_container<fp> dtheta_phi_cc;

    multi_array<fp,2*dx> dz_phi;
    generic_container<fp> dz_phi_cc;

    double electric_energy;

    int order_z;
    bool gpu;

    quasi_neutrality(int o2d, int oz, array<fp,dx> _a, array<fp,dx> _b,
                     int _Nr, int _Ntheta, int _Nz, bool _gpu)
        : a{_a}, b{_b}, Nr{_Nr}, Ntheta{_Ntheta}, Nz{_Nz}, 
          dz_phi{_gpu}, order_z{oz}, gpu{_gpu}
    {
        h[0] = (b[0]-a[0])/fp(Nr);
        h[1] = (b[1]-a[1])/fp(Ntheta);
        h[2] = (b[2]-a[2])/fp(Nz*oz);

        rho.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        rho_cc.resize((Nr+1)*Ntheta*Nz*oz);

        phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dr_phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        dr_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dtheta_phi.resize(Nr*o2d*Ntheta*o2d*Nz*oz);
        dtheta_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

        dz_phi.resize({Nr,o2d,Ntheta,o2d,Nz,oz});
        dz_phi_cc.resize((Nr+1)*Ntheta*Nz*oz);

    }

    generic_container<fp> gauss_o2d;

    virtual void solve() = 0;

    virtual ~quasi_neutrality(){}

    void compute_electric_energy(){

        #ifdef __CUDACC__
        if(gpu)
            phi_cc.copy_to_host();
        #endif

        int r_np_idx = Nr/2;
        fp hh = h[1]*h[2];

        double integral=0.0;

        #pragma omp parallel for schedule(static) reduction(+:integral)
        for(int k=0;k<Nz*order_z;++k){
            for(int j=0;j<Ntheta;++j){

                integral += hh*pow(phi_cc.h_data[r_np_idx + (Nr+1)*(j+Ntheta*k)],2);

            }
        }

        electric_energy = sqrt(integral);
    }


    void follow_characteristics_cc1(fp& r, fp& theta, fp dt,
                                    int i, int j, int k){

        int lidx = i+(Nr+1)*((j%Ntheta)+Ntheta*k);
        fp rr = a[0] + h[0]*i;
        r     += dt*dtheta_phi_cc.h_data[lidx]/rr;
        theta -= dt*dr_phi_cc.h_data[lidx]/rr;
    }


    void follow_characteristics_dg1(fp& r, fp& theta, fp dt,
                                    int ii, int jj, int i, int j, int k){

        constexpr size_t o2d = 2;

        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*k)));
        fp rr = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
        r     += dt*dtheta_phi.h_data[lidx]/rr;
        theta -= dt*dr_phi.h_data[lidx]/rr;
    }


    fp F_at_point(fp* F, fp r, fp theta){

        int i = floor((r-a[0])/h[0]);
        int j = floor((theta-a[1])/h[1]);

        int ip1 = i+1;
        //TODO: rounding problems here?
        if(i==-1) i=0;
        if(i==Nr+1){
            i=Nr;
            ip1=Nr; //r \approx r1 -> F21,F22 have no impact
        }

        if(i<0 || i>Nr){
            cout << "WARNING: i<0 || i>Nr -> to large step size or unreasonable "
                 << "velocity field.\n";
            exit(1);
        }

        fp F11 = F[i + (Nr+1)*((j+Ntheta)%Ntheta)];//((i+Nx)%Nx,(j+Ny)%Ny);
        fp F12 = F[i + (Nr+1)*((j+1+Ntheta)%Ntheta)];//((i+Nx)%Nx,(j+1+Ny)%Ny);
        fp F21 = F[ip1 + (Nr+1)*((j+Ntheta)%Ntheta)];//((i+1+Nx)%Nx,(j+Ny)%Ny);
        fp F22 = F[ip1 + (Nr+1)*((j+1+Ntheta)%Ntheta)];//((i+1+Nx)%Nx,(j+1+Ny)%Ny);

        fp r1 = a[0] + i*h[0];
        fp r2 = a[0] + (i+1)*h[0];
        fp theta1 = a[1] + j*h[1];
        fp theta2 = a[1] + (j+1)*h[1];

        return (F11*(r2-r)*(theta2-theta) + F12*(r2-r)*(theta-theta1) +
                F21*(r-r1)*(theta2-theta) + F22*(r-r1)*(theta-theta1))/(h[0]*h[1]);

    }


    void follow_characteristics_cc2(fp& r, fp& theta, fp dt,
                                    int i, int j, int k){

        int lidx = i+(Nr+1)*((j%Ntheta)+Ntheta*k);
        fp rr = a[0] + h[0]*i;

        fp r12     = r + 0.5*dt*dtheta_phi_cc.h_data[lidx]/rr;
        fp theta12 = theta - 0.5*dt*dr_phi_cc.h_data[lidx]/rr;

        int offset = (Nr+1)*Ntheta*k;

        r += dt*F_at_point(&dtheta_phi_cc.h_data[offset],r12,theta12)/r12;
        theta -= dt*F_at_point(&dr_phi_cc.h_data[offset],r12,theta12)/r12;
    }


    void follow_characteristics_dg2(fp& r, fp& theta, fp dt,
                                    int ii, int jj, int i, int j, int k){

        constexpr size_t o2d = 2;

        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*k)));
        fp rr = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));

        fp r12     = r + 0.5*dt*dtheta_phi.h_data[lidx]/rr;
        fp theta12 = theta - 0.5*dt*dr_phi.h_data[lidx]/rr;

        int offset = (Nr+1)*Ntheta*k;

        r += dt*F_at_point(&dtheta_phi_cc.h_data[offset],r12,theta12)/r12;
        theta -= dt*F_at_point(&dr_phi_cc.h_data[offset],r12,theta12)/r12;
    }

};


template<size_t o2d, size_t oz, size_t ov>
struct dk_block {

    static constexpr size_t dx = 3;
    static constexpr size_t dv = 1;

    using domain_x = multi_array<fp,2*dx>;
    using domain   = multi_array<fp,2*(dx+dv)>;

    virtual void compute_rho_and_invariants(
          unique_ptr<quasi_neutrality>& qns, double&, double&, double&) = 0;
    virtual void advection_z(fp tau) = 0;
    virtual void advection_v(fp tau, unique_ptr<quasi_neutrality>& qns) = 0;
    virtual void advection_rt(fp tau, unique_ptr<quasi_neutrality>& qns, 
                              int stage) = 0;
    virtual void source_term(fp tau, unique_ptr<quasi_neutrality>& qns) = 0;
    virtual void swap() = 0;
    virtual void swap_in() = 0;

    virtual domain& get_data() = 0;

    virtual ~dk_block() {};
};
