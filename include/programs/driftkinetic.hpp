#include <generic/common.hpp>
#include <algorithms/dg.hpp>

#include <programs/dk-block-cpu.hpp>
#include <programs/dk-block-gpu.hpp>


template<size_t o2d, size_t oz, size_t ov>
struct driftkinetic {

    static constexpr size_t dx{3};
    static constexpr size_t dv{1};
    static constexpr size_t d{dx+dv};

    array<fp,dx+dv> a;
    array<fp,dx+dv> b;
    Index Nr, Ntheta, Nv, Nz;

    fp tau, finalT;

    unique_ptr<quasi_neutrality> qns;

    int time_order;
    bool perturbation;
    bc bcr;
    bc bct;

    bool gpu;

    unique_ptr<dk_block<o2d,oz,ov>> block;

    string name;

    fp (*_geq)(fp,fp);

    driftkinetic(array<fp,4> _a, array<fp,4> _b, 
                 Index _Nr, Index _Ntheta, Index _Nv, Index _Nz,
                 fp _tau, fp _finalT,
                 fp (*perturb_func)(fp,fp,fp), 
                 fp (*n0r)(fp),
                 fp (*alpha)(fp,int,int), 
                 fp (*geq)(fp,fp),
                 bool _gpu,
                 int _time_order,
                 bool _perturbation=true,
                 bc _bcr=bc::homogeneous_dirichlet,
                 bc _bct=bc::periodic)
        : a{_a}, b{_b}, 
          Nr{_Nr}, Ntheta{_Ntheta}, Nv{_Nv}, Nz{_Nz}, 
          tau{_tau}, finalT{_finalT}, perturbation{_perturbation}, 
          time_order{_time_order}, bcr{_bcr}, bct{_bct}, gpu{_gpu}, _geq{geq}
    {
        #ifdef USE_SINGLE_PRECISION
        cout  << "Single precision computations are performed" << endl;
        #else
        cout  << "Double precision computations are performed" << endl;
        #endif

        cout << "gpu: " << gpu << endl;

        cout << "a: " << a << endl;
        cout << "b: " << b << endl;

        name = "T"+std::to_string(finalT)+"_t"+std::to_string(tau)+
               "_"+std::to_string(Nr)+"_"+std::to_string(Ntheta)+
               "_"+std::to_string(Nv)+"_"+std::to_string(Nz)+"_"+
               std::to_string(o2d)+std::to_string(oz)+std::to_string(ov)+
               "_"+std::to_string(time_order);

        array<double,3> ax = {a[0],a[1],a[3]};
        array<double,3> bx = {b[0],b[1],b[3]};

        if(gpu==false){
            block.reset( new dk_block_cpu<o2d,oz,ov>(a, b,
                                                     Nr, Ntheta, Nv, Nz, 
                                                     perturb_func, n0r, 
                                                     geq, time_order,
                                                     perturbation, bcr, bct));
            qns.reset( new quasi_neutrality_cpu<o2d,oz>(ax,bx,Nr,Ntheta,Nz,
                                                        false,n0r,alpha));
            name += "_cpu";
        }
        else if(gpu==true){
            #ifdef __CUDACC__
            block.reset( new dk_block_gpu<o2d,oz,ov>(a, b,
                                                     Nr, Ntheta, Nv, Nz, 
                                                     perturb_func, n0r, 
                                                     geq, time_order,
                                                     perturbation, bcr, bct));
            qns.reset( new quasi_neutrality_gpu<o2d,oz>(ax,bx,Nr,Ntheta,Nz,
                                                        true,n0r,alpha));
            name += "_gpu";
            #endif
        }

    }


    void run(){

        cout << "finalT: " << finalT << endl;
        cout << "tau: " << tau << endl;
        int n_steps = ceil(finalT/tau);
        cout << "n_steps: " << n_steps << endl;

        double t=0.0;

        std::ofstream evolution("evolution_dk_"+name+".data");
        evolution << std::setprecision(16);
        
        double mass, l2, total_energy;
        double mass_0, l2_0, total_energy_0;

        gt::start("total");
        //has to be done at the beginning in order to compute the electric
        //potential which is required to obtain the total energy at t=0.
        gt::start("compute_rho");
        block->compute_rho_and_invariants(qns,mass,l2,total_energy);
        gt::stop("compute_rho");

        gt::start("solve_qne");
        qns->solve();
        gt::stop("solve_qne");

        for(int i=0;i<n_steps;++i){

            if(finalT-t<tau)
                tau = finalT-t;

            t += tau;

            gt::start("compute_rho");
            block->compute_rho_and_invariants(qns,mass,l2,total_energy);
            gt::stop("compute_rho");

            gt::start("solve_qne");
            qns->solve();
            gt::stop("solve_qne");

            if(i==0){
                mass_0=mass;
                l2_0=l2;
                total_energy_0=total_energy;
            }

            evolution << t-tau << " " << mass << " " << qns->electric_energy
                      << " " << l2 << " " << total_energy << " "
                      << abs(mass-mass_0)/mass_0 << " " 
                      << abs(l2-l2_0)/l2_0 << " "
                      << abs(total_energy-total_energy_0)/total_energy_0 << endl;

            cout << "\nt: " << t << " " << mass << endl;

            //write_rho_and_phi_cc(i);
            //write_out(i);


            if(time_order==1){

                gt::start("src_trm");
                if(perturbation){
                    block->source_term(0.5*tau,qns);
                    block->swap();
                }
                gt::stop("src_trm");

                gt::start("advection_z");
                block->advection_z(0.5*tau);
                block->swap();
                gt::stop("advection_z");

                gt::start("advection_v");
                block->advection_v(0.5*tau, qns);
                block->swap();
                gt::stop("advection_v");

                gt::start("compute_rho");
                block->compute_rho_and_invariants(qns,mass,l2,total_energy);
                gt::stop("compute_rho");

                gt::start("solve_qne");
                qns->solve();
                gt::stop("solve_qne");

                gt::start("advection_rt");
                block->advection_rt(tau,qns,1);
                block->swap();
                gt::stop("advection_rt");

                gt::start("advection_v");
                block->advection_v(0.5*tau, qns);
                block->swap();
                gt::stop("advection_v");
                
                gt::start("advection_z");
                block->advection_z(0.5*tau);
                block->swap();
                gt::stop("advection_z");

                gt::start("src_trm");
                if(perturbation){
                    block->source_term(0.5*tau,qns);
                    block->swap();
                }
                gt::stop("src_trm");

            }

            if(time_order==2){

                //predictor step
                gt::start("src_trm");
                if(perturbation){
                    block->source_term(0.5*tau,qns);
                    block->swap_in();
                    block->swap();
                }
                gt::stop("src_trm");

                gt::start("advection_z");
                block->advection_z(0.5*tau);
                block->swap();
                gt::stop("advection_z");

                gt::start("advection_v");
                block->advection_v(0.5*tau, qns);
                block->swap();
                gt::stop("advection_v");

                gt::start("advection_rt");
                block->advection_rt(0.5*tau,qns,1);
                block->swap();
                gt::stop("advection_rt");

                gt::start("compute_rho");
                block->compute_rho_and_invariants(qns,mass,l2,total_energy);
                gt::stop("compute_rho");

                gt::start("solve_qne");
                qns->solve();
                gt::stop("solve_qne");

                block->swap_in();

                //corrector step
                gt::start("src_trm");
                if(perturbation){
                    block->source_term(0.5*tau,qns);
                    block->swap();
                }
                gt::stop("src_trm");

                gt::start("advection_z");
                block->advection_z(0.5*tau);
                block->swap();
                gt::stop("advection_z");

                gt::start("advection_v");
                block->advection_v(0.5*tau, qns);
                block->swap();
                gt::stop("advection_v");

                gt::start("advection_rt");
                block->advection_rt(tau,qns,2);
                block->swap();
                gt::stop("advection_rt");

                gt::start("advection_v");
                block->advection_v(0.5*tau, qns);
                block->swap();
                gt::stop("advection_v");
                
                gt::start("advection_z");
                block->advection_z(0.5*tau);
                block->swap();
                gt::stop("advection_z");

                gt::start("src_trm");
                if(perturbation){
                    block->source_term(0.5*tau,qns);
                    block->swap();
                }
                gt::stop("src_trm");
            }
        }

        write_rho_and_phi_cc(n_steps);

        gt::stop("total");

        cout << "\nTimings: " << endl;
        cout << "compute rho: " << gt::total("compute_rho") << endl;
        cout << "solve qne: " << gt::total("solve_qne") << endl;
        cout << "advection_v: " << gt::total("advection_v") << endl;
        cout << "advection_z: " << gt::total("advection_z") << endl;
        cout << "advection_rt: " << gt::total("advection_rt") << endl;
        cout << "source term: " << gt::total("src_trm") << endl;

        cout << "t_total: " << gt::total("total") << endl;

        std::ofstream fs("global_timer_dk_"+name+".data");
        fs << gt::sorted_output() << endl;
    }




    //void write(double t, double mass, string filename){

    //    double energy = qns->electric_energy;
    //    
    //    std::ofstream fs(filename.c_str(),std::ofstream::app);
    //    fs.precision(16);
    //    fs << t << " " << mass << " " << energy << endl;
    //    fs.close();
    //    
    //};

    //void check_rho(){

    //    double c = 0.0;
    //    if(perturbation)
    //        c = 0.0;

    //    double maxerr = 0.0;
    //    array<Index,5> erridx = {0};
    //    std::ofstream err_rho("err_rho");
    //    for(Index l=0;l<Nz;++l){
    //    for(size_t ll=0;ll<oz;++ll){
    //        for(Index j=0;j<Ntheta;++j){
    //        for(size_t jj=0;jj<o2d;++jj){
    //            for(Index i=0;i<Nr;++i){
    //            for(size_t ii=0;ii<o2d;++ii){

    //                int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
    //                                Ntheta*(ll+oz*l))));

    //                double r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
    //                double t = a[1] + h[1]*(j+gauss::x_scaled01(jj,o2d));
    //                double z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));

    //                double exact = n0r(r)*(c + perturb_func(r,t,z)) - c*n0r(r);

    //                double err = exact-rho.h_data[lidx];

    //                if(fabs(err)>maxerr){
    //                    maxerr = std::max(maxerr,fabs(err));
    //                    erridx[0] = ii;
    //                    erridx[1] = jj;
    //                    erridx[2] = i;
    //                    erridx[3] = j;
    //                    erridx[4] = ll;
    //                    erridx[5] = l;
    //                }
    //                if(ll==0 && l==15){
    //                    err_rho << r << " " << t << " " << err << endl;
    //                }
    //            }
    //            }
    //        }
    //        }
    //    }
    //    }

    //    cout << "err rho beginning: " << maxerr << ", erridx: " << erridx << endl;

    //}


    void check_advection(){
        //setting dz phi for test advection in z
        //for(Index l=0;l<Nz;++l){
        //for(size_t ll=0;ll<oz;++ll){
        //    for(Index j=0;j<Ntheta;++j){
        //    for(size_t jj=0;jj<o2d;++jj){
        //        for(Index i=0;i<Nr;++i){
        //        for(size_t ii=0;ii<o2d;++ii){

        //            int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
        //                            Ntheta*(ll+oz*l))));

        //            double r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
        //            double t = a[1] + h[1]*(j+gauss::x_scaled01(jj,o2d));
        //            double z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));

        //            qns->dz_phi.h_data[lidx] = cos(r)*cos(t)*cos(z);
        //            qns->dr_phi.h_data[lidx] = r;
        //            qns->dtheta_phi.h_data[lidx] = r;

        //        }
        //        }
        //    }
        //    }
        //}
        //}
        //for(Index l=0;l<Nz;++l){
        //for(size_t ll=0;ll<oz;++ll){
        //    for(Index j=0;j<Ntheta;++j){
        //        for(Index i=0;i<Nr+1;++i){

        //            double r = a[0] + h[0]*i;
        //            
        //            int lidx = i+(Nr+1)*(j+Ntheta*(ll+oz*l));

        //            qns->dr_phi_cc.h_data[lidx] = r;
        //            qns->dtheta_phi_cc.h_data[lidx] = r;
        //        }
        //    }
        //}
        //}


        array<fp,4> h;
        h[0] = (b[0]-a[0])/fp(Nr);
        h[1] = (b[1]-a[1])/fp(Ntheta);
        h[2] = (b[2]-a[2])/fp(Nv);
        h[3] = (b[3]-a[3])/fp(Nz);

        double maxerr = 0.0;
        for(Index l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){
            for(Index k=0;k<Nv;++k){
            for(size_t kk=0;kk<ov;++kk){
                for(Index j=0;j<Ntheta;++j){
                for(Index i=0;i<Nr;++i){
                    for(size_t jj=0;jj<o2d;++jj){
                    for(size_t ii=0;ii<o2d;++ii){

                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        double r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                        double t = a[1] + h[1]*(j+gauss::x_scaled01(jj,o2d));
                        double v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));
                        double z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));

                        //check advection z                     
                        //double exact = sin(r)*sin(t)*sin(z-v*finalT)*sin(v);

                        //check advection v
                        //double exact = sin(r)*sin(t)*sin(z)*
                        //               sin(v-finalT*cos(r)*cos(t)*cos(z));

                        //check advection rt
                        double exact = sin(r+finalT)*sin(t-finalT)*sin(z)*sin(v);

                        double err = exact - block->get_data().v[lidx];

                        maxerr = std::max(maxerr, fabs(err));

                    }
                    }
                }
                }
            }
            }
        }
        }

        cout << "maxerr advection: " << maxerr << endl;
 

    }


    void write_rho_and_phi_cc(int i){

        std::ofstream outphi("data_phi"+std::to_string(i+1)+".data");
        std::ofstream outrho("data_rho"+std::to_string(i+1)+".data");
        #ifdef __CUDACC__
        if(gpu){
            qns->phi_cc.copy_to_host();
            qns->rho_cc.copy_to_host();
        }
        #endif

        //for(Index k=0;k<Nz*oz;++k){
        int k=0;
        for(Index j=0;j<Ntheta;++j){
            for(Index i=0;i<Nr+1;++i){

                int lidx = i+(Nr+1)*(j+Ntheta*k);

                double r = a[0] + (b[0]-a[0])/double(Nr)*i;
                double t = a[1] + (b[1]-a[1])/double(Ntheta)*j;
                //double z = a[3] + (b[3]-a[3])/double(Nz*oz)*k;
 
                if(k==0){
                    outphi << r << " " << t  << " "
                           << qns->phi_cc.h_data[lidx] << endl;
                    outrho << r << " " << t  << " "
                           << qns->rho_cc.h_data[lidx]/r << endl;
                }
            }
        }
        //}
 
    }


    void write_out(int i){

        double hr = (b[0]-a[0])/double(Nr);
        double ht = (b[1]-a[1])/double(Ntheta);
        double hv = (b[2]-a[2])/double(Nv);

        auto data = block->get_data();

        std::ofstream output_delta("data_outdelta"+std::to_string(i+1)+".data");
        std::ofstream output_full("data_outfull"+std::to_string(i+1)+".data");
        //double max_rho=0.0;
        //for(Index l=0;l<Nz;++l){
        //for(size_t ll=0;ll<oz;++ll){
        //    for(Index k=0;k<Nv;++k){
        //    for(size_t kk=0;kk<ov;++kk){
                int ll=0,kk=0,l=0,k=Nv/2;
                for(Index j=0;j<Ntheta;++j){
                for(size_t jj=0;jj<o2d;++jj){
                    for(Index i=0;i<Nr;++i){
                    for(size_t ii=0;ii<o2d;++ii){

                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        double r = a[0] + hr*(i+gauss::x_scaled01(ii,o2d));
                        double t = a[1] + ht*(j+gauss::x_scaled01(jj,o2d));
                        double v = a[2] + hv*(k+gauss::x_scaled01(kk,ov));
                        //double z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));
                        if(ll==0&&kk==0&&l==0&&k==Nv/2){
                            output_delta << r << " " << t << " " 
                                         << data.v[lidx]/r << endl;
                            output_full << r << " " << t << " " 
                                        << data.v[lidx]/r + _geq(r,v)/r << endl;
                        }
                    }
                    }
                }
                }
        //    }
        //    }
        //}
        //}
    }


    void check_diff(multi_array<fp,8>& d1, multi_array<fp,8>& d2){

        double maxerr = 0.0;
        array<size_t,8> erridx;
        for(Index l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){
            for(Index k=0;k<Nv;++k){
            for(size_t kk=0;kk<ov;++kk){
                for(Index j=0;j<Ntheta;++j){
                for(size_t jj=0;jj<o2d;++jj){
                    for(Index i=0;i<Nr;++i){
                    for(size_t ii=0;ii<o2d;++ii){

                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        double err = d1.v[lidx]-d2.v[lidx];

                        if(fabs(err)>maxerr){
                            maxerr = std::max(maxerr,fabs(err));
                            erridx[0] = ii;
                            erridx[1] = i;
                            erridx[2] = jj;
                            erridx[3] = j;
                            erridx[4] = kk;
                            erridx[5] = k;
                            erridx[6] = ll;
                            erridx[7] = l;
                        }

                    }
                    }
                }
                }
            }
            }
        }
        }

        cout << "maxdiff: " << maxerr << endl;
        cout << "erridx: " << erridx << endl;
    }

    fp* get_output(){
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        return block->get_data().data();
    }
 
};


