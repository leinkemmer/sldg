#pragma once

#ifdef __CUDACC__

#include <programs/dk-abstract.hpp>
#include <algorithms/dg-gpu.cu>
#include <algorithms/sldg2d/sldg2d_lagrange_gpu.hpp>
#include <algorithms/qns-gpu.hpp>


template<size_t o2d, size_t oz, size_t ov>
__global__
void compute_rho_k(fp* in, fp* rho, fp* phi, fp* geq_val,
                   Index Nr, Index Ntheta, Index Nv, 
                   fp ar, fp av, fp hr, fp ht, fp hv, fp hz, 
                   fp* gauss_x, fp* gauss_w, 
                   bool perturbation, double* block_mass, 
                   double* block_l2, double* block_tot_energy){

    size_t iijj=threadIdx.x;
    size_t i=threadIdx.y;
    size_t j=blockIdx.x;
    size_t ll=blockIdx.y;
    size_t l=blockIdx.z;

    //max number of threadIdx.z is limited to 64!
    size_t ii = iijj%o2d;
    size_t jj = iijj/o2d;

    fp* gauss_w_r = gauss_w;
    fp* gauss_w_t = gauss_w;
    fp* gauss_w_v = gauss_w+o2d;
    fp* gauss_w_z = gauss_w+o2d+ov;

    fp* gauss_x_r = gauss_x;
    fp* gauss_x_v = gauss_x+o2d;

    size_t bidx = j + gridDim.x*(ll + gridDim.y*l);
    size_t tidx = ii+o2d*(jj+o2d*i);

    size_t lidx_rho = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*(ll+oz*l))));

    fp weight = hr*0.5*gauss_w_r[ii]*
                ht*0.5*gauss_w_t[jj]*
                hz*0.5*gauss_w_v[ll]*
                hv*0.5;

    size_t offset = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*(ov*Nv)*(ll+oz*l))));
    in += offset;

    fp one_over_r = 1.0/(ar + hr*(i + gauss_x_r[ii]));

    //TODO:
    rho[lidx_rho] = (perturbation) ? 0.0 : 1.0;

    double loc_mass = 0.0;
    double loc_l2   = 0.0;
    double loc_tot_energy=0.0;
    for(int k=0;k<Nv;++k){
        for(int kk=0;kk<ov;++kk){

            int lidx_full = o2d*o2d*Nr*Ntheta*(kk+ov*k);

            fp g_val = in[lidx_full];
            rho[lidx_rho] += g_val*hv*gauss_w_v[kk]*0.5;

            fp w = weight*gauss_w_z[kk];
            fp v = av + hv*(k+gauss_x_v[kk]);
            g_val += geq_val[ii+o2d*(i+Nr*(kk+ov*k))];

            loc_mass += g_val*w;
            loc_l2 += g_val*g_val*one_over_r*w;
            loc_tot_energy += (v*v*0.5 + phi[lidx_rho])*g_val*w;

        }
    }

    //Warning: with order 2 in r-theta at most 128 cells in r allowed!!
    __shared__ double mass[512], l2[512], tot_energy[512];

    mass[tidx] = loc_mass;
    l2[tidx] = loc_l2;
    tot_energy[tidx] = loc_tot_energy;

    block_mass[bidx] = 0.0;
    block_l2[bidx] = 0.0;
    block_tot_energy[bidx] = 0.0;

    __syncthreads();

    if(tidx==0){
        int tot_threads = o2d*o2d*Nr;
        for(int idx=0;idx<tot_threads;++idx){
            block_mass[bidx] += mass[idx];
            block_l2[bidx] += l2[idx];
            block_tot_energy[bidx] += tot_energy[idx];
        }
    }
}


template<size_t o2d, size_t oz, size_t ov>
struct compute_rho_gpu{

    using domain = multi_array<fp,2*(3+1)>;

    Index Nr, Ntheta, Nz, Nv;
    array<fp,4> a;
    array<fp,4> h;
    int block_size;
    bool perturbation;
    generic_container<fp> gauss_w;
    generic_container<fp> gauss_x;
    generic_container<double> block_mass;
    generic_container<double> block_l2;
    generic_container<double> block_tot_energy;

    compute_rho_gpu(Index _Nr, Index _Ntheta, Index _Nz, Index _Nv, 
                    array<fp,4> _a, array<fp,4> _h, bool _perturbation)
        : Nr{_Nr}, Ntheta{_Ntheta}, Nz{_Nz}, Nv{_Nv}, 
          a{_a}, h{_h}, perturbation{_perturbation}
    {
        block_size = Ntheta*oz*Nz;
        block_mass.resize(block_size);
        block_l2.resize(block_size);
        block_tot_energy.resize(block_size);

        gauss_w.resize(o2d+oz+ov);
        vector<fp> gw = gauss::all_weights(o2d);
        std::copy(gw.begin(), gw.end(), &gauss_w.h_data[0]);
        gw = gauss::all_weights(ov);
        std::copy(gw.begin(), gw.end(), &gauss_w.h_data[0]+o2d);
        gw = gauss::all_weights(oz);
        std::copy(gw.begin(), gw.end(), &gauss_w.h_data[0]+o2d+ov);
        gauss_w.copy_to_gpu(); 

        gauss_x.resize(o2d+ov);
        gw = gauss::all_x_scaled01(o2d);
        std::copy(gw.begin(), gw.end(), &gauss_x.h_data[0]);
        gw = gauss::all_x_scaled01(ov);
        std::copy(gw.begin(), gw.end(), &gauss_x.h_data[0]+o2d);
        gauss_x.copy_to_gpu();
    }


    void compute_rho(domain& d_in, generic_container<fp>& rho, 
                     generic_container<fp>& phi,
                     generic_container<fp>& geq_val,
                     double& mass, double& l2, double& tot_energy){

        timer t_kernel, t_remainder;

        t_kernel.start();
        dim3 threads(o2d*o2d,Nr);
        dim3 blocks(Ntheta,oz,Nz);
        compute_rho_k<o2d,oz,ov><<<blocks,threads>>>(d_in.data(), rho.d_data, 
                                phi.d_data, geq_val.d_data, Nr, Ntheta, Nv, 
                                a[0], a[2], h[0], h[1], h[2], h[3],
                                gauss_x.d_data, gauss_w.d_data,
                                perturbation, block_mass.d_data, block_l2.d_data,
                                block_tot_energy.d_data);
                  
        gpuErrchk(cudaDeviceSynchronize());
        t_kernel.stop();

        t_remainder.start();
        block_mass.copy_to_host();
        block_l2.copy_to_host();
        block_tot_energy.copy_to_host();

        mass = 0.0;
        l2 = 0.0;
        tot_energy = 0.0;

        #pragma omp parallel for reduction(+:mass,l2,tot_energy)
        for(int i=0;i<block_size;++i){
            mass += block_mass.h_data[i];
            l2 += block_l2.h_data[i];
            tot_energy += block_tot_energy.h_data[i];
        }
        t_remainder.stop();

        cout << "t kernel: " << t_kernel.total() << ", tremainder: "
             << t_remainder.total() << endl;

        l2 = sqrt(l2);

    }
};


template<size_t o2d, size_t oz, size_t ov>
__global__
void source_term_k(fp* in, fp* out, fp tau, Index Nr, Index Ntheta, Index Nv,
                   fp* dtheta_phi, fp* dz_phi, fp* dr_geq, fp* dv_geq){

    int ii= threadIdx.x%o2d;
    int jj= threadIdx.x/o2d;
    int i = threadIdx.y;
    int j = blockIdx.x;
    int k = blockIdx.y;
    int l = blockIdx.z;
                        
    int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*(k+ov*Nv*l))));

    int lidx_x = ii+o2d*(jj+o2d*(i+Nr*(j+Ntheta*l)));

    int lidx_geq = ii+o2d*(i+Nr*k); 

    out[lidx] = in[lidx] + tau*(dtheta_phi[lidx_x]*dr_geq[lidx_geq] - 
                                dz_phi[lidx_x]*dv_geq[lidx_geq]);
}


template<size_t o2d, size_t oz, size_t ov>
struct dk_block_gpu : public dk_block<o2d,oz,ov> {

    static constexpr size_t dx = 3;
    static constexpr size_t dv = 1;
    static constexpr size_t d = dx+dv;

    //required to create dg_coeff_generic
    using domain_v = multi_array<fp,2*dv>;
    using domain_x = multi_array<fp,2*dx>;
    using domain   = multi_array<fp,2*(dx+dv)>;

    domain d_in, d_in_half, d_out;
    domain h_in;

    array<fp,dx+dv> a;
    array<fp,dx+dv> b;
    array<fp,dx+dv> h;
    array<Index,2*(dx+dv)> e;

    unique_ptr<dg_coeff_generic<dx>> dg_c_v;
    unique_ptr<dg_coeff_generic<dv>> dg_c_z;
    domain_v v_values;
    fp tau_used;
    unique_ptr<translate_gpu<dx,dv,dx>> trgpu_v;
    unique_ptr<translate_gpu<dx,dv,dv>> trgpu_z;

    unique_ptr<sldg2d_lagrange_gpu<o2d>> trgpu_rt;

    fp (*perturb_func)(fp,fp,fp);
    fp (*n0r)(fp);
    fp (*geq)(fp,fp);

    int time_order;
    bool perturbation;
    bc bcr;
    bc bct;

    Index Nr, Ntheta, Nv, Nz;

    generic_container<fp> geq_val;
    generic_container<fp> dr_geq;
    generic_container<fp> dv_geq;

    unique_ptr<compute_rho_gpu<o2d,oz,ov>> crgpu;

    dk_block_gpu(array<fp,dx+dv> _a, array<fp,dx+dv> _b,
                 Index _Nr, Index _Ntheta, Index _Nv, Index _Nz,
                 fp (*_perturb_func)(fp,fp,fp),
                 fp (*_n0r)(fp),
                 fp (*_geq)(fp,fp),
                 int _time_order,
                 bool _perturbation=true,
                 bc _bcr=bc::homogeneous_dirichlet,
                 bc _bct=bc::periodic)
        : d_in{true}, d_in_half{true}, d_out{true}, a{_a}, b{_b},
          Nr{_Nr}, Ntheta{_Ntheta}, Nv{_Nv}, Nz{_Nz},
          tau_used{-1.0}, perturb_func{_perturb_func}, n0r{_n0r}, 
          geq{_geq}, time_order{_time_order}, perturbation{_perturbation}, 
          bcr{_bcr}, bct{_bct}
    {

       h[0] = (b[0]-a[0])/fp(Nr);
       h[1] = (b[1]-a[1])/fp(Ntheta);
       h[2] = (b[2]-a[2])/fp(Nv);
       h[3] = (b[3]-a[3])/fp(Nz);

       e[0] = Nr;
       e[1] = Ntheta;
       e[2] = Nv;
       e[3] = Nz;
       e[4] = o2d;
       e[5] = o2d;
       e[6] = ov;
       e[7] = oz;
       
       //more cout 
       cout << "o2d: " << o2d << ", oz: " << oz << ", ov: " << ov << endl;
       d_in.resize(e);
       d_out.resize(e);
       h_in.resize(e);

       init_f0(); 
       if(time_order>1)
           d_in_half = d_in;

       int dof_x = Nr*o2d*Ntheta*o2d*Nz*oz;
       int dof_v = Nv*ov;
    
       dg_c_v = make_dg_coeff_on_gpu<dx>(ov,dof_x);
       dg_c_z = make_dg_coeff_gpu<dv>(oz,dof_v);
       trgpu_v = make_unique<translate_gpu<dx,dv,dx>>(e,0);
       trgpu_z = make_unique<translate_gpu<dx,dv,dv>>(e,0);

       v_values.resize({Nv,ov});
       init_v_values(a[2],h[2]);

       trgpu_rt = make_unique<sldg2d_lagrange_gpu<o2d>>(a[0],a[1],b[0],b[1],
                                                        Nr,Ntheta,Nz*oz);

       dr_geq.resize(Nr*o2d*Nv*ov);
       dv_geq.resize(Nr*o2d*Nv*ov);
       geq_val.resize(Nr*o2d*Nv*ov);

       init_srctrm();

       crgpu = make_unique<compute_rho_gpu<o2d,oz,ov>>(Nr, Ntheta, Nz, Nv, 
                                                       a, h, perturbation);

       cudaDeviceSynchronize();
       gpuErrchk(cudaPeekAtLastError());
    }


    void init_v_values(fp av, fp hv){

        array<Index,2*dv> idx{0};
        array<Index,2*dv> ev = {Nv,ov};
        do{
            fp xx = gauss::x_scaled01(idx[1],ev[1]);
            v_values(idx) = av + hv*(idx[0] + xx);
        } while(iter_next_memorder(idx,ev));

    }


    void init_f0(){

        fp c = 1.0;
        if(perturbation)
            c = 0.0;

        #pragma omp parallel for schedule(static)
        for(Index l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){
            for(Index k=0;k<Nv;++k){
            for(size_t kk=0;kk<ov;++kk){
                for(Index j=0;j<Ntheta;++j){
                for(Index i=0;i<Nr;++i){
                    for(size_t jj=0;jj<o2d;++jj){
                    for(size_t ii=0;ii<o2d;++ii){

                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        fp r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                        fp t = a[1] + h[1]*(j+gauss::x_scaled01(jj,o2d));
                        fp v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));
                        fp z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));

                        h_in.v[lidx] = geq(r,v)*(c + perturb_func(r,t,z));

                    }
                    }
                }
                }
            }
            }
        }
        }
        d_in = h_in;

    }


    void init_srctrm(){

        fp c = 0.0;
        if(perturbation)
            c = 1.0;

        #pragma omp parallel for schedule(static)
        for(Index k=0;k<Nv;++k){
        for(size_t kk=0;kk<ov;++kk){
            for(Index i=0;i<Nr;++i){
                for(size_t ii=0;ii<o2d;++ii){

                    fp deltar = 1e-7;
                    fp deltav = 1e-7;

                    int idx = ii + o2d*(i+Nr*(kk + ov*k));

                    fp r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                    fp v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));

                    fp drgeq = (geq(r+deltar,v)/(r+deltar)-
                                    geq(r-deltar,v)/(r-deltar))/(2.0*deltar);
                    fp dvgeq = (geq(r,v+deltav)-geq(r,v-deltav))/(2.0*deltav);

                    dr_geq.h_data[idx] = drgeq;//drgeq/r-geq(r,v)/(r*r);
                    dv_geq.h_data[idx] = dvgeq;
                    geq_val.h_data[idx] = geq(r,v)*c;

                }
            }
        }
        }

        dr_geq.copy_to_gpu();
        dv_geq.copy_to_gpu();
        geq_val.copy_to_gpu();
    }


    void swap(){
        d_in.swap(d_out);
        gpuErrchk(cudaDeviceSynchronize());
    }


    void swap_in(){
        d_in.swap(d_in_half);
        gpuErrchk(cudaDeviceSynchronize());
    }


    domain& get_data(){
        gpuErrchk(cudaMemcpy(h_in.data(),d_in.data(),
                             sizeof(fp)*d_in.num_elements(),
                             cudaMemcpyDeviceToHost));
        return h_in;
    }


    void compute_rho_and_invariants(unique_ptr<quasi_neutrality>& qns,
                                    double& mass, double& l2, double& tot_energy){

        crgpu->compute_rho(d_in, qns->rho, qns->phi, geq_val, 
                           mass, l2, tot_energy);

        gpuErrchk(cudaDeviceSynchronize());

    }


    void advection_z(fp tau){
        int dim=3;

        if(tau_used < 0.0 || tau_used != tau){
            dg_c_z->compute(tau,h[dim],v_values);
            dg_c_z->copy_to_gpu();
            tau_used = tau;
            cudaDeviceSynchronize();
        }

        trgpu_z->translate(dim, tau, h[dim], *dg_c_z, d_in, d_out,
                           0, 0, nullptr, false, nullptr, 0, dim);

        gpuErrchk(cudaDeviceSynchronize());
    }


    void advection_v(fp tau, unique_ptr<quasi_neutrality>& qns){
        int dim=2;

        gt::start("adv_v_computeAB");
        dg_c_v->compute(tau,h[dim],qns->dz_phi);
        gpuErrchk(cudaDeviceSynchronize());
        gt::stop("adv_v_computeAB");

        gt::start("adv_v_translate");
        trgpu_v->translate(dim, tau, h[dim], *dg_c_v, d_in, d_out,
                           0, 0, nullptr, false, nullptr, 0, dim);
        cudaDeviceSynchronize();
        gt::stop("adv_v_translate");

    }


    void advection_rt(fp tau, unique_ptr<quasi_neutrality>& qns, int stage){

        trgpu_rt->step_nl(d_in.data(), d_out.data(),
                          *qns,tau,Nv*ov,bcr,bct,stage);

        gpuErrchk(cudaDeviceSynchronize());
    }


    void source_term(fp tau, unique_ptr<quasi_neutrality>& qns){

        dim3 threads(o2d*o2d,Nr);
        dim3 blocks(Ntheta,ov*Nv,oz*Nz);

        source_term_k<o2d,oz,ov><<<blocks,threads>>>(d_in.data(), d_out.data(),
                                                   tau, Nr, Ntheta, Nv, 
                                                   qns->dtheta_phi.d_data, 
                                                   qns->dz_phi.data(), 
                                                   dr_geq.d_data, 
                                                   dv_geq.d_data);

        gpuErrchk(cudaDeviceSynchronize());
    }


};

#endif
