#include <generic/common.hpp>
#include <boost/program_options.hpp>

#include <generic/netcdf.hpp>
#include <container/domain.hpp>

namespace parameters {
    // gpu related parameters
    bool enable_p2p=true;
    bool cuda_aware_mpi=true;
};

#include <programs/vp-block-list.hpp>

#include <programs/poisson_solvers.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif


// type of BC used
enum class bdry_cond {
    periodic=0, dirichlet
};


template<Index dv>
Index max_bdr_coml_len(array<Index,2*dv> ev) {
    Index r = 0;
    for(Index i=0;i<dv;i++)
        r = max(r, ev[dv+i]*prod(slice_no<dv>(i, ev)) );

    return r;
}

//NOTE: the name node in the following is misleading. 
//      a better name for num_nodes would be mpi_grid.
//      This change affects also the mpi.h file and thus
//      also the other projects, therefore, this name 
//      is kept currently.


// the class that performs the numerical simulation
template<size_t dx, size_t dv, fp (*f0)(fp*)>
struct vlasovpoisson {
    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;

    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;

    using domain_x = multi_array<fp,dx_size>;
    using efield  = array< domain_x, dx>;

    bool gpu;
    fp T, tau;
    mpi_process<d> &mpicomm;
    timer total, comm, advect, poisson, comm_poisson, prep;
    vec_d a;
    vec_d b;
    vec_d a_global, b_global;
    index_no e;
    efield E;
    domain_x rho;
    int snapshots;

    vector<vector<fp>> bdr_send;
    vector<vector<fp>> bdr_recv; 

    array<bdry_cond,dx> bc_x;
    Index bdr_max;

    std::unique_ptr< vp_block_list<dx,dv,f0> > blocks;

    std::unique_ptr< poisson_solver<dx> > poisson_s;

    bool fftb;
    int total_num_nodes_x;
    int total_num_nodes_v;
    Index bdr_max_length;

    array<Index,dv> num_blocks;
    Index total_blocks;

    vlasovpoisson(vec_d _a, vec_d _b, index_no _e, fp _T, fp _tau,
            mpi_process<dx+dv>& _mpicomm, int _snapshots, bool _gpu,
            array<Index,d> num_nodes, array<bdry_cond,dx> _bc_x,
            fp _sigma, fp _cg_tol, Index _cg_maxiter, bool _fftb, 
            int MPIProcessPerNode, array<Index,dv> _num_blocks, int maxCFL=1)
        : gpu(_gpu), T(_T), tau(_tau), mpicomm(_mpicomm), a(_a), b(_b), e(_e),
          snapshots(_snapshots), bc_x(_bc_x), fftb(_fftb), num_blocks{_num_blocks},
          total_num_nodes_x(1), total_num_nodes_v(1) {


        for(size_t i=0;i<dx;i++)
            total_num_nodes_x *= num_nodes[i];
        for(size_t i=dx;i<d;i++)
            total_num_nodes_v *= num_nodes[i];

        #ifdef USE_SINGLE_PRECISION
        cout  << "Single precision computations are performed" << endl;
        #else
        cout  << "Double precision computations are performed" << endl;
        #endif

        for(size_t i=0;i<d_size;i++) {
            if(e[i] <= 0) {
                cout << "ERROR: entry " << i << " of e <= 0" << endl;
                exit(1);
            }
        }

        if(mpicomm.rank==0)
            cout << "MPI grid: " << num_nodes << endl;

        mpicomm.init(num_nodes,dx,dv);

        a_global = a; b_global = b;
        global_ab_to_local_ab<d>(a.get_array(), b.get_array(),
                                 mpicomm.node_id, mpicomm.num_nodes);

        std::stringstream s; s << "Node: " << mpicomm.node_id 
            << " of " << mpicomm.num_nodes 
            << " with rank " << mpicomm.rank
            << ". Domain from " << a << " to " << b 
            << ". Rank left: " << mpicomm.rank_left(0) 
            << ". Rank right: " << mpicomm.rank_right(0) << ".";
        mpicomm.print(s.str());

        Index CFL{1};
        for(int i=0; i<dx; ++i)
            if(num_nodes[i] > 1){
                fp max_velocity = max(fabs(b_global[dx+i]),fabs(a_global[dx+i]));
                CFL = max(CFL,Index(tau*max_velocity/2.0/
                            ((b_global[i]-a_global[i])/(e[i]*num_nodes[i])))+1);
                //division by 2 due to Strang splitting, 
                //max advection speed here is maximum velocity
             }

        for(int i=dx; i<dx+dx; ++i)
            if(num_nodes[i] > 1){
                fp max_velocity = 1.0;
                CFL = max(CFL,Index(tau*max_velocity/
                            ((b_global[i]-a_global[i])/(e[i]*num_nodes[i])))+1);
                //assuming that the maximum of the electric field 
                //is less or equal than one which is true for Landau, bot and tsi
            }

        bdr_max = maxCFL;
        if(mpicomm.rank==0){
            cout << "max CFL assuming E<=1: " << CFL << endl;
            cout << "bdr cells set to " << bdr_max << endl;
            if(CFL>maxCFL){
                cout << "WARNING: not enough memory for "
                     << "boundary cells might be provided.\n";
            }
        }

        // maximal amount of data stored in a boundary vector
        bdr_max_length = bdr_max*max_bdr_coml_len<d>(e.get_array());
        if(mpicomm.rank==0)
            cout << "bdr_max_length: " << bdr_max_length << endl;

        total_blocks = prod(num_blocks);

        int add_to_device_number=0;
        if(gpu && MPIProcessPerNode!=1)
            if(total_blocks==1)
                add_to_device_number = mpicomm.rank%MPIProcessPerNode;
            else{
                if(mpicomm.rank==0){
                    cout << "ERROR: code does not support multiple mpi processes "
                         << "and multiple blocks on a node! "
                         << "(both is not possible)\n";
                }
                exit(1);
            }

        blocks.reset( new vp_block_list<dx,dv,f0>(num_blocks, a, b, e, gpu,
                                                  mpicomm.node_id, 
                                                  add_to_device_number,
                                                  bdr_max_length) );

        if(gpu)
            enable_peer_to_peer();

        array<Index,dx_size> emax = e.x_part();
        for(size_t i=0;i<dx;i++) {
            E[i].resize(emax);
        }

        // check OpenMP support
        if(mpicomm.rank==0) {
            #ifdef _OPENMP
            cout << "OpenMP with " << omp_get_max_threads() << " threads"
                 << endl;
            #else
            cout << "No OpenMP support" << endl;
            #endif
        }

        if (fftb) {
            poisson_s = make_unique<fft_poisson_solver<dx,dv>>(
                             a_global.x_part(), b_global.x_part(), emax, mpicomm);
        } else {
            domain_x PHI_start;
            PHI_start.resize(emax);
            std::fill(PHI_start.begin(), PHI_start.end(), 0.0);
            poisson_s = make_unique<dg_poisson_solver<dx,dv>>(
                          a.x_part(), b.x_part(), emax, mpicomm, _sigma, 
                          _cg_tol, _cg_maxiter, PHI_start, tau);
        }

        // empty evolution.data
        if(enable_output && mpicomm.rank == 0) {
            std::ofstream fs("evolution.data");
            fs << "# time mass momentum_x energy kinetic_energy electric_energy"
               << " L2 entropy L1" << endl;
            fs.close();
        }
 
        if (gpu && !parameters::cuda_aware_mpi){
            bdr_send.resize(total_blocks);
            bdr_recv.resize(total_blocks);
            for(int i=0;i<total_blocks;++i){
                bdr_send[i].resize(bdr_max_length); 
                bdr_recv[i].resize(bdr_max_length);
            }
        }
    }

    
    void run() {
        // set initial value
        gt::start("initialization");
        blocks->init();
        
        // needed for the first call to write_variables
        solve_poisson(E,true,false);
        gt::stop("initialization");

        total.start();
        int n_steps=ceil(T/tau);
        double t=0.0;
        for(int i=0;i<n_steps+1;i++) {
            gt::start("step");
            timer t_adv, t_step, t_poisson, t_write, t_mpi_comm; 
            t_step.start();

            t_write.start();
            gt::start("compute_Efield_and_write");
            // write evolution.data file and computes the E field
            write_variables(t,"evolution.data", E);

            if(enable_output && snapshots>=2 && 
                 (i % int(ceil(n_steps/fp(snapshots-1))) == 0 || i==n_steps)) {
                gt::start("write_f");
                write_fE_nc((boost::format("out-t%1%.nc")%t).str(), E);
                gt::stop("write_f");
            }
            gt::stop("compute_Efield_and_write");
            t_write.stop();


            if(i < n_steps) { // last step is only for output
                if(T-t<tau)
                    tau=T-t;

                timestep(tau, t_adv, t_poisson, t_mpi_comm);

                t+=tau;
            }
            t_step.stop();

            if(mpicomm.rank == 0) {
                cout << "Time step took: " << t_step.total() << " s  (adv: "
                    << t_adv.total()  << ", poisson: " << t_poisson.total()
                    << ", write: " << t_write.total() << ", mpi_comm: "
                    << t_mpi_comm.total() << ")" << endl;
            }
            gt::stop("step");
        }
        total.stop();

        string fmt_str = "Runtime: %1% Commun: %2% Commun(Poisson): "
            "%3% Comput(Advect): %4% Comput(Poisson): %5% Prep: %6%";
        mpicomm.print(str(boost::format(fmt_str)
                    %total.total()%comm.total()%comm_poisson.total()
                    %advect.total()%poisson.total()%prep.total()));
        if(mpicomm.rank == 0) {
            cout << "Average time per step: " << total.total()/fp(n_steps)
                 << " s" << endl;

            std::ofstream fs("global_timer.data");
            fs << gt::sorted_output();
        }
    }

    void timestep(fp tau, timer& t_adv, timer& t_poisson, timer& t_mpi_comm) {
        gt::start("timestep");

        Index data_to_send_v=0; 
        Index data_to_send_x=0;

        for(size_t i=0;i<dx;i++) {
            if(bc_x[i] == bdry_cond::dirichlet)
                set_hom_dirichlet_bc(i);
            else {
                t_mpi_comm.start();
                gt::start("mpi_communication_x");
                set_periodic_bc(i,0.5*tau,data_to_send_x);
                gt::stop("mpi_communication_x");
                t_mpi_comm.stop();
            }

            t_adv.start();
            gt::start("advection_v");
            blocks->advection_v(i,0.5*tau);
            gt::stop("advection_v");
            t_adv.stop();

            blocks->swap();
        }

        t_poisson.start();
        gt::start("compute_Efield");
        solve_poisson(E);
        gt::stop("compute_Efield");
        t_poisson.stop();

        // We call advection_E only for v_1, ..., v_{dx}. Otherwise there is no
        // E field and the advection speed is zero. If an vxB term is added that
        // would change
        for(size_t i=dx;i<dx+dx;i++) {
            t_mpi_comm.start();
            gt::start("mpi_communication_v");
            set_periodic_bc(i,tau,data_to_send_v);
            gt::stop("mpi_communication_v");
            t_mpi_comm.stop();
             
            t_adv.start();
            gt::start("advection_E");
            blocks->advection_E(i,tau,E);
            gt::stop("advection_E");
            t_adv.stop();

            blocks->swap();
        }

        for(size_t i=0;i<dx;i++) {
            if(bc_x[i] == bdry_cond::dirichlet)
                set_hom_dirichlet_bc(i);
            else {
                t_mpi_comm.start();
                gt::start("mpi_communication_x");
                set_periodic_bc(i,0.5*tau,data_to_send_x);
                gt::stop("mpi_communication_x");
                t_mpi_comm.stop();
            }

            t_adv.start();
            gt::start("advection_v");
            blocks->advection_v(i,0.5*tau);
            gt::stop("advection_v");
            t_adv.stop();

            blocks->swap();
        }

        if(mpicomm.rank==0){
            cout << "data_to_send_x: " << data_to_send_x*sizeof(fp)*1e-9 
                 << " GB " << endl;
            cout << "data_to_send_v: " << data_to_send_v*sizeof(fp)*1e-9 
                 << " GB " << endl;
        }

        gt::stop("timestep");
    }


    //TODO: why is this called for the advection in x direction?
    void set_hom_dirichlet_bc(Index i) {
        if(total_num_nodes_x != 1){
            cout << "ERROR: Homogeneous boundary conditions not implemented "
                 << "for parallelization in x-direciton." << endl;
            exit(1);
        } 
        for(Index i=0;i<total_blocks;i++){
            fp* bdry = blocks->blocks[i]->get_boundary_storage(bdr_max);
            if(!gpu)
                fill(bdry, bdry+bdr_max_length, 0.0);
            #ifdef __CUDACC__
            else
                gpuErrchk(cudaMemset(bdry,0,sizeof(fp)*bdr_max_length));
            #endif
        }
    }
    

    void set_periodic_bc(Index i, fp tau, Index& data_to_send) {
        int block_num = (i<dx) ? 1 : num_blocks[i-dx];
        if(mpicomm.num_nodes[i]==1 && block_num==1)
            // We just set the boundary length to zero
            for(Index i=0;i<total_blocks;i++) {
                blocks->blocks[i]->get_boundary_storage(0);
            }
        else 
            communication(i, tau, data_to_send);
    }


    //handles both intranode and internode communication
    void communication(Index dim, fp tau, Index& data_to_send){

        vector<MPI_Request> reqs;

        cout << "in communication, dim=" << dim << ", total_blocks: " 
             << total_blocks << endl;

        Index bdr_tot_len = 0;
        Index bdr_tot_left = 0;
        Index bdr_tot_right = 0;
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) reduction(+:data_to_send) if(gpu) \
        lastprivate(bdr_tot_len,bdr_tot_left,bdr_tot_right)
        for(Index i=0;i<total_blocks;++i){
            array<Index,dv> id = blocks->get_id(i);

            bdr_len_info bli;
            if(dim<dx)
                bli = blocks->blocks[i]->pack_boundary(dim, tau, bdr_max_length);
            else
                bli = blocks->blocks[i]->pack_boundary(dim, tau, E[dim-dx], 
                                                       bdr_max_length);

            bdr_tot_len = bli.bdr_tot_len;
            bdr_tot_left = bli.bdr_tot_left;
            bdr_tot_right = bdr_tot_len - bdr_tot_left;
 
            int left_device = -1;
            int right_device = -1;

            if(dim>=dx){
                left_device = (mpicomm.num_nodes[dim]==1) ? 
                                      shift_left_periodic(id,dim-dx) : 
                                      shift_left(id,dim-dx);
                right_device = (mpicomm.num_nodes[dim]==1) ? 
                                      shift_right_periodic(id,dim-dx) : 
                                      shift_right(id,dim-dx);
            }

            //std::stringstream ss;
            //ss << "block_id: " << id << ", left_device: " << left_device
            //   << ", right device: " << right_device << endl;
            //#pragma omp critical
            //mpicomm.print(ss.str());

            Index lin_id_other = (dim>=dx) ? linearize_idx(slice(dim-dx, id),
                                             slice(dim-dx, num_blocks)) : i;

            if(left_device == -1){
                fp* bdry_send = blocks->blocks[i]->get_boundary_to_send();
                fp* bdry_recv = blocks->blocks[i]
                                 ->get_boundary_storage(bli.bdr_len)+bdr_tot_left; 
                
                fp* bdry_to;
                fp* bdry_from;
                if(parameters::cuda_aware_mpi || !gpu){
                    bdry_to = bdry_send;
                    bdry_from = bdry_recv;  
                } 
                else{
                    #ifdef __CUDACC__
                    gpuErrchk(cudaMemcpy(&bdr_send[i][0],bdry_send,
                                         bdr_tot_left*sizeof(fp),
                                         cudaMemcpyDeviceToHost));
                    bdry_to = &bdr_send[i][0];
                    bdry_from = &bdr_recv[i][bdr_tot_left];
                    #endif
                }
                
                MPI_Request req;

                MPI_Isend(bdry_to, bdr_tot_left, MPI_FP, 
                          mpicomm.rank_left(dim), lin_id_other,
                          MPI_COMM_WORLD, &req);

                data_to_send += bdr_tot_left;

                #pragma omp critical
                reqs.push_back(req);

                MPI_Irecv(bdry_from, bdr_tot_right, MPI_FP,
                          mpicomm.rank_left(dim), 
                          lin_id_other+total_blocks,
                          MPI_COMM_WORLD, &req);
                
                #pragma omp critical
                reqs.push_back(req);
            } else {
                #ifdef __CUDACC__
                fp* bdry_recv = blocks->blocks[left_device]->
                                   get_boundary_storage(bli.bdr_len);
                fp* bdry_send = blocks->blocks[i]->
                                   get_boundary_to_send();

                gpuErrchk(cudaMemcpy(bdry_recv,bdry_send,bdr_tot_left*sizeof(fp),
                                     cudaMemcpyDefault));
                #endif
            }

            if(right_device == -1){
                fp* bdry_send = blocks->blocks[i]->get_boundary_to_send() +
                                                                 bdr_tot_left;
                fp* bdry_recv = blocks->blocks[i]->get_boundary_storage(bli.bdr_len);

                fp* bdry_to;
                fp* bdry_from;
                if(parameters::cuda_aware_mpi || !gpu){
                    bdry_to = bdry_send;
                    bdry_from = bdry_recv;  
                } 
                else{
                    #ifdef __CUDACC__
                    gpuErrchk(cudaMemcpy(&bdr_send[i][bdr_tot_left],bdry_send,
                                         bdr_tot_right*sizeof(fp),
                                         cudaMemcpyDeviceToHost));
                    bdry_to = &bdr_send[i][bdr_tot_left];
                    bdry_from = &bdr_recv[i][0];
                    #endif
                }

                MPI_Request req;

                MPI_Isend(bdry_to, bdr_tot_right, MPI_FP,
                          mpicomm.rank_right(dim), 
                          lin_id_other+total_blocks,
                          MPI_COMM_WORLD, &req);

                data_to_send += bdr_tot_right;

                #pragma omp critical
                reqs.push_back(req);

                MPI_Irecv(bdry_from, bdr_tot_left, MPI_FP,
                          mpicomm.rank_right(dim), lin_id_other,
                          MPI_COMM_WORLD, &req);

                #pragma omp critical
                reqs.push_back(req);
            } else {
                #ifdef __CUDACC__
                fp* bdry_recv = blocks->blocks[right_device]->
                                   get_boundary_storage(bli.bdr_len)+bdr_tot_left;
                fp* bdry_send = blocks->blocks[i]->
                                   get_boundary_to_send()+bdr_tot_left;

                gpuErrchk(cudaMemcpy(bdry_recv,bdry_send,bdr_tot_right*sizeof(fp),
                                     cudaMemcpyDefault));
                #endif
            }

        }

        vector<MPI_Status> stats(reqs.size());
        MPI_Waitall(reqs.size(), &reqs[0], &stats[0]);

        #ifdef __CUDACC__
        if(gpu && !parameters::cuda_aware_mpi){
            if(mpicomm.num_nodes[dim]!=1){
                #pragma omp parallel for num_threads(num_threads) \
                schedule(static,1) 
                for(int i=0;i<total_blocks;++i){
                    array<Index,dv> id = blocks->get_id(i);

                    int left_device = -1;
                    int right_device = -1;

                    if(dim>=dx){
                        left_device = shift_left(id,dim-dx);
                        right_device = shift_right(id,dim-dx);
                    }

                    if(left_device == -1){
                        vp_block_gpu<dx,dv,f0>* vp_b_gpu =
                                 static_cast< vp_block_gpu<dx,dv,f0>* >(
                                              blocks->blocks[i].get());
                        fp* d_bdry = vp_b_gpu->d_bdr_data+bdr_tot_left;
                        gpuErrchk(cudaMemcpy(d_bdry,&bdr_recv[i][bdr_tot_left],
                                             bdr_tot_right*sizeof(fp),
                                             cudaMemcpyHostToDevice));
                    }
                    if(right_device == -1){
                        vp_block_gpu<dx,dv,f0>* vp_b_gpu =
                                 static_cast< vp_block_gpu<dx,dv,f0>* >(
                                              blocks->blocks[i].get());
                        fp* d_bdry = vp_b_gpu->d_bdr_data;
                        gpuErrchk(cudaMemcpy(d_bdry,&bdr_recv[i][0],
                                             bdr_tot_left*sizeof(fp),
                                             cudaMemcpyHostToDevice));
                    }
                }
            }
        }
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
        #endif

    }


    //first execution of gpu/mpi functions are much slower than the following
    //ones, therefore we exclude them in the measurements
    void solve_poisson(efield& E, bool compute_rho=true,
                       bool measure=true) {

        if(measure)
            gt::start("compute_rho");
        if(compute_rho) {
            timer t_rho; t_rho.start();
            rho = blocks->compute_rho();
            t_rho.stop();
            cout << "rho: " << t_rho.total() << endl;
        }
        if(measure)
            gt::stop("compute_rho");

        
        if(measure)
            gt::start("mpi_reduce");
        if(total_num_nodes_v != 1) {
            // MPI reduction to master process
            if(mpicomm.rank_v == 0)
                MPI_Reduce(MPI_IN_PLACE,rho.data(),rho.num_elements(),
                        MPI_FP,MPI_SUM,0,mpicomm.MPI_COMM_V);
            else
                MPI_Reduce(rho.data(), NULL, rho.num_elements(),
                        MPI_FP,MPI_SUM,0,mpicomm.MPI_COMM_V);
        }
        if(measure)
            gt::stop("mpi_reduce");

        if(measure)
            gt::start("poisson");
        if(mpicomm.rank_v == 0){
            // make sure that the -1.0 is added to the right-hand side of Poisson's
            // equation
            for (Index i = 0; i < rho.num_elements(); i++)
                rho.v[i] -= 1.0;

            timer t_poiss; 
            t_poiss.start();
            poisson_s->solve(E, rho);
            t_poiss.stop();
            cout << "poisson_s: " << t_poiss.total() << "\n";
        }
        if(measure)
            gt::stop("poisson");
        
        if(measure)
            gt::start("mpi_bcast");
        if(total_num_nodes_v != 1) {
            // MPI broadcast (such that all processes obtain the electric field
                      MPI_Bcast(E[0].data(),E[0].num_elements(),
                                MPI_FP,0,mpicomm.MPI_COMM_V);
            if (dx>1) MPI_Bcast(E[1].data(),E[1].num_elements(),
                                MPI_FP,0,mpicomm.MPI_COMM_V);
            if (dx>2) MPI_Bcast(E[2].data(),E[2].num_elements(),
                                MPI_FP,0,mpicomm.MPI_COMM_V);
        }
        if(measure)
            gt::stop("mpi_bcast");
    }



    /// This computed and write to disk certain physical quantities that are
    /// often of interest in Vlasov simulations (conserved quantities such as
    /// mass, l1, and l2 norm and the kinetic/electric energy).
    void write_variables(fp t, string filename, efield& E) {
        if(enable_output) {
            // invariants
            gt::start("compute_rho_and_invariants");
            fp mass, momentum, l1, l2, K_energy, entropy;
            rho = blocks->compute_rho_and_invariants(mass, momentum, l1, l2,
                      K_energy, entropy);
            gt::stop("compute_rho_and_invariants");

            solve_poisson(E, false);

            fp E_energy=0.0;
            {
                array<Index,dx_size> idx;
                fill(idx.begin(),idx.end(),0);
                array<Index,dx_size> e_space = e.x_part();
                do {
                    fp w=1.0;
                    for(size_t i=0;i<dx;i++) {
                        fp hx=(b[i]-a[i])/fp(e_space[i]);
                        w*=0.5*hx*gauss::w(idx[dx+i],e_space[dx+i]);
                    }

                    for(size_t i=0;i<dx;i++)
                        E_energy += 0.5*w*pow(E[i](idx),2);
                } while(iter_next(idx,e_space));
            }

            gt::start("mpi_reduce");
            array<fp,7> invariants = {mass, momentum, K_energy, l2, entropy, l1, E_energy};
            if(mpicomm.rank == 0)
                MPI_Reduce(MPI_IN_PLACE,&invariants[0],invariants.size(),
                        MPI_FP,MPI_SUM,0,MPI_COMM_WORLD);
            else
                MPI_Reduce(&invariants[0],NULL,invariants.size(),
                        MPI_FP,MPI_SUM,0,MPI_COMM_WORLD);
            gt::stop("mpi_reduce");
            invariants[6] /= total_num_nodes_v;


            cout << "writing evolution from rank: " << mpicomm.rank << " " << t
                 << endl;

            if(mpicomm.rank == 0) {
                std::ofstream fs(filename.c_str(),std::ofstream::app);
                fs.precision(16);
                fs << t << " " << invariants[0] << " " << invariants[1] 
                   << " " << invariants[6]+invariants[2] << " " << invariants[2]
                   << " " << invariants[6] <<  " " << sqrt(invariants[3])
                   << " " << invariants[4] << " " << invariants[5]
                   << endl;
                fs.close();
            }
        }
    }


    vector<fp> grid_points(size_t k) {

        Index np = mpicomm.num_nodes[k]*
                   ( (k<dx) ? 1 : blocks->num_blocks[k-dx]);
        Index N = e[k];
        Index o = e[d+k];
        Index num_cells_global = N*np;
        vector<fp> grid(num_cells_global*o);
        fp h=(b_global[k]-a_global[k])/fp(num_cells_global);
        for(Index j=0;j<num_cells_global;j++)
            for(Index oj=0;oj<o;oj++)
                grid[j*o+oj] = a_global[k] + h*j + h*gauss::x_scaled01(oj,o);

        return grid;
    }

    // write a snapshot as an netcdf file (both f and E)
    void write_fE_nc(string fn, efield& E) {

        // name the variables
        vector<string> str_dims;
        for(size_t i=0;i<dx;i++)
            str_dims.push_back(str(boost::format("x%1%")%(i+1)));
        for(size_t i=0;i<dv;i++)
            str_dims.push_back(str(boost::format("v%1%")%(i+1)));
        str_dims.push_back("dim");

        // determine the grid size of each of the variables over the whole
        // domain
        array<Index,d> elem = dof(e);
        vector<Index> elements(begin(elem), end(elem));
        for(size_t i=0;i<d;i++)
            elements[i] *= mpicomm.num_nodes[i];
        for(size_t i=0;i<dv;i++)
            elements[dx+i] *= blocks->num_blocks[i];
        elements.push_back(dx);
        NCWriterParallel ncw(fn, str_dims, elements);

        vector<NCWriterVariable> var_dims;
        for(size_t k=0;k<str_dims.size();k++)
            var_dims.push_back( ncw.add_variable(str_dims[k], {str_dims[k]}) );

        // E depends on x_1-x_dx and dim
        // (order must be reversed since we store our arrays in column major
        // form)
        vector<string> str_var_E(begin(str_dims), begin(str_dims)+dx);
        str_var_E.push_back("dim");
        NCWriterVariable var_E = ncw.add_variable("E", reverse(str_var_E));

        // f depends on variables x_1-x_dx and v_1 to v_dv
        // (order must be reversed since we store our arrays in column major
        // form)
        vector<string> str_var_f = str_dims;
        str_var_f.pop_back();
        NCWriterVariable var_f = ncw.add_variable("f", reverse(str_var_f));

        ncw.end_definitions();

        // grid points are only written by one process
        if(mpicomm.rank == 0) {
            for(size_t k=0;k<d;k++)
                var_dims[k].write(grid_points(k));

            vector<fp> v_dim;
            for(size_t k=0;k<dx;k++)
                v_dim.push_back(k);
            var_dims[d].write(v_dim);
        }

        // electric field is written by processes that span the
        // entire x domain having the same v domain on which the
        // electric field is actually computed
        if(mpicomm.rank_v == 0){

            array<Index,dx+1> elem_x;
            copy(begin(elem),begin(elem)+dx,begin(elem_x));
            elem_x[dx] = 1;

            array<Index,dx> nodes_x_id;
            int rank_x = mpicomm.rank_x;
            for(size_t i=0;i<dx;++i){
                nodes_x_id[i] = rank_x%mpicomm.num_nodes[i];
                rank_x/=mpicomm.num_nodes[i];
            }

            array<Index,dx+1> offset;
            for(size_t j=0;j<dx;++j)
                offset[j] = nodes_x_id[j]*elem_x[j];

            for(size_t k=0;k<dx;k++) {
                offset[dx] = k;
                //data mode has to be independent since not all processes
                //in MPI_COMM_WORLD are used 
                var_E.write(reverse(offset), reverse(elem_x), E[k].data(),
                            nc_data_mode::independent);
            }
        }

        for(Index i=0;i<blocks->total_blocks;i++) {
            write_info<d> wi = blocks->write_information(i);

            array<Index,d> offset;
            copy(begin(wi.offset), end(wi.offset), begin(offset));
            for(size_t k=0;k<d;k++)
                offset[k] = wi.offset[k] + e[k]*e[d+k]*mpicomm.node_id[k]*
                            ( (k<dx) ? 1 : blocks->num_blocks[k-dx] );

            var_f.write(reverse(offset), reverse(dof(e)), wi.data.data.data());
        }
    }
    

private: 
    void enable_peer_to_peer() {
        #ifdef __CUDACC__
        if(parameters::enable_p2p) {
            int device_count;
            cudaGetDeviceCount(&device_count);

            for(int i=0;i<device_count;i++) {
                cudaSetDevice(i);
                for(int j=0;j<device_count;j++) {
                    int is_able;
                    cudaDeviceCanAccessPeer(&is_able, i, j);
                    cout << "device i=" << i << " j=" << j 
                         << " able-peer-to-peer=" << is_able << endl;
                    if(is_able) // automatically excludes i=j
                        gpuErrchk( cudaDeviceEnablePeerAccess(j, 0) );
                }
            }

            cudaSetDevice(0);
        }
        #endif
    }


    Index get_idx(array<Index,dv> id) {
        Index k = 0;
        Index stride = 1;
        for(size_t i=0;i<dv;i++) {
            k += stride*id[i];
            stride *= num_blocks[i];
        }
        return k;
    }

    long shift_left_periodic(array<Index,dv> id, Index dim) {
        id[dim] = neg_modulo(long(id[dim])-1,num_blocks[dim]);
        return get_idx(id);
    }

    long shift_right_periodic(array<Index,dv> id, Index dim) {
        id[dim] = (id[dim]+1)%num_blocks[dim];
        return get_idx(id);
    }

    long shift_left(array<Index,dv> id, Index dim) {
        if (id[dim] > 0) {
            id[dim]--;
            return get_idx(id);
        } else
            return -1;
    }

    long shift_right(array<Index,dv> id, Index dim) {
        if(id[dim] < num_blocks[dim]-1) {
            id[dim]++;
            return get_idx(id);
        } else
            return -1;
    }
};
