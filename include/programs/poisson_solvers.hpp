#pragma once

#include <cstdio>
#include <generic/common.hpp>
#include <generic/fft.hpp>
#include <container/domain.hpp>
#include <algorithms/dg.hpp>
#include <algorithms/rho.hpp>

template<size_t dx>
struct poisson_solver {

    using domain_x = multi_array<fp,dx+dx>;
    using efield  = array< domain_x, dx>;

    virtual void solve(efield& E, domain_x& rho) = 0; 

    virtual ~poisson_solver() {};
};



template<size_t dx, size_t dv>
struct fft_poisson_solver : public poisson_solver<dx> {

    using domain_x = multi_array<fp,dx+dx>;
    using efield  = array< domain_x, dx>;

    array<fp,dx> a_global;
    array<fp,dx> b_global;
    array<Index,dx+dx> e_xpart;
    array<Index,dx+dx> e_global;
    mpi_process<dx+dv> &mpicomm;

    array<domain_x, dx> total_continuous;
    array<domain_x, dx> total_split;
    efield E_tmp;
    fftw_plan plan_fft;
    array< fftw_plan,             dx> plan_ifft;
    array< vector< fp >,          dx> f_in;
    array< vector< complex<fp> >, dx> f_out;
    vector<fp> f_tmp;

    int rank_x;
    int total_nodes_x{1};

    fft_poisson_solver() = default;    

    fft_poisson_solver( array<fp,dx> _a_x, array<fp,dx> _b_x, array<Index,dx+dx> _e_x,
                        mpi_process<dx+dv>& _mpicomm )
        : a_global(_a_x), b_global(_b_x), e_xpart(_e_x), mpicomm(_mpicomm)
    {
        MPI_Comm_rank(mpicomm.MPI_COMM_X, &rank_x); 
        for(size_t i=0;i<dx;++i) {
            total_nodes_x *= mpicomm.num_nodes[i];
            e_global[i] = e_xpart[i]*mpicomm.num_nodes[i]; 
            e_global[i+dx] = e_xpart[i+dx];
        }
        if(total_nodes_x > 1 && rank_x == 0 ){
            for(int i=0;i<dx;++i){
               total_continuous[i].resize(e_global); 
               total_split[i].resize(e_global);
            }
        }
        if(rank_x == 0) {
            int fft_size = prod(e_global);
            for(size_t i = 0; i < dx; ++i)
                E_tmp[i].resize(e_global);

            cout << "fft_size: " << fft_size << endl;
            for(size_t i=0;i<dx;i++) {
                f_in[i].resize(fft_size);
                f_out[i].resize(fft_size);
            }
            f_tmp.resize(fft_size);

            // Create a plan for FFTW.
            // We have to call reverse here as fftw expects row-major order.
            array<Index,dx> _fft_e = reverse(dof<dx>(e_global));
            array<int,dx> fft_e;
            copy(begin(_fft_e), end(_fft_e), begin(fft_e));
            plan_fft  = fftw_plan_dft_r2c(dx, &fft_e[0], &(f_in[0][0]), 
                        (fftw_complex*)&(f_out[0][0]), FFTW_ESTIMATE);
            for(size_t i=0;i<dx;i++)
                plan_ifft[i] = fftw_plan_dft_c2r(dx, &fft_e[0],
                        (fftw_complex*)&(f_out[i][0]), &(f_in[i][0]),
                        FFTW_ESTIMATE);
        }
    }

    ~fft_poisson_solver() {
        if(rank_x==0){
            fftw_destroy_plan(plan_fft);
            for(size_t i=0;i<dx;i++)
                fftw_destroy_plan(plan_ifft[i]);
            //fftw_cleanup();
        }
    }

    void solve_poisson_fourier() {
        // In FFTW the dimension that is contiguous in memory only has
        // n/2+1 elements
        array<Index,dx> n_fourier = dof<dx>(e_global);
        Index normalization = prod(n_fourier); // Note: nx*ny not (nx/2+1)*ny
        n_fourier[0] = n_fourier[0]/2+1;

        fftw_execute(plan_fft);
        for(auto i : range<dx>(n_fourier)) {
            array<fp,dx> freq;
            freq[0] = i[0];
            for(size_t k=1;k<dx;k++)
                freq[k] = (i[k]<n_fourier[k]/2+1) ? i[k] : -(n_fourier[k]-i[k]);

            fp freq_sq = 0.0;
            for(size_t k=0;k<dx;k++) {
                freq[k] *= 2.0*M_PI/(b_global[k]-a_global[k]);
                freq_sq += pow(freq[k],2);
            }

            // do the computation
            Index idx = linearize_idx(i,n_fourier);
            complex<fp> v = f_out[0][idx];
            fp common_factor = (idx==0) ? 0.0 : 1.0/freq_sq/fp(normalization);
            for(size_t k=0;k<dx;k++)
                f_out[k][idx] = -complex<fp>(0,freq[k])*common_factor*v;
        }
       
        for(size_t i=0;i<dx;i++)
            fftw_execute(plan_ifft[i]);
    }

    void split_2_continuous(){
         array<Index,3> dof = {1,1,1};
         array<Index,3> nodes = {1,1,1};
         for(Index i=0;i<dx;++i){
             dof[i] = e_xpart[i]*e_xpart[dx+i];
             nodes[i] = mpicomm.num_nodes[i];
         }
         size_t g{0};
         for(Index nz=0;nz<nodes[2];++nz)
             for(Index k=0;k<dof[2];++k)
                 for(Index ny=0;ny<nodes[1];++ny)
                     for(Index j=0;j<dof[1];++j)
                         for(Index nx=0;nx<nodes[0];++nx)
                             for(Index i=0;i<dof[0];++i)
                                 total_continuous[0].v[g++] =
               total_split[0].v[i + dof[0]*(j + dof[1]*(k + dof[2]*(nx + nodes[0]*(ny + nodes[1]*nz))))];
    }

    void continuous_2_split(){
         array<Index,3> dof = {1,1,1};
         array<Index,3> nodes = {1,1,1};
         for(Index i=0;i<dx;++i){
             dof[i] = e_xpart[i]*e_xpart[dx+i];
             nodes[i] = mpicomm.num_nodes[i];
         }
         for(Index dim=0;dim<dx;++dim){
             size_t g{0};
             for(Index nz=0;nz<nodes[2];++nz)
                 for(Index ny=0;ny<nodes[1];++ny)
                     for(Index nx=0;nx<nodes[0];++nx)
                         for(Index k=0;k<dof[2];++k)
                             for(Index j=0;j<dof[1];++j)
                                 for(Index i=0;i<dof[0];++i)
                                     total_split[dim].v[g++] = 
               total_continuous[dim].v[i + dof[0]*(nx + nodes[0]*(j + dof[1]*(ny + nodes[1]*(k + dof[2]*nz))))];
         }
    }

    void solve(efield& E, domain_x& rho) {

        if(total_nodes_x > 1) {
            MPI_Gather(rho.data(), rho.num_elements(), MPI_FP, 
                       total_split[0].data(), rho.num_elements(), MPI_FP,
                       0, mpicomm.MPI_COMM_X);
            if(rank_x==0) {
                split_2_continuous(); 
                interpolate_to_equidistant_grid<dx>(total_continuous[0], f_in[0], e_global, f_tmp);
                solve_poisson_fourier();
                interpolate_to_dg_grid(f_in, total_continuous, e_global, E_tmp);
                continuous_2_split();
            }
            MPI_Barrier(mpicomm.MPI_COMM_X);
            for(int i=0;i<dx;++i){
                MPI_Scatter(total_split[i].data(), E[i].num_elements(), MPI_FP,
                            E[i].data(), E[i].num_elements(), MPI_FP,
                            0, mpicomm.MPI_COMM_X);
            }
        } else {
            interpolate_to_equidistant_grid<dx>(rho, f_in[0], e_global, f_tmp);
            solve_poisson_fourier();
            interpolate_to_dg_grid(f_in, E, e_global, E_tmp);
        }
    }
    
};

    



template<size_t dx, size_t dv>
struct dg_poisson_solver : public poisson_solver<dx> {

    using domain_x = multi_array<fp,dx+dx>;
    using efield  = array< domain_x, dx>;

    array<fp,dx> a_xpart;
    array<fp,dx> b_xpart;
    array<Index,dx+dx> e_xpart;
    mpi_process<dx+dv> &mpicomm;

    domain_x PHI; //electric potential
    domain_x PHI_old;

    array< vector<fp>, dx> data_left;
    array< vector<fp>, dx> data_right;

    array< vector<fp>, dx> bdry_left;
    array< vector<fp>, dx> bdry_right;

    array< unique_ptr<dg_local_poisson_matrix<dx>>, dx> dg_lm;
    fp sigma;
    fp cg_tol;
    Index cg_maxiter;
    Index cg_miniter;

    size_t counter;
    size_t num_nodes_x, num_nodes_v;

    int total_iterations{0};

    dg_poisson_solver() = default;

    dg_poisson_solver(array<fp,dx> _a_x, array<fp,dx> _b_x, array<Index,dx+dx> _e_x,
                      mpi_process<dx+dv>& _mpicomm, fp _sigma, fp _cg_tol, Index _cg_maxiter, 
                      domain_x& start, fp step_size)
        : a_xpart(_a_x), b_xpart(_b_x), e_xpart(_e_x), mpicomm(_mpicomm), PHI(start), PHI_old(start),
          sigma(_sigma), cg_tol(_cg_tol), cg_maxiter(_cg_maxiter), counter(0), num_nodes_x(1), num_nodes_v(1)
    {
        //fp approx_condition_n{0.0};
        for(size_t dim = 0; dim < dx; ++dim) {
            dg_lm[dim] = make_dg_local_poisson<dx>(e_xpart[dx+dim], dim, sigma, a_xpart, b_xpart, e_xpart);
            int s = e_xpart[dx+dim]*prod(slice_no<dx>(dim,e_xpart));
            data_left[dim].resize(s);
            data_right[dim].resize(s);
            bdry_left[dim].resize(s);
            bdry_right[dim].resize(s);
            num_nodes_x *= mpicomm.num_nodes[dim];
            //approx_condition_n += pow(e_xpart[dim]*e_xpart[dim+dx]*mpicomm.num_nodes[dx+dim]/2,2);
        }
        //cg_tol /= approx_condition_n;
        for(size_t i = 0; i < dv; ++i)
            num_nodes_v *= mpicomm.num_nodes[dx+i];
      
        //assumes step size less than one
        cg_miniter = 4*sqrt(1.0/step_size);
    }

    
    void collect_bdry_data(fp* __restrict__ in, size_t dim) {

        array<Index, dx+dx> idx, max1;
        fill(idx.begin(), idx.end(), 0);
        Index stride_o = 1, stride_N;
        for(size_t i = 0; i < dim; ++i)
            stride_o *= e_xpart[i]*e_xpart[dx+i];
        stride_N = stride_o*e_xpart[dx+dim];
        max1 = e_xpart;
        max1[dim] = 0;
        max1[dx+dim] = 0;
        do {
            Index lin_idxl = PHI.linear_idx(idx);                          
            Index lin_idxr = lin_idxl + stride_N*(e_xpart[dim]-1);
            Index bdry_index = get_index_slice<dx>(dim,idx,e_xpart);
            for(Index i = 0; i < e_xpart[dx+dim]; ++i) {
                bdry_left[dim][i + bdry_index] = in[lin_idxl + i*stride_o];
                bdry_right[dim][i + bdry_index] = in[lin_idxr + i*stride_o];
            }
        } while( iter_next_memorder(idx, max1) );
    }
    

    void mpi_bdry(fp* __restrict__ in) {

        vector<MPI_Request> reqs(0);
        for(size_t dim = 0; dim < dx; ++dim){

            collect_bdry_data(in, dim);

            if(mpicomm.num_nodes[dim]==1) {
                data_right[dim] = bdry_left[dim];
                data_left[dim] = bdry_right[dim];
            } else {
                Index bdry_len_x = bdry_left[dim].size();
                /*
                cout << "comm: " << mpicomm.node_id << " " << mpicomm.rank
                     << " right endpoint " << mpicomm.rank_right(dim)
                     << " left endpoint " << mpicomm.rank_left(dim) << endl;
                */
                MPI_Request req;                
                MPI_Isend(&bdry_left[dim][0],  bdry_len_x, MPI_FP, mpicomm.rank_left(dim), 
                          1, MPI_COMM_WORLD, &req);
                reqs.push_back(req);

                MPI_Isend(&bdry_right[dim][0], bdry_len_x, MPI_FP, mpicomm.rank_right(dim),
                          2, MPI_COMM_WORLD, &req);
                reqs.push_back(req);

                MPI_Irecv(&data_right[dim][0], bdry_len_x, MPI_FP, mpicomm.rank_right(dim),
                          1, MPI_COMM_WORLD, &req);
                reqs.push_back(req);

                MPI_Irecv(&data_left[dim][0],  bdry_len_x, MPI_FP, mpicomm.rank_left(dim),  
                          2, MPI_COMM_WORLD, &req);
                reqs.push_back(req);
            }
        }
        vector<MPI_Status> stats(reqs.size());
        MPI_Waitall(reqs.size(), &reqs[0], &stats[0]);
    }
    

    double application(fp* __restrict__ in, fp* __restrict__ out) {
        
        mpi_bdry(in);

        Index single_out = dx-1;
        double mean{0.0};

        #pragma omp parallel for schedule(static) reduction(+:mean)
        for(Index i=0;i<e_xpart[single_out];i++) {
            array<Index, dx+dx> idx, _idx;
            array<Index,dx+dx> max1 = e_xpart;
            max1[single_out] = 0;
            fill(_idx.begin(), _idx.end(), 0);
            do {
                idx = _idx;
                idx[single_out] = i;
                Index lin_idx = PHI.linear_idx(idx);
                double s=0.0, W=1.0;
                for(size_t dim = 0; dim < dx; ++dim) {
                    s += dg_lm[dim]->compute(in, idx, lin_idx, e_xpart, data_right, data_left);
                    W *= gauss::w(idx[dx+dim],e_xpart[dx+dim])*0.5;
                }
                out[lin_idx] = s;
                mean += s*W;
            } while( iter_next_memorder(_idx, max1) );      
        }    
        return mean;
    }

 
    fp mpi_inner_product(domain_x& r1, domain_x& r2){
        fp local_ip = std::inner_product(r1.begin(), r1.end(), r2.begin(), 0.0);
        if(num_nodes_x == 1)
            return local_ip;
        else {
            fp global_ip = 0;
            MPI_Allreduce(&local_ip, &global_ip, 1, MPI_FP, MPI_SUM, mpicomm.MPI_COMM_X);
            return global_ip;
        }
    }

    double mean(domain_x& f1){
        double mean=0.0;
        double H=1.0;
        for(size_t i=0;i<dx;++i) {
            H*=0.5/fp(e_xpart[i]);
        }
        array<Index,dx+dx> idx = {0};
        do {
            double W=1.0;
            for(size_t i=0;i<dx;++i)
                W*=gauss::w(idx[dx+i],e_xpart[dx+i]); 
            mean+=f1(idx)*W*H;
        } while( iter_next_memorder(idx,e_xpart) );
        return mean;
    }
 
    template<size_t s> 
    array<double,s> mpi_add(array<double,s>& local) {
        if (num_nodes_x == 1)
            return local;
        else {
            array<double,s> global = {0.0};
            MPI_Allreduce(&local[0], &global[0], s, MPI_DOUBLE, MPI_SUM, mpicomm.MPI_COMM_X); 
            for(double& e : global)
                e /= num_nodes_x;
            return global;
        }
    }

    void solve_poisson_dg(domain_x& rho) {
        timer t_prep, t_total;
        t_total.start(); t_prep.start();

        fp alpha, beta, rnew{0.0};
        domain_x r(e_xpart), w(e_xpart);

        Index l = prod(e_xpart);
        array<Index,dx+dx> idx;
        fill(idx.begin(), idx.end(), 0);     
        double inv_cellnumber{1.0};
        for(size_t i = 0; i<dx; ++i)
            inv_cellnumber /= fp(e_xpart[i]);

        double mean_x0{0.0};
        do {
            double W{1.0};
            for(size_t dim=0;dim<dx;++dim)
                W*=gauss::w(idx[dx+dim], e_xpart[dx+dim])*0.5; 
            PHI_old(idx) = 2*PHI(idx) - PHI_old(idx);
            mean_x0 += PHI_old(idx)*W*inv_cellnumber;
        } while(iter_next_memorder(idx,e_xpart));

        application(&PHI_old.v[0],&w.v[0]);

        fill(idx.begin(), idx.end(), 0); 
        //double c0{0.0}; 
        double mean_r0{0.0};
        double norm_b{0.0};
        do {
            fp W{1.0};
            for(size_t dim = 0; dim < dx; dim++) {
                W *= gauss::w(idx[dx+dim], e_xpart[dx+dim])*0.5;
            }
            r(idx) = -rho(idx)*W - w(idx);
            //c0 += (-rho(idx)*W + r(idx))*PHI_old(idx);
            norm_b  += rho(idx)*W*rho(idx)*W;
            mean_r0 += r(idx)*W*inv_cellnumber; 
        } while( iter_next_memorder(idx, e_xpart));

        array<double,3> local = {mean_x0, mean_r0, norm_b};
        array<double,3> global = mpi_add(local);
        norm_b = global[2]*num_nodes_x;
        for(Index i = 0; i < l; ++i) {
             PHI_old.v[i] -= global[0];
             r.v[i] -= global[1];
        }
        domain_x p(r);
        fp rold = mpi_inner_product(r,r);

        t_prep.stop();

        //double zeta{0.0}; 
        //constexpr Index delay{10};
        //Index miniter = max(delay,cg_miniter); 
        //array<double,delay> termination_vector={0.0};

        Index iter;
        timer t_appl, t_non_appl;
        for(iter = 0; iter < cg_maxiter; ++iter ) {

            t_appl.start();
            double mean_w = application(&p.v[0],&w.v[0]);
            t_appl.stop();
         
            mean_w *= inv_cellnumber/num_nodes_x;
            MPI_Allreduce(MPI_IN_PLACE, &mean_w, 1, MPI_DOUBLE, MPI_SUM, mpicomm.MPI_COMM_X); 

            t_non_appl.start();
            alpha = rold/mpi_inner_product(p,w);

            #pragma omp parallel for simd
            for(Index i = 0; i < l; ++i) {
                PHI_old.v[i] += p.v[i]*alpha;
                r.v[i] -= (w.v[i]-mean_w)*alpha;
            }
            
            rnew  = mpi_inner_product(r,r);

            //c0 += alpha*rold;
            //termination_vector[iter%delay] = alpha*rold;
            if (iter > cg_miniter) {
                //zeta = sum(termination_vector);
                //if (zeta < cg_tol*c0)
                if(rnew < cg_tol*norm_b)
                    break;
            }
            beta  = rnew/rold;
            rold  = rnew;
            #pragma omp parallel for simd
            for(Index i = 0; i < l; ++i) 
                p.v[i] = p.v[i]*beta+r.v[i];
            t_non_appl.stop();
        }
        t_total.stop(); 

        counter++;
        total_iterations += iter+1;

        PHI.swap(PHI_old);

        if(mpicomm.rank==0) {
            cout << "mean(phi): " << mean(PHI) << endl;
            cout << "<r,r> at last iteration: " << rnew << endl;
            cout << "CG: iter: " << iter+1 << "\n";
            cout << "average number of iterations: " << total_iterations/fp(counter) << endl;
            cout << "CG non appl average: " << t_non_appl.total()/fp(iter+1) <<
                    ", CG appl average: " << t_appl.total()/fp(iter+1) << endl;
            cout << "CG non appl total: " << t_non_appl.total() <<
                    ", CG appl total: " << t_appl.total() << endl;
            cout << "CG total: " << t_total.total() << ", prepare: " << t_prep.total() <<
                    ", iterations: " << t_total.total() - t_prep.total() << endl;
        }
    }

    void calculate_efield(efield& E) {

        Index single_out = dx-1;
        #pragma omp parallel for schedule(static)
        for(Index i = 0; i < e_xpart[single_out]; ++i) {        
            array<Index,dx+dx> idx, _idx, idxx;
            std::fill(_idx.begin(), _idx.end(), 0.0);
            array<Index,dx+dx> max1 = e_xpart;
            max1[single_out] = 0;
            do {
                idx = _idx;
                idx[single_out] = i;
                for(size_t dim = 0; dim < dx; ++dim) {
                    fp h = (b_xpart[dim]-a_xpart[dim])/fp(e_xpart[dim]);
                    Index o = e_xpart[dx+dim];
                    double s = 0.0;
                    for(int k = 0; k < o; ++k) {
                        idxx = idx;
                        idxx[dx+dim] = k; 
                        s += PHI(idxx)*dg_lm[dim]->dl_g(k, idx[dx+dim])/h;  
                    }
                    E[dim](idx) = s;
                }
            } while( iter_next_memorder(_idx,max1));
        }
    }

    void solve(efield& E, domain_x& rho) {
            solve_poisson_dg(rho);
            calculate_efield(E);
    }

};




extern "C" {

    //LU factorization and solve
    //factorize once in the constructor, solve triangular systems cheaply
    void dgetrf_(int* M, int* N, double* A, int* LDA,
                 int* IPIV, int* INFO);
    void dgetrs_(char* TRANS, int* N, int* NRHS, double* A,
                 int* LDA, int* IPIV, double* B, int* LDB,
                 int* INFO);
    void sgetrf_(int* M, int* N, float* A, int* LDA,
                 int* IPIV, int* INFO);
    void sgetrs_(char* TRANS, int* N, int* NRHS, float* A,
                 int* LDA, int* IPIV, float* B, int* LDB,
                 int* INFO);
}


//this is just a 1d problem.
template<size_t dx, size_t o>
struct poisson_solver_1d_direct_LDG : public poisson_solver<dx>  {

    using domain_x = multi_array<fp,2*dx>;
    using efield   = array<domain_x,dx>;

    size_t N;
    fp a,b,h;

    vector<fp> globalA;
    vector<fp> globalB;
    vector<int> ipiv;

    poisson_solver_1d_direct_LDG(size_t _N, fp _a, fp _b)
        : N{_N}, a{_a}, b{_b}, h{(b-a)/fp(N)}
    {

        if(dx!=1){
            cout << "ERROR: this Poisson solver is currently only "
                 << "implemented for dx=1. (but dx=" << dx << ")\n";
            exit(1);
        }

        fp  L_0[o];
        fp  L_1[o];
        dg_coeff<o> dg_c;
        dg_c.lagrange_all(1.0, &L_1[0]);
        dg_c.lagrange_all(0.0, &L_0[0]);

        fp Aloc[o];
        fp Bmid_integral[o][o];
        fp Bmid_flux[o][o];
        fp Bleft_flux[o][o];
        fp Cmid[o][o];
        fp Cright[o][o];
        fp Cleft[o][o];

        for(size_t i=0;i<o;++i){
            Aloc[i] = 0.5*h*dg_c.w[i];
            for(size_t j=0;j<o;++j){
                Bmid_flux[i][j] = -L_1[i]*L_1[j];
                Bleft_flux[i][j] = L_0[i]*L_1[j];
                Bmid_integral[i][j] = 0.5*dg_c.w[j]*
                                      lagrange_derivative(dg_c.xi[j],i,o);
                Cright[i][j] = -L_1[i]*L_0[j];
                Cleft[i][j] =  -L_0[i]*L_1[j];
                Cmid[i][j] =    L_1[i]*L_1[j] + L_0[i]*L_0[j];
            }
        }

        int nsize = 2*N*o;
        globalB.resize(nsize,0.0);
        globalA.resize(nsize*nsize,0.0);
        ipiv.resize(2*nsize,0);
        
        //setting up the global matrix
        for(int n=0;n<N;++n){
            for(int i=0;i<o;++i){
                globalA[n*o*2*N*o + i*2*N*o + n*o + i] = Aloc[i];
                for(int j=0;j<o;++j){
                    globalA[n*o*2*N*o + i*2*N*o + N*o + n*o + j] = 
                                    Bmid_integral[i][j] + Bmid_flux[i][j]*(n<N-1);
                    if(n>0)
                        globalA[n*o*2*N*o + i*2*N*o + N*o + (n-1)*o + j] =
                                    Bleft_flux[i][j];
                }
            }
        }
        for(int n=N;n<2*N;++n){
            for(int i=0;i<o;++i){
                for(int j=0;j<o;++j){
                    globalA[n*o*2*N*o + i*2*N*o + (n-N)*o + j] = 
                            -Bmid_integral[j][i] - Bmid_flux[j][i]*(n<2*N-1);
                    if(n<2*N-1)
                        globalA[n*o*2*N*o + i*2*N*o + (n-N+1)*o + j] = 
                            -Bleft_flux[j][i];

                    globalA[n*o*2*N*o + i*2*N*o + n*o + j] = Cmid[i][j];
                    if(n<2*N-1)
                        globalA[n*o*2*N*o + i*2*N*o + (n+1)*o + j] = Cright[i][j];
                    if(n>N)
                        globalA[n*o*2*N*o + i*2*N*o + (n-1)*o + j] = Cleft[i][j];
                }
            }
        }

        //std::ofstream gmA("matrixA.txt");
        //for(int i=0;i<2*N*o;++i){
        //    for(int j=0;j<2*N*o;++j){
        //        gmA << std::setprecision(17) << globalA[j+2*N*o*i] << " ";
        //    }
        //    gmA << endl;
        //}
        //cout << std::setprecision(7) << endl;


        //factorize once and for all
        int info;
        #ifdef USE_SINGLE_PRECISION
        sgetrf_(&nsize, &nsize, globalA.data(), &nsize,
                ipiv.data(), &info);
        #else
        dgetrf_(&nsize, &nsize, globalA.data(), &nsize,
                ipiv.data(), &info);
        #endif

        if(info != 0){
            cout << "ERROR: factorize failed, info=" << info << endl;
            exit(1);
        }
    }


    ~poisson_solver_1d_direct_LDG(){}


    void solve(efield& E, domain_x& rho){

        fill(&globalB[0],&globalB[N*o],0.0);
        for(int i=0;i<N;++i){
            for(int j=0;j<o;++j){
                globalB[N*o + i*o + j] = rho.v[j+i*o]*h*0.5*gauss::w(j,o);
            }
        }

        int nsize = 2*N*o;
        int info;
        int nrhs = 1;
        char c = 'T';

        #ifdef USE_SINGLE_PRECISION
        sgetrs_(&c, &nsize, &nrhs, globalA.data(), &nsize, 
                ipiv.data(), globalB.data(), &nsize, &info);       
        #else
        dgetrs_(&c, &nsize, &nrhs, globalA.data(), &nsize, 
                ipiv.data(), globalB.data(), &nsize, &info);       
        #endif

        if(info != 0){
            cout << "ERROR: solving linear system failed,"
                 << "info=" << info << endl;
            exit(1);
        }

        for(int i=0;i<N;++i){
            for(int j=0;j<o;++j){
                E[0].v[j+i*o] = -globalB[j+i*o];
                rho.v[j+i*o] = globalB[j+i*o+N*o];
            }
        }

    }
    
};


template<size_t dx>
unique_ptr< poisson_solver<dx> > make_poisson_1d_LDG(Index N, Index o, fp a, fp b){

    if(o==1)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,1>(N, a, b));
    else if(o==2)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,2>(N, a, b));
    else if(o==3)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,3>(N, a, b));
    else if(o==4)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,4>(N, a, b));
    else if(o==5)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,5>(N, a, b));
    else if(o==6)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_LDG<dx,6>(N, a, b));
    else {
          cout << "ERROR: LDG poisson solver does not support o=" << o << endl;
          exit(1);
    }
}


extern "C" {

    void dpotrf_(char* UPLO, int* N, double* A, int* LDA,
                 int* INFO);
    void dpotrs_(char* UPLO, int* N, int* NRHS, double* A,
                 int* LDA, double* B, int* LDB, int* INFO);
    void spotrf_(char* UPLO, int* N, float* A, int* LDA,
                 int* INFO);
    void spotrs_(char* UPLO, int* N, int* NRHS, float* A,
                 int* LDA, float* B, int* LDB, int* INFO);

    //routines for banded matrix are faster
    void dgbtrf_(int* M, int* N, int* KL, int* KU, double *A, 
                 int* LDAB, int* IPIV, int* INFO);
    void dgbtrs_(char* TRANS, int* N, int* KL, int* KU,
                 int* NRHS, double* AB, int* LDAB, 
                 int* IPIV, double* B, int* LDB, int* INFO);
    void sgbtrf_(int* M, int* N, int* KL, int* KU, float *A, 
                 int* LDAB, int* IPIV, int* INFO);
    void sgbtrs_(char* TRANS, int* N, int* KL, int* KU,
                 int* NRHS, float* AB, int* LDAB, 
                 int* IPIV, float* B, int* LDB, int* INFO);
}


//NOTE: it is proven that if the rhs has order o, poisson is solved with order o
//      and its derivative has order o-1.
//      To get the full order o on the derivative, I interpolate the rhs of order o
//      to the gauss_legendre points of order o+1, then I differentiate the solution
//      on the gauss_legendre points of order o to get order o on the derivative.
//      However, as the two tests show, I get order o+1 on the derivative 
//      on these points too (probably just superconvergence on those points)

template<size_t dx, Index o_rhs, Index o_derivative>
struct poisson_solver_1d_direct_SIPG : public poisson_solver<dx> {

    using domain_x = multi_array<fp,2>;
    using efield   = array<domain_x,1>;

    Index N;        //total number of cells
    Index N_bdry;   //number of cells close to bdry 
                    // -> interior cells = N-2*N_bdry

    fp h_bdry;
    fp h_interior;
    fp inv_h_bdry;
    fp inv_h_interior;
    
    static constexpr Index o = o_derivative+1;

    domain_x rho_o;

    vector<fp> AA;
    vector<int> ipiv;

    fp dM0[o_derivative][o];
    fp dMN[o_derivative][o];
    fp dM[o_derivative][o];
    fp dR[o_derivative][o];
    fp dL[o_derivative][o];

    fp inv_h(int i){
        if(i<N_bdry || i>=N-N_bdry)
            return inv_h_bdry;
        else
            return inv_h_interior;
    }

    fp h(int i){
        if(i<N_bdry || i>=N-N_bdry)
            return h_bdry;
        else
            return h_interior;
    }

    poisson_solver_1d_direct_SIPG(Index _N, fp a, fp b, 
                                  Index _N_bdry, int ratio,
                                  fp sigma = (o-1)*(o-1)+1)
        : N{_N}, N_bdry{_N_bdry}
    {
        h_bdry = (b-a)/fp(ratio*(N-2*N_bdry) + 2*N_bdry);
        h_interior = ratio*h_bdry;

        if( N-2*N_bdry<0 ){
            cout << "ERROR: illegal number of boundary cells:\n"; 
            cout << "Total number of cells: " << N << endl;
            cout << "Number of boundary cells: " << N_bdry << endl;
            cout << "(results in " << N-2*N_bdry << " interior cells)" << endl;
            cout << "h_bdry: " << h_bdry << ", h_interior: " << h_interior << endl;
            exit(1);
        }
        if( N_bdry==1 ){
            cout << "ERROR: N_bdry=" << N_bdry << " but at least "
                 << "two boundary cells have to be considered.\n";
            exit(1);   
        }

        inv_h_interior = 1.0/(h_interior);
        inv_h_bdry = 1.0/(h_bdry);

        if(o_rhs!=o_derivative+1)
            rho_o.resize({Index(N),Index(o)});

        fp dL_0[o];
        fp dL_1[o];
        fp  L_0[o];
        fp  L_1[o];
        fp dL_g[o][o];

        dg_coeff<o> dg_c;
        for(int j = 0; j < o; ++j) {
            dL_0[j] = lagrange_derivative(0, j, o);
            dL_1[j] = lagrange_derivative(1, j, o);
            for (int k = 0; k < o; ++k) {
                dL_g[j][k] = lagrange_derivative(dg_c.xi[k], j, o);
            }
        }
        dg_c.lagrange_all(1.0, &L_1[0]);
        dg_c.lagrange_all(0.0, &L_0[0]);

        fp M0[o][o];
        fp MN[o][o];
        fp R[o][o];

        fp A[o][o];
        fp B[o][o];
        fp C[o][o];
        fp F0[o][o];
        fp FN[o][o];

        for(int i=0;i<o;++i){
            for(int j=0;j<o;++j){
                A[i][j] = 0.0;
                for(int k=0;k<o;++k){
                    A[i][j] += 0.5*dL_g[i][k]*dL_g[j][k]*dg_c.w[k];
                }

                F0[i][j] = ( dL_0[j]*L_0[i] + L_0[j]*dL_0[i] +
                            sigma*L_0[j]*L_0[i]);
                FN[i][j] = (-dL_1[j]*L_1[i] - L_1[j]*dL_1[i] +
                            sigma*L_1[j]*L_1[i]);

                B[i][j] = ( 0.5*dL_0[j]*L_0[i] + 0.5*L_0[j]*dL_0[i] +
                           sigma*L_0[j]*L_0[i]);
                C[i][j] = (-0.5*dL_1[j]*L_1[i] - 0.5*L_1[j]*dL_1[i] +
                           sigma*L_1[j]*L_1[i]);

                //not used since L[i][j] = R[j][i] (has to be symmetric)
                //L[i][j] = ( 0.5*dL_1[j]*L_0[i] - 0.5*L_1[j]*dL_0[i] -
                //           sigma*L_1[j]*L_0[i] );

                //the following 3 are only used at the boundary. Since
                //I assume at least two boundary cells h does not differ 
                //there and these matrices can be summed up.
                R[i][j] = (-0.5*dL_0[j]*L_1[i] + 0.5*L_0[j]*dL_1[i] -
                           sigma*L_0[j]*L_1[i] );
                M0[i][j] = A[i][j] + F0[i][j] + C[i][j];
                MN[i][j] = A[i][j] + FN[i][j] + B[i][j];

            }
        }


        //required if o!=o_derivative
        fp L_0d[o_derivative];
        fp L_1d[o_derivative];
        dg_coeff<o_derivative> dg_cd;
        dg_cd.lagrange_all(1.0, &L_1d[0]);
        dg_cd.lagrange_all(0.0, &L_0d[0]);
        fp s = 0.5;
        for(int j=0;j<o_derivative;++j){
            for(int k=0;k<o;++k){
 
                dM[j][k] =  0.5*lagrange_derivative(dg_c.xi[k],j,o_derivative)*
                            gauss::w(k,o)
                           +s*L_0d[j]*L_0[k]
                           -s*L_1d[j]*L_1[k];
                dM0[j][k] = 0.5*lagrange_derivative(dg_c.xi[k],j,o_derivative)*
                            gauss::w(k,o)
                           -s*L_1d[j]*L_1[k];
                dMN[j][k] = 0.5*lagrange_derivative(dg_c.xi[k],j,o_derivative)*
                            gauss::w(k,o)
                           +s*L_0d[j]*L_0[k];
                dL[j][k] =  (1.0-s)*L_0d[j]*L_1[k];
                dR[j][k] = -(1.0-s)*L_1d[j]*L_0[k];

            }
        }


        vector<fp> A_full(N*N*o*o,0.0);
        //dirichlet boundary conditions (if not homogeneous, adjust the rhs)
        //N_bdry > 1
        for(int i=0;i<o;++i){
            for(int j=0;j<o;++j){
                A_full[0*N*o*o + i*N*o + j+o*0    ] = M0[i][j]*inv_h(0);
                A_full[0*N*o*o + i*N*o + j+o*(0+1)] = R[i][j]*inv_h(0);
                A_full[(N-1)*N*o*o + i*N*o + j+o*(N-1)] = MN[i][j]*inv_h(N-1);
                A_full[(N-1)*N*o*o + i*N*o + j+o*(N-2)] = R[j][i]*inv_h(N-1);
            }
        }
        //interior
        for(int n=1;n<N-1;++n){
            for(int i=0;i<o;++i){
                for(int j=0;j<o;++j){
                    A_full[n*N*o*o + i*N*o + j+o*n ] = 
                              A[i][j]*inv_h(n) + 
                              
                              0.5*dL_0[j]*L_0[i]*inv_h(n) + 
                              0.5*L_0[j]*dL_0[i]*inv_h(n) +
                              sigma*L_0[j]*L_0[i]*(inv_h(n-1)+inv_h(n))*0.5 + 

                             -0.5*dL_1[j]*L_1[i]*inv_h(n) - 
                              0.5*L_1[j]*dL_1[i]*inv_h(n) +
                              sigma*L_1[j]*L_1[i]*(inv_h(n)+inv_h(n+1))*0.5;

                    A_full[n*N*o*o + i*N*o + j+o*(n-1)] = 
                              0.5*dL_1[j]*L_0[i]*inv_h(n-1) - 
                              0.5*L_1[j]*dL_0[i]*inv_h(n) -
                              sigma*L_1[j]*L_0[i]*(inv_h(n)+inv_h(n-1))*0.5;
                    A_full[n*N*o*o + i*N*o + j+o*(n+1)] = 
                             -0.5*dL_0[j]*L_1[i]*inv_h(n+1) + 
                              0.5*L_0[j]*dL_1[i]*inv_h(n) -
                              sigma*L_0[j]*L_1[i]*(inv_h(n)+inv_h(n+1))*0.5;
                }
            }
        }

        int k = 2*o-1;
        int cols = 1+3*k;
        int n = N*o;
        AA.resize(n*cols);
        //write to banded memory layout
        for(int i=0;i<n;++i){
            for(int j=max(0,i-k);j<min(n,i+k+1);++j)
                 AA[k+j-i+k + i*cols] = A_full[i*n + j];
        }
        ipiv.resize(N*o);

        int info;
        #ifdef USE_SINGLE_PRECISION
        sgbtrf_(&n, &n, &k, &k, AA.data(), &cols, ipiv.data(), &info);
        #else
        dgbtrf_(&n, &n, &k, &k, AA.data(), &cols, ipiv.data(), &info);
        #endif

        if(info != 0){
            cout << "ERROR: factorize failed, info=" << info << endl;
            exit(1);
        }

    }


    void solve_lin_system(fp* B){

        int info;
        char trans = 'N';
        int n = N*o;
        int k = 2*o-1;
        int nrhs = 1;
        int cols = 1+3*k;

        for(int i=0;i<N;++i)
            for(int j=0;j<o;++j)
                B[j+i*o] *= 0.5*gauss::w(j,o)*h(i);

        #ifdef USE_SINGLE_PRECISION
        sgbtrs_(&trans, &n, &k, &k, &nrhs, AA.data(), &cols,
                ipiv.data(), B, &n, &info);
        #else
        dgbtrs_(&trans, &n, &k, &k, &nrhs, AA.data(), &cols,
                ipiv.data(), B, &n, &info);
        #endif

        if(info != 0){
            cout << "ERROR: solving linear system failed,"
                 << "info=" << info << endl;
            exit(1);
        }
    }


    void differentiate(fp* f_dg, fp* df_dg){

        for(size_t j=0;j<o_derivative;++j){
            fp v=0.0;
            for(size_t k=0;k<o;++k){
                v += dM0[j][k]*f_dg[k];
                v += dR[j][k]*f_dg[k+o];
            }
            df_dg[j] = v/(gauss::w(j,o_derivative)*h_bdry*0.5);
        }
        for(int i=1;i<N-1;++i){
            for(size_t j=0;j<o_derivative;++j){
                fp v=0.0;
                for(size_t k=0;k<o;++k){
                    v += dM[j][k]*f_dg[k+o*i];
                    v += dR[j][k]*f_dg[k+o*(i+1)];
                    v += dL[j][k]*f_dg[k+o*(i-1)];
                }
                df_dg[j+o_derivative*i] = v/(gauss::w(j,o_derivative)*h(i)*0.5);
            }
        }
        for(size_t j=0;j<o_derivative;++j){
            fp v=0.0;
            for(size_t k=0;k<o;++k){
                v += dMN[j][k]*f_dg[k+o*(N-1)];
                v += dL[j][k]*f_dg[k+o*(N-2)];
            }
            df_dg[j+o_derivative*(N-1)] = v/(gauss::w(j,o_derivative)*h_bdry*0.5);
        }

    }


    //void differentiate_direct(fp* f_dg, fp* df_dg){
    //    fp inv_h = 1.0/h;
    //    for(int n=0;n<N;++n){
    //        for(size_t i=0;i<o_derivative;++i){
    //            fp v=0.0;
    //            fp x = gauss::x_scaled01(i,o_derivative);
    //            for(size_t j=0;j<o;++j){
    //                v += f_dg[j+o*n]*lagrange_derivative(x,j,o);
    //            }
    //            df_dg[i+o*n] = -v*inv_h;
    //        }
    //    }
    //}


    void interpolate(domain_x& rho_in, domain_x& rho_out){

        array<Index,2> idx={0,0};
        array<Index,2> eout = rho_out.shape();

        Index o_in  = rho_in.shape()[1];
        Index o_out = rho_out.shape()[1];

        do{

            fp xx = gauss::x_scaled01(idx[1],o_out);
            fp fout = 0.0;
            for(Index i=0;i<o_in;++i)
                fout += rho_in({idx[0],i})*lagrange(xx,i,o_in);

            rho_out(idx) = fout;

        } while(iter_next_memorder(idx,eout));
    }


    void solve(efield& E, domain_x& rho){

        assert( rho.shape()[1] == o_rhs && "order of solver different from rhs");
        assert(E[0].shape()[1] == o_derivative && "order of derivative is wrong");

        if(o_rhs==o_derivative+1){
          solve_lin_system(rho.data());
          differentiate(rho.data(),E[0].data());
        }
        else{
            interpolate(rho, rho_o); 
            solve_lin_system(rho_o.data());
            differentiate(rho_o.data(),E[0].data());

            //this is just done to interpolate the solution of the 
            //poisson problem back on the input grid
            interpolate(rho_o, rho);
        }

    }

};


template<size_t dx>
unique_ptr< poisson_solver<dx> > make_poisson_1d_SIPG(Index N, 
                                                      Index o_rhs, 
                                                      Index o_derivative, 
                                                      fp a, fp b, 
                                                      Index N_bdry=0, 
                                                      fp ratio=1.0){
    if(o_rhs==1) {
        if(o_derivative==1)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,1,1>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==0)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,1,0>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o_rhs=1 currently only the derivative "
                 << "of order o=0,1 implemented using SIPG.\n";
            exit(1);
        }
    } else if(o_rhs==2) {
        if(o_derivative==2)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,2,2>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==1)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,2,1>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o_rhs=2 currently only the derivative "
                 << "of order o=1,2 implemented using SIPG.\n";
            exit(1);
        }
    } else if(o_rhs==3) {
        if(o_derivative==3)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,3,3>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==2)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,3,2>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o_rhs=3 currently only the derivative "
                 << "of order o=2,3 implemented using SIPG.\n";
            exit(1);
        }
    } else if(o_rhs==4) {
        if(o_derivative==4)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,4,4>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==3)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,4,3>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o=4 currently only the derivative "
                 << "of order o=3,4 implemented using SIPG.\n";
            exit(1);
        }
    } else if(o_rhs==5) {
        if(o_derivative==5)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,5,5>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==4)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,5,4>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o_rhs=5 currently only the derivative "
                 << "of order o=4,5 implemented using SIPG.\n";
            exit(1);
        }
    } else if(o_rhs==6) {
        if(o_derivative==6)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,6,6>(
                                                    N,a,b,N_bdry,ratio));
        else if(o_derivative==6)
            return unique_ptr< poisson_solver<dx>>(
                           new poisson_solver_1d_direct_SIPG<dx,6,5>(
                                                    N,a,b,N_bdry,ratio));
        else{
            cout << "ERROR: for o_rhs=6 currently only the derivative "
                 << "of order o=5,6 implemented using SIPG.\n";
            exit(1);
        }
    } else {
        cout << "ERROR: SIPG poisson solver does not support o_rhs=" 
             << o_rhs << endl;
        exit(1);
    }
}



extern "C" {
    void dpttrf_(int* N, double* D, double* E, int* INFO);
    void dpttrs_(int* N, int* NRHS, double* D, double* E,
                 double* B, int* LDB, int* INFO);

    void spttrf_(int* N, float* D, float* E, int* INFO);
    void spttrs_(int* N, int* NRHS, float* D, float* E,
                 float* B, int* LDB, int* INFO);
}

template<size_t dx, size_t o>
struct poisson_solver_1d_direct_FD : public poisson_solver<dx> {

    using domain_x = multi_array<fp,2>;
    using efield   = array<domain_x,1>;

    size_t N;
    fp h;

    vector<fp> rhs_equi;
    vector<fp> diag;
    vector<fp> off_diag;
    vector<fp> E_equi;

    poisson_solver_1d_direct_FD(size_t _N, fp a, fp b)
        : N{_N}, h{(b-a)/fp(N*o)}
    {

        rhs_equi.resize(N*o+1);
        diag.resize(N*o-1);
        off_diag.resize(N*o-1);
        E_equi.resize(N*o+1);

        fp h2_inv = 1./(h*h);

        for(int i=0;i<N*o-1;++i){
            diag[i] = 2.0*h2_inv;
            off_diag[i] = -h2_inv;
        }

        int nn = N*o-1;
        int info;
        #ifdef USE_SINGLE_PRECISION
        spttrf_(&nn,diag.data(),off_diag.data(),&info);
        #else
        dpttrf_(&nn,diag.data(),off_diag.data(),&info);
        #endif
        if(info!=0){
            cout << "ERROR: faild to factorize linear system, info=" << info << "\n";
            exit(1);
        }

    }

    fp evaluate_dg(fp x, fp v[o]){

        fp y = 0.0;
        vector<fp> gx = gauss::all_x_scaled01(o);

        for(int i=0;i<o;++i){

            fp l = 1.0;
            for(int j=0;j<i;++j)
                l *= (x-gx[j])/(gx[i]-gx[j]);
            for(int j=i+1;j<o;++j)
                l *= (x-gx[j])/(gx[i]-gx[j]);

            y = y + v[i]*l;
        }
        
        return y;
    }

    //currently done on cell boundarys to smooth the things out
    void interpolate_to_equi(domain_x& rho){

        fill(rhs_equi.begin(),rhs_equi.end(),0.0);

        //std::ofstream out("to_equi"+std::to_string(N)+".data");
        for(int i=0;i<N;++i){

            fp v[o];
            for(int j=0;j<o;++j)
                v[j] = rho.v[j+i*o];


            for(int j=1;j<o;++j){

                fp x = j/fp(o);

                fp fv = evaluate_dg(x,v);

                rhs_equi[j+i*o] = fv;

            }

            fp f0 = evaluate_dg(0.0,v);
            fp f1 = evaluate_dg(1.0,v);
            if(i<N-1)
                rhs_equi[i*o+o] += 0.5*f1;
            if(i>=1)
                rhs_equi[i*o] += 0.5*f0;

            //for(int j=0;j<o;++j){
            //    fp hx = (1-0)/fp(N);
            //    fp x = 0 + hx*(i+j/fp(o)); 
            //    out << x << " " << rhs_equi[j+i*o] << endl;
            //}
            //if(i==N-1)
            //    out << 1 << " " << rhs_equi[N*o] << endl;

        }
    }


    fp interp_lin(fp x, fp yl, fp yr, fp xl, fp xr){
        return (yr-yl)*(x-xl)/(xr-xl)+yl;
    }

    //linear interpolation
    void interpolate_to_dg(domain_x& F_dg, vector<fp>& F_equi){

        for(int i=0;i<N;++i){
            for(int j=0;j<o;++j){

                fp x = gauss::x_scaled01(j,o);

                int idx = x*o;

                F_dg.v[j+i*o] = interp_lin(x, F_equi[i*o + idx], 
                                              F_equi[i*o + idx+1],
                                              idx/fp(o), (idx+1)/fp(o));
            }
        }
    }

    void solve_lin_system(){

        int nn = N*o-1;
        int one = 1;
        int info;

        #ifdef USE_SINGLE_PRECISION
        spttrs_(&nn, &one, diag.data(), off_diag.data(), 
                rhs_equi.data()+1, &nn, &info);
        #else
        dpttrs_(&nn, &one, diag.data(), off_diag.data(), 
                rhs_equi.data()+1, &nn, &info);
        #endif
        
        if(info!=0){
            cout << "ERROR: faild to solve linear system, info=" << info << "\n";
            exit(1);
        }
 
    }

    void differentiate(){
    
        fp h_inv = 1.0/h;
        std::ofstream out("df"+std::to_string(N)+".data");
        for(int i=1;i<N*o;++i){
            E_equi[i] = -(rhs_equi[i+1]-rhs_equi[i-1])*0.5*h_inv;

            out << i*h << " " << E_equi[i] << endl;
        }

        E_equi[0]   = -(-1.5*rhs_equi[0]+
                         2.0*rhs_equi[1]-
                         0.5*rhs_equi[2])*h_inv;

        E_equi[N*o] = -( 0.5*rhs_equi[N*o-2]-
                         2.0*rhs_equi[N*o-1]+
                         1.5*rhs_equi[N*o])*h_inv;
    }

    void solve(efield& E, domain_x& rho) {

        interpolate_to_equi(rho);
        
        solve_lin_system();

        interpolate_to_dg(rho,rhs_equi);

        differentiate();
        
        interpolate_to_dg(E[0],E_equi);
    }

};

template<size_t dx>
unique_ptr< poisson_solver<dx> > make_poisson_1d_FD(Index N, Index o, fp a, fp b){

    if(o==1)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,1>(N, a, b));
    else if(o==2)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,2>(N, a, b));
    else if(o==3)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,3>(N, a, b));
    else if(o==4)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,4>(N, a, b));
    else if(o==5)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,5>(N, a, b));
    else if(o==6)
        return unique_ptr< poisson_solver<dx>>(
                          new poisson_solver_1d_direct_FD<dx,6>(N, a, b));
    else {
          cout << "ERROR: make_dg_coeff_gpu does not support o=" << o << endl;
          exit(1);
    }
}


