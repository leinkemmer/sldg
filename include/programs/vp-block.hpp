#pragma once

#include <generic/common.hpp>
#include <container/domain.hpp>

struct bdr_len_info {
    Index bdr_len;
    Index bdr_tot_len;
    Index bdr_tot_left;

    bdr_len_info() : bdr_len(0), bdr_tot_len(0), bdr_tot_left(0) {}

    bdr_len_info(Index _bdr_len, Index _bdr_tot_len, Index _bdr_tot_left=0)
        : bdr_len(_bdr_len), bdr_tot_len(_bdr_tot_len), 
          bdr_tot_left(_bdr_tot_left) {}
};

struct weno_bdry_transfer_info{

    fp* bdry_left_snd;
    fp* bdry_right_snd;
    fp* bdry_left_rcv;
    fp* bdry_right_rcv;
    size_t size;

    weno_bdry_transfer_info() :
        bdry_left_snd(nullptr),
        bdry_right_snd(nullptr),
        bdry_left_rcv(nullptr),
        bdry_right_rcv(nullptr),
        size(0) {}

    weno_bdry_transfer_info(fp* d_bdry_l_snd,
                            fp* d_bdry_r_snd,
                            fp* d_bdry_l_rcv,
                            fp* d_bdry_r_rcv,
                            size_t _size)
        : bdry_left_snd(d_bdry_l_snd),
          bdry_right_snd(d_bdry_r_snd),
          bdry_left_rcv(d_bdry_l_rcv),
          bdry_right_rcv(d_bdry_r_rcv),
          size(_size) {}
};

template<size_t dx, size_t dv>
Index bdr_adv_direction(array<Index,2*(dx+dv)> e, size_t dim){

    constexpr size_t d = dx+dv;

    Index r=1;
    if(dim<dx) {
        for(size_t k=0;k<dx;++k)
            if(k!=dim)
                r *= e[k]*e[d+k];
    } else {
        for(size_t k=dx;k<d;++k)
            if(k!=dim)
                r *= e[k]*e[d+k];
    }

    return r;
}

enum class source_type {no_source, time_dependent_source, constant_source};

template<size_t dx, size_t dv>
struct source_f{

    const fp sigma_x2inverse;
    const fp t0;
    const fp sigma_t2inverse;
    fp C;
    bool source_used;
    fp start_adjust_domain;
    source_type type;

    #ifdef __CUDACC__
    __host__ __device__
    #endif
    source_f(fp L, fp _t0, source_type st)
        : sigma_x2inverse(1.0/(0.1*L*0.1*L)), t0(_t0),
          sigma_t2inverse(1.0/(t0*t0*0.25)), type(st)
    {
        fp sigma_t = t0*0.5;
        if (type==source_type::time_dependent_source){
            fp max_at = sigma_t*(1.0+sqrt(3.0));
            start_adjust_domain = max_at + 4.0*sigma_t;
            source_used = true;
            C = 1.0/(compute_C(t0,sigma_t)*pow(2.0*M_PI,fp(dv)/2.0));
        } else if (type==source_type::constant_source) {
            //H(t0-t), H = heavyside function
            start_adjust_domain = t0;
            source_used = true;
            C = 1.0/(t0*pow(2.0*M_PI,fp(dv)/2.0));
        } else { //if (type==source_type::no_source) {
            start_adjust_domain = 0.0;
            source_used = false;
        }
    }

    #ifdef __CUDACC__
    __device__ __host__
    #endif
    fp time_dependent_source(fp* xv, fp t){
        fp x = xv[0];
        fp s = exp(-x*x*0.5*sigma_x2inverse); 
        for(size_t i=0;i<dv;++i)
            s *= exp(-xv[i+1]*xv[i+1]*0.5);
        return C*t*t*exp(-(t-t0)*(t-t0)*0.5*sigma_t2inverse)*s;
    }

    #ifdef __CUDACC__
    __device__ __host__
    #endif
    fp constant_source(fp* xv, fp t){
        if(t<t0){
            fp x = xv[0];
            fp s = exp(-x*x*0.5*sigma_x2inverse); 
            for(size_t i=0;i<dv;++i)
                s *= exp(-xv[i+1]*xv[i+1]*0.5);
            return C*s;
        } else
            return 0.0;
    }

    bool is_used(){
        return source_used;
    }

  private:

    //for time dependent source
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    fp compute_C(fp t0, fp sigma_t){
        constexpr size_t N = 10000;
        constexpr fp h = 10000.0/fp(N);
        fp C = 0.0;
        for(size_t i=0;i<N;++i){
            fp ti = (i+0.5)*h;
            C += ti*ti*exp(-(ti-t0)*(ti-t0)/(2.0*sigma_t*sigma_t));
        }
        return C*h;
    }

};


template<size_t dx, size_t dv, fp (*f0)(fp*)>
struct vp_block {
    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;
    
    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;
    using domain_x = multi_array<fp,2*dx>;
    using efield   = array< domain_x, dx>;
   
    virtual void init() = 0;

    virtual void advection_v(int dim, fp tau) = 0;
    virtual void advection_E(int dim, fp tau, efield& E) = 0;
    virtual void swap() = 0;

    virtual bdr_len_info pack_boundary(Index dim, fp tau,
            multi_array<fp,2*dx>& F, Index bdr_max_length) = 0;
    virtual bdr_len_info pack_boundary(Index dim, fp tau,
            Index bdr_max_length) = 0;
    virtual bdr_len_info pack_boundary_fine2coarse(fp tau, 
            Index bdr_max_length) = 0;
    virtual void unpack_boundary_coarse2fine(Index bdr_max_length) = 0;
    virtual fp* get_boundary_storage(Index bdr_len) = 0;
    virtual fp* get_boundary_to_send() = 0;

    virtual domain_x compute_rho() = 0;
    virtual domain_x compute_rho_and_invariants( fp& mass, fp& momentum, 
                     fp& l1, fp& l2, fp& K_energy, fp& entropy) = 0;

    //1x1v only at the moment
    virtual domain_x compute_rho_invariants_fluxes( fp& mass, 
                       fp& momentum, fp& l1, fp& l2, fp& K_energy, fp& entropy,
                       fp& particle_flux_left, fp& particle_flux_right,
                       fp& energy_flux_left, fp& energy_flux_right,
                       bool compute_meanvelocity_and_temperature) = 0;
    virtual void collision_BGK(fp dt, fp nu) = 0;
    virtual void collision_BGK(fp dt, fp nu, multi_array<fp,2*d>& f_half) = 0;

    virtual void source(fp dt, fp t, source_f<dx,dv> f) = 0;
    virtual bool check_reduce_domain_bounds(fp percent_reduce) = 0;
    virtual void reduce_domain_bounds(fp percent_reduce = 0.1) = 0;
    virtual void weno_limiter_1d(int dim, troubled_cell_indicator tci,
                                 troubled_cell_modifier tcm) = 0;
    virtual weno_bdry_transfer_info pack_for_weno_limiter(
                                          int grid_size_ratio) = 0;

    // this gives a pointer to the data residing on the cpu (used for writing to
    // disk)
    virtual domain<d>& get_data() = 0;

    //swap and copy with a multi_array from the outside,
    //required for predictor-corrector schemes
    virtual void swap_data(multi_array<fp,2*d>& f) = 0;
    virtual void copy_data(multi_array<fp,2*d>& f) = 0;

    virtual ~vp_block() {};
};


template<size_t d>
void init_firsttouch(fp (*f)(fp*), domain<d>& in) {
    Index single_out = 3;
    #pragma omp parallel for schedule(static)
    for(Index i=0;i<in.extension[single_out];i++) {
        array<Index,2*d> max1=in.extension;
        max1[single_out]=0;
        array<Index,2*d> _idx; fill(_idx.begin(),_idx.end(),0);

        fp x[d];

        do {
            array<Index,2*d> idx = _idx;
            idx[single_out] = i;

            for(size_t i=0;i<d;i++) {
                fp dx = (in.b[i]-in.a[i])/fp(in.extension[i]);
                fp xx = gauss::x_scaled01(idx[d+i],in.extension[d+i]);
                x[i] = in.a[i] + idx[i]*dx + dx*xx;
            }
            in.data(idx)=f(x);
        } while(iter_next_memorder(_idx,max1));
    }
}

template<size_t dx, size_t dv>
void init_v_values(array_dxdv<fp,dx,dv> a, array_dxdv<fp,dx,dv> b, 
        array<Index,2*(dx+dv)> e, array<multi_array<fp,2*dv>,dv>& v_values) {

    // initialize an array with (v,w)
    array<Index,2*dv> max_v;
    for(size_t i=0;i<dv;i++) {
        max_v[i]    = e[dx+i];
        max_v[dv+i] = e[dx+dv+dx+i];
    }
   
    for(size_t i=0;i<dv;i++)
        v_values[i].resize(max_v);
    
    array<Index,2*dv> idx; fill(idx.begin(), idx.end(), 0);
    do {
        for(size_t i=0;i<dv;i++) {
            Index Nv = e[dx+i];
            Index ov = e[dx+dv+dx+i];
            fp deltav = (b[dx+i] - a[dx+i])/fp(Nv);
            fp xx = gauss::x_scaled01(idx[dv+i], ov);
            v_values[i](idx) = a[dx+i] + idx[i]*deltav + deltav*xx;
        }
    } while(iter_next(idx, max_v));
}

template<size_t dv>
void update_v_values(fp a, fp b, array<Index,2*dv> ev, 
                     multi_array<fp,2*dv>& v_values, Index vdim) {

    array<Index,2*dv> idx; fill(idx.begin(), idx.end(), 0);
    do {
        Index Nv = ev[vdim];
        Index ov = ev[dv+vdim];
        fp deltav = (b - a)/fp(Nv);
        fp xx = gauss::x_scaled01(idx[dv+vdim], ov);
        v_values(idx) = a + idx[vdim]*deltav + deltav*xx;
    } while(iter_next_memorder(idx, ev));
}

