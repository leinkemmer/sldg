#pragma once

#include <generic/common.hpp>
#include <programs/vp-block-cpu.hpp>
#include <programs/vp-block-gpu.hpp>
//#include <unistd.h>

#ifdef _OPENMP
#include <omp.h>
#endif


template<size_t dx, size_t dv>
void init_geometry(array<Index,dv> block_id, array<Index,dv> block_grid,
                   array_dxdv<fp,dx,dv>& a, array_dxdv<fp,dx,dv>& b) {

    // compute the physical boundaries for each block in v-direction
    array<fp,dv> a_v = a.v_part();
    array<fp,dv> b_v = b.v_part();
    global_ab_to_local_ab<dv>(a_v, b_v, block_id, block_grid);
    copy(a_v.begin(), a_v.end(), a.begin()+dx);
    copy(b_v.begin(), b_v.end(), b.begin()+dx);
}

template<size_t d>
struct write_info {
    domain<d>& data;
    array<Index,d> offset;

    write_info(array<Index,d> _offset, domain<d>& _data)
        : data(_data), offset(_offset) {}
};


template<size_t dx, size_t dv, fp (*f0)(fp*)>
struct vp_block_list {
    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;
    
    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;

    using domain_x = multi_array<fp,dx_size>;
    using efield = array< domain_x, dx>;

    bool gpu;

    Index total_blocks;
    array<Index,dv> num_blocks;
    vector< std::unique_ptr<vp_block<dx,dv,f0>> > blocks;

    index_no e;
    int num_ele_domains;

    //only used when a predictor corrector strategy is required
    vector<multi_array<fp,2*d>> auxiliary_memory;

    vp_block_list(array<Index,dv> _num_blocks, vec_d glob_a, vec_d glob_b, 
                  index_no _e, bool _gpu, array<Index,d> node_id,
                  int add_to_device_number, Index bdr_max_length) 
        : gpu(_gpu), num_blocks(_num_blocks), e(_e) {

        total_blocks = prod(num_blocks);
        blocks.resize(total_blocks);

        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            vec_d a = glob_a;
            vec_d b = glob_b;
            init_geometry(get_id(i), num_blocks, a, b);
            if(!gpu)
                blocks[i].reset(
                        new vp_block_cpu<dx,dv,f0>(a, b, e, bdr_max_length) );
            #ifdef __CUDACC__
            else
                blocks[i].reset(
                        new vp_block_gpu<dx,dv,f0>(a, b, e, i+add_to_device_number, 
                                                   bdr_max_length) );
            #endif

            //char hostname[20];
            //gethostname(hostname, 20);
            #pragma omp critical
            cout << "Block id: " << get_id(i) << " a=" << a << " b=" << b 
                 << " e=" << e << " node_id: " << node_id << endl;
                 // << " hostname: " << hostname << endl;
        }
        num_ele_domains=total_blocks;
    }

    vp_block_list(vec_d glob_a, vec_d glob_b, 
                  index_no _e, bool _gpu, int num_gpus,
                  Index bdr_max_length, bool sldg_limiter,
                  vector<int> ratios, vector<location> locations,
                  fp additional_scalar_speed, int _num_ele_domains,
                  bool predictor_corrector_strategy)
        : gpu(_gpu), e(_e), num_ele_domains(_num_ele_domains)
    {
        if(ratios.size()!=locations.size()){
            cout << "ERROR: ratios.size!=locations.size.\n";
            exit(1);
        }
        total_blocks = locations.size();
        blocks.resize(total_blocks);
        if(total_blocks!=num_ele_domains+1){
            cout << "ERROR: currently only one domain for the ions allowed.\n";
            exit(1);
        }

        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            if(i<num_ele_domains){
                //electrons, possibly use a sldg_limiter, different domain sizes,
                //advection in the velocity domain perforemd with -E
                vec_d a = glob_a;
                vec_d b = glob_b;
                fp nd = (num_ele_domains==1) ? 0.0 : 1.0;
                fp h_interior = (glob_b[0]-glob_a[0])/
                                (e[0]*(1.0+nd*2.0/fp(ratios[0])));
                for(int k=0;k<i;++k)
                    a[0] = a[0] + e[0]*h_interior/fp(ratios[k]);
                b[0] = a[0] + e[0]*h_interior/fp(ratios[i]);
                if(!gpu)
                    blocks[i].reset(
                            new vp_block_cpu<dx,dv,f0>(a, b, e, bdr_max_length,
                                                       sldg_limiter, ratios[i],
                                                       locations[i],1.0,-1.0) );
                #ifdef __CUDACC__
                else
                    blocks[i].reset(
                            new vp_block_gpu<dx,dv,f0>(a, b, e, i%num_gpus, 
                                                       bdr_max_length, sldg_limiter,
                                                       ratios[i],locations[i],
                                                       1.0,-1.0) );
                #endif

                #pragma omp critical
                {
                    cout << "electrons, i: " << i << ", "
                         << "a: " << a.get_array() << ", "
                         << "b: " << b.get_array() << endl;    
                }
            }else{
                //ions, advection speed in both direction multiplied with 
                //additional scalar speed (sqrt of electron ion mass ratio used)
                vec_d a = glob_a;
                vec_d b = glob_b;
                if(!gpu)
                    blocks[i].reset(
                            new vp_block_cpu<dx,dv,f0>(a, b, e, bdr_max_length,
                                                       false, 1, location::all,
                                                       additional_scalar_speed,
                                                       additional_scalar_speed) );
                #ifdef __CUDACC__
                else
                    blocks[i].reset(
                            new vp_block_gpu<dx,dv,f0>(a, b, e, i%num_gpus, 
                                                       bdr_max_length, false, 1,
                                                       location::all,
                                                       additional_scalar_speed,
                                                       additional_scalar_speed) );
                #endif

                #pragma omp critical
                {
                    cout << "ions, i: " << i << ", "
                         << "a: " << a.get_array() << ", "
                         << "b: " << b.get_array() << endl;    
                }
            }
        }

        if(predictor_corrector_strategy){
            auxiliary_memory.reserve(total_blocks);
            for(Index i=0;i<total_blocks;++i){
                multi_array<fp,2*d> aux(e.get_array(),gpu);
                auxiliary_memory.push_back(std::move(aux));
            }
        }
        //does not set the array num_blocks;
    }

 
    void init() {
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->init();
        }
    }

    void advection_v(Index dim,fp tau) {
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->advection_v(dim, tau);
        }
    }

    void advection_E(Index dim, fp tau, efield& E) {
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->advection_E(dim, tau, E);
        }
    }

    void swap() {
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->swap();
        }
    }

    domain_x compute_rho() {
        timer t;
        t.start();
        vector< domain_x > rho_red(total_blocks);
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            rho_red[i] = blocks[i]->compute_rho();
        }

        for(Index k=1;k<total_blocks;k++) {
            for(Index i=0;i<rho_red[0].num_elements();i++)
                rho_red[0].v[i] += rho_red[k].v[i];
        }
        t.stop();
        cout << "t_compute_rho: " << t.total() << endl;
        return rho_red[0];
    }

    domain_x compute_rho_and_invariants(fp& mass, fp& momentum, fp& l1,
                                        fp& l2, fp& K_energy, fp& entropy) {
        timer t;t.start();
        vector<domain_x> rho_red(total_blocks);

        mass=0.0; momentum=0.0; l1=0.0; l2=0.0; K_energy=0.0; entropy=0.0;
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for \
        reduction(+:mass,momentum,l1,l2,K_energy,entropy) \
        num_threads(num_threads) schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            fp _mass, _momentum, _l1, _l2, _K_energy, _entropy;
            rho_red[i] = blocks[i]->compute_rho_and_invariants(_mass, _momentum,
                    _l1, _l2, _K_energy, _entropy);
            mass += _mass;
            momentum += _momentum;
            l1 += _l1;
            l2 += _l2;
            K_energy += _K_energy;
            entropy += _entropy;
        }
        
        for(Index k=1;k<total_blocks;k++) {
            for(Index i=0;i<rho_red[0].num_elements();i++)
                rho_red[0].v[i] += rho_red[k].v[i];
        }
        t.stop();
        cout << "t_compute_rho_invariants: " << t.total() << endl;
        return rho_red[0];
    }

    void source(fp tau, fp time, source_f<dx,dv> sf) {
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->source(tau, time, sf);
        }
    }

    void weno_limiter_1d_electrons(int dim, troubled_cell_indicator tci,
                                   troubled_cell_modifier tcm) {
        #ifdef _OPENMP
        int num_threads=min((int)num_ele_domains,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(int i=0;i<num_ele_domains;i++) {
            blocks[i]->weno_limiter_1d(dim,tci,tcm);
        }

        for(int i=0;i<num_ele_domains;++i)
            blocks[i]->swap();
    }

    vector<domain_x> compute_rho_invariants_fluxes(fp& ele_particle_flux_left,
                                                   fp& ele_particle_flux_right,
                                                   fp& ion_particle_flux_left,
                                                   fp& ion_particle_flux_right,
                                                   bool compute_u_and_T) {
        vector<domain_x> rho(total_blocks);
        vector<fp> particle_flux_left(total_blocks);
        vector<fp> particle_flux_right(total_blocks);

        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for \
        num_threads(num_threads) schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            fp mass, momentum, l1, l2, K_energy, entropy;
            fp energy_flux_left, energy_flux_right;
            rho[i] = blocks[i]->compute_rho_invariants_fluxes(
                      mass, momentum, l1, l2, K_energy, entropy,
                      particle_flux_left[i], particle_flux_right[i],
                      energy_flux_left, energy_flux_right,
                      compute_u_and_T);
        }
        
        ele_particle_flux_left = particle_flux_left[0];
        ele_particle_flux_right = particle_flux_right[num_ele_domains-1];
        ion_particle_flux_left = particle_flux_left[num_ele_domains];
        ion_particle_flux_right = particle_flux_right[num_ele_domains];

        return rho;
    }

    void advection_E(Index dim, fp tau, vector<efield>& E) {

        if(E.size()!=total_blocks){
            cout << "ERROR: wrong number of partitions of the electric field.\n";
            cout << "Efield.size()=" << E.size() << ", total_blocks="
                 << total_blocks << endl;
            exit(1);
        }

        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->advection_E(dim, tau, E[i]);
        }
    }

    array<bool,2> check_reduce_domain_bounds(fp percent_reduce){

        vector<bool> reduce(total_blocks);
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            reduce[i] = blocks[i]->check_reduce_domain_bounds(percent_reduce);
        }
        
        bool reduce_ele=true;
        for(int i=0;i<num_ele_domains;++i)
            reduce_ele = (reduce_ele && reduce[i]);

        return {reduce_ele, reduce[num_ele_domains]};
    }

    void reduce_domain_bounds(array<bool,2>& reduce, fp percent_reduce,
                              vec_d& a_electrons, vec_d& b_electrons,
                              vec_d& a_ions, vec_d& b_ions ){

        bool ele_reduce = reduce[0];
        bool ion_reduce = reduce[1];

        int start, end;
        if(!ele_reduce && !ion_reduce){
            return;
        } else if(ele_reduce && ion_reduce){
            start = 0;
            end = total_blocks; 
            a_electrons[1] *= ( 1.0 - percent_reduce );
            b_electrons[1] *= ( 1.0 - percent_reduce );
            a_ions[1] *= ( 1.0 - percent_reduce );
            b_ions[1] *= ( 1.0 - percent_reduce );
        } else if(ele_reduce){
            start = 0;
            end = num_ele_domains;
            a_electrons[1] *= ( 1.0 - percent_reduce );
            b_electrons[1] *= ( 1.0 - percent_reduce );
        } else { // if(ion_reduce){
            start = num_ele_domains;
            end = total_blocks;
            a_ions[1] *= ( 1.0 - percent_reduce );
            b_ions[1] *= ( 1.0 - percent_reduce );
        }

        #ifdef _OPENMP
        int num_threads=min(end-start,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=start;i<end;++i){
            blocks[i]->reduce_domain_bounds(percent_reduce);
        }

        for(Index i=start;i<end;++i)
            blocks[i]->swap();
    }

    void copy_to_aux(){
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->copy_data(auxiliary_memory[i]);
        }
    }

    void swap_with_aux(){
        for(Index i=0;i<total_blocks;i++) {
            blocks[i]->swap_data(auxiliary_memory[i]);
        }
    }

    void BGK_collision(fp tau, fp collision_rate_electrons,
                               fp collision_rate_ions){
        #ifdef _OPENMP
        int num_threads=min((int)total_blocks,omp_get_max_threads());
        #endif
        #pragma omp parallel for num_threads(num_threads) \
        schedule(static,1) if(gpu)
        for(Index i=0;i<total_blocks;i++) {
            fp collision_rate = (i<num_ele_domains) ? collision_rate_electrons
                                                    : collision_rate_ions;
            blocks[i]->collision_BGK(tau,collision_rate,auxiliary_memory[i]);
        }
    }

    write_info<d> write_information(Index k) {
        array<Index,d> offset;
        fill(offset.begin(), offset.end(), 0);
        for(size_t i=0;i<dv;i++) {
            array<Index,dv> id = get_id(k);
            offset[dx+i] = id[i]*e[dx+i]*e[d+dx+i];
        }

        return write_info<d>(offset, blocks[k]->get_data());
    }

    array<Index,dv> get_id(Index k) {
        array<Index,dv> ids;
        for(size_t i=0;i<dv;i++) {
            ids[i] = k % num_blocks[i];
            k /= num_blocks[i];
        }
        return ids;
    }

};

