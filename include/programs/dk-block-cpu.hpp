#pragma once

#include <programs/dk-abstract.hpp>
#include <algorithms/dg.hpp>
#include <algorithms/sldg2d/sldg2d_lagrange_cpu.hpp>
#include <algorithms/qns.hpp>

template<size_t o2d, size_t oz, size_t ov>
struct dk_block_cpu : public dk_block<o2d,oz,ov> {

    static constexpr size_t dx = 3;
    static constexpr size_t dv = 1;
    static constexpr size_t d = dx+dv;

    //required to create dg_coeff_generic
    using domain_v = multi_array<fp,2*dv>;
    using domain_x = multi_array<fp,2*dx>;
    using domain   = multi_array<fp,2*(dx+dv)>;

    domain in, in_half, out;

    array<fp,dx+dv> a;
    array<fp,dx+dv> h;
    array<Index,2*(dx+dv)> e;

    unique_ptr<dg_coeff_generic<dx>> dg_c_v;
    unique_ptr<dg_coeff_generic<dv>> dg_c_z;
    domain_v v_values;
    fp tau_used;

    unique_ptr<sldg2d_lagrange_cpu<o2d>> tr2d;

    fp (*perturb_func)(fp,fp,fp);
    fp (*n0r)(fp);
    fp (*geq)(fp,fp);

    int time_order;
    bool perturbation;
    bc bcr;
    bc bct;

    Index Nr, Ntheta, Nv, Nz;

    generic_container<fp> dr_geq;
    generic_container<fp> dv_geq;
    generic_container<fp> geq_val;

    dk_block_cpu(array<fp,dx+dv> _a, array<fp,dx+dv> b,
                 Index _Nr, Index _Ntheta, Index _Nv, Index _Nz,
                 fp (*_perturb_func)(fp,fp,fp),
                 fp (*_n0r)(fp),
                 fp (*_geq)(fp,fp),
                 int _time_order,
                 bool _perturbation=true,
                 bc _bcr=bc::homogeneous_dirichlet,
                 bc _bct=bc::periodic)
        : a{_a}, Nr{_Nr}, Ntheta{_Ntheta}, Nv{_Nv}, Nz{_Nz},
          tau_used{-1.0}, perturb_func{_perturb_func}, n0r{_n0r}, 
          geq{_geq}, perturbation{_perturbation}, 
          time_order{_time_order}, bcr{_bcr}, bct{_bct}
    {

       h[0] = (b[0]-a[0])/fp(Nr);
       h[1] = (b[1]-a[1])/fp(Ntheta);
       h[2] = (b[2]-a[2])/fp(Nv);
       h[3] = (b[3]-a[3])/fp(Nz);

       e[0] = Nr;
       e[1] = Ntheta;
       e[2] = Nv;
       e[3] = Nz;
       e[4] = o2d;
       e[5] = o2d;
       e[6] = ov;
       e[7] = oz;

       //more cout 
       cout << "o2d: " << o2d << ", oz: " << oz << ", ov: " << ov << endl;
       cout << "e: " << e << endl;
       cout << "h: " << h << endl;
       cout << "order in time: " << time_order << endl;

       in.resize(e);
       out.resize(e);

       init_f0(); 
       if(time_order)
           in_half = in;

       int dof_x = Nr*o2d*Ntheta*o2d*Nz*oz;
       int dof_v = Nv*ov;
    
       dg_c_v = make_dg_coeff_gpu<dx>(ov,dof_x);
       dg_c_z = make_dg_coeff_gpu<dv>(oz,dof_v);

       v_values.resize({Nv,ov});
       init_v_values(a[2],h[2]);

       tr2d = make_unique<sldg2d_lagrange_cpu<o2d>>(a[0],a[1],b[0],b[1],Nr,Ntheta);

       dr_geq.resize(Nr*o2d*Nv*ov);
       dv_geq.resize(Nr*o2d*Nv*ov);
       geq_val.resize(Nr*o2d*Nv*ov);

       init_srctrm();

    }


    void init_v_values(fp av, fp hv){

        array<Index,2*dv> idx{0};
        array<Index,2*dv> ev = {Nv,ov};
        do{
            fp xx = gauss::x_scaled01(idx[1],ev[1]);
            v_values(idx) = av + hv*(idx[0] + xx);
        } while(iter_next_memorder(idx,ev));

    }


    void init_f0(){

        fp c = 1.0;
        if(perturbation)
            c = 0.0;

        #pragma omp parallel for schedule(static)
        for(Index l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){
            for(Index k=0;k<Nv;++k){
            for(size_t kk=0;kk<ov;++kk){
                for(Index j=0;j<Ntheta;++j){
                for(Index i=0;i<Nr;++i){
                    for(size_t jj=0;jj<o2d;++jj){
                    for(size_t ii=0;ii<o2d;++ii){

                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        fp r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                        fp t = a[1] + h[1]*(j+gauss::x_scaled01(jj,o2d));
                        fp v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));
                        fp z = a[3] + h[3]*(l+gauss::x_scaled01(ll,oz));

                        fp geq_rv = geq(r,v);

                        in.v[lidx] = geq_rv*c + geq_rv*perturb_func(r,t,z);

                    }
                    }
                }
                }
            }
            }
        }
        }

    }


    void init_srctrm(){

        fp c = 0.0;
        if(perturbation)
            c = 1.0;

        #pragma omp parallel for schedule(static)
        for(Index k=0;k<Nv;++k){
        for(size_t kk=0;kk<ov;++kk){
            for(Index i=0;i<Nr;++i){
                for(size_t ii=0;ii<o2d;++ii){

                    fp deltar = 1e-7;
                    fp deltav = 1e-7;

                    int idx = ii + o2d*(i+Nr*(kk + ov*k));

                    fp r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                    fp v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));

                    fp drgeq = (geq(r+deltar,v)/(r+deltar)-
                                geq(r-deltar,v)/(r-deltar))/(2.0*deltar);
                    fp dvgeq = (geq(r,v+deltav)-geq(r,v-deltav))/(2.0*deltav);

                    dr_geq.h_data[idx] = drgeq;//drgeq/r-geq(r,v)/(r*r);
                    dv_geq.h_data[idx] = dvgeq;

                    geq_val.h_data[idx] = geq(r,v)*c;

                }
            }
        }
        }

    }


    void swap(){
        in.swap(out);
    }


    void swap_in(){
        in.swap(in_half);
    }


    domain& get_data(){
        return in;
    }

    void advection_z(fp tau){
        int dim=3;

        if(tau_used < 0.0 || tau_used != tau){
            dg_c_z->compute(tau,h[dim],v_values);
            tau_used = tau;
        }

        advection<dv>(dim,in,out,dg_c_z.get());
    }


    void advection_v(fp tau, unique_ptr<quasi_neutrality>& qns){
        int dim=2;

        gt::start("adv_v_computeAB");
        dg_c_v->compute(tau,h[dim],qns->dz_phi);
        gt::stop("adv_v_computeAB");

        gt::start("adv_v_translate");
        advection<dx>(dim,in,out,dg_c_v.get());
        gt::stop("adv_v_translate");
    }


    void advection_rt(fp tau, unique_ptr<quasi_neutrality>& qns, int stage){

        for(int l=0;l<Nz*oz;++l){
            int offset = l*Nv*ov*Nr*o2d*Ntheta*o2d;
            tr2d->step_nl(in.data()+offset,out.data()+offset,
                          *qns,tau,Nv*ov,l,bcr,bct,stage);
        }
    }


    void compute_rho_and_invariants(std::unique_ptr<quasi_neutrality>& qns,
                                    double& mass, double& l2, double& tot_energy){

        compute_rho(qns->rho, qns->phi, mass, l2, tot_energy);
    }

    void source_term(fp tau, unique_ptr<quasi_neutrality>& qns){

        #pragma omp parallel for schedule(static)
        for(Index l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){
            for(Index k=0;k<Nv;++k){
            for(size_t kk=0;kk<ov;++kk){
                for(Index j=0;j<Ntheta;++j){
                for(Index i=0;i<Nr;++i){
                    for(size_t jj=0;jj<o2d;++jj){
                    for(size_t ii=0;ii<o2d;++ii){
                        
                        int lidx = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));

                        int lidx_x = ii+o2d*(jj+o2d*(i+Nr*(j+
                                        Ntheta*(ll+oz*l))));

                        int lidx_geq = ii+o2d*(i+Nr*(kk+ov*k));

                        out.v[lidx] = in.v[lidx] + 
                                      tau*(qns->dtheta_phi.h_data[lidx_x]*
                                           dr_geq.h_data[lidx_geq] - 
                                           qns->dz_phi.v[lidx_x]*
                                           dv_geq.h_data[lidx_geq]);

                    }
                    }
                }
                }
            }
            }
        }
        }
    }


    template<size_t d_adv>
    void advection(size_t dim, domain& in, domain& out,
                   dg_coeff_generic<d_adv>* dg_c) {

        int tile_x  = (dim==0) ? 1 : 16;
        array<Index,3> b = create_grid<d>(dim, e, tile_x);
        vec3 gdim(b[0],b[1],b[2]);

        if(dim >= d) {
            cout << "ERROR: dim>=d in advection() is not valid" << endl;
            exit(1);
        }

        Index o = e[d+dim];

        if(dim == 2)
            run_kernel<2>(o, gdim, tile_x, in.data(), out.data(), 
                          dg_c->p_A(), dg_c->p_B(), dg_c->p_shift());
        else if(dim == 3)
            run_kernel<3>(o, gdim, tile_x, in.data(), out.data(), 
                          dg_c->p_A(), dg_c->p_B(), dg_c->p_shift());
        else {
            cout << "ERROR: dim!=2 && dim!=3" << endl;
            exit(1);
        }
    }


    template<int dim>
    void run_kernel(Index o, vec3 gdim, int tile_x, fp* in, 
                    fp* out, fp* pA, fp* pB, int* pshift) {

        if(o==2)
            run_kernel_o<dim,2>(gdim, tile_x, in, out, pA, pB, pshift);
        else if(o==3)
            run_kernel_o<dim,3>(gdim, tile_x, in, out, pA, pB, pshift);
        else if(o==4)
            run_kernel_o<dim,4>(gdim, tile_x, in, out, pA, pB, pshift);
        else if(o==5)
            run_kernel_o<dim,5>(gdim, tile_x, in, out, pA, pB, pshift);
        else if(o==6)
            run_kernel_o<dim,6>(gdim, tile_x, in, out, pA, pB, pshift);
        else {
            cout << "ERROR: only o=2,3,4,5,6 currently implemented" << endl;
            exit(1);
        }
    }


    template<int dim, Index o>
    void run_kernel_o(vec3 gdim, int tile_x, fp* in, fp* out,
                      fp* pA, fp* pB, int* pshift) {

        vec3 gd(gdim[0],gdim[1],gdim[2]);

        //currently using just one GPU, no boundary exchange has to be done
        int bdr_len = 0;

        #pragma omp parallel for schedule(static)
        for(int i_omp=0;i_omp<gdim[2];i_omp++) {
            for(int iy=0;iy<gdim[1];iy++) {
                for(int ix=0;ix<gdim[0];ix++) {
                    vec3 bid(ix, iy, i_omp);

                    for(int i=0;i<e[dim];i++)
                        for(int it=0;it<tile_x;it++){

                            translate<dx,dv,dim,o>(in, out,
                                    pA, pB, pshift, e[0]*e[d+0],
                                    vec3(it,i,0), bid, vec3(tile_x,e[dim],1),
                                    gd, nullptr, nullptr, &e[0], bdr_len,
                                    nullptr, nullptr, 0, dim);
                        }
                }
            }
        }
    }


    void compute_rho(generic_container<fp>& rho, generic_container<fp>& phi,
                     double& mass, double& l2, double& tot_energy) {
    
        mass = 0.0;
        l2 = 0.0;
        tot_energy = 0.0;

        if(!perturbation){
            for(int k=0;k<Nz;++k){
                for(int kk=0;kk<oz;++kk){
                    for(int j=0;j<Ntheta;++j){
                        for(int i=0;i<Nr;++i){
                            for(int jj=0;jj<o2d;++jj){
                                for(int ii=0;ii<o2d;++ii){
    
                                    int lidx_rho = ii+o2d*(jj+o2d*(i+Nr*
                                                   (j+Ntheta*(kk+oz*k))));
    
                                    fp r = a[0] + h[0]*
                                         (i+gauss::x_scaled01(ii,o2d));
    
                                    rho.h_data[lidx_rho] = -n0r(r);
                                }
                            }
                        }
                    }
                }
            }
        } else
            std::fill(rho.h_data.begin(),rho.h_data.end(),0.0);

        //fp c = 1.0;
        //if(perturbation)
        //    c= 0.0;

        #pragma omp parallel for schedule(static) reduction(+:mass,l2,tot_energy)
        for(int l=0;l<Nz;++l){
        for(int ll=0;ll<oz;++ll){
            for(int k=0;k<Nv;++k){
            for(int kk=0;kk<ov;++kk){
                for(int j=0;j<Ntheta;++j){
                    for(int i=0;i<Nr;++i){
                        for(int jj=0;jj<o2d;++jj){
                        for(int ii=0;ii<o2d;++ii){
    
                            int lidx_full = ii+o2d*(jj+o2d*(i+Nr*
                                            (j+Ntheta*(kk+ov*(k+Nv*(ll+oz*l))))));
    
                            int lidx_rho = ii+o2d*(jj+o2d*(i+Nr*
                                           (j+Ntheta*(ll+oz*l))));

                            int lidx_geq = ii + o2d*(i+Nr*(kk + ov*k));
    
                            fp r = a[0] + h[0]*(i+gauss::x_scaled01(ii,o2d));
                            fp v = a[2] + h[2]*(k+gauss::x_scaled01(kk,ov));
                            fp g_val = in.v[lidx_full];

                            rho.h_data[lidx_rho] += g_val*h[2]*0.5*gauss::w(kk,ov);
                            fp weight = h[0]*0.5*gauss::w(ii,o2d)*
                                        h[1]*0.5*gauss::w(jj,o2d)*
                                        h[2]*0.5*gauss::w(kk,ov)*
                                        h[3]*0.5*gauss::w(ll,oz);

                            g_val += geq_val.h_data[lidx_geq];

                            mass += g_val*weight;
                            l2 += g_val*g_val/r*weight; 
                            tot_energy += g_val*weight*
                                          (v*v*0.5 + phi.h_data[lidx_rho]);

                        }
                        }
                    }
                }
            }
            }
        }
        }

        l2 = sqrt(l2);
    
    }
    
};


