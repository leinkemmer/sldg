#pragma once

#ifdef __CUDACC__

#include <generic/common.hpp>
#include <container/domain.hpp>
#include <programs/vp-block.hpp>
#include <algorithms/dg-gpu.cu>
#include <algorithms/limiter-gpu.cu>
#include <algorithms/rho-gpu.cu>
#include <algorithms/adjust_domain-gpu.cu>


template<size_t dx, size_t dv, fp (*f0)(fp*)>
struct vp_block_gpu : vp_block<dx,dv,f0> {
    static constexpr size_t dx_size = 2*dx;
    static constexpr size_t dv_size = 2*dv;
    static constexpr size_t d       = dx+dv;
    static constexpr size_t d_size  = dx_size+dv_size;
    
    using index_no = index_dxdv_no<dx, dv>;
    using vec_d    = array_dxdv<fp,dx,dv>;
    using domain_x = multi_array<fp,2*dx>;
    using efield   = array< domain_x, dx>;
  
    index_no e;

    gpu_array<fp> a;
    gpu_array<fp> b;
    gpu_array<Index> d_e;
    gpu_array<fp> gauss_x;

    array<multi_array<fp,2*dv>,dv> v_values;

    Index bdr_len; // updated in get_boundary
    
    fp* d_bdr_data;
    fp* d_bdr_to_send;

    unique_ptr< domain<d> > d_in, d_out;

    domain<d> tmp; // storage on the cpu

    unique_ptr<translate_gpu<dx,dv,dv>> trgpu_x;
    unique_ptr<translate_gpu<dx,dv,dx>> trgpu_v;

    unique_ptr<pack_boundary_from_gpu<dx,dv>> pbfgpu;

    unique_ptr<reduction_on_gpu<dx,dv>> crgpu;
    
    unique_ptr<sign_gpu<dx>> sgpu_E;
    unique_ptr<sign_gpu<dv>> sgpu_v;

    vector<double> tau_used;
    vector<bool> domain_changed;

    vector< unique_ptr<dg_coeff_generic<dv>> > dg_c_x;
    vector< unique_ptr<dg_coeff_generic<dx>> > dg_c_v;
    domain_x d_E{true};

    unique_ptr<max_abs_v_stride<d>> max_abs_v;

    unique_ptr<weno_1d_limiter<dx,dv>> weno_limiter;
    int bdry_cell_ratio;
    location loc;

    domain_x d_rho{true};
    domain_x d_u{true};
    domain_x d_T{true};

    vp_block_gpu(vec_d _a, vec_d _b, index_no _e, Index _device_number,
                 Index bdr_max_length, bool sldg_limiter=false, int _ratio=1,
                 location _loc=location::all, fp additional_scalar_speed_x=1.0,
                 fp additional_scalar_speed_v=1.0)
        : device_number(_device_number), e(_e), 
          a(d), b(d), d_e(2*d), gauss_x(sum(e.o_part())),
          tmp(e.get_array(), _a.get_array(), _b.get_array(), false),
          tau_used(dx), dg_c_x(dx), dg_c_v(dv), bdry_cell_ratio(_ratio), loc(_loc) {

        set_device();

        d_in.reset(  new domain<d>(e.get_array(), _a.get_array(), _b.get_array(),
                    true) );
        d_out.reset( new domain<d>(e.get_array(), _a.get_array(), _b.get_array(),
                    true) );

        d_rho.resize(e.x_part());
        d_u.resize(e.x_part());
        d_T.resize(e.x_part());

        init_v_values(_a,_b,e.get_array(),v_values);

        //if ratio=1, memory can be directly used, otherwise
        //it has to be unpacked for which additional memory is required
        int required_memory=1;
        if(bdry_cell_ratio>1)
            required_memory = bdry_cell_ratio+1;
        d_bdr_data  = (fp*)gpu_malloc(sizeof(fp)*bdr_max_length*required_memory);

        bdr_len = 0;

        trgpu_x = make_unique<translate_gpu<dx,dv,dv>>(e, bdr_max_length, 
                                                       sldg_limiter);
        trgpu_v = make_unique<translate_gpu<dx,dv,dx>>(e, bdr_max_length, 
                                                       sldg_limiter);

        pbfgpu = make_unique<pack_boundary_from_gpu<dx,dv>>(e, bdr_max_length);

        sgpu_E = make_unique<sign_gpu<dx>>(e.x_part());
        sgpu_v = make_unique<sign_gpu<dv>>(e.v_part());

        crgpu = make_unique<reduction_on_gpu<dx,dv>>(e,_a.get_array(),_b.get_array());

        fill(begin(tau_used), end(tau_used), -1.0);
        domain_changed.resize(dv);
        fill(begin(domain_changed), end(domain_changed), false);

        for(size_t dim=0;dim<dx;dim++) {
            Index N = prod(e.v_part());
            dg_c_x[dim] = make_dg_coeff_gpu<dv>(e[d+dim], N, 
                                                additional_scalar_speed_x);
        }
        
        for(size_t dim=0;dim<dv;dim++) {
            Index N = prod(e.x_part());
            dg_c_v[dim] = make_dg_coeff_on_gpu<dx>(e[d+dx+dim], N, 
                                                   additional_scalar_speed_v);
        }

        d_bdr_to_send = (fp*)gpu_malloc(bdr_max_length*sizeof(fp));

        a.set(_a.begin(), _a.end());
        a.copy_to_gpu();

        b.set(_b.begin(), _b.end());
        b.copy_to_gpu();

        d_e.set(e.begin(), e.end());
        d_e.copy_to_gpu();

        int offset=0;
        for(size_t i=0;i<d;++i){
            vector<fp> gx = gauss::all_x_scaled01(e[d+i]);
            copy(begin(gx), end(gx), &gauss_x[offset]);
            offset += e[d+i];
        }
        gauss_x.copy_to_gpu();

        weno_limiter = make_unique<weno_1d_limiter<dx,dv>>(d_e,loc);

        max_abs_v = make_unique<max_abs_v_stride<d>>(d_in->data, prod(e.x_part()));

        assert(bdry_cell_ratio > 0);
    }

    ~vp_block_gpu() {
        set_device();
        cudaFree(d_bdr_data);
        cudaFree(d_bdr_to_send);
    }


    void init() {
        set_device();

        size_t N = prod(e.get_array());

        init_gpu<d,f0><<<N/128+1,128>>>(d_in->data.data(), a.d_data, b.d_data,
                                        d_e.d_data, gauss_x.d_data, N);
 
        //fp *d_max;
        //cudaMalloc(&d_max,sizeof(fp));
        //void *d_temp_storage = NULL;
        //size_t temp_storage_bytes = 0;
        //cub::DeviceReduce::Max(d_temp_storage, temp_storage_bytes, 
        //                       d_in->data.data(), d_max, N);
        //cout << "temp_storage_bytes: " << temp_storage_bytes << endl;
        //cudaMalloc(&d_temp_storage, temp_storage_bytes);
        //// Run max-reduction
        //cub::DeviceReduce::Max(d_temp_storage, temp_storage_bytes, 
        //                       d_in->data.data(), d_max, N);
        //cudaDeviceSynchronize();
        //fp h_max;
        //cudaMemcpy(&h_max, d_max, sizeof(fp), cudaMemcpyDeviceToHost);
        //cudaDeviceSynchronize();
        //cudaFree(d_temp_storage);
        //cudaFree(d_max);

        //copy to cpu
        //cudaMemcpy(tmp.data.data(), d_in->data.data(), 
        //           sizeof(fp)*N, cudaMemcpyDeviceToHost);
        //fp h_max = max_element_abs(tmp.data);
        //
        //char hname[30];
        //gethostname(hname,30);
        //cout << "hostname: " << hname << " max_in: " << h_max << endl;
 
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());

    }

    void advection_v(int dim, fp tau) {
        gt::start(str(boost::format("advection_dim%1%")%dim));

        set_device();

        fp dd=(b[dim]-a[dim])/fp(e[dim]);

        // The advection speed in the x-direction is only recomputed
        // if the timestep changes.
        gt::start("dg_c_x");
        if(domain_changed[dim] || tau_used[dim] != tau || tau_used[dim] <= 0.0) {

            if(domain_changed[dim]==true){
                update_v_values<dv>(a[dx+dim],b[dx+dim],e.v_part(),
                                    v_values[dim],dim);
                domain_changed[dim] = false;
            }

            dg_c_x[dim]->compute(tau, dd, v_values[dim]);
            dg_c_x[dim]->copy_to_gpu();
            tau_used[dim] = tau;
            cudaDeviceSynchronize();
        }
        gt::stop("dg_c_x");

        Index bdr_tot_len = bdr_len*e[d+dim]*prod(e.slice(dim));

        trgpu_x->translate(dim, tau, dd, *dg_c_x[dim], d_in->data, d_out->data,
                        bdr_len, bdr_tot_len, d_bdr_data, true, sgpu_v->d_sign,
                        sgpu_v->max_idx_left);

        gt::stop(str(boost::format("advection_dim%1%")%dim));
    }

    void advection_E(int dim, fp tau, efield& E) {
        gt::start(str(boost::format("advection_dim%1%")%dim));

        set_device();

        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> max = e.slice(dim);
        Index bdr_tot_len = bdr_len*o*prod(max);

        gt::start("dg_c_v");
        d_E = E[dim-dx];
        dg_c_v[dim-dx]->compute(tau, dd, d_E);
        cudaDeviceSynchronize(); // this is only to make time measurements sensible
        gt::stop("dg_c_v");

        trgpu_v->translate(dim, tau, dd, *dg_c_v[dim-dx], d_in->data, d_out->data,
                           bdr_len, bdr_tot_len, d_bdr_data, true, sgpu_E->d_sign, 
                           sgpu_E->max_idx_left);

        gt::stop(str(boost::format("advection_dim%1%")%dim));
    }

    void swap() {
        set_device();
        d_in->data.swap(d_out->data);
    }

    void source(fp dt, fp t, source_f<dx,dv> f_source){
        set_device();

        size_t N = prod(e.get_array());

        if(f_source.type==source_type::constant_source)
            source_gpu<dx,dv,0><<<(N+127)/128,128>>>(d_in->data.data(),
                                          d_out->data.data(),
                                          a.d_data, b.d_data, d_e.d_data,
                                          gauss_x.d_data, dt, t, f_source, N);
        if(f_source.type==source_type::time_dependent_source)
            source_gpu<dx,dv,1><<<(N+127)/128,128>>>(d_in->data.data(),
                                          d_out->data.data(),
                                          a.d_data, b.d_data, d_e.d_data,
                                          gauss_x.d_data, dt, t, f_source, N);
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }

    bdr_len_info pack_boundary(Index dim, fp tau, multi_array<fp,2*dx>& F,
                               Index bdr_max_length) {
        
        set_device();

        assert(dim >= Index(dx));

        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp alpha_max = tau*max_element_abs(F)/dd;
        bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);
        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: " << bdr_tot_len << " = bdr_tot_len > bdr_max_length = "
                 << bdr_max_length << endl;
            exit(1);
        }

        sgpu_E->compute(F);
        sgpu_E->copy_to_gpu();
        int bdr_max_left = sgpu_E->max_idx_left;

        //collection only, no communication
        pbfgpu->pack(dim, sgpu_E->d_sign, d_in->data, bdr_len, 
                     d_bdr_to_send, bdr_max_left);

        Index bdr_tot_left = o*bdr_len*bdr_max_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    bdr_len_info pack_boundary(Index dim, fp tau, Index bdr_max_length) {

        set_device();

        assert(dim < Index(dx));

        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp max_element = max(fabs(*v_values[dim].begin()),
                             fabs(*(v_values[dim].end()-1)));

        fp alpha_max = tau*max_element/dd;
        bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);

        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: " << bdr_tot_len << " = bdr_tot_len > bdr_max_length = "
                 << bdr_max_length << endl;
            exit(1);
        }

        sgpu_v->compute(v_values[dim]);
        sgpu_v->copy_to_gpu();
        int bdr_max_left = sgpu_v->max_idx_left;

        //collection only, no communication
        pbfgpu->pack(dim, sgpu_v->d_sign, d_in->data, bdr_len,
                     d_bdr_to_send,bdr_max_left);

        Index bdr_tot_left = o*bdr_len*bdr_max_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    //this is called on the domain with the fine grid
    //ratio is dx_coarse/dx_fine, which has to be a positive integer
    bdr_len_info pack_boundary_fine2coarse(fp tau, Index bdr_max_length) {
        set_device();

        constexpr Index dim = 0;

        assert(dim < Index(dx));

        Index o=e[d+dim];
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        array<Index,2*d-2> e_slice = e.slice(dim);

        fp max_element = max(fabs(*v_values[dim].begin()),
                             fabs(*(v_values[dim].end()-1)));

        fp alpha_max = tau*max_element/(dd*bdry_cell_ratio);
        bdr_len = int(alpha_max)+1;
        Index bdr_tot_len = bdr_len*o*prod(e_slice);

        if(bdr_tot_len > bdr_max_length) {
            cout << "ERROR: " << bdr_tot_len << " = bdr_tot_len > bdr_max_length = "
                 << bdr_max_length << endl;
            exit(1);
        }

        sgpu_v->compute(v_values[dim]);
        sgpu_v->copy_to_gpu();
        int bdr_max_left = sgpu_v->max_idx_left;

        //collection only, no communication
        pbfgpu->pack_fine2coarse(dim, sgpu_v->d_sign, d_in->data, bdr_len,
                                 d_bdr_to_send, bdr_max_left, bdry_cell_ratio, loc);

        Index bdr_tot_left = o*bdr_len*bdr_max_left*
                             bdr_adv_direction<dx,dv>(e.get_array(), dim);

        return bdr_len_info(bdr_len,bdr_tot_len,bdr_tot_left);
    }

    void unpack_boundary_coarse2fine(Index bdr_max_length){
        int bdr_len_coarse = bdr_len/bdry_cell_ratio;
        pbfgpu->unpack_coarse2fine(0,d_bdr_data,bdr_len_coarse,bdr_len,
                                   bdry_cell_ratio, sgpu_v->d_sign,
                                   bdr_max_length, sgpu_v->max_idx_left);
    }

    fp* get_boundary_storage(Index _bdr_len) {
        bdr_len = _bdr_len;
        return d_bdr_data;
    }

    fp* get_boundary_to_send(){
        return d_bdr_to_send;
    }

    domain_x compute_rho() {
        set_device();

        fp mass, momentum, l1, l2, K_energy, entropy;
        return compute_rho_and_invariants(mass, momentum, l1, l2, K_energy,
                entropy);
    }

    domain_x compute_rho_and_invariants(fp& mass, fp& momentum, fp& l1,
                                        fp& l2, fp& K_energy, fp& entropy) {
        set_device();

        Index N = prod(d_rho.shape());

        fp dummy;
        crgpu->compute_rho_and_invariants(d_in->data, d_rho, mass, l1, l2, 
                                          entropy, K_energy, momentum,
                                          dummy, dummy, dummy, dummy);

        domain_x rho(e.x_part());
        gpuErrchk( cudaMemcpy(rho.data(), d_rho.data(), sizeof(fp)*N,
                              cudaMemcpyDeviceToHost));
        gpuErrchk( cudaPeekAtLastError() );

        return rho;
    }

    domain_x compute_rho_invariants_fluxes( fp& mass, fp& momentum, fp& l1,
                                            fp& l2, fp& K_energy, fp& entropy,
                                            fp& particle_flux_left,
                                            fp& particle_flux_right,
                                            fp& energy_flux_left, 
                                            fp& energy_flux_right,
                                            bool compute_u_and_T){
        set_device();

        Index N = prod(d_rho.shape());

        crgpu->compute_rho_and_invariants(d_in->data, d_rho, mass, l1, l2, 
                                          entropy, K_energy, momentum,
                                          particle_flux_left, particle_flux_right,
                                          energy_flux_left, energy_flux_right);

        domain_x rho(e.x_part());
        gpuErrchk( cudaMemcpy(rho.data(), d_rho.data(), sizeof(fp)*N,
                              cudaMemcpyDeviceToHost));

        if(compute_u_and_T && dx==1){
            crgpu->compute_meanvelocity(d_in->data, d_rho, d_u);
            crgpu->compute_temperature(d_in->data, d_rho, d_u, d_T);

            //multi_array<fp,2> u;
            //multi_array<fp,2> T;
            //u=d_u;
            //T=d_T;
            //std::ofstream output("uandT"+std::to_string(counter)+"-"+
            //                             std::to_string(_nu)+".data");
            //for(int i=0;i<e[0];++i){
            //    for(int j=0;j<e[d];++j){
            //        fp x = a[0] + (b[0]-a[0])/fp(e[0])*
            //               (i+gauss::x_scaled01(j,e[d]));
            //        output << x << " " 
            //               << rho.v[j+i*e[d]] << " "
            //               << u.v[j+i*e[d]] << " " 
            //               << T.v[j+i*e[d]] << endl;
            //    }
            //}
            //counter += 1;
        }

        gpuErrchk( cudaPeekAtLastError() );

        return rho;
    }

    void collision_BGK(fp dt, fp nu){
        set_device();
        if(dx==1){

            int v23 = 1;
            for(size_t i=1;i<dv;++i)
                v23 *= e[dx+i]*e[d+dx+i];
            dim3 blocks(e[d],e[dx]*e[d+dx],v23);

            BGK_gpu<dv><<<blocks,e[0]>>>(d_out->data.data(), d_in->data.data(),
                                         d_in->data.data(), dt, nu, d_rho.data(),
                                         d_u.data(), d_T.data(),
                                         a.d_data, b.d_data, d_e.d_data);
        } else {
            cout << "BGK operator implemented only for dx=1" << endl;
        }
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }

    void collision_BGK(fp dt, fp nu, multi_array<fp,2*(dx+dv)>& d_inhalf){
        set_device();
        if(dx==1){
            if(d_inhalf.gpu!=true){
                cout << "ERROR: in at half the time-step not on the gpu" << endl;
                exit(1);
            }

            int v23 = 1;
            for(size_t i=1;i<dv;++i)
                v23 *= e[dx+i]*e[d+dx+i];
            dim3 blocks(e[d],e[dx]*e[d+dx],v23);

            BGK_gpu<dv><<<blocks,e[0]>>>(d_out->data.data(), d_in->data.data(),
                                         d_inhalf.data(), dt, nu, d_rho.data(),
                                         d_u.data(), d_T.data(),
                                         a.d_data, b.d_data, d_e.d_data);
        } else {
            cout << "BGK operator implemented only for dx=1" << endl;
        }
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }

    domain<d>& get_data() {
        tmp = *d_in;
        return tmp;
    }

    void swap_data(multi_array<fp,2*d>& f){
        set_device();

        if(f.gpu!=true){
            cout << "ERROR: input not on the gpu!\n";
            exit(1);
        } else {
            f.swap(d_in->data);
        }
    }

    void copy_data(multi_array<fp,2*d>& f){
        set_device();

        f = d_in->data;

        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }

    bool check_reduce_domain_bounds(fp percent_reduce){

        set_device();
    
        if(dv!=1){
            cout << "ERROR: currently not implemented for dv>1\n";
            exit(1);
        }

        fp tol = 1e-14;

        //percent reduce*0.5 since domain is made smaller on both ends
        //addition of e[1]*0.05 adds buffer of 5% to ensure f (and its
        //derivatives) are zero at the end of the domain to avoid
        //undesired effects of the advection in the velocity domain
        int cells_lost = e[1]*percent_reduce*0.5 + e[1]*0.05;
        Index i_left = cells_lost;
        Index i_right = e[1]-cells_lost;

        Index ov = e[d+dx];
        Index dof_x = e[0]*e[d];

        fp abs_l = max_abs_v->get(i_left*ov,dof_x,d_in->data);
        fp abs_r = max_abs_v->get(i_right*ov-1,dof_x,d_in->data);

        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());

        return (abs_l<tol && abs_r<tol);
    }

    void reduce_domain_bounds(fp percent_reduce){
        set_device();
    
        if(dv!=1){
            cout << "ERROR: currently not implemented for dv>1\n";
            cout << endl;
        }


        //percent reduce*0.5 since domain is made smaller on both ends
        //addition of e[1]*0.05 adds buffer of 5% to ensure f (and its
        //derivatives) are zero at the end of the domain to avoid
        //undesired effects of the advection in the velocity domain
        int cells_lost = e[1]*percent_reduce*0.5 + e[1]*0.05;
        Index i_left = cells_lost;
        Index i_right = e[1]-cells_lost;

        fp a_old = a[1];
        fp b_old = b[1];
        fp a_new = a_old*( 1.0 - percent_reduce );
        fp b_new = b_old*( 1.0 - percent_reduce );

        cout << "reduce_domain_bounds " 
             << "\ni_left: " << i_left << ", i_right: " << i_right
             << "\npercent: " << percent_reduce
             << "\na_new: " << a_new << ", b_new: " << b_new
             << "\na_old: " << a_old << ", b_old: " << b_old << "\n\n";

        project_on_finer_grid_gpu<dx,dv>(a_old,b_old,a_new,b_new,
                                         e.get_array(), 1.0-percent_reduce,
                                         d_in->data, d_out->data);

        a[1] = a_new;
        b[1] = b_new;
        a.copy_to_gpu();
        b.copy_to_gpu();

        crgpu->set_domain(a.h_data,b.h_data);

        domain_changed[0] = true;

        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }
    

    void weno_limiter_1d(int dim, troubled_cell_indicator tci,
                         troubled_cell_modifier tcm){
        set_device();
        //weno_limiter_1d_gpu<dx,dv>(d_in->data, d_out->data, dim, d_e, tci, tcm);
        weno_limiter->apply_weno_limiter_1d_gpu(d_in->data, d_out->data, 
                                                dim, tci, tcm);
        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());
    }


    weno_bdry_transfer_info pack_for_weno_limiter(int grid_size_ratio){
        set_device();

        weno_bdry_transfer_info wbti;
        size_t size = weno_limiter->pack(grid_size_ratio, d_in->data,
                           wbti.bdry_left_snd, wbti.bdry_right_snd,
                           wbti.bdry_left_rcv, wbti.bdry_right_rcv);
        wbti.size = size;

        cudaDeviceSynchronize();
        gpuErrchk(cudaPeekAtLastError());

        return wbti;
    }

private:
    Index device_number;

    void set_device() {
        cudaSetDevice(device_number);
    }
};

#endif
