#include <generic/common.hpp>
#include <memory>

#include <boost/program_options.hpp>

#include <fftw3-mpi.h>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif


/// This class implements a Vlasov--Poisson solver based on the discontinuous
/// Galerkin implementation and the classic time splitting approach introduced
/// by Cheng & Knorr (1976). For the solution of Poisson's equation a Fourier
/// approach is used that is based on the FFTW library.
///
/// Due to the data distribution that the MPI version of the FFTW library uses
/// (which only decomposes the d-dimensinal FFT into n-1 dimensions) our
/// implementation is currently only able to handle two dimensional problems. In
/// the future we plan to use a different solver that avoids the all-to-all
/// communication of the FFT and can be applied directly within the domain
/// decomposition that is most efficient for the Vlasov solver (i.e. a uniform
/// distribution across all space dimensions). In addition, such an approach
/// could be directly applied to the not equidistant grid points of the
/// discontinuous Galerkin approximation.
template<size_t d>
struct vlasovpoisson {
    fp V, L, T, tau;
    int Nx, Nv, ox, ov;
    mpi_process<d> &mpicomm;
    timer total, comm, advect, poisson, comm_poisson, prep;
    array<fp,d> a; array<fp,d> b; array<Index,2*d> e;
    fp (*f0)(array<fp,d>);
    array< vector<fp> ,d/2> lalpha_v;
    multi_array<fp,d> E;
    multi_array<fp,d> rho;
    vector<fp> rho_equidistant; vector<fp> E_equidistant;
    vector<MPI_Group> groups; vector<MPI_Comm> comms;
    MPI_Group group_fft; MPI_Comm comm_fft;
    fftw_complex* f_in, *f_out;
    fftw_plan plan_fft, plan_ifft;
    bool is_veloc_master;
    int snapshots;

    /// The constructor performs a number of initialization tasks. It creates
    /// the MPI groups and communicators that are required for the collective
    /// communication routines that are used to solve Poisson's equation (this
    /// is a lower dimensional problem). It also creates the plans required for
    /// the FFTW library calls.
    vlasovpoisson(fp _L, fp _V, int _Nx, int _Nv, int _ox, int _ov, fp _T,
            fp _tau, fp (*_f0)(array<fp,d>), mpi_process<d>& _mpicomm, 
            int _snapshots) 
        : mpicomm(_mpicomm) {

        L=_L; V=_V; Nx=_Nx; Nv=_Nv; f0=_f0; ox=_ox; ov=_ov; T=_T; tau=_tau;
        snapshots=_snapshots;
        array<Index,d> num_nodes;
        distribute((Index)mpicomm.total_nodes,num_nodes);
        if(mpicomm.rank==0)
            cout << "Number of MPI processes per dimension: " << num_nodes 
                 << endl;
        mpicomm.init(num_nodes);

        for(size_t i=0;i<d/2;i++) { a[i]=0.0; b[i]=L; e[i]=Nx; e[d+i]=ox; }
        for(size_t i=d/2;i<d;i++) { a[i]=-V;  b[i]=V; e[i]=Nv; e[d+i]=ov; }

        global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

        // print node summary in order
        std::stringstream s;
        s << "Node: " << mpicomm.node_id << " of " 
          << mpicomm.num_nodes 
          << ". Domain from " << a << " to " << b 
          << ". Rank left: " << mpicomm.rank_left(0)
          << ". Rank right: " << mpicomm.rank_right(0) << ".";
        mpicomm.print(s.str());

        // initialize the alpha in the v-direction
        for(size_t i=0;i<d/2;i++) {
            Index n=e[d/2+i]; Index o=e[d/2+i+d];
            lalpha_v[i].resize(n*o);
            fp dv=(b[d/2+i]-a[d/2+i])/fp(n);
            for(Index j=0;j<n;j++)
                for(Index k=0;k<o;k++)
                    lalpha_v[i][o*j+k]=a[d/2+i]+j*dv+dv*gauss::x_scaled01(k,o);
        }

        // create the different MPI groups and communicators for the (lower
        // dimensional) Poisson solver
        array<Index,d> e_space;
        for(size_t i=0;i<d/2;i++) {
            e_space[i]=e[i];
            e_space[d/2+i]=e[d+i];
        }
        E.resize(e_space); rho.resize(e_space);
        rho_equidistant.resize(prod(e_space));
        E_equidistant.resize(prod(e_space));
        // groups for MPI_Reduce and MPI_Scatter
        int num_groups=1;
        for(size_t i=0;i<d/2;i++)
            num_groups*=mpicomm.num_nodes[i];
        groups.resize(num_groups); comms.resize(num_groups);
        MPI_Group grp_world; MPI_Comm_group(MPI_COMM_WORLD, &grp_world);
        array<Index,d/2> idx_space; fill(idx_space.begin(),idx_space.end(),0);
        array<Index,d/2> n_space;
        for(size_t i=0;i<d/2;i++)
            n_space[i]=mpicomm.num_nodes[i];
        do {
            vector<int> ranks;
            array<Index,d/2> idx_veloc;
            fill(idx_veloc.begin(),idx_veloc.end(),0);
            array<Index,d/2> n_veloc;
            for(size_t i=0;i<d/2;i++)
                n_veloc[i]=mpicomm.num_nodes[d/2+i];
            do {
                array<Index,d> nid;
                for(size_t i=0;i<d/2;i++) {
                    nid[i]=idx_space[i];
                    nid[d/2+i]=idx_veloc[i];
                }
                ranks.push_back(mpicomm.node_id_to_rank(nid));
            } while(iter_next(idx_veloc,n_veloc));
            int i_grp=offset(idx_space,n_space);
            MPI_Group_incl(grp_world,ranks.size(),&ranks[0],&groups[i_grp]);
            MPI_Comm_create(MPI_COMM_WORLD,groups[i_grp],&comms[i_grp]);
        } while(iter_next(idx_space,n_space));
        // groups for FFT transform
        fill(idx_space.begin(),idx_space.end(),0); vector<int> ranks;
        do {
            array<Index,d> nid;
            for(size_t i=0;i<d/2;i++) {
                nid[i]=idx_space[i];
                nid[d/2+i]=0;
            }
            ranks.push_back(mpicomm.node_id_to_rank(nid));
        } while(iter_next(idx_space,n_space));
        MPI_Group_incl(grp_world,ranks.size(),&ranks[0],&group_fft);
        MPI_Comm_create(MPI_COMM_WORLD,group_fft,&comm_fft);
        // fftw init
        fftw_mpi_init();
        // create the plan for the FFT
        int veloc_master=0;
        for(size_t i=d/2;i<d;i++)
            veloc_master+=mpicomm.node_id[i]; 
        is_veloc_master=(veloc_master==0) ? true : false;
        if(is_veloc_master) {
            long NNx=1;
            for(size_t i=0;i<d/2;i++)
                NNx*=e[i]*e[d+i]*mpicomm.num_nodes[i];
            Index alloc_local, local_ni, local_i_start, local_no, local_o_start;
            alloc_local=fftw_mpi_local_size_1d(NNx,comm_fft,FFTW_FORWARD,
                    FFTW_ESTIMATE,&local_ni,&local_i_start,&local_no,
                    &local_o_start);
            std::stringstream ss;
            ss << "Local FFT size: " << mpicomm.node_id
               << " " << alloc_local << " " << local_i_start << " " << local_ni 
               << " " << local_o_start << " " << local_no;
            if(NNx % (num_nodes[0]*num_nodes[0]) != 0)
                ss << endl << "WARNING: the FFTW data distribution is only "
                   << "consistent with this program if the total number of "
                   << "points is divisible by the square of the number of "
                   << "processes" << endl;
            mpicomm.print(ss.str(),comm_fft,num_nodes[0]);
            f_in=fftw_alloc_complex(alloc_local);
            f_out=fftw_alloc_complex(alloc_local);
            plan_fft  = fftw_mpi_plan_dft_1d(NNx,f_in,f_out,comm_fft,
                    FFTW_FORWARD,FFTW_ESTIMATE);
            plan_ifft = fftw_mpi_plan_dft_1d(NNx,f_out,f_in,comm_fft,
                    FFTW_BACKWARD,FFTW_ESTIMATE);
        }
        MPI_Group_free(&grp_world);

        // empty evolution.data
        if(enable_output) {
            std::ofstream fs(filename<2>("evolution",mpicomm.node_id).c_str());
            fs.close();
        }

        // check OpenMP support
        if(mpicomm.rank==0) {
            #ifdef _OPENMP
            cout << "OpenMP with " << omp_get_max_threads() << " threads" 
                 << endl;
            #else
            cout << "No OpenMP support" << endl;
            #endif
        }

        // wait for everyone
        MPI_Barrier(MPI_COMM_WORLD);
    }

    ~vlasovpoisson() {
        if(is_veloc_master) {
            fftw_free(f_in); fftw_free(f_out);
            fftw_destroy_plan(plan_fft);
            fftw_destroy_plan(plan_ifft);
        }
        fftw_mpi_cleanup();
    }

    /// This is the main entry point of the Vlasov--Poisson solver. It starts
    /// the computation, organizes the timings, and implements the Strang
    /// splitting scheme.
    void run() {
        // init initial value
        domain<d> in(e, a, b); in.init(f0);
        domain<d> out = in;
        poisson_fft(in,E);

        total.start();
        int n_steps=ceil(T/tau); fp t=0.0;
        for(int i=0;i<n_steps+1;i++) {
            // write evolution.data file
            write_variables(t,filename<2>("evolution",mpicomm.node_id), in, E);
            if(i % int(ceil(n_steps/fp(snapshots-1))) == 0 || i==n_steps) {
                string fn_f = (boost::format("f-t%1%")%t).str();
                string fn_E = (boost::format("E-t%1%")%t).str();
                poisson_fft(in,E);
                in.write_equi(filename<d>(fn_f,mpicomm.node_id),1000);
                in.write_equi(E,filename<2>(fn_E,mpicomm.node_id),1000);
            }

            if(i < n_steps+1) { // last step is only for output
                if(T-t<tau) tau=T-t;
                advection_v(0.5*tau,in,out);
                poisson_fft(out,E);
                advection_E(tau,out,in);
                advection_v(0.5*tau,in,out); in=out;
                t+=tau;
            }
        }
        total.stop();

        mpicomm.print(str(boost::format(
            "Runtime: %1% Commun: %2% Commun(Poisson): %3% Comput(Advect): %4% "
            "Comput(Poisson): %5% Prep: %6%")%total.total()%comm.total()%
                    comm_poisson.total()%advect.total()%poisson.total()%
                    prep.total()));
    }

    void advection(int dim, domain<d>& in, domain<d>& out, int dim2,
            vector<fp>& lalpha, fp tau) {
        if((Index)lalpha.size() != e[dim2]*e[d+dim2]) {
            cout << "ERROR: vector of alphas must have the same length as the "
                 << "corresponding dimension has degrees of freedom." << endl;
            exit(1);
        }
        //compute maximal alpha
        fp alpha_max=1.0;
        fp dd=(b[dim]-a[dim])/fp(e[dim]);
        for(size_t i=0;i<lalpha.size();i++)
            alpha_max=max(alpha_max,abs(tau*lalpha[i]/dd));
        int bdr_len=int(alpha_max)+1;

        // initialize temporary arrays holding boundary data
        array<Index,2*d-2> max = slice_no<d>(dim,e); int o=e[d+dim];
        Index bdr_tot_len=bdr_len*o*prod(max);
        vector<fp> bdr_to(bdr_tot_len);
        vector<fp> bdr_from_left(bdr_tot_len);
        vector<fp> bdr_from_right(bdr_tot_len);

        MPI_Barrier(MPI_COMM_WORLD); prep.start();
        in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,&lalpha,dim2);
        prep.stop();
        MPI_Barrier(MPI_COMM_WORLD); comm.start(); 
        // start sending and receiving the data
        MPI_Request reqs[4];
        // since alpha can be both positive and negative we have to send our
        // data to the left and right adjacent process
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_left(dim),  1,
                MPI_COMM_WORLD, reqs);
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(dim), 2,
                MPI_COMM_WORLD, reqs+1);
        MPI_Irecv(&bdr_from_left[0],  bdr_from_left.size(), MPI_FP,
                mpicomm.rank_left(dim),   2, MPI_COMM_WORLD, reqs+2);
        MPI_Irecv(&bdr_from_right[0], bdr_from_right.size(), MPI_FP, 
                mpicomm.rank_right(dim), 1, MPI_COMM_WORLD, reqs+3);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        MPI_Barrier(MPI_COMM_WORLD); advect.start();
        int ldim2=(dim2>dim)?dim2-1:dim2; int ldim2o=ldim2+d-1;

        // set up the fast advection routines
        std::unique_ptr< dg_coeff<2> > dg2_c;
        std::unique_ptr< dg_coeff<4> > dg4_c;
        std::unique_ptr< dg_coeff<6> > dg6_c;
        if(o==2)
            dg2_c.reset(new dg_coeff<2>());
        else if(o==4)
            dg4_c.reset(new dg_coeff<4>());
        else if(o==6)
            dg6_c.reset(new dg_coeff<6>());
        else {
            cout << "ERROR: currently only o=2,4,6 are implemented." << endl;

        }

        for(Index i=0;i<max[0];i++) {
            array<Index,2*d-2> max1=max; max1[0]=0;
            array<Index,2*d-2> idx; fill(idx.begin(),idx.end(),0);
            do {
                idx[0]=i;
                domain<2>::view vwork = slice_array(in.data, dim, idx);
                domain<2>::view vout = slice_array(out.data, dim, idx);
                fp alpha=tau*lalpha[e[d+dim2]*idx[ldim2]+idx[ldim2o]]/dd;
                if(o==2)
                    translate1d<1,2>(vwork, vout, NULL, bdr_len*o, alpha,
                            *dg2_c, TM_INTERIOR);
                else if(o==4)
                    translate1d<1,4>(vwork, vout, NULL, bdr_len*o, alpha,
                            *dg4_c, TM_INTERIOR);
                else if(o==6)
                    translate1d<1,6>(vwork, vout, NULL, bdr_len*o, alpha,
                            *dg6_c, TM_INTERIOR);
            } while(iter_next(idx,max1));
        }
        advect.stop();

        MPI_Barrier(MPI_COMM_WORLD); 
        comm.start();
        MPI_Status stats[4];
        MPI_Waitall(4, reqs, stats);
        comm.stop();

        MPI_Barrier(MPI_COMM_WORLD); advect.start();
        for(Index i=0;i<max[0];i++) {
            array<Index,2*d-2> max2=max; max2[0]=0;
            array<Index,2*d-2> idx; fill(idx.begin(),idx.end(),0);
            do {
                idx[0]=i;
                domain<2>::view vwork = slice_array(in.data, dim, idx);
                domain<2>::view vout = slice_array(out.data, dim, idx);
                fp alpha=tau*lalpha[e[d+dim2]*idx[ldim2]+idx[ldim2o]]/dd;
                fp* bdr_from_l;
                if(alpha>=0)
                    bdr_from_l=in.unpack_boundary(idx, dim, bdr_len,
                            bdr_from_left);
                else
                    bdr_from_l=in.unpack_boundary(idx, dim, bdr_len,
                            bdr_from_right);

                if(o==2)
                    translate1d<1,2>(vwork, vout, bdr_from_l, bdr_len*o, alpha,
                            *dg2_c, TM_BOUNDARY);
                else if(o==4)
                    translate1d<1,4>(vwork, vout, bdr_from_l, bdr_len*o, alpha,
                            *dg4_c, TM_BOUNDARY);
                else if(o==6)
                    translate1d<1,6>(vwork, vout, bdr_from_l, bdr_len*o, alpha,
                            *dg6_c, TM_BOUNDARY);
            } while(iter_next(idx,max2));
        }
        advect.stop();
    }

    void advection_v(fp tau, domain<d>& in, domain<d>& out) {
        advection(0,in,out,1,lalpha_v[0],tau);
    }

    void advection_E(fp tau, domain<d>& in, domain<d>& out) {
        vector<fp> EE(E.num_elements());
        for(Index i=0;i<E.num_elements();i++) EE[i]=*(E.data()+i);
        advection(1,in,out,0,EE,tau);
    }

    /// This function solves Poisson's equation using the FFTW library. The
    /// three steps are:
    /// - Compute the density on each MPI process and and send the result to the
    ///   MPI processes that participate in the linear solve (the group_fft
    ///   group)
    /// - Perform the FFT to solve Poisson's equation
    /// - Distribute the computed results to the relevant MPI processes.
    ///
    /// Since the FFT opertes on equidistant data we have to perform an
    /// interpolation between the Gauss-Legendre points employed in our
    /// discontinuous Galerkin implementation and an equidistant grid.
    void poisson_fft(domain<d>& work, multi_array<fp,d>& E) {
        MPI_Barrier(MPI_COMM_WORLD); poisson.start();
        array<Index,2*d> idx;
        fill(idx.begin(), idx.end(), 0);
        array<Index,d> idx_x;
        int num_nodes_v=1;
        for(size_t i=d/2;i<d;i++)
            num_nodes_v*=mpicomm.num_nodes[i];
        fill(rho.data(), rho.data()+rho.num_elements(), -1/fp(num_nodes_v));
        // perform the integration in v-space
        do {
            fp w=1.0;
            for(size_t i=0;i<d/2;i++) {
                idx_x[i]=idx[i]; idx_x[d/2+i]=idx[d+i];
                fp dv=(b[d/2+i]-a[d/2+i])/fp(e[d/2+i]);
                w*=0.5*dv*gauss::w(idx[d+d/2+i],ov);
            }
            rho(idx_x)+=w*work.data(idx);
        } while(iter_next(idx,e));
        // convert to equidistant coordinates
        for(int i=0;i<Nx;i++) // TODO: 2d only
            for(int j=0;j<ox;j++) {
                rho_equidistant[ox*i+j]=evaluate1d(equi_x(j,ox),ox,&rho(i,0));
            }
        poisson.stop();

        MPI_Barrier(MPI_COMM_WORLD);
        comm_poisson.start();
        array<int,d/2> nid; array<int,d/2> nn;
        for(size_t i=0;i<d/2;i++) { 
            nid[i]=mpicomm.node_id[i];
            nn[i]=mpicomm.num_nodes[i];
        }
        int group_id=offset(nid,nn);
        if(is_veloc_master) { // master
            // 0 refers to the rank in the groups[group_id] group 
            MPI_Reduce(MPI_IN_PLACE,&rho_equidistant[0],rho_equidistant.size(),
                    MPI_FP,MPI_SUM,0,comms[group_id]);
        } else {
            MPI_Reduce(rho.data(),&rho_equidistant[0],rho_equidistant.size(),
                    MPI_FP,MPI_SUM, 0,comms[group_id]);
        }
        comm_poisson.stop();

        MPI_Barrier(MPI_COMM_WORLD); 
        poisson.start();
        if(mpicomm.node_id[1]==0) {
            for(int i=0;i<Nx;i++)
                for(int j=0;j<ox;j++) {
                    f_in[ox*i+j][0]=rho_equidistant[ox*i+j];
                    f_in[ox*i+j][1]=0.0;
                }
            fftw_execute(plan_fft);
            int start=mpicomm.node_id[0]*Nx*ox;
            int end=start+Nx*ox;
            int total=mpicomm.num_nodes[0]*Nx*ox;
            for(int i=start;i<end;i++) {
                fp i_freq = (i<total/2+1) ? i : -(total-i);
                fp k= (2*M_PI/L)*i_freq;
                // divide by complex<fp>(0,1)*k
                int li=i-start;
                fp re=f_out[li][0]; fp im=f_out[li][1];
                f_out[li][0] = (i==0) ? 0.0 :  im/k/fp(total);
                f_out[li][1] = (i==0) ? 0.0 : -re/k/fp(total);
            }
            fftw_execute(plan_ifft);
            for(int i=0;i<Nx;i++)
                for(int j=0;j<ox;j++) {
                    E_equidistant[i*ox+j]=f_in[ox*i+j][0]; 
                }
        }
        poisson.stop();

        MPI_Barrier(MPI_COMM_WORLD); poisson.start();
        if(mpicomm.node_id[1]==0) { //TODO: 2d only
            for(int i=0;i<Nx;i++)
                for(int j=0;j<ox;j++){
                    E(i,j)=interpolate1d(gauss::x_scaled01(j,ox),ox,
                            &E_equidistant[ox*i]);
                }
        }
        poisson.stop();

        MPI_Barrier(MPI_COMM_WORLD);
        comm_poisson.start();
        MPI_Bcast(E.data(),E.num_elements(),MPI_FP,0,comms[group_id]);
        comm_poisson.stop();
    }

    /// This computed and write to disk certain physical quantities that are
    /// often of interest in Vlasov simulations (conserved quantities such as
    /// mass, l1, and l2 norm and the kinetic/electric energy).
    void write_variables(fp t, string filename, domain<d>& f,
            multi_array<fp,d>& E) {
        if(enable_output) {
            fp E_energy=0.0;
            if(is_veloc_master) {
                array<Index,d> idx; fill(idx.begin(),idx.end(),0);
                array<Index,d> e_space;
                for(size_t i=0;i<d/2;i++) {
                    e_space[i]=e[i];
                    e_space[d/2+i]=e[d+i];
                }
                do {
                    fp w=1.0;
                    for(size_t i=0;i<d/2;i++) {
                        fp dx=(b[i]-a[i])/fp(e_space[i]);
                        w*=0.5*dx*gauss::w(idx[d/2+i],e_space[d/2+i]);
                    }
                    E_energy += 0.5*w*pow(E(idx),2);
                } while(iter_next(idx,e_space));

            }
            fp mass=0.0; fp momentum=0.0; fp l1=0.0; fp l2=0.0; fp K_energy=0.0;
            fp entropy=0.0;
            array<Index,2*d> idx; fill(idx.begin(),idx.end(),0);
            do {
                fp w=1.0;
                array<fp,d> x;
                for(size_t i=0;i<d;i++) {
                    fp dx=(b[i]-a[i])/fp(e[i]);
                    w*=0.5*dx*gauss::w(idx[d+i],e[d+i]);
                    x[i] = f.a[i] + idx[i]*dx 
                           + dx*gauss::x_scaled01(idx[d+i],e[d+i]);
                }
                mass += w*f.data(idx);
                l1 += w*abs(f.data(idx));
                l2 += w*pow(f.data(idx),2);
                fp fc = max(f.data(idx),fp(1e-32));
                entropy += w*fc*log(fc);
                fp vsq=0.0; for(size_t i=0;i<d/2;i++) vsq+=pow(x[d/2+i],2);
                K_energy += 0.5*w*f.data(idx)*vsq;
                momentum += w*x[d/2]*f.data(idx);
            } while(iter_next(idx,e));

            std::ofstream fs(filename.c_str(),std::ofstream::app);
            fs.precision(16);
            fs << t << " " << mass << " " << momentum << " " 
               << E_energy+K_energy << " " << K_energy << " " << E_energy 
               <<  " " << l2 << " " << entropy << " " << l1 << endl;
            fs.close();
        }
    }
};

