#pragma once

#include <generic/common.hpp>

#include <generic/utility.hpp>
#include <generic/mpi.hpp>

/// This enum is used in the domain::pack_boundary function to specify what
/// boundary data should be stored in the linear data format (these are
/// subsequently send via MPI to neighboring processes).
enum leftright { 
    BDRY_LEFT,    ///< the flow is always from right to left
    BDRY_RIGHT,   ///< the flow is always from left to right
    BDRY_DYNAMIC  ///< the flow (depending on the value of the remaining 
                  ///  dimensions) can be in either direction and the correct
                  ///  boundary data has to be determined dynamically.
};

/// This class manages the data stored on each MPI process. This includes the
/// coefficients in the polynomial basis as well as some additional information.
/// It includes methods for initialization, file output, obtaining a slice into
/// the coefficient array, and provides a dimension independent way of
/// linearizing the boundary values.
template<size_t d>
struct domain {
    typedef multi_array<fp, 2*d> domain_data;
    typedef multi_array_view<fp> view;

    /// Stores the extension of the physical domain.
    array<fp,d> a;
    array<fp,d> b;

    domain_data data;
    mpi_process<d> *mpi;
    /// Stores the number of cells and number of coefficients stored per cell.
    array<Index,2*d> extension;

    // allows initialization on the GPU (data from dom is automatically copied
    // to the GPU)
    domain(const domain& dom, bool gpu=false) {
        if(!gpu)
            *this = dom;
        else {
#ifdef __CUDACC__
            a = dom.a;
            b = dom.b;
            mpi = dom.mpi;
            extension = dom.extension;

            data.gpu = true;
            data.resize(extension);

            data = dom.data;
#else
            cout << "ERROR: compiled without GPU support" << __FILE__ << ":" 
                 << __LINE__ << endl;
            exit(1);
#endif
        }
    }

    domain(array<Index,2*d> _extension, array<fp,d> _a, array<fp,d> _b,
            mpi_process<d> *_mpi=NULL)
        : a(_a), b(_b), mpi(_mpi), extension(_extension) {
        data.resize(extension);
    }
    
    domain(array<Index,2*d> _extension, array<fp,d> _a, array<fp,d> _b,
            bool gpu, mpi_process<d> *_mpi=NULL) 
        : a(_a), b(_b), mpi(_mpi), extension(_extension) {
        data.gpu = gpu;
        data.resize(extension);
    }

    /// Initialize the coefficient array from the function f.
    void init(fp (*f)(array<fp,d>));
    /// Write an output to a text file that can be plotted with gnuplot. The
    /// variable text is includes as a commented header.
    void write(string file, string text="");
    /// Write an output that is given by an external data source (must be a
    /// boost::multi_array).
    void write(multi_array<fp,d>& E, string file, string text="");

    /// Write an output to a text file using an equidistant grif of size n.
    void write_equi(string file, Index n, string text="");
    void write_equi(multi_array<fp,d>& E, string file, Index n, string text="");

    /// Obtain the slice of the coefficient array that is constructed by holding

    /// Determine the required boundary data and construct a linearized version
    /// that can be send via MPI. The caller is responsible for passing a vector
    /// that has been initialized with the correct number of elements.  If
    /// BDRY_DYNAMIC (see the relevant documentation) is set the variable dim2
    /// gives the dimension that determines the direction of the flow. The
    /// vector lalpha is then used to determine if the flow is from the left to
    /// the right or vice versa. Note that only the sign of lalpha is used.
    void pack_boundary(size_t dim, leftright lr, Index bdr_len,
            vector<fp>& bdr_data, std::function<fp(array<Index,2*d>)> F);
    void pack_boundary(size_t dim, leftright lr, Index bdr_len, 
            vector<fp>& bdr_data, multi_array<fp,d>* F);
    // TODO: this is the original implementation which is slower
    void pack_boundary(size_t dim, leftright lr, Index bdr_len,
            vector<fp>& bdr_data, vector<fp>* lalpha=nullptr, Index dim2=0);

    fp* unpack_boundary(array<Index,2*d-2> idx, size_t dim, Index bdr_len,
            vector<fp>& bdr_data);
};

template<Index d>
array<Index,d> get_compl_idx(Index dim, array<Index,2*d> _idx) {
    array<Index,d> idx;
    fill(idx.begin(),idx.end(),0);
    if(dim < d/2) {
        copy(_idx.begin()+d/2,   _idx.begin()+d,   idx.begin());
        copy(_idx.begin()+d+d/2, _idx.begin()+2*d, idx.begin()+d/2);
    } else {
        copy(_idx.begin(),   _idx.begin()+d/2,   idx.begin());
        copy(_idx.begin()+d, _idx.begin()+d+d/2, idx.begin()+d/2);
    }
    return idx;
}
    
// We have to extract the right value from F (in advection). Computing the
// correct index is not really difficult but a bit cumbersome to do for
// arbitrary d.
template<Index d>
array<Index,d> get_idx_F(Index dim, array<Index,2*d-2> idx) {
    // First create a full index (length 2*d)
    array<Index,2*d> _idx;
    int k = 0;
    for(Index i=0;i<2*d;i++) {
        if(i!=dim && i!=d+dim) {
            _idx[i] = idx[k]; k++;
        }
    }

    // Then select the part of the index we need.
    return get_compl_idx<d>(dim, _idx);
}


template<size_t d>
void domain<d>::init(fp (*f)(array<fp, d>)) {
    array<Index,2*d> idx;
    for(size_t i=0;i<2*d;i++)
        idx[i]=0;

    array<fp,d> x;
    do {
        for(size_t i=0;i<d;i++) {
            fp dx = (b[i]-a[i])/fp(extension[i]);
            x[i] = a[i] + idx[i]*dx
                   + dx*gauss::x_scaled01(idx[d+i],extension[d+i]);
        }
        data(idx)=f(x);
    } while(iter_next(idx,extension));
}

extern bool enable_output;

template<size_t d>
void domain<d>::write(string file, string text) {
    if(enable_output) {
        std::ofstream fs(file.c_str());
        fs << "# " << text << endl;
        array<Index,2*d> idx;
        for(size_t i=0;i<2*d;i++)
            idx[i]=0;
        array<fp,d> x;
        do {
            for(size_t i=0;i<d;i++) {
                fp dx = (b[i]-a[i])/fp(extension[i]);
                x[i] = a[i] + idx[i]*dx
                       + dx*gauss::x_scaled01(idx[d+i],extension[d+i]);
                fs << x[i] << " ";
            }
            fs << data(idx) << endl;
        } while(iter_next(idx,extension));
        fs.close();
    }
}

template<>
void domain<1>::write_equi(string file, Index n, string text) {
    if(enable_output) {
        std::ofstream fs(file.c_str()); fs << "# " << text << endl;
        fs.precision(17);

        for(Index i=0;i<n;i++) {
            fp h = (b[0]-a[0])/fp(extension[0]);
            fp x = i*(b[0]-a[0])/fp(n);

            Index cell   = Index(floor(x/h));
            fp x_cell = (x - cell*h)/h;

            fp f = evaluate1d(x_cell, extension[1],
                    &data(array<Index,2>({cell,0ul})));
            fs << x << " " << f << endl;
        }
        fs.close();
    }
}

template<>
void domain<2>::write_equi(string file, Index n, string text) {
    if(enable_output) {
        std::ofstream fs(file.c_str()); fs << "# " << text << endl;
        fs.precision(17);

        for(Index i=0;i<n;i++) {
            for(Index j=0;j<n;j++) {
                fp hx = (b[0]-a[0])/fp(extension[0]);
                fp x = i*(b[0]-a[0])/fp(n);

                fp hy = (b[1]-a[1])/fp(extension[1]);
                fp y = j*(b[1]-a[1])/fp(n);

                Index cell_x   = Index(floor(x/hx));
                fp x_cell = (x - cell_x*hx)/hx;

                Index cell_y   = Index(floor(y/hy));
                fp y_cell = (y - cell_y*hy)/hy;

                fp r = 0.0;
                for(Index m=0;m<extension[2];m++)
                    for(Index k=0;k<extension[3];k++)
                        r+=data(array<Index,4>({cell_x,cell_y,m,k}))
                            *lagrange(x_cell,m,extension[2])
                            *lagrange(y_cell,k,extension[3]);

                fs << x << " " << y << " " << r << endl;
            }
        }
        fs.close();
    }
}


template<size_t d>
void domain<d>::write(multi_array<fp,d>& E, string file, string text) {
    if(enable_output) {
        std::ofstream fs(file.c_str());
        fs << "# " << text << endl;
        array<Index,d> idx; fill(idx.begin(),idx.end(),0);
        array<Index,d> ext; for(Index i=0;i<d/2;i++) { 
            ext[i]=extension[i];
            ext[d/2+i]=extension[d+i];
        }
        array<fp,d/2> x;
        do {
            for(Index i=0;i<d/2;i++) {
                fp dx = (b[i]-a[i])/fp(extension[i]);
                x[i] = a[i] + idx[i]*dx 
                       + dx*gauss::x_scaled01(idx[d/2+i],extension[d+i]);
                fs << x[i] << " ";
            }
            fs << E(idx) << endl;
        } while(iter_next(idx,ext));
        fs.close();
    }
}

template<>
void domain<2>::write_equi(multi_array<fp,2>& E, string file, Index n, 
        string text) {
    if(enable_output) {
        std::ofstream fs(file.c_str()); fs << "# " << text << endl;
        fs.precision(17);

        for(Index i=0;i<n;i++) {
            fp h = (b[0]-a[0])/fp(extension[0]);
            fp x = i*(b[0]-a[0])/fp(n);

            Index cell   = int(floor(x/h));
            fp x_cell = (x - cell*h)/h;

            fp e = evaluate1d(x_cell, extension[2], 
                    &E(array<Index,2>({cell,0})));
            fs << x << " " << e << endl;
        }
        fs.close();
    }
}

template<size_t d>
void domain<d>::pack_boundary(size_t dim, leftright lr, Index bdr_len, 
        vector<fp>& bdr_data, multi_array<fp,d>* F) {

    pack_boundary(dim, lr, bdr_len, bdr_data, [dim,F](array<Index,2*d> idx) {
            array<Index,d> idx_compl = get_compl_idx<d>(dim, idx);
            return (*F)(idx_compl);
            });
}

template<size_t d>
void domain<d>::pack_boundary(size_t dim, leftright lr, Index bdr_len, 
        vector<fp>& bdr_data, std::function<fp(array<Index,2*d>)> F) {
    // check that there is enough space
    int total=1; 
    for(size_t i=0;i<d;i++)
        total*=(i==dim)?(extension[d+i]*bdr_len):(extension[i]*extension[d+i]);
    if(total>int(bdr_data.size())) {
        cout << "ERROR: total storage necessary " << total 
             << " vs available storage " << bdr_data.size() << endl;
        exit(1);
    }
    array<Index,2*d> max=extension;
    array<Index,2*d> idx; fill(idx.begin(),idx.end(),0);
    // do not iterate over the dimension dim (we do that inside the loop)
    max[dim]=1; max[d+dim]=1;

    int n=extension[dim]; int o=extension[d+dim];


    Index single_out=2;
    if(dim == 2)
        single_out = 3;

    max[dim]   = bdr_len;
    max[d+dim] =  o;
    max[single_out] = 1;
    //Index lin_idx = 0;
    #pragma omp parallel for schedule(static)
    for(Index k=0;k<extension[single_out];k++)
    for(auto idx : range<2*d>(max)) {
        idx[single_out] = k;

        // global offset
        int global_offset=0; int prod=1;
        for(size_t i=0;i<d;i++) {
            if(i!=dim) {
                global_offset+=(idx[i]*extension[d+i]+idx[d+i])*prod;
                prod*=extension[i]*extension[i+d];
            }
        }

        // chose left or right
        //array<Index,d> idx_compl = get_compl_idx<d>(dim, idx);
        //leftright lrdynamic = ((*F)(idx_compl)>=0) ? BDRY_RIGHT : BDRY_LEFT;
        leftright lrdynamic = (F(idx)>=0) ? BDRY_RIGHT : BDRY_LEFT;

        array<Index,2*d> _idx = idx;
        if(lrdynamic==BDRY_RIGHT)
            _idx[dim] = n-1-idx[dim];

        bdr_data[bdr_len*o*global_offset+o*idx[dim]+idx[d+dim]] = data(_idx);

        idx[single_out] = 0;
    }
}

template<size_t d>
void domain<d>::pack_boundary(size_t dim, leftright lr, Index bdr_len,
        vector<fp>& bdr_data, vector<fp> *lalpha, Index dim2) {
    // check that there is enough space
    int total=1; 
    for(size_t i=0;i<d;i++)
        total*=(i==dim)?(extension[d+i]*bdr_len):(extension[i]*extension[d+i]);
    if(total>int(bdr_data.size())) {
        cout << "ERROR: total storage necessary " << total 
             << " vs available storage " << bdr_data.size() << endl;
        exit(1);
    }
    array<Index,2*d> max=extension;
    array<Index,2*d> idx; fill(idx.begin(),idx.end(),0);
    // do not iterate over the dimension dim (we do that inside the loop)
    max[dim]=0; max[d+dim]=0;

    Index n=extension[dim]; Index o=extension[d+dim];
    do {
        // compute the offset in the linear storage format
        Index global_offset=0; Index prod=1;
        for(size_t i=0;i<d;i++) {
            if(i!=dim) {
                global_offset+=(idx[i]*extension[d+i]+idx[d+i])*prod;
                prod*=extension[i]*extension[i+d];
            }
        }
        // save to linear storage format
        for(Index i=0;i<bdr_len;i++) {
            for(Index j=0;j<o;j++) {
                array<Index,2*d> lidx=idx; lidx[d+dim]=j; lidx[dim]=i;
                // determine if left or right boundary is requested dynamically
                // (if necessary)
                leftright lrdynamic=lr;
                if(lr==BDRY_DYNAMIC)
                    lrdynamic=((*lalpha)[extension[d+dim2]*idx[dim2]
                            +idx[d+dim2]]>=0) ? BDRY_RIGHT : BDRY_LEFT;
                // compute the offset and store the data
                lidx[dim]= (lrdynamic==BDRY_LEFT) ? i : (n-1-i);
                bdr_data[bdr_len*o*global_offset+o*i+j]=data(lidx);
            }
        }
    } while(iter_next(idx,max));
}


template<size_t d>
fp* domain<d>::unpack_boundary(array<Index,2*d-2> idx, size_t dim,
        Index bdr_len, vector<fp>& bdr_data) {
    // compute the offset in the linear storage format
    int global_offset=0; int prod=1;
    for(size_t i=0;i<dim;i++) {
        global_offset+=(idx[i]*extension[i+d]+idx[i+(d-1)])*prod;
        prod*=extension[i]*extension[i+d];
    } 
    // dim in idx is dim+1 in full index (length 2*d)
    for(size_t i=dim;i<d-1;i++) {
        global_offset+=(idx[i]*extension[i+1+d]+idx[i+(d-1)])*prod;
        prod*=extension[i+1]*extension[i+1+d];
    }
    int o=extension[dim+d];
    // return the pointer to the relevant data
    return &bdr_data[bdr_len*o*global_offset];
}

