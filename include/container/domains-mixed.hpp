#pragma once

#include <generic/fft.hpp>

// Data structure choices:
//    store everything equidistant
//    interpolate to dG, Burgers' equation, evaluate on equidistant

struct domain2d_equi {
    private:
        domain2d_equi() {}
    public:

        int nx, ox;
        int Nx, Ny;
        fp* un;
        fp Lx, Ly;
        fp hx, hy;

        domain2d_equi(int _nx, int _ox, int _ny, fp _Lx, fp _Ly) {
            nx = _nx; ox = _ox;
            Nx = nx*ox; Ny = _ny;
            Lx = _Lx; Ly = _Ly;
            hx = Lx/fp(Nx); hy = Ly/fp(Ny);
            un = fftw_alloc_real(Nx*Ny);
            if(un == NULL) {
                cout << "ERROR: could not initialize memory " << Nx << " " 
                     << Ny <<  " " << ox << endl;
                exit(1);
            }
        }

        ~domain2d_equi() {
            fftw_free(un);
        }

        fp& operator()(int i, int j) {
            return un[i + j*Nx];
        }

        fp x(int i) {
            // TODO: allow non-symmetric domains
            return -0.5*Lx + i*hx;
        }

        fp y(int j) {
            // TODO: allow non-symmetric domains
            return -0.5*Ly + j*hy;
        }

        void init(fp (*u0)(fp,fp)) {
            for(int j=0;j<Ny;j++)
                for(int i=0;i<Nx;i++)
                    (*this)(i,j) = u0(x(i),y(j));
        }

        void write(string fn) {
            std::ofstream fs(fn.c_str());
            fs.precision(16);
            for(int j=0;j<Ny;j++)
                for(int i=0;i<Nx;i++)
                    fs << x(i) << " " << y(j) << " " << (*this)(i,j) << endl;
            fs.close();
        }

        fp max_amplitude() {
            return *std::max_element(un, un+Nx*Ny);
        }

        fp mass() {
            return accumulate(un, un+Nx*Ny, 0.0)*hx*hy;
        }

        // momentum (L^2 norm squared)
        fp momentum() {
            return std::inner_product(un, un+Nx*Ny, un, 0.0)*hx*hy;
        }

};

struct domain2d_frequency {

    int Nx, Ny;
    fp Lx, Ly;
    fp hx, hy;
    fftw_complex* uf;

    domain2d_frequency(int _Nx, int _Ny, fp _Lx, fp _Ly) {
        Nx = _Nx; Ny = _Ny; Lx = _Lx; Ly = _Ly;
        uf = fftw_alloc_complex((Ny/2+1)*Nx);
    }

    ~domain2d_frequency() {
        fftw_free(uf);
    }
};


struct domain2d_dG {
    typedef multi_array<fp, 3> domain_data;

    int nx, Ny, ox;
    fp Lx, Ly;
    domain_data data;

    domain2d_dG(int _nx, int _ox, int _Ny, fp _Lx, fp _Ly) {
        nx = _nx; ox = _ox; Ny = _Ny;
        Lx = _Lx; Ly = _Ly;
        data.resize(array<Index,3>({Ny,nx,ox}));
    }
};

