#!/usr/bin/env python
from numpy import *
from pylab import *

import sys

def load_evol(directory):
    fn=directory+'/evolution-{0},{1}.data'; i=1; j=0;
    print('Try to load ',fn.format(0,0))
    data=loadtxt(fn.format(0,0))
    x=data[:,0]; ee=data[:,5]; te=data[:,6]
    while True:
        try:
            while True:
                fns=fn.format(i,j)
                print('Trying to read ',fns)
                newdata=loadtxt(fns);
                for k in range(data.shape[0]):
                    ee[k]+=newdata[k][5]
                    te[k]+=newdata[k][6]
                i+=1
        except FileNotFoundError:
            if i==0:
                break
            i=0; j+=1
    print('Loaded {0} files with dimension {1}'.format(i,data.shape));
    return (x,ee,te)

def load_density(directory, procs):
    fn=directory+'/out-{0},{1}.data'
    data=zeros((0,3))
    for j in range(0,procs):
        for i in range(0,procs):
            fns=fn.format(i,j)
            datanew=loadtxt(fns)
            print(shape(datanew))
            data=append(data,datanew,axis=0)
    data=data[np.lexsort((data[:,0],data[:,1]))]
    # sort(data, axis=0)
    print(shape(data))
    x=data[:,0]; y=data[:,1]; z=data[:,2]
    n=int(sqrt(len(z)/2))
    return (min(x),max(x),min(y),max(y),z.reshape(n,2*n))

rcParams.update({'font.size': 18})
fig,axes=subplots(nrows=3,ncols=2,figsize=(15, 14.4))
# fig.tight_layout()
# rc('xtick', labelsize=16) 
# rc('ytick', labelsize=16)
# subplots_adjust(wspace=0.45)

dirs=['validation-stronglandau/16','validation-stronglandau/64','validation-stronglandau/256',
        'validation-stronglandau/1024-o3',
        '/home/lukas/validation-stronglandau/1024',
        '/home/lukas/validation-stronglandau/1024-finer']
cores=[4, 8, 16, 32, 32, 32]; #
# dirs=['/home/lukas/validation-stronglandau/1024-finer']
# cores=[32]; #
num_subs=6
for k in range(6):
    [x,ee,te] = load_evol(dirs[mod(k,num_subs)])
    [minx,maxx,miny,maxy,z]=load_density(dirs[mod(k,num_subs)],cores[mod(k,num_subs)])
    subplot(6,2,1+2*k);
    imshow(z,extent=(minx,maxx,miny,maxy),aspect=0.5);
    colorbar(ticks=[0,0.15,0.3,0.45])
    ylabel('v'); yticks([-6,-3,0,3,6])
    if k==5:
        xlabel('x'); 
        xticks([0,2,4,6,8,10,12]);
    else:
        xticks([]);
    if k==0:
        title('particle density', fontsize=18);

    subplot(6,2,2+2*k);
    semilogy(x,ee,linewidth=2);
    semilogy([0,7.5],[5,5*exp(-0.5*1.473*7.5)],'k-',linewidth=2)
    yticks([1e-9,1e-7, 1e-5,1e-3,1e-1,1e1])
    if k==5:
        xlabel('time'); 
    xticks([0,20,40,60,80,100]); yticks([1e-9,1e-6,1e-3,1e-0])
    if k==0:
         title('electric energy', fontsize=18);
    # subplot(3,3,3+3*k); semilogy(x,abs((te-te[0])/te[0]));
    # xticks([0,100,200,300])
    # if k==0:
        # title('energy error', fontsize=18); 

savefig('vp-validation.pdf', bbox_inches='tight', pad_inches=0)

