#define GENERIC_NO_STORAGE
#include <generic/common.hpp>

#include <boost/program_options.hpp>
#include <boost/math/special_functions/bessel.hpp>
using namespace boost::math;


#include <boost/multi_array.hpp>
#define USE_BOOST_MULTIARRAY
template<class T, Index d>
using multi_array = typename boost::multi_array<T,d>;
template<class T>
using multi_array_view = typename multi_array<T,3>::template array_view<2>::type;
#include <container/domain.hpp>

#include <algorithms/burgers_dg.hpp>

bool enable_output; //is read in domain.hpp

fp iv_gauss(array<fp,1> x) {
    return 0.25*exp(-10.0*pow(x[0]-M_PI,2));
}

fp iv_periodic(array<fp,1> x) {
    return sin(x[0]);
}

fp iv_sech(array<fp,1> x) {
    return sech(x[0]);
}

fp iv_lorentz(array<fp,1> x) {
    return 1.0/(1.0+pow(x[0],2));
}

fp iv_osc(array<fp,1> x) {
    if(abs(x[0]) < 1)
        return cos(20*(0.5+abs(x[0]))*x[0])*exp(-1/(1-pow(x[0],2)));

    else
        return 0.0;
}

fp sol_periodic(fp t, fp x) {
    fp s=0.0;
    for(int n=1;n<1000;n++) {
        s += 2.0*cyl_bessel_j(n, -n*t)/(-n*t)*sin(n*x);
    }
    return s;
}

fp mass(domain<1>& u, int n, int o, fp h) {
    fp s = 0.0;
    for(int i=0;i<n;i++)
        for(int j=0;j<o;j++)
            s += 0.5*h*gauss::w(j,o)*u.data[i][j];
    return s;
}

struct burgers {
    fp a, b, T, tau;
    int n, o, snapshots, num_it;
    fp(*u0)(array<fp,1>);
    iterative_method im;
    timer t_step;

    burgers(fp _a, fp _b, int _n, int _o, fp _T, fp _tau, fp(*_u0)(array<fp,1>),
            int _snapshots, int _num_it, iterative_method _im) {
        a=_a; b=_b; n=_n; o=_o; T=_T; tau=_tau; u0=_u0; snapshots=_snapshots;
        num_it=_num_it; im=_im;
    }

    void run() {
        array<fp,1> va = {{a}}; array<fp,1> vb = {{b}};
        array<Index,2> ve = {{n,o}};
        domain<1> in(ve, va, vb); in.init(u0);
        domain<1> out = in;

        // boundary information
        // this is rather conservative and not ideal from a performance
        // standpoint but it allows us to take large time steps for this test
        // example
        int bdr_len=n; 
        vector<fp> left_bdr(bdr_len*o);
        vector<fp> right_bdr(bdr_len*o);

        // initial mass
        fp h = (b-a)/fp(n);
        fp m0 = mass(in,n,o,h);

        int n_steps=ceil(T/tau); fp t=0.0;
        for(int i=0;i<n_steps+1;i++) {
            string fn = (boost::format("f-t%1%.data")%t).str();
            if(i % int(ceil(n_steps/fp(snapshots-1))) == 0 || i==n_steps)
                in.write_equi(fn,1000);

            t_step.start();
            if(i < n_steps+1) { // last step is only for output
                if(T-t<tau) tau=T-t;
                typedef typename domain<1>::domain_data::index_range range;
                domain<1>::view slice_in  =
                    in.data[ boost::indices[range()][range()] ];
                domain<1>::view slice_out =
                    out.data[ boost::indices[range()][range()] ];

                in.pack_boundary(0,BDRY_RIGHT,bdr_len,left_bdr);
                in.pack_boundary(0,BDRY_LEFT,bdr_len,right_bdr);
                burgers_dg(slice_in,slice_out,tau,b-a,&left_bdr[0],
                        &right_bdr[0],bdr_len,num_it,im);
                in = out;
                t+=tau;
            }
            t_step.stop();
        }

        // If possible check the error against the exact solution
        if(u0 == iv_periodic) {
            fp err = 0.0;
            for(int i=0;i<n;i++) {
                for(int j=0;j<o;j++) {
                    fp x = a + h*(i+gauss::x_scaled01(j,o));
                    err = max(err,abs(sol_periodic(T, x)-out.data[i][j]));
                }
            }
            cout << "Error: " << err << endl;
        }
        fp mT = mass(out,n,o,h);
        cout << "Error (mass): " << abs(mT-m0) << endl;
        cout << "Time/step: " << 1e3*t_step.average() << " ms" << endl;
    }

};


int main(int argc, char* argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string identifier, problem, it_method;
    int num_cells, o, number_snapshots, num_it;
    fp T, tau, L;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("num_cells,N",value<int>(&num_cells)->default_value(128),
         "number of cells used in the simulation")
        ("order,o",value<int>(&o)->default_value(2),
         "order of the space approximation")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("final_time,T",value<fp>(&T)->default_value(0.5),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("sin"),
         "initial value for the problem that is to be solved")
        ("iter,i",value<int>(&num_it)->default_value(5),
         "number of iterations for the Burgers' equation")
        ("iter_method",value<string>(&it_method)->default_value("secant"),
         "iterative method used (either fixedpoint, secant, or newton)")
        ("domain_length,L",value<fp>(&L)->default_value(5.0),
         "the length of the domain is [-L,L] (currently only supported for "
         "sech and lorentz)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        iterative_method im;
        if(it_method == "newton")           im = IM_NEWTON;
        else if(it_method == "secant")      im = IM_SECANT;
        else if(it_method == "fixedpoint")  im = IM_FIXEDPOINT;
        else { 
            cout << "ERROR: iter_method must be either fixedpoint, secant, or "
                 << "newton" << endl;
            exit(1); 
        }

        fp(*u0)(array<fp,1>);
        fp a, b;
        if(problem == "sin") {
            u0 = iv_periodic;
            a = 0.0; b = 2*M_PI;
        } else if(problem == "gauss") {
            u0 = iv_gauss;
            a = 0.0; b = 2*M_PI;
        } else if(problem == "sech") {
            u0 = iv_sech;
            a = -L; b = L;
        } else if(problem == "lorentz") {
            u0 = iv_lorentz;
            a = -L; b = L;
        } else if(problem == "osc") { 
            u0 = iv_osc;
            a = -1.0; b = 1.0; 
        } else {
            cout << "ERROR: Problem " << problem << " is not available "
                 << "(accepted values are sin, gauss, sech, and lorentz)" 
                 << endl;
            exit(1);
        }

        burgers bu(a, b, num_cells, o, T, tau, u0, number_snapshots, num_it, im);
        bu.run();
    }

    return 0;
}

