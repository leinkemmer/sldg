//TODO: merge sol and solpx

#include <boost/program_options.hpp>
#include <generic/netcdf.hpp>
#include <programs/solpx.hpp>

bool enable_output; //is read in domain.hpp


#ifdef __CUDACC__
__host__ __device__
#endif
fp init_to_0(fp* UNUSED(xv)){
    return 0.0;
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp init_impulse(fp* xv){
    fp x = xv[0];
    fp v = xv[1];

    constexpr fp L = 200.0;
    constexpr fp sigma = 0.1*L;
    constexpr fp inv_sigma2 = 1./sigma/sigma;

    return exp(-x*x*0.5*inv_sigma2)*exp(-v*v*0.5)/sqrt(2*M_PI);
}

troubled_cell_indicator parse_tci(string& s){
    troubled_cell_indicator tci;
    if(s=="minmod")
        tci = troubled_cell_indicator::minmod;
    else if(s=="mean_err")
        tci = troubled_cell_indicator::mean_err;
    else if(s=="none")
        tci = troubled_cell_indicator::none;
    else {
        cout << "ERROR: cell indicator \"" << s 
             << "\" not available." << endl;
        exit(1);
    }
    return tci;
}

troubled_cell_modifier parse_tcm(string& s){
    troubled_cell_modifier tcm;
    if(s=="linear_weno")
        tcm = troubled_cell_modifier::linear_weno;
    else if(s=="simple_weno")
        tcm = troubled_cell_modifier::simple_weno;
    else if(s=="hweno")
        tcm = troubled_cell_modifier::hweno;
    else if(s=="none")
        tcm = troubled_cell_modifier::none;
    else{
        cout << "ERROR: cell modifier \"" << s 
             << "\" not available." << endl;
        exit(1);
    }
    return tcm;
}



int main(int argc, char *argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string dof, problem, poisson_solver;
    string _tcm, _tci;
    int number_snapshots, bdry_cell_ratio;
    fp T, tau;
    bool gpu=false;
    bool adjust_domain;
    bool sldg_limiter;
    int num_gpus, num_ele_domains;
    fp collision_rate;

    //TODO:snapshots,
    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(0),
         "number of snapshots that are written to disk (including initial and "
         "final time), returns currently txt files.")
        ("dof",value<string>(&dof)->default_value("16 16 4 4"),
         "degrees of freedom in the format nx1 nv1 ox1 ov1")
        ("final_time,T",value<fp>(&T)->default_value(12000.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("test"),
         "problem: constant source up to T=3000")
        ("tci",value<string>(&_tci)->default_value("none"),
         "troubled cell indicator: none, minmod, mean_err")
        ("tcm",value<string>(&_tcm)->default_value("none"),
         "troubled cell modifier: none, simple_weno, linear_weno, "
         "hweno")
        ("sldg_limiter",value<bool>(&sldg_limiter)->default_value(false),
         "applys sldg limiter")
        ("adjust_domain_v",value<bool>(&adjust_domain)->default_value(false),
         "the velocity domain is reduced according to the loss of high velocity "
         "particles, i.e., vmax becomes smaller over time depending on f")
        ("bdry_cell_ratio_x",value<int>(&bdry_cell_ratio)->default_value(1),
         "amount of boundary cells that fit in an interior cell")
        ("collision_rate",value<fp>(&collision_rate)->default_value(0.0),
         "collision rate of the electrons, collision rate of the ions is "
         "adjusted according to the mass ratio.")
        ("num_ele_domains",value<int>(&num_ele_domains)->default_value(1),
         "number of partitions of the x domain of the electrons, "
         "currently only 1 and 3 allowed.")
        //TODO: collision
        #ifdef __CUDACC__
        ("gpu",value<bool>(&gpu)->default_value(false),
         "the simulation uses GPUs")
        ("num_gpus",value<int>(&num_gpus)->default_value(1),
         "number of GPUs the simulation uses")
        #endif
        ;

        #ifndef __CUDACC__
        gpu = false; // makes sure that gpu is always initialized
        #endif

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {

        index_dxdv_no<1, 1> e(parse<4>(dof));
        cout << "parsed e: " << e << endl;

        troubled_cell_indicator tci = parse_tci(_tci);
        troubled_cell_modifier tcm = parse_tcm(_tcm);
        
        if(problem=="ii"){
            //L=200 also used in init_impulse
            cout << "initial impulse, no source" << endl;

            fp L = 200;
            fp vmax = 9.0;

            fp ion_electron_mass_ratio = 400.0;

            source_f<1,1> source_function(L,0,source_type::no_source);
            cout << "L=" << L << ", vmax=" << vmax
                 << ", mass ratio=" << ion_electron_mass_ratio
                 << ", source used=" << source_function.is_used() << endl;

            scrape_off_layer_px<1,1,init_impulse> solpx({-L,-vmax}, {L,vmax},
                            e,T,tau, source_function,
                            ion_electron_mass_ratio, collision_rate,
                            bdry_cell_ratio,
                            number_snapshots, adjust_domain,
                            tci, tcm, sldg_limiter, num_ele_domains,
                            gpu, num_gpus);
            solpx.run();

        } else if(problem=="cs"){

            fp t0 = 2000;
            cout << "constant source up to time t0=" << t0 << "." << endl;
            
            fp L = 200;
            fp vmax = 9.0;

            fp ion_electron_mass_ratio = 400.0;

            source_f<1,1> source_function(L,t0,source_type::constant_source);

            cout << "L=" << L << ", vmax=" << vmax 
                 << ", mass ratio=" << ion_electron_mass_ratio
                 << ", source used=" << source_function.is_used() << endl;

            scrape_off_layer_px<1,1,init_to_0> solpx({-L,-vmax}, {L,vmax},
                            e,T,tau, source_function, 
                            ion_electron_mass_ratio, collision_rate,
                            bdry_cell_ratio,
                            number_snapshots, adjust_domain,
                            tci, tcm, sldg_limiter, num_ele_domains,
                            gpu, num_gpus);
            solpx.run();

        } else if(problem=="tds"){

            fp t0 = 1000;
            cout << "time dependent source, t0=" << t0 << endl;
            
            fp L = 200;
            fp vmax = 9.0;

            fp ion_electron_mass_ratio = 400.0;

            source_f<1,1> source_function(L,t0,source_type::time_dependent_source);

            cout << "L=" << L << ", vmax=" << vmax
                 << ", mass ratio=" << ion_electron_mass_ratio
                 << ", source used=" << source_function.is_used() << endl;

            scrape_off_layer_px<1,1,init_to_0> solpx({-L,-vmax}, {L,vmax},
                            e,T,tau, source_function,
                            ion_electron_mass_ratio, collision_rate,
                            bdry_cell_ratio,
                            number_snapshots, adjust_domain,
                            tci, tcm, sldg_limiter, num_ele_domains,
                            gpu, num_gpus);
            solpx.run();

        } else {
            cout << "ERROR: problem=" << problem << " not defined. " << endl;
            cout << "Possibilities: "
                 << "ii  (initial impulse no source), "
                 << "cs  (initially zero with constant source up to t0 = 2000, "
                 << "tds (initially zero with time dependent source (gaussianlike)\n";
        }
    }
    return 0;
}

