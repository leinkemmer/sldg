#include "translate_linear.hpp"

#define BOOST_TEST_MAIN SLDG2D_CPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

//Remark: for Legendre basis polynomials, the reference error is given 
//        by their paper (2017). For order 2 it is exactly the same, for
//        order 3 l2 is exactly the same and linfinity sometimes smaller or
//        large, I guess this depends on which points the function is evaluated
//        which has more impact than in l2 by computing the integral?
//        For Lagrange polynomials the error is provided by myself

bool gpu=false;

template<size_t o>
void shift(bool gpu, basis_type bt, double factor=0.5){

    auto f0 = [](double x, double y){return sin(x+y);};

    double final_time = M_PI;

    double ax = 0.0;
    double bx = 2.0*M_PI;
    double ay = 0.0;
    double by = 2.0*M_PI;

    shift_a a;
    shift_b b;

    double err_tol_l2_o2[4] = {7.01e-3,1.74e-3,4.32e-4,1.09e-4};
    double err_tol_li_o2[4] = {3.25e-2,8.41e-3,2.14e-3,5.39e-4}; 
    double err_tol_l2_o3[4] = {3.51e-4,4.38e-5,5.47e-6,6.83e-7};
    double err_tol_li_o3[4] = {2.69e-3,3.39e-4,4.24e-5,5.31e-6}; 

    double err_tol_l2_o2_lag[4] = {0.00077,9.5e-05,1.2e-05,1.5e-06};
    double err_tol_li_o2_lag[4] = {0.0012,0.00015,1.8e-05,2.3e-06};

    double errold_l2=-1;
    double errold_linf=-1;
    int iter=0;
    for(int N=20;N<161;N*=2){

        double dt = factor*(bx-ax)/double(N);

        auto err =  translate_l<o>(f0, ax, ay, bx, by, N, 
                                   dt, final_time, a, b,
                                   gpu, bt);

        BOOST_CHECK( abs(err[2])<4e-15 );
        cout << "N: " << N << ", err in mass: " << err[2] 
             << ", l2: " << err[0] << ", li: " << err[1] << endl;

        if(bt==basis_type::legendre){
            if(o==2){
                BOOST_CHECK(err[0]<err_tol_l2_o2[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o2[iter]);
            }
            if(o==3){
                BOOST_CHECK(err[0]<err_tol_l2_o3[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o3[iter]);
            }
        }
        if(bt==basis_type::lagrange){
            BOOST_CHECK(err[0]<err_tol_l2_o2_lag[iter]);
            BOOST_CHECK(err[1]<err_tol_li_o2_lag[iter]);
        }
        if(errold_l2>0){
            double order_l2 = -log(err[0]/errold_l2)/log(2);
            double order_li = -log(err[1]/errold_linf)/log(2);
            BOOST_CHECK( order_l2 > fp(o)-0.1 );
            BOOST_CHECK( order_li > fp(o)-0.1 );
        }
        errold_l2 = err[0];
        errold_linf = err[1];
        iter++;
    }

}


BOOST_AUTO_TEST_CASE( sldg2d_shift ){

    cout << "\nGPU: " << gpu << endl;
    cout << "LEGENDRE SHIFT, o=2&3" << endl;
    shift<2>(gpu, basis_type::legendre);
    shift<3>(gpu, basis_type::legendre);

    cout << "LAGRANGE SHIFT, o=2" << endl;
    shift<2>(gpu, basis_type::lagrange);
    cout << "\n";
}


template<size_t o>
void rotate(bool gpu, basis_type bt, double factor=0.5){

    auto f0 = [](double x, double y){return exp(-x*x-y*y);};

    double final_time = 2.0*M_PI;

    double ax =-2.0*M_PI;
    double bx = 2.0*M_PI;
    double ay =-2.0*M_PI;
    double by = 2.0*M_PI;

    rotate_a a;
    rotate_b b;

    double err_tol_l2_o2[4] = {1.82e-2*0.5,3.63e-3*0.5,7.73e-4*0.5,1.83e-4*0.5};
    double err_tol_li_o2[4] = {1.13e-1,3.30e-2,1.08e-2,3.33e-3}; 
    double err_tol_l2_o3[4] = {9.06e-4,1.07e-4,1.34e-5,1.67e-6};
    double err_tol_li_o3[4] = {2.16e-2,3.27e-3,4.64e-4,5.76e-5}; 

    double err_tol_l2_lag_o2[4] = {0.0066,0.001,0.0002,4.7e-05};
    double err_tol_li_lag_o2[4] = {0.08,0.024,0.006,0.0016};

    double errold_l2=-1;
    double errold_linf=-1;
    int iter=0;
    for(int N=20;N<161;N*=2){

        double dt = factor*(bx-ax)/double(N);

        auto err =  translate_l<o>(f0, ax, ay, bx, by, N, 
                                   dt, final_time, a, b,
                                   gpu, bt);

        //N=20 to coarse grid wich introduced to much diffusion 
        //and transports mass out of the domain.
        if(N>20)
            BOOST_CHECK( abs(err[2])<5e-13 );
        cout << "N: " << N << ", err in mass: " << err[2] 
             << ", l2: " << err[0] << ", li: " << err[1] << endl;

        if(bt==basis_type::legendre){
            if(o==2){
                BOOST_CHECK(err[0]<err_tol_l2_o2[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o2[iter]);
            }
            if(o==3){
                BOOST_CHECK(err[0]<err_tol_l2_o3[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o3[iter]);
            }
        } 
        if(bt==basis_type::lagrange){
                BOOST_CHECK(err[0]<err_tol_l2_lag_o2[iter]);
                BOOST_CHECK(err[1]<err_tol_li_lag_o2[iter]);
        }
        if(errold_l2>0){
            double order_l2 = -log(err[0]/errold_l2)/log(2);
            BOOST_CHECK( order_l2 > fp(o)-0.01 );
            //order in linfinity up to 0.4 below o
            //-log(err[1]/errold_linf)/log(2) << endl;
        }
        errold_l2 = err[0];
        errold_linf = err[1];
        iter++;
    }
   
}


BOOST_AUTO_TEST_CASE( sldg2d_rotate ){

    cout << "\nGPU: " << gpu << endl;
    cout << "LEGENDRE ROTATE, o=2&3" << endl;
    rotate<2>(gpu, basis_type::legendre);
    rotate<3>(gpu, basis_type::legendre);

    cout << "LAGRANGE ROTATE, o=2" << endl;
    rotate<2>(gpu, basis_type::lagrange);
    cout << "\n";
}



template<size_t o>
void swirl(bool gpu, basis_type bt, double factor=0.5){

    auto f0 = [](double x, double y){
        constexpr double rb0 = 0.3*M_PI;
        double rbx = sqrt((x-rb0)*(x-rb0) + y*y);
        return (rbx<rb0) ? rb0*pow(cos(rbx*M_PI/(2.0*rb0)),6) : 0.0;
    };

    double final_time = 1.5;

    double ax =-M_PI;
    double bx = M_PI;
    double ay =-M_PI;
    double by = M_PI;

    swirl_a a;
    swirl_b b;

    double err_tol_l2_o2[4] = {1.26e-2,2.93e-3,5.97e-4,1.31e-4}; 
    double err_tol_li_o2[4] = {2.04e-1,6.93e-2,1.49e-2,4.33e-3}; 
    double err_tol_l2_o3[4] = {3.23e-3,6.59e-4,1.43e-4,3.15e-5}; 
    double err_tol_li_o3[4] = {9.18e-2,3.76e-2,9.08e-3,2.67e-3}; 
    
    double err_tol_l2_lag_o2[4] = {0.0083,0.0016,0.0003,6.1e-05}; 
    double err_tol_li_lag_o2[4] = {0.16,0.045,0.0087,0.0016}; 

    double errold_l2=-1;
    double errold_linf=-1;
    int iter=0;
    for(int N=20;N<161;N*=2){

        double dt = factor*(bx-ax)/double(N);

        auto err =  translate_l<o>(f0, ax, ay, bx, by, N, 
                                   dt, final_time, a, b, 
                                   gpu, bt);

        BOOST_CHECK( err[2]<1e-14 );
        cout << "N: " << N << ", err in mass: " << err[2] 
             << ", l2: " << err[0] << ", li: " << err[1] << endl;

        if(bt==basis_type::legendre){
            if(o==2){
                BOOST_CHECK(err[0]<err_tol_l2_o2[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o2[iter]);
            }
            if(o==3){
                BOOST_CHECK(err[0]<err_tol_l2_o3[iter]);
                BOOST_CHECK(err[1]<err_tol_li_o3[iter]);
            }
        }
        if(bt==basis_type::lagrange){
                BOOST_CHECK(err[0]<err_tol_l2_lag_o2[iter]);
                BOOST_CHECK(err[1]<err_tol_li_lag_o2[iter]);
        }
        if(errold_l2>0){
            double order_l2 = -log(err[0]/errold_l2)/log(2);
            BOOST_CHECK(order_l2 > 2.0 );
            //order in linifinity from 1.2 to 2.5, not consistent
            //-log(err[1]/errold_linf)/log(2) << endl;
        }
        errold_l2 = err[0];
        errold_linf = err[1];
        iter++;
    }
   
}


BOOST_AUTO_TEST_CASE( sldg2d_swirl ){

    cout << "\nGPU: " << gpu << endl;
    cout << "LEGENDRE SWIRLING DEFORMATION, o=2&3" << endl;
    swirl<2>(gpu, basis_type::legendre);
    swirl<3>(gpu, basis_type::legendre);

    cout << "LAGRANGE SWIRLING DEFORMATION, o=2" << endl;
    swirl<2>(gpu, basis_type::lagrange);
    cout << "\n";
}



//template<size_t o>
//void perturbation(bool gpu, basis_type bt, double factor=0.5){
//
//    auto f0 = [](double x, double y){return 1.0-1e-7*2*sin(x)*sin(y);};
//
//    double final_time = 200;
//
//    double ax =0.0*M_PI;
//    double bx =2.0*M_PI;
//    double ay =0.0*M_PI;
//    double by =2.0*M_PI;
//
//    perturb_a a;
//    perturb_b b;
//
//    double errold_l2=-1;
//    double errold_linf=-1;
//    int iter=0;
//    for(int N=20;N<21;N*=2){
//
//        double dt = 1.0+factor-factor;
//
//        std::pair<double,double> err =  translate_l<o>(f0, ax, ay, bx, by, N, 
//                                                       dt, final_time, a, b,
//                                                       gpu, bt);
//
//        cout << "err l2: " << err.first << ", "
//             << "err linf: " << err.second << endl;
// 
//        if(errold_l2>0){
//            cout << "order: " << -log(err.first/errold_l2)/log(2)
//                 << ", " << -log(err.second/errold_linf)/log(2) << endl;
//        }
//        errold_l2 = err.first;
//        errold_linf = err.second;
//        iter++;
//    }
//   
//}
