#include <generic/common.hpp>
#include <programs/vlasovpoisson.hpp>

#define BOOST_TEST_MAIN VLASOVPOISSONPV
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

bool enable_output = true;

fp iv_weak_landau(array<fp,4> xv) {
    fp x=xv[0];
    fp v=xv[2]; fp w=xv[3];
    return 1/(2.0*M_PI)*exp(-v*v/2.0)*exp(-w*w/2.0)*(1+0.01*cos(0.5*x));
}

BOOST_AUTO_TEST_CASE( vlasovpoisson_ll ) {
    bool openmp=false;
    #ifdef _OPENMP
    openmp=true;
    #endif
    mpi_init(openmp);
    mpi_process<2> mpicomm;

    fp tau = 0.1;
    auto weak_landau = [](array<fp,2> xv) {
        fp x=xv[0]; fp v=xv[1];
        return fp(1/(sqrt(2*M_PI))*exp(-v*v/2.0)*(1+0.01*cos(0.5*x)));
    };
    if(mpicomm.total_nodes != 1) {
        // TODO: we would have to read evolution-0,0.data, evolution-0,1.data,
        // ...
        cout << "ERROR: This test currently only for 1 mpi process" << endl;
        exit(1);
    }
    {
        vlasovpoisson<2> vp(4.0*M_PI,6.0,16,16,4,4,10.0,tau,weak_landau,
                mpicomm,0);
        vp.run();
    } // dtor of vp is called here (before MPI_Finalize)

    if(mpicomm.rank == 0) {
        // read the input file and see if the results are good
        std::ifstream fs("evolution-0,0.data");
        fp mass0, mom0, ee0, ignore;
        string s; getline(fs, s); // ignore header
        getline(fs, s); std::istringstream iss(s);
        iss >> ignore >> mass0 >> mom0 >> ignore >>  ignore >> ee0;

        array<fp,3> ee;
        vector<fp> t_max, ee_max;
        for(Index i=0;;i++) {
            fp t, mass, mom;
            getline(fs, s); std::istringstream iss(s);
            if(fs.eof())
                break;
            iss >> t >> mass >> mom >> ignore >>  ignore >> ee[i%3];

            #ifdef USE_SINGLE_PRECISION
            BOOST_CHECK_SMALL( abs(mass-mass0)/mass0, fp(8e-6) );
            BOOST_CHECK_SMALL( abs(mom-mom0), fp(2e-6) );
            #else
            BOOST_CHECK_SMALL( abs(mass-mass0)/mass0, fp(1e-11) );
            BOOST_CHECK_SMALL( abs(mom-mom0), fp(5e-11) );
            #endif

            if(i>3) {
                int j = i%3;
                if(ee[j] < ee[neg_modulo(j-1,3)] &&
                   ee[neg_modulo(j-2,3)] < ee[neg_modulo(j-1,3)]) {
                    t_max.push_back(t-tau);
                    ee_max.push_back(ee[neg_modulo(j-1,3)]);
                }
            }

        }

        for(size_t k=2;k<t_max.size();k++) {
            fp gamma = -0.5*(log(ee_max[k])-log(ee_max[0]))/(t_max[k]-t_max[0]);
            BOOST_CHECK_SMALL( abs(gamma-0.153), 6e-3);
        }
    }

    mpi_destroy();
}

