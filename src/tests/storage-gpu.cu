#include <generic/common.hpp>

#define BOOST_TEST_MAIN STORAGE_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

bool enable_output; //is read in domain.hpp

Index factorial(Index k) {
    Index prod = 1;
    for(Index i=1;i<=k;i++)
        prod *= i;
    return prod;
}


template<size_t d>
void check_multi_array_assign_swap() {

    cout << "\n\nd=" << d << endl;

    array<Index, d> n;
    array<Index,d> ntmp; //dummy which is resized inside variables
    for(size_t i=0;i<d;i++){
        n[i] = 1+i;
        ntmp[i] = i;
    }


    multi_array<fp,d> h_ma1(n);

    for(auto i : range<d>(n)) {
        h_ma1(i) = 2.0;
    }

    // check assignment
    cout << "check assignment 3 times" << endl;
    multi_array<fp,d> d_ma1(ntmp,true);
    d_ma1 = h_ma1;
    multi_array<fp,d> d_ma2(ntmp,true);
    d_ma2 = d_ma1;
    multi_array<fp,d> h_ma2;
    h_ma2 = d_ma2;
    BOOST_CHECK( error_inf(h_ma2.shape(), n) == 0 );
    for(auto i : range<d>(n))
        BOOST_CHECK_SMALL( abs(h_ma2(i)-2.0), 1e-15 );


    // check copy constructor
    cout << "check copy:" << endl;
    multi_array<fp,d> d_ma3 = d_ma2;
    multi_array<fp,d> h_ma3(ntmp);
    cout << "check assignment:" << endl;
    h_ma3 = d_ma3;
    BOOST_CHECK( error_inf(h_ma3.shape(), n) == 0 );
    for(auto i : range<d>(n))
        BOOST_CHECK_SMALL( abs(h_ma3(i)-2.0), 1e-15 );
}

//run this with 'compute-sanitizer --tool memcheck'
BOOST_AUTO_TEST_CASE( multi_array_assign_swap ) {
    check_multi_array_assign_swap<2>();
    check_multi_array_assign_swap<4>();
    check_multi_array_assign_swap<6>();
    check_multi_array_assign_swap<8>();
    //check_multi_array_assign_swap<10>();
    //check_multi_array_assign_swap<12>();
}
