#include <generic/common.hpp>

#define BOOST_TEST_MAIN BGK_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <algorithms/dg-gpu.cu>

#ifdef USE_SINGLE_PRECISION
float machine_prec = 1e-6;
#else
double machine_prec = 1e-15;
#endif

bool enable_output; //is read in domain.hpp


//Test just compares GPU with CPU result!!!
template<size_t dv>
fp bgk(int Nx, int Nv, int o){

    constexpr size_t dx = 1;
    constexpr size_t d = dv+dx;

    gpu_array<fp> a(d);
    gpu_array<fp> b(d);
    gpu_array<fp> h(d);
    a[0] = -1.0;
    b[0] = 1.0;
    h[0] = (b[0]-a[0])/fp(Nx);

    array<Index,2*d> e;
    e[0] = Nx;
    e[d] = o;
    for(size_t i=0;i<dv;++i){
        e[dx+i] = Nv;
        e[d+dx+i] = o;
        a[dx+i] = -9.0;
        b[dx+i] =  9.0;
        h[dx+i] = (b[dx+i]-a[dx+i])/fp(e[dx+i]);
    }
    array<Index,2*dx> e_x{Nx,o};

    cout << "e: " << e << endl;
    gpu_array<Index> d_e(2*d);
    d_e.set(e.begin(),e.end());
    d_e.copy_to_gpu();

    multi_array<fp,2*dx> rho(e_x);
    multi_array<fp,2*dx> d_rho(e_x,true);
    multi_array<fp,2*dx> mom1(e_x);
    multi_array<fp,2*dx> d_mom1(e_x,true);
    multi_array<fp,2*dx> mom2(e_x);
    multi_array<fp,2*dx> d_mom2(e_x,true);

    //assuming f = exp(-4*x^2)*exp(-v^2)/sqrt(M_PI)
    array<Index,2*dx> idx_x={0};
    do{
        fp x = a[0] + h[0]*(idx_x[0] + gauss::get_x_scaled01(idx_x[1],e_x[1]));

        rho(idx_x) = exp(-2.0*x*x);
        mom1(idx_x) = exp(-4.0*x*x);
        mom2(idx_x) = 1.0+x*x*exp(-2.0*x*x);

    }while(iter_next_memorder(idx_x,e_x));

    d_rho = rho;
    d_mom1 = mom1;
    d_mom2 = mom2;

    multi_array<fp,2*d> fin(e);
    multi_array<fp,2*d> d_fin(e,true);
    multi_array<fp,2*d> fmid(e);
    multi_array<fp,2*d> d_fmid(e,true);
    multi_array<fp,2*d> out_from_gpu(e);
    multi_array<fp,2*d> d_out(e,true);

    array<Index,2*d> idx={0};
    do{

        fp fi = 1.0;
        fp fm = 1.0;
        for(size_t i=0;i<d;++i){
            fp x = a[i] + h[i]*(idx[i] + gauss::get_x_scaled01(idx[d+i],e[d+i]));
            fi *= exp(-x*x);
            fm *= exp(-2.0*x*x);
        }

        fin(idx) = fi;
        fmid(idx) = fm;
    }while(iter_next_memorder(idx,e));

    d_fin = fin;
    d_fmid = fmid;

    int v23 = 1;
    for(size_t i=1;i<dv;++i)
        v23 *= e[dx+i]*e[d+dx+i];
    dim3 blocks(e[d],e[dx]*e[d+dx],v23);
    fp dt = 1.5;
    fp nu = 3.0;
    BGK_gpu<dv><<<blocks,e[0]>>>(d_out.data(), d_fin.data(), d_fmid.data(), dt, nu,
                             d_rho.data(), d_mom1.data(), d_mom2.data(), 
                             a.d_ptr(), b.d_ptr(), d_e.d_data);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    out_from_gpu = d_out;

    multi_array<fp,2*d> fout(e);

    array<fp,d> ha; copy(a.h_data.begin(), a.h_data.end(), ha.begin());
    array<fp,d> hh; copy(h.h_data.begin(), h.h_data.end(), hh.begin());
    BGK_cpu<dx,dv>(fout,fin,fmid,dt,nu,rho,mom1,mom2,ha,hh,e);

    //for(Index i=0;i<fout.num_elements();++i){
    //    cout << "i: " << i << ", " << out_from_gpu.v[i] 
    //         << " " << fout.v[i] << endl;
    //}

    return inf_norm<d>(fout,out_from_gpu);
    
}


BOOST_AUTO_TEST_CASE( test_bgk  ) {
    

    BOOST_CHECK_SMALL( bgk<1>(50, 50, 4), 530*machine_prec );
    //BOOST_CHECK_SMALL( bgk<1>(101, 132, 3), machine_prec );

    //BOOST_CHECK_SMALL( bgk<3>(12, 13, 4), machine_prec );
    //BOOST_CHECK_SMALL( bgk<3>(21, 23, 4), machine_prec );

}

