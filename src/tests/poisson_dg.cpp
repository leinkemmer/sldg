#include <generic/common.hpp>
#include <generic/utility.hpp>

#define BOOST_TEST_MAIN POISSON_SOLVER
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <programs/poisson_solvers.hpp>
#include <generic/storage.hpp>

bool enable_output = true; //is read in domain.hpp

//TODO: mpiinit and mpidestroy problem when only one test is called.

BOOST_AUTO_TEST_CASE( poisson_dg_1d ) {
    std::cout << "\n\n1d\n";
    constexpr size_t dx = 1;

    array<fp,4> B = {1.0, 2*M_PI, 4*M_PI, 20*M_PI/3.0};
    mpi_init(1);
    mpi_process<2> mpicomm;
    array<Index,2> num_nodes = {1,1};
    mpicomm.init(num_nodes,dx,dx);
    for(auto bb : B) {
        array<fp,dx> a = {0.0};
        array<fp,dx> b = {bb};

        array<fp,dx> aaa = a;
        array<fp,dx> bbb = b;
        for (size_t i=0;i<dx;i++) {
            a[i] = aaa[i] + mpicomm.node_id[i]*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
            b[i] = aaa[i] + (mpicomm.node_id[i]+1)*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
        }
     
        Index N = 10;
        Index o = 4;
        array<Index,dx+dx> e = {N/mpicomm.num_nodes[0],o};

        multi_array<fp,dx+dx> rho(e), ref(e), start(e);
        array<Index,dx+dx> idx; fill(idx.begin(), idx.end(), 0);
        fp xx;
        fp hx = (b[0]-a[0])/fp(e[0]);
        do {
            xx =  a[0] + idx[0]*hx + hx*gauss::x_scaled01(idx[dx],e[dx]);
            rho(idx) = -cos(2*M_PI/bb*xx);
            ref(idx) = (cos(2*M_PI/bb*xx))/(2*M_PI/bb*2*M_PI/bb);
            start(idx) = 0.0;
        } while( iter_next_memorder(idx,e) );
        fp sigma = 10.0;
        fp tol = 1e-5;
        dg_poisson_solver<dx,dx> dgps(a, b, e, mpicomm, sigma, tol, 70, start, 1);
        dgps.solve_poisson_dg(rho);
        multi_array<fp,dx+dx> sol = dgps.PHI;
        fp err_dg = inf_norm<dx>(sol, ref);
        fp max_ref = fabs(max_element_abs(ref));

        if (mpicomm.rank == 0) {
            std::cout << "ref_err_absolute: " << err_dg << "\n";
            std::cout << "ref_err_relative: " << err_dg/max_ref << "\n";
        }

        BOOST_CHECK_SMALL( err_dg/max_ref, fp(1e-4) );
    }
    //mpi_destroy();
}


BOOST_AUTO_TEST_CASE( poisson_dg_2d ) {
    std::cout << "\n\n2d\n";
    constexpr size_t dx = 2;

    array<fp,4> B = {1.0, 2*M_PI, 4*M_PI, 20*M_PI/3.0};
    //mpi_init(1);
    mpi_process<4> mpicomm;
    mpicomm.init({1,1,1,1},dx,dx);
    for(auto bb : B) {
        array<fp,dx> a = {0.0, 0.0};
        array<fp,dx> b = {bb,  bb };

        array<fp,dx> aaa = a;
        array<fp,dx> bbb = b;
        for (size_t i=0;i<dx;i++) {
            a[i] = aaa[i] + mpicomm.node_id[i]*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
            b[i] = aaa[i] + (mpicomm.node_id[i]+1)*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
        }

        Index N = 18;
        Index o = 4;
        array<Index,dx+dx> e = {N/mpicomm.num_nodes[0],N/mpicomm.num_nodes[1],o,o};

        multi_array<fp,4> rho(e), ref(e), start(e);
        array<Index,dx+dx> idx; fill(idx.begin(), idx.end(), 0);
        fp xx,yy;
        fp hx = (b[0]-a[0])/fp(e[0]);
        fp hy = (b[1]-a[1])/fp(e[1]);
        do {
            xx =  a[0] + idx[0]*hx + hx*gauss::x_scaled01(idx[dx  ],e[dx  ]);
            yy =  a[1] + idx[1]*hy + hy*gauss::x_scaled01(idx[dx+1],e[dx+1]);
            rho(idx) = cos(2*M_PI/bb*xx) + cos(2*M_PI/bb*yy);
            ref(idx) = -(cos(2*M_PI/bb*xx) + cos(2*M_PI/bb*yy))/(2*M_PI/bb*2*M_PI/bb);
            start(idx) = 0.0;
        } while( iter_next_memorder(idx,e) );

        fp sigma = 10.0;
        fp tol = 1e-5;
        dg_poisson_solver<dx,dx> dgps(a, b, e, mpicomm, sigma, tol, 100, start, 1);
        dgps.solve_poisson_dg(rho);
        multi_array<fp,4> sol = dgps.PHI;
        fp err_dg = inf_norm<dx>(sol, ref);
        fp max_ref = fabs(max_element_abs(ref));

        if(mpicomm.rank == 0){
            std::cout << "ref_err_absolute: " << err_dg << "\n";
            std::cout << "ref_err_relative: " << err_dg/max_ref << "\n";
        }

        BOOST_CHECK_SMALL( err_dg/max_ref, fp(1e-4) );
    }
    //mpi_destroy();
}

BOOST_AUTO_TEST_CASE( poisson_dg_3d ) {
    std::cout << "\n\n3d\n";
    constexpr size_t dx = 3;

    array<fp,4> B = {1.0, 2*M_PI, 4*M_PI, 20*M_PI/3.0};
    //mpi_init(1);
    mpi_process<6> mpicomm;
    mpicomm.init({1,1,1,1,1,1},dx,dx);
    for(auto bb : B) {
        array<fp,dx> a = {0.0, 0.0, 0.0};
        array<fp,dx> b = {bb, bb, bb};
        array<fp,dx> aaa = a;
        array<fp,dx> bbb = b;
        for (size_t i=0;i<dx;i++) {
            a[i] = aaa[i] + mpicomm.node_id[i]*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
            b[i] = aaa[i] + (mpicomm.node_id[i]+1)*(bbb[i]-aaa[i])/
                            fp(mpicomm.num_nodes[i]);
        }
        Index N = 12;
        Index o = 4;
        array<Index,dx+dx> e = {N/mpicomm.num_nodes[0],
                                N/mpicomm.num_nodes[1],
                                N/mpicomm.num_nodes[2],o,o,o};

        multi_array<fp,dx+dx> rho(e), ref(e), start(e);
        array<Index,dx+dx> idx; fill(idx.begin(), idx.end(), 0);
        fp xx,yy,zz;
        fp hx = (b[0]-a[0])/fp(e[0]);
        fp hy = (b[1]-a[1])/fp(e[1]);
        fp hz = (b[2]-a[2])/fp(e[2]);
        do {
            xx =  a[0] + idx[0]*hx + hx*gauss::x_scaled01(idx[dx  ],e[dx  ]);
            yy =  a[1] + idx[1]*hy + hy*gauss::x_scaled01(idx[dx+1],e[dx+1]);
            zz =  a[2] + idx[2]*hz + hz*gauss::x_scaled01(idx[dx+2],e[dx+2]);
            rho(idx) = -0.03*(cos(2*M_PI/bb*xx) + cos(2*M_PI/bb*yy) + 
                              cos(2*M_PI/bb*zz));
            ref(idx) = (cos(2*M_PI/bb*xx) + cos(2*M_PI/bb*yy) + 
                        cos(2*M_PI/bb*zz))/(2*M_PI/bb*2*M_PI/bb)*0.03;
            start(idx) = 0.0;
        } while( iter_next_memorder(idx,e) );

        fp sigma = 10.0;
        fp tol = 1e-5;
        dg_poisson_solver<dx,dx> dgps(a, b, e, mpicomm, sigma, tol, 100, start, 1);
        dgps.solve_poisson_dg(rho);
        multi_array<fp,dx+dx> sol = dgps.PHI;
        fp max_ref = fabs(max_element_abs(ref));
        fp err_dg = inf_norm<dx>(sol, ref);

        if(mpicomm.rank == 0){
            std::cout << "ref_err_absolute: " << err_dg << "\n";
            std::cout << "ref_err_relative: " << err_dg/max_ref << "\n";
        }
        
        BOOST_CHECK_SMALL( err_dg/max_ref, fp(5e-4) );
    }
    mpi_destroy();
}


struct errors{

    fp err_f_l2;
    fp err_f_max;
    fp err_df_l2;
    fp err_df_max;

    errors(fp v)
        :    err_f_l2{v},
             err_f_max{v},
             err_df_l2{v},
             err_df_max{v} {}

};

std::ostream& operator<<(std::ostream& os, errors& err){

    os << "L2   error f:  " << err.err_f_l2 << "\n"
       << "Lmax error f:  " << err.err_f_max << "\n" 
       << "L2   error df: " << err.err_df_l2 << "\n" 
       << "Lmax error df: " << err.err_df_max;

    return os;
}

template<size_t o> 
errors solve_poisson1d(int N, string solver){

    constexpr size_t dx=1;

    fp a = -1.0;
    fp b =  1.0;
    fp h = (b-a)/fp(N);
    array<Index,2> e = {N,o};

    timer tconst;

    tconst.start();
    unique_ptr< poisson_solver<dx> > psdp;
    if(solver=="LDG"){
        psdp = make_poisson_1d_LDG<dx>(N,o,a,b);
        cout << "LDG solver" << endl;
    } else if(solver=="SIPG"){
        psdp = make_poisson_1d_SIPG<dx>(N,o,o,a,b);
        cout << "SIPG solver" << endl;
    } else if(solver=="FD"){
        psdp = make_poisson_1d_FD<dx>(N,o,a,b);
        cout << "FD solver" << endl;
    }else{
        cout << "ERROR: solver " << solver << " not available!\n";
        exit(1);
    }
    tconst.stop();

    cout << "time to construct/factorize: " << tconst.total() << endl;

    multi_array<fp,2> rho;
    rho.resize(e);
    array<multi_array<fp,2>,dx> E;
    E[0].resize(e);

    array<Index,2> idx = {0};
    //std::ofstream outrhs("outrhs"+std::to_string(N)+".data");
    do{
        fp x = a+h*(idx[0]+gauss::x_scaled01(idx[1],o));
        rho(idx) = -(4.0*x*exp(x) + x*x*exp(x) + exp(x));
        //outrhs << x << " " << rho(idx) << endl;
    } while(iter_next_memorder(idx,e));

    ////does fail for LDG since there the initial rhs changes 
    ////at each iteration currently
    //
    //int n_try = 50;
    //timer tsolve;
    //for(int i=0;i<n_try;++i){

    //    tsolve.start();
    //    psdp->solve(E,rho);
    //    tsolve.stop();

    //}
    //cout << "average time to solve, N=" << N << ": " 
    //     << tsolve.total()/fp(n_try) << endl;

    errors err{0.0};
    psdp->solve(E,rho);
    std::ofstream outf("outf"+std::to_string(N)+".data");
    std::ofstream outdf("outdf"+std::to_string(N)+".data");
    for(int i=0;i<N;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h*(i+gauss::x_scaled01(j,o));
    
            outf  << x << " " << rho.v[i*o + j] << endl;
            outdf << x << " " << E[0].v[j+i*o] << endl;

            //attention: E = -dx phi
            fp err_df = E[0].v[j+i*o] + 2.0*x*exp(x) + (x*x-1.0)*exp(x);
            fp err_f  = rho.v[i*o + j] - (x*x-1.0)*exp(x);

            err.err_df_max = max(err.err_df_max,abs(err_df));
            err.err_f_max = max(err.err_f_max,abs(err_f));

            err.err_df_l2 += err_df*err_df*gauss::w(j,o)*h*0.5;
            err.err_f_l2 += err_f*err_f*gauss::w(j,o)*h*0.5;
        }
    }
    err.err_f_l2 = sqrt(err.err_f_l2);
    err.err_df_l2 = sqrt(err.err_df_l2);
 
    return err;

}


//for the following high order schemes, the error for
//single precision computations is always close to machine
//precision and thus the order can not bee seen

#ifndef USE_SINGLE_PRECISION

BOOST_AUTO_TEST_CASE(poisson_dg_1d_direct_LDG){

    constexpr size_t o = 4;

    errors old_err{-1.0};
    for(int n=16;n<550;n*=2){

        cout << "\n\nN: " << n << endl;

        errors err = solve_poisson1d<o>(n,"LDG");

        cout << "errors:\n" << err << endl;

        if(old_err.err_f_l2>0.0){
            fp order_f_l2 = -log(err.err_f_l2/old_err.err_f_l2)/log(2.0);
            fp order_f_max = -log(err.err_f_max/old_err.err_f_max)/log(2.0);
            fp order_df_l2 = -log(err.err_df_l2/old_err.err_df_l2)/log(2.0);
            fp order_df_max = -log(err.err_df_max/old_err.err_df_max)/log(2.0);

            cout << "order f l2:  " << order_f_l2 << endl;
            cout << "order f max: " << order_f_max << endl;
            cout << "order df l2:  " << order_df_l2 << endl;
            cout << "order df max: " << order_df_max << endl;
            if( n<300){
                BOOST_CHECK_GT( order_f_l2, 4.0);
                BOOST_CHECK_GT( order_f_max, 4.0-0.1);
                BOOST_CHECK_GT( order_df_l2, 4.0-0.2);
                BOOST_CHECK_GT( order_df_max, 4.0-0.25);
            }
        }
        old_err.err_f_l2 = err.err_f_l2;
        old_err.err_f_max = err.err_f_max;
        old_err.err_df_l2 = err.err_df_l2;
        old_err.err_df_max = err.err_df_max;
    }

    BOOST_CHECK_LT( old_err.err_f_l2, 1.7e-12);
    BOOST_CHECK_LT( old_err.err_f_max, 8.2e-12);
    BOOST_CHECK_LT( old_err.err_df_l2, 5.8e-12);
    BOOST_CHECK_LT( old_err.err_df_max, 2.7e-11);

}


BOOST_AUTO_TEST_CASE(poisson_dg_1d_direct_SIPG){

    constexpr size_t o = 4;

    errors old_err{-1.0};
    for(int n=16;n<300;n*=2){

        cout << "\n\nN: " << n << endl;

        errors err = solve_poisson1d<o>(n,"SIPG");

        cout << "errors:\n" << err << endl;

        if(old_err.err_f_l2>0.0){
            fp order_f_l2 = -log(err.err_f_l2/old_err.err_f_l2)/log(2.0);
            fp order_f_max = -log(err.err_f_max/old_err.err_f_max)/log(2.0);
            fp order_df_l2 = -log(err.err_df_l2/old_err.err_df_l2)/log(2.0);
            fp order_df_max = -log(err.err_df_max/old_err.err_df_max)/log(2.0);

            cout << "order f l2:  " << order_f_l2 << endl;
            cout << "order f max: " << order_f_max << endl;
            cout << "order df l2:  " << order_df_l2 << endl;
            cout << "order df max: " << order_df_max << endl;

            if(old_err.err_f_l2 > 5e-10){
                BOOST_CHECK_GT( order_f_l2, 5.0);
                BOOST_CHECK_GT( order_f_max, 5.0-0.1);
                BOOST_CHECK_GT( order_df_l2, 5.0-0.1);
                BOOST_CHECK_GT( order_df_max, 5.0-0.1);
            }
        }
        old_err.err_f_l2 = err.err_f_l2;
        old_err.err_f_max = err.err_f_max;
        old_err.err_df_l2 = err.err_df_l2;
        old_err.err_df_max = err.err_df_max;
    }

    BOOST_CHECK_LT( old_err.err_f_l2, 5.6e-11);
    BOOST_CHECK_LT( old_err.err_f_max, 5.7e-11);
    BOOST_CHECK_LT( old_err.err_df_l2, 9.0e-11);
    BOOST_CHECK_LT( old_err.err_df_max, 1.2e-10);

}


BOOST_AUTO_TEST_CASE(poisson_dg_1d_direct_FD){

    constexpr size_t o = 4;

    errors old_err{-1.0};
    for(int n=16;n<300;n*=2){

        cout << "\n\nN: " << n << endl;

        errors err = solve_poisson1d<o>(n,"FD");

        cout << "errors:\n" << err << endl;

        if(old_err.err_f_l2>0.0){
            fp order_f_l2 = -log(err.err_f_l2/old_err.err_f_l2)/log(2.0);
            fp order_f_max = -log(err.err_f_max/old_err.err_f_max)/log(2.0);
            fp order_df_l2 = -log(err.err_df_l2/old_err.err_df_l2)/log(2.0);
            fp order_df_max = -log(err.err_df_max/old_err.err_df_max)/log(2.0);

            cout << "order f l2:  " << order_f_l2 << endl;
            cout << "order f max: " << order_f_max << endl;
            cout << "order df l2:  " << order_df_l2 << endl;
            cout << "order df max: " << order_df_max << endl;

            BOOST_CHECK_GT( order_f_l2   , 2.0-0.1);
            BOOST_CHECK_GT( order_f_max  , 2.0-0.1);
            BOOST_CHECK_GT( order_df_l2  , 2.0-0.1);
            BOOST_CHECK_GT( order_df_max , 2.0-0.1);
        }
        old_err.err_f_l2 = err.err_f_l2;
        old_err.err_f_max = err.err_f_max;
        old_err.err_df_l2 = err.err_df_l2;
        old_err.err_df_max = err.err_df_max;
    }

    BOOST_CHECK_LT( old_err.err_f_l2 , 5e-6);
    BOOST_CHECK_LT( old_err.err_f_max , 7e-6);
    BOOST_CHECK_LT( old_err.err_df_l2 , 2e-5);
    BOOST_CHECK_LT( old_err.err_df_max , 3e-5);

}


template<size_t o, size_t o_derivative> 
errors solve_poisson1d_SIPG(int N){

    constexpr size_t dx=1;

    fp a = -1.0;
    fp b =  1.0;
    fp h = (b-a)/fp(N);
    array<Index,2> e = {N,o};
    array<Index,2> e_d = {N,o_derivative};

    timer tconst;

    tconst.start();
    unique_ptr< poisson_solver<dx> > psdp;
    psdp = make_poisson_1d_SIPG<dx>(N,o,o_derivative,a,b);
    tconst.stop();
    cout << "SIPG solver" << endl;

    cout << "time to construct/factorize: " << tconst.total() << endl;

    multi_array<fp,2> rho;
    rho.resize(e);
    array<multi_array<fp,2>,dx> E;
    E[0].resize(e_d);

    array<Index,2> idx = {0};
    //std::ofstream outrhs("outrhs"+std::to_string(N)+".data");
    do{
        fp x = a+h*(idx[0]+gauss::x_scaled01(idx[1],o));
        rho(idx) = -(4.0*x*exp(x) + x*x*exp(x) + exp(x));
        //outrhs << x << " " << rho(idx) << endl;
    } while(iter_next_memorder(idx,e));


    errors err{0.0};

    cout << "before solving: " << endl;
    psdp->solve(E,rho);
    cout << "after solving: " << endl;

    std::ofstream outf("outf"+std::to_string(N)+".data");
    std::ofstream outdf("outdf"+std::to_string(N)+".data");
    for(int i=0;i<N;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h*(i+gauss::x_scaled01(j,o));
    
            outf  << x << " " << rho.v[i*o + j] << endl;

            //attention: E = -dx phi
            fp err_f  = rho.v[i*o + j] - (x*x-1.0)*exp(x);

            err.err_f_max = max(err.err_f_max,abs(err_f));

            err.err_f_l2 += err_f*err_f*gauss::w(j,o)*h*0.5;
        }
    }
    err.err_f_l2 = sqrt(err.err_f_l2);

    for(int i=0;i<N;++i){
        for(size_t j=0;j<o_derivative;++j){
            fp x = a+h*(i+gauss::x_scaled01(j,o_derivative));
    
            outdf << x << " " << E[0].v[j+i*o_derivative] << endl;

            //attention: E = -dx phi
            fp err_df = E[0].v[j+i*o_derivative] + 2.0*x*exp(x) + (x*x-1.0)*exp(x);

            err.err_df_max = max(err.err_df_max,abs(err_df));

            err.err_df_l2 += err_df*err_df*gauss::w(j,o_derivative)*h*0.5;
        }
    }
    err.err_df_l2 = sqrt(err.err_df_l2);
 
    return err;

}


BOOST_AUTO_TEST_CASE(poisson_dg_1d_direct_SIPG_o_derivative){

    constexpr size_t o = 4;

    errors old_err{-1.0};
    //larger n does not reduce the error 
    //(A not that good conditioned I guess)
    //lowest error approximately 10^-10
    for(int n=16;n<300;n*=2){

        cout << "\n\nN: " << n << endl;

        errors err = solve_poisson1d_SIPG<o,o-1>(n);

        cout << "errors:\n" << err << endl;

        if(old_err.err_f_l2>0.0){
            fp order_f_l2 = -log(err.err_f_l2/old_err.err_f_l2)/log(2.0);
            fp order_f_max = -log(err.err_f_max/old_err.err_f_max)/log(2.0);
            fp order_df_l2 = -log(err.err_df_l2/old_err.err_df_l2)/log(2.0);
            fp order_df_max = -log(err.err_df_max/old_err.err_df_max)/log(2.0);

            cout << "order f l2:  " << order_f_l2 << endl;
            cout << "order f max: " << order_f_max << endl;
            cout << "order df l2:  " << order_df_l2 << endl;
            cout << "order df max: " << order_df_max << endl;

            BOOST_CHECK_GT( order_f_l2 , 4.0);
            BOOST_CHECK_GT( order_f_max , 4.0-0.1);
            if(n<200){
                //interesting that on these points the order is 1 higher
                BOOST_CHECK_GT( order_df_l2 , 4.0-0.01);
                BOOST_CHECK_GT( order_df_max , 4.0-0.1);
            }
        }
        old_err.err_f_l2 = err.err_f_l2;
        old_err.err_f_max = err.err_f_max;
        old_err.err_df_l2 = err.err_df_l2;
        old_err.err_df_max = err.err_df_max;
    }

    BOOST_CHECK( old_err.err_f_l2 < 1.1e-10);
    BOOST_CHECK( old_err.err_f_max < 1.2e-9);
    BOOST_CHECK( old_err.err_df_l2 < 1.2e-10);
    BOOST_CHECK( old_err.err_df_max < 2.5e-10);

}


template<size_t o> 
errors solve_poisson1d_SIPG_with_smaller_bdry_cells(int N_tot, int N_bdry, int ratio){

    constexpr size_t dx=1;

    fp a = -1.0;
    fp b =  1.0;
    int N_interior = N_tot-2*N_bdry;
    fp h_bdry = (b-a)/fp(ratio*N_interior + 2*N_bdry);
    fp h_interior = ratio*h_bdry;
    cout << "h_bdry=" << h_bdry << ", h_interior=" << h_interior
         << " -> ratio=" << ratio << endl;

    array<Index,2> e = {N_tot,o};
    array<Index,2> e_d = {N_tot,o};

    timer tconst;

    tconst.start();
    unique_ptr< poisson_solver<dx> > psdp = 
           make_poisson_1d_SIPG<dx>(N_tot,o,o,a,b,N_bdry,ratio);
    tconst.stop();

    cout << "time to construct/factorize: " << tconst.total() << endl;

    multi_array<fp,2> rho;
    rho.resize(e);
    array<multi_array<fp,2>,dx> E;
    E[0].resize(e_d);


    std::ofstream outrhs("outrhs"+std::to_string(N_tot)+".data");
    int k=0;
    for(int i=0;i<N_bdry;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_bdry*(i+gauss::x_scaled01(j,o));
            rho.v[j+i*o] =  -(4.0*x*exp(x) + x*x*exp(x) + exp(x));
            outrhs << x << " " << rho.v[k++] << endl;
        }
    }
    for(int i=N_bdry;i<N_tot-N_bdry;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_bdry*N_bdry+h_interior*(i-N_bdry+gauss::x_scaled01(j,o));
            rho.v[j+i*o] = -(4.0*x*exp(x) + x*x*exp(x) + exp(x));
            outrhs << x << " " << rho.v[k++] << endl;
        }
    }
    for(int i=N_tot-N_bdry;i<N_tot;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_interior*N_interior+h_bdry*N_bdry+
                   h_bdry*(i-N_tot+N_bdry+gauss::x_scaled01(j,o));
            rho.v[j+i*o] = -(4.0*x*exp(x) + x*x*exp(x) + exp(x));
            outrhs << x << " " << rho.v[k++] << endl;
        }
    }

    errors err{0.0};

    psdp->solve(E,rho);

    //std::ofstream outf("outf"+std::to_string(N_tot)+".data");
    //std::ofstream outdf("outdf"+std::to_string(N_tot)+".data");
    for(int i=0;i<N_bdry;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_bdry*(i+gauss::x_scaled01(j,o));

            fp err_f  = rho.v[i*o + j] - (x*x-1.0)*exp(x);
            err.err_f_max = max(err.err_f_max,abs(err_f));
            err.err_f_l2 += err_f*err_f*gauss::w(j,o)*h_bdry*0.5;

            //attention: E = -dx phi
            fp err_df = E[0].v[j+i*o] + 2.0*x*exp(x) + (x*x-1.0)*exp(x);
            err.err_df_max = max(err.err_df_max,abs(err_df));
            err.err_df_l2 += err_df*err_df*gauss::w(j,o)*h_bdry*0.5;

            //outf  << x << " " << rho.v[i*o + j] << endl;
            //outdf << x << " " << E[0].v[j+i*o_derivative] << endl;
        }
    }
    for(int i=N_bdry;i<N_tot-N_bdry;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_bdry*N_bdry+h_interior*(i-N_bdry+gauss::x_scaled01(j,o));

            fp err_f  = rho.v[i*o + j] - (x*x-1.0)*exp(x);
            err.err_f_max = max(err.err_f_max,abs(err_f));
            err.err_f_l2 += err_f*err_f*gauss::w(j,o)*h_bdry*0.5;

            //attention: E = -dx phi
            fp err_df = E[0].v[j+i*o] + 2.0*x*exp(x) + (x*x-1.0)*exp(x);
            err.err_df_max = max(err.err_df_max,abs(err_df));
            err.err_df_l2 += err_df*err_df*gauss::w(j,o)*h_bdry*0.5;

            //outf  << x << " " << rho.v[i*o + j] << endl;
            //outdf << x << " " << E[0].v[j+i*o_derivative] << endl;
        }
    }
    for(int i=N_tot-N_bdry;i<N_tot;++i){
        for(size_t j=0;j<o;++j){
            fp x = a+h_interior*N_interior+h_bdry*N_bdry+
                   h_bdry*(i-N_tot+N_bdry+gauss::x_scaled01(j,o));

            fp err_f  = rho.v[i*o + j] - (x*x-1.0)*exp(x);
            err.err_f_max = max(err.err_f_max,abs(err_f));
            err.err_f_l2 += err_f*err_f*gauss::w(j,o)*h_bdry*0.5;

            //attention: E = -dx phi
            fp err_df = E[0].v[j+i*o] + 2.0*x*exp(x) + (x*x-1.0)*exp(x);
            err.err_df_max = max(err.err_df_max,abs(err_df));
            err.err_df_l2 += err_df*err_df*gauss::w(j,o)*h_bdry*0.5;

            //outf  << x << " " << rho.v[i*o + j] << endl;
            //outdf << x << " " << E[0].v[j+i*o_derivative] << endl;
        }
    }
    err.err_f_l2 = sqrt(err.err_f_l2);
    err.err_df_l2 = sqrt(err.err_df_l2);

    return err;
}


BOOST_AUTO_TEST_CASE( poisson_dg_1d_direct_SIPG_finer_cells_on_bdry ){

    constexpr size_t o=4;
    fp olderr_l2=-1, olderr_max=-1;
    fp olderr_dfl2=-1, olderr_dfmax=-1;
    for(int N=4;N<400;N*=2){
        cout << "\n\nN: " << N << endl;
        errors err = solve_poisson1d_SIPG_with_smaller_bdry_cells<o>(3*N, N, 8);
        fp err_l2 = err.err_f_l2;
        fp err_max = err.err_f_max;
        fp err_dfmax = err.err_df_max;
        fp err_dfl2 = err.err_df_l2;
        cout << "err fmax=" << err_max 
             << ", err fl2=" << err_l2 << endl
             << "err dfmax=" << err_dfmax 
             << ", err dfl2=" << err_dfl2 << endl;
        if(olderr_l2>0){
            fp order_max = -log(err_max/olderr_max)/log(2.0);
            fp order_l2 = -log(err_l2/olderr_l2)/log(2.0);
            fp order_dfmax = -log(err_dfmax/olderr_dfmax)/log(2.0);
            fp order_dfl2 = -log(err_dfl2/olderr_dfl2)/log(2.0);
            cout << "order f l2: " << order_l2 
                 << ", order f max: " << order_max << endl
                 << "order df l2: " << order_dfl2
                 << ", order df max: " << order_dfmax << endl;

            if(N<50){
                BOOST_CHECK_GT( order_max, 5.0-0.2);
                BOOST_CHECK_GT( order_dfmax, 5.0-0.2);
                BOOST_CHECK_GT( order_l2, 5.0);
                BOOST_CHECK_GT( order_dfl2, 5.0-0.2);
            }
        }
        olderr_l2 = err_l2; 
        olderr_max = err_max; 
        olderr_dfl2 = err_dfl2; 
        olderr_dfmax = err_dfmax; 
    }
    BOOST_CHECK_LT( olderr_l2, 3.8e-10);
    BOOST_CHECK_LT( olderr_max, 8.1e-10);

}

#endif
