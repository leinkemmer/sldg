#include <generic/common.hpp>

#define BOOST_TEST_MAIN WENO_LIMITER 
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <algorithms/limiter.hpp>
#include <algorithms/dg.hpp>


bool enable_output=true; //is read in domain.hpp


template<size_t o>
fp analytic_beta(array<fp,o> coeff){
    fp exact3 =    36*coeff[0]*coeff[0];
    fp exact2 =    12*coeff[0]*coeff[0] + 12*coeff[0]*coeff[1] +
                    4*coeff[1]*coeff[1];
    fp exact1 = 9./5.*coeff[0]*coeff[0] + 3.*coeff[0]*coeff[1] +
                4./3.*coeff[1]*coeff[1] +    coeff[2]*coeff[2] + 
                  2.0*coeff[0]*coeff[2] + 2.*coeff[1]*coeff[2];


    return exact1+exact2+exact3;
}

BOOST_AUTO_TEST_CASE( betas ){

    constexpr int o = 4;
    cout << "o: " << o << endl;

    //some random numbers
    array<fp,o> coeff0 = {-0.03, 1.5,-2.8, 28.8};
    array<fp,o> coeff1 = {-1.24, 0.3, 4.1,-10.2};
    array<fp,o> coeff2 = {+2.48,-3.7, 0.25, 0.0};

    array<fp,o> u_0;
    array<fp,o> u_1;
    array<fp,o> u_2;
    for(int i=0;i<o;++i){
        u_0[i] = evaluate_standard_basis<o-1>(
                 gauss::x_scaled01(i,o),coeff0.data());
        u_1[i] = evaluate_standard_basis<o-1>(
                 gauss::x_scaled01(i,o),coeff1.data());
        u_2[i] = evaluate_standard_basis<o-1>(
                 gauss::x_scaled01(i,o),coeff2.data());
    }

    //check results u_0:

    array<fp,3> beta;
    beta[0] = compute_beta<o>(u_0.data());
    beta[1] = compute_beta<o>(u_1.data());
    beta[2] = compute_beta<o>(u_2.data());

    cout << "beta: " << beta << endl;

    fp exact_b0 = analytic_beta(coeff0);
    fp exact_b1 = analytic_beta(coeff1);
    fp exact_b2 = analytic_beta(coeff2);

    BOOST_CHECK_SMALL( abs(beta[0]-exact_b0), 7e-14 );
    BOOST_CHECK_SMALL( abs(beta[1]-exact_b1), 6e-13 );
    BOOST_CHECK_SMALL( abs(beta[2]-exact_b2), 2e-13 );

}


BOOST_AUTO_TEST_CASE( get_maxmino4 ){

    constexpr Index o = 4;

    fp in1[o];
    fp in2[o];
    fp out[o];
    fp parabola[o];
    fp line[o];
    //maxmin of in1,2 computed on bdry, out interior, 
    for(int i=0;i<o;++i){
        fp x = gauss::x_scaled01(i,o);
        in2[i] = 3.0*x*x*x - 2.0*x*x + 1.0*x - 4.0;
        in1[i] = 0.0*x*x*x + 4.0*x*x - 0.0*x -10.0;
        out[i] = 3.0*x*x*x - 4.0*x*x + 1.0*x + 1.0;
        parabola[i] = -2.0*x*x + 1.0;
        line[i] = 3.0*x - 5.0;
    }

    fp coeff[o];
    fp fl, fr;
    for(int i=1;i<10;++i){
        fp alpha = fp(i)/10.0;

        fp max_out_p = -100;
        fp min_out_p =  100;
        fp max_in_p = -100;
        fp min_in_p =  100;
        maxmin_o4_with_points(in1, in2, out,
                              alpha, max_in_p, min_in_p,
                              max_out_p, min_out_p);

        fp max_out_e = -100;
        fp min_out_e =  100;
        fp max_in_e = -100;
        fp min_in_e =  100;
        maxmin_o4(in1, alpha, 1.0, coeff, max_in_e, min_in_e, fl, fr);
        maxmin_o4(in2, 0.0, alpha, coeff, max_in_e, min_in_e, fl, fr);
        maxmin_o4(out, 0.0, 1.0,   coeff, max_out_e, min_out_e, fl, fr);

        BOOST_CHECK_SMALL( abs(max_out_e - max_out_p), 2e-4);
        BOOST_CHECK_SMALL( abs(min_out_e - min_out_p), 4e-5);
        BOOST_CHECK_SMALL( abs(max_in_e - max_in_p), 4e-3);
        BOOST_CHECK_SMALL( abs(min_in_e - min_in_p), 4e-2); 
    }
    fp max_p_e = -100;
    fp min_p_e =  100;
    maxmin_o4(parabola, -1.0, 1.0, coeff, max_p_e, min_p_e, fl, fr);
    BOOST_CHECK_SMALL( abs(max_p_e - 1.0), 5e-16);
    BOOST_CHECK_SMALL( abs(min_p_e + 1.0), 3e-15);
    fp max_l_e = -100;
    fp min_l_e =  100;
    maxmin_o4(line, 1.0/3.0, 8.0/3.0, coeff, max_l_e, min_l_e,fl,fr);
    BOOST_CHECK_SMALL( abs(max_l_e - 3.0), 6e-14);
    BOOST_CHECK_SMALL( abs(min_l_e + 4.0), 6e-15);
}

//checks two scenarios..
BOOST_AUTO_TEST_CASE( troubled_indicators ){

    constexpr Index o = 4;

    auto func_c = [](fp x){return cos(x);};

    fp a = 1.0;
    fp deltax = 0.4;

    fp f_in[3*o];
    for(int i=0;i<3;++i){
        for(int j=0;j<o;++j){
            fp x = a + deltax*(i + gauss::x_scaled01(j,o));
            f_in[j+i*o] = func_c(x);
        }
    }

    //note: minmod would mark the sin as troubled because in this 
    //      interval the maximum appears, while the other one does not 
    //      which is probably better because a smooth function is considered
    //      on which in general no problems with the projections appear
    BOOST_CHECK( !minmod_troubled_indicator<o>(f_in) );
    bool mean_troubled_idctr = mean_troubled_indicator<1,2,o>(f_in);
    BOOST_CHECK( !mean_troubled_idctr );

    auto func_nc = [](fp x){
        if(x<1.4) return 0.0;
        if(x<1.8) return 0.5 + 4*(x-1.6);
        return 1.0;
    };

    for(int i=0;i<3;++i){
        for(int j=0;j<o;++j){
            fp x = a + deltax*(i + gauss::x_scaled01(j,o));
            f_in[j+i*o] = func_nc(x);
        }
    }

    //note: minmod would mark 0-1-0 not as troubled, while the other one does 
    BOOST_CHECK( minmod_troubled_indicator<o>(f_in) );
    mean_troubled_idctr = mean_troubled_indicator<1,2,o>(f_in);
    BOOST_CHECK( mean_troubled_idctr );
}


template<Index o>
void check_sldg_indicator(fp* fin1, fp* fin2, bool compare_success){

    fp out[o];
    vector<fp> gx = gauss::all_x_scaled01(o);
    vector<fp> gw = gauss::all_weights(o);
    fp aux[o];
    for(int i=0;i<o;++i){
        aux[i] = 1.0;
        for(int j=0;j<i;++j)
            aux[i] /= (gx[i]-gx[j]);
        for(int j=i+1;j<o;++j)
            aux[i] /= (gx[i]-gx[j]);
    }

    for(int i=1;i<10;++i){
        fp alpha = fp(i)/10.0;
        dg_coeff<o> dg_c;
        dg_c.set(alpha);
        for(int i=0;i<o;++i){
            out[i] = 0.0;
            for(int j=0;j<o;++j){
                out[i] += dg_c.A[i][j]*fin1[j] + dg_c.B[i][j]*fin2[j];
            }
        }
        fp Idelta = mean_indicator_sldg<o>(alpha,fin1,fin2,out,aux);

        bool is_troubled = (Idelta>0.5);

        BOOST_CHECK_EQUAL( is_troubled, compare_success );
    }
}

//add more tests
BOOST_AUTO_TEST_CASE( find_constrained_poly ){

    constexpr int o=4;

    fp in[o];
    for(int i=0;i<o;++i)
        in[i] = sin(M_PI*gauss::x_scaled01(i,o));
    
    fp lr = 1.0;
    fp mean0 = -2.0;

    fp out[o];
    constrained_poly_minimization_o4(in, out, lr, mean0);

    //reference computed with a Matlab script
    fp reference[o] = { -1.907608827474539e-01, -1.221533980007275e+00,
                        -2.742334092349141e+00, -3.876977893578400e+00};


    for(int i=0;i<o;++i)
        BOOST_CHECK_SMALL( abs((reference[i]-out[i])/reference[i]), 1.4e-14 );

    lr = -1.0;
    constrained_poly_minimization_o4(in, out, lr, mean0);

    for(int i=0;i<o;++i)
        BOOST_CHECK_SMALL( abs((reference[o-i-1]-out[i])/reference[o-1-i]), 1.4e-14 );

}



fp check_modifier_step_linear_poly(void (*modifier)(fp*,fp*)){

    constexpr Index o=4;

    fp in[3*o];
    fp* in1 = &in[0*o];
    fp* in0 = &in[1*o];
    fp* in2 = &in[2*o];

    fill(in1,in1+o,0.0);
    fill(in2,in2+o,1.0);
    dg_coeff<o> dg_c;

    dg_c.set(0.5);

    for(int j=0;j<o;++j){
        in0[j] = 0.0;
        for(int k=0;k<o;++k)
            in0[j] += dg_c.A[j][k]*in1[k] + dg_c.B[j][k]*in2[k];
    }

    fp check_in[3*o];
    copy(in,in+3*o,check_in);
    
    fp mean_in=0.0;
    for(int i=0;i<o;++i)
        mean_in+=in0[i]*gauss::w(i,o)*0.5;

    fp out[o];
    
    modifier(in,out);
    for(int i=0;i<o;++i)
        cout << out[i] << endl;

    fp mean_out=0.0;
    for(int i=0;i<o;++i)
        mean_out+=out[i]*gauss::w(i,o)*0.5;

    BOOST_CHECK_SMALL( abs(mean_in-mean_out), 4e-15 );

    //don't modify the input
    for(int i=0;i<3*o;++i)
        BOOST_CHECK_EQUAL( in[i], check_in[i] );

    fp coeff[o];
    fp maxv = -10.0;
    fp minv =  10.0;
    fp tmp;
    maxmin_o4(out, 0.0, 1.0, coeff, maxv, minv, tmp, tmp);

    cout << "maxv: " << maxv << ", minv: " << minv << endl;

    return maxv;
}

BOOST_AUTO_TEST_CASE( modifier_test ){

    check_modifier_step_linear_poly(modifier_linear_weno<98,100,4>);
    check_modifier_step_linear_poly(modifier_simple_weno<4>);
    check_modifier_step_linear_poly(modifier_average<4>);
    check_modifier_step_linear_poly(modifier_hweno<4>);
}


bool is_tr(fp* loc_in){
    if(loc_in[4]==1 || loc_in[4]==4)
        return true; 
    else
        return false;
};
void mod_tr(fp* loc_in,fp* loc_out){
    if(loc_in[4]==1)
        for(int i=0;i<4;++i)
            loc_out[i] = loc_in[i];
    else if(loc_in[4]==4)
        for(int i=0;i<4;++i)
            loc_out[i] = loc_in[i+4*2];
    else{
        cout << "should not printed" << endl;
    }
};
BOOST_AUTO_TEST_CASE( weno_limiter_a ){

    //checks memory accesses
    int nx = 4;
    int nv = 4;
    constexpr Index o = 4;

    vector<fp> in(nx*o*nv*o);
    for(int j=0;j<nv*o;++j){
        for(int i=0;i<nx*o;++i){
            int idx = i + nx*o*j; 
            int v = i/o+1;
            in[idx] = v;
        }
    }

    vector<fp> out(nx*o*nv*o,-1);
    int dim = 0;
    weno_limiter_1d_dim<o,2,is_tr,mod_tr>(dim,in.data(),out.data(),{nx,nv,o,o});

    for(int j=0;j<nv*o;++j){
        for(int i=0;i<nx*o;++i){
            int idx = i + nx*o*j; 
            if(j<nv*o/2){
               if(i>=(nx-1)*o)
                   BOOST_CHECK(out[idx]==0);
               else{
                   int v = i/o+1;
                   BOOST_CHECK(out[idx]==v);
               }
            } else {
               if(i<o)
                   BOOST_CHECK(out[idx]==0);
               else{
                   int v = i/o+1;
                   BOOST_CHECK(out[idx]==v);
               }
            }
        }
    }


    for(int j=0;j<nv*o;++j){
        for(int i=0;i<nx*o;++i){
            int idx = i + nx*o*j; 
            int v = j/o+1;
            in[idx] = v;
        }
    }

    dim = 1;
    weno_limiter_1d_dim<o,2,is_tr,mod_tr>(dim,in.data(),out.data(),{nx,nv,o,o});

    for(int j=0;j<nv*o;++j){
        for(int i=0;i<nx*o;++i){
            int idx = i + nx*o*j; 
            if(j>o||j<(nv-1)*o){
                int v = j/o+1;
                BOOST_CHECK(in[idx]==v);
            }
            else if(j<o)
                BOOST_CHECK(in[idx]==4);
            else 
                BOOST_CHECK(in[idx]==1);
        }
    }

}



BOOST_AUTO_TEST_CASE( weno_limiter_b ){

    constexpr size_t d = 2;

    Index Nx = 5;
    Index Ny = 3;
    Index o = 4;

    array<Index,2*d> e = {Nx,Ny,o,o};

    multi_array<fp,2*d> in(e);
    multi_array<fp,2*d> out(e);

    fill(in.data(),in.data()+in.num_elements(),0.0);
    fill(out.data(),out.data()+out.num_elements(),0.0);

    int dofx = Nx*o;
    int dofy = Ny*o;
    for(int j=0;j<dofy;++j){
        for(int i=0;i<dofx-o;++i){
            int idx = i+dofx*j;
            if(i/o>j/o){
                in.v[idx+2] = 1; 
            }
        }
    }


    int dim0 = 0;
    weno_limiter_1d_k<d>(o, dim0, in, out, e,
                         troubled_cell_indicator::mean_err,
                         troubled_cell_modifier::average);

    for(int j=0;j<dofy;++j){
        for(int i=0;i<dofx;++i){
            int idx = i+dofx*j;
            if(i/o==j/o+1)
                BOOST_CHECK(out.v[idx]==0.5);
            else if(i/o==Nx-1 && j<dofy/2)
                BOOST_CHECK(out.v[idx]==0.5);
            else
                BOOST_CHECK(out.v[idx]==in.v[idx]);
        }
    }


    fill(in.begin(),in.begin()+in.num_elements(),0.0);
    for(int j=0;j<dofy;++j){
        for(int i=0;i<dofx;++i){
            int idx = i+dofx*j;
            if(i/o<j/2){
                in.v[idx] = 1; 
            }
        }
    }

    int dim1 = 1;
    weno_limiter_1d_k<d>(o, dim1, in, out, e,
                         troubled_cell_indicator::mean_err,
                         troubled_cell_modifier::average);

    for(int j=0;j<dofy;++j){
        for(int i=0;i<dofx;++i){
            int idx = i+dofx*j;
            if(i/o==0 && j/o==0)
                BOOST_CHECK(out.v[idx] == 0.5);
            else if(i/o==2 && j/o==1)
                BOOST_CHECK(out.v[idx] == 0.5);
            else if(i/o==4 && j/o==2)
                BOOST_CHECK(out.v[idx] == 0.5);
            else
                BOOST_CHECK(out.v[idx] == in.v[idx]);
        }
    }


}


BOOST_AUTO_TEST_CASE( troubled_indicators_sldg ){

    constexpr Index o=4;

    fp fin1[o];
    fp fin2[o];

    //cout << "fail 1 (discontinuous function): " << endl;
    for(int i=0;i<o;++i){
        fin1[i] = 0;
        fin2[i] = 1;
    }
    check_sldg_indicator<o>(fin1,fin2,true);

    //cout << "fail 2 (big jump in opposite direction): " << endl;
    for(int i=0;i<o;++i){
        fp x1 = 0.5*gauss::x_scaled01(i,o);
        fin1[i] = x1*x1*x1;
        fp x2 = 0.5*gauss::x_scaled01(i,o) + 0.5;
        fin2[i] = 0.5 + x2*x2*x2;
    }
    check_sldg_indicator<o>(fin1,fin2,true);

    //cout << "success 1 (smooth function): " << endl;
    for(int i=0;i<o;++i){
        fp x1 = gauss::x_scaled01(i,o);
        fin1[i] = sin(x1);
        fp x2 = gauss::x_scaled01(i,o) + 1.0;
        fin2[i] = sin(x2);
    }
    check_sldg_indicator<o>(fin1,fin2,false);

}


template<Index o>
void check_sldg_modifier(fp alpha, fp* fin1, fp* fin2, fp* out, 
                         void (*modifier)(fp,fp*,fp*,fp*)){

    //cout << "\nalpha: " << alpha << endl;
    fp in1[o];
    fp in2[o];
    for(int i=0;i<o;++i){
        in1[i] = fin1[i];
        in2[i] = fin2[i];
    }
    dg_coeff<o> dg_c;
    dg_c.set(alpha);
    for(int i=0;i<o;++i){
        out[i] = 0.0;
        for(int j=0;j<o;++j){
            out[i] += dg_c.A[i][j]*in1[j] + dg_c.B[i][j]*in2[j];
        }
    }
    //cout << "projection: " << endl;
    //for(int i=0;i<o;++i)
    //    cout << out[i] << endl;
 
    //should I apply a maxminlimiter as well?
    modifier(alpha, in1, in2, out);

    //cout << "modified: " << endl;
    //for(int i=0;i<o;++i)
    //    cout << out[i] << endl;
}


BOOST_AUTO_TEST_CASE( modifier_sldg ){

    constexpr Index o = 4;

    fp fin1[o];
    fp fin2[o];
    for(int i=0;i<o;++i){
        fin1[i] = 0.0;
        fp x = gauss::get_x_scaled01(i,o);
        fin2[i] = 0.5*x*x+0.5;
    }

    int N = 10;
    for(int i=1;i<N;++i){
        fp alpha = fp(i)/fp(N);

        fp out_l[o];
        check_sldg_modifier<o>(alpha, fin1, fin2, out_l, 
                               modifier_sldg_lweno<o>);

        fp mass_lweno=0.0;
        for(int i=0;i<o;++i){
            mass_lweno += 0.5*gauss::w(i,o)*out_l[i];
            //for this test example, linear weno generates
            if(i<o-1) 
                BOOST_CHECK_LE(out_l[i], out_l[i+1]);
        }

        fp out_p[o];
        check_sldg_modifier<o>(alpha, fin1, fin2, out_p, 
                               modifier_sldg_meanparabola<o>);

        fp mass_pweno=0.0;
        for(int i=0;i<o;++i){
            mass_pweno += 0.5*gauss::w(i,o)*out_p[i];
        }

        BOOST_CHECK_SMALL( abs(mass_pweno-mass_lweno), 6e-15 );
    }


}


