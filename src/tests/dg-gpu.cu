#include <generic/common.hpp>

#define BOOST_TEST_MAIN DG_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg-gpu.cu>
#include <algorithms/rho.hpp>
#include <algorithms/rho-gpu.cu>

#include <programs/vp-block-list.hpp>
#include <generic/common.hpp>

#ifdef USE_SINGLE_PRECISION
float machine_prec = 1e-6;
#else
double machine_prec = 1e-15;
#endif

bool enable_output; //is read in domain.hpp


fp iv_strong_landau(array<fp,4> xv) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];
    return 1.0/(2.0*M_PI)*exp(-v*v/2.0)*exp(-w*w/2.0)
        *(1.0+0.5*(cos(0.5*x) + cos(0.5*y)));
}

fp iv_strong_landau_six_dim(array<fp,6> xv) {
    fp x=xv[0]; fp y=xv[1]; fp z=xv[2];
    fp v=xv[3]; fp w=xv[4]; fp u=xv[5];
    return 1.0/pow((2.0*M_PI),3.0/2.0)*exp(-v*v/2.0)*exp(-w*w/2.0)*exp(-u*u/2.0)
        *(1.0+1.0/3.0*(cos(0.5*x) + cos(0.5*y) + cos(0.5*z)));
}

template<Index d>
multi_array<fp,d> construct_F(array<Index,d> e) {
    multi_array<fp,d> F(e);

    for(auto i : range<d>(e)) {
        fp val = 0.0;
        for(Index k=0;k<d;k++)
            val += pow(-1.0,k+1)*fp(k)/fp(d)*fp(i[k])/fp(e[k]);
        F(i) = val;
    }
    
    return F;
}


template<Index d>
fp check_advection(Index dim) {


    Index Nx=20, Nv=19, ox=4, ov=4;
    fp tau = 0.9;

    array<Index,d> s = (dim<2) ?
        array<Index,d>({Nv,Nv,ov,ov}) : array<Index,d>({Nx,Nx,ox,ox});
    multi_array<fp,d> F = construct_F<d>(s);
    
    array<Index,2*d> e = {Nx,Nx,Nv,Nv,ox,ox,ov,ov};
    array<fp,4> a = {0.0, 0.0, -6.0, -6.0};
    array<fp,4> b = {2*M_PI, 2*M_PI, 6.0, 6.0};
    fp dd=(b[dim]-a[dim])/fp(e[dim]);
    domain<d> in(e, a, b);
    domain<d> out(e, a, b);

    in.init(iv_strong_landau);    

    // Do the computation on the CPU
    vector<fp> bdr_to;
    Index bdr_max_length = 5*ov*(Nx*ox*Nx*ox*Nv*ov);
    bdr_to.resize(bdr_max_length);

    fp alpha_max = 3.0;
    fp bdr_len = int(alpha_max)+1;
    in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,&F);

    dg_coeff<4> dg4_c;

    array<Index,2*d-2> max = slice_no<d>(dim,e);
    for(auto idx : range<2*d-2>(max)) {

        domain<4>::view vin  = slice_array(in.data, dim, idx);
        domain<4>::view vout = slice_array(out.data, dim, idx);
        fp alpha = tau*F( get_idx_F<d>(dim,idx) )/dd;


        fp* bdr_ptr = in.unpack_boundary(idx, dim, bdr_len, bdr_to);
        translate1d<1,4>(vin, vout, bdr_ptr, bdr_len*ox, alpha, dg4_c, TM_ALL);
    }
    
    // Do the computation on the GPU
    multi_array<fp,2*d> d_in(in.data.shape(), true);
    multi_array<fp,2*d> d_out(in.data.shape(), true);
    d_in = in.data;
    translate_gpu<d/2,d/2,d/2> trgpu(e, bdr_max_length);

    Index N = (dim<d/2) ? Nv*Nv*ov*ov : Nx*Nx*ox*ox;
    unique_ptr<dg_coeff_generic<d/2>> dg_c = make_dg_coeff_gpu<d/2>(e[d+dim], N);
    dg_c->compute(tau, dd, F);
    dg_c->copy_to_gpu();
    trgpu.translate(dim, tau, dd, *dg_c, d_in, d_out, 0,0,nullptr,true,nullptr,0);
    
    multi_array<fp,2*d> out_from_gpu(in.data.shape());
    out_from_gpu = d_out;

    return inf_norm<d>(out.data, out_from_gpu);
}


BOOST_AUTO_TEST_CASE( dg_gpu_o4 ) {

    BOOST_CHECK_SMALL( check_advection<4>(0), machine_prec );
    BOOST_CHECK_SMALL( check_advection<4>(1), machine_prec );
    BOOST_CHECK_SMALL( check_advection<4>(2), machine_prec );
    BOOST_CHECK_SMALL( check_advection<4>(3), machine_prec );
}


template<Index d>
fp check_advection_six_dim(Index dim) {
    Index Nx=6, Nv=5, ox=3, ov=3;
    fp tau = 0.9;

    array<Index,d> s = (dim<3) ?
        array<Index,d>({Nv,Nv,Nv,ov,ov,ov}) : array<Index,d>({Nx,Nx,Nx,ox,ox,ox});
    multi_array<fp,d> F = construct_F<d>(s);
    
    array<Index,2*d> e = {Nx,Nx,Nx,Nv,Nv,Nv,ox,ox,ox,ov,ov,ov};
    array<fp,6> a = {0.0, 0.0, 0.0, -6.0, -6.0, -6.0};
    array<fp,6> b = {2*M_PI, 2*M_PI, 2*M_PI, 6.0, 6.0, 6.0};
    fp dd=(b[dim]-a[dim])/fp(e[dim]);
    domain<d> in(e, a, b);
    domain<d> out(e, a, b);

    in.init(iv_strong_landau_six_dim);    

    // Do the computation on the CPU
    vector<fp> bdr_to;
    Index bdr_max_length = 5*ov*(Nx*ox*Nx*ox*Nx*ox*Nv*ov*Nv*ov);
    bdr_to.resize(bdr_max_length);

    fp alpha_max = 3.0;
    fp bdr_len = int(alpha_max)+1;
    in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,&F);

    dg_coeff<3> dg3_c;

    array<Index,2*d-2> max = slice_no<d>(dim,e);
    for(auto idx : range<2*d-2>(max)) {

        domain<6>::view vin  = slice_array(in.data, dim, idx);
        domain<6>::view vout = slice_array(out.data, dim, idx);
        fp alpha = tau*F( get_idx_F<d>(dim,idx) )/dd;


        fp* bdr_ptr = in.unpack_boundary(idx, dim, bdr_len, bdr_to);
        translate1d<1,3>(vin, vout, bdr_ptr, bdr_len*ox, alpha, dg3_c, TM_ALL);
    }
    
    // Do the computation on the GPU
    multi_array<fp,2*d> d_in(in.data.shape(), true);
    multi_array<fp,2*d> d_out(in.data.shape(), true);
    d_in = in.data;
    translate_gpu<d/2,d/2,d/2> trgpu(e, bdr_max_length);

    Index N = (dim<d/2) ? Nv*Nv*Nv*ov*ov*ov : Nx*Nx*Nx*ox*ox*ox;
    unique_ptr<dg_coeff_generic<d/2>> dg_c = make_dg_coeff_gpu<d/2>(e[d+dim], N);
    dg_c->compute(tau, dd, F);
    dg_c->copy_to_gpu();
    trgpu.translate(dim, tau, dd, *dg_c, d_in, d_out, 0,0,nullptr,true,nullptr,0);
    
    multi_array<fp,2*d> out_from_gpu(in.data.shape());
    out_from_gpu = d_out;

    return inf_norm<d>(out.data, out_from_gpu);
}


BOOST_AUTO_TEST_CASE( dg_gpu_6dim ) {

    BOOST_CHECK_SMALL( check_advection_six_dim<6>(0), machine_prec );
    BOOST_CHECK_SMALL( check_advection_six_dim<6>(1), machine_prec );
    BOOST_CHECK_SMALL( check_advection_six_dim<6>(2), machine_prec );
    BOOST_CHECK_SMALL( check_advection_six_dim<6>(3), machine_prec );
    BOOST_CHECK_SMALL( check_advection_six_dim<6>(4), machine_prec );
    BOOST_CHECK_SMALL( check_advection_six_dim<6>(5), machine_prec );
}



template<Index d>
fp check_advection_with_boundary(Index dim) {
    Index Nx=16, Nv=17, ox=4, ov=4;
    fp tau = 0.9;

    array<Index,d> e_advfield = ( (dim>=d/2) ? array<Index,d>({Nx,Nx,ox,ox}) : 
                                               array<Index,d>({Nv,Nv,ov,ov}) );

    array<Index,2*d> e = {Nx,Nx,Nv,Nv,ox,ox,ov,ov};
    array<fp,4> a = {0.0, 0.0, -2.0, -2.0};
    array<fp,4> b = {2*M_PI, 2*M_PI, 2.0, 2.0};
    fp dd=(b[dim]-a[dim])/fp(e[dim]);

    array_dxdv<fp,2,2> a_ = {0.0, 0.0, -2.0, -2.0};
    array_dxdv<fp,2,2> b_ = {2*M_PI, 2*M_PI, 2.0, 2.0};
    array<multi_array<fp,d>,d/2> v_values;
    init_v_values(a_,b_,e,v_values);

    multi_array<fp,d> F = ( (dim>=d/2) ? construct_F<d>(e_advfield) : v_values[dim] );

    domain<d> in(e, a, b);
    domain<d> out(e, a, b);

    in.init(iv_strong_landau);    

    // Do the computation on the CPU
    int nr=1;
    if(dim<d/2)
	    nr = e[(dim+1)%2]*e[d+(dim+1)%2];
    else
        nr = e[d/2 + (dim-1)%2]*e[d + d/2 + (dim-1)%2]; 
    Index bdr_max_length = 5*e[d+dim]*prod(e_advfield)*nr;

    fp alpha_max = tau*max_element_abs(F)/dd; //3.0
    fp bdr_len = int(alpha_max)+1;

    dg_coeff<4> dg4_c;

    vector<fp> bdr_to(bdr_max_length);
    in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,&F);
    array<Index,2*d-2> max = slice_no<d>(dim,e);
    for(auto idx : range<2*d-2>(max)) {

        domain<4>::view vin  = slice_array(in.data, dim, idx);
        domain<4>::view vout = slice_array(out.data, dim, idx);
        fp alpha = tau*F( get_idx_F<d>(dim,idx) )/dd;

        fp* bdr_ptr = in.unpack_boundary(idx, dim, bdr_len, bdr_to);
        translate1d<1,4>(vin, vout, bdr_ptr, bdr_len*ox, alpha, dg4_c, TM_ALL);
    }

    // Do the computation on the GPU
    multi_array<fp,2*d> d_in(in.data.shape(), true);
    multi_array<fp,2*d> d_out(in.data.shape(), true);
    d_in = in.data;
 
    gpu_array<fp> bdry_to_gpu(bdr_max_length);

    pack_boundary_from_gpu<d/2,d/2> pbfgpu(e, bdr_max_length);
    sign_gpu<d/2> sgpu_F(e_advfield);
    sgpu_F.compute(F);
    sgpu_F.copy_to_gpu();
    pbfgpu.pack(dim,sgpu_F.d_sign,d_in,bdr_len,
                bdry_to_gpu.d_data,sgpu_F.max_idx_left);

    translate_gpu<d/2,d/2,d/2> trgpu(e, bdr_max_length);

    Index N = prod(e_advfield);
    unique_ptr<dg_coeff_generic<d/2>> dg_c = make_dg_coeff_gpu<d/2>(e[d+dim], N);
    dg_c->compute(tau, dd, F);
    dg_c->copy_to_gpu();

    //pbfgpu.rearrange(dim,bdr_len,bdr_max_length,bdry_to_gpu.d_data,
    //                 sgpu_F.d_sign,sgpu_F.max_idx_left);
    //bdry_to_gpu.copy_to_host();
    ////check boundary 
    //fp err=0.0;
    //for(size_t i=0;i<bdr_to.size();i++)
    //    err = std::max(err, abs(bdr_to[i]-bdry_to_gpu[i]));
    //BOOST_CHECK_SMALL( err, machine_prec );
    //cout << "err bdry: " << err << endl;

    trgpu.translate(dim, tau, dd, *dg_c, d_in, d_out, bdr_len, bdr_max_length,
                    bdry_to_gpu.d_data, true, sgpu_F.d_sign, sgpu_F.max_idx_left);

    multi_array<fp,2*d> out_from_gpu(in.data.shape());
    out_from_gpu = d_out;

    return inf_norm<d>(out.data, out_from_gpu);
}


BOOST_AUTO_TEST_CASE( advection_with_boundary ) {

    BOOST_CHECK_SMALL( check_advection_with_boundary<4>(3), machine_prec );
}


template<Index d>
fp check_pack_boundary_from_gpu(Index dim) {
    Index Nx=16, Nv=17, ox=4, ov=4;

    array<Index,d> s = array<Index,d>({Nx,Nx,ox,ox});
    multi_array<fp,d> F = construct_F<d>(s);
    
    array<Index,2*d> e = {Nx,Nx,Nv,Nv,ox,ox,ov,ov};
    array<Index,d> e_xpart = {Nx,Nx,ox,ox};
    array<fp,4> a = {0.0, 0.0, -3.0, -2.5};
    array<fp,4> b = {2*M_PI, 2*M_PI, 6.0, 2.0};
    domain<d> in(e, a, b);

    in.init(iv_strong_landau);    

    // pack the boundary on the CPU
    fp bdr_len = 4;
    Index bdr_max_length = bdr_len*ov*(Nx*ox*Nx*ox*Nv*ov);
    vector<fp> bdr_to(bdr_max_length);

    in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,&F);

    // pack the boundary on the GPU 
    multi_array<fp,2*d> d_in(in.data.shape(), true);
    d_in = in.data;
    
    gpu_array<fp> bdry_to_gpu(bdr_max_length);

    pack_boundary_from_gpu<d/2,d/2> pbfgpu(e, bdr_max_length);
    sign_gpu<d/2> sgpu(e_xpart);
    sgpu.compute(F);
    sgpu.copy_to_gpu();
    pbfgpu.pack(dim,sgpu.d_sign,d_in,bdr_len,
                bdry_to_gpu.d_data,sgpu.max_idx_left);
    pbfgpu.rearrange(dim,bdr_len,bdr_max_length,bdry_to_gpu.d_data,
                     sgpu.d_sign,sgpu.max_idx_left);
    bdry_to_gpu.copy_to_host();
 
    // error in inf norm
    fp err=0.0;
    for(size_t i=0;i<bdr_to.size();i++)
        err = max(err, abs(bdr_to[i]-bdry_to_gpu[i]));
    cout << "err _v: " << err << endl;
    return err;
}

template<Index d>
fp check_pack_boundary_from_gpu_x(Index dim) {
    Index Nx=16, Nv=17, ox=4, ov=4;

    //array<Index,d> s = array<Index,d>({Nx,Nx,ox,ox});

    array<Index,8> e = {Nx,Nx,Nv,Nv,ox,ox,ov,ov};
    array<Index,d> e_vpart = {Nv,Nv,ov,ov};
    array<fp,4> a = {0.0, 0.0, -3.0, -2.5};
    array<fp,4> b = {2*M_PI, 2*M_PI, 6.0, 2.0};
    array_dxdv<fp,2,2> a_ = {0.0, 0.0, -3.0, -2.5};
    array_dxdv<fp,2,2> b_ = {2*M_PI, 2*M_PI, 6.0, 2.0};
    domain<d> in(e, a, b);

    array<multi_array<fp,d>,d/2> v_values;
    init_v_values(a_,b_,e,v_values);
    
    multi_array<fp,d>& F = v_values[dim%2];

    in.init(iv_strong_landau);    

    // pack the boundary on the CPU
    fp alpha_max = 0.1*6.0/(b[dim]-a[dim])*fp(e[dim]);
    Index bdr_len = int(alpha_max)+1;
    cout << "bdr_len: " << bdr_len << endl;
    Index bdr_max_length = bdr_len*ov*(Nx*ox*Nv*ov*Nv*ov);
    vector<fp> bdr_to(bdr_max_length);

    using index_no = index_dxdv_no<2, 2>;
    in.pack_boundary(dim,BDRY_DYNAMIC,bdr_len,bdr_to,
              [&F](array<Index,2*(d)> _idx) {
              index_no idx(_idx);
              return F(idx.v_part());
              });

    // pack the boundary on the GPU 
    multi_array<fp,2*d> d_in(in.data.shape(), true);
    d_in = in.data;
 
    gpu_array<fp> bdry_to_gpu(bdr_max_length);

    pack_boundary_from_gpu<d/2,d/2> pbfgpu(e, bdr_max_length);
    sign_gpu<d/2> sgpu(e_vpart);
    sgpu.compute(F);
    sgpu.copy_to_gpu();
    pbfgpu.pack(dim,sgpu.d_sign,d_in,bdr_len,
                bdry_to_gpu.d_data,sgpu.max_idx_left);
    pbfgpu.rearrange(dim,bdr_len,bdr_max_length,bdry_to_gpu.d_data,
                     sgpu.d_sign,sgpu.max_idx_left);
    bdry_to_gpu.copy_to_host();
    // error in inf norm
    fp err=0.0;
    for(size_t i=0;i<bdr_to.size();i++){
        err = max(err, abs(bdr_to[i]-bdry_to_gpu[i]));
    }
    cout << "err _x: " << err << endl;
    return err;
}

BOOST_AUTO_TEST_CASE( pack_boundary_gpu ) {
    
    BOOST_CHECK_SMALL( check_pack_boundary_from_gpu<4>(2), fp(5e-15) );
    BOOST_CHECK_SMALL( check_pack_boundary_from_gpu<4>(3), fp(5e-15) );
    BOOST_CHECK_SMALL( check_pack_boundary_from_gpu_x<4>(0), fp(5e-15) );
    BOOST_CHECK_SMALL( check_pack_boundary_from_gpu_x<4>(1), fp(5e-15) );

}

__host__ __device__
fp f0_1(fp* xv){
    fp x = xv[0];
    fp v = xv[1];
    return sin(x)*exp(-v*v);
}

__host__ __device__
fp f0_3(fp* xv){
    fp x = xv[0];
    fp v1 = xv[1];
    fp v2 = xv[2];
    fp v3 = xv[3];
    return sin(x)*exp(-v1*v1)*exp(-v2*v2)*exp(-v3*v3);
}

template<size_t dv, fp (*f0)(fp*)>
void check_pack_fine2coarse(Index Nx, Index Nv, Index o){

    constexpr size_t dx = 1;
    constexpr size_t d = dx+dv;

    index_dxdv_no<dx,dv> e;
    array_dxdv<fp,dx,dv> a;
    array_dxdv<fp,dx,dv> b;
    e[0] = Nx;
    e[d] = o;
    a[0] = -1.0;
    b[0] =  1.0;
    for(size_t i=0;i<dv;++i){
        e[i+1] = Nv;
        e[d+i+1] = o;
        a[i+1] =-1.0;
        b[i+1] = 1.0;
    }
    fp tau = 0.19;

    fp hx = (b[0]-a[0])/fp(e[0]);
    fp CFL = max(abs(a[1]),abs(b[1]))*tau/hx;
    cout << "CFL: " << CFL << endl;
    int bdr_len = int(CFL)+1;

    Index bdr_max_length = bdr_len*o*prod(e.slice(0));
    cout << "bdr_max_length: " << bdr_max_length << endl;

    vp_block_cpu<dx,dv,f0> block_cpu(a,b,e,bdr_max_length);
    block_cpu.init();

    bdr_len_info bli;

    bli = block_cpu.pack_boundary(0,tau,bdr_max_length);

    cout << "bdr_len: " << bli.bdr_len << endl;
    vector<fp> ref = block_cpu.bdr_to_send;

    fill(block_cpu.bdr_to_send.begin(),block_cpu.bdr_to_send.end(),-100.0);

    //naive checks with ratio=1, should give the same output as pack boundary
    block_cpu.loc = location::left;
    bli = block_cpu.pack_boundary_fine2coarse(tau,bdr_max_length);

    fp err_left=0.0;
    for(int i=0;i<bdr_max_length;++i){
        fp reference = (i>=bli.bdr_tot_left) ? ref[i] : 0.0;
        fp err = abs(reference - block_cpu.bdr_to_send[i]);
        err_left = max(err_left,err);
    }
    BOOST_CHECK_SMALL(err_left,machine_prec);
 
    fill(block_cpu.bdr_to_send.begin(),block_cpu.bdr_to_send.end(),-100.0);

    block_cpu.loc = location::right;
    bli = block_cpu.pack_boundary_fine2coarse(tau,bdr_max_length);

    fp err_right=0.0;
    for(int i=0;i<bdr_max_length;++i){
        fp reference = (i<bli.bdr_tot_left) ? ref[i] : 0.0;
        fp err = abs(reference - block_cpu.bdr_to_send[i]);
        err_right = max(err_right,err);
    }
    BOOST_CHECK_SMALL(err_right,machine_prec);
 

    dg_evaluator dg(o);
    //checks against the transferred data with the big grid
    for(int ratio=1;ratio<9;++ratio){

        cout << "\n\nratio: " << ratio << endl;

        index_dxdv_no<dx,dv> e_fg = e;
        e_fg[0] = e[0]*ratio;
        vp_block_cpu<dx,dv,f0> block_cpu_fine_grid(a,b,e_fg,bdr_max_length,
                                                   false,ratio);
        block_cpu_fine_grid.init();

        //check collection for sending to left domain
        block_cpu_fine_grid.loc = location::right;
        bli = block_cpu_fine_grid.pack_boundary_fine2coarse(tau,bdr_max_length);
        vector<fp> out = block_cpu_fine_grid.bdr_to_send;
        cout << "bdr_len: " << bli.bdr_len << endl;

        fp* p_out = out.data();
        fp* p_ref = ref.data();

        fp maxerr = 0.0;
        int dof_v = prod(e.v_part());
        for(int i=0;i<dof_v/2;++i){
            for(int k=0;k<bli.bdr_len;++k){
                for(int j=0;j<o;++j){
                    fp f2c = p_out[k*o+j];
                    fp ref = p_ref[k*o+j];
                    maxerr = max(maxerr,abs(f2c-ref));
                }
            }
            p_out += bli.bdr_len*o;
            p_ref += bli.bdr_len*o;
        } 

        #ifdef USE_SINGLE_PRECISION
        fp tol_comp = machine_prec; 
        #else
        fp tol_comp = 7.3e-11;
        #endif

        BOOST_CHECK_SMALL( maxerr, tol_comp );

        //check collection for sending to right domain
        block_cpu_fine_grid.loc = location::left;
        bli = block_cpu_fine_grid.pack_boundary_fine2coarse(tau,bdr_max_length);

        out = block_cpu_fine_grid.bdr_to_send;
        cout << "bdr_len: " << bli.bdr_len << endl;

        p_out = out.data()+bli.bdr_len*o*dof_v/2;
        p_ref = ref.data()+bli.bdr_len*o*dof_v/2;

        maxerr = 0.0;
        for(int i=dof_v/2;i<dof_v;++i){
            for(int k=0;k<bli.bdr_len;++k){
                for(int j=0;j<o;++j){
                    fp f2c = p_out[k*o+j];
                    fp ref = p_ref[k*o+j];
                    maxerr = max(maxerr,abs(f2c-ref));
                }
            }
            p_out += bli.bdr_len*o;
            p_ref += bli.bdr_len*o;
        } 
        BOOST_CHECK_SMALL( maxerr, tol_comp );


    }

}


BOOST_AUTO_TEST_CASE( pack_bdry_fine_grid_to_coarse_grid_cpu ){

    check_pack_fine2coarse<1,f0_1>(20, 10, 4);
    check_pack_fine2coarse<3,f0_3>(20, 10, 4);
    
}

template<size_t dv, fp (*f0)(fp*)>
void check_pack_fine2coarse_gpu_cpu_comparison(Index Nx, Index Nv, Index o){

    constexpr size_t dx = 1;
    constexpr size_t d = dx+dv;

    index_dxdv_no<dx,dv> e;
    array_dxdv<fp,dx,dv> a;
    array_dxdv<fp,dx,dv> b;
    e[0] = Nx;
    e[d] = o;
    a[0] = -1.0;
    b[0] =  1.0;
    for(size_t i=0;i<dv;++i){
        e[i+1] = Nv;
        e[d+i+1] = o;
        a[i+1] =-1.0;
        b[i+1] = 1.0;
    }
    fp tau = 0.19;

    fp hx = (b[0]-a[0])/fp(e[0]);
    fp CFL = max(abs(a[1]),abs(b[1]))*tau/hx;
    cout << "CFL: " << CFL << endl;
    int bdr_len = int(CFL)+1;

    Index bdr_max_length = bdr_len*o*prod(e.slice(0));
    cout << "bdr_max_length: " << bdr_max_length << endl;

    bdr_len_info bli_cpu;
    bdr_len_info bli_gpu;

    //checks against the transferred data with the big grid
    for(int ratio=1;ratio<9;++ratio){

        cout << "\n\nratio: " << ratio << endl;

        index_dxdv_no<dx,dv> e_fg = e;
        e_fg[0] = e[0]*ratio;
        vp_block_cpu<dx,dv,f0> block_cpu_fine_grid(a,b,e_fg,
                                                   bdr_max_length,false,ratio);
        vp_block_gpu<dx,dv,f0> block_gpu_fine_grid(a,b,e_fg,0,
                                                   bdr_max_length,false,ratio);
        block_cpu_fine_grid.init();
        block_gpu_fine_grid.init();

        //check collection for sending to left domain
        block_cpu_fine_grid.loc = location::right;
        block_gpu_fine_grid.loc = location::right;
        bli_cpu = block_cpu_fine_grid.pack_boundary_fine2coarse(tau,bdr_max_length);
        bli_gpu = block_gpu_fine_grid.pack_boundary_fine2coarse(tau,bdr_max_length);

        cout << "bdr_len=" << bli_cpu.bdr_len << endl;
        
        BOOST_CHECK_EQUAL( bli_cpu.bdr_len,      bli_gpu.bdr_len );
        BOOST_CHECK_EQUAL( bli_cpu.bdr_tot_left, bli_gpu.bdr_tot_left );
        BOOST_CHECK_EQUAL( bli_cpu.bdr_tot_len,  bli_gpu.bdr_tot_len );

        vector<fp> cpu_ref = block_cpu_fine_grid.bdr_to_send;

        size_t len_left = cpu_ref.size();
        vector<fp> out_from_gpu_l(len_left);
        gpuErrchk(cudaMemcpy(out_from_gpu_l.data(),
                             block_gpu_fine_grid.d_bdr_to_send,
                             len_left*sizeof(fp),cudaMemcpyDeviceToHost));

        fp maxerr = 0.0;
        for(size_t k=0;k<len_left;++k)
            maxerr = max(maxerr,fabs(cpu_ref[k]-out_from_gpu_l[k]));
        BOOST_CHECK_SMALL( maxerr, machine_prec );

        //check collection for sending to right domain
        block_cpu_fine_grid.loc = location::left;
        block_gpu_fine_grid.loc = location::left;
        bli_cpu = block_cpu_fine_grid.pack_boundary_fine2coarse(tau, bdr_max_length);
        bli_gpu = block_gpu_fine_grid.pack_boundary_fine2coarse(tau, bdr_max_length);

        cpu_ref = block_cpu_fine_grid.bdr_to_send;
        size_t len_right = cpu_ref.size();
        vector<fp> out_from_gpu_r(len_right);
        gpuErrchk(cudaMemcpy(out_from_gpu_r.data(),
                             block_gpu_fine_grid.d_bdr_to_send,
                             len_right*sizeof(fp),cudaMemcpyDeviceToHost));

        cout << "bdr_len=" << bli_cpu.bdr_len << endl;

        BOOST_CHECK_EQUAL( bli_cpu.bdr_len,      bli_gpu.bdr_len );
        BOOST_CHECK_EQUAL( bli_cpu.bdr_tot_left, bli_gpu.bdr_tot_left );
        BOOST_CHECK_EQUAL( bli_cpu.bdr_tot_len,  bli_gpu.bdr_tot_len );

        maxerr = 0.0;
        for(size_t k=0;k<len_right;++k)
            maxerr = max(maxerr,fabs(cpu_ref[k]-out_from_gpu_r[k]));
        BOOST_CHECK_SMALL( maxerr, machine_prec );


    }
}


BOOST_AUTO_TEST_CASE( pack_bdry_fine_grid_to_coarse_grid_gpu ){
    //compares cpu results to gpu results
    //cpu results are checked by the test 
    //"pack_bdry_fine_grid_to_coarse_grid_cpu"

    check_pack_fine2coarse_gpu_cpu_comparison<1,f0_1>(20, 10, 4);
    check_pack_fine2coarse_gpu_cpu_comparison<3,f0_1>(20, 10, 4);

}



BOOST_AUTO_TEST_CASE( test_unpack_c2f ){

    constexpr size_t dx=1;
    constexpr size_t dv=1;
    constexpr int dim=0;

    auto func = [](fp x, fp v){
        return exp(-v*v*0.5)*(x*x*x-4.0*x*x-x);
    };
    
    int bdr_len = 4;
    int o = 4;

    int Nv = 40;
    int Nx = 47;
    int dof_v = Nv*o;

    int bdr_max_length = dof_v*bdr_len*o;

    int ratio = 4;

    fp v_max = 6.0;
    array_dxdv<fp,dx,dv> a = {0.0,-v_max};
    array_dxdv<fp,dx,dv> b = {0.0, v_max};
    index_dxdv_no<dx,dv> e = {Nx,Nv,o,o};

    vp_block_cpu<dx,dv,f0_1> block_cpu(a,b,e,bdr_max_length,false,ratio);
    vector<fp>& bdry_data = block_cpu.bdr_data;

    fp hx = 1.0;
    fp hv = 2.0*v_max/fp(dof_v);
    int out_offset = ratio*bdr_max_length;
    //std::ofstream input("in.data");
    for(int v_idx=0;v_idx<dof_v;++v_idx){
        fp v = a[1]+hv*v_idx;
        for(int bl=0;bl<bdr_len;++bl){
            for(int j=0;j<o;++j){
                int idx = j+o*bl+o*bdr_len*v_idx;
                fp x = a[0]+hx*(bl + gauss::x_scaled01(j,o));
                bdry_data[idx+out_offset] = func(x,v);
                //input << x << " " << v << " " << bdry_data[idx+out_offset] << endl;
            }
        }
    }

    block_cpu.bdr_len = bdr_len*ratio;
    block_cpu.unpack_boundary_coarse2fine(bdr_max_length);

    fp max_err_cpu = 0.0;
    hx = hx/fp(ratio);
    for(int v_idx=0;v_idx<dof_v;++v_idx){
        fp v = a[1]+hv*v_idx;
        for(int bl=0;bl<bdr_len;++bl){
            for(int r=0;r<ratio;++r){
                for(int j=0;j<o;++j){

                    int ridx = r;
                    if(v>=0)
                        ridx = ratio-1-r;

                    int idx = j+o*(ridx+ratio*(bl+bdr_len*v_idx));
                    fp x = a[0]+hx*(r+ratio*bl + gauss::x_scaled01(j,o));

                    fp err = bdry_data[idx] - func(x,v);
                    max_err_cpu = max(max_err_cpu,abs(err));

                }
            }
        }
    }
    BOOST_CHECK_LT( max_err_cpu, 10*machine_prec );

    pack_boundary_from_gpu<dx,dv> pfgpu(e,bdr_max_length);

    sign_gpu<dv> sign_gpu(e.v_part());
    sign_gpu.compute(block_cpu.v_values[0]);
    sign_gpu.copy_to_gpu();
    gpu_array<fp> d_bdry_data(bdr_max_length*(ratio+1));
    std::copy_n(bdry_data.data() + out_offset, bdr_max_length,
                d_bdry_data.h_data.data() + out_offset);

    d_bdry_data.copy_to_gpu();
    pfgpu.unpack_coarse2fine(dim,d_bdry_data.d_data,bdr_len,bdr_len*ratio,ratio,
                             sign_gpu.d_sign,bdr_max_length,sign_gpu.max_idx_left);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    d_bdry_data.copy_to_host();

    fp maxerr_comparison_gpu_cpu = 0.0;
    for(int i=0;i<bdr_max_length*ratio;++i){
        fp err = abs(d_bdry_data[i] - bdry_data[i]);
        maxerr_comparison_gpu_cpu = max(maxerr_comparison_gpu_cpu,err);
    }

    BOOST_CHECK_LT( maxerr_comparison_gpu_cpu, 6*machine_prec );
}




