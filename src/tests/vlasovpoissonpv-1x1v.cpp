#include <generic/common.hpp>
#include <programs/vlasovpoissonpv.hpp>

#define BOOST_TEST_MAIN VLASOVPOISSONPV
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

bool enable_output = true; //is read in domain.hpp

#ifdef USE_SINGLE_PRECISION
float machine_prec = 1e-7;
#else
double machine_prec = 1e-16;
#endif

#ifdef __CUDACC__
__host__ __device__
#endif
fp weak_landau(fp xv[2]) {
    fp x=xv[0]; fp v=xv[1];
    return fp(1/sqrt(2.0*M_PI)*exp(-v*v/2.0)*(1+0.01*cos(0.5*x)));
};
 
BOOST_AUTO_TEST_CASE( vlasovpoissonpv_ll ) {
    bool openmp=false;
    #ifdef _OPENMP
    openmp=true;
    #endif
    mpi_init(openmp);
    mpi_process<2> mpicomm;

    fp tau = 0.1;
    {
        bool gpu = false;
        #ifdef __CUDACC__
        gpu = true;
        #endif
        array<Index,2> num_nodes = {1,2};
        array<Index,1> block_grid = {1};
        int nGPU=2;
        if(mpicomm.total_nodes==1){
            num_nodes = {1,1};
            nGPU=1;
        }

        vlasovpoisson<1,1,weak_landau> vp({0.0,-6.0},{4.0*M_PI,6.0},
                {18,19,3,3},fp(10.0),tau,mpicomm,2,gpu,
                num_nodes, {bdry_cond::periodic},fp(10.0), fp(1e-6), 
                Index(70), true, nGPU, block_grid);
        vp.run();
    } // dtor of vp is called here (before MPI_Finalize)

    if(mpicomm.rank == 0) {
        // read the input file and see if the results are good
        std::ifstream fs("evolution.data");
        fp mass0, mom0, ee0, ignore;
        string s; getline(fs, s); // ignore header
        getline(fs, s); std::istringstream iss(s);
        iss >> ignore >> mass0 >> mom0 >> ignore >>  ignore >> ee0;

        array<fp,3> ee;
        vector<fp> t_max, ee_max;
        for(Index i=0;;i++) {
            fp t, mass, mom;
            getline(fs, s); std::istringstream iss(s);
            if(fs.eof()) {
                if(i<=10) {
                    cout << "ERROR: not enough entries in output file." << endl;
                    exit(1);
                }

                break;
            }
            iss >> t >> mass >> mom >> ignore >>  ignore >> ee[i%3];

            BOOST_CHECK_SMALL( fp(fabs(mass-mass0)/mass0), fp(2e6*machine_prec) );
            BOOST_CHECK_SMALL( fp(fabs(mom-mom0)), fp(5e5*machine_prec) );

            if(i>3) {
                int j = i%3;
                if(ee[j] < ee[neg_modulo(j-1,3)] && 
                        ee[neg_modulo(j-2,3)] < ee[neg_modulo(j-1,3)]) {
                    t_max.push_back(t-tau);
                    ee_max.push_back(ee[neg_modulo(j-1,3)]);
                }
            }

        }

        // make sure enough gamma are available
        BOOST_CHECK( t_max.size() >= 4);

        for(size_t k=2;k<t_max.size();k++) {
            fp gamma = -0.5*(log(ee_max[k])-log(ee_max[0]))/(t_max[k]-t_max[0]);
            BOOST_CHECK_SMALL( abs(gamma-0.153), 3e-3);
            cout << "gamma[" << k << "]=" << gamma << endl;
        }
    }

    mpi_destroy();
}

