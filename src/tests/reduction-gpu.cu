#include <generic/common.hpp>

#define BOOST_TEST_MAIN DG_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg-gpu.cu>
#include <algorithms/rho.hpp>
#include <algorithms/rho-gpu.cu>

#include <programs/vp-block.hpp>
#include <generic/common.hpp>

#ifdef USE_SINGLE_PRECISION
float machine_prec = 1e-6;
#else
double machine_prec = 1e-15;
#endif

bool enable_output; //is read in domain.hpp



template<size_t dx, size_t dv>
fp iv_strong_landau(array<fp,dx+dv> xv){
 
    constexpr size_t d = dx+dv;

    fp x_part = 1.0;
    for(size_t i=0;i<dx;++i)
        x_part += 1.0/fp(dv)*cos(0.5*xv[i]); 

    fp v_part = 1.0;
    for(size_t i=dx;i<d;++i)
        v_part *= exp(-xv[i]*xv[i]/2.0);

    return 1.0/pow(2.0*M_PI,fp(dv)/2.0)*v_part*x_part;
}


template<size_t dx, size_t dv>
fp check_reduction_ri(int N, int o) {

    constexpr size_t d = dx+dv;
    
    // set up f
    array<Index,2*d> e;
    array<Index,2*dx> e_x;
    array<fp,d> a, b;
    for(size_t k=0;k<dx;++k){
        a[k] = 0.0;
        b[k] = 2*M_PI;
        e[k] = N;
        e[k+d] = o;
        e_x[k] = N;
        e_x[k+dx] = o;
    }
    for(size_t k=dx;k<d;++k){
        a[k] = -6.0;
        b[k] =  6.0;
        e[k] = N;
        e[k+d] = o;
    }
    cout << "e: " << e << ", e_x: " << e_x << endl;
 
    domain<d> f(e, a, b);
    f.init(iv_strong_landau<dx,dv>);    

    multi_array<fp,2*d> d_f(e, true);
    d_f = f.data;

    multi_array<fp,2*dx> d_rho{e_x,true};
    multi_array<fp,2*dx> rho_from_gpu{e_x};

    cout << "prod(e_x): " << prod(e_x) << endl;

    // compute on GPU
    fp mass, l1, l2, entropy, K_energy, momentum;
    #ifdef USE_SINGLE_PRECISION
    fp tol_discr = 7e-4;
    #else
    fp tol_discr = 1e-5;
    #endif

    for(int num_blocks=200;num_blocks<1100;num_blocks+=200){
        reduction_on_gpu<dx,dv> crgpu(e,a,b,num_blocks);
        gpuErrchk( cudaPeekAtLastError() );

        fp dummy;
        crgpu.compute_rho_and_invariants(d_f, d_rho, mass, l1, l2, 
                                         entropy, K_energy, momentum,
                                         dummy,dummy,dummy,dummy);

        gpuErrchk( cudaMemcpy(rho_from_gpu.data(), d_rho.data(), 
                              sizeof(fp)*prod(e_x), cudaMemcpyDeviceToHost) );
        
        cout << "GPU: \n";
        cout << "mass " <<  mass << " l1 " << l1 << " l2 " << l2;
        cout << " K_energy " << K_energy << " momentum " << momentum << "\n";
        if(dx==1 && dv==3) {
            BOOST_CHECK_SMALL( (fp)abs(mass-2.0*M_PI), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l1-2.0*M_PI), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l2-19.0/72.0/sqrt(M_PI)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(K_energy-3.0*M_PI), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(momentum-0.0), tol_discr );
    
        }
        if(dx==2 && dv==2) {
            BOOST_CHECK_SMALL( (fp)abs(mass-pow(2.0*M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l1-pow(2.0*M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l2-5.0*M_PI/4.0), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(K_energy-4*pow(M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(momentum-0.0), tol_discr );
        }
        if(dx==2 && dv==3) {
            BOOST_CHECK_SMALL( (fp)abs(mass-pow(2.0*M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l1-pow(2.0*M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l2-5.0*sqrt(M_PI)/9.0), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(K_energy-6*pow(M_PI,2)), tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(momentum-0.0), tol_discr );
        } 
        if(dx==3 && dv==3) {
            fp exactl1 = pow(2.0*M_PI,3.0); //mass = l1 since f>0
            fp exactl2 = 7.0/6.0*pow(M_PI,(3.0/2.0));
            fp exactK_energy = 12*pow(M_PI,3);
            BOOST_CHECK_SMALL( (fp)abs(mass-exactl1)/exactl1, tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l1-exactl1)/exactl1, tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(l2-exactl2)/exactl2, fp(10.0)*tol_discr ); 
            BOOST_CHECK_SMALL( (fp)abs(K_energy-exactK_energy)/exactK_energy, 
                               tol_discr );
            BOOST_CHECK_SMALL( (fp)abs(momentum-0.0), tol_discr );
        }
    }

    //omp_set_num_threads(1);
    int omp_max_thr = 1;
    #ifdef _OPENMP
    omp_max_thr = omp_get_max_threads();
    #endif
    vector<multi_array<fp,2*dx>> rho(omp_max_thr);
    for(size_t i=0;i<rho.size();i++) {
        rho[i].resize(e_x);
        fill(rho[i].data(), rho[i].data()+rho[i].num_elements(), 0.0);
    }
    fp mass2=0.0; fp momentum2=0.0; fp l12=0.0; fp l22=0.0; fp K_energy2=0.0;
    fp entropy2=0.0;
    timer t1; t1.start();
    fp dummy;
    compute_invariants_and_rho_omp<dx,dv>(f, rho, mass2, momentum2, l12,
            l22, K_energy2, entropy2, dummy, dummy, dummy, dummy);
    t1.stop();
    cout << "compute_rho_cpu " << d << " dim: " << t1.total() << endl;
    // do the reduction for rho
    for(size_t k=1;k<rho.size();k++) {
        for(Index i=0;i<rho[0].num_elements();i++)
            rho[0].v[i] += rho[k].v[i];
    }

    cout << "CPU: \n";
    cout << "mass " <<  mass2 << " l1 " << l12 << " l2 " << l22;
    cout << " K_energy " << K_energy2 << " momentum " << momentum2 << "\n";
    if(dx==2 && dv==2){
        BOOST_CHECK_SMALL( (fp)abs(mass2-pow(2.0*M_PI,2)), tol_discr);
        BOOST_CHECK_SMALL( (fp)abs(l12-pow(2.0*M_PI,2)), tol_discr );
        BOOST_CHECK_SMALL( (fp)abs(l22-5.0*M_PI/4.0), tol_discr );
        BOOST_CHECK_SMALL( (fp)abs(K_energy2-4*pow(M_PI,2)), tol_discr );
        BOOST_CHECK_SMALL( (fp)abs(momentum2-0.0), tol_discr );
    }

    #ifdef USE_SINGLE_PRECISION
    fp tol_comp = 7e-4;
    #else
    fp tol_comp = 5e-8;
    #endif

    cout << "(fp)abs(): \n";
    cout << "mass " <<  (fp)abs(mass-mass2) << " l1 " 
         << (fp)abs(l1-l12) << " l2 " << (fp)abs(l2-l22);
    cout << " K_energy " << (fp)abs(K_energy-K_energy2) 
         << " momentum " << (fp)abs(momentum-momentum2) << "\n";
    BOOST_CHECK_SMALL( (fp)abs(mass-mass2), tol_comp );
    BOOST_CHECK_SMALL( (fp)abs(l1-l12), tol_comp );
    BOOST_CHECK_SMALL( (fp)abs(l2-l22), tol_comp );
    BOOST_CHECK_SMALL( (fp)abs(K_energy-K_energy2), tol_comp );
    BOOST_CHECK_SMALL( (fp)abs(momentum-momentum2), tol_comp );

    return inf_norm<dx>(rho[0], rho_from_gpu);
}


//since BOOST_CHECK_SMALL is defined as a macro function,
//we have to use this kind of functions to avoid commas 
template<size_t dx>
fp check_reduction_ri_v1o4(int N){
    cout << "\n\ndx=" << dx << ", dv=1, rho and invariants\n\n";
    return check_reduction_ri<dx,1>(N,4);
}

template<size_t dx>
fp check_reduction_ri_v2o4(int N){
    cout << "\n\ndx=" << dx << ", dv=2, rho and invariants\n\n";
    return check_reduction_ri<dx,2>(N,4);
}

template<size_t dx>
fp check_reduction_ri_v3o4(int N){
    cout << "\n\ndx=" << dx << ", dv=3, rho and invariants\n\n";
    return check_reduction_ri<dx,3>(N,4);
}

BOOST_AUTO_TEST_CASE( reduction_rho_and_invariants_gpu ) {
    
    BOOST_CHECK_SMALL( check_reduction_ri_v1o4<3>(25), fp(100.0*machine_prec) );
    BOOST_CHECK_SMALL( check_reduction_ri_v2o4<2>(25), fp(100.0*machine_prec) );
    BOOST_CHECK_SMALL( check_reduction_ri_v3o4<1>(25), fp(200.0*machine_prec) );
    BOOST_CHECK_SMALL( check_reduction_ri_v3o4<2>(10), fp(100.0*machine_prec) );

  //  BOOST_CHECK_SMALL( check_reduction_v2o4<2>(36), fp(100.0*machine_prec) );
  //  BOOST_CHECK_SMALL( check_reduction_v3o4<1>(36), fp(100.0*machine_prec) );
  //  //BOOST_CHECK_SMALL( check_reduction_v3<2>(20,2), fp(100.0*machine_prec) );
  //  //BOOST_CHECK_SMALL( check_reduction_v3o4<2>(10), fp(100.0*machine_prec) );
  //  BOOST_CHECK_SMALL( check_reduction_v3o4<2>(14), fp(100.0*machine_prec) );
  //  BOOST_CHECK_SMALL( check_reduction_v3o4<3>(8), fp(100.0*machine_prec) );

}


template<size_t dv>
fp flux_test_f(array<fp,1+dv> xv){
 
    fp x = xv[0];
    fp v1 = xv[1];

    fp vsq = 0.0;
    for(size_t i=1;i<dv;++i)
        vsq += xv[1+i]*xv[1+i];

    //return exp(-pow(x-5.0*v1,2)-v1*v1 - vsq);
    return exp(-2.0*pow(x-2.0*v1,2)-x*x/20.0 - vsq);
}
fp flux_test_rho_exact(fp x){
    return 0.5*exp(-x*x/20.0)*sqrt(M_PI/2.0);
}
fp flux_test_u_exact(fp x){
    return 0.5*x;
}
fp flux_test_T_exact(fp UNUSED(x)){
   return 1.0/16.0;
}
fp flux_test_energy_exact(fp x){
    return 1./128.*exp(-x*x/20.0)*sqrt(M_PI/2.0)*x*(3.0+4*x*x);
    //exp(-x*x/20)*pi^(3/2)*x*(19+4*x*x)/128/sqrt(2)
}

//runs with dx=1, dv=1 
//TODO: extend it to dv=3, formula for energy is given, 
//the others change only slightly
template<size_t dv>
fp check_reduction_uT(int N, int o) {

    //currently dx is fixed to one
    constexpr size_t dx = 1;
    constexpr size_t d = dx+dv;
    
    // set up f
    array<Index,2*d> e;
    array<Index,2*dx> e_x;
    array<fp,d> a, b;
    for(size_t k=0;k<dx;++k){
        a[k] = -5.0;
        b[k] =  5.0;
        e[k] = N;
        e[k+d] = o;
        e_x[k] = N;
        e_x[k+dx] = o;
    }
    for(size_t k=dx;k<d;++k){
        a[k] = -5.0;
        b[k] =  5.0;
        e[k] = N;
        e[k+d] = o;
    }
    cout << "e: " << e << ", e_x: " << e_x << endl;
 
    fp tol_comp;
    if(dv==1)
        tol_comp = 2e-4;
    if(dv==3)
        tol_comp = 3e-3;

    domain<d> f(e, a, b);
    f.init(flux_test_f<dv>);    

    multi_array<fp,2*d> d_f(e, true);
    d_f = f.data;

    std::ofstream out("f.data");
    array<Index,4> iidx = {0};

    fp hx = (b[0]-a[0])/fp(e[0]);
    fp hv = (b[1]-a[1])/fp(e[1]);
    do{

        fp x = a[0] + hx*(iidx[0]+gauss::x_scaled01(iidx[d+0],e[d+0]));
        fp v = a[1] + hv*(iidx[1]+gauss::x_scaled01(iidx[d+1],e[d+1]));

        out << x << " " << v << " " << f.data(iidx) << endl;;

    } while(iter_next_memorder(iidx,e));

    fp xl = a[0] + hx*(gauss::x_scaled01(0,e[d]));
    fp xr = b[0] - hx*(gauss::x_scaled01(0,e[d]));
    fp exact_pfluxl = flux_test_u_exact(xl)*flux_test_rho_exact(xl);
    fp exact_pfluxr = flux_test_u_exact(xr)*flux_test_rho_exact(xr);
    fp exact_efluxl = flux_test_energy_exact(xl);
    fp exact_efluxr = flux_test_energy_exact(xr);

    multi_array<fp,2*dx> d_rho{e_x,true};
    multi_array<fp,2*dx> rho_from_gpu{e_x};
    multi_array<fp,2*dx> d_u{e_x,true};
    multi_array<fp,2*dx> u_from_gpu{e_x};
    multi_array<fp,2*dx> d_T{e_x,true};
    multi_array<fp,2*dx> T_from_gpu{e_x};

    cout << "prod(e_x): " << prod(e_x) << endl;

    // compute on GPU
    fp particle_flux_left_gpu, particle_flux_right_gpu;
    fp energy_flux_left_gpu, energy_flux_right_gpu;
    for(int num_blocks=200;num_blocks<1100;num_blocks+=200){
        
        reduction_on_gpu<dx,dv> crgpu(e,a,b,num_blocks);
        gpuErrchk( cudaPeekAtLastError() );

        fp dummy;
        crgpu.compute_rho_and_invariants(d_f, d_rho, dummy, dummy, 
                                         dummy, dummy, dummy, dummy,
                                         particle_flux_left_gpu, 
                                         particle_flux_right_gpu, 
                                         energy_flux_left_gpu,
                                         energy_flux_right_gpu);

        crgpu.compute_meanvelocity(d_f, d_rho, d_u);

        crgpu.compute_temperature(d_f, d_rho, d_u, d_T);


        gpuErrchk( cudaMemcpy(rho_from_gpu.data(), d_rho.data(), 
                              sizeof(fp)*prod(e_x), cudaMemcpyDeviceToHost) );
        gpuErrchk( cudaMemcpy(u_from_gpu.data(), d_u.data(), 
                              sizeof(fp)*prod(e_x), cudaMemcpyDeviceToHost) );
        gpuErrchk( cudaMemcpy(T_from_gpu.data(), d_T.data(), 
                              sizeof(fp)*prod(e_x), cudaMemcpyDeviceToHost) );

        std::ofstream reductiondata("reductiondata.data");

        for(int i=0;i<e[0];++i){
            for(int j=0;j<e[d];++j){
                fp x = a[0] + (b[0]-a[0])/fp(e[0])*
                       (i + gauss::x_scaled01(j,e[d]));
                reductiondata << x << " " << rho_from_gpu.v[j+i*o] << " " 
                              << u_from_gpu.v[j+i*o] << " " 
                              << T_from_gpu.v[j+i*o] << endl;
            }
        }
        
        cout << "GPU: \n";
        cout << "particle flux left: " << particle_flux_left_gpu << " "
             << "energy flux left: " << energy_flux_left_gpu << " "
             << "particle flux right: " << particle_flux_right_gpu << " "
             << "energy flux right: " << energy_flux_right_gpu << "\n";
 
        BOOST_CHECK_SMALL( abs(particle_flux_left_gpu-exact_pfluxl), machine_prec);
        BOOST_CHECK_SMALL( abs(energy_flux_left_gpu-exact_efluxl), machine_prec);
        BOOST_CHECK_SMALL( abs(particle_flux_right_gpu-exact_pfluxr), machine_prec);
        BOOST_CHECK_SMALL( abs(energy_flux_right_gpu-exact_efluxr), machine_prec);

        fp maxerr_rho_gpu = 0.0;
        fp maxerr_u_gpu = 0.0;
        fp maxerr_T_gpu = 0.0;
        array<Index,2*dx> idx = {0};
        fp hx = (b[0]-a[0])/fp(e[0]);
        do{
            fp x = a[0] + hx*(idx[0] + gauss::x_scaled01(idx[dx],e_x[dx]));

            fp ref_rho = flux_test_rho_exact(x);
            fp ref_u = flux_test_u_exact(x); 
            fp ref_T = flux_test_T_exact(x);

            fp err_rho = ref_rho - rho_from_gpu(idx);
            fp err_u = ref_u - u_from_gpu(idx);
            fp err_T = ref_T - T_from_gpu(idx);
            
            maxerr_rho_gpu = max(maxerr_rho_gpu, abs(err_rho));
            maxerr_u_gpu = max(maxerr_u_gpu, abs(err_u));
            maxerr_T_gpu = max(maxerr_T_gpu, abs(err_T));

        } while(iter_next_memorder(idx,e_x));

        BOOST_CHECK_SMALL( maxerr_rho_gpu, machine_prec );
        BOOST_CHECK_SMALL( maxerr_u_gpu, 2000*machine_prec );
        BOOST_CHECK_SMALL( maxerr_T_gpu, 40*machine_prec );
    }

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    //Reduction on CPU
    int omp_max_thr = 1;
    #ifdef _OPENMP
    omp_max_thr = omp_get_max_threads();
    #endif
    vector<multi_array<fp,2*dx>> rho(omp_max_thr);
    vector<multi_array<fp,2*dx>> u(omp_max_thr);
    vector<multi_array<fp,2*dx>> T(omp_max_thr);
    for(size_t i=0;i<rho.size();i++) {
        rho[i].resize(e_x);
        u[i].resize(e_x);
        T[i].resize(e_x);
    }

    fp particle_flux_left_cpu, particle_flux_right_cpu;
    fp energy_flux_left_cpu, energy_flux_right_cpu;

    timer t1; t1.start();
    fp dummy;
    compute_invariants_and_rho_omp<dx,dv>(f,
        rho, dummy, dummy, dummy, dummy, dummy,
        dummy,particle_flux_left_cpu, particle_flux_right_cpu,
        energy_flux_left_cpu, energy_flux_right_cpu);
    for(size_t k=1;k<rho.size();k++) {
        for(Index i=0;i<rho[0].num_elements();i++){
            rho[0].v[i] += rho[k].v[i];
        }
    }

    compute_mean_velocity<dx,dv>(f, rho, u);
    for(size_t k=1;k<u.size();k++) {
        for(Index i=0;i<u[0].num_elements();i++){
            u[0].v[i] += u[k].v[i];
        }
    }

    compute_temperature<dx,dv>(f, rho, u, T);
    for(size_t k=1;k<T.size();k++) {
        for(Index i=0;i<T[0].num_elements();i++){
            T[0].v[i] += T[k].v[i];
        }
    }


    t1.stop();

    cout << "CPU: \n";
    cout << "particle flux left: " << particle_flux_left_cpu << " "
         << "energy flux left: " << energy_flux_left_cpu << " "
         << "particle flux right: " << particle_flux_right_cpu << " "
         << "energy flux right: " << energy_flux_right_cpu << "\n";
 
    BOOST_CHECK_SMALL( abs(particle_flux_left_cpu-exact_pfluxl),  machine_prec);
    BOOST_CHECK_SMALL( abs(energy_flux_left_cpu-exact_efluxl),    machine_prec);
    BOOST_CHECK_SMALL( abs(particle_flux_right_cpu-exact_pfluxr), machine_prec);
    BOOST_CHECK_SMALL( abs(energy_flux_right_cpu-exact_efluxr),   machine_prec);

    fp maxerr_rho_cpu = 0.0;
    fp maxerr_u_cpu = 0.0;
    fp maxerr_T_cpu = 0.0;
    array<Index,2*dx> idx = {0};
    std::ofstream out_mom("mom.data");
    do{
        fp x = a[0] + hx*(idx[0] + gauss::x_scaled01(idx[dx],e_x[dx]));

        fp ref_rho = flux_test_rho_exact(x);
        fp ref_u = flux_test_u_exact(x);
        fp ref_T = flux_test_T_exact(x);

        fp err_rho = ref_rho - rho[0](idx);
        fp err_u = ref_u - u[0](idx);
        fp err_T = ref_T - T[0](idx);
        
        maxerr_rho_cpu = max(maxerr_rho_cpu, abs(err_rho));
        maxerr_u_cpu = max(maxerr_u_cpu, abs(err_u));
        maxerr_T_cpu = max(maxerr_T_cpu, abs(err_T));

        out_mom << x << " " << ref_T << " " << T[0](idx) << endl;

    } while(iter_next_memorder(idx,e_x));

    BOOST_CHECK_SMALL( maxerr_rho_cpu, machine_prec );
    BOOST_CHECK_SMALL( maxerr_u_cpu, 2000*machine_prec );
    BOOST_CHECK_SMALL( maxerr_T_cpu, 40*machine_prec );

    //CPU-GPU comparison
    BOOST_CHECK_SMALL( abs(particle_flux_left_gpu - particle_flux_left_cpu),
                       20*machine_prec);
    BOOST_CHECK_SMALL( abs(energy_flux_left_gpu-energy_flux_left_cpu), 
                       20*machine_prec);
    BOOST_CHECK_SMALL( abs(particle_flux_right_gpu-particle_flux_right_cpu),
                       20*machine_prec);
    BOOST_CHECK_SMALL( abs(energy_flux_right_gpu-energy_flux_right_cpu),
                       20*machine_prec);

    //fluxes integrated over outflow region compared only between cpu and gpu,
    //they are not compared to analytical solutions yet
    BOOST_CHECK_SMALL( abs(particle_flux_left_gpu-particle_flux_left_cpu),
                       20*machine_prec);
    BOOST_CHECK_SMALL( abs(particle_flux_right_gpu-particle_flux_right_cpu),
                       20*machine_prec);

    BOOST_CHECK_SMALL( inf_norm<dx>(u[0], u_from_gpu), 1440*machine_prec );
    BOOST_CHECK_SMALL( inf_norm<dx>(T[0], T_from_gpu), 300*machine_prec );

    return inf_norm<dx>(rho[0], rho_from_gpu);
}



template<size_t dv>
fp check_reduction_uT_dx1_o4(int N){
    cout << "\ncheck reduction moments and fluxes dv=" << dv << "\n";
    return check_reduction_uT<dv>(N,4);
}

BOOST_AUTO_TEST_CASE( reduction_meanvelocity_temperature_fluxes ) {
    
    BOOST_CHECK_SMALL( check_reduction_uT_dx1_o4<1>(100), fp(100.0*machine_prec) );
    //BOOST_CHECK_SMALL( check_reduction_uT_dx1_o4<3>(25), fp(200.0*machine_prec) );

}

