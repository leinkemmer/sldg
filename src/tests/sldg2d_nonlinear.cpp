#include "poisson_translate.hpp"
#include <algorithms/sldg2d/sldg2d_legendre_cpu.hpp>
#include <algorithms/sldg2d/sldg2d_lagrange_cpu.hpp>

#define BOOST_TEST_MAIN SLDG2D_NONLINEAR_CPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>


template<size_t o,typename T>
array<double,3> translate_nl(double (*f0)(double,double), double dt, 
                                      double ax, double ay, double bx, double by,
                                      int N, double final_T,
                                      T& poisson, double c, bool divide,
                                      double (*g)(double,double),
                                      basis_type bt){

    size_t Nloc = (bt==basis_type::legendre) ? o*(o+1)/2 : o*o;

    double hx = (bx-ax)/double(N);
    double hy = (by-ay)/double(N);

    generic_container<double> in(N*N*Nloc);
    generic_container<double> out(N*N*Nloc);
    generic_container<double> dg_sol_rkstage(N*N*Nloc);

    double mass0, mass;
    std::pair<double,double> err;
    if(bt==basis_type::legendre){

        init_legendre<o>(f0,N,N,ax,ay,hx,hy,in.data(false));
        std::ofstream output("data_out"+std::to_string(0)+".data");
        mass0 = compute_mass_and_write_legendre<o>(ax, ay, hx, hy, N, N,
                                          in.data(false), output, 2);

        sldg2d_legendre_cpu<o> dg2d{ax,ay,bx,by,N,N};
        dg2d.translate_nl(in, out, dg_sol_rkstage, poisson,final_T,dt,
                          c,divide,g,mass0);

        std::ofstream outputend("data_outend.data");
        mass = compute_mass_and_write_legendre<o>(ax,ay,hx,hy,N,N,
                                                out.data(false),
                                                outputend,2);

        err = compute_errors_legendre<o>(f0,N,N,ax,bx,ay,by,out.data(false));
    }
    if(bt==basis_type::lagrange){

        init_lagrange<o>(f0,N,N,ax,ay,hx,hy,in.data(false));
        std::ofstream output("data_out"+std::to_string(0)+".data");
        mass0 = compute_mass_and_write_lagrange<o>(ax, ay, hx, hy, N, N,
                                          in.data(false), output);

        sldg2d_lagrange_cpu<o> dg2d{ax,ay,bx,by,N,N};
        dg2d.translate_nl(in, out, dg_sol_rkstage, poisson,final_T,dt,
                          c,divide,g,mass0);

        std::ofstream outputend("data_outend.data");
        mass = compute_mass_and_write_lagrange<o>(ax,ay,hx,hy,N,N,
                                                out.data(false), outputend);

        err = compute_errors_lagrange<o>(f0,N,N,ax,bx,ay,by,out.data(false));
    }

    return {err.first, err.second, abs(mass-mass0)};
}


template<size_t o>
void stationary_guiding_center_fft(basis_type bt, double factor=0.5){

    auto f0 = [](double x, double y){return -2.0*sin(x)*sin(y);};

    double final_time = 1.0;

    double ax = 0.0;
    double bx = 2.0*M_PI;
    double ay = 0.0;
    double by = 2.0*M_PI;

    double err_tol_l2[5] = {1.57e-2,4.00e-3,1.79e-3,1.02e-3,6.49e-4};
    double err_tol_li[5] = {8.55e-2,2.48e-2,1.16e-2,6.71e-3,4.34e-3};

    double err_tol_l2_lag[5] = {0.0069,0.0016,0.00066,0.00043,0.00025};
    double err_tol_li_lag[5] = {0.028,0.0082,0.0039,0.0023,0.0015};

    double errold_l2=-1;
    double errold_linf=-1;
    int iter=0;
    for(int N=20;N<101;N+=20){

        poisson_fft<o> poisson{N,N,ax,ay,bx,by};

        double dt = factor*(bx-ax)/double(N);
        auto err =  translate_nl<o>(f0, dt, ax, ay, bx, by,
                                    N, final_time, 
                                    poisson,0.0,false,f0,bt);

        cout << "N: " << N << ", masserr: " << err[2] << ", ";
        cout << "err l2: " << err[0]
             << "err linf: " << err[1] << endl;

        if(bt==basis_type::legendre){
            BOOST_CHECK(err[0]<err_tol_l2[iter]);
            BOOST_CHECK(err[1]<err_tol_li[iter]);
        }
        if(bt==basis_type::lagrange){
            BOOST_CHECK(err[0]<err_tol_l2_lag[iter]);
            BOOST_CHECK(err[1]<err_tol_li_lag[iter]);
        }
        if(errold_l2>0){
            double denom = log(double(N)/double(N-20));
            cout << "order: " << -log(err[0]/errold_l2)/denom << ", " 
                 << -log(err[1]/errold_linf)/denom << endl;
        }
        errold_l2 = err[0];
        errold_linf = err[1];
        iter++;
    }

}


BOOST_AUTO_TEST_CASE( sldg2d_nonlinear ){

    cout << "\nLEGENDRE, explicit trapezoidal characteristics tracing\n";
    stationary_guiding_center_fft<2>(basis_type::legendre);
    cout << "\nLAGRANGE, explicit midpoint characteristics tracing\n";
    stationary_guiding_center_fft<2>(basis_type::lagrange);
}


BOOST_AUTO_TEST_CASE( poisson_for_sldg2d ){

    cout << "\nPoisson for sldg2d test" << endl;
    array<double,5> err_leg = test_poisson_fft_legendre();
    BOOST_CHECK( err_leg[0] < 0.00052 );
    BOOST_CHECK( err_leg[1] < 2.2e-07 );
    BOOST_CHECK( err_leg[2] < 2.2e-07 );
    BOOST_CHECK( err_leg[3] < 0.00026 );
    BOOST_CHECK( err_leg[4] < 0.0 );
    array<double,6> err_lag = test_poisson_fft_lagrange();
    BOOST_CHECK( err_lag[0] < 0.00052 );
    BOOST_CHECK( err_lag[1] < 5.8e-08 );
    BOOST_CHECK( err_lag[2] < 5.8e-08 );
    BOOST_CHECK( err_lag[3] < 0.00026 );
    BOOST_CHECK( err_lag[4] < 0.00026 );
    BOOST_CHECK( err_lag[5] < 0.0 );
 
}

//template<size_t o>
//void gc_perturb(double factor=0.5, 
//                basis_type bt=basis_type::legendre){
//
//    auto f0 = [](double x, double y){return 1.0 - 1e-7*2.0*sin(x)*sin(y);};
//
//    double final_time = 200.0;
//
//    double ax = 0.0;
//    double bx = 2.0*M_PI;
//    double ay = 0.0;
//    double by = 2.0*M_PI;
//
//    double errold_l2=-1;
//    double errold_linf=-1;
//    int iter=0;
//    for(int N=20;N<21;N+=20){
//        poisson_fft<o> poisson{N,N,ax,ay,bx,by};
//
//        double dt = 1.0 + factor-factor;//(bx-ax)/double(N)*factor;
//        std::pair<double,double> err =  translate_nl<o>(f0, dt, ax, ay, bx, by,
//                                                        N, final_time, 
//                                                        poisson,-1.0,false,f0,bt);
//
//        cout << "err l2: " << err.first
//             << "err linf: " << err.second << endl;
//
//        if(errold_l2>0){
//            double denom = log(double(N)/double(N-20));
//            cout << "order: " << -log(err.first/errold_l2)/denom << ", " 
//                 << -log(err.second/errold_linf)/denom << endl;
//        }
//        errold_l2 = err.first;
//        errold_linf = err.second;
//        iter++;
//    }
//
//}   


