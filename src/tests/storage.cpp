#include <generic/common.hpp>

#define BOOST_TEST_MAIN STORAGE
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

bool enable_output; //is read in domain.hpp

Index factorial(Index k) {
    Index prod = 1;
    for(Index i=1;i<=k;i++)
        prod *= i;
    return prod;
}

template<Index d>
void check_multi_array_storage() {
    array<Index, d> n;
    for(Index i=0;i<d;i++)
        n[i] = 1+i;


    multi_array<fp,d> ma(n);
    BOOST_CHECK( ma.num_elements() == factorial(d) );

    // store some values
    array<fp, 12> factors = {0.7,-3.5,5.0,4.1,7.3,-2.9,2.4,-1.8,5.7,-1.5,2.2,3.7};
    static_assert(d <= 12, "check_multi_array only supports d<=12");
    for(auto i : range<d>(n)) {
        fp val = 0.0;
        for(Index k=0;k<d;k++)
            val += factors[k]*i[k];
        
        ma(i) = val;
    }

    // check that the values are correct
    for(auto i : range<d>(n)) {
        fp val = 0.0;
        for(Index k=0;k<d;k++)
            val += factors[k]*i[k];
            
        BOOST_CHECK_SMALL( fabs(ma(i) - val), 1e-15);
    }
}

BOOST_AUTO_TEST_CASE( multi_array_storage ) {
    check_multi_array_storage<2>();
    check_multi_array_storage<4>();
    check_multi_array_storage<6>();
    check_multi_array_storage<8>();
    //check_multi_array_storage<10>();
    //check_multi_array_storage<12>();
}

template<size_t d>
void check_multi_array_assign_swap() {
    array<Index, d> n;
    for(size_t i=0;i<d;i++)
        n[i] = 1+i;

    multi_array<fp,d> ma1(n);
    multi_array<fp,d> ma2(n);

    for(auto i : range<d>(n)) {
        ma1(i) = 1.0;
        ma2(i) = 2.0;
    }

    // check swap
    ma1.swap(ma2);
    for(auto i : range<d>(n)) {
        BOOST_CHECK_SMALL( abs(ma1(i)-2.0), 1e-15);
        BOOST_CHECK_SMALL( abs(ma2(i)-1.0), 1e-15);
    }

    // check copy constructor
    multi_array<fp,d> ma3 = ma2;
    BOOST_CHECK( error_inf(ma3.shape(), n) == 0 );
    for(auto i : range<d>(n))
        BOOST_CHECK_SMALL( abs(ma3(i)-1.0), 1e-15 );

    // check assignment
    multi_array<fp,d> ma4;
    ma4 = ma1;
    BOOST_CHECK( error_inf(ma4.shape(), n) == 0 );
    for(auto i : range<d>(n))
        BOOST_CHECK_SMALL( abs(ma4(i)-2.0), 1e-15 );
}

BOOST_AUTO_TEST_CASE( multi_array_assign_swap ) {
    check_multi_array_assign_swap<2>();
    check_multi_array_assign_swap<4>();
    check_multi_array_assign_swap<6>();
    check_multi_array_assign_swap<8>();
    //check_multi_array_assign_swap<10>();
    //check_multi_array_assign_swap<12>();
}
