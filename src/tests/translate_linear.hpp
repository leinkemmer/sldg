#pragma once

#include <algorithms/sldg2d/sldg2d_legendre_gpu.hpp>
#include <algorithms/sldg2d/sldg2d_legendre_cpu.hpp>
#include <algorithms/sldg2d/sldg2d_lagrange_cpu.hpp>
#include <algorithms/sldg2d/sldg2d_lagrange_gpu.hpp>

template<size_t o, typename F1, typename F2>
array<fp,3> translate_l(double (*f0)(double,double),
                                     double ax, double ay, double bx, double by,
                                     int N, double dt, double final_T, F1 a, F2 b,
                                     bool gpu, basis_type bt){

    size_t Nloc = (bt==basis_type::legendre) ? o*(o+1)/2 : o*o;

    double hx = (bx-ax)/double(N);
    double hy = (by-ay)/double(N);

    generic_container<double> in(N*N*Nloc);
    generic_container<double> out(N*N*Nloc);

    std::ofstream output_begin("test/data_out"+std::to_string(0)+".data");
    double mass0;
    if(bt==basis_type::legendre){
        init_legendre<o>(f0,N,N,ax,ay,hx,hy,in.data(false));
        mass0 = compute_mass_and_write_legendre<o>(ax, ay, hx, hy, N, N,
                                             in.data(false), output_begin, 2);
    }
    else if(bt==basis_type::lagrange){
        init_lagrange<o>(f0,N,N,ax,ay,hx,hy,in.data(false));
        mass0 = compute_mass_and_write_lagrange<o>(ax, ay, hx, hy, N, N,
                                             in.data(false), output_begin);
    }

    #ifdef __CUDACC__
    if(gpu==true)
        in.copy_to_gpu();
    #endif


    if(gpu==true){
        #ifdef __CUDACC__
        if(bt==basis_type::legendre){
            sldg2d_legendre_gpu<o> dg2d{ax,ay,bx,by,N,N};
            dg2d.translate(in, out, a, b, dt, final_T, 1);
        }
        else if(bt==basis_type::lagrange){
            sldg2d_lagrange_gpu<o> dg2d{ax,ay,bx,by,N,N};
            dg2d.translate(in, out, a, b, dt, final_T, 1);
        }
        #endif
    }
    else if(gpu==false){
        if(bt==basis_type::legendre){
            sldg2d_legendre_cpu<o> dg2d{ax,ay,bx,by,N,N};
            dg2d.translate(in, out, a, b, dt, final_T, 1);
        }
        else if(bt==basis_type::lagrange){
            sldg2d_lagrange_cpu<o> dg2d{ax,ay,bx,by,N,N};
            dg2d.translate(in, out, a, b, dt, final_T, 1);
        }
    }

    #ifdef __CUDACC__
    if(gpu==true)
        out.copy_to_host();
    #endif
    double mass_end;
    std::ofstream output_end("test/data_outend.data");
    std::pair<double,double> err;

    if(bt==basis_type::legendre){
        mass_end = compute_mass_and_write_legendre<o>(ax,ay,hx,hy,N,N,
                                                    out.data(false), output_end, 2);
        err = compute_errors_legendre<o>(f0,N,N,ax,bx,ay,by,out.data(false));
    }
    else if(bt==basis_type::lagrange){
        mass_end = compute_mass_and_write_lagrange<o>(ax,ay,hx,hy,N,N,
                                                    out.data(false), output_end);
        err = compute_errors_lagrange<o>(f0,N,N,ax,bx,ay,by,out.data(false));

    }

    return {err.first,err.second,abs(mass_end-mass0)};
}

struct shift_a{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double UNUSED(y), double UNUSED(t)) {
        return +1+x-x;
    }
};


struct shift_b{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double UNUSED(y), double UNUSED(t)) {
        return +1+x-x;
    }
};


struct rotate_a{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double UNUSED(x), double y, double UNUSED(t)) {
        return -y;
    }
};


struct rotate_b{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double UNUSED(y), double UNUSED(t)) {
        return +x;
    };
};


struct swirl_a{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double y, double t) {
        return -pow(cos(0.5*x),2)*sin(y)*cos(M_PI*t/1.5)*M_PI;
    };
};


struct swirl_b{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double y, double t) {
        return  pow(cos(0.5*y),2)*sin(x)*cos(M_PI*t/1.5)*M_PI;
    };
};


struct perturb_a{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double y, double UNUSED(t)) {
        return  1e-7*sin(x)*cos(y);
    };
};


struct perturb_b{

    #ifdef __CUDACC__ 
    __host__ __device__ 
    #endif
    double operator()(double x, double y, double UNUSED(t)) {
        return -1e-7*cos(x)*sin(y);
    };
};


