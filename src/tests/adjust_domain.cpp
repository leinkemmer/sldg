#include <generic/common.hpp>

#define BOOST_TEST_MAIN ADJUST_DOMAIN_BDRY
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

#include <programs/vp-block-cpu.hpp>
#include <algorithms/adjust_domain.hpp>

bool enable_output = true; //is read in domain.hpp


BOOST_AUTO_TEST_CASE( interp_on_finer_grid ){

    constexpr size_t dx = 1;
    constexpr size_t dv = 1;
    constexpr size_t d = dx+dv;

    array<fp,dx+dv> a = {-6.0,-6.0};
    array<fp,dx+dv> b = { 6.0, 6.0};

    Index N = 50;
    Index o = 4;

    fp hx = (b[0]-a[0])/fp(N);
    fp hv = (b[0]-a[0])/fp(N);

    array<Index,2*(dx+dv)> e = {N,N,o,o};

    multi_array<fp,2*(dx+dv)> in(e);
    multi_array<fp,2*(dx+dv)> out(e);
    multi_array<fp,2*(dx+dv)> ref(e);

    array<Index,2*(dx+dv)> idx = {0};

    do{
        fp x = a[0] + hx*(idx[0] + gauss::x_scaled01(idx[dx+dv+0],o));
        fp v = a[1] + hv*(idx[1] + gauss::x_scaled01(idx[dx+dv+1],o));

        in(idx) = exp(-x*x)*exp(-v*v);

    } while(iter_next_memorder(idx,e));

    array<fp,dv> av_old = {-6.0};
    array<fp,dv> bv_old = { 6.0};
    array<fp,dv> av_new = {-3.0};
    array<fp,dv> bv_new = { 3.0};

    interpolate_on_finer_grid<dx,dv>(av_old,bv_old,av_new,bv_new,e,in,out);

    fp hv_new = (bv_new[0]-av_new[0])/fp(N);

    do{
        fp x = a[0]      + hx*(idx[0] + gauss::x_scaled01(idx[dx+dv+0],o));
        fp v = av_new[0] + hv_new*(idx[1] + gauss::x_scaled01(idx[dx+dv+1],o));
        
        ref(idx) = exp(-x*x)*exp(-v*v);

    } while(iter_next_memorder(idx,e));

    BOOST_CHECK_LT( inf_norm<d>(out,ref), 1e-5); 

}

fp f0(fp* xv){
    fp x = xv[0];
    fp v = xv[1];
    return sin(x)*exp(-v*v);
}


BOOST_AUTO_TEST_CASE( reduce_projection ){

    constexpr size_t dx=1;
    constexpr size_t dv=1;
    constexpr size_t d = dx+dv;

    Index N = 20;
    Index o = 4;

    array_dxdv<fp,dx,dv> a = {-0.0,-6.0};
    array_dxdv<fp,dx,dv> b = { 2.0*M_PI, 6.0};
    array<Index,2*(dx+dv)> e = {N,N,o,o};
    multi_array<fp,2*d> in(e);
    multi_array<fp,2*d> out(e);
    multi_array<fp,2*d> out_interp(e);

    dg_evaluator interp(o);

    array<Index,2*d> idx = {0};
    std::ofstream input("in.data");
    do{
        fp x[d];
        for(size_t i=0;i<d;++i)
            x[i] = a[i] + (b[i]-a[i])/fp(e[i])*
                          (idx[i]+gauss::x_scaled01(idx[d+i],e[d+i]));

        in(idx) = f0(x);

        for(size_t i=0;i<d;++i)
            input << x[i] << " ";
        input << in(idx) << endl;

    } while(iter_next_memorder(idx,e));


    fp remainding_domain_size = 0.5;
    array<fp,dv> av_old; av_old[0] = a[1];
    array<fp,dv> bv_old; bv_old[0] = b[1];

    array<fp,dv> av_new; av_new[0] = av_old[0]*remainding_domain_size;
    array<fp,dv> bv_new; bv_new[0] = bv_old[0]*remainding_domain_size;

    //cout << "aold: " << av_old << ", anew: " << av_new << endl;
    //cout << "bold: " << bv_old << ", bnew: " << bv_new << endl;

    project_on_finer_grid<dx,dv>(av_old, bv_old, av_new, bv_new,
                                 e, remainding_domain_size, in, out);
    interpolate_on_finer_grid<dx,dv>(av_old,bv_old,av_new,bv_new,e,in,out_interp);

    for(size_t i=0;i<dv;++i){
        a[dx+i] = av_new[i];
        b[dx+i] = bv_new[i];
    }
    std::ofstream output("out.data");
    fill(idx.begin(),idx.end(),0);
    fp maxerr = 0.0;
    do{
        fp x[d];
        for(size_t i=0;i<d;++i)
            x[i] = a[i] + (b[i]-a[i])/fp(e[i])*
                          (idx[i]+gauss::x_scaled01(idx[d+i],e[d+i]));

        for(size_t i=0;i<d;++i)
            output << x[i] << " ";

        output << out(idx) << " " << out(idx)-f0(x) << " " 
               << out(idx)-out_interp(idx) << endl;

        maxerr = max(maxerr, abs(out(idx) - f0(x)));

    } while(iter_next_memorder(idx,e));

    BOOST_CHECK_LT( maxerr, 2.7e-4 );

}




BOOST_AUTO_TEST_CASE( check_reduction_with_projections ){

    constexpr size_t dx=1;
    constexpr size_t dv=1;

    Index N = 35;
    Index o = 4;

    array_dxdv<fp,dx,dv> a = {0,-6};
    array_dxdv<fp,dx,dv> b = {2*M_PI, 6};
    index_dxdv_no<dx,dv> e = {N,N,o,o};
    vp_block_cpu<dx,dv,f0> block(a,b,e,0);
    block.init();

    //for(auto e: block.v_values[0])
    //    cout << e << endl;

    //update_v_values<dv>(-5,5,e.v_part(),block.v_values[0],0);

    //for(auto e: block.v_values[0])
    //    cout << e << endl;

    fp mass, mom, l1, l2, ke, tmp;
    block.compute_rho_and_invariants(mass,mom,l1,l2,ke,tmp);

    block.reduce_domain_bounds(0.1);
    block.swap();

    fp mass_n, mom_n, l1_n, l2_n, ke_n, tmp_n;
    block.compute_rho_and_invariants(mass_n,mom_n,l1_n,l2_n,ke_n,tmp_n);

    BOOST_CHECK_SMALL( abs(mass-mass_n), 1e-14);
    BOOST_CHECK_SMALL( abs(mom- mom_n), 1e-14);
    BOOST_CHECK_SMALL( abs(l1-l1_n), 2e-13);
    BOOST_CHECK_SMALL( abs(l2-l2_n), 3e-9);
    BOOST_CHECK_SMALL( abs(ke-ke_n), 2e-7);

}



