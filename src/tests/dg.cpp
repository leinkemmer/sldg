#include <generic/common.hpp>

#define BOOST_TEST_MAIN DG
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

#ifdef USE_SINGLE_PRECISION
double err_limit = 4e-4;
#else
double err_limit = 5e-12;
#endif


bool enable_output; //is read in domain.hpp

fp gaussian(array<fp,1> x) {
    return exp(-15.0*x[0]*x[0]);
}

fp gaussian2d(array<fp,2> x) {
    return exp(-15.0*x[0]*x[0])*exp(-15.0*x[1]*x[1]);
}

fp gaussian3d(array<fp,3> x) {
    return exp(-15.0*x[0]*x[0])*exp(-15.0*x[1]*x[1])*exp(-15.0*x[2]*x[2]);
}

BOOST_AUTO_TEST_CASE( order_1d ) {
    using std::left;
    using std::setw;
    array<fp,6> tol0 = {4.0, 0.07, 8e-4, 4e-5, 3e-6, 3e-7};

    unsigned width = 15; fp last_error=0.0;
    for(int o=1;o<7;o++) {
        cout << "------------------------------------" << endl;
        cout << "1d translation test with o=" << o << endl;
        cout << "------------------------------------" << endl;
        cout << left << setw(width) << "n" << setw(width) << "error (inf)"
             << setw(width) << "order" << endl;
        int initial_n = 32;
        int level = 0;
        for(int n=initial_n;n<=1024;n*=2) {
            array<Index,2> e = {{n,o}};
            array<fp,1> a = {{-1.5}}; array<fp,1> b = {{1.5}};
            domain<1> in(e, a, b); in.init(gaussian);
            domain<1> work = in;
            in.write("in.data");
            domain<1> out(e, a, b);
            fp tau = 1.2; fp tauinside=0.2;
            for(int i=0;i<n*int(1/tauinside);i++) {
                array<Index,0> idx;
                domain<1>::view vwork = slice_array(work.data, 0, idx);
                domain<1>::view vout = slice_array(out.data, 0, idx);
                // set up boundary data (periodic), only works for translation
                // to the right and for tau<2
                vector<fp> bdr(2*o);
                for(size_t i=0;i<bdr.size()/o;i++)
                    for(int j=0;j<o;j++)
                        bdr[o*i+j] = vwork(n-1-i,j);
                // do the translation
                translate1d<1>(vwork, vout, &bdr[0], 2*o, tau, TM_INTERIOR);
                translate1d<1>(vwork, vout, &bdr[0], 2*o, tau, TM_BOUNDARY);
                work = out;
            }
            out.write("out.data");
            fp error=inf_norm<1>(in.data, out.data);
            cout << setw(width) << n << setw(width) << error << setw(width)
                 << ((n==initial_n) ? "-" : to_str(log(last_error/error)/log(2)))
                 << endl;
            last_error=error;

            BOOST_CHECK_SMALL( error,
                    fp(max(tol0[o-1]/pow(2.0,o*level),err_limit)) );
            level++;
        }
    }
}

BOOST_AUTO_TEST_CASE( order_2d ) {
    using std::left;
    using std::setw;
    array<fp,6> tol0 = {0.07, 9e-4, 5e-5, 3e-6, 3e-7};

    unsigned width = 15; fp last_error=0.0;
    int advection_dim=1;
    for(int o=2;o<7;o++) {
        cout << "------------------------------------" << endl;
        cout << "2d translation test with o=" << o << endl;
        cout << "------------------------------------" << endl;
        cout << left << setw(width) << "n" << setw(width) << "error (inf)"
             << setw(width) << "order" << endl;
        int initial_n = 32;
        for(int n=initial_n;n<=128;n*=2) {
            array<Index,4> e = {{n,n+4,o,o}};
            array<fp,2> a = {{-1.5,-1.5}}; array<fp,2> b = {{1.5,1.5}};
            domain<2> in(e, a, b); in.init(gaussian2d);
            domain<2> work = in;
            in.write("in.data");
            domain<2> out(e, a, b);
            fp tau = 1.2; fp tauinside=0.2;
            int level = 0;
            for(int i=0;i<n*int(1/tauinside);i++) {
                // iterate over complementary dimension
                for(int m=0;m<n;m++) {
                    for(int mo=0;mo<o;mo++) {
                        array<Index,2> idx = {{m, mo}};
                        domain<2>::view vwork  = slice_array(work.data,
                                advection_dim, idx);
                        domain<2>::view vout = slice_array(out.data,
                                advection_dim, idx);
                        // set up boundary data (periodic), only works for
                        // translation to the right and for tau<2
                        vector<fp> bdr(2*o);
                        for(size_t i=0;i<bdr.size()/o;i++)
                            for(int j=0;j<o;j++)
                                bdr[o*i+j] = vwork(n-1-i,j);
                        // do the translation
                        translate1d<1>(vwork, vout, &bdr[0], 2*o, tau,
                                TM_INTERIOR);
                        translate1d<1>(vwork, vout, &bdr[0], 2*o, tau,
                                TM_BOUNDARY);
                    }
                }
                work = out;
            }
            out.write("out.data");
            fp error=inf_norm<2>(in.data, out.data);
            cout << setw(width) << n << setw(width) << error << setw(width)
                 << ((n==initial_n) ? "-" : to_str(log(last_error/error)/log(2)))
                 << endl;
            last_error=error;
            
            BOOST_CHECK_SMALL( error,
                    fp(max(tol0[o-2]/pow(2.0,o*level),err_limit)) );
            level++;
        }
    }
}


BOOST_AUTO_TEST_CASE( order_3d ) {
    using std::left;
    using std::setw;
    array<fp,3> tol0 = {0.3, 0.02, 1e-3};

    unsigned width = 15; fp last_error=0.0;
    int advection_dim=1;
    for(int o=2;o<5;o++) {
        cout << "------------------------------------" << endl;
        cout << "3d translation test with o=" << o << endl;
        cout << "------------------------------------" << endl;
        cout << left << setw(width) << "n" << setw(width) << "error (inf)"
             << setw(width) << "order" << endl;
        int initial_n = 16;
        for(int n=initial_n;n<=64;n*=2) {
            array<Index,6> e = {{n,n+5,n+2,o,o,o}};
            array<fp,3> a = {{-1.5,-1.5,-1.5}}; array<fp,3> b = {{1.5,1.5,1.5}};
            domain<3> in(e, a, b); in.init(gaussian3d);
            domain<3> work = in;
            in.write("in.data");
            domain<3> out(e, a, b);
            fp tau = 1.2; fp tauinside=0.2;
            int level = 0;
            for(int i=0;i<n*int(1/tauinside);i++) {
                // iterate over complementary dimensions
                for(int m=0;m<n;m++) {
                    for(int mo=0;mo<o;mo++) {
                        for(int l=0;l<n;l++) {
                            for(int lo=0;lo<o;lo++) {
                                array<Index,4> idx = {{m, l, mo, lo}};
                                domain<3>::view vwork  = 
                                    slice_array(work.data, advection_dim, idx);
                                domain<3>::view vout = 
                                    slice_array(out.data, advection_dim, idx);
                                // set up boundary data (periodic), only works
                                // for translation to the right and for tau<2
                                vector<fp> bdr(2*o);
                                for(size_t i=0;i<bdr.size()/o;i++)
                                    for(int j=0;j<o;j++)
                                        bdr[o*i+j] = vwork(n-1-i,j);
                                // do the translation
                                translate1d<1>(vwork, vout, &bdr[0], 2*o, tau,
                                        TM_INTERIOR);
                                translate1d<1>(vwork, vout, &bdr[0], 2*o, tau,
                                        TM_BOUNDARY);
                            }
                        }
                    }
                }
                work = out;
            }
            out.write("out.data");
            fp error=inf_norm<3>(in.data, out.data);
            cout << setw(width) << n << setw(width) << error << setw(width) 
                 << ((n==initial_n) ? "-" : to_str(log(last_error/error)/log(2)))
                 << endl;
            last_error=error;
            
            BOOST_CHECK_SMALL( error,
                    fp(max(tol0[o-2]/pow(2.0,o*level),err_limit)) );
            level++;
        }
    }
}


/// Determine the order of the discontinuous Galerkin implementation in two 
/// space dimension by comparing it against the known solution for successively
/// finer grids.
/// Cache blocking techniques are employed for the advection in the
/// x-direction.
BOOST_AUTO_TEST_CASE( order_2d_blocked ) {
    using std::left;
    using std::setw;
    array<fp,6> tol0 = {1e-1, 1e-3, 4e-5, 2e-6, 2e-7};

    unsigned width = 15; fp last_error=0.0;
    int advection_dim=1;
    for(int o=2;o<7;o++) {
        cout << "------------------------------------" << endl;
        cout << "2d translation test with o=" << o << " (blocked)" <<  endl;
        cout << "------------------------------------" << endl;
        cout << left << setw(width) << "n" << setw(width) << "error (inf)"
             << setw(width) << "order" << endl;
        int initial_n = 32;
        for(int n=initial_n;n<=128;n*=2) {
            array<Index,4> e = {{n,n,o,o}};
            array<fp,2> a = {{-1.5,-1.5}}; array<fp,2> b = {{1.5,1.5}};
            domain<2> in(e, a, b); in.init(gaussian2d);
            domain<2> work = in;
            in.write("in.data");
            domain<2> out(e, a, b);
            fp tau = 1.2; fp tauinside=0.2;
            int level = 0;
            for(int i=0;i<n*int(1/tauinside);i++) {
                int block_size = min(64,n); int num_blocks = n/block_size;
                // iterate over complementary dimension (blocked)
                for(int b_j=0;b_j<num_blocks;b_j++) {
                    for(int b_m=0;b_m<num_blocks;b_m++) {
                        for(int m=0;m<n;m++) {
                            for(int mo=0;mo<o;mo++) {
                                array<Index,2> idx = {{m, mo}};
                                domain<2>::view vwork  = 
                                    slice_array(work.data, advection_dim, idx);
                                domain<2>::view vout = 
                                    slice_array(out.data, advection_dim, idx);
                                // set up boundary data (periodic), only works
                                // for translation to the right and for tau<2
                                vector<fp> bdr(2*o);
                                for(size_t i=0;i<bdr.size()/o;i++)
                                    for(int j=0;j<o;j++)
                                        bdr[o*i+j] = vwork(n-1-i,j);
                                // do the translation
                                translate1d<1>(vwork, vout, &bdr[0], 2*o, tau,
                                        TM_ALL, b_j, block_size);
                            }
                        }
                    }
                }
                work = out;
            }
            out.write("out.data");
            fp error=inf_norm<2>(in.data, out.data);
            cout << setw(width) << n << setw(width) << error << setw(width)
                 << ((n==initial_n) ? "-" : to_str(log(last_error/error)/log(2)))
                 << endl;
            last_error=error;
            
            BOOST_CHECK_SMALL( error,
                    fp(max(tol0[o-2]/pow(2.0,o*level),err_limit)) );
            level++;
        }
    }
}
