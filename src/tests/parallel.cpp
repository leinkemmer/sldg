/// This file is a collection of different functions that tests and validates
/// the discontinuous Galerkin code for a simple advection problem. In addition,
/// the code found here demonstrates the various functions and how they are
/// used. The file is organized as follows:
/// - test_{1,2,4}_mpi: solves the advection equation using MPI in 1,2, and 4
///   dimensions.
/// - test_{2,4}_hybrid: solves the advection equation using a hybrid OpenMP+MPI
///   implementation.

#include <generic/common.hpp>

#include <boost/program_options.hpp>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

#include <sched.h>

bool enable_output; //is read in domain.hpp

fp gaussian(array<fp,1> x) {
    return exp(-15.0*x[0]*x[0]);
}

fp gaussian2d(array<fp,2> x) {
    return exp(-15.0*x[0]*x[0])*exp(-15.0*x[1]*x[1]);
}

fp gaussian3d(array<fp,3> x) {
    return exp(-15.0*x[0]*x[0])*exp(-15.0*x[1]*x[1])*exp(-15.0*x[2]*x[2]);
}

/// Compute the solution of the advection equation using the discontinuous
/// Galerkin implementation in a single space dimension. Timing for computation
/// and communication overhead is provided. Also this function demonstrates the
/// interleaving of communication and computation.
void test_1d_mpi(int n, int num_steps, int o) {
    mpi_process<1> mpicomm;
    array<Index,1> num_nodes = {{mpicomm.total_nodes}};
    mpicomm.init(num_nodes);

    array<fp,1> a = {{-1.5}}; array<fp,1> b = {{1.5}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes 
      << ". Domain from " << a << " to " << b;
    mpicomm.print(s.str());

    // init initial value
    array<Index,2> e = {{n,o}};
    domain<1> in(e, a, b); in.init(gaussian);
    domain<1> work = in; domain<1> out = in;
    in.write(filename<1>("in",mpicomm.node_id));

    fp alpha=3.2; int bdr_len=int(abs(alpha))+1;
    vector<fp> bdr_to(bdr_len*o); vector<fp> bdr_from(bdr_len*o);

    timer total; total.start();
    timer comm; timer comput;
    for(int i=0;i<num_steps;i++) {
        // set up the necessary boundary data
        comm.start();
        array<Index,0> idx;
        domain<1>::view vwork = slice_array(work.data, 0, idx);
        for(int i=0;i<bdr_len;i++)
            for(int j=0;j<o;j++)
                bdr_to[o*i+j] = vwork(n-1-i,j);

        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Request reqs[2];
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        comput.start();
        domain<1>::view vout = slice_array(out.data, 0, idx);
        translate1d<1>(vwork, vout, &bdr_from[0], bdr_len*o, alpha, TM_INTERIOR);
        comput.stop();

        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        comput.start();
        translate1d<1>(vwork, vout, &bdr_from[0], bdr_len*o, alpha, TM_BOUNDARY);
        work=out;
        comput.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(str(boost::format("Runtime: %1% Commun: %2% Comput: %3%")
                %total.total()%comm.total()%comput.total()));
    out.write(filename<1>("out",mpicomm.node_id));
}


/// Compute the solution of the advection equation using the discontinuous
/// Galerkin implementation in two space dimension. Timing for computation,
/// communication overhead, preparation of the boundary data, and
/// synchronization overhead is provided. In addition, the use of the
/// pack_boundary and unpack_boundary functions are demonstrated and
/// interleaving between communication and computation is used.
void test_2d_mpi(int n, int num_steps, int o, fp alpha_unnormalized) {
    mpi_process<2> mpicomm;
    array<Index,2> num_nodes;
    distribute<2>((Index)mpicomm.total_nodes,num_nodes);
    if(mpicomm.rank==0)
        cout << "Number of MPI processes per dimension: " << num_nodes << endl;
    mpicomm.init(num_nodes);

    array<fp,2> a = {{-1.5,0.0}}; array<fp,2> b = {{1.5,1.0}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes 
      << ". Domain from " << a << " to " << b << ". Rank left: "
      << mpicomm.rank_left(0) << ". Rank right: " << mpicomm.rank_right(0)
      << ".";
    mpicomm.print(s.str());


    // init initial value
    array<Index,4> e = {{n,n,o,o}};
    domain<2> in(e, a, b); in.init(gaussian2d);
    domain<2> work = in; domain<2> out = in;
    in.write(filename<2>("in",mpicomm.node_id));

    // determine the CFL number alpha based on a displacement of 1e-3
    // (approximately alpha=0.5 for 512 grid points) For 4096 cores this gives a
    // CFL number of 32, For 16k cores a CFL number of 64 (which is still inside
    // a single cell)
    fp alpha=-alpha_unnormalized*fp(n)*fp(num_nodes[0]);
    int bdr_len=abs(int(alpha))+1;
    vector<fp> bdr_to(bdr_len*o*n*o); vector<fp> bdr_from(bdr_len*o*n*o);

    timer total;
    timer comm; timer comput; timer prep; timer sync;

    int adv_direction=0;
    sync.start(); MPI_Barrier(MPI_COMM_WORLD); sync.stop();
    total.start();
    for(int i=0;i<num_steps;i++) {
        //sync.start(); MPI_Barrier(MPI_COMM_WORLD); sync.stop();
        // set up the necessary boundary data
        prep.start();
        work.pack_boundary(0,BDRY_RIGHT,bdr_len,bdr_to);
        prep.stop();

        // start sending and receiving the data
        comm.start();
        MPI_Request reqs[2];
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        comput.start();
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork =
                    slice_array(work.data, adv_direction, idx);
                domain<2>::view vout =
                    slice_array(out.data, adv_direction, idx);
                translate1d<1>(vwork, vout, NULL, bdr_len*o, alpha, TM_INTERIOR);
            }
        }
        comput.stop();

        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        comput.start();
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork =
                    slice_array(work.data, adv_direction, idx);
                domain<2>::view vout =
                    slice_array(out.data, adv_direction, idx);
                fp* bdr_from_l=work.unpack_boundary(idx, 0, bdr_len, bdr_from);
                translate1d<1>(vwork, vout, bdr_from_l, bdr_len*o, alpha,
                        TM_BOUNDARY);
            }
        }
        work=out;
        comput.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(str(boost::format("Runtime: %1% Commun: %2% Comput: %3% "
                    "Prep %4% Sync %5%")%total.total()%comm.total()
                %comput.total()%prep.total()%sync.total()));
    out.write(filename<2>("out",mpicomm.node_id));
}

/// Compute the solution of the advection equation using the discontinuous
/// Galerkin implementation in two space dimension. Timing for computation,
/// communication overhead, preparation of the boundary data, and
/// synchronization overhead is provided. In addition, the use of the
/// pack_boundary and unpack_boundary functions are demonstrated and
/// interleaving between communication and computation is used.
/// Cache blocking techniques are employed for the advection in the
/// x-direction.
void test_2d_mpi_blocked(int n, int num_steps, int o, fp alpha_unnormalized) {
    mpi_process<2> mpicomm;
    array<Index,2> num_nodes;
    distribute<2>((Index)mpicomm.total_nodes,num_nodes);
    if(mpicomm.rank==0)
        cout << "Number of MPI processes per dimension: " << num_nodes << endl;
    mpicomm.init(num_nodes);

    array<fp,2> a = {{-1.5,0.0}}; array<fp,2> b = {{1.5,1.0}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes
      << ". Domain from " << a << " to " << b << ". Rank left: "
      << mpicomm.rank_left(0) << ". Rank right: " << mpicomm.rank_right(0)
      << ".";
    mpicomm.print(s.str());

    // init initial value
    array<Index,4> e = {{n,n,o,o}};
    domain<2> in(e, a, b); in.init(gaussian2d);
    domain<2> work = in; domain<2> out = in;
    in.write(filename<2>("in",mpicomm.node_id));

    // determine the CFL number alpha based on a displacement of 1e-3
    // (approximately alpha=0.5 for 512 grid points) For 4096 cores this gives a
    // CFL number of 32, For 16k cores a CFL number of 64 (which is still inside
    // a single cell)
    fp alpha=-alpha_unnormalized*fp(n)*fp(num_nodes[0]);
    int bdr_len=abs(int(alpha))+1;
    vector<fp> bdr_to(bdr_len*o*n*o); vector<fp> bdr_from(bdr_len*o*n*o);

    timer total;
    timer comm; timer comput; timer prep; timer sync;

    int adv_direction=0;
    sync.start(); MPI_Barrier(MPI_COMM_WORLD); sync.stop();
    total.start();
    for(int i=0;i<num_steps;i++) {
        //sync.start(); MPI_Barrier(MPI_COMM_WORLD); sync.stop();
        // set up the necessary boundary data
        prep.start();
        work.pack_boundary(0,BDRY_RIGHT,bdr_len,bdr_to);
        prep.stop();

        // start sending and receiving the data
        comm.start();
        MPI_Request reqs[2];
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        int block_size = 64; int num_blocks = n/block_size;
        comput.start();
        for(int b_j=0;b_j<num_blocks;b_j++) {
            for(int b_m=0;b_m<num_blocks;b_m++) {
                for(int m=b_m*block_size;m<(b_m+1)*block_size;m++) {
                    for(int mo=0;mo<o;mo++) {
                        array<Index,2> idx = {{m, mo}};
                        domain<2>::view vwork =
                            slice_array(work.data, adv_direction, idx);
                        domain<2>::view vout =
                            slice_array(out.data, adv_direction, idx);
                        translate1d<1>(vwork, vout, NULL, bdr_len*o, alpha,
                                TM_INTERIOR, b_j, block_size);
                    }
                }
            }
        }
        comput.stop();

        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        comput.start();
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork =
                    slice_array(work.data, adv_direction, idx);
                domain<2>::view vout =
                    slice_array(out.data, adv_direction, idx);
                fp* bdr_from_l=work.unpack_boundary(idx, 0, bdr_len, bdr_from);
                translate1d<1>(vwork, vout, bdr_from_l, bdr_len*o, alpha,
                        TM_BOUNDARY);
            }
        }
        work=out;
        comput.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(str(boost::format("Runtime: %1% Commun: %2% Comput: %3% "
                    "Prep %4% Sync %5%")%total.total()%comm.total()
                %comput.total()%prep.total()%sync.total()));
    out.write(filename<2>("out",mpicomm.node_id));
}


#ifdef _OPENMP
#include <omp.h>
#endif

/// This function provides the same functionality as test_2d_mpi but uses a
/// OpenMP parallelization within each MPI process.
void test_2d_hybrid(int n, int num_steps, int o, fp alpha_unnormalized) {
    mpi_process<2> mpicomm;
    array<Index,2> num_nodes;
    distribute<2>((Index)mpicomm.total_nodes,num_nodes);
    if(mpicomm.rank==0)
        cout << "Number of MPI processes per dimension: " << num_nodes << endl;
    mpicomm.init(num_nodes);

    array<fp,2> a = {{-1.5,0.0}}; array<fp,2> b = {{1.5,1.0}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes
      << ". Domain from " << a << " to " << b << ". Rank left: "
      << mpicomm.rank_left(0) << ". Rank right: " << mpicomm.rank_right(0)
      << ".";
    mpicomm.print(s.str());
    if(mpicomm.rank==0) {
        #ifdef _OPENMP
        cout << "OpenMP with " << omp_get_max_threads() << " threads" << endl;
        #else
        cout << "No OpenMP support" << endl;
        #endif
    }

    // init initial value
    array<Index,4> e = {{n,n,o,o}};
    domain<2> in(e, a, b); in.init(gaussian2d);
    domain<2> work = in; domain<2> out = in;
    in.write(filename<2>("in",mpicomm.node_id));

    #ifdef _OPENMP
    vector<int> pinned_to(omp_get_max_threads());
    #pragma omp parallel
    {
        int id=omp_get_thread_num();
        pinned_to[id]=sched_getcpu();
    } 
    int len=100; char hostname[100]; MPI_Get_processor_name(hostname,&len);
    std::stringstream ss; 
    ss << "OpenMP: rank: " << mpicomm.rank << ". Host: " << hostname
       << ". Pinned to: ";
    for(int i=0;i<omp_get_max_threads();i++) ss << pinned_to[i] << " ";
    mpicomm.print(ss.str());
    #endif

    fp alpha=-alpha_unnormalized*fp(n)*fp(num_nodes[0]);
    int bdr_len=abs(int(alpha))+1;
    vector<fp> bdr_to(bdr_len*o*n*o); vector<fp> bdr_from(bdr_len*o*n*o);

    timer total; total.start();
    timer comm; timer comput;
    timer comput2;
    for(int i=0;i<num_steps;i++) {
        // set up the necessary boundary data
        comm.start();
        // the parallel version of the pack_boundary function is not yet
        // implemented
        #pragma omp parallel for
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork = slice_array(work.data, 0, idx);
                for(int i=0;i<bdr_len;i++)
                    for(int j=0;j<o;j++)
                        bdr_to[bdr_len*o*o*m+bdr_len*o*mo+o*i+j] = vwork(n-1-i,j);
            }
        }
        // start sending and receiving the data
        MPI_Request reqs[2];
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        comput.start();
        #pragma omp parallel for
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork = slice_array(work.data, 0, idx);
                domain<2>::view vout = slice_array(work.data, 0, idx);
                translate1d<1>(vwork, vout, NULL, bdr_len*o, alpha, TM_INTERIOR);
            }
        }
        comput.stop();

        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        comput.start(); comput2.start();
        #pragma omp parallel for
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                array<Index,2> idx = {{m, mo}};
                domain<2>::view vwork = slice_array(work.data, 0, idx);
                domain<2>::view vout = slice_array(out.data, 0, idx);
                translate1d<1>(vwork, vout,
                        &bdr_from[bdr_len*o*o*m+bdr_len*o*mo], bdr_len*o, alpha,
                        TM_BOUNDARY);
            }
        }
        // OpenMP parallelization of work=out; (necessary to obtain performance
        // comparable to MPI)
        #pragma omp parallel for
        for(Index i=0;i<work.data.num_elements();i++)
            *(work.data.data()+i)=*(out.data.data()+i);
        comput2.stop();
        comput.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(str(boost::format("Runtime: %1% Commun: %2% Comput: %3% "
                    "Comput2: %4%")%total.total()%comm.total()%comput.total()
                    %comput2.total()));
    out.write(filename<2>("out",mpicomm.node_id));
}


fp gaussian4d(array<fp,4> x) {
    return exp(-5.0*x[0]*x[0])*sin(M_PI*x[1])*cos(5.0*M_PI*x[2])*sin(M_PI*x[3]);
}

/// Compute the solution of the advection equation using the discontinuous
/// Galerkin implementation in four space dimension. Timing for computation,
/// communication overhead, preparation of the boundary data, and
/// synchronization overhead is provided. In addition, the use of the
/// pack_boundary and unpack_boundary functions are demonstrated and
/// interleaving between communication and computation is used.
void test_4d_mpi(int n, int num_steps, int o, fp alpha_unnormalized) {
    mpi_process<4> mpicomm;
    array<Index,4> num_nodes;
    distribute<4>((Index)mpicomm.total_nodes,num_nodes);
    if(mpicomm.rank==0)
        cout << "Number of MPI processes per dimension: " << num_nodes << endl;
    mpicomm.init(num_nodes);

    array<fp,4> a = {{-1.5,0.0,0.0,0.0}}; array<fp,4> b = {{1.5,1.0,1.0,1.0}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes
      << ". Domain from " << a << " to " << b << ". Rank left: "
      << mpicomm.rank_left(0) << ". Rank right: " << mpicomm.rank_right(0)
      << ".";
    mpicomm.print(s.str());

    // init initial value
    array<Index,8> e = {{n,n,n,n,o,o,o,o}};
    domain<4> in(e, a, b); in.init(gaussian4d);
    domain<4> work = in; domain<4> out = in;
    in.write(filename<4>("in",mpicomm.node_id));

    // For 4096 cores this gives a CFL number of 32, For 16k cores a CFL number
    // of 64 (which is still inside a single cell)
    fp alpha=-alpha_unnormalized*fp(n)*fp(num_nodes[0]);
    int bdr_len=int(abs(alpha))+1;
    vector<fp> bdr_to(bdr_len*o*n*o*n*o*n*o);
    vector<fp> bdr_from(bdr_len*o*n*o*n*o*n*o);

    timer total; total.start();
    timer comm; timer comput; timer prep;
    for(int i=0;i<num_steps;i++) {
        MPI_Barrier(MPI_COMM_WORLD);
        // set up the necessary boundary data
        prep.start();
        work.pack_boundary(0,BDRY_RIGHT,bdr_len,bdr_to);
        prep.stop();
        // start sending and receiving the data
        MPI_Barrier(MPI_COMM_WORLD);
        comm.start(); 
        MPI_Request reqs[2];
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        MPI_Barrier(MPI_COMM_WORLD);
        comput.start();
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                for(int m2=0;m2<n;m2++) {
                    for(int mo2=0;mo2<o;mo2++) {
                        for(int m3=0;m3<n;m3++) {
                            for(int mo3=0;mo3<o;mo3++) {
                                array<Index,6> idx = {{m, m2, m3, mo, mo2, mo3}};
                                domain<4>::view vwork =
                                    slice_array(work.data, 0, idx);
                                domain<4>::view vout =
                                    slice_array(out.data, 0, idx);
                                translate1d<1>(vwork, vout, NULL, bdr_len*o,
                                        alpha, TM_INTERIOR);
                            }
                        }
                    }
                }
            }
        }
        comput.stop();

        MPI_Barrier(MPI_COMM_WORLD);
        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        MPI_Barrier(MPI_COMM_WORLD);
        comput.start();
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                for(int m2=0;m2<n;m2++) {
                    for(int mo2=0;mo2<o;mo2++) {
                        for(int m3=0;m3<n;m3++) {
                            for(int mo3=0;mo3<o;mo3++) {
                                array<Index,6> idx = {{m, m2, m3, mo, mo2, mo3}};
                                domain<4>::view vwork =
                                    slice_array(work.data, 0, idx);
                                domain<4>::view vout =
                                    slice_array(out.data, 0, idx);
                                fp* bdr_from_l=work.unpack_boundary(idx, 0,
                                        bdr_len, bdr_from);
                                translate1d<1>(vwork, vout, bdr_from_l,
                                        bdr_len*o, alpha, TM_BOUNDARY);
                            }
                        }
                    }
                }
            }
        }
        work=out;
        comput.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(str(boost::format("Runtime: %1% Commun: %2% Comput: %3% "
        "Prep: %4%")%total.total()%comm.total()%comput.total()%prep.total()));
    out.write(filename<4>("out",mpicomm.node_id));
}

/// This function provides the same functionality as test_4d_mpi but uses a
/// OpenMP parallelization within each MPI process.
void test_4d_hybrid(int n, int num_steps, int o, fp alpha_unnormalized) {
    mpi_process<4> mpicomm;
    array<Index,4> num_nodes;
    distribute<4>((Index)mpicomm.total_nodes,num_nodes);
    if(mpicomm.rank==0)
        cout << "Number of MPI processes per dimension: " << num_nodes << endl;
    mpicomm.init(num_nodes);

    array<fp,4> a = {{-1.5,0.0,0.0,0.0}}; array<fp,4> b = {{1.5,1.0,1.0,1.0}};
    global_ab_to_local_ab(a,b,mpicomm.node_id,mpicomm.num_nodes);

    // print node summary in order
    std::stringstream s;
    s << "Node: " << mpicomm.node_id << " of " << mpicomm.num_nodes
      << ". Domain from " << a << " to " << b << ". Rank left: "
      << mpicomm.rank_left(0) << ". Rank right: " << mpicomm.rank_right(0)
      << ".";
    mpicomm.print(s.str());
    if(mpicomm.rank==0) {
        #ifdef _OPENMP
        cout << "OpenMP with " << omp_get_max_threads() << " threads" << endl;
        #else
        cout << "No OpenMP support" << endl;
        #endif
    }

    // init initial value
    array<Index,8> e = {{n,n,n,n,o,o,o,o}};
    domain<4> in(e, a, b); in.init(gaussian4d);
    domain<4> work = in; domain<4> out = in;
    in.write(filename<4>("in",mpicomm.node_id));

    #ifdef _OPENMP
    vector<int> pinned_to(omp_get_max_threads());
    #pragma omp parallel
    {
        int id=omp_get_thread_num();
        pinned_to[id]=sched_getcpu();
    } 
    int len=100; char hostname[100]; MPI_Get_processor_name(hostname,&len);
    std::stringstream ss; 
    ss << "OpenMP: rank: " << mpicomm.rank << ". Host: " << hostname
       << ". Pinned to: ";
    for(int i=0;i<omp_get_max_threads();i++) ss << pinned_to[i] << " ";
    mpicomm.print(ss.str());
    #endif

    fp alpha=-alpha_unnormalized*fp(n)*fp(num_nodes[0]);
    int bdr_len=int(abs(alpha))+1;
    vector<fp> bdr_to(bdr_len*o*n*o*n*o*n*o);
    vector<fp> bdr_from(bdr_len*o*n*o*n*o*n*o);

    timer total; total.start();
    timer comm; timer comput;
    timer comput2;
    for(int i=0;i<num_steps;i++) {
        // set up the necessary boundary data
        comm.start();
        work.pack_boundary(0,BDRY_RIGHT,bdr_len,bdr_to);
        // start sending and receiving the data
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Request reqs[2];
        MPI_Isend(&bdr_to[0], bdr_to.size(), MPI_FP, mpicomm.rank_right(0),
                1, MPI_COMM_WORLD, reqs);
        MPI_Irecv(&bdr_from[0], bdr_from.size(), MPI_FP, mpicomm.rank_left(0),
                1, MPI_COMM_WORLD, reqs+1);
        comm.stop();

        // do the part of the computation we can without requiring the boundary
        // data
        comput.start();
        #pragma omp parallel for
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                for(int m2=0;m2<n;m2++) {
                    for(int mo2=0;mo2<o;mo2++) {
                        for(int m3=0;m3<n;m3++) {
                            for(int mo3=0;mo3<o;mo3++) {
                                array<Index,6> idx = {{m, m2, m3, mo, mo2, mo3}};
                                domain<4>::view vwork =
                                    slice_array(work.data, 0, idx);
                                domain<4>::view vout =
                                    slice_array(out.data, 0, idx);
                                translate1d<1>(vwork, vout, NULL, bdr_len*o,
                                        alpha, TM_INTERIOR);
                            }
                        }
                    }
                }
            }
        }
        comput.stop();

        comm.start();
        MPI_Status stats[2];
        MPI_Waitall(2, reqs, stats);
        comm.stop();

        comput.start(); comput2.start();
        #pragma omp parallel for
        for(int m=0;m<n;m++) {
            for(int mo=0;mo<o;mo++) {
                for(int m2=0;m2<n;m2++) {
                    for(int mo2=0;mo2<o;mo2++) {
                        for(int m3=0;m3<n;m3++) {
                            for(int mo3=0;mo3<o;mo3++) {
                                array<Index,6> idx = {{m, m2, m3, mo, mo2, mo3}};
                                domain<4>::view vwork = 
                                    slice_array(work.data, 0, idx);
                                domain<4>::view vout =
                                    slice_array(out.data, 0, idx);
                                fp* bdr_from_l=work.unpack_boundary(idx, 0,
                                        bdr_len, bdr_from);
                                translate1d<1>(vwork, vout, bdr_from_l,
                                        bdr_len*o, alpha, TM_BOUNDARY);
                            }
                        }
                    }
                }
            }
        }
        // OpenMP parallelization of work=out; (necessary to obtain performance
        // comparable to MPI)
        #pragma omp parallel for
        for(Index i=0;i<work.data.num_elements();i++)
            *(work.data.data()+i)=*(out.data.data()+i);
        comput.stop(); comput2.stop();
    }
    total.stop();
    // print statistics in order
    mpicomm.print(
        str(boost::format("Runtime: %1% Commun: %2% Comput: %3% ""Comput2: %4%")
        %total.total()%comm.total()%comput.total()%comput2.total()));
    out.write(filename<4>("out",mpicomm.node_id));
}

int main(int argc, char* argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string identifier, problem;
    int num_cells, num_steps, o;
    fp alpha;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("identifier",value<string>(&identifier)->default_value(""),
         "The identifier of the problem/test to run (e.g. test_1d_mpi)")
        ("num_cells,N",value<int>(&num_cells)->default_value(128),
         "number of cells used in the simulation")
        ("order,o",value<int>(&o)->default_value(2),
         "order of the space approximation")
        ("num_steps",value<int>(&num_steps)->default_value(500),
         "number of time steps conducted")
        ("alpha",value<fp>(&alpha)->default_value(1e-3),
         "t*v for the advection tests")
        ("enable_output",value<bool>(&enable_output)->default_value(false),
         "enable or disable writing to disk")
        ("problem",value<string>(&problem)->default_value("bot"),
         "initial value for the problem that is to be solved");

    positional_options_description pos;
    pos.add("identifier", 1);

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).positional(pos).run(),
            vm);
    notify(vm);

    if(vm.count("help") || identifier=="") {
        cout << desc << endl;
        return 1;
    } else {
        mpi_init(true);

        if(identifier=="test_1d_mpi")
            test_1d_mpi(num_cells, num_steps, o);
        else if(identifier=="test_2d_mpi")
            test_2d_mpi(num_cells, num_steps, o, alpha);
        else if(identifier=="test_2d_mpi_blocked")
            test_2d_mpi_blocked(num_cells, num_steps, o, alpha);
        else if(identifier=="test_2d_hybrid")
            test_2d_hybrid(num_cells, num_steps, o, alpha);
        else if(identifier=="test_4d_mpi")
            test_4d_mpi(num_cells, num_steps, o, alpha);
        else if(identifier=="test_4d_hybrid")
            test_4d_hybrid(num_cells, num_steps, o, alpha);
        else {
            cout << "ERROR: could not run identifier " << identifier << endl;
            return 1;
        }

        mpi_destroy();
    }

    return 0;
}

