#pragma once

#include <complex>
#include <fftw3.h>
#include <algorithms/sldg2d/legendre_f.hpp>
#include <algorithms/sldg2d/lagrange_f.hpp>

template<size_t o>
struct poisson_fft{

    using cmplx = std::complex<double>;

    static constexpr size_t Nloc = o*(o+1)/2;

    int Nx, Ny;
    double ax, ay, bx, by;
    double hx, hy;

    ext_vect<double> in_at_cc;
    ext_vect<double> E1;
    ext_vect<double> E2;

    ext_vect<double> E1old;
    ext_vect<double> E2old;

    ext_vect<double> E1_dg;
    ext_vect<double> E2_dg;

    ext_vect<cmplx> inhat;
    ext_vect<cmplx> E1hat;
    ext_vect<cmplx> E2hat;

    ext_vect<double> phi;

    double x_star;
    double y_star;

    fftw_plan plan_forward;
    fftw_plan plan_backward_x;
    fftw_plan plan_backward_y;
    fftw_plan plan_backward_phi;

    poisson_fft(int _Nx, int _Ny, double _ax,
                double _ay, double _bx, double _by)
        : Nx{_Nx}, Ny{_Ny}, ax{_ax}, ay{_ay}, bx{_bx}, by{_by}
    {

        hx = (bx-ax)/double(Nx);
        hy = (by-ay)/double(Ny);

        in_at_cc.resize(Nx,Ny);
        E1.resize(Nx,Ny);
        E2.resize(Nx,Ny);
        E1old.resize(Nx+1,Ny+1);
        E2old.resize(Nx+1,Ny+1);

        E1_dg.resize(o*Nx,o*Ny);
        E2_dg.resize(o*Nx,o*Ny);

        inhat.resize(Nx/2+1,Ny);
        E1hat.resize(Nx/2+1,Ny);
        E2hat.resize(Nx/2+1,Ny);

        phi.resize(Nx,Ny);

        plan_forward = fftw_plan_dft_r2c_2d(Ny,Nx,in_at_cc.h_data.data(),
                                            (fftw_complex*)inhat.h_data.data(),
                                            FFTW_ESTIMATE);

        plan_backward_x = fftw_plan_dft_c2r_2d(Ny,Nx,
                                               (fftw_complex*)E1hat.h_data.data(),
                                               E1.h_data.data(),FFTW_ESTIMATE);
        plan_backward_y = fftw_plan_dft_c2r_2d(Ny,Nx,
                                               (fftw_complex*)E2hat.h_data.data(),
                                               E2.h_data.data(),FFTW_ESTIMATE);
        plan_backward_phi = fftw_plan_dft_c2r_2d(Ny,Nx,
                                               (fftw_complex*)inhat.h_data.data(),
                                               phi.h_data.data(),FFTW_ESTIMATE);
    }

    ~poisson_fft(){

        fftw_destroy_plan(plan_forward);
        fftw_destroy_plan(plan_backward_x);
        fftw_destroy_plan(plan_backward_y);
    }


    void solve_legendre(double* in, double c,
                        double (*f0)(double,double), bool div, int n){

        get_values_at_cc_legendre(in,c,f0,div, n);
        solve_poisson_and_differentiate();
    }


    void solve_lagrange(double* in, double c,
                        double (*f0)(double,double), bool div, int n){

        get_values_at_cc_lagrange(in,c,f0,div, n);
        solve_poisson_and_differentiate();
        interpolate_to_dg();
    }


    double F_at_point(ext_vect<double>& F, double x, double y){

        //int ii = round((x-ax)/hx);
        //if(fabs(ax+ii*hx - x) > MACHINE_PREC ){
        //    ii = floor((x-ax)/hx);
        //} else {
        //    cout << "WARNING: on boundary x\n";
        //    ii = 2*ii-1;
        //}

        //int jj = round((y-ay)/hy);
        //if(fabs(ay+jj*hy - y) > MACHINE_PREC ){
        //    jj = floor((y-ay)/hy);
        //} else {
        //    cout << "WARNING: on boundary y\n";
        //    jj = 2*jj-1;
        //}

        int i = floor((x-ax)/hx);
        int j = floor((y-ay)/hy);

        double F11 = F((i+Nx)%Nx,(j+Ny)%Ny);
        double F12 = F((i+Nx)%Nx,(j+1+Ny)%Ny);
        double F21 = F((i+1+Nx)%Nx,(j+Ny)%Ny);
        double F22 = F((i+1+Nx)%Nx,(j+1+Ny)%Ny);

        double x1 = ax + i*hx;
        double x2 = ax + (i+1)*hx;

        double y1 = ay + j*hy;
        double y2 = ay + (j+1)*hy;

        return (F11*(x2-x)*(y2-y) + F12*(x2-x)*(y-y1) +
                F21*(x-x1)*(y2-y) + F22*(x-x1)*(y-y1))/(hx*hy);

    }


    double compute_Energy(){

        double energy = 0.0;
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                energy += pow(E1(i,j),2.0) + pow(E2(i,j),2.0);

            }
        }

        return energy*hx*hy;
    }


    double compute_Enstrophy(double* in){

        constexpr size_t o_integrate = 6;

        double enstrophy=0.0;
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){
                for(size_t jj=0;jj<o_integrate;++jj){
                    for(size_t ii=0;ii<o_integrate;++ii){

                        double x = ax + hx*(i + gauss::x_scaled01(ii,o_integrate));
                        double y = ay + hy*(j + gauss::x_scaled01(jj,o_integrate));

                        double v = in[0 + Nloc*(i+j*Nx)];
                        if(o>1){
                            v += in[1 + Nloc*(i+j*Nx)]*(x-hx*(i+0.5)-ax)/hx +
                                 in[2 + Nloc*(i+j*Nx)]*(y-hy*(j+0.5)-ay)/hy;
                        }
                        if(o>2){
                            double xx = (x-hx*(i+0.5)-ax)/hx;
                            double yy = (y-hy*(j+0.5)-ay)/hy;
                            v += in[3 + Nloc*(i+j*Nx)]*(xx*xx-1./12.) +
                                 in[4 + Nloc*(i+j*Nx)]*xx*yy +
                                 in[5 + Nloc*(i+j*Nx)]*(yy*yy-1./12.);
                        }

                        enstrophy += v*v*gauss::w(ii,o_integrate)*
                                         gauss::w(jj,o_integrate);

                    }
                }
            }
        }

        return enstrophy*hx*hy*0.25;
    }


    void compute1(double& x, double& y, double dt, int i, int j){

        x -= dt*E1(i%Nx,j%Ny);
        y -= dt*E2(i%Nx,j%Ny);

        E1old(i,j) = F_at_point(E1,x,y);
        E2old(i,j) = F_at_point(E2,x,y);

    }


    void compute2(double& x, double& y, double dt, int i, int j){

        x -= dt*0.5*(E1(i%Nx,j%Ny) + E1old(i,j));
        y -= dt*0.5*(E2(i%Nx,j%Ny) + E2old(i,j));

    }


    void follow_characteristics_cc1(double& x, double&y, double dt, 
                                   int i, int j, int UNUSED(k)){

        x -= dt*E1(i%Nx,j%Ny);
        y -= dt*E2(i%Nx,j%Ny);

    }


    void follow_characteristics_dg1(double& x, double&y, double dt, 
                                    int ii, int jj, int i, int j, int UNUSED(k)){

        x -= dt*E1_dg(ii+o*i,jj+o*j);
        y -= dt*E2_dg(ii+o*i,jj+o*j);

    }


    void follow_characteristics_cc2(double& x, double&y, double dt, 
                                   int i, int j, int UNUSED(k)){

        double x12 = x - 0.5*dt*E1(i%Nx,j%Ny);
        double y12 = y - 0.5*dt*E2(i%Nx,j%Ny);

        x -= dt*F_at_point(E1,x12,y12);
        y -= dt*F_at_point(E2,x12,y12);
    }


    void follow_characteristics_dg2(double& x, double&y, double dt, 
                                    int ii, int jj, int i, int j, int UNUSED(k)){


        double x12 = x - 0.5*dt*E1_dg(ii+o*i,jj+o*j);
        double y12 = y - 0.5*dt*E2_dg(ii+o*i,jj+o*j);

        x -= dt*F_at_point(E1,x12,y12);
        y -= dt*F_at_point(E2,x12,y12);

    }



    //TODO: the same has to be done with E12_dg
    //void follow_characteristics_cc2(double& x, double& y, double dt, 
    //                                int i, int j, int UNUSED(k)){

    //    x -= dt*0.5*(E1(i%Nx,j%Ny) + E1old(i,j));
    //    y -= dt*0.5*(E2(i%Nx,j%Ny) + E2old(i,j));

    //}


    //be aware of race conditions when parallelizing
    void get_values_at_cc_legendre(double* in, double c, 
                                   double (*f0)(double,double),
                                   bool divide, int n){

        fill(begin(in_at_cc.h_data),end(in_at_cc.h_data),0.0);

        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                for(int jj=0;jj<2;++jj){
                    for(int ii=0;ii<2;++ii){

                        double x = ax + hx*(i + ii);
                        double y = ay + hy*(j + jj);

                        double v = in[0 + Nloc*(i+j*Nx)];
                        if(o>1){
                            v += in[1 + Nloc*(i+j*Nx)]*(x-hx*(i+0.5)-ax)/hx +
                                 in[2 + Nloc*(i+j*Nx)]*(y-hy*(j+0.5)-ay)/hy;
                        }
                        if(o>2){
                            double xx = (x-hx*(i+0.5)-ax)/hx;
                            double yy = (y-hy*(j+0.5)-ay)/hy;
                            v += in[3 + Nloc*(i+j*Nx)]*(xx*xx-1./12.) +
                                 in[4 + Nloc*(i+j*Nx)]*xx*yy +
                                 in[5 + Nloc*(i+j*Nx)]*(yy*yy-1./12.);
                        }

                        if(divide)
                            v = v/f0(x,y) + c;
                        else
                            v += c;

                        in_at_cc((i+ii)%Nx,(j+jj)%Ny) += v*0.25;

                    }
                }
            }
        }
        if(n>=0){
            std::ofstream output("data_rhs"+std::to_string(n)+".data");
            for(int j=0;j<Ny;++j){
                for(int i=0;i<Nx;++i){
                    double x = ax + hx*i;
                    double y = ay + hy*j;
                    output << x << " " << y << " " << in_at_cc(i,j) << endl;
                }
            }
        }

    }

    
    double basisf(double x, double y, int i, int j){

        return (x-gauss::x_scaled01(1-i,2))*(y-gauss::x_scaled01(1-j,2))/
               ((gauss::x_scaled01(i,2)-gauss::x_scaled01(1-i,2))*
                (gauss::x_scaled01(j,2)-gauss::x_scaled01(1-j,2)));
    }


    void get_values_at_cc_lagrange(double* in, double c, 
                                   double (*f0)(double,double),
                                   bool divide, int n){

        fill(begin(in_at_cc.h_data),end(in_at_cc.h_data),0.0);

        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                for(int jj=0;jj<o;++jj){
                    for(int ii=0;ii<o;++ii){

                        double v_in = in[ii+o*jj + o*o*(i+j*Nx)] + c;
                        if(divide)
                            cout << f0(i,n);

                        double v_bl = v_in*basisf(0.0,0.0,ii,jj);
                        double v_br = v_in*basisf(1.0,0.0,ii,jj);
                        double v_tl = v_in*basisf(0.0,1.0,ii,jj);
                        double v_tr = v_in*basisf(1.0,1.0,ii,jj);

                        in_at_cc(i,       j) += v_bl*0.25;
                        in_at_cc((i+1)%Nx,j) += v_br*0.25;
                        in_at_cc(i,       (j+1)%Ny) += v_tl*0.25;
                        in_at_cc((i+1)%Nx,(j+1)%Ny) += v_tr*0.25;

                    }
                }
            }
        }
        if(n>=0){
            std::ofstream output("data_rhs"+std::to_string(n)+".data");
            for(int j=0;j<Ny;++j){
                for(int i=0;i<Nx;++i){
                    double x = ax + hx*i;
                    double y = ay + hy*j;
                    output << x << " " << y << " " << in_at_cc(i,j) << endl;
                }
            }
        }


    }


    void interpolate_to_dg(){
        
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                double cc_x[4];
                double cc_y[4];
                for(int jj=0;jj<2;++jj)
                    for(int ii=0;ii<2;++ii){
                        cc_x[ii+2*jj] = E1((i+ii)%Nx,(j+jj)%Ny);
                        cc_y[ii+2*jj] = E2((i+ii)%Nx,(j+jj)%Ny);
                    }

                for(int jj=0;jj<o;++jj){
                    for(int ii=0;ii<o;++ii){

                        double x = gauss::x_scaled01(ii,o);
                        double y = gauss::x_scaled01(jj,o);

                        double v_x = cc_x[0]*(1.0-x)*(1.0-y) +
                                     cc_x[1]*(x-0.0)*(1.0-y) +
                                     cc_x[2]*(1.0-x)*(y-0.0) +
                                     cc_x[3]*(x-0.0)*(y-0.0);

                        E1_dg(ii+o*i,jj+o*j)  = v_x;

                        double v_y = cc_y[0]*(1.0-x)*(1.0-y) +
                                     cc_y[1]*(x-0.0)*(1.0-y) +
                                     cc_y[2]*(1.0-x)*(y-0.0) +
                                     cc_y[3]*(x-0.0)*(y-0.0);

                        E2_dg(ii+o*i,jj+o*j)  = v_y;

                    }
                }
            }
        }
    }


    void differentiate_via_fd(){

        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){
                E1(i,j) = -(phi(i,(j+1)%Ny)-phi(i,(j-1+Ny)%Ny))/(2.0*hy);
                E2(i,j) =  (phi((i+1)%Nx,j)-phi((i-1+Nx)%Nx,j))/(2.0*hx);
            }
        }

    }
   

    void solve_poisson_and_differentiate(){

        double normalization = Nx*Ny;

        fftw_execute(plan_forward);

        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx/2+1;++i){

                double freq_x = 2.0*M_PI/(bx-ax)*i;
                double freq_y = 2.0*M_PI/(by-ay)*((j<Ny/2+1) ? j : -(Ny-j));

                double freq_sq = freq_x*freq_x + freq_y*freq_y;

                cmplx v = inhat(i,j);

                double common_factor = (i+j==0) ? 0.0 :
                                       1.0/freq_sq/normalization;

                inhat(i,j) *= common_factor;
                E1hat(i,j) = -cmplx(0,freq_y)*common_factor*v;
                E2hat(i,j) =  cmplx(0,freq_x)*common_factor*v;
            }
        }

        //fftw_execute(plan_backward_x);
        //fftw_execute(plan_backward_y);
        fftw_execute(plan_backward_phi);

        differentiate_via_fd();

    }


};


template<size_t o=2>
array<double,5> test_poisson_fft_legendre(){

    constexpr size_t Nloc = o*(o+1)/2;

    double ax = 0.0*M_PI;
    double ay = 0.0*M_PI;
    double bx = 2.0*M_PI;
    double by = 2.0*M_PI;

    auto f0 = [](double x, double y){return -2.0*sin(x)*sin(y);};

    double errold_cc= -1.0;
    double errold_x = -1.0;
    double errold_y = -1.0;
    double errold_phi = -1.0;

    double order_test = -1.0;

    for(int N=20;N<161;N*=2){

        int Nx=N;
        int Ny=N;

        poisson_fft<o> gc{Nx, Ny, ax, ay, bx, by};

        generic_container<double> in(Nloc*Nx*Ny);
        init_legendre<o>(f0,Nx,Ny,ax,ay,gc.hx,gc.hy,in.data(false));

        gc.get_values_at_cc_legendre(in.data(false),0.0,f0,false,0);

        double maxerr = 0.0;
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                double x = ax + i*gc.hx;
                double y = ay + j*gc.hy;

                double err = f0(x,y)-gc.in_at_cc(i,j);

                maxerr = std::max(maxerr, fabs(err));

            }
        }

        if(errold_cc>0.0){
            double order = -log(maxerr/errold_cc)/log(2);
            if(order < 1.9)
                order_test = 1.0;

        }
        errold_cc = maxerr;

        gc.solve_poisson_and_differentiate();

        double maxerr_x = 0.0;
        double maxerr_y = 0.0;
        double maxerr_phi = 0.0;
        auto E1 = [](double x, double y){return  sin(x)*cos(y);};
        auto E2 = [](double x, double y){return -cos(x)*sin(y);};
        auto phi = [](double x, double y){return -sin(x)*sin(y);};
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                double x = ax + i*gc.hx;
                double y = ay + j*gc.hy;

                double err_x = E1(x,y)-gc.E1(i,j);
                double err_y = E2(x,y)-gc.E2(i,j);
                double err_phi = phi(x,y)-gc.phi(i,j);

                maxerr_x = std::max(maxerr_x, fabs(err_x));
                maxerr_y = std::max(maxerr_y, fabs(err_y));
                maxerr_phi = std::max(maxerr_phi, fabs(err_phi));
            }
        }

        if(errold_x>0.0 && errold_y>0.0){
            double order_x = -log(maxerr_x/errold_x)/log(2);
            double order_y = -log(maxerr_y/errold_y)/log(2);
            double order_phi = -log(maxerr_phi/errold_phi)/log(2);

            if(order_x < 1.9 || order_y < 1.9 || order_phi < 1.9)
                order_test = 1.0;

        }
        errold_x = maxerr_x;
        errold_y = maxerr_y;
        errold_phi = maxerr_phi;


    }


    return {errold_cc, errold_x, errold_y, errold_phi,order_test};

}


template<size_t o=2>
array<double,6> test_poisson_fft_lagrange(){

    constexpr size_t Nloc = o*o;

    double ax = 0.0*M_PI;
    double ay = 0.0*M_PI;
    double bx = 2.0*M_PI;
    double by = 2.0*M_PI;

    auto f0 = [](double x, double y){return -2.0*sin(x)*sin(y);};

    double errold_cc= -1.0;
    double errold_x = -1.0;
    double errold_y = -1.0;
    double errold_x_dg = -1.0;
    double errold_y_dg = -1.0;

    double order_test = -1.0;

    for(int N=20;N<161;N*=2){

        int Nx=N;
        int Ny=N;

        poisson_fft<o> gc{Nx, Ny, ax, ay, bx, by};

        generic_container<double> in(Nloc*Nx*Ny);
        init_lagrange<o>(f0,Nx,Ny,ax,ay,gc.hx,gc.hy,in.data(false));

        gc.get_values_at_cc_lagrange(in.data(false),0.0,f0,false,0);

        double maxerr = 0.0;
        std::ofstream output("out.data");
        std::ofstream reference("ref.data");
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                double x = ax + i*gc.hx;
                double y = ay + j*gc.hy;

                double err = f0(x,y)-gc.in_at_cc(i,j);

                maxerr = std::max(maxerr, fabs(err));

                output << x << " " << y << " " << gc.in_at_cc(i,j) << endl;
                reference << x << " " << y << " " << f0(x,y) << endl;

            }
        }

        if(errold_cc>0.0){
            double order = -log(maxerr/errold_cc)/log(2);
            if(order < 1.9)
                order_test = 1.0;
        }
        errold_cc = maxerr;

        gc.solve_poisson_and_differentiate();

        double maxerr_x = 0.0;
        double maxerr_y = 0.0;
        auto E1 = [](double x, double y){return  sin(x)*cos(y);};
        auto E2 = [](double x, double y){return -cos(x)*sin(y);};
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                double x = ax + i*gc.hx;
                double y = ay + j*gc.hy;

                double err_x = E1(x,y)-gc.E1(i,j);
                double err_y = E2(x,y)-gc.E2(i,j);

                maxerr_x = std::max(maxerr_x, fabs(err_x));
                maxerr_y = std::max(maxerr_y, fabs(err_y));

            }
        }
        //cout << "error cc: " << maxerr_x << ", " << maxerr_y << endl;

        if(errold_x>0.0 && errold_y>0.0){
            double order_x = -log(maxerr_x/errold_x)/log(2);
            double order_y = -log(maxerr_y/errold_y)/log(2);
            if(order_x < 1.9 || order_y < 1.9)
                order_test= 1.0;

        }
        errold_x = maxerr_x;
        errold_y = maxerr_y;

        gc.interpolate_to_dg();

        //check interpolation to dg
        double maxerr_x_dg = 0.0;
        double maxerr_y_dg = 0.0;
        for(int j=0;j<Ny;++j){
            for(int i=0;i<Nx;++i){

                for(int jj=0;jj<o;++jj){
                    for(int ii=0;ii<o;++ii){

                        double x = ax + gc.hx*(i + gauss::x_scaled01(ii,o));
                        double y = ay + gc.hy*(j + gauss::x_scaled01(jj,o));

                        double err_x = E1(x,y)-gc.E1_dg(ii+o*i,jj+o*j);
                        double err_y = E2(x,y)-gc.E2_dg(ii+o*i,jj+o*j);

                        maxerr_x_dg = std::max(maxerr_x_dg, fabs(err_x));
                        maxerr_y_dg = std::max(maxerr_y_dg, fabs(err_y));
                    }
                }
            }
        }
        //cout << "error dg: " << maxerr_x_dg << ", " << maxerr_y_dg << endl;

        if(errold_x_dg>0.0 && errold_y_dg>0.0){
            double order_x = -log(maxerr_x_dg/errold_x_dg)/log(2);
            double order_y = -log(maxerr_y_dg/errold_y_dg)/log(2);
            if(order_x < 1.9 || order_y < 1.9)
                order_test= 1.0;

        }
        errold_x_dg = maxerr_x_dg;
        errold_y_dg = maxerr_y_dg;

    }

    return {errold_cc, errold_x, errold_y, errold_x_dg, errold_y_dg, order_test};
}

