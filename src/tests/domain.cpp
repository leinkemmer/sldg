#include <generic/common.hpp>

#define BOOST_TEST_MAIN DOMAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <container/domain.hpp>
#include <algorithms/dg.hpp>

bool enable_output; //is read in domain.hpp

#ifdef USE_SINGLE_PRECISION
float machine_prec = 1e-7;
#else
double machine_prec = 1e-16;
#endif


template<Index d>
fp func_val(array<Index,2*d> idx) {
    fp val = 0.0;
    for(Index k=0;k<2*d;k++)
        val += fp(k+1)*idx[k];
    return val;
}

template<Index d>
domain<d> domain_for_pack_boundary(Index dim, Index o) {

    array<Index, 2*d>   ext;
    array<fp, d>  a, b;

    for(Index i=0;i<d;i++) {
        ext[i]   = 8+i;
        ext[d+i] = o;
        a[i] = -fp(i);
        b[i] =  fp(i)*0.5;
    }

    domain<d> dom(ext, a, b);
    for(auto i : range<2*d>(ext)) {
        dom.data(i) = func_val<d>(i);
    }

    return dom;
}

template<size_t d>
fp func_F(const array<Index,d>& idx) {
    fp sum=0.0;
    for(size_t i=0;i<d;i++) {
        sum += pow(-1,i+1)*idx[i];
    }
    return sum;
}

template<Index d>
fp error_for_pack_boundary(domain<d>& dom, Index bdr_len, vector<fp>& bdr_data,
        Index dim, leftright lr, int dim2=-1) {

    array<Index, 2*d-2> ext_compl = slice_no<d>(dim, dom.extension);
    Index o = dom.extension[d+dim];

    Index dim2_idx = ((Index)dim2<dim) ? dim2 : dim2-1;

    fp err = 0.0;
    for(auto i : range<2*d-2>(ext_compl)) {
        fp* p = dom.unpack_boundary(i, dim, bdr_len, bdr_data);

        for(Index k=0;k<bdr_len;k++)
            for(Index ko=0;ko<o;ko++) {
                array<Index, 2*d> idx;
                leftright use_lr = lr;
                if(lr == BDRY_DYNAMIC) {
                    if(dim2==-1) {
                        idx = unslice<d>(dim, i, 0, 0);
                        array<Index,d> idx_xv = get_compl_idx<d>(dim, idx);
                        if(func_F(idx_xv) < 0.0)
                            use_lr = BDRY_LEFT;
                        else
                            use_lr = BDRY_RIGHT;
                    } else {
                        if((i[dim2_idx]*o+i[d-1+dim2_idx])%3 == 0)
                            use_lr = BDRY_RIGHT;
                        else
                            use_lr = BDRY_LEFT;
                    }
                }
                if(use_lr == BDRY_LEFT)
                    idx = unslice<d>(dim, i, k, ko);
                else
                    idx = unslice<d>(dim, i, dom.extension[dim]-k-1, ko);
                err = max( err, (fp)fabs(*p - func_val<d>(idx)) );
                p++;
            }
    }

    return err;
}

template<Index d>
fp check_pack_boundary_1(Index dim, Index o, leftright lr) {

    domain<d> dom = domain_for_pack_boundary<d>(dim, o);

    // set up boundary data
    Index bdr_len = 3;
    array<Index, 2*d-2> ext_compl = slice_no<d>(dim, dom.extension);
    Index n_compl = prod(ext_compl);
    vector<fp> bdr_data(bdr_len*n_compl*o);

    dom.pack_boundary(dim, lr, bdr_len, bdr_data);
    
    return error_for_pack_boundary<d>(dom, bdr_len, bdr_data, dim, lr);
}

template<Index d>
fp check_pack_boundary_2(Index dim, Index o, Index dim2) {

    domain<d> dom = domain_for_pack_boundary<d>(dim, o);

    // set up boundary data
    Index bdr_len = 3;
    array<Index, 2*d-2> ext_compl = slice_no<d>(dim, dom.extension);
    Index n_compl = prod(ext_compl);
    vector<fp> bdr_data(bdr_len*n_compl*o);

    // set up lalpha
    vector<fp> lalpha(dom.extension[dim2]*o);
    for(size_t i=0;i<lalpha.size();i++)
        lalpha[i] = 0.5 - 1.0*(i%3);

    dom.pack_boundary(dim, BDRY_DYNAMIC, bdr_len, bdr_data, &lalpha, dim2);
    
    return error_for_pack_boundary<d>(dom, bdr_len, bdr_data, dim,
            BDRY_DYNAMIC, dim2);
}

template<Index d>
fp check_pack_boundary_3(Index dim, Index o) {

    domain<d> dom = domain_for_pack_boundary<d>(dim, o);

    // set up boundary data
    Index bdr_len = 3;
    array<Index, 2*d-2> ext_compl = slice_no<d>(dim, dom.extension);
    Index n_compl = prod(ext_compl);
    vector<fp> bdr_data(bdr_len*n_compl*o);

    // set up F
    array<Index,d> ext_xv = get_compl_idx<d>(dim, dom.extension);
    multi_array<fp,d> F(ext_xv);
    for(auto i : range<d>(ext_xv))
        F(i) = func_F<d>(i);

    dom.pack_boundary(dim, BDRY_DYNAMIC, bdr_len, bdr_data, &F);
    
    return error_for_pack_boundary<d>(dom, bdr_len, bdr_data, dim, BDRY_DYNAMIC);
}

BOOST_AUTO_TEST_CASE( pack_boundary_1 ) {

    // d=2
    for(Index o=2;o<=6;o++) {
        for(Index dim=0;dim<2;dim++) {
            BOOST_CHECK_SMALL( check_pack_boundary_1<2>(dim, o, BDRY_LEFT) ,
                    machine_prec );
            BOOST_CHECK_SMALL( check_pack_boundary_1<2>(dim, o, BDRY_RIGHT),
                    machine_prec );
        }
    }
    
    // d=4
    for(Index o=2;o<=4;o++) {
        for(Index dim=0;dim<4;dim++) {
            BOOST_CHECK_SMALL( check_pack_boundary_1<4>(dim, o, BDRY_LEFT),
                    machine_prec );
            BOOST_CHECK_SMALL( check_pack_boundary_1<4>(dim, o, BDRY_RIGHT),
                    machine_prec );
        }
    }
}

BOOST_AUTO_TEST_CASE( pack_boundary_2 ) {

    // d=2
    for(Index o=2;o<=6;o++) {
        for(Index dim=0;dim<2;dim++) {
            for(Index dim2=0;dim2<2;dim2++) {
                if(dim != dim2) {
                    BOOST_CHECK_SMALL( check_pack_boundary_2<2>(dim, o, dim2),
                            machine_prec );
                    BOOST_CHECK_SMALL( check_pack_boundary_2<2>(dim, o, dim2),
                            machine_prec );
                }
            }
        }
    }

    // d=4
    for(Index o=2;o<=4;o++) {
        for(Index dim=0;dim<4;dim++) {
            for(Index dim2=0;dim2<4;dim2++) {
                if(dim != dim2) {
                    BOOST_CHECK_SMALL( check_pack_boundary_2<4>(dim, o, dim2),
                            machine_prec );
                    BOOST_CHECK_SMALL( check_pack_boundary_2<4>(dim, o, dim2),
                            machine_prec );
                }
            }
        }
    }
}

BOOST_AUTO_TEST_CASE( pack_boundary_3 ) {

    // d=2
    for(Index o=2;o<=6;o++) {
        for(Index dim=0;dim<2;dim++) {
            BOOST_CHECK_SMALL( check_pack_boundary_3<2>(dim, o) , machine_prec );
        }
    }
    
    // d=4
    for(Index o=2;o<=4;o++) {
        for(Index dim=0;dim<4;dim++) {
            BOOST_CHECK_SMALL( check_pack_boundary_3<4>(dim, o) , machine_prec );
        }
    }
}

