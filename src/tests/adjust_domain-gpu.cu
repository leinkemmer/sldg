#include <algorithms/adjust_domain.hpp>
#include <algorithms/adjust_domain-gpu.cu>
#include <programs/vp-block-cpu.hpp>
#include <programs/vp-block-gpu.hpp>

#define BOOST_TEST_MAIN ADJUST_DOMAIN_BDRY_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

bool enable_output = true;

BOOST_AUTO_TEST_CASE( max_value_test ){

    constexpr size_t d = 2;

    Index N = 123;
    Index o = 4;
    array<Index,2*d> e = {N,N+1,o,o};
    
    multi_array<fp,2*d> h_in(e);
    multi_array<fp,2*d> d_in(e,true);

    array<Index,2*d> idx = {0};
    fp hx = 2*M_PI/fp(e[0]);
    fp hy = 2*M_PI/fp(e[1]);
    do{
        fp x = hx*(idx[0] + gauss::x_scaled01(idx[d+0],e[d+0]));
        fp y = hy*(idx[1] + gauss::x_scaled01(idx[d+1],e[d+1]));
        h_in(idx) = sin(x)*cos(y);
    }while(iter_next_memorder(idx,e));

    d_in = h_in;

    Index dof_x = e[0]*e[2];

    max_abs_v_stride<d> max_v(d_in, dof_x);

    for(int vidx=0;vidx<e[1]*e[d+1];vidx += 6){
        fp max_cpu = check_max_abs_v<1,1>(vidx,dof_x,h_in);
        fp max_gpu = max_v.get(vidx,dof_x,d_in);
        BOOST_CHECK_SMALL( abs(max_cpu-max_gpu), 1e-16 );
    }

}

__host__ __device__
fp f0(fp* xv){
    fp x = xv[0];
    fp v = xv[1];
    return cos(x)*exp(-v*v);
}


BOOST_AUTO_TEST_CASE( adjust_domain_gpu ){

    constexpr size_t dx = 1;
    constexpr size_t dv = 1;

    array_dxdv<fp,dx,dv> a_cpu = {0.0,-8.0};
    array_dxdv<fp,dx,dv> b_cpu = {2.0*M_PI,8.0};

    array_dxdv<fp,dx,dv> a_gpu = a_cpu;
    array_dxdv<fp,dx,dv> b_gpu = b_cpu;
 
    Index Nx = 200;
    Index Nv = 200;
    Index o = 4;
    index_dxdv_no<dx,dv> e = {Nx,Nv,o,o};
    vp_block_cpu<dx,dv,f0> block_cpu(a_cpu,b_cpu,e,0);
    vp_block_gpu<dx,dv,f0> block_gpu(a_gpu,b_gpu,e,0,0);

    block_cpu.init();
    block_gpu.init();

    fp max_cpu = block_cpu.check_reduce_domain_bounds(0.1); 
    fp max_gpu = block_gpu.check_reduce_domain_bounds(0.1); 
    BOOST_CHECK_SMALL( abs(max_cpu-max_gpu), 1e-16 );

    block_cpu.reduce_domain_bounds(0.1); 
    block_gpu.reduce_domain_bounds(0.1); 

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    block_gpu.swap();
    block_cpu.swap();

    fp error = inf_norm<dx+dv>(block_cpu.get_data().data,
                               block_gpu.get_data().data);

    cout << "error: " << error << endl;
    BOOST_CHECK_SMALL( error, 1e-13 );
}











