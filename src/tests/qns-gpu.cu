#include <algorithms/qns-gpu.hpp>


#define BOOST_TEST_MAIN QNS_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

//modifies data_hat
void compute_derivative(int dim, array<fp,3> a, array<fp,3> b,
                        int Ntheta, int Nz, int Nr, cucmplx* data_hat){

    if(dim<2||dim>3){
        cout << "only derivative in theta or z allowed" << endl;
        exit(1);
    }

    fp normalization = 1.0/(Ntheta*Nz);

    for(int iz=0;iz<Nz;++iz){
        fp freq_z = ((iz<Nz/2+1) ? iz : -(Nz-iz))*2.0*M_PI/(b[2]-a[2]);
        for(int it=0;it<Ntheta/2+1;++it){
            fp freq_t = it*2.0*M_PI/(b[1]-a[1]);
            for(int ir=0;ir<Nr;++ir){

                fp factor = normalization*((dim==2) ? freq_t : freq_z);

                int lidx = ir + Nr*(it + (Ntheta/2+1)*iz);

                //data_hat[lidx] *= factor;
                data_hat[lidx] = {-factor*data_hat[lidx].y,
                                   factor*data_hat[lidx].x};

            }
        }
    }
}


fp check_fft(int N, int dim_derivative){

    int Nx = N;
    int Ny = N+10;
    int Nz = N+50;

    fp a = 0.0;
    fp b = 2.0*M_PI;
    fp hx = (b-a)/fp(Nx);
    fp hy = (b-a)/fp(Ny);
    fp hz = (b-a)/fp(Nz);

    generic_container<fp> in(Nx*Ny*Nz);
    generic_container<cucmplx> out(Nx*(Ny/2+1)*Nz);

    fft_2d_in_3d_gpu fft{Nx,Ny,Nz};

    for(int k=0;k<Nz;++k)
        for(int j=0;j<Ny;++j)
            for(int i=0;i<Nx;++i){

                fp x = a + i*hx;
                fp y = a + j*hy;
                fp z = a + k*hz;

                y = y - (a+b)*0.5;
                z = z - (a+b)*0.5;

                in.h_data[i+Nx*(j+Ny*k)] = sin(x)*exp(-4*y*y)*exp(-4.0*z*z);

            }

    in.copy_to_gpu();

    fft.forward(in.d_data, out.d_data);

    out.copy_to_host();
    compute_derivative(dim_derivative, {a,a,a}, {b,b,b}, Ny, Nz, Nx, 
                       out.h_data.data());
    out.copy_to_gpu();

    fft.backward(out.d_data, in.d_data);

    in.copy_to_host();

    fp maxerr = 0.0;
    for(int k=0;k<Nz;++k)
        for(int j=0;j<Ny;++j)
            for(int i=0;i<Nx;++i){

                fp x = a + i*hx;
                fp y = a + j*hy;
                fp z = a + k*hz;

                y = y - (a+b)*0.5;
                z = z - (a+b)*0.5;

                fp ref;
                if(dim_derivative==2)
                    ref = -8.0*y*sin(x)*exp(-4.0*y*y)*exp(-4.0*z*z);
                if(dim_derivative==3)
                    ref = -8.0*z*sin(x)*exp(-4.0*y*y)*exp(-4.0*z*z);

                maxerr = std::max(maxerr, fabs(ref-in.h_data[i+Nx*(j+Ny*k)]));

            }

    return maxerr;
}


BOOST_AUTO_TEST_CASE( qns_fft_test_2din3d ){

#ifdef USE_SINGLE_PRECISION
    BOOST_CHECK( check_fft(30, 2)<5e-6 );
    BOOST_CHECK( check_fft(30, 3)<6e-6 );
#else
    BOOST_CHECK( check_fft(30, 2)<1.3e-11 );
    BOOST_CHECK( check_fft(30, 3)<5.6e-15 );
#endif

}




fp K(fp r){return r*r*0.5;};
fp Kprime(fp r){return r;};
fp alpha(fp r, int k, int l){return sin(r)*(l!=0) + k*k;};
fp exact_qns(fp r, fp t, fp z){
   return (-1.0+cos(2.0*M_PI*r))*(cos(2.0*t + z)+r*sin(t));
};
fp rhs_qns(fp r, fp t, fp z){
    fp d0  = (-1.0+cos(2.0*M_PI*r))*(cos(2.0*t + z)+r*sin(t));
    fp dr  = -2.0*M_PI*sin(2.0*M_PI*r)*
                (cos(2.0*t + z)+r*sin(t)) +
             (-1.0+cos(2.0*M_PI*r))*sin(t);
    fp drr = -pow(2.0*M_PI,2)*cos(2.0*M_PI*r)*
                (cos(2.0*t + z)+r*sin(t)) +
             -2.0*M_PI*sin(2.0*M_PI*r)*sin(t)*2.0;
    fp dtt = -4.0*(-1.0+cos(2.0*M_PI*r))*
              (cos(2.0*t + z)+1.0/4.0*sin(t)*r);
    fp mean_z = (-1.0+cos(2.0*M_PI*r))*r*sin(t);

    return -K(r)*drr-Kprime(r)*dr -dtt + alpha(r,0,1)*(d0 - mean_z);//
};


template<size_t o2d, size_t oz>
fp check_qns_neumann(int N){

    array<fp,3> a = {1.0,0.0,0.0};
    array<fp,3> b = {2.0,2*M_PI,2.0*M_PI};

    //amount of cells
    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b[0]-a[0])/fp(Nr);
    fp ht = (b[1]-a[1])/fp(Ntheta);
    fp hz = (b[2]-a[2])/fp(Nz*oz);

    quasi_neutrality_gpu<o2d,oz> qns{a,b,Nr,Ntheta,Nz,true,K,alpha};

    for(size_t k=0;k<Nz*oz;++k){
        for(int j=0;j<Ntheta;++j){
            for(int i=0;i<Nr+1;++i){

                fp r = a[0] + i*hr;
                fp t = a[1] + j*ht;
                fp z = a[2] + hz*(k + 0.5);

                qns.rho_cc.h_data[i+(Nr+1)*(j+Ntheta*k)] = rhs_qns(r,t,z);
            }
        }
    }

    qns.rho_cc.copy_to_gpu();

    qns.ode_fd->solve_qns(qns.rho_cc.d_data, qns.phi_cc.d_data);

    qns.phi_cc.copy_to_host();

    fp maxerr = 0.0;
    for(size_t k=0;k<Nz*oz;++k){
        for(int j=0;j<Ntheta;++j){
            for(int i=0;i<Nr+1;++i){
                fp r = a[0] + i*hr;
                fp t = a[1] + j*ht;
                fp z = a[2] + hz*(k + 0.5);

                fp val = qns.phi_cc.h_data[i+(Nr+1)*(j+Ntheta*k)];
                fp ref = exact_qns(r,t,z);

                fp err = val-ref;

                maxerr = std::max(maxerr,fabs(err));
            }
        }
    }

    return maxerr;
}

BOOST_AUTO_TEST_CASE( qns_quasi_neutrality_only ){

    fp maxerr_old = -1.0;
    for(int N=20;N<81;N*=2){
        fp maxerr = check_qns_neumann<2,2>(N);
        if(maxerr_old>0.0){
            fp order = -log(maxerr/maxerr_old)/log(2.0);
            BOOST_CHECK( order > 1.95 );
        }
        maxerr_old = maxerr;
    }
    BOOST_CHECK( maxerr_old < 2.5e-3 );

}



fp not_used1(fp x){return x;};
fp not_used2(fp x, int i, int j){return x+i+j;};


template<size_t o2d, size_t oz>
std::pair<fp,fp> check_interpolation(int N){

    fp a = 0.0;
    fp b = 2.0*M_PI;

    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b-a)/fp(Nr);
    fp ht = (b-a)/fp(Ntheta);
    fp hz = (b-a)/fp(Nz);

    quasi_neutrality_gpu<o2d,oz> qns{{a,a,a},{b,b,b},Nr,Ntheta,Nz,
                                     true, not_used1, not_used2};

    auto f0 = [](fp r, fp t, fp z){
        return cos(r)*sin(t)*cos(z);
    };

    generic_container<fp> rho_dg_nodes(Nr*Ntheta*o2d*o2d*Nz*oz);
    int idx=0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){
                    for(size_t jy=0;jy<o2d;++jy){
                        for(size_t jx=0;jx<o2d;++jx){

                            fp r = a + hr*(i+gauss::x_scaled01(jx,o2d));
                            fp t = a + ht*(j+gauss::x_scaled01(jy,o2d));
                            fp z = a + hz*(k+gauss::x_scaled01(jz,oz));

                            rho_dg_nodes.h_data[idx++] = f0(r,t,z);

                        }
                    }
                }
            }
        }
    }
    rho_dg_nodes.copy_to_gpu();

    qns.interpolate_z_dg_to_equi(rho_dg_nodes.d_data, rho_dg_nodes.d_data);

    rho_dg_nodes.copy_to_host();
    idx=0;
    fp hz_e = (b-a)/fp(Nz*oz);
    fp err_z_dg_to_equi = 0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            fp err_z = 0.0;
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){
                    for(size_t jy=0;jy<o2d;++jy){
                        for(size_t jx=0;jx<o2d;++jx){

                            fp r = a + hr*(i+gauss::x_scaled01(jx,o2d));
                            fp t = a + ht*(j+gauss::x_scaled01(jy,o2d));
                            fp z = a + hz_e*(0.5 + jz+k*oz);

                            fp err = rho_dg_nodes.h_data[idx++] - f0(r,t,z);

                            err_z = std::max(err_z,fabs(err));

                        }
                    }
                }
            }
            err_z_dg_to_equi = std::max(err_z_dg_to_equi,err_z);
        }
    }

    qns.evaluate_at_grid_corners(rho_dg_nodes.d_data, qns.rho_cc.d_data);

    qns.rho_cc.copy_to_host();

    idx=0;
    fp err_to_cc=0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            fp err_k = 0.0;
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz_e*(0.5 + jz+k*oz);

                    fp out = qns.rho_cc.h_data[idx++];

                    fp err = out - f0(r,t,z);
                    err_k = std::max(err_k,fabs(err));
                }
            }
            err_to_cc = std::max(err_to_cc,fabs(err_k));
        }
    }

    return {err_z_dg_to_equi, err_to_cc};
}


BOOST_AUTO_TEST_CASE( qns_interpolation_to_equi ){

    fp err_old_cc = -1.0;
    fp err_old_dg = -1.0;
    for(int N=20;N<81;N*=2){
        auto err = check_interpolation<2,2>(N);
        if(err_old_cc > 0.0){
            fp order_cc = -log(err.second/err_old_cc)/log(2.0);
            fp order_dg = -log(err.first/err_old_dg)/log(2.0);
            BOOST_CHECK( order_cc > 1.98 );
            BOOST_CHECK( order_dg > 1.98 );
        }
        err_old_cc = err.second;
        err_old_dg = err.first;
    }
    BOOST_CHECK( err_old_cc < 1e-3);
    BOOST_CHECK( err_old_dg < 6.5e-5);

}


template<size_t o2d, size_t oz>
std::tuple<fp,fp,fp> check_derivatives(int N){

    fp a = 0.0;
    fp b = 2.0*M_PI;

    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b-a)/fp(Nr);
    fp ht = (b-a)/fp(Ntheta);

    quasi_neutrality_gpu<o2d,oz> qns{{a,a,a},{b,b,b},Nr,Ntheta,Nz,
                                     true, not_used1, not_used2};

    auto f0 = [](fp r, fp t, fp z){ return (-1.0+cos(r))*cos(t)*cos(z); };
    auto dr_f = [](fp r, fp t, fp z){ return -sin(r)*cos(t)*cos(z); };
    auto dt_f = [](fp r, fp t, fp z){ return -(-1.0+cos(r))*sin(t)*cos(z); };
    auto dz_f = [](fp r, fp t, fp z){ return -(-1.0+cos(r))*cos(t)*sin(z); };


    fp hz_e = (b-a)/fp(Nz*oz);
    int idx=0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz_e*(0.5 + jz+k*oz);

                    qns.phi_cc.h_data[idx++] = f0(r,t,z);

                }
            }
        }
    }

    qns.phi_cc.copy_to_gpu();

    qns.compute_all_derivatives();

    qns.dr_phi_cc.copy_to_host();
    qns.dtheta_phi_cc.copy_to_host();
    qns.dz_phi_cc.copy_to_host();

    idx=0;
    fp maxerr_r=0.0;
    fp maxerr_t=0.0;
    fp maxerr_z=0.0;
    std::ofstream output("out.data");
    std::ofstream reference("ref.data");
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz_e*(0.5 + jz+k*oz);

                    fp dr_f_exact = dr_f(r,t,z);
                    fp dt_f_exact = dt_f(r,t,z);
                    fp dz_f_exact = -dz_f(r,t,z);

                    if(i==0){
                        output << t << " " << z << " "
                               << qns.dz_phi_cc.h_data[idx] << endl;
                        reference  << t << " " << z << " " << dz_f_exact << endl;

                    }
                    fp err_r = dr_f_exact - qns.dr_phi_cc.h_data[idx];
                    fp err_t = dt_f_exact - qns.dtheta_phi_cc.h_data[idx];
                    fp err_z = dz_f_exact - qns.dz_phi_cc.h_data[idx];

                    idx++;

                    maxerr_r = std::max(maxerr_r,fabs(err_r));
                    maxerr_t = std::max(maxerr_t,fabs(err_t));
                    maxerr_z = std::max(maxerr_z,fabs(err_z));
                }
            }
        }
    }


    return {maxerr_r, maxerr_t, maxerr_z};
}


BOOST_AUTO_TEST_CASE( qns_derivatives ){

    fp err_r_old = -1.0;
    fp err_t_old = -1.0;
    fp err_z_old = -1.0;
    for(int N=20;N<81;N*=2){
        auto err = check_derivatives<2,2>(N);
        if(err_r_old>0.0){
            fp order_r = -log(std::get<0>(err)/err_r_old)/log(2.0);
            fp order_t = -log(std::get<1>(err)/err_t_old)/log(2.0);
            fp order_z = -log(std::get<2>(err)/err_z_old)/log(2.0);
            BOOST_CHECK( order_r > 1.99 );
            BOOST_CHECK( order_t > 1.99 );
            BOOST_CHECK( order_z > 1.99 );
        }
        err_r_old = std::get<0>(err);
        err_t_old = std::get<1>(err);
        err_z_old = std::get<2>(err);
    }
    BOOST_CHECK( err_r_old < 1.2e-3 );
    BOOST_CHECK( err_t_old < 2.2e-3 );
    BOOST_CHECK( err_z_old < 5.2e-4 );
}



template<size_t o2d, size_t oz>
std::pair<fp,fp> check_interpolation_equi_to_dg(int N){

    fp a = 0.0;
    fp b = 2.0*M_PI;

    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b-a)/fp(Nr);
    fp ht = (b-a)/fp(Ntheta);
    fp hz = (b-a)/fp(Nz);


    quasi_neutrality_gpu<o2d,oz> qns{{a,a,a},{b,b,b},Nr,Ntheta,Nz,
                                     true, not_used1, not_used2};

    auto f0 = [](fp r, fp t, fp z){
        return cos(r)*sin(t)*cos(z);
    };

    int idx=0;
    fp hz_e = (b-a)/fp(Nz*oz);
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz_e*(0.5 + jz+k*oz);

                    qns.dr_phi_cc.h_data[idx++] = f0(r,t,z);

                }
            }
        }
    }

    qns.dr_phi_cc.copy_to_gpu();

    qns.interpolate_z_equi_to_dg(qns.dr_phi_cc.d_data, qns.dr_phi_cc.d_data);

    qns.dr_phi_cc.copy_to_host();

    idx=0;
    fp err_z_equi_to_dg = 0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            fp err_z = 0.0;
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz*(k+gauss::x_scaled01(jz,oz));

                    fp err = qns.dr_phi_cc.h_data[idx++] - f0(r,t,z);

                    err_z = std::max(err_z,fabs(err));

                }
            }
            err_z_equi_to_dg = std::max(err_z_equi_to_dg,err_z);
        }
    }

    qns.interpolate_cc_to_dg(qns.dr_phi_cc.d_data, qns.dr_phi.d_data);

    qns.dr_phi.copy_to_host();

    idx=0;
    fp maxerr_from_cc=0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){
                    for(size_t jy=0;jy<o2d;++jy){
                        for(size_t jx=0;jx<o2d;++jx){

                            fp r = a + hr*(i+gauss::x_scaled01(jx,o2d));
                            fp t = a + ht*(j+gauss::x_scaled01(jy,o2d));
                            fp z = a + hz*(k+gauss::x_scaled01(jz,oz));

                            fp err = qns.dr_phi.h_data[idx++] - f0(r,t,z);
                            maxerr_from_cc = std::max(maxerr_from_cc,
                                                      fabs(err));
                        }
                    }
                }
            }
        }
    }

    return {err_z_equi_to_dg,maxerr_from_cc};
}

BOOST_AUTO_TEST_CASE( qns_interpolation_to_dg ){

    fp errold_dg = -1.0;
    fp errold_cc = -1.0;
    for(int N=20;N<81;N*=2){
        auto err = check_interpolation_equi_to_dg<2,2>(N);
        if(errold_cc>0.0){
            fp order_cc = -log(err.second/errold_cc)/log(2);
            fp order_dg = -log(err.first/errold_dg)/log(2);
            BOOST_CHECK( order_cc > 1.98 );
            BOOST_CHECK( order_dg > 1.98 );
        }
        errold_dg = err.first;
        errold_cc = err.second;
    }
    BOOST_CHECK( errold_dg < 6.5e-5 );
    BOOST_CHECK( errold_cc < 1e-3 );

}

//    cout << "\nchek electric_energy:" << endl;
//    check_energy<o2d,oz>(20);
//
//    cout << "\ncheck total: " << endl;
//    check_all<o2d,oz,2>(40);
//    check_all<o2d,oz,2>(80);
//



//TODO: reduction not checked here!
template<size_t o2d, size_t oz, size_t ov>
array<fp,6> check_all(int N){

    array<fp,3> a = {1.0,0.0,0.0};
    array<fp,3> b = {2.0,2*M_PI,2.0*M_PI};

    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b[0]-a[0])/fp(Nr);
    fp ht = (b[1]-a[1])/fp(Ntheta);
    fp hz = (b[2]-a[2])/fp(Nz);


    quasi_neutrality_gpu<o2d,oz> qns{a,b,Nr,Ntheta,Nz,true,K,alpha};

    int idx=0;
    for(int l=0;l<Nz;++l){
        for(size_t ll=0;ll<oz;++ll){

            fp z = a[2] + hz*(l+gauss::x_scaled01(ll,oz));

            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){
                    for(size_t jj=0;jj<o2d;++jj){
                        for(size_t ii=0;ii<o2d;++ii){

                            fp r = a[0] + hr*(i+gauss::x_scaled01(ii,o2d));
                            fp t = a[1] + ht*(j+gauss::x_scaled01(jj,o2d));

                            qns.rho.h_data[idx++] = rhs_qns(r,t,z);

                        }
                    }
                }
            }
        }
    }
    qns.rho.copy_to_gpu();

    qns.solve();

    auto exact_dr = [](fp r, fp t, fp z){
       return -2.0*M_PI*sin(2.0*M_PI*r)*(cos(2.0*t + z)+r*sin(t))+
              (-1.0+cos(2.0*M_PI*r))*sin(t);
    };
    auto exact_dt = [](fp r, fp t, fp z){
       return (-1.0+cos(2.0*M_PI*r))*(-2.0*sin(2.0*t + z)+r*cos(t));
    };
    auto exact_dz = [](fp r, fp t, fp z){
       return +(-1.0+cos(2.0*M_PI*r))*sin(2.0*t + z); //- -> + since minus is required
    };

    qns.dr_phi_cc.copy_to_host();
    qns.dtheta_phi_cc.copy_to_host();
    qns.dz_phi_cc.copy_to_host();

    idx=0;
    fp err_r_cc = 0.0;
    fp err_t_cc = 0.0;
    fp err_z_cc = 0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a[0] + hr*i;
                    fp t = a[1] + ht*j;
                    fp z = a[2] + hz*(k+gauss::x_scaled01(jz,oz));

                    fp err_r = qns.dr_phi_cc.h_data[idx] - exact_dr(r,t,z);
                    fp err_t = qns.dtheta_phi_cc.h_data[idx] - exact_dt(r,t,z);
                    fp err_z = qns.dz_phi_cc.h_data[idx] - exact_dz(r,t,z);

                    idx++;

                    err_r_cc = std::max(err_r_cc,fabs(err_r));
                    err_t_cc = std::max(err_t_cc,fabs(err_t));
                    err_z_cc = std::max(err_z_cc,fabs(err_z));

                }
            }
        }
    }

    array<fp,6> err;
    err[0] = err_r_cc;
    err[1] = err_t_cc;
    err[2] = err_z_cc;

    qns.dr_phi.copy_to_host();
    qns.dtheta_phi.copy_to_host();
    multi_array<fp,6> dz_phi;
    dz_phi.resize({Nr,Ntheta,Nz,o2d,o2d,oz});
    dz_phi = qns.dz_phi;

    idx=0;
    //std::ofstream output("out.data");
    //std::ofstream input("in.data");
    fp err_r_dg = 0.0;
    fp err_t_dg = 0.0;
    fp err_z_dg = 0.0;
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr;++i){
                    for(size_t jy=0;jy<o2d;++jy){
                        for(size_t jx=0;jx<o2d;++jx){

                            fp r = a[0] + hr*(i+gauss::x_scaled01(jx,o2d));
                            fp t = a[1] + ht*(j+gauss::x_scaled01(jy,o2d));
                            fp z = a[2] + hz*(k+gauss::x_scaled01(jz,oz));

                            fp err_r = qns.dr_phi.h_data[idx] - exact_dr(r,t,z);
                            fp err_t = qns.dtheta_phi.h_data[idx] -
                                           exact_dt(r,t,z);
                            fp err_z = dz_phi.v[idx] - exact_dz(r,t,z);

                            idx++;

                            err_r_dg = std::max(err_r_dg,fabs(err_r));
                            err_t_dg = std::max(err_t_dg,fabs(err_t));
                            err_z_dg = std::max(err_z_dg,fabs(err_z));


                        }
                    }
                }
            }
        }
    }

    err[3] = err_r_dg;
    err[4] = err_t_dg;
    err[5] = err_z_dg;

    return err;

}


BOOST_AUTO_TEST_CASE( qns_total ){

    auto err40 = check_all<2,2,2>(40);
    auto err80 = check_all<2,2,2>(80);

    BOOST_CHECK( -log(err80[0]/err40[0])/log(2.0) > 1.96 );
    BOOST_CHECK( -log(err80[1]/err40[1])/log(2.0) > 1.99 );
    BOOST_CHECK( -log(err80[2]/err40[2])/log(2.0) > 1.98 );
    BOOST_CHECK( -log(err80[3]/err40[3])/log(2.0) > 1.99 );
    BOOST_CHECK( -log(err80[4]/err40[4])/log(2.0) > 1.99 );
    BOOST_CHECK( -log(err80[5]/err40[5])/log(2.0) > 1.99 );

    BOOST_CHECK( err80[0] < 2.1e-2);
    BOOST_CHECK( err80[1] < 7e-3 );
    BOOST_CHECK( err80[2] < 5e-3 );
    BOOST_CHECK( err80[3] < 1.4e-2 );
    BOOST_CHECK( err80[4] < 1.7e-2 );
    BOOST_CHECK( err80[5] < 8e-4 );
}


template<size_t o2d, size_t oz>
fp check_energy(int N){

    fp a = 0.0;
    fp b = 2.0*M_PI;

    int Nr = N;
    int Ntheta = N;
    int Nz = N;

    fp hr = (b-a)/fp(Nr);
    fp ht = (b-a)/fp(Ntheta);

    quasi_neutrality_gpu<o2d,oz> qns{{a,a,a},{b,b,b},Nr,Ntheta,Nz,
                                     true, not_used1, not_used2};

    auto f0 = [](fp r, fp t, fp z){
        return r*sin(t)*sin(z);
    };

    int idx=0;
    fp hz_e = (b-a)/fp(Nz*oz);
    for(int k=0;k<Nz;++k){
        for(size_t jz=0;jz<oz;++jz){
            for(int j=0;j<Ntheta;++j){
                for(int i=0;i<Nr+1;++i){

                    fp r = a + hr*i;
                    fp t = a + ht*j;
                    fp z = a + hz_e*(0.5 + jz+k*oz);

                    qns.phi_cc.h_data[idx++] = f0(r,t,z);

                }
            }
        }
    }
    qns.phi_cc.copy_to_gpu();

    qns.compute_electric_energy();

    cout << "electric energy: " << qns.electric_energy << endl;
    return abs(qns.electric_energy - M_PI*M_PI);
}


BOOST_AUTO_TEST_CASE( qns_energy ){

    fp err= check_energy<2,2>(20);
#ifdef USE_SINGLE_PRECISION
    BOOST_CHECK( err<2e-6 );
#else
    BOOST_CHECK( err<2e-15 );
#endif

}

