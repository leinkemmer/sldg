#include <algorithms/limiter.hpp>
#include <algorithms/limiter-gpu.cu>
#include <programs/vp-block.hpp>

#define BOOST_TEST_MAIN LIMITER_GPU
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>

bool enable_output = true;


BOOST_AUTO_TEST_CASE( limiter_gpu ){
    
    //checks against results obtained on cpu; 
    //TODO: are the used functions too trivial to check properly?

    constexpr size_t dx = 1;
    constexpr size_t dv = 1;
    constexpr size_t d = dx+dv;

    Index Nx = 190;
    Index Nv = 210;
    Index o = 4;

    array<Index,2*d> e = {Nx,Nv,o,o};

    gpu_array<Index> d_e(2*d);
    d_e.set(e.begin(),e.end());
    d_e.copy_to_gpu();

    multi_array<fp,2*d> h_in(e);
    multi_array<fp,2*d> d_in(e,true);
    multi_array<fp,2*d> h_out(e);
    multi_array<fp,2*d> d_out(e,true);
    multi_array<fp,2*d> h_out_from_gpu(e);

    array<Index,2*d> idx={0};
    do{
        h_in(idx) = rand()%10;
    } while(iter_next_memorder(idx,e));

    d_in = h_in;

    //cout << "input: " << endl;
    //do{
    //    cout << h_in(idx) << " ";
    //    if(idx[2]+idx[0]*e[2]==Nx*o-1)
    //        cout << "\n";
    //} while(iter_next_memorder(idx,e));

    weno_1d_limiter<dx,dv> wl(d_e,location::all);

    int dim = 0;
    wl.apply_weno_limiter_1d_gpu(d_in, d_out, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    weno_limiter_1d_k<d>(o,dim,h_in,h_out,e,
                         troubled_cell_indicator::minmod,
                         troubled_cell_modifier::linear_weno);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    h_out_from_gpu = d_out;

    fp maxerr0 = inf_norm<d>(h_out,h_out_from_gpu);
    cout << "maxerr0: " << maxerr0 << endl;

    BOOST_CHECK_SMALL(abs(maxerr0),1.5e-14);

    dim=1;
    d_in = h_in;
    wl.apply_weno_limiter_1d_gpu(d_in, d_out, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    weno_limiter_1d_k<d>(o,dim,h_in,h_out,e,
                         troubled_cell_indicator::minmod,
                         troubled_cell_modifier::linear_weno);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    h_out_from_gpu = d_out;

    fp maxerr1 = inf_norm<d>(h_out,h_out_from_gpu);
    cout << "maxerr1: " << maxerr1 << endl;

    BOOST_CHECK_SMALL(abs(maxerr1),1.1e-14);

    //cout << "gpu_out: " << endl;
    //do{
    //    cout << h_out_from_gpu(idx) << " ";
    //    if(idx[2]+idx[0]*e[2]==Nx*o-1)
    //        cout << "\n";
    //} while(iter_next_memorder(idx,e));

    //cout << "cpu_out: " << endl;
    //do{
    //    cout << h_out(idx) << " ";
    //    if(idx[2]+idx[0]*e[2]==Nx*o-1)
    //        cout << "\n";
    //} while(iter_next_memorder(idx,e));

    //cout << "diff: " << endl;
    //do{
    //    cout << h_out(idx)-h_out_from_gpu(idx) << " ";
    //    if(idx[2]+idx[0]*e[2]==Nx*o-1)
    //        cout << "\n";
    //} while(iter_next_memorder(idx,e));


}

__host__ __device__
fp f0(fp* xv){
    fp x = xv[0];
    fp v = xv[1];
    //polynomial of degree 3 in x
    return (x*x*x - x)*sin(M_PI*v);
}


void test_pack(int N, int ratio){

    constexpr size_t dx=1;
    constexpr size_t dv=1;
    constexpr size_t d=dx+dv;

    constexpr Index o=4;

    gpu_array<Index> d_e(2*d);
    array<Index,2*d> ext;
    for(int i=0;i<2;++i){
        ext[i]=N+i*2;
        d_e[i]=N+i*2;
        ext[i+d]=o;
        d_e[i+d]=o;
    }
    d_e.copy_to_gpu();

    //domain = [-1,1]x[-1,1]
    fp h_bdry = 2.0/fp(d_e[0]*(ratio+2));

    gpu_array<fp> d_gauss_x(d*o);
    int offset=0;
    for(size_t i=0;i<d;++i){
        vector<fp> gx = gauss::all_x_scaled01(ext[d+i]);
        copy(begin(gx), end(gx), &d_gauss_x[offset]);
        offset += ext[d+i];
    }
    d_gauss_x.copy_to_gpu();

    multi_array<fp,2*d> d_in_l(ext,true);
    multi_array<fp,2*d> d_in_m(ext,true);
    multi_array<fp,2*d> d_in_r(ext,true);

    int dof=prod(ext);
    constexpr fp ax =-1.0;
    constexpr fp bx = 1.0;
    gpu_array<fp> d_a(d); d_a[1] = ax;
    gpu_array<fp> d_b(d); d_b[1] = bx;

    d_a[0] = -1.0;
    d_b[0] = -1.0 + h_bdry*N;
    d_a.copy_to_gpu();
    d_b.copy_to_gpu();
    cout << "domain l: " << d_a[0] << "," << d_b[0] << endl;
    init_gpu<d,f0><<<(dof+127)/128,128>>>(d_in_l.data(),d_a.d_data,
                                          d_b.d_data, d_e.d_data,
                                          d_gauss_x.d_data,dof);
    d_a[0] = d_b[0];
    d_b[0] = d_b[0] + ratio*h_bdry*N;
    d_a.copy_to_gpu();
    d_b.copy_to_gpu();
    cout << "domain m: " << d_a[0] << "," << d_b[0] << endl;
    init_gpu<d,f0><<<(dof+127)/128,128>>>(d_in_m.data(),d_a.d_data,
                                          d_b.d_data, d_e.d_data,
                                          d_gauss_x.d_data,dof);
    d_a[0] = d_b[0];
    d_b[0] = d_b[0] + h_bdry*N;
    d_a.copy_to_gpu();
    d_b.copy_to_gpu();
    cout << "domain r: " << d_a[0] << "," << d_b[0] << endl;
    init_gpu<d,f0><<<(dof+127)/128,128>>>(d_in_r.data(),d_a.d_data,
                                          d_b.d_data, d_e.d_data,
                                          d_gauss_x.d_data,dof);

    array<Index,2*d> e_bdry = ext;
    e_bdry[0]=1; //one cells
    multi_array<fp,2*d> d_bdry_data_ll(ext,true);
    multi_array<fp,2*d> d_bdry_data_lr(ext,true);
    multi_array<fp,2*d> d_bdry_data_ml(ext,true);
    multi_array<fp,2*d> d_bdry_data_mr(ext,true);
    multi_array<fp,2*d> d_bdry_data_rl(ext,true);
    multi_array<fp,2*d> d_bdry_data_rr(ext,true);

    int threads=128;
    int dof_v1 = ext[1]*ext[d+1];
    pack_all_for_limiter_xdim<dv><<<(dof_v1+threads-1)/threads,threads>>>(
                              location::left, d_bdry_data_ll.data(),
                              d_bdry_data_lr.data(),
                              ratio, d_in_l.data(), d_e.d_data);
    pack_all_for_limiter_xdim<dv><<<(dof_v1+threads-1)/threads,threads>>>(
                              location::mid, d_bdry_data_ml.data(),
                              d_bdry_data_mr.data(),
                              ratio, d_in_m.data(), d_e.d_data);
    pack_all_for_limiter_xdim<dv><<<(dof_v1+threads-1)/threads,threads>>>(
                              location::right, d_bdry_data_rl.data(),
                              d_bdry_data_rr.data(),
                              ratio, d_in_r.data(), d_e.d_data);

    multi_array<fp,2*d> bdry_data_ll(e_bdry);
    multi_array<fp,2*d> bdry_data_lr(e_bdry);
    multi_array<fp,2*d> bdry_data_ml(e_bdry);
    multi_array<fp,2*d> bdry_data_mr(e_bdry);
    multi_array<fp,2*d> bdry_data_rl(e_bdry);
    multi_array<fp,2*d> bdry_data_rr(e_bdry);

    bdry_data_ll = d_bdry_data_ll;
    bdry_data_lr = d_bdry_data_lr;
    bdry_data_ml = d_bdry_data_ml;
    bdry_data_mr = d_bdry_data_mr;
    bdry_data_rl = d_bdry_data_rl;
    bdry_data_rr = d_bdry_data_rr;

    fp hv=(d_b[1]-d_a[1])/fp(d_e[1]);
    fp b_left   = ax + h_bdry*ext[0];
    fp b_right  = bx - h_bdry*ext[0];
    cout << "grid size transitions: " << b_left << ", " << b_right << endl;
    fp x[2];
    fp h_interior = h_bdry*ratio;
    for(int i=0;i<ext[1];++i){
        for(int j=0;j<o;++j){

            fp v = d_a[1] + hv*(i + gauss::get_x_scaled01(j,o));

            x[1] = v;
            for(int k=0;k<o;++k){
                fp ref_ll = 0.0;
                x[0] = b_left - h_interior + h_interior*gauss::get_x_scaled01(k,o);
                fp ref_lr = f0(x);
                BOOST_CHECK_SMALL(abs(bdry_data_ll.v[k+o*(j+o*i)]-ref_ll), 6e-16);
                BOOST_CHECK_SMALL(abs(bdry_data_lr.v[k+o*(j+o*i)]-ref_lr), 6e-16);
            }
            for(int k=0;k<o;++k){
                x[0] = b_left + h_bdry*gauss::get_x_scaled01(k,o);
                fp ref_ml = f0(x);
                x[0] = b_right - h_bdry + h_bdry*gauss::get_x_scaled01(k,o);
                fp ref_mr = f0(x);
                BOOST_CHECK_SMALL(abs(bdry_data_ml.v[k+o*(j+o*i)]-ref_ml), 6e-16);
                BOOST_CHECK_SMALL(abs(bdry_data_mr.v[k+o*(j+o*i)]-ref_mr), 6e-16);
            }
            for(int k=0;k<o;++k){
                x[0] = b_right + h_interior*gauss::get_x_scaled01(k,o);
                fp ref_rl = f0(x);
                fp ref_rr = 0.0;
                BOOST_CHECK_SMALL(abs(bdry_data_rl.v[k+o*(j+o*i)]-ref_rl), 6e-16);
                BOOST_CHECK_SMALL(abs(bdry_data_rr.v[k+o*(j+o*i)]-ref_rr), 6e-16);
            }
        }
    }

}

BOOST_AUTO_TEST_CASE( pack_all_for_weno_xdim ){

    //different ratios are tested
    test_pack(10, 10);
    test_pack(100, 10);
    test_pack(10, 5);
    test_pack(100, 5);
    test_pack(210, 1);
}


BOOST_AUTO_TEST_CASE( limiter_with_boundary_transfer ){

    //this tests pack+limiter (of splitted domain) = limiter (of whole domain) 
    //using 1 gpu (for multiple gpus, only device settings and copying changes)
    //ratio of 1 is used. The limiter does not care about it, only the pack 
    //function, where checks for different ratios are done in another test.

    constexpr size_t dx = 1;
    constexpr size_t dv = 1;
    constexpr size_t d = dx+dv;
    constexpr int ratio = 1;

    Index Nx = 50;
    Index Nv = 170;
    Index o = 4;

    array<Index,2*d> e_split = {Nx,Nv,o,o};
    gpu_array<Index> d_e_split(2*d);
    d_e_split.set(e_split.begin(),e_split.end());
    d_e_split.copy_to_gpu();

    array<Index,2*d> e_full = {3*Nx,Nv,o,o};
    gpu_array<Index> d_e_full(2*d);
    d_e_full.set(e_full.begin(),e_full.end());
    d_e_full.copy_to_gpu();

    //GPU
    multi_array<fp,2*d> d_in_full(e_full,true);
    multi_array<fp,2*d> d_out_full(e_full,true);
    multi_array<fp,2*d> d_in_left(e_split,true);
    multi_array<fp,2*d> d_out_left(e_split,true);
    multi_array<fp,2*d> d_in_mid(e_split,true);
    multi_array<fp,2*d> d_out_mid(e_split,true);
    multi_array<fp,2*d> d_in_right(e_split,true);
    multi_array<fp,2*d> d_out_right(e_split,true);

    //initialize on CPU
    multi_array<fp,2*d> h_in_full(e_full);
    multi_array<fp,2*d> h_in_left(e_split);
    multi_array<fp,2*d> h_in_mid(e_split);
    multi_array<fp,2*d> h_in_right(e_split);

    for(int iv=0;iv<Nv*o;++iv){
        for(int n=0;n<3;++n){
            for(int ix=0;ix<Nx*o;++ix){

                fp val = rand()%10;

                size_t idx_full = ix+Nx*o*(n + 3*iv);
                h_in_full.v[idx_full] = val;

                size_t idx_split = ix+Nx*o*iv;
                if(n==0)
                    h_in_left.v[idx_split] = val;
                if(n==1)
                    h_in_mid.v[idx_split] = val;
                if(n==2) 
                    h_in_right.v[idx_split] = val;
            }
        }
    }
    //and copy to GPU
    d_in_full = h_in_full;
    d_in_left = h_in_left;
    d_in_mid  = h_in_mid;
    d_in_right = h_in_right;

    weno_1d_limiter<dx,dv> wl_full(d_e_full,location::all);
    weno_1d_limiter<dx,dv> wl_split_left(d_e_split,location::left);
    weno_1d_limiter<dx,dv> wl_split_mid(d_e_split,location::mid);
    weno_1d_limiter<dx,dv> wl_split_right(d_e_split,location::right);

    int dim = 0;
    wl_full.apply_weno_limiter_1d_gpu(d_in_full, d_out_full, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    weno_bdry_transfer_info info_left;
    weno_bdry_transfer_info info_mid;
    weno_bdry_transfer_info info_right;

    size_t sizel = wl_split_left.pack(ratio,d_in_left,
                       info_left.bdry_left_snd,
                       info_left.bdry_right_snd,
                       info_left.bdry_left_rcv,
                       info_left.bdry_right_rcv);
    size_t sizem = wl_split_mid.pack(ratio,d_in_mid,
                      info_mid.bdry_left_snd,
                      info_mid.bdry_right_snd,
                      info_mid.bdry_left_rcv,
                      info_mid.bdry_right_rcv);
    size_t sizer = wl_split_right.pack(ratio,d_in_right,
                        info_right.bdry_left_snd,
                        info_right.bdry_right_snd,
                        info_right.bdry_left_rcv,
                        info_right.bdry_right_rcv);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    //check if size is correct
    BOOST_CHECK_EQUAL(sizel, sizem);
    BOOST_CHECK_EQUAL(sizel, sizer);
    //transfer data
    gpuErrchk(cudaMemcpy(info_left.bdry_right_rcv, info_mid.bdry_left_snd, 
                         sizel, cudaMemcpyDeviceToDevice));
    gpuErrchk(cudaMemcpy(info_mid.bdry_left_rcv, info_left.bdry_right_snd, 
                         sizem, cudaMemcpyDeviceToDevice));
    gpuErrchk(cudaMemcpy(info_mid.bdry_right_rcv, info_right.bdry_left_snd, 
                         sizem, cudaMemcpyDeviceToDevice));
    gpuErrchk(cudaMemcpy(info_right.bdry_left_rcv, info_mid.bdry_right_snd, 
                         sizer, cudaMemcpyDeviceToDevice));
    //transfer data done
    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    wl_split_left.apply_weno_limiter_1d_gpu(d_in_left, d_out_left, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 
    wl_split_mid.apply_weno_limiter_1d_gpu(d_in_mid, d_out_mid, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 
    wl_split_right.apply_weno_limiter_1d_gpu(d_in_right, d_out_right, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    //copy to host and check
    h_in_full  = d_out_full;
    h_in_left  = d_out_left;
    h_in_mid   = d_out_mid;
    h_in_right = d_out_right;

    for(int iv=0;iv<Nv*o;++iv){
        for(int n=0;n<3;++n){
            for(int ix=0;ix<Nx*o;++ix){

                size_t idx_full = ix+Nx*o*(n + 3*iv);
                fp val_full = h_in_full.v[idx_full];

                size_t idx_split = ix+Nx*o*iv;
                if(n==0){
                    fp val_split_l = h_in_left.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_l );
                } if(n==1){
                    fp val_split_m = h_in_mid.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_m );
                }if(n==2) {
                    fp val_split_r = h_in_right.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_r );
                }
            }
        }
    }

 
    //use output data to apply limiter in v direction
    dim=1;
    wl_full.apply_weno_limiter_1d_gpu(d_out_full, d_in_full, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    wl_split_left.apply_weno_limiter_1d_gpu(d_out_left, d_in_left, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 
    wl_split_mid.apply_weno_limiter_1d_gpu(d_out_mid, d_in_mid, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 
    wl_split_right.apply_weno_limiter_1d_gpu(d_out_right, d_in_right, dim,
                                 troubled_cell_indicator::minmod,
                                 troubled_cell_modifier::linear_weno); 

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    h_in_full  = d_in_full;
    h_in_left  = d_in_left;
    h_in_mid   = d_in_mid;
    h_in_right = d_in_right;

    for(int iv=0;iv<Nv*o;++iv){
        for(int n=0;n<3;++n){
            for(int ix=0;ix<Nx*o;++ix){

                size_t idx_full = ix+Nx*o*(n + 3*iv);
                fp val_full = h_in_full.v[idx_full];

                size_t idx_split = ix+Nx*o*iv;
                if(n==0){
                    fp val_split_l = h_in_left.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_l );
                } if(n==1){
                    fp val_split_m = h_in_mid.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_m );
                }if(n==2) {
                    fp val_split_r = h_in_right.v[idx_split];
                    BOOST_CHECK_EQUAL( val_full, val_split_r );
                }
            }
        }
    }
 

}

/*
    cout << "\nall" << endl;
    for(int iv=0;iv<Nv*o;++iv){
        for(int n=0;n<3;++n){
            for(int ix=0;ix<Nx*o;++ix){

                size_t idx_full = ix+Nx*o*(n + 3*iv);
                cout << h_in_full.v[idx_full] << " ";
            }
            cout << "  ";
        }
        cout << endl;
    }

    cout << "\nleft" << endl;
    for(int iv=0;iv<Nv*o;++iv){
        for(int ix=0;ix<Nx*o;++ix){

            size_t idx_split = ix+Nx*o*iv;
            cout << h_in_left.v[idx_split] << " ";
        }
        cout << endl;
    }

    cout << "\nmid" << endl;
    for(int iv=0;iv<Nv*o;++iv){
        for(int ix=0;ix<Nx*o;++ix){

            size_t idx_split = ix+Nx*o*iv;
            cout << h_in_mid.v[idx_split] << " ";
        }
        cout << endl;
    }

    cout << "\nright" << endl;
    for(int iv=0;iv<Nv*o;++iv){
        for(int ix=0;ix<Nx*o;++ix){

            size_t idx_split = ix+Nx*o*iv;
            cout << h_in_right.v[idx_split] << " ";
        }
        cout << endl;
    }



    exit(1);

*/



