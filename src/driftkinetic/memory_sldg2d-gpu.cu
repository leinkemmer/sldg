#include <algorithms/sldg2d/sldg2d_lagrange_gpu.hpp>

int main(){


    for(int N = 32;N<100;N+=16){

        cout << "\n\nN: " << N << endl;

        int Nx = N;
        int Ny = N;
        int Nz = N;
        int batch = Nz;
    
        constexpr size_t o=2;
        
        int vs_mem = sizeof(vertex_star);
        int un_mem = sizeof(lag_upstream_nodes<o>);
        int face_mem = sizeof(face<o>);
        int es_mem = sizeof(element_star<o>);
        int as_mem = sizeof(lag_adjoint_solution<o>);
        int ai_mem = sizeof(lag_all_integrals<o>);
        size_t tot_vs_mem = vs_mem*(Nx+1)*(Ny+1)*batch;
        size_t tot_un_mem = un_mem*(Nx)*(Ny)*batch;
        size_t tot_fl_mem = face_mem*(Nx)*(Ny+1)*batch;
        size_t tot_fr_mem = face_mem*(Nx+1)*(Ny)*batch;
        size_t tot_es_mem = es_mem*(Nx)*(Ny)*batch;
        size_t tot_as_mem = as_mem*(Nx)*(Ny)*batch;
        size_t tot_ai_mem = ai_mem*(Nx)*(Ny)*batch;
    
        cout << "vs: " <<      tot_vs_mem*1e-9 << endl;
        cout << "un: " <<      tot_un_mem*1e-9 << endl; 
        cout << "face_l: " <<  tot_fl_mem*1e-9 << endl;
        cout << "face_r: " <<  tot_fr_mem*1e-9 << endl;
        cout << "es: " <<      tot_es_mem*1e-9 << endl; 
        cout << "as: " <<      tot_as_mem*1e-9 << endl; 
        cout << "ai: " <<      tot_ai_mem*1e-9 << endl; 
        cout << "total_memory = " << (tot_vs_mem +
                                     tot_un_mem + 
                                     tot_fl_mem +
                                     tot_fr_mem +
                                     tot_es_mem +
                                     tot_as_mem +
                                     tot_ai_mem)*1e-9 << endl;
    }

}
