#include <programs/driftkinetic.hpp>
#include <boost/program_options.hpp>

bool enable_output=true;

namespace dkconfig{

    constexpr double rmin = 0.1;
    constexpr double rmax = 14.5;
    constexpr double Kn0 = 0.055;
    constexpr double KTi = 0.27586;
    constexpr double deltaRTi = 1.45;
    constexpr double deltaRn0 = 2.0*deltaRTi;
    constexpr double epsilon = 1e-6;
    constexpr double rp = (rmin+rmax)*0.5;
    constexpr double deltaR = 4.0*deltaRn0/deltaRTi;
    constexpr double L = 1506.759067;
    constexpr double vmax = 7.32;

    double compute_Cn0(){

        double integral = 0;
        int N = 1000;
        double hr = (rmax-rmin)/double(N);
        for(int i=0;i<N;++i){
            double r = rmin + (i+0.5)*hr;
            integral += hr*exp(-Kn0*deltaRn0*tanh((r-rp)/deltaRn0));
        }

        return (rmax-rmin)/integral;
    }

    double Cn0 = compute_Cn0();

    double n0r(double r){
        return Cn0*exp(-Kn0*deltaRn0*tanh((r-rp)/deltaRn0))*r;
    }

    //Te == Ti
    double Ti(double r){
        return exp(-KTi*deltaRTi*tanh((r-rp)/deltaRTi));
    }

    double geq(double r, double v){
        return n0r(r)*exp(-pow(v,2.0)/(2.0*Ti(r)))/(pow(2.0*M_PI*Ti(r),0.5));
    }

    double perturb_func(double r, double theta, double z){
        double n=1.0;
        double m=5.0; 
        return epsilon*exp(-pow(r-rp,2.0)/deltaR)*cos(2.0*M_PI*n/L*z + m*theta);
    }

    double alpha(double r, int k, int l){
        return n0r(r)*( 1.0/Ti(r)*(l!=0) + double(k*k)/(r*r) );
    };

};



int main(int argc, char* argv[]){
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    cout << "Cn0: " << dkconfig::Cn0 << endl;
    cout << "vmax: " << dkconfig::vmax << endl;

    array<fp,4> a = {dkconfig::rmin, 0.0, -dkconfig::vmax, 0.0};
    array<fp,4> b = {dkconfig::rmax, 2.0*M_PI, dkconfig::vmax, dkconfig::L};

    string str_cells;
    fp finalT, tau;
    bool gpu, perturbation;
    int time_order;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("cells",value<string>(&str_cells)->default_value("32 32 32 32"),
          "degrees of freedom in the format nr ntheta nz nv")
        ("final_time,T",value<fp>(&finalT)->default_value(200.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(5),
         "time step size used in the simulation")
        ("perturbation",value<bool>(&perturbation)->default_value(true),
         "if true, the deltaf formulatio is use, otherwise the f formulation")
        ("order_in_time",value<int>(&time_order)->default_value(1),
         "if 1, first order method in time, "
         "if 2, second order method in time (computationally twice as expensive "
         "and 1.5 times more memory required as the first order method) ")
        #ifdef __CUDACC__
        ("gpu",value<bool>(&gpu)->default_value(false),
         "the simulation uses GPUs")
        #endif
        ;
        //TODO
        //("snapshots",value<int>(&number_snapshots)->default_value(2),
        // "number of snapshots that are written to disk (including initial and "
        // "final time)")
        //("order")
 
    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {

        array<Index,4> cells = parse<4>(str_cells);
        Index Nr = cells[0];
        Index Ntheta = cells[1];
        Index Nv = cells[3];
        Index Nz = cells[2];

        //not necessary
        bc bcr = bc::homogeneous_dirichlet;
        bc bct = bc::periodic;


        driftkinetic<2,2,2> dk{a, b, Nr, Ntheta, Nv, Nz,
                               tau, finalT, 
                               dkconfig::perturb_func, dkconfig::n0r, 
                               dkconfig::alpha, dkconfig::geq, gpu, 
                               time_order, perturbation, bcr, bct};
        dk.run();
    }

    return 0;
}
