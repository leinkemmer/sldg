#include <programs/driftkinetic.hpp>

bool enable_output=true;

namespace dkconfig{

    constexpr double rmin = 0.1;
    constexpr double rmax = 14.5;
    constexpr double Kn0 = 0.055;
    constexpr double KTi = 0.27586;
    constexpr double deltaRTi = 1.45;
    constexpr double deltaRn0 = 2.0*deltaRTi;
    constexpr double epsilon = 1e-6;
    constexpr double rp = (rmin+rmax)*0.5;
    constexpr double deltaR = 4.0*deltaRn0/deltaRTi;
    constexpr double L = 1506.759067;
    constexpr double vmax = 7.32;

    double compute_Cn0(){

        double integral = 0;
        int N = 1000;
        double hr = (rmax-rmin)/double(N);
        for(int i=0;i<N;++i){
            double r = rmin + (i+0.5)*hr;
            integral += hr*exp(-Kn0*deltaRn0*tanh((r-rp)/deltaRn0));
        }

        return (rmax-rmin)/integral;
    }

    double Cn0 = compute_Cn0();

    double n0r(double r){
        return Cn0*exp(-Kn0*deltaRn0*tanh((r-rp)/deltaRn0))*r;
    }

    //Te == Ti
    double Ti(double r){
        return exp(-KTi*deltaRTi*tanh((r-rp)/deltaRTi));
    }

    double geq(double r, double v){
        return n0r(r)*exp(-pow(v,2.0)/(2.0*Ti(r)))/(pow(2.0*M_PI*Ti(r),0.5));
    }

    double perturb_func(double r, double theta, double z){
        double n=1.0;
        double m=5.0; 
        return epsilon*exp(-pow(r-rp,2.0)/deltaR)*cos(2.0*M_PI*n/L*z + m*theta);
    }

    double alpha(double r, int k, int l){
        return n0r(r)*( 1.0/Ti(r)*(l!=0) + double(k*k)/(r*r) );
    };

};


double inf_norm(vector<fp>& ref, vector<fp>& approx){
    
    double maxerr = 0.0;
    double maxv = 0.0;
    for(size_t i=0;i<ref.size();++i){
        double err = abs(ref[i]-approx[i]);
        maxv = std::max(abs(ref[i]),maxv);
        if(err>maxerr){
            maxerr = err;
        }
    }
    return maxerr/maxv;
}

double l2_norm(vector<fp>& ref, vector<fp>& approx){
    
    double l2err = 0.0;
    double l2ref = 0.0;
    for(size_t i=0;i<ref.size();++i){
        double err = abs(ref[i]-approx[i]);
        l2ref += ref[i]*ref[i];
        l2err += err*err;
    }
    return sqrt(l2err/l2ref);
}

int main(){

    cout << "Cn0: " << dkconfig::Cn0 << endl;
    cout << "vmax: " << dkconfig::vmax << endl;

    array<fp,4> a = {dkconfig::rmin, 0.0, -dkconfig::vmax, 0.0};
    array<fp,4> b = {dkconfig::rmax, 2.0*M_PI, dkconfig::vmax, dkconfig::L};

    array<Index,4> cells = {64,64,64,128};
    Index Nr = cells[0];
    Index Ntheta = cells[1];
    Index Nv = cells[3];
    Index Nz = cells[2];

    //not necessary
    bc bcr = bc::homogeneous_dirichlet;
    bc bct = bc::periodic;

    bool perturbation = true;

    fp finalT = 500;

    size_t tot_elements = Nr*Ntheta*Nz*Nv*2*2*2*2;
    vector<fp> ref(tot_elements);
    vector<fp> out(tot_elements);

    bool gpu=false;
    #ifdef __CUDACC__
    gpu=true;
    #endif

    //reference solution
    {
        int order_in_time = 2;
        cout << "reference sol" << endl;
        fp tau = finalT/fp(2000);
        cout << "tau: " << tau << endl;
        driftkinetic<2,2,2> dk_ref{a, b, Nr, Ntheta, Nv, Nz,
                               tau, finalT, 
                               dkconfig::perturb_func, dkconfig::n0r, 
                               dkconfig::alpha, dkconfig::geq, gpu, 
                               order_in_time, perturbation, bcr, bct};
        cout << "dk-ref const" << endl;
        dk_ref.run();
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        std::copy_n(dk_ref.get_output(),tot_elements,ref.begin());
        gt::reset();
    }


    //order 1
    array<fp,10> err_max_order1 = {0.0};
    array<fp,10> err_l2_order1 = {0.0};
    array<fp,10> T1 = {0.0};
    int iter=0;
    for(int tt=2;tt<17;tt+=2) {

        int order_in_time = 1;
        //fp tau = finalT/fp(tt);
        fp tau = (tt);
        cout << "tau: " << tau << endl;
        driftkinetic<2,2,2> dk{a, b, Nr, Ntheta, Nv, Nz,
                               tau, finalT, 
                               dkconfig::perturb_func, dkconfig::n0r, 
                               dkconfig::alpha, dkconfig::geq, gpu, 
                               order_in_time, perturbation, bcr, bct};
        dk.run();
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        std::copy_n(dk.get_output(),tot_elements,out.begin());
        double err_max = inf_norm(ref,out);
        double err_l2 = inf_norm(ref,out);
        err_max_order1[iter] = err_max;
        err_l2_order1[iter] = err_l2;
        T1[iter] = tau;
        iter++;
        gt::reset();
    }
    cout << "max error order 1: " << err_max_order1 << ", " << T1 << endl;
    cout << "l2  error order 1: " << err_l2_order1 << ", " << T1 << endl;


    //order 2
    array<fp,10> err_max_order2 = {0};
    array<fp,10> err_l2_order2 = {0};
    array<fp,10> T2 = {0};
    iter=0;
    for(int tt=2;tt<17;tt+=2) {

        int order_in_time = 2;
        //fp tau = finalT/fp(tt);
        fp tau = fp(tt);
        cout << "tau: " << tau << endl;
        driftkinetic<2,2,2> dk{a, b, Nr, Ntheta, Nv, Nz,
                               tau, finalT, 
                               dkconfig::perturb_func, dkconfig::n0r, 
                               dkconfig::alpha, dkconfig::geq, gpu, 
                               order_in_time, perturbation, bcr, bct};
        dk.run();
        #ifdef __CUDACC__
        gpuErrchk(cudaDeviceSynchronize());
        #endif
        std::copy_n(dk.get_output(),tot_elements,out.begin());
        double err_max = inf_norm(ref,out);
        double err_l2 = l2_norm(ref,out);
        err_max_order2[iter] = err_max;
        err_l2_order2[iter] = err_l2;
        T2[iter] = tau;
        iter++;
        gt::reset();
    }
    cout << "max error order 1: " << err_max_order1 << ", " << T1 << endl;
    cout << "max error order 2: " << err_max_order2 << ", " << T2 << endl;
    cout << "l2  error order 1: " << err_l2_order1 << ", " << T1 << endl;
    cout << "l2  error order 2: " << err_l2_order2 << ", " << T2 << endl;

    return 0;
}


