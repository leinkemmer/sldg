#include <programs/vlasovpoissonpv.hpp>

#ifdef __CUDACC__
__device__ __host__
#endif
fp iv_weak_landau(fp xv[2]) {
    fp x=xv[0]; fp v=xv[1];
    return 1/(sqrt(2*M_PI))*exp(-v*v/2.0)*(1+0.01*cos(0.5*x));
}

#ifdef __CUDACC__
__device__ __host__
#endif
fp iv_strong_landau(fp xv[2]) {
    fp x=xv[0]; fp v=xv[1];
    return 1/(sqrt(2*M_PI))*exp(-v*v/2.0)*(1+0.5*cos(0.5*x));
}

#ifdef __CUDACC__
__device__ __host__
#endif
fp iv_bump_on_tail(fp xv[2]) {
    fp k = 1; fp alpha = 0.1;
    fp a = 1/sqrt(M_PI)*exp(-xv[1]*xv[1]);
    fp b = 1/sqrt(M_PI*0.25)*exp(-2.0*pow(xv[1]-2.5,2.0))
        *(1.0+alpha*cos(k*xv[0]));
    return 0.8*a+0.2*b;
}

#ifdef __CUDACC__
__device__ __host__
#endif
fp iv_bump_on_tail_two(fp xv[2]) {
    fp a = 1/sqrt(2.0*M_PI)*exp(-xv[1]*xv[1]/2.0);
    fp b = 1/sqrt(2.0*M_PI)*exp(-2.0*pow(xv[1]-4.5,2.0));
    fp c = (1.0+0.03*cos(0.3*xv[0]));
    return (0.9*a+0.2*b)*c;
}

#ifdef __CUDACC__
__device__ __host__
#endif
fp iv_tsi(fp xv[2]) {
    fp x = xv[0]; fp v = xv[1];

    fp a = exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
    fp b = exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);

    return 0.5*(1.0+0.001*cos(0.2*x))*(a+b);
}


bool enable_output; //is read in domain.hpp

int main(int argc, char *argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, dof, str_num_nodes, str_block_grid="1";
    int number_snapshots, MPIProcessPerNode=1, max_cfl;
    fp T, tau, sigma, cg_tol;
    Index cg_maxiter;
    bool fftb, gpu=false;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("dof",value<string>(&dof)->default_value("32 32 4 4"),
          "degrees of freedom in the format nx nv ox ov")
        ("final_time,T",value<fp>(&T)->default_value(200.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("mpi_grid",value<string>(&str_num_nodes)->default_value("1 1"),
         "number of mpi processes used")
        ("sigma",value<fp>(&sigma)->default_value(11.0),
         "parameter sigma of the DG Poisson solver")
        ("cg_tol",value<fp>(&cg_tol)->default_value(1e-8),
         "tolerance of in the CG method of the DG Poisson solver")
        ("cg_maxiter",value<Index>(&cg_maxiter)->default_value(2000),
         "maximum number of iterations in the CG method of the DG Poisson solver")
        ("fft",value<bool>(&fftb)->default_value(true),
         "fft poisson solver or DG")
        ("max_cfl",value<int>(&max_cfl)->default_value(5),
         "largest CFL number, amount of memory for boundary transfer depends on it")
        #ifdef __CUDACC__
        ("gpu",value<bool>(&gpu)->default_value(false),
         "the simulation uses GPUs")
        ("enable_p2p",value<bool>(&parameters::enable_p2p)->default_value(true),
         "Peer-to-peer transfer is explicitly enabled.")
        ("cuda_aware_mpi",value<bool>(&parameters::cuda_aware_mpi)->default_value(false),
         "Use of CUDA aware MPI (if available) such that MPI takes care"
         "about the data transfer between different GPUs.")
        ("mpi_process_per_node",value<int>(&MPIProcessPerNode)->default_value(1),
         "number of MPI processes per nodes used"
         "(each process corresponds to one GPU on a node)")
        ("vblock_grid",value<string>(&str_block_grid)->default_value("1"),
         "GPU grid per mpi process, partition in v direction only.")
        #endif
        ;

        #ifndef __CUDACC__
        gpu = false; // makes sure that gpu is always initialized
        #endif

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);



    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        bool openmp=false;
        #ifdef _OPENMP
        openmp=true;
        #endif
        mpi_init(openmp);
        mpi_process<2> mpicomm;

        index_dxdv_no<1, 1> e(parse<4>(dof));
        cout << "parsed e: " << e << endl;

        array<Index,2> num_nodes  = parse<2>(str_num_nodes);
        cout << "parsed num_nodes: " << num_nodes << endl;

        array<Index,1> block_grid = parse<1>(str_block_grid);

        cout << "problem: " << problem << endl;

        array<bdry_cond,1> b_type = {bdry_cond::periodic};
        if(problem == "ll") {
            vlasovpoisson<1,1,iv_weak_landau> vp({0.0,-6.0},
                    {4.0*M_PI,6.0},e,T,tau, mpicomm,
                    number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "nl") {
            vlasovpoisson<1,1,iv_strong_landau> vp({0.0,-6.0},
                    {4.0*M_PI,6.0},e,T,tau,
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "bot") {
            vlasovpoisson<1,1,iv_bump_on_tail> vp({0.0,-6.0},
                    {4.0*M_PI,6.0},e,T,tau, 
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "bot2") {
            vlasovpoisson<1,1,iv_bump_on_tail_two> vp({0.0,-9.0},
                    {20.0/3.0*M_PI,9.0},e,T,tau, 
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "tsi") {
            vlasovpoisson<1,1,iv_tsi> vp({0.0,-6.0},
                    {10.0*M_PI,6.0},e,T,tau, mpicomm,
                    number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else {
            cout << "ERROR: problem " << problem << " is not available "
                 << "(accepted values are ll, nl, bot, tsi)" 
                 << endl;
            exit(1);
        }

        mpi_destroy();
    }
    return 0;
}

