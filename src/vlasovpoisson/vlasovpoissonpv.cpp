#include <programs/vlasovpoissonpv.hpp>

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_weak_landau(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];
    return 1/(2.0*M_PI)*exp(-v*v/2.0)*exp(-w*w/2.0)
        *(1+0.01*cos(0.5*x)+0.01*cos(0.5*y));
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_strong_landau(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];
    //return 1/(2.0*M_PI)*exp(-v*v/2.0)*exp(-w*w/2.0)*(1+0.5*cos(0.5*x));
    return 1/(2.0*M_PI)*exp(-v*v/2.0)*exp(-w*w/2.0)
        *(1+0.5*(cos(0.5*x) + cos(0.5*y)));
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_bump_on_tail(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];

    fp k = 0.3; fp alpha = 0.002;
    fp factor = exp(-w*w/2.0)/sqrt(2.0*M_PI);
    fp a = exp(-v*v/2.0)/sqrt(2.0*M_PI);
    fp b = exp(-2.0*pow(v-4.5,2.0))/sqrt(M_PI/2.0);
    fp c = (1.0 + alpha*cos(k*x) + alpha*cos(k*y));
    return factor*(0.9*a+0.1*b)*c;
}


#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_bump_on_tail_oblique(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];

    fp k = 0.3; fp alpha = 0.002;
    fp a = exp(-0.5*v*v)*exp(-0.5*w*w)/(2.0*M_PI);
    fp b = exp(-2.0*pow(v-4.25,2.0))*exp(-2.0*pow(w-1.5,2.0))/(M_PI);
    return 0.9*a + 0.1*b*(1.0 + alpha/sqrt(2.0)*cos(k*x) + alpha/sqrt(2.0)*cos(k*y));
}



#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_blop(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];

    fp epsilon = 0.1;

    fp factor1=exp(-0.5*v*v)/sqrt(2.0*M_PI);
    fp factor2=exp(-0.5*w*w)/sqrt(2.0*M_PI);
    return factor1*factor2*(1.0 + epsilon*exp(-4.0*pow(x-M_PI,2))
            *exp(-4.0*pow(y-M_PI,2)));
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_tsi(fp xv[4]) {
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];

    fp factor1 = exp(-0.5*pow(v-2.4,2)) + exp(-0.5*pow(v+2.4,2));
    fp factor2 = exp(-0.5*pow(w-2.4,2)) + exp(-0.5*pow(w+2.4,2));
    return (1.0 + 1e-3*cos(0.2*x)*cos(0.2*y))*factor1*factor2/(8.0*M_PI);
}

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_tsi2(fp xv[4]){
    fp x=xv[0]; fp y=xv[1];
    fp v=xv[2]; fp w=xv[3];

    fp factor1 = exp(-0.5*pow(v-2.4,2)) + exp(-0.5*pow(v+2.4,2));
    fp factor2 = exp(-0.5*pow(w-2.4,2)) + exp(-0.5*pow(w+2.4,2));
    return fp((1.0 + 1e-3*(cos(0.2*x) + cos(0.2*y)))*factor1*factor2/(8.0*M_PI));
}
 
bool enable_output; //is read in domain.hpp

int main(int argc, char *argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, dof, str_num_nodes, str_block_grid="1 1";
    int number_snapshots, MPIProcessPerNode=1, max_cfl;
    fp T, tau, sigma, cg_tol;
    Index cg_maxiter;
    bool fftb, gpu=false;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("dof",value<string>(&dof)->default_value("16 16 16 16 4 4 4 4"),
          "degrees of freedom in the format nx1 nx2 nv1 nv2 ox1 ox2 ov1 ov2")
        ("final_time,T",value<fp>(&T)->default_value(200.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("mpi_grid",value<string>(&str_num_nodes)->default_value("1 1 1 1"),
         "number of mpi processes used")
        ("sigma",value<fp>(&sigma)->default_value(10.0),
         "parameter sigma of the DG Poisson solver")
        ("cg_tol",value<fp>(&cg_tol)->default_value(1e-6),
         "tolerance squared of the relative error in the CG method of the DG Poisson solver")
        ("cg_maxiter",value<Index>(&cg_maxiter)->default_value(0),
         "maximum number of iterations in the CG method of the DG Poisson solver"
         "its default value is the square root of the degrees of freedom times 1.1")
        ("fft",value<bool>(&fftb)->default_value(true),
         "fft poisson solver or DG")
        ("max_cfl",value<int>(&max_cfl)->default_value(4),
         "largest CFL number, amount of memory for boundary transfer depends on it")
        #ifdef __CUDACC__
        ("gpu",value<bool>(&gpu)->default_value(false),
         "the simulation uses GPUs")
        ("enable_p2p",value<bool>(&parameters::enable_p2p)->default_value(true),
         "Peer-to-peer transfer is explicitly enabled.")
        ("cuda_aware_mpi",value<bool>(&parameters::cuda_aware_mpi)->default_value(false),
         "Use of CUDA aware MPI (if available) such that MPI takes care"
         "about the data transfer between different GPUs.")
        ("mpi_process_per_node",value<int>(&MPIProcessPerNode)->default_value(1),
         "number of MPI processes per nodes used"
         "(each process corresponds to one GPU on a node)")
        ("vblock_grid",value<string>(&str_block_grid)->default_value("1 1"),
         "GPU grid per mpi process, partition in v direction only.")
        #endif
        ;

        #ifndef __CUDACC__
        gpu = false; // makes sure that gpu is always initialized
        #endif

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        bool openmp=false;
        #ifdef _OPENMP
        openmp=true;
        #endif
        mpi_init(openmp);
        mpi_process<4> mpicomm;

        index_dxdv_no<2, 2> e(parse<8>(dof));
        cout << "parsed e: " << e << endl;

        array<Index,4> num_nodes  = parse<4>(str_num_nodes);
        cout << "parsed num_nodes: " << num_nodes << endl;
        
        array<Index,2> block_grid  = parse<2>(str_block_grid);
        cout << "parsed block_grid: " << block_grid << endl;

        if (fftb==0 && cg_maxiter==0){
            Index num_nodes_x = 1;
            for(Index i=0;i<2;++i)
                num_nodes_x *= num_nodes[i];
            cg_maxiter = Index(pow(num_nodes_x*prod(e.x_part()),1.0/2.0)*1.1);
            cout << "cg_maxiter: " << cg_maxiter << endl;
        }


        array<bdry_cond,2> b_type = {bdry_cond::periodic, bdry_cond::periodic};
        if(problem == "ll") {
            vlasovpoisson<2,2,iv_weak_landau> vp({0.0,0.0,-6.0,-6.0},
                    {4.0*M_PI,4.0*M_PI,6.0,6.0},e,T,tau, mpicomm,
                    number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "nl") {
            vlasovpoisson<2,2,iv_strong_landau> vp({0.0,0.0,-6.0,-6.0},
                    {4.0*M_PI,4.0*M_PI,6.0,6.0},e,T,tau,
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "bot") {
            vlasovpoisson<2,2,iv_bump_on_tail> vp({0.0,0.0,-9.0,-9.0},
                    {20.0*M_PI,20.0*M_PI,9.0,9.0},e,T,tau,
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "bot-oblique") {
            vlasovpoisson<2,2,iv_bump_on_tail_oblique> vp({0.0,0.0,-9.0,-9.0},
                    {20.0*M_PI,20.0*M_PI,9.0,9.0},e,T,tau,
                    mpicomm,number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "blop") {
            vlasovpoisson<2,2,iv_blop> vp({0.0,0.0,-6.0,-6.0},
                    {4.0*M_PI,4.0*M_PI,6.0,6.0},e,T,tau, mpicomm,
                    number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "tsi") {
            vlasovpoisson<2,2,iv_tsi> vp({0.0,0.0,-6.0,-6.0},
                    {10.0*M_PI,10.0*M_PI,6.0,6.0},e,T,tau, mpicomm,
                    number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else if(problem == "tsi2") {
            vlasovpoisson<2,2,iv_tsi2> vp({0.0,0.0,-6.0,-6.0},
                    {10.0*M_PI,10.0*M_PI,6.0,6.0},e,T,tau,
                    mpicomm, number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else {
            cout << "ERROR: problem " << problem << " is not available "
                 << "(accepted values are bot, nl, ll, vac, ts, or ts2)" 
                 << endl;
            exit(1);
        }

        mpi_destroy();
    }
    return 0;
}

