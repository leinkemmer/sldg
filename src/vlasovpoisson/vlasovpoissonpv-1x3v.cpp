#include <programs/vlasovpoissonpv.hpp>

#ifdef __CUDACC__
__host__ __device__
#endif
fp iv_weak_landau(fp xv[4]) {
    fp x=xv[0]; fp v=xv[1];
    fp w=xv[2]; fp u=xv[3];
    return fp(1/(pow(2.0*M_PI,3.0/2.0))*exp(-v*v/2.0)*exp(-w*w/2.0)
             *exp(-u*u/2.0)*(1+0.01*cos(0.5*x)));
}
 
bool enable_output; //is read in domain.hpp

int main(int argc, char *argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, dof, str_num_nodes, str_block_grid="1 1 1";
    int number_snapshots, MPIProcessPerNode=1, max_cfl;
    fp T, tau, sigma, cg_tol;
    Index cg_maxiter;
    bool fftb, gpu=false;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("dof",value<string>(&dof)->default_value("16 16 16 16 4 4 4 4"),
          "degrees of freedom in the format nx1 nx2 nv1 nv2 ox1 ox2 ov1 ov2")
        ("final_time,T",value<fp>(&T)->default_value(200.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("ll"),
         "initial value for the problem that is to be solved")
        ("mpi_grid",value<string>(&str_num_nodes)->default_value("1 1 1 1"),
         "number of mpi processes used")
        ("sigma",value<fp>(&sigma)->default_value(11.0),
         "parameter sigma of the DG Poisson solver")
        ("cg_tol",value<fp>(&cg_tol)->default_value(1e-8),
         "tolerance of in the CG method of the DG Poisson solver")
        ("cg_maxiter",value<Index>(&cg_maxiter)->default_value(2000),
         "maximum number of iterations in the CG method of the DG Poisson solver")
        ("fft",value<bool>(&fftb)->default_value(true),
         "fft poisson solver or DG")
        ("max_cfl",value<int>(&max_cfl)->default_value(5),
         "largest CFL number, amount of memory for boundary transfer depends on it")
        #ifdef __CUDACC__
        ("gpu",value<bool>(&gpu)->default_value(false),
         "the simulation uses GPUs")
        ("enable_p2p",value<bool>(&parameters::enable_p2p)->default_value(true),
         "Peer-to-peer transfer is explicitly enabled.")
        ("cuda_aware_mpi",value<bool>(&parameters::cuda_aware_mpi)->default_value(false),
         "Use of CUDA aware MPI (if available) such that MPI takes care"
         "about the data transfer between different GPUs.")
        ("mpi_process_per_node",value<int>(&MPIProcessPerNode)->default_value(1),
         "number of MPI processes per nodes used"
         "(each process corresponds to one GPU on a node)")
        ("vblock_grid",value<string>(&str_block_grid)->default_value("1 1"),
         "GPU grid per mpi process, partition in v direction only.")
        #endif
        ;

        #ifndef __CUDACC__
        gpu = false; // makes sure that gpu is always initialized
        #endif

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        bool openmp=false;
        #ifdef _OPENMP
        openmp=true;
        #endif
        mpi_init(openmp);
        mpi_process<4> mpicomm;

        index_dxdv_no<1, 3> e(parse<8>(dof));
        cout << "parsed e: " << e << endl;

        array<Index,4> num_nodes  = parse<4>(str_num_nodes);
        cout << "parsed num_nodes: " << num_nodes << endl;

        array<Index,3> block_grid = parse<3>(str_block_grid);
        cout << "parsed block_grid: " << block_grid << endl;

        array<bdry_cond,1> b_type = {bdry_cond::periodic};
        if(problem == "ll") {
            vlasovpoisson<1,3,iv_weak_landau> vp({0.0,-6.0,-6.0,-6.0},
                    {4.0*M_PI,6.0,6.0,6.0},e,T,tau, 
                    mpicomm, number_snapshots,gpu,num_nodes,b_type,
                    sigma, cg_tol, cg_maxiter, fftb, MPIProcessPerNode, 
                    block_grid, max_cfl);
            vp.run();
        } else {
            cout << "ERROR: problem " << problem << " is not available "
                 << "(accepted values are ll)" 
                 << endl;
            exit(1);
        }

        mpi_destroy();
    }
    return 0;
}

