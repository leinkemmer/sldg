#include <programs/vlasovpoisson.hpp>


fp iv_strong_landau(array<fp,2> xv) {
    fp x=xv[0]; fp v=xv[1];
    return 1/(sqrt(2*M_PI))*exp(-v*v/2.0)*(1+0.5*cos(0.5*x));
}

fp iv_weak_landau(array<fp,2> xv) {
    fp x=xv[0]; fp v=xv[1];
    return 1/(sqrt(2*M_PI))*exp(-v*v/2.0)*(1+0.01*cos(0.5*x));
}

fp iv_bump_on_tail(array<fp,2> xv) {
    fp k = 1; fp alpha = 0.1;
    fp a = 1/sqrt(M_PI)*exp(-xv[1]*xv[1]);
    fp b = 1/sqrt(M_PI*0.25)*exp(-2.0*pow(xv[1]-2.5,2.0))
        *(1.0+alpha*cos(k*xv[0]));
    return 0.8*a+0.2*b;
}

fp iv_bump_on_tail_mv(array<fp,2> xv) {
    //fp k = 1.5; fp alpha = 0.03;
    fp k = 0.3; fp alpha = 0.03;
    fp a = 1.0/sqrt(2.0*M_PI)*exp(-0.5*xv[1]*xv[1]);
    fp b = 1.0/sqrt(2.0*M_PI)*exp(-2.0*pow(xv[1]-4.5,2.0))
        *(1+alpha*cos(k*xv[0]));
    return 9./10.*a + 2./10.*b;
}
fp iv_vacuum_expansion(array<fp,2> xv) {
    fp x=xv[0]; fp v=xv[1];
    return 1/(sqrt(2*M_PI))*exp(-pow(v,2)/2.0)*1/(sqrt(2*M_PI))
        *exp(-pow(x-2*M_PI,2)/2.0);
}


fp iv_two_stream(array<fp,2> xv) {
    fp alpha = 1e-2;
    fp L = 100.0;

    fp perturbation = cos(2*M_PI/L*xv[0]);
    //  fp perturbation = 0.0;
    //  for(int i=1;i<20;i++) {
    //    fp phase = 2*M_PI*(rand()%1000000000)/1e9; // generate a random phase
    //    perturbation += cos(i*2*M_PI/L*xv[0]+phase);
    //  }

    fp a = 1/sqrt(M_PI)*exp(-pow(xv[1]+2,2.0));
    fp b = 1/sqrt(M_PI)*exp(-pow(xv[1]-2,2.0));

    return (0.5*a + 0.5*b)*(1.0+alpha*perturbation);

    //return 1/sqrt(M_PI)*pow(xv[1],2)*exp(-pow(xv[1],2))*(1-0.05*cos(5.0*xv[0]));
}

fp iv_two_stream_2(array<fp,2> xv) {
    fp x = xv[0]; fp v = xv[1];

    fp a = exp(-0.5*pow(v-2.4,2))/sqrt(2.0*M_PI);
    fp b = exp(-0.5*pow(v+2.4,2))/sqrt(2.0*M_PI);

    return 0.5*(1.0+0.001*cos(0.2*x))*(a+b);
}

fp iv_blop(array<fp,2> xv) {
    fp x=xv[0]; fp v=xv[1];

    fp epsilon = 1.0;

    fp factor1=exp(-0.5*v*v)/sqrt(2.0*M_PI);
    return factor1*(1.0 + epsilon*sqrt(8*exp(1))*(x-2.0*M_PI)
            *exp(-4.0*pow(x-2.0*M_PI,2)));
}

fp iv_nonconstu(array<fp,2> xv) {
    double x = xv[0];
    double v = xv[1];
    double rho = (1.0+0.01*cos(0.5*x))/sqrt(2.0*M_PI);
    double u = 0.2*cos(x);
    return exp(-0.5*v*v)*rho*(1.0 + u*v + 0.5*u*u*(pow(v,2)-1.0)
                             + 1.0/6.0*pow(u,3)*(pow(v,3)-3.0*v)
                             + 1.0/24.0*pow(u,4)*(3.0-6.0*pow(v,2)+pow(v,4))
                             + 1.0/120.0*pow(u,5)*(15.0*v-10.0*pow(v,3)+pow(v,5)));
}


bool enable_output; //is read in domain.hpp

int main(int argc, char* argv[]) {
    using namespace boost;
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem;
    int num_cells_x, num_cells_v, ox, ov, number_snapshots;
    fp T, tau;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("num_cells_x",value<int>(&num_cells_x)->default_value(128),
         "number of cells used in the space directions")
        ("num_cells_v",value<int>(&num_cells_v)->default_value(128),
         "number of cells used in the velocity directions")
        ("order_x",value<int>(&ox)->default_value(2),
         "order of the space discretization in space directions")
        ("order_v",value<int>(&ov)->default_value(2),
         "order of the space discretization in velocity directions")
        ("final_time,T",value<fp>(&T)->default_value(200.0),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(0.1),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("bot"),
         "initial value for the problem that is to be solved");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        bool openmp=false;
        #ifdef _OPENMP
        openmp=true;
        #endif
        mpi_init(openmp);
        mpi_process<2> mpicomm;

        if(problem == "bot") {
            vlasovpoisson<2> vp(4.0*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,
                    tau,iv_bump_on_tail,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "botmv") {
            vlasovpoisson<2> vp(20.0*M_PI,9.0,num_cells_x,num_cells_v,ox,ov,T,
                    tau,iv_bump_on_tail_mv,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "nl") {
            vlasovpoisson<2> vp(4.0*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,
                    tau,iv_strong_landau,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "ll") {
            vlasovpoisson<2> vp(4.0*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,
                    tau,iv_weak_landau,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "vac") {
            vlasovpoisson<2> vp(4.0*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,
                    tau,iv_vacuum_expansion,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "ts") {
            vlasovpoisson<2> vp(100.0,6.0,num_cells_x,num_cells_v,ox,ov,T,tau,
                    iv_two_stream,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "ts2") {
            vlasovpoisson<2> vp(10*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,tau,
                    iv_two_stream_2,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "blop") {
            vlasovpoisson<2> vp(4*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,tau,
                    iv_blop,mpicomm, number_snapshots);
            vp.run();
        }
        else if(problem == "nonconstu") {
            vlasovpoisson<2> vp(4*M_PI,6.0,num_cells_x,num_cells_v,ox,ov,T,tau,
                    iv_nonconstu, mpicomm, number_snapshots);
            vp.run();
        } else {
            cout << "ERROR: problem " << problem << " is not available "
                 << "(accepted values are bot, nl, ll, vac, ts, or ts2)" << endl;
            exit(1);
        }

        mpi_destroy();
    }

    return 0;
}

