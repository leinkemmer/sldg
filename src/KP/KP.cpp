#define GENERIC_NO_STORAGE
#include <generic/common.hpp>

#include <boost/program_options.hpp>
#include <boost/math/special_functions/bessel.hpp>
using namespace boost::math;

#include <fftw3.h>
#include <gsl/gsl_spline.h>


// TODO: we use here the boost multi_array instead of the multi_array class
// in storage.hpp
#define USE_BOOST_MULTIARRAY
#include <boost/multi_array.hpp>
template<class T, Index d>
using multi_array = typename boost::multi_array<T,d>;
template<class T>
using multi_array_view = typename multi_array<T,3>::template array_view<2>::type;
#include <container/domain.hpp>

#include <container/domains-mixed.hpp>
#include <algorithms/burgers_dg.hpp>
#include <algorithms/interpolation.hpp>


enum interpolation_method {
    INM_DIRECT,
    INM_SPLINE,
    INM_LAGRANGE7,
    INM_LAGRANGE8,
    INM_LAGRANGE9,
    INM_LAGRANGE,
    INM_LAGRANGE_P1
};


bool enable_output;

fp iv_soliton(fp x, fp y) {
    fp epsilon = 1.0; // assumes that epsilon is set to 1
    fp c = 2.0;
    fp a = epsilon*sqrt(c)/sqrt(2.0);
    return c*pow(sech(a*x),2);
}

fp sol_soliton(fp t, fp x, fp y) {
    fp epsilon = 1.0; // assumes that epsilon is set to 1
    fp c = 2.0;
    fp a = epsilon*sqrt(c)/sqrt(2.0);
    fp b = 2.0*c*pow(epsilon,2);
    return c*pow(sech(a*(x-b*t)),2);
}

// this is the initial value from Klein and Roidot
fp iv_schwartzian(fp x, fp y) {
    fp s = sqrt(x*x+y*y);
    return (x != 0 || y !=0) ? 2.0*x*tanh(s)*pow(sech(s),2)/s : 0;
}

fp sol_zaitsev(fp t, fp x, fp y) {
    fp alpha = 1.0;
    fp beta  = 0.5;
    fp delta = sqrt(3/(1-pow(beta,2)))*pow(alpha,2);
    fp c     = pow(alpha,2)*(4-pow(beta,2))/(1-pow(beta,2));

    return 2*pow(alpha,2)*(1-beta*cosh(alpha*(x-c*t))*cos(delta*y))
        /pow(cosh(alpha*(x-c*t))-beta*cos(delta*y),2);
}

fp iv_zaitsev(fp x, fp y) {
    return sol_zaitsev(0.0, x, y);
}

fp iv_zatisev_perturbed(fp x, fp y) {
    fp Lx = 25.0;
    return sol_zaitsev(0.0, x+0.5*Lx,y)
        + 6*(x+0.5*Lx)*exp(-pow(x+0.5*Lx,2)-pow(y,2));
}


void fft(domain2d_equi& in, domain2d_frequency& out) {
    fftw_plan p = fftw_plan_dft_r2c_2d(in.Ny,in.Nx,in.un,out.uf,
            FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    fftw_execute(p);
    fftw_destroy_plan(p);
}

void inv_fft(domain2d_frequency& in, domain2d_equi& out) {
    // Backward Fourier transform
    fftw_plan p = fftw_plan_dft_c2r_2d(in.Ny,in.Nx,in.uf,out.un,FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);
}


void apply_dispersion(fp tau, domain2d_frequency& u, fp lambda, fp epsilon) {
    int Nx = u.Nx; int Ny = u.Ny;
    fp Lx = u.Lx; fp Ly = u.Ly;

    fp delta = std::numeric_limits<fp>::epsilon(); // machine epsilon
    for(int j=0;j<Ny;j++) {
        for(int i=0;i<Nx/2+1;i++) {
            fp i_freq = i; // in this case there are no negative frequencies
            fp j_freq = (j<Ny/2+1) ? j : -(Ny-j);
            i_freq *= 2*M_PI/Lx; j_freq *= 2*M_PI/Ly;

            if(i!=0) {
                complex<fp> dispersion(0.0,-pow(i_freq,3));
                complex<fp> regularized_x =
                    1.0/(pow(i_freq,2) + pow(lambda*delta,2))
                    *complex<fp>(-lambda*delta, -i_freq);
                complex<fp> diffusion_y(-pow(j_freq,2),0.0);
                complex<fp> diffusion = regularized_x*diffusion_y;

                complex<fp> exponent =
                    -pow(epsilon,2)*dispersion - lambda*diffusion;

                fftw_complex &v = *(u.uf + i + j*(Nx/2+1));
                complex<fp> value(v[0], v[1]);
                value *= exp(exponent*tau);
                v[0] = value.real()/fp(Nx*Ny); v[1] = value.imag()/fp(Nx*Ny);
            } else {
                fftw_complex &v = *(u.uf + i + j*(Nx/2+1));
                if(j != 0) {
                    v[0] = 0.0; v[1] = 0.0;
                } else {
                    v[0] /= fp(Nx*Ny);
                    v[1] /= fp(Nx*Ny);
                }
            }
        }
    }
}

void apply_burger(fp tau, fp Lx, domain2d_dG& in, domain2d_dG& out,
        int num_it, iterative_method im) {

    int ny = in.Ny;
    int nx = in.nx; int ox = in.ox;

    // boundary information
    int bdr_len=30;

    vector<fp> left_bdr(bdr_len*ox);
    vector<fp> right_bdr(bdr_len*ox);

    // solve Burgers' equation
    for(int m=0;m<ny;m++) {
        typedef typename domain<1>::domain_data::index_range range;
        domain<1>::view slice_in  = 
            in.data[ boost::indices[m][range()][range()] ];
        domain<1>::view slice_out =
            out.data[ boost::indices[m][range()][range()] ];

        for(int i=0;i<bdr_len;i++)
            for(int o=0;o<ox;o++) {
                right_bdr[o + ox*i] = slice_in[i][o];
                left_bdr[o + ox*i]  = slice_in[nx-1-i][o];
            }

        //translate1d<1>(slice_in, slice_out, &left_bdr[0], bdr_len, 8.0*tau);
        //burgers_dg_constvel(slice_in,slice_out,6.0*tau,Lx,&left_bdr[0],
        //&right_bdr[0],bdr_len,num_it,im);
        burgers_dg(slice_in,slice_out,6.0*tau,Lx,&left_bdr[0],&right_bdr[0],
                bdr_len,num_it,im);
    }
}

struct KP {
    fp T, tau, Lx, Ly;
    int snapshots, num_it, ox, oy, nx, ny;
    fp(*u0)(fp,fp);
    iterative_method im;
    interpolation_method inm;
    fp lambda, epsilon;

    domain2d_equi      u;
    domain2d_frequency u_f;
    domain2d_dG u_dG_in, u_dG_out;

    KP(array<fp,2> _a, array<fp,2> _b, int _nx, int _ox, int _ny, fp _T,
            fp _tau, fp(*_u0)(fp,fp), fp _lambda, fp _epsilon, int _snapshots,
            int _num_it,
            iterative_method _im, interpolation_method _inm) :
        u(_nx,_ox,_ny,_b[0]-_a[0],_b[1]-_a[1]),
        u_f(_nx*_ox,_ny,_b[0]-_a[0],_b[1]-_a[1]),
        u_dG_in(_nx, _ox, _ny, _b[0]-_a[0],_b[1]-_a[1]),
        u_dG_out(_nx, _ox, _ny, _b[0]-_a[0],_b[1]-_a[1])

    {
        T=_T; tau=_tau; u0=_u0; snapshots=_snapshots; num_it=_num_it; im=_im;
        inm=_inm;
        lambda = _lambda; epsilon = _epsilon;
        Lx=_b[0]-_a[0]; Ly=_b[1]-_a[1];
        nx=_nx; ox=_ox; ny=_ny;

        u.init(u0);
    }

    void step(fp tau) {
        // apply the diffusion
        fft(u, u_f);
        apply_dispersion(0.5*tau, u_f, lambda, epsilon);
        inv_fft(u_f, u);

        // solve Burgers' equation
        if(inm == INM_SPLINE || inm == INM_DIRECT)
            to_dG(u, u_dG_in);
        else if(inm == INM_LAGRANGE7)
            to_dG_lagrange(u, u_dG_in, 7);
        else if(inm == INM_LAGRANGE8)
            to_dG_lagrange(u, u_dG_in, 8);
        else if(inm == INM_LAGRANGE9)
            to_dG_lagrange(u, u_dG_in, 9);
        else if(inm == INM_LAGRANGE)
            // GSL can't handle two or fewer points
            to_dG_lagrange(u, u_dG_in, max(3,u_dG_in.ox));
        else if(inm == INM_LAGRANGE_P1)
            to_dG_lagrange(u, u_dG_in, max(3,u_dG_in.ox+1));
        apply_burger(tau, Lx, u_dG_in, u_dG_out, num_it, im);
        if(inm == INM_DIRECT)
            to_equi_directeval(u_dG_out, u);
        else if(inm == INM_LAGRANGE7)
            to_equi_lagrange(u_dG_out, u, 7);
        else if(inm == INM_LAGRANGE8)
            to_equi_lagrange(u_dG_out, u, 8);
        else if(inm == INM_LAGRANGE9)
            to_equi_lagrange(u_dG_out, u, 9);
        else if(inm == INM_LAGRANGE)
            to_equi_lagrange(u_dG_out, u, max(3,u_dG_in.ox));
        else if(inm == INM_LAGRANGE_P1)
            to_equi_lagrange(u_dG_out, u, max(3,u_dG_in.ox+1));
        else
            to_equi(u_dG_out, u);

        // apply the diffusion
        fft(u, u_f);
        apply_dispersion(0.5*tau, u_f, lambda, epsilon);
        inv_fft(u_f, u);
    }


    void run() {
        std::ofstream fs_evolution("evolution.data");
        fs_evolution.precision(16);
        fs_evolution << "# t max_amplitude mass momentum" << endl;

        int n_steps=ceil(T/tau); fp t=0.0;
        for(int i=0;i<n_steps+1;i++) {
            fs_evolution << t << '\t' << u.max_amplitude() << '\t' << u.mass() 
                         << '\t' << u.momentum() << endl;

            string fn = (boost::format("f-t%1%.data")%t).str();
            if(i % int(ceil(n_steps/fp(snapshots-1))) == 0 || i==n_steps)
                u.write(fn);

            // TODO: output
            if(i < n_steps+1) { // last step is only for output
                if(T-t<tau) tau=T-t;

                step(tau);
                t+=tau;
            }
        }
    }

};

fp iv_exp(fp x, fp y) {
    return exp(-0.1*x*x);
}

int main(int argc, char* argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, it_method, interp_method;
    int num_cells_x, num_cells_y, ox, number_snapshots, num_it;
    fp T, tau, epsilon, lambda;

    // WARNING: direct_evaluation can be unstable under certain circumstances
    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("enable_output",value<bool>(&enable_output)->default_value(true),
         "enable or disable writing to disk")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and "
         "final time)")
        ("num_cells_x",value<int>(&num_cells_x)->default_value(512),
         "number of cells used in the space directions")
        ("num_cells_y",value<int>(&num_cells_y)->default_value(256),
         "number of cells used in the velocity directions")
        ("order_x",value<int>(&ox)->default_value(2),
         "order of the space discretization in space directions")
        ("final_time,T",value<fp>(&T)->default_value(0.4),
         "final time of the simulation")
        ("step_size",value<fp>(&tau)->default_value(1e-3),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("schwartzian"),
         "initial value for the problem that is to be solved")
        ("iter,i",value<int>(&num_it)->default_value(5),
         "number of iterations for the Burgers' equation")
        ("iter_method",value<string>(&it_method)->default_value("secant"),
         "iterative method used (either fixedpoint, secant, or newton)")
        ("interp_method",value<string>(&interp_method)->default_value("spline"),
         "method used to interpolate from dG to the equidistant grid "
         "(either direct_evaluation, spline, lagrange, lagrange+1, lagrange7, "
         "lagrange8, or lagrange9)")
        ("epsilon",value<fp>(&epsilon)->default_value(0.1),
         "epsilon parameter for the KP equation")
        ("lambda",value<fp>(&lambda)->default_value(-1.0),
         "lambda parameter for the KP equation");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        iterative_method im;
        if(it_method == "newton")           im = IM_NEWTON;
        else if(it_method == "secant")      im = IM_SECANT;
        else if(it_method == "fixedpoint")  im = IM_FIXEDPOINT;
        else { 
            cout << "ERROR: iter_method must be either fixedpoint, secant, or "
                 << "newton" << endl; exit(1);
        }

        interpolation_method inm;
        if(interp_method == "direct_evaluation")  inm = INM_DIRECT;
        else if(interp_method == "spline")        inm = INM_SPLINE;
        else if(interp_method == "lagrange")      inm = INM_LAGRANGE;
        else if(interp_method == "lagrange+1")    inm = INM_LAGRANGE_P1;
        else if(interp_method == "lagrange7")     inm = INM_LAGRANGE7;
        else if(interp_method == "lagrange8")     inm = INM_LAGRANGE8;
        else if(interp_method == "lagrange9")     inm = INM_LAGRANGE9;
        else { 
            cout << "ERROR: interp_method must be either spline, lagrange, "
                 << "lagrange+1, lagrange7, lagrange8, lagrange9, or "
                 << "direct_evaluation" << endl; exit(1);
        }

        fp(*u0)(fp,fp);
        fp _a1, _a2, _b1, _b2;
        if(problem == "soliton") {
            u0 = iv_soliton;
            _a1 = -20.0;   _b1 = 20.0;   _a2 = -20.0;   _b2 = 20.0;
        } else if(problem == "schwartzian") {
            u0 = iv_schwartzian;
            _a1 = -5*M_PI; _b1 = 5*M_PI; _a2 = -5*M_PI; _b2 = 5*M_PI;
        } else if(problem == "zaitsev") {
            u0 = iv_zaitsev;
            _a1 = -25.0; _b1 = 25.0;  _a2 = -2*M_PI; _b2 = 2*M_PI;
        } else if(problem == "zaitsevpert") {
            u0 = iv_zatisev_perturbed;
            _a1 = -25.0; _b1 = 25.0;  _a2 = -2*M_PI; _b2 = 2*M_PI;
        } else {
            cout << "ERROR: Problem " << problem << " is not available "
                 << "(accepted values are soliton, schwartzian, zaitsev, and "
                 << "zaitsevpert)" << endl;
            exit(1);
        }

        array<fp,2> a = {{_a1,_a2}}; array<fp,2> b = {{_b1,_b2}};
        KP kp(a, b, num_cells_x, ox, num_cells_y, T, tau, u0, lambda, epsilon,
                number_snapshots, num_it, im, inm);
        kp.run();
    }

    return 0;
}

